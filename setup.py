# -*- coding: utf-8 -*-
"""Rozšíření pro Plone ziskej.site pro Získej app.
"""

from setuptools import find_packages
from setuptools import setup


setup(
    name='ziskej.site',
    version='2.9.1',
    description="An add-on for Plone for Ziskej app",
    long_description='',
    # Get more from https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        "Environment :: Web Environment",
        "Framework :: Plone",
        "Framework :: Plone :: 5.0",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
    ],
    keywords='Python Plone',
    url='',
    author='AOW',
    author_email='aow@aow.cz',
    license='GPL version 2',
    packages=find_packages('src', exclude=['ez_setup']),
    namespace_packages=['ziskej'],
    package_dir={'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'plone.api',
        'Products.GenericSetup>=1.8.2',
        'setuptools',
        'z3c.jbot',
        'plone.app.dexterity',
        'plone.app.contenttypes',
        'plone.directives.form',
        'plone.validatehook',
        'collective.z3cform.datagridfield',
        'Products.TextIndexNG3',
        'czech-sort',
        'dexterity.membrane',
        #'Products.AutoUserMakerPASPlugin',
        #'Products.OneTimeTokenPAS',
        #'niteoweb.loginas',
        #'collective.impersonate',
        'collective.behavior.internalnumber',
        'pyjwt',
        'requests',
#        'urllib3',  # cpk_api_lib - dočasně
#        'multicache',  # cpk_api_lib - dočasně
#        'pymarc',  # cpk_api_lib - dočasně
        'openpyxl',  # pip install openpyxl
    ],
    extras_require={
        'test': [
            'plone.app.testing',
            # Plone KGS does not use this version, because it would break
            # Remove if your package shall be part of coredev.
            # plone_coredev tests as of 2016-04-01.
            'plone.testing>=5.0.0',
            'plone.app.contenttypes',
            'plone.app.robotframework[debug]',
        ],
    },
    entry_points="""
    [z3c.autoinclude.plugin]
    target = plone
    """,
)
