===========
ziskej.site
===========

ziskej.site je hlavním balíčkem Ziskej app a obsahuje funkcionalitu (logiku)
aplikace.  Silně závisí na Zope a/nebo Plone.

*********************
Struktura ziskej.site
*********************

Struktura balíčku je standardní:

content
=======

Dexterity CT (Content Types, typy obsahu), definice.  Pro detailní dokumentaci
viz příslušné soubory, jako např. content/library.py pro Library CT resp.
content/libraryct.py for LibraryCT CT (první CT je pro Container, v tomto
případě kontejner pro libraries, druhé CT je pro Content Type).

browser
=======

Definice pohledů, browser views.  Browser views jsou seskupeny ve složkách jako
třeba admin, content_view atp.

browser/viewlet
===============

Speciální složkou uvnitř browser je složka viewlet obsahující viewlety, třídy
(py) a šablony (pt).  Viewlet je část stránky, jako třeba footer (patička
stránky) nebo header (hlavička stránky), obvykle obdobný na všech stránkách. 
Může být statický, jak je obvyklé pro footer nebo závislý na kontextu, jak je
obvyklé pro navigaci, záložky v hlavičce, breadcrumbs, volba jazyka atp.

profiles
========

Úvodní konfigurace je obsažena v profilu pro portal_setup v profiles/default
jak je obvyklé.  Viz portal_setup tool ve ZMI.

helpers
=======

Balíček bsahuje značné množství pomocných nástrojů (helpers) v kořenové složce.
Některé jsou závislé na Zope / CMF / Plone a některé jsou nezávislé.  Nezávislé
by mohly být separovány v samostatném balíčku, nezávislém na Zope / Plone.

Viz dokumentaci v každém z nich.

Příklady: ticket_helpers, ticket_load_balancing, cpk_api, marc21, notifications.

auth
====

Obsahuje autentizaci a autorizaci a příslušné nástroje.

behavior
========

Definuje bahaviors pro Dexterity CT definice.

data
====

Obsahuje příklady dat z CPK API atp.

locales
=======

Placeholder pro lokalizaci balíčku.

migration
=========

Někdy je změněna definice obsahu nbeo worlflow nebo portal_catalog a je potřebné
změnit existující obsah nebo provést jednorázové kroky (přidat index, rozšířit
metadata každého brain výsledku hledání v portal_catalog atp.) - tyto situace
pokrývají migrace.  Používají portal_setup.

tests
=====

Placeholder pro testy.

************
GenericSetup
************

Plone 5 používá pro uložení konfigurace GenericSetup.  Úvodní konfigurace
komponenty ziskej.site je v profiles/default.

Konfiguraci lze upravovat TtW (Through the Web) a exportovat v xml podobě
víceméně odpovídající profiles/default/, upravit v xml a následně importovat.
ZMI: /portal_setup/manage_main.


********************
Content Types aka CT
********************

*Poznámka: vše popsáno na příkladu CT, který reprezentuje knihovnu (např.
Národní technickou knihovnu).*

Používáme Dexterity framework pro definici typů obsahu.

Tip: pro načtení jakékoli změny v content nebo profiles xml je lepší restart
Zope.  Pro nový CT nebo změnu v profiles xml navíc následně reinstalace
ziskej.site na adrese /portal_quickinstaller/manage_installProductsForm.
Reinstalaci lze obejít pomocí selektivního importu v /portal_setup.

content
=======

Definice atributů viz content/library.py class ILibrary(Interface).  Python
nemá Interface, toto je umělý konstrukt Zope 3, který zaručí podobnou
funkcionalitu.

Samotná implementace viz content/library.py classLibrary(Container).

Všechny CT jsou Container.  Mohly by být i Item, ale z důvodů další
rozšiřovatelnosti např. o přílohy (soubory, obrázky) byl zvolen Container pro
vše.

Všechny CT dědí z IBase/Base viz content/base.py, je tedy možno dát všem
společný atribut nebo metodu.

browser (BrowserView)
=====================

Pro pohledy na CT se používá BrowserView viz browser/library.py s konfigurací
v browser/configure.zcml.

atributy
--------

Tag browser:page typicky obsahuje atributy:

- name .. view pokud jde o defaultní pohled na CT (proč zrovna view viz
  profiles/default/types/library.xml sekce default_view a action sction_id
  view)

- for .. pro který Interface - může být pro konkrétní CT např.
  ziskej.site.content.library.ILibrary nebo pro jakýkoli CT implementující
  konkrétní Inteface - např. IBase pro všechny CT definované v ziskej.site nebo
  * pro všechny CT

- class .. modul a třída, jejíž __call__ se požije, např. .library.LibraryView

- permission .. jakou permission potřebuje uživatel mít, aby měl přístup
  k tomuto pohledu, např. zope2.View


********
Membrane
********

Pro uživatele (čtenář, knihovník) používáme Dexterity CT - pomocí membrane
rozšíření a prostřednictvím PAS.

Settings
========

/@@dexteritymembrane-settings

- Use email address for username?  False

- Use object UUID for the userid?  False

Related docs:

- Members as content: https://docs.plone.org/develop/plone/members/membrane.html

- Moving members (nepotřebujeme): https://docs.plone.org/develop/plone/members/membrane.html#moving-members

