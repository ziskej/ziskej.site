# -*- coding: utf-8 -*-
"""Zapouzdření CPK API pro potřeby Získej app.  Část proběhne už
v ziskej.cpk_api, zbytek zde.  Tato verze se od cpk_api odlišuje zejména
v možnosti asynchronních (neblokujících) požadavků a je nyní používána jen pro
dotazy na dostupnost jednotek.
"""

import json
import os.path
import requests
import logging
import time

try:
    from ziskej.site import ZISKEJ_ASYNC_BASE_URL, USE_ASYNC_PORT_9080
    from ziskej.site.utils_lib import pprint, safe_unicode
except ImportError:
    from . import ZISKEJ_ASYNC_BASE_URL, USE_ASYNC_PORT_9080
    from utils_lib import pprint, safe_unicode

# # https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
# #import urllib3
# #urllib3.disable_warnings()

# # https://stackoverflow.com/questions/27981545/suppress-insecurerequestwarning-unverified-https-request-is-being-made-in-pytho#28002687
# from requests.packages.urllib3.exceptions import InsecureRequestWarning
# requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# # https://stackoverflow.com/questions/11029717/how-do-i-disable-log-messages-from-the-requests-library#11029841
# logging.getLogger("requests").setLevel(logging.WARNING)
# #logging.getLogger("urllib3").setLevel(logging.WARNING)


logger = logging.getLogger("ziskej.site.cpk_api")

DEBUG = False

REQUESTS_VERIFY = False


class CpkApiException2(Exception):
    pass


class CpkApiBase(object):
    """Base for all connections to CPK API wrapped by Flask app.  CPK API
    related validations, parsing (including MARC21 XML parsing) and changes are
    implemented in ziskej.cpk_api.  Here only additional validation, parsing and
    changes happens.

    This class provides base for each type of request.

    Be aware some request leads to multiple requests to CPK API at the end.
    """

    def __init__(self, settings=None):
        super(CpkApiBase, self).__init__()

        # settings
        # now only base_url is used - in the future we might to have some Flask
        # or CPK API parameters such as timeout, CPK API host and port
        # configured in settings too
        if settings is not None:
            self.settings = settings
        else:
            # default settings
            self.settings = dict(base_url = ZISKEJ_ASYNC_BASE_URL)
        self.base_url = self.settings.get('base_url', None)
        self.base_method = None  # see sync/async
        self.is_sync = None  # see sync/async

    def _request_base(self, url, is_async_retrieve=False):
        """Calling Flask app itself including catching errors such as Flask app
        is down and including basic Flask app respond parsing to have proper CPK
        API result.
        """

        if DEBUG:
            print 'url: {}'.format(url)
        result_obj = requests.get(url, verify=REQUESTS_VERIFY)

        # Flask app request failed at all, probably FLask app is donw etc.
        if not result_obj.ok:
            if DEBUG:
                pprint(result_obj)
            logger.error('URL: {url}'.format(url=url))
            logger.warning('Request failed')
            raise CpkApiException2('Request failed')

        # Flask app response is always json - if not it is an error
        try:
            result_json = result_obj.json()
        except Exception, e:
            logger.exception('Invalid flask response - no json')
            raise CpkApiException2('Request failed')

        # Not yet
        if is_async_retrieve and result_json.get('status', None) == 'NotYet':
            return None

        # {'status': 'Error', 'error_message': 'Lorem ipsum'}
        if result_json.get('status', None) != 'OK':
            logger.warning(u'CPK API failed: {}'.format(safe_unicode(result_json.get('error_message', u'Unknown error'))))
            raise CpkApiException2('Request failed')

        # {'status': 'OK', 'data': 'Lorem ipsum'}
        data = result_json.get('data', None)

        return data


class CpkApiBaseSync(CpkApiBase):
    """Async version - not blocking design.
    """

    def __init__(self, settings=None):
        super(CpkApiBaseSync, self).__init__(settings)

        # sync / async
        self.is_sync = True

        if USE_ASYNC_PORT_9080:
            self.base_method = u'cpkd'
        else:
            self.base_method = u'cpkapi/sync'

    def _request(self, url):
        return self._request_base(url)


class CpkApiBaseAsync(CpkApiBase):
    """Sync version - blocking design.
    """

    def __init__(self, settings=None):
        super(CpkApiBaseAsync, self).__init__(settings)

        # sync / async
        self.is_sync = False

        if USE_ASYNC_PORT_9080:
            self.base_method = u'cpk'
        else:
            self.base_method = u'cpkapi/async/init'

    def _request_init(self, url):
        data = self._request_base(url)

        rid = data.get('rid', None)
        if not rid:
            logger.exception('Invalid flask response - no rid')
            raise CpkApiException2('Request failed')

        return rid

    def _request_retrieve(self, rid):
        if USE_ASYNC_PORT_9080:
            url = '{base_url}retrieve/{rid}'.format(
                base_url = self.base_url,
                rid = rid)
        else:
            url = '{base_url}cpkapi/async/retrieve/{rid}'.format(
                base_url = self.base_url,
                rid = rid)

        # None for NotYet
        data = self._request_base(url, is_async_retrieve=True)
        return data


class CpkUnitAsync(CpkApiBaseAsync):

    def call_init(self, unit_id):
        url = '{base_url}{base_method}/unit?id={unit_id}'.format(
            base_url = self.base_url,
            base_method = self.base_method,
            unit_id = unit_id)

        rid = self._request_init(url)
        return rid

    def call_retrieve(self, rid):
        result = self._request_retrieve(rid)

        # NotYet
        if result is None:
            return None

        # parse result
        data = dict()
        try:
            data['id'] = result['id']
            data['availability'] = result['availability']
            data['duedate'] = result['duedate']
            data['json'] = result  # SUGGESTION prověřit
        except KeyError:
            logger.exception('Invalid flask response - unexpected json structure')
            raise CpkApiException2('Request failed')
        return data
