# -*- coding: utf-8 -*-
"""Validátory pro Dexterity CT.
"""

import re
import logging
import requests

from zope.interface import Invalid
from plone import api

from ziskej.site import _


logger = logging.getLogger("ziskej")

EMAIL_REGEXP = re.compile(r"([0-9a-zA-Z_&.'+-]+!)*[0-9a-zA-Z_&.'+-]+"
    "@(([0-9a-zA-Z]([0-9a-zA-Z-]*[0-9a-z-A-Z])?\.)+"
    "[a-zA-Z]{2,6}|([0-9]{1,3}\.){3}[0-9]{1,3})$")

ID_REGEXP = re.compile(r"[a-zA-Z]+[0-9a-zA-Z_+]*$")

PHONE = "+-/()1234567890 "

def validate_phone(value):
    """Zjednodušená validace tel. čísel, jako validní jsou považovány i hodnoty,
    které ve skutečnosti tel. číslem nejsou.
    """

    for k in value:
        if k not in PHONE:
            raise Invalid(_(u"Neplatné telefonní číslo"))
    return True

def validate_email(value):
    if value:
        if not EMAIL_REGEXP.match(value):
            raise Invalid(_(u"Neplatná emailová adresa"))
    return True

def validate_idp_entityid(value):
    # https://shibo.vkol.cz/idp/shibboleth
    if not value:
        return True
    if '?' in value:
        return False
    try:
        result_obj = requests.get(value, verify=False)
        if result_obj.ok:
            print 'ok'
            # SUGGESTION možná raději xml parsovat
            result = bool(result_obj.text.startswith('<?xml'))
            print 'zacatek:'
            print result_obj.text[:128]
            print '--'
        else:
            print 'status_code'
            print result_obj.status_code
    except Exception, e:
        logger.info('Invalid validate_idp_entityid: ' + str(e))
        return False
    return result

def validate_username(value):
    if not value:
        return True

    if not ID_REGEXP.match(value):
        raise Invalid(_(u"Neplatné přihlašovací jméno"))

    if len(value) < 5:
        raise Invalid(_(u"Příliš krátké přihlašovací jméno"))

    # SUGGESTION validovat zda může být id
    if value in ('admin', 'olda', 'aow', 'ntk', 'ziskej', ):
        raise Invalid(_(u"Rezervované přihlašovací jméno"))

    # SUGGESTION jednoznačnost mezi zope uživateli

    return True

def validate_unique_username(value, context=None):
    """Jednoznačnost mezi uživateli - knihovníky a čtenáři s přihlédnutím
    k tomu, že právě můžeme editovat uživatele beze změny username.

    Inspirováno dexterity.membrane.embrane.helpers validate_unique_email.
    """

    if not value:
        return
    # SUGGESTION mcatalog (membrane)
    catalog = api.portal.get_tool(name='portal_catalog')
    query = dict(
        portal_type = ('Librarian', 'Reader', ), 
        getId = value,
        )
    brains = catalog(query)
    len_brains = len(brains)
    if len_brains == 0:
        return
    if len_brains > 1:
        msg = "Multiple matches on username {username}".format(username=value)
        logger.warn(msg)
        return _(u"Toto username je už použité")

    # Might be this member, being edited.  That should have been
    # caught by our invariant though
    match = brains[0]
    try:
        found = match.getObject()
    except (AttributeError, KeyError, Unauthorized):
        # This is suspicious.  Best not to use this one.
        pass
    else:
        if found == context:
            # We are the only match.  Good.
            logger.debug("Only this object itself has username {username}".format(username=value))
            return

    # There is a match but it is not this member or we cannot get
    # the object.
    msg = "username {username} is already in use.".format(username=value)
    logger.debug(msg)
    return _(u"Toto username je už použité")
