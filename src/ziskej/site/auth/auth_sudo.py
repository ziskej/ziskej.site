# -*- coding: utf-8 -*-
"""Autentizace pro administrátora umožňující testovat jako uživatel se
specifickou rolí.  Později zrušeno a nahrazeno.
"""

import logging

from plone import api

from ziskej.site.utils_lib import pprint, safe_utf8, safe_unicode


logger = logging.getLogger('ziskej.auth')

# sudo
# FIXME-sudo
# sudo je víceméně hotové, chybí notifikace a audit log (možná), pak otestovat.
# Ad notifikace: operátor jedná za knihovnu a ta teď proto nedostane notifikaci.
SUDO_ENABLED = True

COOKIE_SUDO = 'op_su_sigla_storage'
PARAMETER_SUDO = 'op_su_sigla'


def get_active_library_by_sigla(portal, sigla):
    if not sigla:
        return None
    try:
        sigla = str(sigla)
    except UnicodeDecodeError:
        return None
    library = getattr(portal.libraries, sigla, None)
    if not library:
        return None
    if not library.is_active:
        return None
    return library

def auth_sudo(context, request, user_dict):

    # Nastavit vše pro možnost sudo
    portal = api.portal.get()
    base_url = context.absolute_url()
    user_dict['sudo_marker'] = False

    # Původní nastavení
    sudo_sigla = request.cookies.get(COOKIE_SUDO, None)
    library = get_active_library_by_sigla(portal, sudo_sigla)
    if not library:
        sudo_sigla = None

    # Požadováno nastavení
    par_sudo = request.form.get(PARAMETER_SUDO, None)    
    if par_sudo is not None:
        par_library = get_active_library_by_sigla(portal, par_sudo)
        if not par_library:
            par_sudo = u''
        if par_sudo:
            sudo_sigla = par_sudo
            library = par_library
            request.response.setCookie(COOKIE_SUDO, sudo_sigla, path="/")
        else:
            sudo_sigla = None
            library = None
            request.response.expireCookie(COOKIE_SUDO, path="/")

    # Zjistit, zda je aktivní sudo
    logger.info('sudo_sigla: {}'.format(sudo_sigla))

    # Není aktivní, vše hotovo
    if not sudo_sigla:
        # FIXME-sudo toto je jen pro rychlejší testování, šlo by sice uvažovat
        # o tom sem dát nejpoužívanější tři sudo, ale to je overkill
        example_sigla = 'aba013'
        user_dict['sudo_links'].append(dict(
            url='{}?{}={}'.format(base_url, PARAMETER_SUDO, example_sigla),
            label='Zahájit práci jako knihovník knihovny {}'.format(
                example_sigla.upper()),
        ))
        return None
    
    # Jinak nastavit sudo
    if library:
        user_dict['sudo_links'].append(dict(
            url='{}?{}='.format(base_url, PARAMETER_SUDO),
            label='Ukončit práci za knihovníka knihovny {}'.format(
                library.Title()),
        ))
        user_dict['library'] = library
        user_dict['role'] = 'Librarian'
        user_dict['sudo_marker'] = True  # marker, že sudo je právě teď aktivní
        return user_dict

    return None
