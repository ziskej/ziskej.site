# -*- coding: utf-8 -*-
"""Základní utility pro autentizaci a autorizace.
"""

import logging
import random
import string

from plone import api
from plone.dexterity.utils import createContent
from Products.CMFCore.utils import getToolByName

from ziskej.site.utils_lib import pprint, safe_utf8, safe_unicode


logger = logging.getLogger('ziskej.auth')


# General

def suggest_password():
    # SUGGESTION https://www.python.org/dev/peps/pep-0506/
    chars = string.letters + string.digits
    length = 32
    value = ''.join(random.sample(chars, length))
    return value


# Plone auth get info

def is_manager(context, request):
    # Anonymous
    is_anon = api.user.is_anonymous()    
    if is_anon:
        return False

    member = api.user.get_current()

    # Manager
    roles = api.user.get_roles(username=str(member))
    is_manager_value = bool('Manager' in roles)

    return is_manager_value

def get_groups(context):
    membership = getToolByName(context, 'portal_membership')
    return membership.getAuthenticatedMember().getGroups()


# Plone auth log in

def plone_login(username, request):
    """Log in user with login name username.  Return user obj if membrane user
    otherwise None.
    """

    #transaction.commit()
    portal = api.portal.get()
    logger.info('now log in as {username}'.format(username=username))

    portal.acl_users.session._setupSession(username, request.RESPONSE)
    #portal.acl_users.session._setupSession('test1', request.RESPONSE)

    #password = '12345'
    #user_ = portal.acl_users.authenticate(username, password, request)
    #user_ = portal.acl_users.membrane_users.authenticate(username, password, request)
    #user_ = portal.acl_users.authenticate('test1', password, request)
    #logger.info('user_: {username}'.format(username=str(user_)))

    #transaction.commit()
    member = api.user.get_current()
    user_id = str(member)
    logger.info('plone_user: {username}'.format(username=user_id))
    if user_id == username:
        user = get_user_obj_by_user_id(user_id)
    else:
        user = None

    return user


# Plone create / update

def create_librarian(library, obj_dict):
    print "obj_dict"
    print obj_dict

    obj_id = str(obj_dict['id'])

    # see https://docs.plone.org/external/plone.app.dexterity/docs/reference/manipulating-content-objects.html
    # see alse https://docs.plone.org/develop/plone/content/creating.html
    obj = createContent('Librarian', **obj_dict)
    library[obj_id] = obj
    obj = library[obj_id]

    print "obj: {info}".format(info=str(obj))
    print '------------------------------------------------------'
    logger.info('Librarian {obj_id} has been created for {sigla}'.format(obj_id=obj_id, sigla=library.sigla))

    return obj

def plone_update_obj(obj, obj_dict):
    is_updated = False
    for k in obj_dict:
        v_orig = getattr(obj, k, None)
        v = obj_dict.get(k, None)
        if v_orig != v:
            setattr(obj, k, v)
            try:
                logger.info('doit for {k}: {v_orig} to {v}'.format(k=k, v_orig=v_orig, v=v))
            except UnicodeEncodeError:
                logger.info('doit for {k}'.format(k=k))
            is_updated = True
    return is_updated


# Plone Catalog / Membrane catalog

def get_user_obj_by_user_id(user_id, warnings=True):
    """Vyhledá objekt uživatele na základě user_id, None pokud se nejedná ani
    o čtenáře ani o knihovníka - tzn. obyčejný Zope uživatel - operátor nebo
    admin.
    """

    if not user_id:
        return None
    if user_id in ('admin', ):
        return None
    portal = api.portal.get()
    mcatalog = portal.membrane_tool
    query = dict(
        # getUserName = user_id,
        getId = user_id,
        )
    objs = []
    brains = mcatalog(query)
    for brain in brains:
        objs.append(brain.getObject())
    if len(objs) == 0:
        logger.info('get_user_obj_by_user_id: user not found: {}'.format(user_id))
        return None
    if len(objs) > 1:
        logger.info('Multiple Reader objs for user_id {user_id}'.format(user_id=user_id))
    return objs[0]

def get_reader_by_eppn(eppn):
    """Find Reader object (membrane) by eppn aka eduPersonPrincipalName.
    """

    portal = api.portal.get()
    mcatalog = portal.membrane_tool

    query = dict(
        portal_type = 'Reader',
        eppn = eppn,
        sort_on = 'modified',
        sort_order = 'reverse',
        )

    objs = []
    brains = mcatalog(query)
    for brain in brains:
        objs.append(brain.getObject())

    if len(objs) == 0:
        return None

    NO_GRACE_FOR_MULTIPLE = True
    if len(objs) > 1:
        if NO_GRACE_FOR_MULTIPLE:
            raise Exception('eppn is not unique')
        # otherwise log it and return last modified
        logger.warning('Multiple Reader objs for eppn {eppn}'.format(eppn=eppn))
        return objs[0]
        
    return objs[0]

def get_librarians_by_library(library=None, library_id=None, is_library_admin=None):
    """Vyhledá objekty knihovníků dané knihovny library (nebo podle library_id).
    is_library_admin None .. bez restrikce, True .. jen správci, False .. jen
    nesprávci.
    """

    portal = api.portal.get()

    if library is None:
        if not library_id:
            return None
        library = getattr(portal.libraries, library_id, None)
        if not library:
            return None

    library_path = '/'.join(library.getPhysicalPath())
    mcatalog = portal.membrane_tool
    query = dict(
        portal_type = 'Librarian',
        path = library_path,
        )
    brains = mcatalog(query)

    objs = []
    for brain in brains:
        obj = brain.getObject()
        if is_library_admin is not None:
            if is_library_admin and not obj.is_library_admin:
                continue
            if not is_library_admin and obj.is_library_admin:
                continue
        objs.append(obj)

    if len(objs) == 0:
        return None
    return objs

def get_library_by_idp_entityid(idp_entityid, only_active=True):
    catalog = api.portal.get_tool(name='portal_catalog')
    query = dict(
        portal_type = u'Library',
        idp_entityid = safe_unicode(idp_entityid),
        )
    if only_active:
        query['review_state'] = 'published'

    # https://docs.plone.org/develop/plone/searching_and_indexing/query.html#bypassing-query-security-check
    brains = catalog.unrestrictedSearchResults(query)

    objs = [brain._unrestrictedGetObject() for brain in brains]

    if len(objs) == 0:
        return None
    if len(objs) > 1:
        raise Exception('Multiple libraries with same EntityID IdP')
    return objs[0]
