# -*- coding: utf-8 -*-
"""Autentizace pomocí CPK API a Shibbolethu v CPK.
"""

import json
import logging
import uuid
import transaction

from DateTime import DateTime
from plone import api
from plone.dexterity.utils import createContent

from ziskej.site.auth.auth_base import (
    get_library_by_idp_entityid,
    get_reader_by_eppn,
    plone_login,
    plone_update_obj,
    suggest_password,
    )
from ziskej.site.utils_lib import pprint, safe_utf8, safe_unicode
from ziskej.site.validators import validate_email, validate_idp_entityid


logger = logging.getLogger('ziskej.auth')


def auth_ziskejapi(context, request, obj_dict):
    """Získej API pro CPK založené na CPK Shibboleth
    """

    portal = api.portal.get()

    #if not validate_email(email):

    # dt_now
    dt_now = DateTime()

    # username, ntk_user_id
    rnd_uuid = str(uuid.uuid4()).replace('-', '')[:12]
    # username (obj_id) has to be ascii string not unicode
    username = 'r_{rnd_uuid}'.format(rnd_uuid=rnd_uuid)
    obj_id = username
    ntk_user_id = 'ziskej_{rnd_uuid}'.format(rnd_uuid=rnd_uuid)

    # any password
    password = suggest_password()

    # create plone user
    obj_dict_ = dict(
        # username, ntk user
        id = obj_id,
        username = username,
        usertype = 'ziskejapi',
        ntk_user_id = ntk_user_id,
        # fullname
        first_name = obj_dict['first_name'],
        last_name = obj_dict['last_name'],
        # email
        email = obj_dict['email_user'],
        email_user = obj_dict['email_user'],
        email_user_verification_code = '',
        is_email_user_validated = False,
        is_notification_enabled = obj_dict['is_notification_enabled'],
        # library
        library = obj_dict['library'],
        reader_lid = u'',
        is_library_confirmed = False,
        libraries = None,
        library_claim = '',
        # shibboleth
        eppn = obj_dict['eppn'],
        idp_entityid = None,
        identities_json = None,
        reader_library_id = obj_dict.get('reader_library_id', u''),
        # gdpr
        gdpr_registration = obj_dict['gdpr_registration'],
        gdpr_registration_date = dt_now,
        gdpr_data = obj_dict['gdpr_data'],
        gdpr_data_date = dt_now,
        # timestamps
        last_login_date = dt_now,
        # password
        password=password,
        confirm_password=password,
        )
    print "obj_dict_"
    print obj_dict_

    # see https://docs.plone.org/external/plone.app.dexterity/docs/reference/manipulating-content-objects.html
    # see alse https://docs.plone.org/develop/plone/content/creating.html
    obj = createContent('Reader', **obj_dict_)
    portal.readers[obj_id] = obj
    obj = portal.readers[obj_id]

    print "obj: {info}".format(info=str(obj))
    print '------------------------------------------------------'
    logger.info('Reader {obj_id} has been created'.format(obj_id=obj_id))

    return obj
