# -*- coding: utf-8 -*-
"""Autentizace a autorizace a příslušné nástroje.

Propojuje Zope / Plone a specifické části jako například Shibboleth, Perun atp.
"""

import logging
import urllib

from plone import api
#from AccessControl.unauthorized import Unauthorized
from Acquisition import aq_inner, aq_parent

from ziskej.site.auth.auth_base import (
    get_groups,
    get_librarians_by_library,
    get_user_obj_by_user_id,
    )
from ziskej.site.auth.auth_sudo import auth_sudo, SUDO_ENABLED
from ziskej.site.utils_lib import pprint, safe_utf8, safe_unicode


logger = logging.getLogger('ziskej.auth')


def auth_ziskej_role(context, request, caller=None):
    """Vrací Získej roli a příslušnou knihovnu aktuálního uživatele.
    """

    user_dict = dict(
        user_id = None,
        user = None,
        role = None,
        library = None,
        is_anon = False,
        is_manager = False,
        is_ziskej_admin = False,
        is_ziskej_operator = False,
        is_library_admin = False,
        is_admin = False,
        is_edit = False,
        sudo_marker = False,
        sudo_links = [],
        )

    portal = api.portal.get()

    # Anonymous
    is_anon = api.user.is_anonymous()
    if is_anon:
        user_dict['is_anon'] = True
        return user_dict

    member = api.user.get_current()

    # Manager
    roles = api.user.get_roles(username=str(member))
    is_manager_value = bool('Manager' in roles)
    user_dict['is_manager'] = is_manager_value

    # groups
    user_dict['groups'] = get_groups(context)
    user_dict['is_ziskej_admin'] = user_dict['is_manager'] or 'ZiskejAdmin' in user_dict['groups']
    user_dict['is_ziskej_operator'] = user_dict['is_ziskej_admin'] or 'ZiskejOperator' in user_dict['groups']
    # admin v daném kontextu (např. správce knihovny v kontextu jeho knihovny)
    user_dict['is_admin'] = user_dict['is_ziskej_operator']  # SUGGESTION ověřit
    # může v daném kontextu alespoň něco editovat (např. knihovník v kontextu jeho knihovny)
    user_dict['is_edit'] = user_dict['is_ziskej_operator']  # SUGGESTION ověřit

    # sudo
    if SUDO_ENABLED and user_dict['is_ziskej_operator']:
        _user_dict = None
        try:
            _user_dict = auth_sudo(context, request, user_dict)
        except Exception, e:
            logger.warning('sudo failed: ' + str(e))
        if _user_dict:
            user_dict = _user_dict

    # Reader or Librarian
    user_id = str(member)
    user_dict['user_id'] = user_id
    user = get_user_obj_by_user_id(user_id)

    if user:

        user_dict['user'] = user

        #if user.portal_type in ('Reader', 'Librarian', ):
        user_dict['role'] = user.portal_type

    # Aktuální knihovna
    if user and user_dict['role'] == 'Reader':
        if user.library and isinstance(user.library, basestring):
            user_library = user.library[:6].lower()
            user_dict['library'] = getattr(portal.libraries, user_library, None)
            #print user_library
            #print user_dict['library']
        #if (not user.library or not user_dict['library']) and user.libraries and len(user.libraries):
        #    user_library = user.libraries[0][:6].lower()
        #    user_dict['library'] = getattr(portal.libraries, user_library, None)
        #    #print user_library
        #    #print user_dict['library']
    
    if user and user_dict['role'] == 'Librarian':
        user_dict['is_library_admin'] = bool(user.is_library_admin)
        library = aq_parent(aq_inner(user))
        # Librarian, který není bezprostředně v knihovně
        if library.portal_type != 'Library':
            library = None  
            logger.error('Missing Library for Librarian {user_id}'.format(user_id=user_id))
            raise Exception('Misconfigured Library')
        user_dict['library'] = library

    # if is_manager_value:
    #     user_library = 'aba013'
    #     user_dict['library'] = getattr(portal.libraries, user_library, None)
    #     user_dict['role'] = 'Librarian'

    return user_dict

def auth_wrapper(context, request, url=None):
    """Zapouzdřuje použití zvolené (viz nastavení) verze autentizace - Perun,
    CPK API nebo Zope.
    """

    if not api.user.is_anonymous():
        member = api.user.get_current()
        user_id = str(member)
        user = get_user_obj_by_user_id(user_id)
        return user

    # Ostatní způsoby autentizace jsou zrušeny.
    auth_zope(context, request, url)
    user = None

    if bool(user is None) != api.user.is_anonymous():
        raise Exception('Internal error')

    return user

def auth_zope(context, request, url=None):
    """Zope auth - je voláno, pokud uživatel není přihlášen a je vynuceno
    standardní Zope přihlášení.
    """

    portal = api.portal.get()
    if url:
        url_encoded = urllib.urlencode({'from': url})
        login_url = '{portal_url}/login_form?{url_encoded}'.format(portal_url=portal.absolute_url(), url_encoded=url_encoded)
    else:
        login_url = '{portal_url}/login_form'.format(portal_url=portal.absolute_url())
    logger.info('login_url: {login_url}'.format(login_url=login_url))
    request.RESPONSE.redirect(login_url)
    return
