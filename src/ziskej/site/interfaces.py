# -*- coding: utf-8 -*-
"""Obsahuje interfaces nespecifické pro Dexterity CT, ty jsou součástí
content/library.py atp.
"""

from zope.publisher.interfaces.browser import IDefaultBrowserLayer


class IZiskejSiteLayer(IDefaultBrowserLayer):
    """Marker interface that defines a browser layer."""
