# -*- coding: utf-8 -*-
"""Pomocný nástroj pro obecné úložiště pomocí Zope anotací, které se neukládájí
přímo v perzistentním objektu v ZODB, ale samostatně, na daný objekt navázaně.
Snižuje pravděpodobnost konfliktů mezi transakcemi.
"""

from DateTime import DateTime
from zope.annotation.interfaces import IAnnotations
from persistent.list import PersistentList
from persistent.dict import PersistentDict
from Products.CMFCore.utils import getToolByName


KEY = "ziskej.site.storage_key_value"
KEY_EXP = "ziskej.site.storage_key_value_exp"

DEBUG = False


def setupAnnotations(context):
    """Set up the annotations if they haven't been set up already. The rest
    of the functions in here assume that this has already been set up.
    """

    annotations = IAnnotations(context)
    if not KEY in annotations:
        annotations[KEY] = PersistentDict()
    if not KEY_EXP in annotations:
        annotations[KEY_EXP] = PersistentDict()
    return annotations

def storage_set(context, key, value, expire=None):
    """Create new or update existing key: value with optional expiration as
    ISO datetime 2018-05-31T12:30:12 local time.
    """

    if DEBUG:
        try:
            print u'storage_set {} value {} expire {}'.format(key, value, expire)
        except:
            print u'storage_set {} value ??? expire {}'.format(key, expire)
    annotations = setupAnnotations(context)
    annotations[KEY][key] = value
    if expire is not None:
        now = DateTime()
        if expire == u'24h':
            expire = now + 1
        elif expire == u'1h':
            expire = now + 1.0/24
        elif expire == u'15min':
            expire = now + 1.0/24/60*15
        else:
            raise Exception('Uknown expiration')
        annotations[KEY_EXP][key] = expire.ISO()

def storage_get(context, key, default=None):
    """Get value or default if not exist"""
    if DEBUG:
        print u'storage_get {}'.format(key)
    annotations = setupAnnotations(context)
    if key in annotations[KEY]:
        if key in annotations[KEY_EXP]:
            expire = DateTime(annotations[KEY_EXP][key])
            if DateTime() > expire:
                storage_del(context, key)
                if DEBUG:
                    try:
                        print u'storage_get {} expired default {}'.format(key, default)
                    except:
                        print u'storage_get {} expired default ???'.format(key)
                return default
        value = annotations[KEY][key]
        if DEBUG:
            try:
                print u'storage_get {} value {}'.format(key, value)
            except:
                print u'storage_get {} value ???'.format(key)
        return value
    if DEBUG:
        try:
            print u'storage_get {} default {}'.format(key, default)
        except:
            print u'storage_get {} default ???'.format(key)
    return default

def storage_del(context, key):
    """Remove key: value if exists"""
    if DEBUG:
        print u'storage_del {}'.format(key)
    annotations = setupAnnotations(context)
    if key in annotations[KEY]:
        del annotations[KEY][key]
    if key in annotations[KEY_EXP]:
        del annotations[KEY_EXP][key]

def storage_keys(context):
    if DEBUG:
        print u'storage_keys'
    annotations = setupAnnotations(context)
    return annotations[KEY].keys()

def storage_garbage(context):
    if DEBUG:
        print u'storage_garbage'
    annotations = setupAnnotations(context)
    keys = annotations[KEY].keys()
    for key in annotations[KEY_EXP].keys():
        if key not in keys:
            del annotations[KEY_EXP][key]
    for key in keys:
        storage_get(context, key)

def storage_reset(context):
    if DEBUG:
        print u'storage_garbage'
    annotations = setupAnnotations(context)
    keys = annotations[KEY].keys()
    for key in annotations[KEY_EXP].keys():
        if key not in keys:
            del annotations[KEY_EXP][key]
    for key in keys:
        storage_del(context, key)
