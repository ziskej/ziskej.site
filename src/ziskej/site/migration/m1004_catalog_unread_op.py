# -*- coding: utf-8 -*-

import logging

from plone import api
from DateTime import DateTime

from Products.ZCatalog.Catalog import CatalogError

from ziskej.site.utils import safe_utf8


logger = logging.getLogger("ziskej.migration")

PREFIX = 'm10002'


def _log(idx, total, path, info):
    logger.info('{prefix} {idx}/{total} [{info}] {path}'.format(
        idx=idx+1,
        total=total,
        path=path,
        prefix=PREFIX,
        info=info))

def _catalog(catalog):
    request = api.portal.getRequest()

    index_names = ['unread_op', 'complaint_state', ]
    index_type = dict(
        unread_op = 'BooleanIndex',
        )
    columns = ['unread_op', 'complaint_state', ]

    for index_name in index_names:
        try:
            catalog.delIndex(index_name)
        except CatalogError:
            pass
        catalog.addIndex(index_name, index_type.get(index_name, 'FieldIndex'))
        # nepotřebujeme reindex, protože následně děláme změnu všech objektů
        # vč. reindex objektu
        #catalog.reindexIndex(index_name, request)

    for column in columns:
        try:
            catalog.delColumn(column)
        except ValueError:
            pass
        print 'catalog.addColumn({})'.format(column)
        catalog.addColumn(column)
        # nepotřebujeme reindex, protože následně děláme změnu všech objektů
        # vč. reindex objektu

def _data(catalog):
    brains = catalog(portal_type=('TicketMVS', 'SubticketMVS', ))
    total = len(brains)
    for idx, brain in enumerate(brains):
        path = brain.getPath()
        obj = brain.getObject()

        _log(idx, total, path, 'initializing')

        # samotná změna pro unread a librarian
        obj.unread_op = bool(obj.complaint_state == u'objected')

        # reindex pro všechny změny
        obj.reindexObject()

def catalog_unread_op(context):
    catalog = api.portal.get_tool('portal_catalog')
    _catalog(catalog)
    _data(catalog)
