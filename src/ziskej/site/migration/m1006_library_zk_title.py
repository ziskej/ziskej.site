# -*- coding: utf-8 -*-

import logging

from plone import api
from Products.ZCatalog.Catalog import CatalogError


logger = logging.getLogger("ziskej.migration")

PREFIX = 'm10006'
DEBUG = False


def _log(idx, total, path, info, hid=None):
    if hid:
        hid = '{} '.format(hid)
    else:
        hid = ''
    logger.info('{prefix} {idx}/{total} [{info}] {hid}{path}'.format(
        idx=idx+1,
        total=total,
        path=path,
        prefix=PREFIX,
        info=info,
        hid=hid,
        ))

def _fix_tickets_subtickets(portal, catalog):
    libraries = portal.libraries

    brains = catalog(
        portal_type=('TicketMVS', 'SubticketMVS', ),
        sort_on='hid',
        sort_order='reverse',
        )
    total = len(brains)

    for idx, brain in enumerate(brains):
        path = brain.getPath()
        obj = brain.getObject()

        value_zk = obj.library_zk_title
        sigla_zk = obj.library_zk
        if sigla_zk:
            library_zk = getattr(libraries, sigla_zk, None)
        else:
            library_zk = None
        library_zk_title = u''
        library_zk_title_short = u''
        if library_zk:
            library_zk_title = library_zk.Title()
            library_zk_title_short = library_zk.Title(short=True)

        value_dk = obj.library_dk_title
        sigla_dk = obj.library_dk
        if sigla_dk:
            library_dk = getattr(libraries, sigla_dk, None)
        else:
            library_dk = None
        library_dk_title = u''
        library_dk_title_short = u''
        if library_dk:
            library_dk_title = library_dk.Title()
            library_dk_title_short = library_dk.Title(short=True)

        updated = False

        if DEBUG:
            print " "
            print "hid:", obj.hid, obj.portal_type
            print "library_zk_title:", repr(library_zk_title)
            print "library_zk_title_short:", repr(library_zk_title_short)
            print "value_zk:", repr(value_zk)
            print "library_dk_title:", repr(library_dk_title)
            print "library_dk_title_short:", repr(library_dk_title_short)
            print "value_dk:", repr(value_dk)

        if library_zk_title_short and library_zk_title_short != value_zk:
            obj.library_zk_title = library_zk_title
            updated = True

        if library_dk_title_short and library_dk_title_short != value_dk:
            obj.library_dk_title = library_dk_title
            updated = True

        if not updated:
            #_log(idx, total, path, 'skipping', hid=obj.hid)
            continue

        obj.reindexObject()

        _log(idx, total, path, 'fixing', hid=obj.hid)

        # if idx > 50:
        #     raise Exception('Debug')

def fix_library_zk_title(context):
    portal = api.portal.get()
    catalog = api.portal.get_tool('portal_catalog')
    logger.info('Fixing library.Title for tickets and subtickets library_zk_title and library_dk_title')
    _fix_tickets_subtickets(portal, catalog)
