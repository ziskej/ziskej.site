# -*- coding: utf-8 -*-

import logging

from plone import api
from Products.ZCatalog.Catalog import CatalogError


logger = logging.getLogger("ziskej.migration")

PREFIX = 'm10007'
DEBUG = False
FORCE_ALL = True  # Ponechat na True i na produkci


def _log(idx, total, path, info, hid=None):
    if hid:
        hid = '{} '.format(hid)
    else:
        hid = ''
    logger.info('{prefix} {idx}/{total} [{info}] {hid}{path}'.format(
        idx=idx+1,
        total=total,
        path=path,
        prefix=PREFIX,
        info=info,
        hid=hid,
        ))

def _fix_report_entry_ticket_type(portal, catalog):

    tickets = getattr(portal, 'tickets')

    brains = catalog(
        portal_type='ReportEntry',
        sort_on='hid',
        sort_order='reverse',
        )
    total = len(brains)

    for idx, brain in enumerate(brains):
        path = brain.getPath()
        obj = brain.getObject()

        value_re = obj.ticket_type
        ticket = getattr(tickets, obj.ticket_id, None)
        if ticket is None:
            raise Exception('Invalid ticket_id')
        value_t = ticket.ticket_type

        updated = False

        if DEBUG:
            print " "
            print "hid:", obj.hid
            print "value_re:", repr(value_re)
            print "value_t:", repr(value_t)

        if FORCE_ALL or value_re != value_t:
            obj.ticket_type = value_t
            updated = True

        if not updated:
            # _log(idx, total, path, 'skipping', hid=obj.hid)
            continue

        obj.reindexObject()

        _log(idx, total, path, 'fixing', hid=obj.hid)

def fix_report_entry_ticket_type(context):
    portal = api.portal.get()
    catalog = api.portal.get_tool('portal_catalog')
    logger.info('Fixing ReportEntry.ticket_type')
    _fix_report_entry_ticket_type(portal, catalog)
