# -*- coding: utf-8 -*-

import logging

from plone import api
from DateTime import DateTime


logger = logging.getLogger("ziskej.migration")

PREFIX = 'm10001'


def _log(idx, total, path, info):
    logger.info('{prefix} {idx}/{total} [{info}] {path}'.format(
        idx=idx+1,
        total=total,
        path=path,
        prefix=PREFIX,
        info=info))

def _report_date_catalog(catalog):
    catalog.delIndex('report_date')
    catalog.addIndex('report_date', 'DateIndex')
    # nepotřebujeme reindex, protože následně děláme změnu všech objektů
    # vč. reindex objektu
    #catalog.reindexIndex('report_date', api.portal.getRequest())

def _report_date_data(catalog):
    brains = catalog(portal_type=('TicketMVS', 'SubticketMVS', ))
    total = len(brains)
    for idx, brain in enumerate(brains):
        path = brain.getPath()
        obj = brain.getObject()
        value = obj.report_date

        # zatím není vyplněno, není co migrovat
        if not value:
            _log(idx, total, path, 'skip empty')
            continue

        # není potřeba migrovat
        if value == DateTime(value):
            _log(idx, total, path, 'skip already migrated')
            continue

        _log(idx, total, path, 'migrating')
        # samotná změna
        value += ' 12:00 GMT+1' # CET, tzn. zimní čas, nic jiného nemigrujeme
        value = DateTime(value)
        obj.report_date = value
        # vynucení reindexu (celého objektu, stačilo by report_date)
        obj.reindexObject()

def report_date_datetime(context):
    catalog = api.portal.get_tool('portal_catalog')
    _report_date_catalog(catalog)
    _report_date_data(catalog)
