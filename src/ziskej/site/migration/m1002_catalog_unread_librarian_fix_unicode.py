# -*- coding: utf-8 -*-

import logging

from plone import api
from DateTime import DateTime

from Products.ZCatalog.Catalog import CatalogError

from ziskej.site.utils import safe_utf8


logger = logging.getLogger("ziskej.migration")

PREFIX = 'm10002'


def _log(idx, total, path, info):
    logger.info('{prefix} {idx}/{total} [{info}] {path}'.format(
        idx=idx+1,
        total=total,
        path=path,
        prefix=PREFIX,
        info=info))

def _catalog(catalog):
    request = api.portal.getRequest()
    index_names = ['unread_r', 'unread_zk', 'unread_dk', 'librarian', ]
    index_type = dict(
        unread_r = 'BooleanIndex',
        unread_zk = 'BooleanIndex',
        unread_dk = 'BooleanIndex',
        librarian = 'FieldIndex',
        )
    for index_name in index_names:
        try:
            catalog.delIndex(index_name)
        except CatalogError:
            pass
        catalog.addIndex(index_name, index_type.get(index_name, 'FieldIndex'))
        # nepotřebujeme reindex, protože následně děláme změnu všech objektů
        # vč. reindex objektu
        #catalog.reindexIndex(index_name, request)

        try:
            catalog.delColumn(index_name)
        except ValueError:
            pass
        catalog.addColumn(index_name)

    # reader_lid / Reader_lid - unicode issue
    # https://reinout.vanrees.org/weblog/2008/10/14/unicodedecodeerror-in-plones-catalog.html
    try:
        catalog.delIndex('reader_lid')
    except CatalogError:
        pass
    try:
        catalog.delIndex('Reader_lid')
    except CatalogError:
        pass
    catalog.addIndex('Reader_lid', 'FieldIndex')
    try:
        catalog.delColumn('reader_lid')
    except ValueError:
        pass
    try:
        catalog.delColumn('Reader_lid')
    except ValueError:
        pass
    catalog.addColumn('Reader_lid')

def _data(catalog):
    brains = catalog(portal_type=('TicketMVS', 'SubticketMVS', ))
    total = len(brains)
    for idx, brain in enumerate(brains):
        path = brain.getPath()
        obj = brain.getObject()

        _log(idx, total, path, 'initializing')

        # samotná změna pro unread a librarian
        if obj.portal_type == 'TicketMVS':
            obj.unread_r = False
            obj.unread_zk = False
        elif obj.portal_type == 'SubticketMVS':
            obj.unread_dk = False
        obj.librarian = u''

        # reader_lid / Reader_lid - není potřeba žádná akce, stačí reindex
        obj.reader_lid = safe_utf8(obj.reader_lid)

        # reindex pro všechny změny
        obj.reindexObject()

def catalog_unread_librarian_fix_unicode(context):
    catalog = api.portal.get_tool('portal_catalog')
    _catalog(catalog)
    _data(catalog)
