# -*- coding: utf-8 -*-

import logging

from plone import api
from Products.ZCatalog.Catalog import CatalogError


logger = logging.getLogger("ziskej.migration")

PREFIX = 'm10005'


def _log(idx, total, path, info):
    logger.info('{prefix} {idx}/{total} [{info}] {path}'.format(
        idx=idx+1,
        total=total,
        path=path,
        prefix=PREFIX,
        info=info))

def _catalog(catalog):
    request = api.portal.getRequest()

    index_names = ['Was_any_zk', ]
    index_type = dict(
        Was_any_zk = 'BooleanIndex',
        )

    for index_name in index_names:
        try:
            catalog.delIndex(index_name)
            logger.info('Removing index {}'.format(index_name))
        except CatalogError:
            logger.info('No need to remove index {}'.format(index_name))
        logger.info('Adding index {}'.format(index_name))
        catalog.addIndex(index_name, index_type.get(index_name, 'FieldIndex'))
        logger.info('Reindexing index {}'.format(index_name))
        catalog.reindexIndex(index_name, request)
        logger.info('Finished set up for index {}'.format(index_name))

def catalog_was_any_zk(context):
    catalog = api.portal.get_tool('portal_catalog')
    _catalog(catalog)
