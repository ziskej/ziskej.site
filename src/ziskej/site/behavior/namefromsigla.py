# -*- coding: utf-8 -*-
from plone.app.content.interfaces import INameFromTitle
from zope.component import adapter
from zope.interface import implementer
import logging

from ziskej.site.content.base import IBase

logger = logging.getLogger(__name__)


class INameFromSigla(INameFromTitle):
    """Get the name from the sigla.

    Note that when you want this behavior, then you MUST NOT enable
    the IDublinCore, IBasic, INameFromTitle or INameFromFile behaviors
    on your type.
    """


@implementer(INameFromSigla)
@adapter(IBase)
class NameFromSigla(object):

    def __init__(self, context):
        self.context = context

    @property
    def title(self):
        logger.info('NameFromSigla.title')
        return IBase(self.context).sigla
