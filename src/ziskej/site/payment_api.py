# -*- coding: utf-8 -*-
"""Zapouzdření napojení na NTK Platebátor a NTK kreditní systém (Paysys)
prostřednictvím Získej async.
"""

import datetime
import json
import jwt
import os.path
import requests
import logging
import time
import uuid

try:
    from ziskej.site import (
        ZISKEJ_ASYNC_APP_ID,
        ZISKEJ_ASYNC_NTK_INSTANCE,
        ZISKEJ_ASYNC_BASE_URL,
        ZISKEJ_ASYNC_BASE_URL_LOCAL,
        ZISKEJ_API_ID,
        SHARED_SECRET_ZISKEJ_APP,
        USE_ASYNC_PORT_9080,
        )
    from ziskej.site.utils_lib import pprint, safe_unicode
    from DateTime import DateTime
except ImportError:
    from . import (
        ZISKEJ_ASYNC_APP_ID,
        ZISKEJ_ASYNC_NTK_INSTANCE,
        ZISKEJ_ASYNC_BASE_URL,
        ZISKEJ_ASYNC_BASE_URL_LOCAL,
        ZISKEJ_API_ID,
        SHARED_SECRET_ZISKEJ_APP,
        USE_ASYNC_PORT_9080,
        )
    from utils_lib import pprint, safe_unicode


logger = logging.getLogger("ziskej.site.payment_api")

DEBUG = False
DEBUG_RESPONSE = True
DEBUG_CACHE = True
DEBUG_USE_LOCAL_ASYNC_PLATEBATOR = True
DEBUG_PLATEBATOR_DUMMY = False
DEBUG_PLATEBATOR_FORCE = True
PLATEBATOR_DISABLED = False
LOG_PLATEBATOR = True
USE_CREDIT_INFO_CACHE = True
REQUEST_VERIFY = False

DT_30S = 30.0/(24.0*60.0*60.0)


class ZiskejAsyncException(Exception):
    pass


class CreditInfoCache(object):

    def __init__(self):
        self.data = dict()
        self.timestamp = dict()

    def has(self, name):
        return bool(name in self.data)

    def get(self, name):
        timestamp_cache = self.timestamp.get(name, None)
        if timestamp_cache is None:
            return None
        # expirace 30 s

        if DateTime() + DT_30S > timestamp_cache:
            if DEBUG or DEBUG_CACHE:
                print "CreditInfoCache expired"
            return None
        return self.data.get(name, None)

    def set(self, name, value):
        self.data[name] = value
        self.timestamp[name] = DateTime()

    def clear(self, name):
        if name in self.timestamp:
            del self.timestamp[name]
        if name in self.data:
            del self.data[name]


class ZiskejAsyncBase(object):

    def __init__(self, settings=None):
        super(ZiskejAsyncBase, self).__init__()

        # settings
        if settings is not None:
            self.settings = settings
        else:
            # default settings
            self.settings = dict(base_url = ZISKEJ_ASYNC_BASE_URL)
        self.base_url = self.settings.get('base_url', None)
        if DEBUG_USE_LOCAL_ASYNC_PLATEBATOR and ZISKEJ_ASYNC_BASE_URL_LOCAL:
            self.base_url = ZISKEJ_ASYNC_BASE_URL_LOCAL
        if self.base_url and self.base_url.endswith(u'/'):
            self.base_url = self.base_url[:-1]
        if DEBUG:
            print "ZiskejAsyncBase base_url:", self.base_url

    def _parse_result_obj(self, result_obj):
        if not result_obj.ok:
            try:
                result_dict = result_obj.json()
            except:
                result_dict = None
            if not result_dict:
                result_dict = dict()
            error_message = result_dict.get(u'error', None)
            if DEBUG or DEBUG_RESPONSE:
                print(result_obj)
                print(result_obj.status_code)
                print(result_obj.text)
                print(error_message)
            if error_message:
                self.error_message = u'{} ({})'.format(
                    error_message,
                    result_obj.status_code,
                    )
                raise ZiskejAsyncException('error_message')
            self.error_message = u'Unexpected response: {}'.format(
                result_obj.status_code,
                )
            raise ZiskejAsyncException('error_message')

        try:
            result_dict = result_obj.json()
        except json.decoder.JSONDecodeError as e:
            if str(e) == 'Expecting value: line 1 column 1 (char 0)':
                raise ZiskejAsyncException('Invalid response: Not json format.')
            raise

        return result_dict

    def _url(self, url):
        if not self.base_url:
            raise ZiskejAsyncException('Missing configuration.')
        return u'{base_url}{url}'.format(
            base_url = self.base_url,
            url = url,
            )

    def _headers(self):
        now = datetime.datetime.utcnow()
        token_dict = dict(
            iss = ZISKEJ_ASYNC_APP_ID,
            iat = now,
            exp = now + datetime.timedelta(seconds=3600),
            app = ZISKEJ_ASYNC_APP_ID,
            )
        token = jwt.encode(token_dict, SHARED_SECRET_ZISKEJ_APP, algorithm='HS256')
        token = token.decode('utf-8')

        headers = {
            'Authorization': 'bearer {}'.format(token),
            'X-Ziskej-Instance': '{}'.format(ZISKEJ_ASYNC_NTK_INSTANCE),
            }
        return headers


class ZiskejAsyncCredit(ZiskejAsyncBase):

    def credit_info_base(self, ntk_user_id):
        if not ntk_user_id:
            raise ZiskejAsyncException('credit_info() failed because of missing ntk_user_id')

        # debug
        if ntk_user_id == 'test':
            ntk_user_id = '411731'  # hamad2

        # cache
        value = None
        if USE_CREDIT_INFO_CACHE:
            value = credit_info_cache.get(ntk_user_id)
        if value is not None:
            # cache HIT
            if DEBUG or DEBUG_CACHE:
                print "cache hit:", value, "for", ntk_user_id
            return value
        # cache MISS
        if DEBUG or DEBUG_CACHE:
            print "cache miss for", ntk_user_id

        if USE_ASYNC_PORT_9080:
            url = self._url(u'/ntk/users/{}/credit'.format(ntk_user_id))
        else:
            url = self._url(u'/ntkapi/users/{}/credit'.format(ntk_user_id))
        if DEBUG:
            print "url:", url

        headers = self._headers()
        result_obj = requests.get(url, headers=headers, verify=REQUEST_VERIFY)

        result_dict = self._parse_result_obj(result_obj)

        if DEBUG:
            print "result_dict:", result_dict
        status = result_dict.get('status', None)
        if DEBUG:
            print "status:", status
        if status == u'NACK':
            message = result_dict.get('message', None)
            raise ZiskejAsyncException('credit_info({}) failed because of response NACK: {}'.format(ntk_user_id, repr(message)))
        if status != u'ACK':
            raise ZiskejAsyncException('credit_info({}) failed because of invalid response status: {}'.format(ntk_user_id, status))

        # UCET_INFO nebo UCET_ZUSTATEK
        ucet = result_dict.get('ucet', None)
        # if not ucet or not isinstance(ucet, dict):
        #     raise ZiskejAsyncException('credit_info({}) failed because of invalid response: missing ucet'.format(ntk_user_id))
        if ucet:
            zustatek = ucet.get('zustatek', None)
        else:
            zustatek = result_dict.get('zustatek', None)
        if not zustatek or not isinstance(zustatek, dict):
            raise ZiskejAsyncException('credit_info({}) failed because of invalid response: invalid or missing zustatek'.format(ntk_user_id))
        disponibilni = zustatek.get('disponibilni', None)
        if disponibilni is None:
            raise ZiskejAsyncException('credit_info({}) failed because of invalid response: invalid zustatek, missing disponibilni'.format(ntk_user_id))
        if DEBUG:
            print "disponibilni:", repr(disponibilni)
        try:
            disponibilni = int(disponibilni)
        except:
            raise ZiskejAsyncException('credit_info({}) failed because of invalid response: invalid ucet, invalid zustatek, invalid disponibilni: {}'.format(ntk_user_id, repr(disponibilni)))
        if DEBUG:
            print "disponibilni:", repr(disponibilni)

        # cache
        credit_info_cache.set(ntk_user_id, disponibilni)
        if DEBUG or DEBUG_CACHE:
            print "cache save:", disponibilni, "for", ntk_user_id

        return disponibilni

    def pay_base(self, ntk_user_id, amount, vs=None):
        self.error_message = ''

        if not ntk_user_id:
            self.error_message = u'Chybějící ntk_user_id.'
            raise ZiskejAsyncException('error_message')

        # debug
        if ntk_user_id == 'test':
            ntk_user_id = '411731'  # hamad2

        if USE_ASYNC_PORT_9080:
            url = self._url(u'/ntk/users/{}/payments'.format(ntk_user_id))
        else:
            url = self._url(u'/ntkapi/users/{}/payments'.format(ntk_user_id))
        if DEBUG:
            print "url:", url

        data = dict(amount = amount)
        if vs:
            data['vs'] = vs
        data_json = json.dumps(data, ensure_ascii=False).encode('utf8')

        headers = self._headers()
        result_obj = requests.post(url, data=data_json, headers=headers,
                                   verify=REQUEST_VERIFY)

        result_dict = self._parse_result_obj(result_obj)
        self.result_dict = result_dict

        if DEBUG:
            print "result_dict:", result_dict
        status = result_dict.get('status', None)
        if DEBUG:
            print "status:", status
        if status == u'NACK':
            message = result_dict.get('message', None)
            self.error_message = u'{}'.format(message)
            raise ZiskejAsyncException('error_message')
            #raise ZiskejAsyncException('pay({}) failed because of response NACK: {}'.format(ntk_user_id, repr(message)))
        if status != u'ACK':
            self.error_message = u'Invalid status: {}'.format(status)
            raise ZiskejAsyncException('error_message')
            #raise ZiskejAsyncException('pay({}) failed because of invalid response status: {}'.format(ntk_user_id, status))

        return


class ZiskejAsyncPlatebator(ZiskejAsyncBase):

    def platebator_create_base(self, data):
        if USE_ASYNC_PORT_9080:
            url = self._url(u'/ntk/platebator')
        else:
            url = self._url(u'/ntkapi/platebator')
        data_json = json.dumps(data, ensure_ascii=False).encode('utf8')
        if DEBUG:
            print "url:", url
            print "data:", data
            print "data_json:", data_json

        headers = self._headers()
        result_obj = requests.post(url, data=data_json, headers=headers,
                                   verify=REQUEST_VERIFY)

        result_dict = self._parse_result_obj(result_obj)
        if DEBUG:
            print "result_dict:", result_dict
        platebator_id = result_dict.get('uuid', None)
        if DEBUG:
            print "platebator_id:", platebator_id
        return platebator_id

    def platebator_check_base(self, platebator_id):
        if USE_ASYNC_PORT_9080:
            url = self._url(u'/ntk/platebator/{}'.format(platebator_id))
        else:
            url = self._url(u'/ntkapi/platebator/{}'.format(platebator_id))
        if DEBUG:
            print "url:", url

        headers = self._headers()
        result_obj = requests.get(url, headers=headers, verify=REQUEST_VERIFY)

        result_dict = self._parse_result_obj(result_obj)
        if DEBUG:
            print "result_dict:", result_dict
        paid = result_dict.get('paid', None)
        if DEBUG:
            print "paid:", paid
        return paid


credit_info_cache = CreditInfoCache()
