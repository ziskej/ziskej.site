# -*- coding: utf-8 -*-
"""Zapouzdření CPK API pro potřeby Získej app.  Část proběhne už
v ziskej.cpk_api, zbytek zde.  Tato verze je pro synchronní (blokující)
požadavky a je nyní používána pro většinu dotazů na CPK API s výjimkou dotazů
na dostupnost jednotek.
"""

from __future__ import print_function

import json
import os.path
import requests
import logging

try:
    from ziskej.site import ZISKEJ_ASYNC_BASE_URL, USE_ASYNC_PORT_9080
    from ziskej.site.utils_lib import pprint, safe_unicode
except ImportError:
    ZISKEJ_ASYNC_BASE_URL = 'https://ziskej-test.techlib.cz:9080/'
    USE_ASYNC_PORT_9080 = False
    from utils_lib import pprint, safe_unicode

# # https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
# #import urllib3
# #urllib3.disable_warnings()

# # https://stackoverflow.com/questions/27981545/suppress-insecurerequestwarning-unverified-https-request-is-being-made-in-pytho#28002687
# from requests.packages.urllib3.exceptions import InsecureRequestWarning
# requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# # https://stackoverflow.com/questions/11029717/how-do-i-disable-log-messages-from-the-requests-library#11029841
# logging.getLogger("requests").setLevel(logging.WARNING)
# #logging.getLogger("urllib3").setLevel(logging.WARNING)


logger = logging.getLogger("ziskej.site.cpk_api")

DEBUG = False
DEBUG_CASLIN = False


marc_mappings = {
    '001': 'control_number',
    '003': 'control_number_id',
    '005': 'latest_transaction_datetime',
    '006': 'characteristics',  #TODO: subfields http://www.marc21.ca/M21/COD/ZONE/BIB/BCZe-006.html
    '007': 'physical_description', #TODO: subfields http://www.marc21.ca/M21/COD/ZONE/BIB/BCZe-007.html
    '008': 'general_info', #TODO: subfields http://www.marc21.ca/M21/COD/ZONE/BIB/BCZe-008.html
    '010': 'loc_control_number', #TODO: subfields
    '013': 'patent_control_info', #TODO: subfields
    '015': 'national_bibliography_number', #subfields: a: National bibliography number
    '040': 'cataloging_source',  # a: Original cataloging agency b: Language of cataloging
    '072': 'subject_category_code',  #a: Subject category code x:  Subject category code subdivision 2: Source
    '080': 'universal_decimal_classification_number',  #a: Universal Decimal Classification number, 2: Edition identifier
    '130': 'main_entry_uniform_title',  #a: Uniform title
    '245': 'title_statement',  #a: Title b: Remainder of title
    '246': 'varying_form_title',  #a: Title proper/short title i: Display text
    '260': 'publication_distribution',  #a: Place of publication, distribution, etc. b:  Name of publisher, distributor, etc. c: Date of publication, distribution, etc.
    '300': 'physical_description',  #a: extent
    '310': 'current_publication_frequency',  #a: Current publication frequency
    '362': 'dates_of_publication',  #a: Dates of publication and/or sequential designation
    '500': 'general_note',  #a: General note
    '770': 'supplement',  #t: Title
    '996': '996',  #
}

marc_mappings_996 = {
    'b': 'barcode',
    'c': 'storage_sig',
    'h': 'free_choice_sig',
    'd': 'volume_details',
    'v': 'year_number',
    'i': 'volume_number',
    'y': 'year',
    'l': 'branch',
    'r': 'collection',
    's': 'status',
    'n': 'borrowings',
    'a': 'availability_hours',
    'q': 'hidden',
    'w': 'administrative_id',
    'u': 'z30_item_sequence',
    'j': 'admin_base_code',
    'e': 'e',  # details unknown
    't': 't',  # details unknown
}


class CpkApiException(Exception):
    pass


class CpkApi(object):
    """Connector to CPK API.  This calls Flask app which calls CPK API.  CPK API
    related validations, parsing (including MARC21 XML parsing) and changes are
    done in ziskej.cpk_api.  Here only additional validation, parsing and
    changes happens.

    This class provides base for each type of request.

    Be aware some request leads to multiple requests to CPK API at the end.
    """

    def __init__(self, settings):
        super(CpkApi, self).__init__()

        # settings
        # now only base_url is used - in the future we might to have some Flask
        # or CPK API parameters such as timeout, CPK API host and port
        # configured in settings too
        if settings is not None:
            self.settings = settings
        else:
            # default settings
            self.settings = dict(base_url = ZISKEJ_ASYNC_BASE_URL)
        self.base_url = self.settings.get('base_url', None)

        # request / response data
        self._last_request = None
        self._last_response = None
        self._last_status = None
        self._last_error_message = None
        self._last_data = None

        # real result including validation, parsing and additional changes
        self.data = None

    def _request(self, url, verify=True):
        """Calling Flask app itself including catching errors such as Flask app
        is down and including basic Flask app respond parsing to have proper CPK
        API result.
        """

        self._last_request = None
        self._last_response = None
        self._last_status = None
        self._last_error_message = None
        self._last_data = None

        # Flask app request
        self._last_request = url
        print("Calling Získej async for CPK:", url)
        result_obj = requests.get(url, verify=verify)
        self._last_response = result_obj

        # Flask app request failed at all, probably FLask app is donw etc.
        if not result_obj.ok:
            if DEBUG:
                pprint(result_obj)
            logger.error('URL: {url}'.format(url=url))
            logger.warning('Request failed')
            raise CpkApiException('Request failed')

        # Flask app response is always json - if not it is an error
        try:
            result_json = result_obj.json()
        except Exception, e:
            logger.exception('Invalid flask response - no json')
            raise CpkApiException('Request failed')

        # status OK / Error
        self._last_status = result_json.get('status', None)

        # {'status': 'Error', 'error_message': 'Lorem ipsum'}
        if result_json.get('status', None) != 'OK':
            error_message = result_json.get('error_message', u'Unknown error')
            skip_str = u'Request failed: '
            error_message = safe_unicode(error_message)
            if error_message[:len(skip_str)] == skip_str:
                error_message = error_message[len(skip_str):]
            self._last_error_message = error_message
            logger.warning(u'CPK API failed: {}'.format(error_message))
            raise CpkApiException('CPK Request failed: {}'.format(error_message))

        # {'status': 'OK', 'data': 'Lorem ipsum'}
        data = result_json.get('data', None)
        self._last_data = data
        if DEBUG:
            print('_request data')
            pprint(data)
            print(' ')

        return data

    @property
    def request_url(self):
        """Each class have to define url in Flask app which should be called."""
        raise Exception('Internal error')

    def do_request(self):
        """Make request and expect CpkApiException when something go wrong."""
        # Tento request je v aktuální konfiguraci vždy lokální.  Získej async 
        # app poslouchá jen na SSL portu.  Po obnovení certifikátů v srpnu 2020
        # přestala fungovat verifikace, je proto vypnuta.
        return self._request(self.request_url, verify=False)

    def do_request_no_exception(self):
        """Make request and catch CpkApiException - convert them to empty
        result.
        """

        try:
            return self.do_request()
        except CpkApiException, e:
            return None

    def call(self):
        """Only this method should be used outside.  Each class could redefine
        it for additional validation, parsing, data changes etc.
        """

        if self.data is None:
            self.data = self.do_request()

        return self.data



class CpkUnit(CpkApi):
    """CPK API: Document aka 1.2

    Each doecument could have multiple units.  This class encapsulate
    functionality for finding availability of one unit.

    Be aware this leads to request to CPK API which could fail by timeout or
    invalid response.  Treat this as unavailable.

    SUGGESTION This should use async version of Flask app.

    Result json example:

        {
            "id":"ABA001.NKC01002610829.NKC50002611255000010",
            "availability":"unavailable",
            "availability_note":"archivní exemplář",
            "duedate":null,
            "queue":"",
            "ext":{"opac_status":"unavailable"},
        }
    """

    def __init__(self, settings, unit_id):
        super(CpkUnit, self).__init__(settings)
        self.unit_id = unit_id

    @property
    def request_url(self):
        if USE_ASYNC_PORT_9080:
            url = '{base_url}cpkd/unit?id={unit_id}'.format(
                base_url=self.base_url,
                unit_id=self.unit_id)
        else:
            url = '{base_url}cpkapi/sync/unit?id={unit_id}'.format(
                base_url=self.base_url,
                unit_id=self.unit_id)
        return url

    def call(self):
        if self.data is None:
            self.data = self.do_request_no_exception()

        # Error
        # For example because of
        # {'error_message': 'Request failed: http status is not OK (404)', 'status': 'Error'}
        if self.data is None:
            data = dict(
                id = self.unit_id,
                availability = 'not_found',
                duedate = None,
                json = dict(error_message=self._last_error_message),
                )
            return data

        data = dict()
        data['id'] = self.data.get('id')
        if data['id'] != self.unit_id:
            logger.exception("cpk_api CpkUnit.call: data['id']={value1} != {value2} = self.unit_id".format(value1=data['id'], value2=self.unit_id))
            raise CpkApiException('Invalid CPK API response')
        data['availability'] = self.data.get('availability')
        data['duedate'] = self.data.get('duedate')
        data['json'] = self.data
        return data


class CpkDocument(CpkApi):
    """CPK API: Document aka 1.2

    Returns CPK data for one document.  It generates two CPK API calls because
    CPK is not able to return all data in one.  See ziskej.cpk_api for details.

    At the end Získej app is using only few attributes from both requests.  This
    class provides this filter - in call new data dict is build using class
    properties.

    This should be quick so async version of Flask app call is not required.
    """

    def __init__(self, settings, doc_id):
        if DEBUG:
            print("CpkDocument.__init__({})".format(doc_id))
        super(CpkDocument, self).__init__(settings)
        self.doc_id = doc_id
        self.ignore_missing_unit_ids = False

    @property
    def request_url(self):
        if USE_ASYNC_PORT_9080:
            # http://127.0.0.1:8000/cpkd/document?id=nkp.NKC01-002179378
            url = '{base_url}cpkd/document?id={doc_id}'.format(
                base_url=self.base_url,
                doc_id=self.doc_id)
        else:
            url = '{base_url}cpkapi/sync/document?id={doc_id}'.format(
                base_url=self.base_url,
                doc_id=self.doc_id)
        return url

    @property
    def doc_id_from_info(self):
        value = u''
        try:
            value = self.data['info']['id']
        except KeyError:
            pass
        except AttributeError:
            pass
        if not value:
            value = u''
            logger.warning(u'Invalid CPK API document({doc_id}): {doc_data}'.format(
                doc_id=self.doc_id,
                doc_data=self.data))
            raise CpkApiException('Invalid CPK API document result')
        return value        

    @property
    def title(self):
        value = u''
        try:
            value = self.data['info']['title']
        except KeyError:
            pass
        except AttributeError:
            pass
        if not value:
            value = u''
            logger.info('CPK API document: Missing title for document({doc_id}): {doc_data}'.format(
                doc_id=self.doc_id,
                doc_data=self.data))
        return value

    @property
    def author(self):
        value = u''
        try:
            value = self.data['info']['authors'].get('main', u'')
        except KeyError:
            pass
        except AttributeError:
            pass
        if not value:
            value = u''
        return value

    def guess_sigla_from_996t(self):
        default_value = None

        # missing sigla
        if '996' not in self.data['marc']:
            logger.info('missing_sigla guess_sigla_from_996t failed due to missing 996')
            return default_value

        # make fields list
        if not isinstance(self.data['marc']['996'], list):
            fields = [self.data['marc']['996'], ]
        else:
            fields = self.data['marc']['996']

        # Example:
        # <datafield tag="996" ind1=" " ind2=" ">
        #    <subfield code="b">331100242483</subfield>
        #    <subfield code="c">Reg N-908(437.3)/MI</subfield>
        #    <subfield code="l">PK1S</subfield>
        #    <subfield code="s">A</subfield>
        #    <subfield code="a">24</subfield>
        #    <subfield code="w">0154865_0001</subfield>
        #    <subfield code="t">VYG001.331100242483</subfield>
        # </datafield>
        # <datafield tag="996" ind1=" " ind2=" ">
        #    <subfield code="b">331100242484</subfield>
        #    <subfield code="c">Reg N-908(437.3)/MI</subfield>
        #    <subfield code="l">PK1S</subfield>
        #    <subfield code="s">A</subfield>
        #    <subfield code="a">24</subfield>
        #    <subfield code="w">0154865_0002</subfield>
        #    <subfield code="t">VYG001.331100242484</subfield>
        # </datafield>

        siglas = []
        for field in fields:
            subfs = None
            sigla = u''
            try:
                subfs = field.get('subfields', [])
            except KeyError:
                continue
            except AttributeError:
                continue
            if not subfs:
                continue
            for subf in subfs:
                if 't' in subf:
                    sigla = subf['t']
                    break
            if not sigla:
                continue
            sigla = sigla[:6].upper().strip()
            if sigla not in siglas:
                siglas.append(sigla)

        if not len(siglas):
            logger.info('missing_sigla guess_sigla_from_996t failed due to no 996 t')
            return default_value

        if len(siglas) == 1:
            return siglas[0]

        if u'ABA008' in siglas:
            return u'ABA008'

        logger.info('missing_sigla guess_sigla_from_996t failed due to too many: {}'.format(repr(siglas)))
        return default_value

    @property
    def sigla(self):
        default_value = u''

        # missing sigla
        if '910' not in self.data['marc']:
            value = self.guess_sigla_from_996t()
            if value:
                return value
            return default_value

        # make fields list
        if not isinstance(self.data['marc']['910'], list):
            fields = [self.data['marc']['910'], ]
        else:
            fields = self.data['marc']['910']

        # Example:
        # https://www.knihovny.cz/api/v1/record?id=nlk.69370&field[]=fullRecord
        # <datafield tag="910" ind1=" " ind2=" ">
        # <subfield code="a">ABA008</subfield>
        # <subfield code="b">K 38101/652</subfield>
        # <subfield code="y">x</subfield>
        # </datafield>
        # <datafield tag="910" ind1=" " ind2=" ">
        # <subfield code="a">ABE015</subfield>
        # <subfield code="b">K 107/TRS 652</subfield>
        # <subfield code="y">0</subfield>
        # </datafield>

        siglas = []
        for field in fields:
            subfs = None
            sigla = u''
            try:
                subfs = field.get('subfields', [])
            except KeyError:
                continue
            except AttributeError:
                continue
            if not subfs:
                continue
            for subf in subfs:
                if 'a' in subf:
                    sigla = subf['a']
                    break
            if not sigla:
                continue
            sigla = sigla.upper().strip()
            if sigla not in siglas:
                siglas.append(sigla)

        if not len(siglas):
            value = self.guess_sigla_from_996t()
            if value:
                return value
            return default_value

        if len(siglas) == 1:
            return siglas[0]

        if u'ABA008' in siglas:
            return u'ABA008'

        value = self.guess_sigla_from_996t()
        if value:
            return value
        return default_value

    @property
    def year_publication(self):
        value = u''
        subfs = None
        try:
            subfs = self.data['marc']['publication_distribution'].get('subfields', [])
        except KeyError:
            pass
        except AttributeError:
            pass
        if subfs:
            for subf in subfs:
                if 'c' in subf:
                    value = subf['c']
                    break
        if not value:
            value = u''
        return value

    @property
    def unit_ids(self):
        unit_ids = []
        try:
            fields = self.data['marc']['996']
            if not isinstance(fields, list):
                fields = [fields, ]
        except KeyError:
            logger.warning('Missing unit_ids aka marc 996 t due to missing 996')
            return unit_ids
        for field in fields:
            if 'subfields' not in field:
                # skip missing subfields
                continue
            subfields = field['subfields']
            for subfield in subfields:
                if 't' not in subfield:
                    continue
                unit_id = subfield['t']
                unit_ids.append(unit_id)
                break
        if not unit_ids:
            logger.warning('Missing unit_ids aka marc 996 t due to missing 996 t')
        return unit_ids

    def marc_field_subfield(self,
                            field_id,
                            subfield_id,
                            only_first = False,
                            prefer_list = False,
                            prefer_empty_string = True,
                            remove_trail = u' ,/:;'):
        """Returns list of valus in field / subfield."""

        values = []
        field_id = str(field_id)
        try:
            fields = self.data['marc'][marc_mappings.get(field_id, field_id)]
        except KeyError:
            if prefer_empty_string:
                return u''
            return None
        if not isinstance(fields, list):
            fields = [fields, ]
        for field in fields:
            if 'subfields' not in field:
                # skip missing subfields
                continue
            subfields = field['subfields']
            for subfield in subfields:
                if subfield_id not in subfield:
                    continue
                value = subfield[subfield_id]
                if not value:
                    value = u''
                if value:
                    value = value.strip()
                    if remove_trail:
                        value = value.strip(remove_trail)
                    values.append(value)
                break
            if only_first and len(values):
                break
        if not prefer_list:
            if len(values) == 0:
                if prefer_empty_string:
                    return u''
                return None
            if len(values) == 1:
                return values[0]
        return values

    def _author_upper(self, author):
        if not author:
            return u''
        idx = author.find(u',')
        if idx != -1:
            result = author[:idx].upper()
            result += author[idx:]
        else:
            result = author.upper()
        return result

    def _get_safe_citation_item(self, name, value, separator):
        citation_value = u''
        if not value:
            return citation_value
        if isinstance(value, list):
            logger.info('get_citation for {}: {} is list'.format(
                self.doc_id,
                name,
                ))
            for item in value:
                if isinstance(item, basestring):
                    citation_value += item
                    citation_value += u', '
                else:
                    logger.info('get_citation for {}: {} list item is not string'.format(
                        self.doc_id,
                        name,
                        ))
            if citation_value:
                citation_value = citation_value[:-2]
        elif isinstance(value, basestring):
            citation_value = u'{}'.format(value)
        else:
            logger.info('get_citation for {}: {} is unsupported type'.format(
                self.doc_id,
                name,
                ))
        if citation_value and separator:
            citation_value += separator
        return citation_value

    def get_citation(self):
        """Based on this alg:

        Dokument zobrazovat ve formátu:

            PŘÍJMENÍ1, Jméno1; PŘÍJMENÍ2, Jméno 2. Název knihy. Číslo vydání. Místo vydání: Vydavatelství, Rok. Počet stran. ISBN.

        Příklad:

            TOLKIEN, John Ronald Reuel. Pán prstenů: Dvě věže. 1. vyd. Praha: Mladá fronta, 1991. 320 s. ISBN 80-204-0194-6.

        CPK mapping

            Hlavní autor .. 100 a, transformace: část před první čárkou UPPER CASE
            Další autoři .. 700 a, vícenásobně, transformace: část před první čárkou každé položky UPPER CASE
            Název .. 245 a
            Číslo vydání .. 250 a
            Místo vydání .. 260 a
            Vydavatelství .. 260 b, vícenásobně
            Rok .. 260 c
            Počet stran .. 300 a
            ISBN .. 020 a, vícenásobně, vezme se jen první

        FIELDING, Joy. Potok stínů. Praha: Ikar, 2013. ISBN 978-80-249-2210-2.
        FIELDING, Joy. Potok stínů. Vyd. 1. Praha: Ikar, 2013. 302 s. 978-80-249-2210-2.

        """

        data = dict()
        data['main_author'] = self.marc_field_subfield('100', 'a')
        data['other_authors'] = self.marc_field_subfield('700', 'a', prefer_list=True)
        data['title'] = self.marc_field_subfield('245', 'a')
        data['issue_number'] = self.marc_field_subfield('250', 'a')
        data['issue_place'] = self.marc_field_subfield('260', 'a')
        data['issuer'] = self.marc_field_subfield('260', 'b')
        data['issue_year'] = self.marc_field_subfield('260', 'c')
        data['page_number'] = self.marc_field_subfield('300', 'a')
        data['isbn'] = self.marc_field_subfield('020', 'a', only_first=True)

        citation = u''

        citation_value = self._get_safe_citation_item(
            'main_author', data['main_author'], None)
        if citation_value:
            citation += self._author_upper(citation_value)

        for item in data['other_authors']:
            citation += u'; '
            citation += self._author_upper(item)
        citation += u'. '

        citation += self._get_safe_citation_item(
            'title', data['title'], u'. ')

        citation_value = self._get_safe_citation_item(
            'issue_number', data['issue_number'], None)
        if citation_value:
            citation += citation_value
            if not citation_value.endswith('.'):
                citation += u'. '
            else:
                citation += u' '

        citation += self._get_safe_citation_item(
            'issue_place', data['issue_place'], u': ')

        citation += self._get_safe_citation_item(
            'issuer', data['issuer'], u', ')

        citation += self._get_safe_citation_item(
            'issue_year', data['issue_year'], u'. ')

        citation_value = self._get_safe_citation_item(
            'page_number', data['page_number'], None)
        if citation_value:
            citation += citation_value
            if not citation_value.endswith('.'):
                citation += u'. '
            else:
                citation += u' '

        citation_value = self._get_safe_citation_item(
            'isbn', data['isbn'], u'. ')
        if citation_value:
            citation += u'ISBN '
            citation += citation_value

        citation = citation.strip()

        # https://www.knihovny.cz/Record/bmc.784961
        if DEBUG:
            print(' ')
            print('marc')
            pprint(self.data['marc'])
            print(' ')
            print('citation')
            print(citation)
            print(' ')

        return citation, data

    def parse_caslin(self):
        data = self.marc_field_subfield('996', 'e', prefer_list=True)
        if DEBUG or DEBUG_CASLIN:
            print("parse_caslin:", data)
        if not isinstance(data, list):
            logger.warning('Invalid caslin_info not list')
            data = []
        result = []
        for sigla in data:
            if not sigla or not sigla.strip():
                continue
            result.append(sigla.strip().upper())
        return result

    def call(self):
        if DEBUG:
            print("CpkDocument.call() for {}".format(self.doc_id))
        BLACKLIST = ['kram-', ]
        for x in BLACKLIST:
            if self.doc_id.startswith(x):
                raise CpkApiException('Blacklisted document id.')

        if self.data is None:
            if DEBUG:
                print("do_request --------")
            self.data = self.do_request()
            if DEBUG:
                print("do_request -------- END")

        # result doc_id
        doc_id = self.doc_id_from_info
        if self.doc_id != doc_id:
            logger.warning(u'Invalid CPK API document({doc_id}): {doc_data}'.format(
                doc_id=self.doc_id,
                doc_data=self.data))
            raise CpkApiException('Invalid CPK API document result')

        # title
        title = self.title

        # sigla
        sigla = self.sigla
        if sigla is None:
            if DEBUG:
                print("no sigla END")
            return None

        # citation (ticket.doc_fullname)
        citation, citation_dict = self.get_citation()

        # if DEBUG:
        #     print(' ')
        #     print('raw data')
        #     pprint(self.data)
        #     print(' ')
        #     print('citation')
        #     print(citation)
        #     print(' ')
        #     print('citation_dict')
        #     pprint(citation_dict)
        #     print(' ')

        # Souborný katalog, caslin
        if self.doc_id.startswith('caslin.'):
            unit_ids = []
            availability = dict()  # unit_id as key
            caslin_info = dict(
                sigla_ids = self.parse_caslin(),
                )

        # Standardní knihovna
        else:

            # units
            unit_ids = self.unit_ids
            if not self.ignore_missing_unit_ids and not len(unit_ids):
                if DEBUG:
                    print("no unit_ids END")
                return None

            # availability
            # the question is when to find out about availability - it is needed
            # for DK order not sooner
            availability = dict()  # unit_id as key

            caslin_info = None

        data = dict(
            id = doc_id,
            title = title,
            sigla = sigla,
            author=self.author,
            year_publication=self.year_publication,
            citation = citation,
            citation_dict = citation_dict,
            unit_ids = unit_ids,
            availability = availability,
            #raw_data = self.data,
            caslin_info = caslin_info,
            )
        if DEBUG or DEBUG_CASLIN:
            print("CpkDocument.call() for {} result:".format(doc_id))
            pprint(data)
        return data


class CpkDocumentsOverview(CpkApi):

    def __init__(self, settings, doc_ids):
        super(CpkDocumentsOverview, self).__init__(settings)
        self.doc_ids = doc_ids

    @property
    def request_url(self):
        if USE_ASYNC_PORT_9080:
            # http://127.0.0.1:8000/cpkd/documents_overview?ids=nkp.NKC01-002179378,mzvkol.SVK01-000887781k
            doc_ids_str = ','.join(self.doc_ids)
            url = '{base_url}cpkd/documents_overview?ids={doc_ids_str}'.format(
                base_url=self.base_url,
                doc_ids_str=doc_ids_str)
        else:
            doc_ids_str = ','.join(self.doc_ids)
            url = '{base_url}cpkapi/sync/documents_overview?ids={doc_ids_str}'.format(
                base_url=self.base_url,
                doc_ids_str=doc_ids_str)
        return url

    def _guess_siglas(self, data):
        for doc_id in data:
            doc_data = data[doc_id]
            if doc_data['sigla']:
                continue
            siglas = []
            for unit_id in doc_data['unit_ids']:
                sigla = unit_id[:6].upper()
                if sigla not in siglas:
                    siglas.append(sigla)
            if len(siglas) == 1:
                doc_data['sigla'] = siglas[0]
                continue
            if u'ABA008' in siglas:
                doc_data['sigla'] = u'ABA008'
                continue

            if len(siglas) == 0:
                logger.warning('Cannot guess sigla from overview, no sigla in unit_ids for doc_id {}'.format(doc_id))
            else:
                logger.warning('Cannot guess sigla from overview, too many siglas in unit_ids for doc_id {}: {}'.format(doc_id, repr(siglas)))
        return data

    def call(self):
        data = self.do_request()
        if DEBUG:
            print(" ")
            print("--------------------------------")
            print("CpkDocumentsOverview.call")
            print("self.doc_ids", self.doc_ids)
            print("result:")
            pprint(data)
        data = self._guess_siglas(data)
        if DEBUG:
            print(" ")
            print("after _guess_siglas:")
            pprint(data)
            print("--------------------------------")
            print(" ")
        return data


# class CpkLibraries(CpkApi):
#     """CPK API: Libraries aka 2.1

#     It fails on timeout.  Do not use it.
#     """

#     def __init__(self, settings, portal=None, request=None, cpkapi_dir=None, createContent=None):
#         super(CpkLibraries, self).__init__(settings)
#         self.portal = portal
#         self.request = request
#         self.cpkapi_dir = cpkapi_dir
#         self.data = None
#         self.createContent = createContent
#         self.limit_on_page = 0  # means maximum what is 50
#         self.starting_page = 1  # first page has index 1 in CPK
#         #self.page_limit = 0  # means maximum what is 1000
#         self.page_limit = 1

#     def _cpk(self):
#         # cpkd is sync (blocking) version
#         url = '{base_url}cpkd/libraries?limit_on_page={limit_on_page}&starting_page={starting_page}&page_limit={page_limit}'.format(
#             base_url = self.base_url,
#             limit_on_page = self.limit_on_page,
#             starting_page = self.starting_page,
#             page_limit = self.page_limit,
#             )
#         result_obj = self._request(url)
#         self.data = result_obj.json()
#         if DEBUG:
#             print("CpkLibraries result_obj.json(): ")
#             pprint(self.data)

#     def call(self):
#         if self.data is None:
#             self._cpk()
#             if self.data is None:
#                 raise Exception('Invalid state')

#         # if self.data.get('status', None) == 'Error':
#         #     # {'error_message': 'Request failed: http status is not OK (404)', 'status': 'Error'}
#         #     data = []
#         #     return data

#         return self.data

#     def get_all_libraries(self):
#         logger.warning('It will fail on timeout.  Please use async version - NIY.')
#         self.page_limit = 0  # means maximum what is 1000
#         self.data = None
#         return self.call()


class CpkLibrary(CpkApi):
    """CPK API: Library aka 2.2

    See library_helpers.
    """

    def __init__(self, settings, portal=None, request=None, cpkapi_dir=None, createContent=None, library_id=None):
        super(CpkLibrary, self).__init__(settings)
        self.portal = portal
        self.request = request
        self.cpkapi_dir = cpkapi_dir
        self.data = None
        self.createContent = createContent
        self.library_id = library_id

    @property
    def request_url(self):
        if not self.library_id:
            raise CpkApiException('No library_id.')
        if USE_ASYNC_PORT_9080:
            url = '{base_url}cpkd/library?id={library_id}'.format(
                base_url=self.base_url,
                library_id=self.library_id)
        else:
            url = '{base_url}cpkapi/sync/library?id={library_id}'.format(
                base_url=self.base_url,
                library_id=self.library_id)
        return url

    @property
    def sigla(self):
        if self.data is None:
            return None
        sigla = self.data.get('SGL', None)
        if sigla is None:
            return None
        return sigla.upper()
