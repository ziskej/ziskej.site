# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): Branch.
"""

from zope import schema
from zope.interface import Interface, implements
from plone.dexterity.content import Container
from plone.directives import form

from ziskej.site import _
from ziskej.site.utils import make_searchable_text
from ziskej.site.content.base import IBase, Base


class IBranch(IBase):

    # Adresa knihovny
    address = schema.TextLine(
        title=_(u'Adresa'),
        required=False,
    )


class Branch(Base):
    implements(IBranch)

    def SearchableText(self):
        return make_searchable_text(self, 'title', 'description', )

    def get_new_id(self):
        print "------------------------"
        aaa
        #print self.absolute_url()
        try:
            print self.__parent__
        except Exception, e:
            print str(e)
            raise
        print "------------------------E"
        return u''
