# -*- coding: utf-8 -*-
"""Základní třídy a interfaces pro typy obsahu (CT) v Získej app.
"""

from zope import schema
from zope.interface import Interface, implements
from plone.dexterity.content import Container
from plone.directives import form

from ziskej.site import _
from ziskej.site.utils_lib import pprint, safe_utf8, safe_unicode


class IBase(Interface):
    pass


class Base(Container):
    implements(IBase)

    def SearchableText(self):
        return u""

    def canSetDefaultPage(self):
        return False

    def exclude_from_nav(self):
        return False

    def Title(self):
        """We need reasonable robust default"""
        try:
            result = self.title
        except AttributeError, e:
            result = self.getId()
        return result

    def Description(self):
        """We need reasonable robust default"""
        try:
            result = self.description
        except AttributeError, e:
            result = u''
        return result

    def get_new_id(self):
        return u''


class IBaseCT(IBase):
    pass


class BaseCT(Base):
    implements(IBaseCT)


class ITicketBase(IBase):
    pass


class TicketBase(Base):
    pass


class IPMItemBase(schema.interfaces.IDict):

    date = schema.Datetime(
        title=_(u'Datum a čas'),
        required=True,
    )

    sender = schema.TextLine(
        title=_(u'Odesílatel'),
        required=True,
    )

    text = schema.Text(
        title=_(u'Zpráva'),
        required=True,
    )


class PMItemBase(schema.Object):
    implements(IPMItemBase)


class IBaseUser(IBase):

    form.mode(passwd_reset_token='display')
    #write_permission(passwd_reset_token='cmf.ManagePortal')
    passwd_reset_token = schema.TextLine(
        title=_(u'Token pro reset hesla'),
        required=False,
        missing_value=None,
    )


class BaseUser(Base):
    implements(IBaseCT)

    @property
    def fullname(self):
        """We have first_name and last_name"""
        value = u'{} {}'.format(safe_unicode(self.first_name), safe_unicode(self.last_name)).strip()
        return value

    def Title(self):
        return u'{username} ({fullname})'.format(username=self.username, fullname=self.fullname)
