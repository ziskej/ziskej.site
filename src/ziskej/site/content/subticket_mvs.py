# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): SubticketMVS.

Založený na: Subticket.
"""

from plone import api
#from zope import schema
from zope.interface import implements
#from plone.directives import form

from ziskej.site import _
#from ziskej.site.utils import make_searchable_text
from ziskej.site.content.subticket_base import ISubticket, Subticket


class ISubticketMVS(ISubticket):
    """Požadavek, přebírá atributy z ISubticket."""
    pass


class SubticketMVS(Subticket):
    """Požadavek, více viz Subticket."""
    implements(ISubticketMVS)

    # Workflow constraints

    @property
    def can_accept(self):
        if self.wf_review_state not in ('queued', 'conditionally_accepted', ):
            return False
        return True

    @property
    def can_conditionally_accept(self):
        if self.wf_review_state not in ('queued', ):
            return False
        return True

    @property
    def can_send(self):
        if self.wf_review_state not in ('accepted', ):
            return False
        if self.is_mvs:
            if self.cena is None or self.cena == u'':
                return False
        if self.is_mrs:
            # FIXME-MRS
            pass
        if self.is_edd:
            if not self.pages_number_dk:  # 0 nemůže nastat
                return False
            if not self.Is_pdf_uploaded():
                return False
            if not self.Is_Dilia_data():
                return False
        if self.is_small_cancel_asked:
            return False
        return True

    @property
    def can_send_back(self):
        if not self.is_mvs:
            return False
        if self.wf_review_state not in ('sent', ):
            return False
        if self.is_small_complaint_undecided:
            return False
        ticket = self.ticket
        if ticket.subticket_id == self.getId():
            if ticket.wf_review_state not in ('closed', ) and not ticket.is_reader_return:
                return False
        return True

    @property
    def can_close(self):
        if self.wf_review_state not in ('sent', 'sent_back', ):
            return False
        if self.is_small_complaint_undecided:
            return False
        return True

    @property
    def can_cancel(self):
        if self.wf_review_state not in ('queued', 'conditionally_accepted', 'accepted', 'sent', ):
            return False
        return True

    @property
    def can_refuse(self):
        if self.wf_review_state not in ('queued', 'conditionally_accepted', 'accepted', 'sent', ):
            return False
        return True
