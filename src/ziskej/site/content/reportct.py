# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): ReportCT.

Právě jeden a pevné umístění /reports.
"""

from zope.interface import implements

from ziskej.site.content.base import IBaseCT, BaseCT


class IReportCT(IBaseCT):
    pass


class ReportCT(BaseCT):
    implements(IReportCT)
