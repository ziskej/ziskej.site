# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): LibraryCT.

Kontejner pro knihovny (Library), právě jeden a pevné umístění /libraries.
"""

from zope.interface import implements

from ziskej.site.content.base import IBaseCT, BaseCT


class ILibraryCT(IBaseCT):
    pass


class LibraryCT(BaseCT):
    implements(ILibraryCT)
