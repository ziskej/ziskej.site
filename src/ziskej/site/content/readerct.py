# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): ReaderCT.

Kontejner pro čtenáře (Reader), právě jeden a pevné umístění /readers.
"""

from zope.interface import implements

from ziskej.site.content.base import IBaseCT, BaseCT


class IReaderCT(IBaseCT):
    pass


class ReaderCT(BaseCT):
    implements(IReaderCT)
