# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): Subticket.

Společný základ pro požadavky různých typů.
"""

from __future__ import print_function

import json
import logging
import datetime

from DateTime import DateTime
from plone.autoform.directives import read_permission, write_permission
from plone.dexterity.content import Container
from plone.directives import form
from plone import api
from zope import schema
from zope.interface import Interface, implements

from ziskej.site import _, ZISKEJ_VAR_EDD
from ziskej.site.utils import (
    datetime_add_wd,
    datetime_add_d,
    edd_add_file,
    edd_remove_file,
    format_date,
    format_historylog_entry,
    make_searchable_text,
    pprint,
    safe_unicode,
    safe_utf8,
    sanitize_input_text,
    )
from ziskej.site.ticket_helpers import (
    TICKET_HID_MIN,
    get_subticket_wf_label,
    )
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger('ziskej')

DEBUG = False


class ISubticket(ITicketBase):
    """Base class pro všechny typy Ticket*

    Atributy:

    Base attrs
    ----------

    id .. náhodné kontextově jedinečné id
    fullname .. (generated)

    Human ID
    --------

    shortname .. u'MV100001-001' - generated, catalog-metadata
    hid .. 1 - catalog-index, catalog-metadata

    Refs
    ----

    library_dk .. library sigla

    Document
    --------

    Aktuálně poptávaný / dodaváný dokument (titul) by měl být vždy právě jeden.
    Nicméně může se v čase měnit.  Aktuální subticket a ticket by měly být vždy
    v souladu.  Nicméně dokument v ticketu bude nastavený i před vytvořením
    prvního subticketu a subticket zůstane nastavený i ve chvíli kdy pžřestane
    být aktuálním subticketem.

    doc_data pak obsahuje kompletní data o dokumentu ve formátu json.

    doc_id .. 'nkp.NKC01-002179378'
    doc_title .. 'Zaklínač. I., Poslední přání'
    doc_fullname .. 'Zaklínač. I., Poslední přání / Vojtěch Jarník, 1897-1970, 1984'
    doc_data .. (json)

    Audit
    -----

    historylog

    """

    # --------------------------------------------------------------------------
    # identifikace ticketu

    # ticket_id umožní vrátit ticket obj bez katalogu
    form.mode(ticket_id='display')
    write_permission(ticket_id='cmf.ManagePortal')
    ticket_id = schema.ASCIILine(
        title=_(u'ID požadavku'),
        description=_(u'ID'),
        required=True,
    )

    # ticket hid, subticket nemá vlastní hid (to je změna z 2017-10)
    form.mode(hid='display')
    write_permission(hid='cmf.ManagePortal')
    hid = schema.Int(
        title=_(u'HID'),
        description=_(u'Human ID'),
        required=True,
        min=TICKET_HID_MIN,
    )

    # # MVA = MVS ŽK, MVP = MVS DK
    # form.mode(hid_prefix='display')
    # write_permission(hid_prefix='cmf.ManagePortal')
    # hid_prefix = schema.ASCIILine(
    #     title=_(u'HID prefix'),
    #     description=_(u'Human ID'),
    #     required=False,
    #     missing_value=u"",
    # )

    # fullhid je 'MVP100001-ABA013' resp. 'MVP100001-ABA013-2' atp.
    form.mode(fullhid_sigla='display')
    write_permission(fullhid_sigla='cmf.ManagePortal')
    fullhid_sigla = schema.ASCIILine(
        title=_(u'HID SIGLA'),
        required=False,
        missing_value=u"",
    )

    # fullhid je 'MVP100001-ABA013' resp. 'MVP100001-ABA013-2' atp.
    form.mode(fullhid_nr='display')
    write_permission(fullhid_nr='cmf.ManagePortal')
    fullhid_nr = schema.Int(
        title=_(u'HID pořadí'),
        description=_(u'Pořadí podpožadavku stejné knihovně pokud jich je více'),
        required=True,
        default=0,
    )

    # sigla ŽK
    form.mode(library_zk='display')
    write_permission(library_zk='cmf.ManagePortal')
    library_zk = schema.ASCIILine(
        title=_(u'Žádající knihovna'),
        required=False,
        missing_value=u"",
    )

    # sigla DK
    form.mode(library_dk='display')
    write_permission(library_dk='cmf.ManagePortal')
    library_dk = schema.ASCIILine(
        title=_(u'Dožádaná knihovna'),
        required=False,
        missing_value=u"",
    )

    # název ŽK
    form.mode(library_zk_title='display')
    write_permission(library_zk_title='cmf.ManagePortal')
    library_zk_title = schema.TextLine(
        title=_(u'Název žádající knihovny'),
        required=False,
        missing_value=u"",
    )

    # název DK
    form.mode(library_dk_title='display')
    write_permission(library_dk_title='cmf.ManagePortal')
    library_dk_title = schema.TextLine(
        title=_(u'Název dožádané knihovny'),
        required=False,
        missing_value=u"",
    )

    # Knihovník zpracovávající požadavek
    form.mode(librarian='display')
    write_permission(librarian='cmf.ManagePortal')
    librarian = schema.TextLine(
        title=_(u'Knihovník'),
        required=False,
        missing_value=u"",
    )

    # --------------------------------------------------------------------------
    # Dates as TextLine with date as ISO
    # see TicketCoreView.setup_timeline()

    form.mode(assigned_date='display')
    write_permission(assigned_date='cmf.ManagePortal')
    assigned_date = schema.TextLine(
        title=_(u'Datum přidělení'),
        description=_(u'Den, kdy automaticky (load balancing alg) nebo manuálně přiřazeno dožádané knihovně.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(decide_date='display')
    write_permission(decide_date='cmf.ManagePortal')
    decide_date = schema.TextLine(
        title=_(u'Termín rozhodnutí'),
        description=_(u'Den, do kdy je vyžadováno rozhodnutí o přijetí, nastavuje systém automaticky na základě assigned_date a konfigurace systému.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(decided_date='display')
    write_permission(decided_date='cmf.ManagePortal')
    decided_date = schema.TextLine(
        title=_(u'Datum rozhodnutí'),
        description=_(u'Den, kdy dožádaná knihovna (DK) přijala požadavek nebo jinak rozhodla.  Nevyplňuje se, pokud k ukončení zpracovávání nedošlo akcí DK.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(cond_accepted_date='display')
    write_permission(cond_accepted_date='cmf.ManagePortal')
    cond_accepted_date = schema.TextLine(
        title=_(u'Datum podmínečného přijetí'),
        description=_(u'Den, kdy dožádaná knihovna podmínečně přijala požadavek.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(accepted_date='display')
    write_permission(accepted_date='cmf.ManagePortal')
    accepted_date = schema.TextLine(
        title=_(u'Datum přijetí'),
        description=_(u'Den, kdy dožádaná knihovna přijala požadavek.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(refused_date='display')
    write_permission(refused_date='cmf.ManagePortal')
    refused_date = schema.TextLine(
        title=_(u'Datum odmítnutí'),
        description=_(u'Den, kdy dožádaná knihovna odmítla požadavek resp. kdy žádající knihovna odmítla podmínečné přijetí.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(sent_date='display')
    write_permission(sent_date='cmf.ManagePortal')
    sent_date = schema.TextLine(
        title=_(u'Datum odeslání'),
        description=_(u'Den, kdy dožádaná knihovna odeslala.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(back_date='display')
    write_permission(back_date='cmf.ManagePortal')
    back_date = schema.TextLine(
        title=_(u'Termín vrácení do DK'),
        description=_(u'Den, do kterého je potřeba vrátit do dožádané knihovny (DK), tzn. kdy by měl být dokument fyzicky zpět v DK, nastavuje DK, notifikace dostává žádající knihovna.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(sent_back_date='display')
    write_permission(sent_back_date='cmf.ManagePortal')
    sent_back_date = schema.TextLine(
        title=_(u'Datum odeslání zpět'),
        description=_(u'Den odeslání zpět do dožádané knihovny.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(closed_date='display')
    write_permission(closed_date='cmf.ManagePortal')
    closed_date = schema.TextLine(
        title=_(u'Datum uzavření'),
        description=_(u'Den uzavření podpožadavku.'),
        required=False,
        missing_value=None,
        default=None,
    )

    # Date + time as zope DateTime

    # Datum pro reporty
    form.mode(report_date='display')
    write_permission(report_date='cmf.ManagePortal')
    report_date = schema.Datetime(
        title=_(u'Datum pro reporty'),
        required=False,
        missing_value=None,
    )

    # Datum, kdy se naposledy změnilo něco významného
    form.mode(modified_date='display')
    write_permission(modified_date='cmf.ManagePortal')
    modified_date = schema.Datetime(
        title=_(u'Poslední významná změna'),
        required=False,
        missing_value=None,
    )

    # --------------------------------------------------------------------------
    # obsahové atributy

    # skutečná cena požadavku - stanovená DK a nepřímo schválená ŽK
    form.mode(cena='display')
    write_permission(cena='cmf.ManagePortal')
    cena = schema.Int(
        title=_(u'Cena'),
        description=_(u'Cena, za kterou dožádaná knihovna poskytne MVS.'),
        required=False,
        min=0,
        max=1000,
        missing_value=None,
    )

    # skutečný počet stran - stanovený DK
    form.mode(pages_number_dk='display')
    write_permission(pages_number_dk='cmf.ManagePortal')
    pages_number_dk = schema.Int(
        title=_(u'Skutečný počet stran'),
        description=_(u'Skutečný naskenovaný počet stran.'),
        required=False,
        min=0,
        max=1000,  # FIXME parametrizovat
        missing_value=None,
    )

    form.mode(pdf_file_id='display')
    write_permission(pdf_file_id='cmf.ManagePortal')
    pdf_file_id = schema.TextLine(
        title=_(u'PDF soubor ID'),
        description=_(u'PDF soubor ID.'),
        required=False,
        missing_value=u"",
    )

    form.mode(pdf_file_filename='display')
    write_permission(pdf_file_filename='cmf.ManagePortal')
    pdf_file_filename = schema.TextLine(
        title=_(u'PDF soubor filename'),
        description=_(u'PDF soubor původní název souboru.'),
        required=False,
        missing_value=u"",
    )

    # Datum expirace PDF
    form.mode(download_expiration_date='display')
    write_permission(download_expiration_date='cmf.ManagePortal')
    download_expiration_date = schema.Datetime(
        title=_(u'Datum expirace PDF'),
        description=_(u'Datum a čas expirace uložení PDF.'),
        required=False,
        missing_value=None,
    )

    # Datum posledního stažení čtenářem
    form.mode(downloaded_date='display')
    write_permission(downloaded_date='cmf.ManagePortal')
    downloaded_date = schema.Datetime(
        title=_(u'Datum posledního stažení čtenářem'),
        description=_(u'Datum a čas posledního stažení čtenářem, přesněji datum zahájení posledního pokusu o stažení, nezávisle na tom, zda byl úspěšný.'),
        required=False,
        missing_value=None,
    )

    # Počet stažení
    form.mode(downloaded_number='display')
    write_permission(downloaded_number='cmf.ManagePortal')
    downloaded_number = schema.Int(
        title=_(u'Počet stažení'),
        description=_(u'Počet zahájení stahování PDF čtenářem nezávisle na tom, zda bylo úspěšné nebo ne.'),
        required=False,
        min=0,
        max=1000,  # FIXME parametrizovat
        missing_value=None,
    )

    # --------------------------------------------------------------------------
    # log hlavních změn objektu

    # audit log hlavních změn objektu pro operátora
    form.mode(historylog='display')
    write_permission(historylog='cmf.ManagePortal')
    historylog = schema.Text(
        title=_(u'History log'),
        description=_(u'Toto je zobrazováno pouze servisnímu středisku.'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    # audit log hlavních změn objektu pro DK
    form.mode(historylog_dk='display')
    write_permission(historylog_dk='cmf.ManagePortal')
    historylog_dk = schema.Text(
        title=_(u'History log DK'),
        description=_(u'Toto je zobrazováno pouze DK.'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    # --------------------------------------------------------------------------
    # Source for doc

    # nkp.NKC01-002179378
    form.mode(doc_id='display')
    write_permission(doc_id='cmf.ManagePortal')
    doc_id = schema.TextLine(
        title=_(u'Titul ID'),
        description=_(u'Titul CPK ID, např. nkp.NKC01-002179378'),
        required=False,
        missing_value=u"",
    )

    # Zaklínač. I., Poslední přání
    form.mode(doc_title='display')
    write_permission(doc_title='cmf.ManagePortal')
    doc_title = schema.TextLine(
        title=_(u'Název titulu'),
        required=False,
        missing_value=u"",
    )

    # Zaklínač. I., Poslední přání / Vojtěch Jarník, 1897-1970, 1984'
    form.mode(doc_fullname='display')
    write_permission(doc_fullname='cmf.ManagePortal')
    doc_fullname = schema.TextLine(
        title=_(u'Titul / Autor, Rok vydání'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_data='display')
    write_permission(doc_data='cmf.ManagePortal')
    doc_data = schema.Text(
        title=_(u'Dokument data'),
        description=_(u'Toto je zobrazováno pouze administrátorům a je určeno pro ladění.'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    form.mode(doc_sent='display')
    write_permission(doc_sent='cmf.ManagePortal')
    doc_sent = schema.TextLine(
        title=_(u'Poslat nebo vyzvednout'),
        description=_(u'Poslat poštou nebo vyzvednout osobně.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_dk_id='display')
    write_permission(doc_dk_id='cmf.ManagePortal')
    doc_dk_id = schema.TextLine(
        title=_(u'ID dokumentu v AKS'),
        description=_(u'ID dokumentu v AKS dožádané knihovny.'),
        required=False,
        missing_value=u"",
    )

    form.mode(rfid='display')
    write_permission(rfid='cmf.ManagePortal')
    rfid = schema.TextLine(
        title=_(u'RFID'),
        description=_(u'RFID jednotky, exempláře.'),
        required=False,
        missing_value=u"",
    )

    form.mode(barcode='display')
    write_permission(barcode='cmf.ManagePortal')
    barcode = schema.TextLine(
        title=_(u'Čárový kód'),
        description=_(u'Čárový kód jednotky, exempláře.'),
        required=False,
        missing_value=u"",
    )

    # --------------------------------------------------------------------------
    # pm (personal messaging) data

    # sender - obvyklé hodnoty jsou '', 'library_zk', 'library_dk'
    form.mode(pm='display')
    write_permission(pm='cmf.ManagePortal')
    pm = schema.List(
        title=_(u'Diskuze ŽK a DK'),
        required=False,
        missing_value=[],
        value_type=schema.Text(),
        #value_type=IPMItemBase,  # {date, sender, text}
        #value_type=schema.Object(IPMItemBase),  # {date, sender, text}
    )

    # pm: datum a čas poslední odpovědi
    form.mode(pm_date='display')
    write_permission(pm_date='cmf.ManagePortal')
    pm_date = schema.Datetime(
        title=_(u'Datum a čas poslední zprávy'),
        required=False,
        missing_value=None,
    )

    # pm: kdo má nepřečtenou zprávu - obvyklé hodnoty jsou '', 'library_zk', 'library_dk'
    form.mode(pm_unread='display')
    write_permission(pm_unread='cmf.ManagePortal')
    pm_unread = schema.TextLine(
        title=_(u'Nepřečtená zpráva - příjemce'),
        required=False,
        missing_value=u"",
    )

    # DK nepřečtený požadavek
    form.mode(unread_dk='display')
    write_permission(unread_dk='cmf.ManagePortal')
    unread_dk = schema.Bool(
        title=_(u'Nepřečtený požadavek - DK'),
        required=False,
        missing_value=False,
    )

    # Operator nepřečtený požadavek
    form.mode(unread_op='display')
    write_permission(unread_op='cmf.ManagePortal')
    unread_op = schema.Bool(
        title=_(u'Nepřečtený požadavek - Operátor'),
        required=False,
        missing_value=False,
        default=False,
    )

    # --------------------------------------------------------------------------
    # lhůty - další akce název

    # lhůty - další akce název
    form.mode(next_action_title='display')
    write_permission(next_action_title='cmf.ManagePortal')
    next_action_title = schema.TextLine(
        title=_(u'Další akce'),
        required=False,
        missing_value=u"",
    )

    # lhůty - další akce termín
    form.mode(next_action_date='display')
    write_permission(next_action_date='cmf.ManagePortal')
    next_action_date = schema.Datetime(
        title=_(u'Termín další akce'),
        required=False,
        missing_value=None,
    )

    # lhůty - další akce poslední mailová notifikace
    form.mode(next_action_last_notification='display')
    write_permission(next_action_last_notification='cmf.ManagePortal')
    next_action_last_notification = schema.Datetime(
        title=_(u'Termín poslední notifikace'),
        required=False,
        missing_value=None,
    )

    # --------------------------------------------------------------------------
    # Storno

    form.mode(cancellation_small_ask_date='display')
    write_permission(cancellation_small_ask_date='cmf.ManagePortal')
    cancellation_small_ask_date = schema.TextLine(
        title=_(u'Datum požádání o malé storno'),
        description=_(u'Den, kdy knihovník žádající knihovny požádal o malé storno.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(cancellation_small_answer_date='display')
    write_permission(cancellation_small_answer_date='cmf.ManagePortal')
    cancellation_small_answer_date = schema.TextLine(
        title=_(u'Datum odmítnutí žádosti o malé storno'),
        description=_(u'Den, kdy knihovník dožádané knihovny odmítl žádost o malé storno.'),
        required=False,
        missing_value=None,
        default=None,
    )

    # --------------------------------------------------------------------------
    # Malá reklamace

    # Datum vytvoření malé reklamace
    # Přijatá měkká reklamace nechává nastaveno i po vyřízení, nemělo by tedy
    # zastavovat běžný proces.
    form.mode(complaint_date='display')
    write_permission(complaint_date='cmf.ManagePortal')
    complaint_date = schema.TextLine(
        title=_(u'Reklamace - datum a čas'),
        required=False,
        missing_value=None,
        default=None,
    )

    # Datum přijetí měkké malé reklamace (nové dodání), resp. přijetí námitky
    form.mode(complaint_soft_date='display')
    write_permission(complaint_soft_date='cmf.ManagePortal')
    complaint_soft_date = schema.TextLine(
        title=_(u'Reklamace, jen nové dodání - datum přijetí'),
        required=False,
        missing_value=None,
        default=None,
    )

    # Datum nového odeslání v rámci měkké malé reklamace (nové dodání)
    form.mode(complaint_soft_resent_date='display')
    write_permission(complaint_soft_resent_date='cmf.ManagePortal')
    complaint_soft_resent_date = schema.TextLine(
        title=_(u'Reklamace, jen nové dodání - datum nového dodání'),
        required=False,
        missing_value=None,
        default=None,
    )

    # Text z číselníku možných reklamací a nově i volný text
    form.mode(complaint_text='display')
    write_permission(complaint_text='cmf.ManagePortal')
    complaint_text = schema.TextLine(
        title=_(u'Reklamace - text'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # Pro většinu reklamací prázdné, pro EDD indikátor, zda čtenář přímo nebo
    # knihovník ŽK
    # reader | zk (resp. None)
    form.mode(complaint_who='display')
    write_permission(complaint_who='cmf.ManagePortal')
    complaint_who = schema.TextLine(
        title=_(u'Reklamace - kým'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # new | accepted | declined | objected | objection_accepted | objection_declined | soft | soft_ready | soft_done
    # Stav soft znamená, že byla přijata malá měkká reklamace a je v procesu
    # zpracování, tzn. nového dodání, DK ukončí tento proces novým dodáním PDF
    # souboru – zatím jen EDD umožňuje malou měkkou reklamaci.
    # Stav soft_ready je stav soft poté co byl změněn soubor.
    # Stav soft_done je stav soft po novém dodání.
    form.mode(complaint_state='display')
    write_permission(complaint_state='cmf.ManagePortal')
    complaint_state = schema.TextLine(
        title=_(u'Reklamace - stav'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # Zda je poslední reklamace typu "jen nové dodání" - jinak je typu vrátit
    # peníze a ukončit subticket. Nastavuje se při vytvoření reklamace. Tedy bez
    # závislosti jak dopadne.
    form.mode(complaint_is_soft='display')
    write_permission(complaint_is_soft='cmf.ManagePortal')
    complaint_is_soft = schema.Bool(
        title=_(u'Reklamace - jen nové dodání'),
        required=False,
        missing_value=False,
        default=False,
    )

    # Odůvodnění námitky, volný text
    form.mode(objection_text='display')
    write_permission(objection_text='cmf.ManagePortal')
    objection_text = schema.TextLine(
        title=_(u'Námitka - text'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # Odůvodnění rozhodnutí SC, volný text
    form.mode(objection_decision_text='display')
    write_permission(objection_decision_text='cmf.ManagePortal')
    objection_decision_text = schema.TextLine(
        title=_(u'Rozhodnutí o námitce - text'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # --------------------------------------------------------------------------
    # důvody (např. zamítnutí) a upřesnění

    form.mode(refuse_reason='display')
    write_permission(refuse_reason='cmf.ManagePortal')
    refuse_reason = schema.TextLine(
        title=_(u'Důvod zamítnutí DK'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(cond_a_reason='display')
    write_permission(cond_a_reason='cmf.ManagePortal')
    cond_a_reason = schema.TextLine(
        title=_(u'Důvod podmínečného přijetí'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(cond_a_fee='display')
    write_permission(cond_a_fee='cmf.ManagePortal')
    cond_a_fee = schema.Int(
        title=_(u'Vyšší cena'),
        description=_(u'Cena, za kterou dožádaná knihovna může poskytnout, bude ještě upřesněno.'),
        required=False,
        min=0,
        max=1000,
        missing_value=None,
    )

    form.mode(cond_a_edition='display')
    write_permission(cond_a_edition='cmf.ManagePortal')
    cond_a_edition = schema.TextLine(
        title=_(u'Jiné vydání'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(cond_a_send_date='display')
    write_permission(cond_a_send_date='cmf.ManagePortal')
    cond_a_send_date = schema.TextLine(
        title=_(u'Očekáváané datum odeslání'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(cond_a_return_d='display')
    write_permission(cond_a_return_d='cmf.ManagePortal')
    cond_a_return_d = schema.Int(
        title=_(u'Počet dnů na výpůjčku'),
        required=False,
        min=0,
        max=1000,
        missing_value=None,
    )

    # Vyzvednout v DK (místo poslání do ŽK)
    form.mode(is_sent_here='display')
    write_permission(is_sent_here='cmf.ManagePortal')
    is_sent_here = schema.Bool(
        title=_(u'Vyzvednout v DK - místo poslání do ŽK'),
        required=False,
        missing_value=False,
    )


class Subticket(TicketBase):
    implements(ISubticket)

    @property
    def ticket(self):
        """Ticket je dán jako parent, tato implementace využívá toho, že je
        vždy nastaveno ticket_id.  Výhodou je, že umožní recovery po změně
        umístění subticketu mimo ticket.
        """

        if DEBUG:
            print('subticket.ticket_id: ' + self.ticket_id)
        if self.ticket_id is None:
            return None
        portal = api.portal.get()
        value = getattr(portal.tickets, self.ticket_id, None)
        if DEBUG:
            print(value)
            print(' ')
        return value

    @property
    def ticket_type(self):
        """Typ požadavku je stejný jako typ objednávky: mvs, mrs, edd.
        Přebírá se z ticketu.
        """

        ticket = self.ticket
        # Nemělo by nastat, ale celý systém je nastaven jako MVS first.
        if not ticket:
            return 'mvs'
        return ticket.ticket_type

    @property
    def hid_prefix(self):
        LABELS = dict(
            mvs = u'MVP',
            mrs = u'MVP',  # FIXME-MRS
            edd = u'EDP',
        )
        return LABELS.get(self.ticket_type, self.ticket_type)

    @property
    def ticket_type_label(self):
        LABELS = dict(
            mvs = u'MVS',
            mrs = u'MRS',
            edd = u'EDD',
        )
        return LABELS.get(self.ticket_type, self.ticket_type)

    @property
    def is_mvs(self):
        return bool(self.ticket_type == 'mvs')

    @property
    def is_mrs(self):
        return bool(self.ticket_type == 'mrs')

    @property
    def is_edd(self):
        return bool(self.ticket_type == 'edd')

    @property
    def created_dt(self):
        """Nyní se pro vrácení této hodnoty používají Zope metadata, to se
        může v budoucnu změnit, pokud se změní, pak stačí změnit jen zde.
        """

        return self.created()

    @property
    def created_iso(self):
        return self.created_dt.ISO()

    @property
    def modified_dt(self):
        """Nyní se pro vrácení této hodnoty používají Zope metadata, to se
        může v budoucnu změnit, pokud se změní, pak stačí změnit jen zde.
        """

        return self.modified()

    @property
    def modified_iso(self):
        return self.modified_dt.ISO()

    @property
    def is_complaint(self):
        """Zda je reklamováno.  Přijatá měkká reklamace nechává nastaveno na
        True i po vyřízení, nemělo by tedy zastavovat běžný proces.
        """

        return bool(self.complaint_date)

    @property
    def complaint_text_label(self):
        text_label = dict(
            complaint_late = u'zatím nedošlo',
            complaint_wrong = u'došlo něco jiného',
            )
        return text_label.get(self.complaint_text, self.complaint_text)

    @property
    def complaint_state_label(self):
        if not self.is_complaint:
            return u''
        value = u''
        if self.complaint_state == u'new':
            value = u'Čeká se na rozhodnutí.'
        elif self.complaint_state == u'accepted':
            value = u'Reklamace přijata.'
        elif self.complaint_state == u'declined':
            value = u'Reklamace odmítnuta.'
        elif self.complaint_state == u'objected':
            value = u'Reklamace odmítnuta, což bylo rozporováno,  Nyní vyřizuje Servisní centrum ZÍSKEJ.'
        elif self.complaint_state == u'objection_accepted':
            value = u'Reklamace odmítnuta, což bylo rozporováno a Servisní centrum ZÍSKEJ reklamaci uznalo.'
        elif self.complaint_state == u'objection_declined':
            value = u'Reklamace odmítnuta, což bylo rozporováno a Servisní centrum ZÍSKEJ reklamaci zamítlo.'
        elif self.complaint_state == u'soft':
            value = u'Reklamace přijata, čeká se na nové dodání.'
        elif self.complaint_state == u'soft_ready':
            value = u'Reklamace přijata, připraveno nové dodání, zatím neodesláno.'
        elif self.complaint_state == u'soft_done':
            value = u'Reklamace přijata a vyřízena.  Připraveno a odesláno nové dodání.'
        return value

    @property
    def complaint_label(self):
        if not self.is_complaint:
            return u''
        # FIXME-EDD-COMPLAINT
        who_label = u'Žádající knihovna'
        if self.complaint_who == 'reader':
            who_label = u'Čtenář'
        value = u'{who} reklamuje: {text}.  '.format(
            who=who_label,
            text=self.complaint_text_label,
            )
        if self.complaint_is_soft:
            value += u'Je požadováno nové dodání.  '
        else:
            value += u'Je požadováno ukončení požadavku a vrácení ceny.  '
        value += self.complaint_state_label
        return value

    @property
    def is_small_complaint_undecided(self):
        """Effectivelly stop any important changes.
        """

        if self.wf_review_state in ('closed', 'cancelled', 'refused', ):
            return False
        return bool(self.complaint_state in (u'new', u'objected', ))

    @property
    def complaint_accepted_is_soft(self):
        """Marker pro filter v seznamu."""
        return bool(self.complaint_soft_date)

    @property
    def is_small_cancel_asked(self):
        """Effectivelly stop any important changes and accept or decline
        cancellation request.
        """

        if self.wf_review_state in ('closed', 'cancelled', 'refused', ):
            return False
        if self.cancellation_small_ask_date is None:
            return False
        if self.cancellation_small_answer_date is None:
            return True
        if self.cancellation_small_answer_date >= self.cancellation_small_ask_date:
            return False
        return True

    @property
    def is_small_cancel_declined(self):
        """Byla žádost o storno odmítnuta?
        
        Používá se pro: Žádost o malé storno odmítnuta - neumožnit zažádat
        znovu.
        """

        if self.cancellation_small_answer_date and \
                self.cancellation_small_ask_date and \
                self.cancellation_small_answer_date >= self.cancellation_small_ask_date and \
                self.wf_review_state != 'cancelled':
            return True
        return False

    @property
    def cancel_label(self):
        value = u''
        if self.wf_review_state == 'cancelled' and not self.is_complaint:
            value = u'Požadavek byl stornován'
        elif self.wf_review_state == 'cancelled' and self.is_complaint:
            value = u''
        elif self.is_small_cancel_declined:
            value = u'Žádost o storno požadavku byla zamítnuta.'
            # u'Žádost o storno objednávky byla zamítnuta.  Ale nyní není žádný aktivní požadavek, zvažte zda nepřehodnotit zamítnutí.'
        elif self.is_small_cancel_asked:
            value = u'Žádost o storno požadavku byla podána a čeká se na odpověď.'
        return value

    @property
    def complaint_cancel_label(self):
        values = []
        # Storno
        value = self.cancel_label
        if value:
            values.append(value)
        # Reklamace
        value = self.complaint_label
        if value:
            values.append(value)
        # Formátování
        value = u', '.join(values)
        if not value:
            value = u''
        return value

    def get_reports(self):
        #print([x for x in dir(self) if 'Value' in x])
        reports = self.objectValues()
        reports = [x for x in reports if x and x.portal_type == 'ReportEntry']
        if DEBUG:
            print('reports')
            print(reports)
        return reports

    def reports_html(self):
        reports = self.get_reports()
        result = u''
        for report in reports:
            result += u'<li>{}</li>\n'.format(report.fullname)
        if result:
            result = u'<ul>{}</ul>\n'.format(result)
        return result

    @property
    def fullname(self):
        doc_title = self.doc_title
        if not doc_title and self.is_edd:
            doc_title = self.doc_fullname
        LIMIT = 48
        if len(doc_title) > LIMIT:
            doc_title = doc_title[:LIMIT] + u'...'
        return u'{shortname}: {doc_title}'.format(
                shortname=self.shortname,
                doc_title=doc_title,
                )

    @property
    def shortname(self):
        return u'{prefix}{hid}'.format(
            prefix=self.hid_prefix,
            hid=self.hid,
            )
        # # alternativní shortname
        # return u'{prefix}{hid}-{fullhid_sigla}'.format(
        #     prefix=self.hid_prefix,
        #     hid=self.hid,
        #     fullhid_sigla=self.fullhid_sigla,
        #     )

    # fullhid je 'MVP100001-ABA013' resp. 'MVP100001-ABA013-2' atp.
    @property
    def fullhid(self):
        value = u'{prefix}{hid}-{fullhid_sigla}'.format(
            prefix=self.hid_prefix,
            hid=self.hid,
            fullhid_sigla=self.fullhid_sigla,
            )
        if self.fullhid_postfix:
            value = u'{value}-{fullhid_postfix}'.format(
                value=value,
                fullhid_postfix=self.fullhid_postfix,
                )
        return value

    # název pro ŽK
    def TitleZK(self):
        library_dk = self.library_dk
        if library_dk:
            library_dk = library_dk.upper()
        value = u'{shortname}: {sigla}'.format(
                shortname=self.shortname,
                sigla=library_dk,
                )
        if self.fullhid_nr:
            value += u' ({fullhid_nr}.)'.format(fullhid_nr=self.fullhid_nr)
        return value

    def Title(self):
        return self.fullname

    def add_historylog(self, value, who=None):
        # vždy zapisujeme do hlavního audit logu pro servisní středisko
        self.historylog = format_historylog_entry(
            prev=self.historylog,
            value=value,
            )
        # a někdy i do historie pro DK
        if who and 'dk' in who:
            self.historylog_dk = format_historylog_entry(
                prev=self.historylog_dk,
                value=value,
                )

    def get_historylog(self, who=None, format='html'):
        if not who:
            return u''
        value = u''
        if who == 'operator':
            value = self.historylog
        elif who == 'library_dk':
            value = self.historylog_dk
        if not value:
            return u''
        values = value.split('\n')
        values.reverse()
        values = [x for x in values if x]
        if format != 'html':
            return values
        result = u''
        for value in values:
            result += u'<li>{}</li>\n'.format(value)
        result = u'<ul>{}</ul>\n'.format(result)
        return result

    @property
    def refused_date_guess(self):
        """Starší požadavky nemají po odmítnutí vyplněn refused_date, toto je
        pokus o odhad hodnoty pro ně.
        """

        # Jedná se o novější požadavek s vyplněným refused_date
        if self.refused_date:
            return self.refused_date
        # Požadavek nebyl nikdy odmítnut
        wf_review_state = self.wf_review_state
        if wf_review_state != 'refused':
            return None
        # Je nutné se pokusit odhadnout datum odmítnutí
        # Datum rozhodnutí je nejpřesnější guess
        if self.decided_date:
            return self.decided_date
        # Alternativně a přesnějni je možné vzít z audit logu
        return self.cond_accepted_date

    # Nepoužívá se, s výjimkou /debug
    @property
    def decided_state(self):
        decided_date = self.decided_date
        accepted_date = self.accepted_date
        cond_accepted_date = self.cond_accepted_date
        wf_review_state = self.wf_review_state
        if not decided_date:
            if not accepted_date:
                if not cond_accepted_date:
                    # ['cancelled', 'queued']
                    value = u''
                else:
                    # ['refused', 'conditionally_accepted']
                    if wf_review_state == 'refused':
                        # DK nejprve podmíněně přijal a pak odmítl, datum
                        # odmítnutí nicméně není v decided_date.
                        value = u'refused-dk-cond_accepted'
                    else:
                        value = u'cond_accepted'
            else:
                if not cond_accepted_date:
                    value = u'error-2'
                else:
                    value = u'error-3'
        else:
            if not accepted_date:
                if not cond_accepted_date:
                    # ['refused']
                    value = u'refused'
                else:
                    # ['refused']
                    value = u'refused-zk-dk-cond_accepted'
            else:
                if not cond_accepted_date:
                    # ['closed', 'cancelled', 'sent', 'sent_back', 'refused',
                    #  'accepted']
                    if wf_review_state == 'refused':
                        # DK nejprve přijal a pak odmítl.
                        value = u'refused-dk-accepted'
                    else:
                        value = u'accepted'
                else:
                    # ['sent', 'cancelled', 'accepted', 'refused']
                    value = u'accepted-cond_accepted'
        return value

    def is_cancelled_by_sc(self):
        if self.wf_review_state != 'cancelled':
            if DEBUG:
                print("is_cancelled_by_sc False due to wf_review_state")
            return False
        hlog_start = u'Požadavek byl odebrán Servisním centrem ZÍSKEJ a tedy stornován'
        hlog_start_tmp = u'Požadavek byl odebrán (SC) a tedy stornován.'
        hlog_end = u'SC ZÍSKEJ)'
        hlog_end_tmp = u'knihovna ?)'
        hlog = self.get_historylog(who='operator', format='raw')
        for hlog_item in hlog:
            if (hlog_start in hlog_item or hlog_start_tmp in hlog_item) and \
                    (hlog_item.endswith(hlog_end) or
                    hlog_item.endswith(hlog_end_tmp)):
                if DEBUG:
                    print("is_cancelled_by_sc True due to text")
                return True
        if DEBUG:
            pprint(hlog)
            print("is_cancelled_by_sc False otherwise")
        return False

    def is_conditionally_accepted_by_dk(self):
        if self.wf_review_state == 'conditionally_accepted':
            return True
        if self.wf_review_state not in ('refused', 'sent', 'sent_back',
                'cancelled'):
            return False
        history = self.wf_history()
        for one in history:
            if one["review_state"] == 'conditionally_accepted':
                return True
        return False

    @property
    def wf_status(self):
        """Vrátí workflow status dict doplněný o workflow review_state label"""
        wf_tool = api.portal.get_tool('portal_workflow')
        status = wf_tool.getStatusOf("subticketmvs_workflow", self)
        review_state = status["review_state"]
        status["review_state_label"] = get_subticket_wf_label(review_state, review_state)
        dt = status["time"]
        status["ts"] = dt.strftime('%d.%m.%Y %H:%M')
        return status

    @property
    def wf_review_state(self):
        """Vrátí workflow review_state."""
        return self.wf_status['review_state']

    @property
    def wf_review_state_label(self):
        """Vrátí workflow review_state label."""
        return self.wf_status['review_state_label']

    @property
    def is_wf_cancel_by_operator(self):
        wf_tool = api.portal.get_tool('portal_workflow')
        history = wf_tool.getHistoryOf("subticketmvs_workflow", self)
        result = []
        if history is None:
            return False
        for one in history:
            if one["comments"] == 'small cancellation by operator':
                return True

        return False

    def wf_history(self):
        """Vrátí workflow history - seznam jednotlivých workflow status, každý
        z nich dict doplněný o workflow review_state label.
        """

        wf_tool = api.portal.get_tool('portal_workflow')
        history = wf_tool.getHistoryOf("subticketmvs_workflow", self)
        result = []
        if history is None:
            return result
        for one in history:
            # one = {'action': None, 'review_state': 'draft', 'actor': 'admin', 'comments': '', 'time': DateTime('2017/10/23 22:15:59.599496 GMT+2')}
            review_state = one["review_state"]
            one['review_state_label'] = get_subticket_wf_label(review_state, review_state)
            dt = one["time"]
            one["ts"] = dt.strftime('%d.%m.%Y %H:%M')
            result.append(one)
        return result

    @property
    def status_apiaks(self):
        API_LABELS = {
            'queued': u'Přidělený',
            'conditionally_accepted': u'Podmínečně přijatý',
            'accepted': u'Přijatý, ve zpracování',
            'sent': u'Odeslaný',
            'sent_back': u'Odeslaný zpět',
            'closed': u'Uzavřený',
            'cancelled': u'Stornovaný',
            'refused': u'Odmítnutý',
            }

        status = self.wf_review_state

        # nyní už máme vše připraveno
        return dict(
            status = status,
            status_label = API_LABELS.get(status, status),
            is_open = status not in ('refused', 'cancelled', 'closed', ),
            )

    @property
    def is_open_apiaks(self):
        return bool(
            self.wf_review_state not in ('refused', 'cancelled', 'closed', ))

    def is_open_or_closed_in_apiaks(self, closed_in):
        wf_review_state = self.wf_review_state
        if wf_review_state not in ('refused', 'cancelled', 'closed', ):
            return True
        return bool(DateTime(self.closed_date) + closed_in + 1 > DateTime())

    def ts_wf_last(self):
        """ts_wf_last aka timestamp workflow last change"""
        wf_tool = api.portal.get_tool('portal_workflow')
        status = wf_tool.getStatusOf("ticketmvs_workflow", self)
        return status["time"]

    def ts_wf_limit(self):
        """ts_wf_limit aka timestamp workflow limit for next action"""
        return None

    def Next_action_date(self):
        dt = self.next_action_date
        if not dt:
            return u''
        return format_date(dt, is_time=True, is_html=True)

    def set_next_action(self, na_type=None, na_value=None, na_text=None):
        """Nastaví další akci na text na_text a datum podle na_type resp. pro
        některé na_type i podle na_value.

        na_type = None .. vymaže další akci
        na_type = 'date_iso' .. nastaví datum na datum podle na_value, kde
                                očekává string ve tvaru '2017-11-15'
        na_type = '1wd' .. nastaví datum na datum +1 pracovní den od teď (včetně
                           času)
        na_type = '28d' .. nastaví datum na datum +28 dbů od teď (včetně času)

        do budoucna přidat:
        na_type = 'wd' .. nastaví datum na datum podle na_value na na_value
                          pracovních dnů ode dneška
        na_type = 'd' .. nastaví datum na datum podle na_value na na_value dnů
                         ode dneška

        SUGGESTION ZIS-54
        """

        if na_type not in (None, 'date_iso', '1wd', '28d', ):
            logger.error('Unknown next action type ' + na_type)
            raise Exception('Unknown next action type')
        if not na_type:
            na_date = None
            na_text = u''
        elif na_type == 'date_iso':
            # na_value = '2017-11-15'
            na_date = DateTime(na_value + ' 12:00')
        elif na_type == '1wd':
            na_date = datetime_add_wd(1)
        elif na_type == '28d':
            na_date = datetime_add_d(28)

        self.next_action_title = na_text
        self.next_action_date = na_date
        self.reindexObject()
        ticket = self.ticket
        if ticket:
            ticket.next_action_title = na_text
            ticket.next_action_date = na_date
            ticket.reindexObject()
        else:
            logger.warning('subticket.set_next_action missing ticket')

    def add_message_txt(self, sender, text):
        # validation, sanitization
        text = sanitize_input_text(text)
        now = DateTime()

        # save message
        obj_str = u'{sender}\n{iso}\n{text}'.format(sender=sender, iso=safe_unicode(now.ISO()), text=text)
        if self.pm is None:
            self.pm = []
        self.pm.append(obj_str)
        self.pm_date = now
        if sender == 'library_zk':
            self.pm_unread = u'library_dk'
        else:
            self.pm_unread = u'library_zk'

        # send notifications
        logger.info('Notification subticket.add_message_txt')

        return True

    def get_messages(self, render_html=True, is_apiaks=False, unread_by=None):
        if self.pm is None or len(self.pm) == 0:
            return []
        is_unread = bool(self.pm_unread == unread_by)
        messages = []
        for item in self.pm:
            lines = item.split('\n', 3)
            sender = lines[0]
            date = lines[1]
            text = lines[2]
            if not is_apiaks and render_html:
                if sender == u'library_dk':
                    sender = u'Dožádaná knihovna'
                else:
                    sender = u'Žádající knihovna'
                date = DateTime(date).strftime('%-d.%-m.%Y %-H:%M')
            if not is_apiaks:
                messages.append(
                    dict(
                        sender = sender,
                        date = date,
                        text = text,
                        )
                    )
            else:
                messages.append(
                    dict(
                        sender = sender,
                        created_datetime = date,
                        unread = is_unread,
                        text = text,
                        )
                    )
        messages.reverse()
        return messages

    def get_messages_counts(self, unread_by=None):
        count_messages = 0
        count_messages_unread = 0
        if self.pm is not None:
            count_messages = len(self.pm)
            if self.pm_unread == unread_by:
                count_messages_unread = 1
        return dict(
            count_messages = count_messages,
            count_messages_unread = count_messages_unread,
            )

    def Is_pdf_uploaded(self):
        if not self.pdf_file_id:
            return False
        return True

    def remove_pdf_file(self, remove_download_expiration_date=True):
        edd_remove_file(ZISKEJ_VAR_EDD, self.pdf_file_id)
        self.pdf_file_id = None
        self.pdf_file_filename = None
        if remove_download_expiration_date:
            self.download_expiration_date = None

    def add_pdf_file(self, content, filename):
        pdf_file_id = 'edd-{}-{}-{}.pdf'.format(
            self.hid,
            DateTime().ISO()[:10],
            self.getId(),
            )
        edd_add_file(ZISKEJ_VAR_EDD, pdf_file_id, content)
        self.pdf_file_id = pdf_file_id
        self.pdf_file_filename = filename
        # Nastavit download_expiration_date až po odeslání notifikace čtenáři

    def is_pdf_file_unexpired(self):
        if self.wf_review_state != 'sent':
            return False
        if self.download_expiration_date is None:
            return False
        dt = DateTime()
        # dt = dt + 100  # DEBUG
        pdt = datetime.datetime.strptime(
            dt.strftime('%Y-%m-%d %-H:%M'), '%Y-%m-%d %H:%M')  # no timezone
        return bool(self.download_expiration_date > pdt)

    def is_pdf_file_complaintable(self):
        if self.wf_review_state != 'sent':
            return False
        if self.download_expiration_date is None:
            return False
        if not self.downloaded_number:  # None, 0
            return False
        dt = DateTime() - ziskej_parameter('edd_complaint_days')  # 31
        # dt = dt + 100  # DEBUG
        pdt = datetime.datetime.strptime(
            dt.strftime('%Y-%m-%d %-H:%M'), '%Y-%m-%d %H:%M')  # no timezone
        return bool(self.download_expiration_date > pdt)

    def cena_from_pages_number(self, pages_number_dk):
        """Určí cenu za EDD, kterou dostane DK.

        Používá se po změně skutečného počtu stran, ale dříve, než dojde
        k zápisu této změny, proto je nutné počet předat jako parametr.
        """

        value = None
        if not pages_number_dk:
            return value
        return int(pages_number_dk)

    def Edd_data(self):
        """EDD článek i výňatek: pages_from, pages_to, doc_title_in, doc_quote,
        doc_number_year, doc_number_pyear, doc_number_pnumber.
        """

        ticket = self.ticket
        if not ticket:
            return None
        return ticket.Edd_data()

    def Is_Dilia_data(self):
        ticket = self.ticket
        if not ticket:
            return False
        return ticket.Is_Dilia_data()

    def update_attrs_if_changed(self, **obj_dict):
        ALLOWED_ATTRS = (
            'cena', 'back_date', 'report_date', 'pages_number_dk', 'doc_dk_id',
            'rfid', 'barcode', )
        is_updated = False
        for k in obj_dict:
            if k not in ALLOWED_ATTRS:
                logger.error('update_attrs_if_changed key {} is not allowed'.format(k))
                raise Exception('Internal error')
            v_orig = getattr(self, k, None)
            v = obj_dict.get(k, None)
            if v_orig != v:
                setattr(self, k, v)
                #logger.info('doit for {k}: {v_orig} to {v}'.format(k=k, v_orig=v_orig, v=v))
                is_updated = True
        return is_updated

    def export_as_dict(self):
        data = dict(id = self.id)
        names = [
            'ticket_id', 'ticket_type', 'ticket_type_label',
            'hid', 'hid_prefix',
            'fullhid_sigla', 'fullhid_nr',
            'library_zk', 'library_dk',
            'library_zk_title', 'library_dk_title',
            'librarian',
            'created_iso', 'modified_iso',
            'assigned_date', 'decide_date',
            'decided_date', 'cond_accepted_date', 'accepted_date',
            'decided_state', 'refused_date', 'refused_date_guess',
            'sent_date', 'back_date',
            'sent_back_date', 'closed_date',
            'report_date',
            'modified_date',
            'cena',
            'historylog', 'historylog_dk', 
            'doc_id', 'doc_title', 'doc_fullname',
            'doc_data', 
            'doc_sent',
            'doc_dk_id', 'rfid', 'barcode',
            'pages_number_dk', 'pdf_file_id',
            'download_expiration_date', 'downloaded_date', 'downloaded_number',
            'pm', 'pm_date', 'pm_unread',
            'unread_dk', 'unread_op',
            'next_action_title', 'next_action_date',
            'next_action_last_notification',
            'cancellation_small_ask_date', 'cancellation_small_answer_date',
            'complaint_date', 'complaint_text', 'complaint_state',
            'objection_text', 'objection_decision_text',
            'refuse_reason',
            'cond_a_reason', 'cond_a_fee', 'cond_a_edition',
            'cond_a_send_date', 'cond_a_return_d',
            'is_sent_here',
            ]
        names_dt = [
            'report_date', 'modified_date', 'pm_date', 'next_action_date',
            'next_action_last_notification',
            'download_expiration_date', 'downloaded_date',
            ]
        names_json = ['doc_data', ]
        for name in names:
            value = getattr(self, name, None)
            if callable(value):
                value = value()
            if name in names_dt and value is not None:
                value = value.strftime('%Y-%m-%d %-H:%M:%S')
            if name in names_json and value is not None:
                try:
                    value = json.loads(value)
                except:
                    pass
            data[name] = value
        return data
