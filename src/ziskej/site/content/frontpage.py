# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): Frontpage.
"""

import json

from zope import schema
from zope.interface import Interface, implements
from plone.autoform.directives import read_permission, write_permission
from plone.dexterity.content import Container
from plone.directives import form

from ziskej.site import _
from ziskej.site.utils import make_searchable_text
from ziskej.site.content.base import IBase, Base


class IFrontpage(IBase):

    # json
    form.mode(parameters_data='display')
    write_permission(parameters_data='cmf.ManagePortal')
    parameters_data = schema.Text(
        title=_(u'Parametry systému jako JSON'),
        required=False,
        missing_value=u"",
        default=u"",
    )


class Frontpage(Base):
    implements(IFrontpage)

    def SearchableText(self):
        return make_searchable_text(self, 'title', 'description', )

    def set_parameters(self, parameters, delete_missing=False):
        if not delete_missing:
            parameters_orig = dict()
            if self.parameters_data:
                try:
                    parameters_orig = json.loads(self.parameters_data)
                except:
                    pass
            for par in parameters:
                parameters_orig[par] = parameters[par]
            self.parameters_data = json.dumps(parameters_orig)
        else:
            self.parameters_data = json.dumps(parameters)

    def get_parameters(self, name=None, default=None):
        parameters = dict()
        if self.parameters_data:
            try:
                parameters = json.loads(self.parameters_data)
            except:
                pass

        par_default = dict(
            emergency_header = u'',
            # mvs_ticket_hid_prefix = u'MVA',
            # mvs_subticket_hid_prefix = u'MVP',
            testing_now_str = u'',
            edd_selection_label = u'Výňatek z monografie a periodika'
            )

        if name is not None:
            if default is not None:
                par_default[name] = default
            return parameters.get(name, par_default.get(name, default))

        result = dict()
        for par in par_default:
            if par in parameters:
                result[par] = parameters[par]
            else:
                result[par] = par_default[par]

        return result

    def get_parameter(self, name, default=None):
        return self.get_parameters(name=name, default=default)
