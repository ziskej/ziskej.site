# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): ReportEntry.
"""

from __future__ import print_function

import json

from zope import schema
from zope.interface import implements
from plone.autoform.directives import read_permission, write_permission
from plone.directives import form

from ziskej.site import _
from ziskej.site.content.base import IBase, Base
from ziskej.site.ticket_helpers import TICKET_HID_MIN
from ziskej.site.utils_lib import format_date


class IReportEntry(IBase):

    # ticket_type
    form.mode(ticket_type='display')
    write_permission(ticket_type='cmf.ManagePortal')
    ticket_type = schema.ASCIILine(
        title=_(u'Typ objednávky'),
        description=_(u'Typ objednávky: mvs, mrs, edd.'),
        required=True,
        default="mvs",
    )

    form.mode(ticket_id='display')
    write_permission(ticket_id='cmf.ManagePortal')
    ticket_id = schema.ASCIILine(
        title=_(u'ID objednávky'),
        description=_(u'Ticket ID'),
        required=True,
    )

    form.mode(subticket_id='display')
    write_permission(subticket_id='cmf.ManagePortal')
    subticket_id = schema.ASCIILine(
        title=_(u'ID požadavku'),
        description=_(u'Subticket ID'),
        required=True,
    )

    # ticket hid, subticket nemá vlastní hid (to je změna z 2017-10)
    form.mode(hid='display')
    write_permission(hid='cmf.ManagePortal')
    hid = schema.Int(
        title=_(u'HID'),
        description=_(u'Human ID'),
        required=True,
        min=TICKET_HID_MIN,
    )

    # MVA = MVS ŽK, MVP = MVS DK, EDA = EDD ŽK, EDP = EDD DK
    # přebírá se ze subticket, kde je hid_prefix property metoda
    form.mode(hid_prefix='display')
    write_permission(hid_prefix='cmf.ManagePortal')
    hid_prefix = schema.ASCIILine(
        title=_(u'HID prefix'),
        description=_(u'Human ID'),
        required=False,
        missing_value=u"",
    )

    # sigla ŽK
    form.mode(library_zk='display')
    write_permission(library_zk='cmf.ManagePortal')
    library_zk = schema.ASCIILine(
        title=_(u'Žádající knihovna'),
        required=False,
        missing_value=u"",
    )

    # sigla DK
    form.mode(library_dk='display')
    write_permission(library_dk='cmf.ManagePortal')
    library_dk = schema.ASCIILine(
        title=_(u'Dožádaná knihovna'),
        required=False,
        missing_value=u"",
    )

    # název ŽK
    form.mode(library_zk_title='display')
    write_permission(library_zk_title='cmf.ManagePortal')
    library_zk_title = schema.TextLine(
        title=_(u'Název žádající knihovny'),
        required=False,
        missing_value=u"",
    )

    # název DK
    form.mode(library_dk_title='display')
    write_permission(library_dk_title='cmf.ManagePortal')
    library_dk_title = schema.TextLine(
        title=_(u'Název dožádané knihovny'),
        required=False,
        missing_value=u"",
    )

    # Datum pro report
    form.mode(report_date='display')
    write_permission(report_date='cmf.ManagePortal')
    report_date = schema.Datetime(
        title=_(u'Datum pro report'),
        required=False,
        missing_value=None,
    )

    # Cena pro report
    form.mode(cena='display')
    write_permission(cena='cmf.ManagePortal')
    cena = schema.Int(
        title=_(u'Cena'),
        description=_(u'Cena pro report'),
        required=False,
        min=0,
        max=1000,
        missing_value=None,
    )

    # user_id uživatele, který způsobil zápis reportu
    form.mode(user_id='display')
    write_permission(user_id='cmf.ManagePortal')
    user_id = schema.TextLine(
        title=_(u'Uživatelské jméno'),
        required=False,
        missing_value=u"",
    )

    # std | auto-cancel | auto-complaint | operator
    # Standardní, Automatická oprava - storno, reklamace, Zásah servisního střediska
    form.mode(entry_type='display')
    write_permission(entry_type='cmf.ManagePortal')
    entry_type = schema.TextLine(
        title=_(u'Typ položky'),
        required=False,
        missing_value=u"",
    )

    # Podtyp záznamu
    # MVS
    # Různé kombinace těchto parametrů:
    #     std x ret (standardní MVS x vrácení, protože storno po odeslání resp. uznaná reklamace)
    #     lib x rdr (objednávka vytvořena knihovníkem x čtenářem a proto platba čtenáře předem)
    #     szk x ntk (stadardní ŽK x NTK)
    #     sdk x ntk (stadardní DK x NTK)
    # Výsledné varianty:
    #     1) std-lib-szk-sdk, ret-lib-szk-sdk
    #     2) std-lib-ntk-sdk, ret-lib-ntk-sdk
    #     3) std-lib-szk-ntk, ret-lib-szk-ntk
    #     4) std-rdr-szk-sdk, ret-rdr-szk-sdk
    #     5) std-rdr-ntk-sdk, ret-rdr-ntk-sdk
    #     6) std-rdr-szk-ntk, ret-rdr-szk-ntk
    # EDD
    # Varianty:
    #     1) edd-dk-std, edd-dk-ret
    #     2) edd-dilia-std, edd-dilia-ret
    form.mode(entry_subtype='display')
    write_permission(entry_subtype='cmf.ManagePortal')
    entry_subtype = schema.TextLine(
        title=_(u'Typ položky'),
        required=False,
        missing_value=u"",
    )

    # Poznámka
    form.mode(note='display')
    write_permission(note='cmf.ManagePortal')
    note = schema.Text(
        title=_(u'Poznámka'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    form.mode(payment_entry_id='display')
    write_permission(payment_entry_id='cmf.ManagePortal')
    payment_entry_id = schema.TextLine(
        title=_(u'ID meziknihovní platby'),
        required=False,
        default=u'',
        missing_value=u'',
    )

    form.mode(payment_timestamp='display')
    write_permission(payment_timestamp='cmf.ManagePortal')
    payment_timestamp = schema.TextLine(
        title=_(u'Timestamp meziknihovní platby'),
        required=False,
        default=u'',
        missing_value=u'',
    )

    # Nepoužívá se
    form.mode(ntk_payment_timestamp='display')
    write_permission(ntk_payment_timestamp='cmf.ManagePortal')
    ntk_payment_timestamp = schema.TextLine(
        title=_(u'Timestamp platby prostřednictvím NTK platebního systému'),
        required=False,
        default=u'',
        missing_value=u'',
    )

    form.mode(is_paid_in_advance='display')
    write_permission(is_paid_in_advance='cmf.ManagePortal')
    is_paid_in_advance = schema.Bool(
        title=_(u'Zaplaceno čtenářem'),
        description=_(u'Objednávka byla zaplacena čtenářem, proto ŽK neplatí.'),
        required=False,
        missing_value=False,
    )

    # json
    # EDD Dília data json dict keys
    # dilia: rok_vydani, zeme_vydani, vydavatel,
    #        is_kolektiv,
    #        autor_jmeno_1, autor_prijmeni_1,
    #        autor_jmeno_2, autor_prijmeni_2,
    #        autor_jmeno_3, autor_prijmeni_3,
    form.mode(edd_dilia_data='display')
    write_permission(edd_dilia_data='cmf.ManagePortal')
    edd_dilia_data = schema.Text(
        title=_(u'Data pro Dília XML report (EDD)'),
        required=False,
        missing_value=u"",
        default=u"",
    )


class ReportEntry(Base):
    implements(IReportEntry)

    def export_as_dict(self, use_repr=False):
        data = dict(id = self.id)

        names = ['ticket_type', 'ticket_id', 'subticket_id', 'hid',
                 'hid_prefix', 'library_zk', 'library_dk', 'library_zk_title',
                 'library_dk_title', 'report_date', 'cena', 'user_id',
                 'entry_type', 'entry_subtype', 'note', 'payment_entry_id',
                 'payment_timestamp', 'ntk_payment_timestamp',
                 'is_paid_in_advance', 'edd_dilia_data', ]
        names_dt = ['report_date', ]
        names_json = ['edd_dilia_data', ]
        for name in names:
            value = getattr(self, name, None)
            if name == 'edd_dilia_data':
                print('-- edd_dilia_data')
            if callable(value):
                value = value()
            if name in names_dt and value is not None:
                value = value.strftime('%Y-%m-%d %-H:%M:%S')
            if name in names_json and value:
                try:
                    value = json.loads(value)
                except:
                    pass
                    raise
            if name == 'edd_dilia_data':
                print('  ', repr(value))
            data[name] = value
        return data

    def pprint(self):
        print('id', ':', repr(self.id))
        attrs = ['ticket_type', 'ticket_id', 'subticket_id', 'hid',
                 'hid_prefix', 'library_zk', 'library_dk', 'library_zk_title',
                 'library_dk_title', 'report_date', 'cena', 'user_id',
                 'entry_type', 'entry_subtype', 'note', 'payment_entry_id',
                 'payment_timestamp', 'ntk_payment_timestamp',
                 'is_paid_in_advance', ]
        for attr in attrs:
            value = getattr(self, attr, None)
            print(attr, ':', repr(value))

    def Report_date(self, is_html=False):
        dt = self.report_date
        if not dt:
            return u''
        value = format_date(dt, is_time=True, is_html=is_html)
        return value

    def Title(self):
        return u'Cena {fee} Kč, {user_id} [{date}]'.format(date=self.Report_date(), fee=self.cena, user_id=self.user_id)

    @property
    def fullname(self):
        return u'Cena {fee} Kč, {user_id} [{date}]'.format(date=self.Report_date(is_html=True), fee=self.cena, user_id=self.user_id)

    def has_payment_entry(self):
        """Má platbu v rámci prostředků evidovaných v Získej"""
        return bool(self.payment_entry_id) and bool(self.payment_timestamp)

    # Nepoužívá se
    # @property
    # def has_ntk_payment(self):
    #     """Má platbu prostřednictvím NTK platebního systému?"""
    #     return bool(self.ntk_payment_timestamp)

    def setup(self, is_zk_std=True, is_dk_std=True, is_dilia=False, is_paid_in_advance=False):
        if self.ticket_type == 'mvs':
            return self._setup_mvs(
                is_zk_std=is_zk_std,
                is_dk_std=is_dk_std,
                is_paid_in_advance=is_paid_in_advance,
                )
        if self.ticket_type == 'edd':
            return False
        raise Exception('Internal error')

    def _setup_mvs(self, is_zk_std=True, is_dk_std=True, is_paid_in_advance=False):
        updated = False
        if self.is_paid_in_advance != is_paid_in_advance:
            self.is_paid_in_advance = is_paid_in_advance
            updated = True
        subtype = u''
        if self.entry_type == u'std':
            subtype += u'std'
        else:
            subtype += u'ret'
        subtype += u'-'
        if is_paid_in_advance:
            subtype += u'rdr'
        else:
            subtype += u'lib'
        subtype += u'-'
        if is_zk_std:
            subtype += u'szk'
        else:
            subtype += u'ntk'
        subtype += u'-'
        if is_dk_std:
            subtype += u'sdk'
        else:
            subtype += u'ntk'
        if self.entry_subtype != subtype:
            self.entry_subtype = subtype
            updated = True
        return updated
