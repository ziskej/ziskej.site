# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): TicketMVS.

Založený na: Ticket.
"""

import logging

#from zope import schema
from zope.interface import implements
#from plone.directives import form

from ziskej.site import (
    _,
    ENFORCE_PAYSYS,
    )
#from ziskej.site.utils import make_searchable_text
from ziskej.site.content.ticket_base import ITicket, Ticket


logger = logging.getLogger("ziskej")


class ITicketMVS(ITicket):
    """Objednávka, přebírá atributy z ITicket."""
    pass


class TicketMVS(Ticket):
    """Objednávka, více viz Ticket."""
    implements(ITicketMVS)

    # Workflow constraints

    @property
    def can_assign(self):
        if self.wf_review_state not in ('created', 'assigned', 'pending', ):
            return False

        # nelze, když není ověřen čtenář
        if not self.is_reader_approved:
            return False

        # nelze, když existuje aktivní subticket
        if self.subticket_id:
            return False

        # nelze, pokud je zažádáno o velké storno
        if self.is_big_cancel_asked:
            return False

        # nelze, když není určeno pořadí DK (po načtení dostupnosti), resp. není určena manuální DK
        if self.lb_state not in (u'manual: chosen',
                                 u'auto: load availability order accepted',
                                 u'auto: current',
                                 u'auto: last closed', ):
            return False

        # je vše připraveno pro další přiřazení?
        if not self.is_lb_ready_for_assignment:
            return False

        # MVS only
        if self.is_mvs:

            # vytvořeno čtenářem přes CPK
            if self.is_created_by_cpk_for_reader():

                # zatím nezaplaceno
                if not self.is_paid_in_advance():
                    return False

            # nevytvořeno čtenářem přes CPK
            else:

                # je dostatek kreditu pro vytvoření požadavku?
                if ENFORCE_PAYSYS and not self.mvs_zk_enough_available():
                    return False

        return True

    @property
    def can_prepare(self):
        wf_status = self.wf_status
        #if self.wf_review_state not in ('created', 'assigned', 'pending', ):  # pending - without DK
        if wf_status['review_state'] not in ('assigned', ):
            return False
        if wf_status['subticket_review_state'] not in ('sent', ):
            return False

        # nelze, pokud je zažádáno o velké storno
        if self.is_big_cancel_asked:
            return False

        # nelze, když není ověřen čtenář
        if not self.is_reader_approved:
            return False

        # nelze, když není vše vyplněno
        if self.is_mvs:
            if not self.reader_from_date or not self.reader_to_date or \
                    self.reader_fee is None or self.reader_fee == u'' or \
                    not self.reader_place:
                return False

        if self.is_mrs:
            if not self.reader_from_date or self.reader_fee is None or \
                    self.reader_fee == u'' or not self.reader_place:
                return False

        # print "-- EDD FIXME 013 ignorovat"
        # FIXME-EDD prepared
        # if self.is_edd:
        #     if not self.reader_from_date or self.reader_fee is None or \
        #             self.reader_fee == u'' or not self.reader_place:
        #         return False

        return True

    @property
    def can_cancel(self):
        return True

    @property
    def can_reject(self):
        return True

    @property
    def can_stop(self):
        return True

    @property
    def can_close(self):
        if self.wf_review_state not in ('prepared', ):
            return False

        return True

    # end of workflow can_(transition)
