# -*- coding: utf-8 -*-
"""Content type Library"""

from __future__ import print_function

from datetime import datetime, timedelta
import json
import jwt
import os.path
import logging
import uuid

from DateTime import DateTime
from zope import schema
from zope.interface import Interface, implements
from plone.autoform.directives import read_permission, write_permission
from plone.dexterity.content import Container
from plone.directives import form
from plone.supermodel import model
from plone import api

from ziskej.site import (
    _,
    SECRETS_DIR,
    APIAKS_ENABLED,
    )
from ziskej.site.auth.auth import get_librarians_by_library
from ziskej.site.content.base import IBase, Base
from ziskej.site.library_payment_helpers import (
    credit_info,
    ZiskejAsyncException,
    )
from ziskej.site.ticket_helpers import (
    get_subticket_by_report_entry,
    get_ticket_by_hid,
    list_tickets_by_sigla,
    list_subtickets_by_sigla,
    )
from ziskej.site.ziskej_parameters import ziskej_parameter
from ziskej.site.utils import (
    count_wd,
    datetime_add_wd,
    datetime_normalize,
    format_normalized_dt,
    make_searchable_text,
    format_historylog_entry,
    pprint,
    safe_utf8,
    safe_unicode,
    )
from ziskej.site.validators import validate_email, Invalid


logger = logging.getLogger("ziskej")

CENA_MIN = 0
CENA_MAX = 200
CENA_DEFAULT = 100

DEBUG_VAC = False
DEBUG_VAC24 = False


class ILibrary(IBase):
    """Knihovna

    Zdroj CPK API - příklad:

    SGL BOA001
    NAZ Moravská zemská knihovna v Brně
    VAR Moravian Library
    ZKR MZK
    ICO {'a': '00094943', 'b': 'CZ00094943'}
    EMK 82
    TYP {'a': 'KK', 'b': 'krajská knihovna'}
    FCE ['profesionální', 'pověřená regionální funkcí', 'právní subjektivita']
    ADR {'u': 'Kounicova 65a', 'c': '601 87', 'm': 'Brno', 'g': '49°12\'31.14"N, 16°35\'38.5"E'}
    MES Brno
    KRJ {'a': 'Jihomoravský', 'b': 'Brno'}
    JMN [{'t': 'prof. PhDr.', 'k': 'Tomáš', 'p': 'Kubíček', 'c': 'Ph.D.', 'r': 'ředitel', 'o': 'Pan'}, {'t': 'PhDr.', 'k': 'Jindra', 'p': 'Pavelková', 'r': 'zástupce ředitele', 'o': 'Paní'}]
    TEL ['541 646 111', '541 646 101 (sekretariát)', '541 646 175 (MVS)', '541 646 210 (odd. půjčovny)', '541 646 206 (půjčovna)', '541 646 201 (informace)', '542 162 150 (Odbor doplňování KF)', '542 646 190 (Odbor informačních služeb)', '541 646 194 (Studovna humanitních věd)', '541 646 156 (Studovna rukopisů a starých tisků)', '541 646 196 (Studovna přírodních a technických věd)', '541 646 301 (Česká knihovna)', '541 646 185 (Studovna periodik a norem)', '541 646 321 (Zahraniční knihovny)', '541 646 126 (Odbor knihovnictví)', '541 646 164 (Hudební knihovna)']
    FAX ['541 646 100 (Kounicova 65a)', '541 646 300 (Česká knihovna)', '541 646 177 (MVS)']
    EML [{'u': 'mzk@mzk.cz', 'z': 'knihovna'}, {'u': 'tomas.kubicek@mzk.cz', 'z': 'ředitel'}, {'u': 'jindra.pavelkova@mzk.cz', 'z': 'zástupce ředitele'}, {'u': 'petr.zabicka@mzk.cz', 'z': 'náměstek'}, {'u': 'pujcovna@mzk.cz', 'z': 'informace a půjčovna'}, {'u': 'akvizice@mzk.cz', 'z': 'odbor doplňování fondů'}, {'u': 'mvs@mzk.cz', 'z': 'mvs'}, {'u': 'casopisy@mzk.cz', 'z': 'odbor periodik'}, {'u': 'irena.vesela@mzk.cz', 'z': 'hudební knihovna'}, {'u': 'rukopisy@mzk.cz', 'z': 'odbor rukopisů a starých tisků'}, {'u': 'michal.indrak@mzk.cz', 'z': 'odbor digitalizace'}, {'u': 'ceskaknihovna@mzk.cz', 'z': 'česká knihovna'}, {'u': 'zhr@mzk.cz', 'z': 'zahraniční knihovny'}]
    URL [{'u': 'https://vufind.mzk.cz/', 'z': 'online katalog'}, {'u': 'http://www.mzk.cz/katalogy-databaze/online-katalogy', 'z': 'online katalog'}, {'u': 'http://www.mzk.cz', 'z': 'web'}]
    MVS {'c': 'MVS vykonává zdarma. Knihovna je zapojena do MVS v SKC.', 'u': 'http://www.mzk.cz/pro-knihovny/meziknihovni-sluzby'}
    OTD {'1': '8:30-22:00', '2': '8:30-22:00', '3': '8:30-22:00', '4': '8:30-22:00', '5': '8:30-22:00', '6': '9:00-17:00'}
    SLU ['Samoobslužné vracení v době uzavření knihovny (bibliobox)', 'Půjčování čteček e-knih, tabletů', 'Meziknihovní výpůjční služby', 'Přístup k online licencovaným zdrojům z knihovny', 'Rešeršní služby', 'Wifi', 'Autentifikace MojeID', 'Kopírovací (reprografické) a tiskové služby', 'Dodávání dokumentů (dodávání digitálních kopií článků a částí knih)', 'Digitalizace dokumentů na přání (pouze dokumenty, které nejsou chráněny autorským právem)', 'Individuální studijní prostory (včetně možnosti objednání)', 'Týmová studovna', 'Poslechová místa pro hudbu a sledování videa z fondu knihovny', 'Pronájem prostor (např. učeben, sálů atd.)', 'Kavárna, bufet', 'Prostor pro přebalení a nakrmení malých dětí', 'Parkování na speciálních místech pro auta zdravotně handicapovaných návštěvníků knihovny', 'Ptejte se knihovny']
    PRK ['Česká digitální knihovna', 'eBook on Demand', 'Europeana', 'Knihovny.cz', 'Manuscriptorium', 'Souborný katalog ČR', 'Virtuální národní fonotéka', 'Virtuální polytechnická knihovna', 'Portál Historické fondy', 'Databáze národních autorit NK ČR', 'Jednotná informační brána', 'ObálkyKnih.cz']
    POI MZK je knihovnou s univerzálním knihovním fondem doplněným specializovanými fondy. Od roku 1935 má právo povinného výtisku. Od roku 2009 je výzkumnou organizací. Plní funkci krajské knihovny. Na území Jihomoravského kraje plní regionální funkce, koordinuje jejich plnění a vykonává další související činnosti.
    ADK http://www.mzk.cz//pro-knihovny/region/knihovny
    AKT 20170425
    FND {'k': '4239016 knih.jedn.', 'p': '5731 tit.periodik', 'r': '2017'}
    KNS Aleph
    DRL http://aleph.nkp.cz/F/?func=direct&doc_number=000000732&local_base=ADR
    """

    # Sigla je jednoznačný identifikátor knihovny přidělovaný min.
    # Knihovna může mít i více Sigla, např. po převzetí jiné knihovny
    # CPK API: SGL
    # Ex: SGL BOA001
    # obj.sigla == 'BOA001'
    # obj.id == 'boa001'
    form.mode(sigla='display')
    sigla = schema.TextLine(
        title=_(u'Sigla'),
        description=_(u'Primární identifikátor knihovny'),
        required=True,
    )

    # Název knihovny
    # CPK API: NAZ
    # Ex: NAZ Moravská zemská knihovna v Brně
    form.mode(fullname='display')
    fullname = schema.TextLine(
        title=_(u'Název knihovny'),
        description=_(u'Název knihovny se přebírá z ADR, nelze jej editovat.  Pokud je potřeba jej změnit, je potřeby to udělat přímo v ADR, podobně i pro jiné položky přebírané z ADR.'),
        required=True,
    )

    # Zkratka knihovny
    # Nepřebírat z CPK
    # CPK API: ZKR
    # Ex: ZKR MZK
    #write_permission(shortname='ziskej.operator')
    shortname = schema.TextLine(
        title=_(u'Zkratka knihovny'),
        description=_(u'Zkratka knihovny se používá v navigačních prvcích.  Editovat ji může jen operátor.  Pro aktivaci knihovny je nutné vyplnit.'),
        required=False,
        missing_value=u"",
    )

    is_cpk = schema.Bool(
        title=_(u'Je v CPK'),
        required=False,
        missing_value=False,
    )

    # Adresa knihovny
    # CPK API: ADR
    # Ex: ADR {'u': 'Kounicova 65a', 'c': '601 87', 'm': 'Brno', 'g': '49°12\'31.14"N, 16°35\'38.5"E'}
    form.mode(address='display')
    address = schema.TextLine(
        title=_(u'Adresa'),
        description=_(u'Adresa se přebírá z ADR.'),
        required=True,
    )

    # E-mail
    # CPK API: EML
    # Ex: EML: [{u: mzk@mzk.cz, z: knihovna}, {u: mvs@mzk.cz, z: mvs}, ...],
    form.mode(email='display')
    email = schema.TextLine(
        title=_(u'E-mail'),
        description=_(u'Prvotní naplnění je z ADR, následně se automaticky neaktualizuje.'),
        required=False,
    )

    # E-mail MVS
    # CPK API: EML
    # Ex: EML: [{u: mzk@mzk.cz, z: knihovna}, {u: mvs@mzk.cz, z: mvs}, ...],
    form.mode(email_mvs='display')
    email_mvs = schema.TextLine(
        title=_(u'E-mail MVS'),
        description=_(u'Prvotní naplnění je z ADR, následně se automaticky neaktualizuje.  E-mail nebo seznam e-mailů oddělených středníkem pro notifikace pro případ, kdy není upřesněno níže.'),
        required=False,
    )

    # E-mail MVS ŽK
    form.mode(email_mvs_zk='display')
    email_mvs_zk = schema.TextLine(
        title=_(u'E-mail MVS ŽK'),
        description=_(u'Přímo zadávaný e-mail nebo seznam e-mailů oddělených středníkem pro notifikace pro ŽK.'),
        required=False,
    )

    # E-mail MVS DK
    form.mode(email_mvs_dk='display')
    email_mvs_dk = schema.TextLine(
        title=_(u'E-mail MVS DK'),
        description=_(u'Přímo zadávaný e-mail nebo seznam e-mailů oddělených středníkem pro notifikace pro DK.'),
        required=False,
    )

    # E-mail EDD
    form.mode(email_edd='display')
    email_edd = schema.TextLine(
        title=_(u'E-mail EDD'),
        description=_(u'Přímo zadávaný e-mail nebo seznam e-mailů oddělených středníkem pro notifikace pro případ, kdy není upřesněno níže.'),
        required=False,
    )

    # E-mail EDD ŽK
    form.mode(email_edd_zk='display')
    email_edd_zk = schema.TextLine(
        title=_(u'E-mail EDD ŽK'),
        description=_(u'Přímo zadávaný e-mail nebo seznam e-mailů oddělených středníkem pro notifikace pro ŽK.'),
        required=False,
    )

    # E-mail EDD DK
    form.mode(email_edd_dk='display')
    email_edd_dk = schema.TextLine(
        title=_(u'E-mail EDD DK'),
        description=_(u'Přímo zadávaný e-mail nebo seznam e-mailů oddělených středníkem pro notifikace pro DK.'),
        required=False,
    )

    # Phone
    # CPK API: TEL
    # Ex: TEL: [541 646 111, 541 646 175 (MVS), ...]
    form.mode(phone='display')
    phone = schema.TextLine(
        title=_(u'Telefon'),
        description=_(u'Prvotní naplnění je z ADR, následně se automaticky neaktualizuje.'),
        required=False,
    )

    # Phone MVS
    form.mode(phone_mvs='display')
    phone_mvs = schema.TextLine(
        title=_(u'Telefon MVS'),
        description=_(u'Přímo zadávaný, následně se automaticky neaktualizuje.'),
        required=False,
    )

    # Phone EDD
    form.mode(phone_edd='display')
    phone_edd = schema.TextLine(
        title=_(u'Telefon EDD'),
        description=_(u'Přímo zadávaný, následně se automaticky neaktualizuje.'),
        required=False,
    )

    # URL
    # CPK API: URL
    # Ex: URL [{'u': 'https://vufind.mzk.cz/', 'z': 'online katalog'}, {'u': 'http://www.mzk.cz/katalogy-databaze/online-katalogy', 'z': 'online katalog'}, {'u': 'http://www.mzk.cz', 'z': 'web'}]
    form.mode(url='display')
    url = schema.URI(
        title=_(u'URL'),
        description=_(u'Webová adresa knihovny se přebírá z ADR.'),
        required=True,
        )

    # Fakturační údaje:
    # jen předvyplnit při prvním importu, pak už neaktualizovat
    # - Obchodní jméno (předvypnit z Název)
    # - Sídlo (předvypnit z Adresa)
    # - IČ (předvypnit)

    # Fakturační údaje: Obchodní jméno (předvypnit z Název)
    # CPK API: NAZ
    # Ex: NAZ Moravská zemská knihovna v Brně
    invoice_name = schema.TextLine(
        title=_(u'Fakturace - název'),
        description=_(u'Fakturační údaje - obchodní jméno.  Prvotní naplnění je z ADR, následně se automaticky neaktualizuje.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # Fakturační údaje: Sídlo (předvypnit z Adresa)
    # CPK API: ADR
    # Ex: ADR {'u': 'Kounicova 65a', 'c': '601 87', 'm': 'Brno', 'g': '49°12\'31.14"N, 16°35\'38.5"E'}
    invoice_address = schema.TextLine(
        title=_(u'Fakturace - adresa'),
        description=_(u'Fakturační údaje - sídlo.  Prvotní naplnění je z ADR, následně se automaticky neaktualizuje.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # Fakturační údaje: IČ (předvypnit)
    # CPK API: ICO
    # Ex: ICO {'a': '00094943', 'b': 'CZ00094943'}
    ico = schema.TextLine(
        title=_(u'Fakturace - IČ'),
        description=_(u'Fakturační údaje - IČ.  Prvotní naplnění je z ADR, následně se automaticky neaktualizuje.'),
        required=True,
    )

    # deeplink_base_url
    # CPK API: ?
    deeplink_base_url = schema.URI(
        title=_(u'Base URL katalogu knihovny'),
        description=_(u'Základ webové adresy katalogu knihovny.'),
        required=True,
        )

    # hrubý formát dat naimportovaný z CPK, needitovatelné
    form.mode(cpkdata='display')
    write_permission(cpkdata='cmf.ManagePortal')
    cpkdata = schema.Text(
        title=_(u'Import z CPK'),
        description=_(u'Toto je zobrazováno pouze administrátorům a je určeno pro ladění propojení s CPK.'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    # log hlavních změn objektu
    form.mode(historylog='display')
    write_permission(historylog='cmf.ManagePortal')
    historylog = schema.Text(
        title=_(u'History log'),
        description=_(u'Toto je zobrazováno pouze administrátorům a je určeno pro ladění.'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    # Provozní doba, json
    form.mode(opening_hours='display')
    write_permission(opening_hours='cmf.ManagePortal')
    opening_hours = schema.Text(
        title=_(u'Provozní doba, json'),
        description=_(u'Toto je zobrazováno pouze administrátorům a je určeno pro ladění.'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    # Dovolená, json
    form.mode(vacation='display')
    write_permission(vacation='cmf.ManagePortal')
    vacation = schema.Text(
        title=_(u'Dovolená, json'),
        description=_(u'Toto je zobrazováno pouze administrátorům a je určeno pro ladění.'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    # Whitelist, user input
    whitelist = schema.List(
        title=_(u'Whitelist DK'),
        description=_(u'Seznam preferovaných knihoven (jejich sigly) jako dožádané knihovny.  Každou knihovnu (siglu) na samostatný řádek.'),
        required=False,
        missing_value=[],
        default=[],
        value_type=schema.ASCIILine(),
    )

    # Blacklist, user input
    blacklist = schema.List(
        title=_(u'Blacklist DK'),
        description=_(u'Seznam blokovaných knihoven (jejich sigly) jako dožádané knihovny.  Každou knihovnu (siglu) na samostatný řádek.'),
        required=False,
        missing_value=[],
        default=[],
        value_type=schema.ASCIILine(),
    )

    # # Whitelist, json
    # form.mode(whitelist='display')
    # write_permission(whitelist='cmf.ManagePortal')
    # whitelist = schema.Text(
    #     title=_(u'Whitelist, json'),
    #     description=_(u'Toto je zobrazováno pouze administrátorům a je určeno pro ladění.'),
    #     required=False,
    #     default=u"",
    #     missing_value=u"",
    # )

    # # Blacklist, json
    # form.mode(blacklist='display')
    # write_permission(blacklist='cmf.ManagePortal')
    # blacklist = schema.Text(
    #     title=_(u'Blacklist, json'),
    #     description=_(u'Toto je zobrazováno pouze administrátorům a je určeno pro ladění.'),
    #     required=False,
    #     default=u"",
    #     missing_value=u"",
    # )

    # IdP EntityID
    #idp_entityid = schema.URI(
    idp_entityid = schema.TextLine(
        title=_(u'IdP EntityID'),
        description=_(u'Shibboleth - EntityID IdP knihovny.  Požadováno pro aktivaci.'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    reader_lid_label = schema.TextLine(
        title=_(u'Název pro Čtenář ID'),
        description=_(u'Název políčka pro identifikaci čtenáře - zobrazuje se jako nadpis pro políčko, kam si knihovník může zapsat interní identifikaci čtenáře, např. číslo průkazky čtenáře, username...'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    reader_lid_help = schema.TextLine(
        title=_(u'Nápověda pro čtenář ID'),
        description=_(u'Nápověda pro identifikaci čtenáře zobrazuje se jako text popisující co čtenář ID znamená, aby knihovník nebo čtenář věděli co tam napsat.'),
        required=False,
        default=u"",
        missing_value=u"",
    )

    # json
    form.mode(reader_lid_map='display')
    write_permission(reader_lid_map='cmf.ManagePortal')
    reader_lid_map = schema.Text(
        title=_(u'Mapování čtenářů na lid, json'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(mvs_zk_allowed='display')
    write_permission(mvs_zk_allowed='cmf.ManagePortal')
    mvs_zk_allowed = schema.Bool(
        title=_(u'Může být MVS ŽK'),
        required=False,
        missing_value=False,
    )

    form.mode(mvs_zk='display')
    write_permission(mvs_zk='cmf.ManagePortal')
    mvs_zk = schema.Bool(
            title=_(u'Chce být MVS ŽK'),
            required=False,
            missing_value=False,
    )

    form.mode(mvs_dk_allowed='display')
    write_permission(mvs_dk_allowed='cmf.ManagePortal')
    mvs_dk_allowed = schema.Bool(
            title=_(u'Může být MVS DK'),
            required=False,
            missing_value=False,
    )

    form.mode(mvs_dk='display')
    write_permission(mvs_dk='cmf.ManagePortal')
    mvs_dk = schema.Bool(
            title=_(u'Chce být MVS DK'),
            required=False,
            missing_value=False,
    )

    form.mode(mrs_zk_allowed='display')
    write_permission(mrs_zk_allowed='cmf.ManagePortal')
    mrs_zk_allowed = schema.Bool(
        title=_(u'Může být MRS ŽK'),
        required=False,
        missing_value=False,
    )

    form.mode(mrs_zk='display')
    write_permission(mrs_zk='cmf.ManagePortal')
    mrs_zk = schema.Bool(
            title=_(u'Chce být MRS ŽK'),
            required=False,
            missing_value=False,
    )

    form.mode(mrs_dk_allowed='display')
    write_permission(mrs_dk_allowed='cmf.ManagePortal')
    mrs_dk_allowed = schema.Bool(
            title=_(u'Může být MRS DK'),
            required=False,
            missing_value=False,
    )

    form.mode(mrs_dk='display')
    write_permission(mrs_dk='cmf.ManagePortal')
    mrs_dk = schema.Bool(
            title=_(u'Chce být MRS DK'),
            required=False,
            missing_value=False,
    )

    form.mode(edd_zk_allowed='display')
    write_permission(edd_zk_allowed='cmf.ManagePortal')
    edd_zk_allowed = schema.Bool(
        title=_(u'Může být EDD ŽK'),
        required=False,
        missing_value=False,
    )

    form.mode(edd_zk='display')
    write_permission(edd_zk='cmf.ManagePortal')
    edd_zk = schema.Bool(
            title=_(u'Chce být EDD ŽK'),
            required=False,
            missing_value=False,
    )

    form.mode(edd_dk_allowed='display')
    write_permission(edd_dk_allowed='cmf.ManagePortal')
    edd_dk_allowed = schema.Bool(
            title=_(u'Může být EDD DK'),
            required=False,
            missing_value=False,
    )

    form.mode(edd_dk='display')
    write_permission(edd_dk='cmf.ManagePortal')
    edd_dk = schema.Bool(
            title=_(u'Chce být EDD DK'),
            required=False,
            missing_value=False,
    )

    # --------------------------------------------------------------------------
    # FIELDSET fees
    model.fieldset(
        'fees',
        label=_(u"Poplatky"),
        fields=['mvs_zk_cena', 'mvs_zk_dk_cena', 'mvs_dk_cena', 'mvs_dk_cena_min', 'mvs_dk_cena_max', ]
    )

    # Cena MVS (přirážka) - Žádající knihovna
    mvs_zk_cena = schema.Int(
        title=_(u'Cena MVS ŽK'),
        description=_(u'Žádající knihovna fixní cena MVS (nevyplněno .. není, 0 .. zdarma)'),
        required=False,
        min=CENA_MIN,
        max=CENA_MAX,
    )

    # Cena MVS (neznámá DK) - Žádající knihovna
    mvs_zk_dk_cena = schema.Int(
        title=_(u'Cena MVS ŽK pro neznámou DK'),
        description=_(u'Žádající knihovna pro neznámou dožádanou knihovnu (pro odhady), nemá smysl, pokud je vyplněno Cena MVS ŽK'),
        required=False,
        min=CENA_MIN,
        max=CENA_MAX,
    )

    # Cena MVS - Dožadovaná knihovna
    mvs_dk_cena = schema.Int(
        title=_(u'Cena MVS DK'),
        description=_(u'Dožadovaná knihovna cena MVS (předpokládaná)'),
        required=False,
        min=CENA_MIN,
        max=CENA_MAX,
    )

    # Cena MVS - Dožadovaná knihovna
    mvs_dk_cena_min = schema.Int(
        title=_(u'Cena MVS DK min'),
        description=_(u'Dožadovaná knihovna cena MVS (minimální)'),
        required=False,
        min=CENA_MIN,
        max=CENA_MAX,
    )

    # Cena MVS - Dožadovaná knihovna
    mvs_dk_cena_max = schema.Int(
        title=_(u'Cena MVS DK max'),
        description=_(u'Dožadovaná knihovna cena MVS (maximální)'),
        required=False,
        min=CENA_MIN,
        max=CENA_MAX,
    )

    # Platby

    credit_ntk_user_id = schema.ASCIILine(
        title=_(u'NTK kredit id'),
        description=_(u'Přihlašovací jméno uživatele pro NTK kredit.'),
        required=False,
        missing_value='',
    )

    form.mode(credit_ntk='display')
    write_permission(credit_ntk='cmf.ManagePortal')
    credit_ntk = schema.Int(
        title=_(u'Použitý NTK kredit'),
        description=_(u'Kolik celkem zaplaceno z finančního konta'),
        required=True,
        default = 0,
    )

    form.mode(credit_ntk_todo='display')
    write_permission(credit_ntk_todo='cmf.ManagePortal')
    credit_ntk_todo = schema.Int(
        title=_(u'Použít NTK kredit'),
        description=_(u'Kolik celkem by mělo být ještě zaplaceno z finančního konta'),
        required=True,
        default = 0,
    )

    form.mode(credit_ziskej='display')
    write_permission(credit_ziskej='cmf.ManagePortal')
    credit_ziskej = schema.Int(
        title=_(u'Získaný kredit v Získej'),
        description=_(u'Kolik získáno v rámci Získej a zatím nefakturováno (pozor, může být i záporné díky reklamacím po fakturaci)'),
        required=True,
        default = 0,
    )

    form.mode(credit_ziskej_invoiced='display')
    write_permission(credit_ziskej_invoiced='cmf.ManagePortal')
    credit_ziskej_invoiced = schema.Int(
        title=_(u'Již vyfakturovaný kredit v Získej'),
        description=_(u'Kolik získáno v rámci Získej a už fakturováno'),
        required=True,
        default = 0,
    )

    # json
    form.mode(credit_log='display')
    write_permission(credit_log='cmf.ManagePortal')
    credit_log = schema.Text(
        title=_(u'Kompletní historie týkající se kreditu NTK nebo Získej, json'),
        required=True,
        default=u"",
    )

    # API AKS

    form.mode(apiaks_aks_secret='display')
    write_permission(apiaks_aks_secret='cmf.ManagePortal')
    apiaks_aks_secret = schema.ASCIILine(
        title=_(u'API AKS Secret'),
        required=False,
        default="",
        missing_value="",
    )

    form.mode(apiaks_allowed='display')
    write_permission(apiaks_allowed='cmf.ManagePortal')
    apiaks_allowed = schema.Bool(
        title=_(u'Může být API AKS'),
        required=False,
        missing_value=False,
    )

    form.mode(apiaks_enabled='display')
    write_permission(apiaks_enabled='cmf.ManagePortal')
    apiaks_enabled = schema.Bool(
            title=_(u'Chce být API AKS'),
            required=False,
            missing_value=False,
    )

    form.mode(callback_url_r_t='display')
    write_permission(callback_url_r_t='cmf.ManagePortal')
    callback_url_r_t = schema.ASCIILine(
        title=_(u'Callback URL'),
        description=_(u'Callback URL pro event_reader_ticket_changed.'),
        required=False,
        default="",
        missing_value="",
    )

    form.mode(callback_http_r_t='display')
    write_permission(callback_http_r_t='cmf.ManagePortal')
    callback_http_r_t = schema.ASCIILine(
        title=_(u'Callback HTTP'),
        description=_(u'Callback HTTP verb pro event_reader_ticket_changed.'),
        required=False,
        default="",
        missing_value="",
    )

    form.mode(callback_url_zk_st='display')
    write_permission(callback_url_zk_st='cmf.ManagePortal')
    callback_url_zk_st = schema.ASCIILine(
        title=_(u'Callback URL'),
        description=_(u'Callback URL pro event_zk_subticket_changed.'),
        required=False,
        default="",
        missing_value="",
    )

    form.mode(callback_http_zk_st='display')
    write_permission(callback_http_zk_st='cmf.ManagePortal')
    callback_http_zk_st = schema.ASCIILine(
        title=_(u'Callback HTTP'),
        description=_(u'Callback HTTP verb pro event_zk_subticket_changed.'),
        required=False,
        default="",
        missing_value="",
    )

    form.mode(callback_url_dk_st='display')
    write_permission(callback_url_dk_st='cmf.ManagePortal')
    callback_url_dk_st = schema.ASCIILine(
        title=_(u'Callback URL'),
        description=_(u'Callback URL pro event_dk_subticket_changed.'),
        required=False,
        default="",
        missing_value="",
    )

    form.mode(callback_http_dk_st='display')
    write_permission(callback_http_dk_st='cmf.ManagePortal')
    callback_http_dk_st = schema.ASCIILine(
        title=_(u'Callback HTTP'),
        description=_(u'Callback HTTP verb pro event_dk_subticket_changed.'),
        required=False,
        default="",
        missing_value="",
    )

class Library(Base):
    implements(ILibrary)

    def is_apiaks(self):
        if not APIAKS_ENABLED:
            return False
        return bool(self.apiaks_allowed and self.apiaks_enabled)

    def apiaks_generate_secret(self):
        if not APIAKS_ENABLED:
            return

        secret = str(uuid.uuid4())

        secret_filename = '{}-secret.txt'.format(self.sigla.lower())
        filepath = os.path.join(SECRETS_DIR, secret_filename)
        if not os.path.isdir(SECRETS_DIR):
            raise Exception('Invalid configuration for secrets.')
        with open(filepath, 'wb') as f:
            f.write(secret)

        self.apiaks_aks_secret = secret

    def apiaks_generate_token(self):
        if not APIAKS_ENABLED:
            return u''
        if not self.apiaks_aks_secret:
            return u''
        app_id = u'{}'.format(self.sigla)
        app_id = app_id.lower()
        now = datetime.utcnow()
        token_dict = dict(
            iss=app_id,
            iat=now,
            exp=now + timedelta(seconds=3600),
            app=app_id,
            )
        token = jwt.encode(
            token_dict, self.apiaks_aks_secret, algorithm='HS256')
        return token

    def SearchableText(self):
        return make_searchable_text(self, 'sigla', 'fullname', 'shortname')

    def Title(self, short=True):
        """Vrací název knihovny ve tvaru OLA001: VKOL, je vidět např.
        v seznamu objednávek z pohledu SC, kde se používá
        ticket.library_zk_title, které se nastavuje při vytvoření objednávky
        z library.Title().  Verze pro short=False není zatím použita nikde.
        """

        if short and self.shortname:
            value = u'{sigla}: {shortname}'.format(
                sigla=self.sigla,
                shortname=self.shortname,
                )
        elif self.shortname:
            value = u'{sigla}: {fullname} ({shortname})'.format(
                sigla=self.sigla,
                shortname=self.shortname,
                fullname=self.fullname,
                )
        else:
            value = u'{sigla}: {fullname}'.format(
                sigla=self.sigla,
                fullname=self.fullname,
                )
        return value

    def ShortName(self):
        if self.shortname:
            return self.shortname
        return self.fullname

    def Address(self):
        if self.address:
            return self.address
        return u''

    def can_be_published(self):
        errors = self.validate_for_activation()
        #print("errors: {errors}".format(errors=str(errors)))
        return not len(errors)

    def validate_for_activation(self):
        obj = self
        errors = []

        # idp_entityid mandatory
        IS_SHIBBOLETH_MANDATORY = False
        if IS_SHIBBOLETH_MANDATORY and not obj.idp_entityid:
            errors.append(
                dict(
                    message = 'IdP EntityID je pro aktivaci povinné - upravit je možné v editaci profilu knihovny.',
                    error_type = 'error',
                    )
                )

        # správce knihovny
        librarians = get_librarians_by_library(library=self, is_library_admin=True)
        if librarians is None:
            errors.append(
                dict(
                    message = 'Není nastaven žádný správce knihovny - přidejte správce knihovny nebo změňte oprávnění knihovníka, aby byl správcem knihovny.',
                    error_type = 'error',
                    )
                )

        #if errors:
        #    logger.info("library.validate_for_activation errors: {errors}".format(errors=str(errors)))
        return errors

    def add_historylog(self, value):
        self.historylog = format_historylog_entry(prev=self.historylog, value=value)

    @property
    def wf_review_state(self):
        """Vrátí workflow review_state."""
        wf_tool = api.portal.get_tool('portal_workflow')
        status = wf_tool.getStatusOf("library_workflow", self)
        review_state = status["review_state"]
        return review_state

    @property
    def is_active(self):
        return bool(self.wf_review_state in ('published', ))

    def notify_emails_by_service_role(self, service=None, role=None):
        if service == 'mvs':
            if role == 'zk':
                value = self.email_mvs_zk
            elif role == 'dk':
                value = self.email_mvs_dk
            else:
                raise Exception('Internal error')
            if value is None:
                value = u''
            value = value.strip()
            if not value:
                value = self.email_mvs
        elif service == 'mrs':
            if role == 'zk':
                value = self.email_mvs_zk
            elif role == 'dk':
                value = self.email_mvs_dk
            else:
                raise Exception('Internal error')
            if value is None:
                value = u''
            value = value.strip()
            if not value:
                value = self.email_mvs
        elif service == 'edd':
            if role == 'zk':
                value = self.email_edd_zk
            elif role == 'dk':
                value = self.email_edd_dk
            else:
                raise Exception('Internal error')
            if value is None:
                value = u''
            value = value.strip()
            if not value:
                value = self.email_edd
        else:
            raise Exception('Internal error')
        if not value:
            return []
        if isinstance(value, basestring):
            if ';' not in value:
                value = [value]
            else:
                value = [x.strip() for x in value.split(';') if x.strip()]
        elif isinstance(value, list):
            pass
        else:
            raise Exception('Internal error')
        items = []
        for item in value:
            item = item.strip()
            try:
                validate_email(item)
                items.append(item)
            except Invalid:
                continue
        return items

    def get_reader_lid(self, reader_id):
        data_json = self.reader_lid_map
        if data_json:
            try:
                data = json.loads(data_json)
            except:
                data = None
        else:
            data = None
        if data is None:
            data = dict()
        return data.get(reader_id, None)

    def set_reader_lid(self, reader_id, lid):
        data_json = self.reader_lid_map
        if data_json:
            try:
                data = json.loads(data_json)
            except:
                data = None
        else:
            data = None
        if data is None:
            data = dict()
        value = data.get('reader_id', None)
        if value == lid:
            return False
        if not lid:
            if 'reader_id' in data:
                del data[reader_id]
        else:
            data[reader_id] = lid
        data_json = json.dumps(data, ensure_ascii=False)
        self.reader_lid_map = data_json
        return True

    def Mvs_dk_cena(self):
        return 70
        # value = self.mvs_dk_cena
        # if value is None or not isinstance(value, int):
        #     return CENA_MAX  # 200
        # if value < 0:
        #     return 0
        # if value > CENA_MAX:
        #     return CENA_MAX
        # return value

    def Is_mvs_zk(self, ignore_vacation=True, now_dt=None):  # ignore_open_hour_min=True
        if DEBUG_VAC:
            print("-- Is_mvs_zk(ignore_vacation={})".format(ignore_vacation))
        # Není aktivní nebo není MVS ŽK
        if not (self.is_active and self.mvs_zk_allowed and self.mvs_zk):
            if DEBUG_VAC:
                print("False not active")
            return False

        if ignore_vacation:
            if DEBUG_VAC:
                print("True ignore_vacation")
            return True

        # Tato část se používá jen pro určení, zda čtenář může vytvořit
        # objednávku.  Tato část je závislá na aktuálním datu a čase, proto
        # nelze rozumně použít v catalogu ani cachovat.

        # Pokud by se mělo používat i jinde, pak lze přidat další přepínač
        # ignore_open_hour_min, zda vyžadovat minimálně hodinu do konce
        # provozní doby, aby den byl započítán.

        # Pokud má knihovna dnes i zítra dovolenou nebo je dnes méně než 1 h do
        # konce provozní doby a zítra začíná dovolená, tak deaktivovat pro
        # účely seznamu aktivních knihoven, který CPK používá pro určení, zda
        # lze vytvořit novou objednávku.

        # Samotná implementace převedena do is_able_to_react_in_1d a rozšířena
        # o provozní dobu, viz diskuze.

        return self.is_able_to_react_in_1d(now_dt=now_dt)

    def Is_mvs_dk(self):
        return bool(self.is_active and self.mvs_dk_allowed and self.mvs_dk)

    def Is_mvs(self, ignore_vacation=True, now_dt=None):
        return bool(self.Is_mvs_zk(
            ignore_vacation=ignore_vacation, now_dt=now_dt) or
            self.Is_mvs_dk())

    def Is_mrs_zk(self):
        return bool(self.is_active and self.mrs_zk_allowed and self.mrs_zk)

    def Is_mrs_dk(self):
        return bool(self.is_active and self.mrs_dk_allowed and self.mrs_dk)

    def Is_mrs(self, ignore_vacation=True, now_dt=None):
        return bool(self.Is_mrs_zk() or self.Is_mrs_dk())

    def Is_edd_zk(self):
        return bool(self.is_active and self.edd_zk_allowed and self.edd_zk)

    def Is_edd_dk(self):
        return bool(self.is_active and self.edd_dk_allowed and self.edd_dk)

    def Is_edd(self, ignore_vacation=True, now_dt=None):
        return bool(self.Is_edd_zk() or self.Is_edd_dk())

    def Is_any_zk(self, ignore_vacation=True, now_dt=None):
        if self.Is_mvs_zk(ignore_vacation=ignore_vacation, now_dt=now_dt):
            return True
        if self.Is_mrs_zk():
            return True
        if self.Is_edd_zk():
            return True
        return False

    def Was_mvs_zk(self):
        if self.Is_mvs_zk():
            return True
        return bool(len(list_tickets_by_sigla(self.sigla, 'mvs')))

    def Was_mvs_dk(self):
        if self.Is_mvs_dk():
            return True
        return bool(len(list_subtickets_by_sigla(self.sigla, 'mvs')))

    def Was_mvs(self):
        return bool(self.Was_mvs_zk() or self.Was_mvs_dk())

    def Was_edd_zk(self):
        if self.Is_edd_zk():
            return True
        return bool(len(list_tickets_by_sigla(self.sigla, 'edd')))

    def Was_edd_dk(self):
        if self.Is_edd_dk():
            return True
        return bool(len(list_subtickets_by_sigla(self.sigla, 'edd')))

    def Was_edd(self):
        return bool(self.Was_edd_zk() or self.Was_edd_dk())

    def Was_any_zk(self):
        return self.Was_mvs_zk() or self.Was_edd_zk()

    # Library credit

    # Library credit: amount

    @property
    def Credit_ntk(self):
        if not self.credit_ntk:
            return 0
        if not isinstance(self.credit_ntk, int):
            logger.warning('Invalid credit_ntk for {sigla}: {value}'.format(
                sigla = self.sigla,
                value = repr(self.credit_ntk),
                ))
            return 0
        return int(self.credit_ntk)

    @property
    def Credit_ntk_todo(self):
        if not self.credit_ntk_todo:
            return 0
        if not isinstance(self.credit_ntk_todo, int):
            logger.warning('Invalid credit_ntk_todo for {sigla}: {value}'.format(
                sigla = self.sigla,
                value = repr(self.credit_ntk_todo),
                ))
            return 0
        return int(self.credit_ntk_todo)

    @property
    def Credit_ntk_total(self):
        return self.Credit_ntk + self.Credit_ntk_todo

    @property
    def Credit_ziskej(self):
        if not self.credit_ziskej:
            return 0
        if not isinstance(self.credit_ziskej, int):
            logger.warning('Invalid credit_ziskej for {sigla}: {value}'.format(
                sigla = self.sigla,
                value = repr(self.credit_ziskej),
                ))
            return 0
        return int(self.credit_ziskej)

    @property
    def Credit_ziskej_invoiced(self):
        if not self.credit_ziskej_invoiced:
            return 0
        if not isinstance(self.credit_ziskej_invoiced, int):
            logger.warning('Invalid credit_ziskej_invoiced for {sigla}: {value}'.format(
                sigla = self.sigla,
                value = repr(self.credit_ziskej_invoiced),
                ))
            return 0
        return int(self.credit_ziskej_invoiced)

    @property
    def Credit_ziskej_available(self):
        return self.Credit_ziskej - self.Credit_ziskej_invoiced

    # Library credit: events

    def Credit_ntk_user_id(self):
        value = self.credit_ntk_user_id
        if not value:
            logger.warning('Missing credit_ntk_user_id for {}'.format(self.sigla))
            #return u'test'
            return None
        try:
            value = str(value)
        except:
            logger.warning('Invalid credit_ntk_user_id for {}'.format(self.sigla))
            return None
        return value

    def paysys_credit_info(self):
        # NTK nepoužívá kredit v NTK
        if self.sigla == 'ABA013':
            return None
        ntk_user_id = self.Credit_ntk_user_id()
        if ntk_user_id:
            value = credit_info(ntk_user_id)
        else:
            value = None
        return value

    # def credit_ziskej_mvs_reader_fee(self, ticket, action_type=u'std'):
    #     """Čtenář zaplatil pomocí Platebátoru, připsat ŽK do kreditu v rámci
    #     Získej.
    #     """

    #     # změnit credit_ziskej
    #     hid = ticket.hid
    #     credit_ziskej = self.Credit_ziskej
    #     # amount musí být vždy 70, ale pro testování může být nastaveno na 1
    #     amount = ticket.payment_amount
    #     credit_ziskej += amount
    #     self.credit_ziskej = credit_ziskej
    #     self._p_changed = True
    #     logger.info('{} credit_ziskej changed {} / {} (credit_ziskej_mvs_reader_fee) for {}'.format(self.sigla, amount, credit_ziskej, hid))
    #     ITEM_TYPE = dict(
    #         std = u'mvs_reader_fee_std',
    #         )
    #     item_type = ITEM_TYPE.get(action_type, action_type)

    #     # credit_ziskej log
    #     # SUGGESTION přidat i payment_service_id, payment_created_date
    #     # a payment_paid_date?
    #     item_log = dict(
    #         version = 1,
    #         item_category = u'mvs_reader_fee',
    #         item_type = item_type,
    #         hid = hid,
    #         amount = amount,
    #         credit_ziskej = credit_ziskej,
    #         payment_id = ticket.payment_id,
    #         platebator_id = ticket.payment_platebator_uuid,
    #         )
    #     self.credit_log_add(item_log)

    def mvs_payment_credit_ziskej(self, subticket, report_entry, payment_id,
                                  timestamp, action_type=u'std',
                                  is_negative=False, payment_timestamp=None):
        """připsat kredit v Získej.

        action_type=u'std'

        Proběhlo odeslání z DK do ŽK.  Došlo k vytvoření report_entry.  Nyní je
        potřeba připsat kredit v rámci Získej (credit_ziskej) pro DK.

        action_type=u'cancel'
        action_type=u'complaint'
        action_type=u'complaint_after_objection'

        Proběhla příslušná akce: malé storno po odeslání z DK do ČK, uznaná
        reklamace, reklamace uznaná po odvolání.  Došlo k vytvoření opravného
        report_entry.  Nyní je potřeba připsat kredit v rámci Získej
        (credit_ziskej) pro ŽK.

        Nově se credit Získej používá nejen pro připsání kreditu v Získej, ale i
        pro zaplacení z kreditu v Získej a to pro mvs payment rollback, který
        nastane při malém storno po odeslání nebo uznané reklamaci.  V takových
        případech je nastaveno is_negative na True a částka se použije záporná.
        """

        # změnit credit_ziskej
        hid = subticket.hid
        credit_ziskej = self.Credit_ziskej
        # amount musí být vždy 70, ale pro testování může být nastaveno na 1
        # report_entry.cena je pro storno a reklamaci záporná, ale pro platby
        # nás zajímá absolutní hodnota a opačného směru je docíleno nikoli
        # zápornou částkou, ale záměnou knihoven library_paysys / library_credit
        amount = abs(report_entry.cena)
        if is_negative:
            amount = -amount
        credit_ziskej += amount
        self.credit_ziskej = credit_ziskej
        self._p_changed = True
        logger.info('{} {} credit_ziskej changed {} / {} (mvs_payment_credit_ziskej) for {}'.format(self.sigla, action_type, amount, credit_ziskej, hid))

        # item_category .. hodnota u'mvs_dk' znamená, že původní poplatek je
        #                  v rámci platby MVS dožádané knihovně

        # item_type .. každý typ záznamu má vlastní item_type, ten ale není
        #              totožný s hodnotou parametru action_type, je proto nutné
        #              transformovat, každopádně je snaha udržet v logu
        #              item_type jednoznačné
        # transformace parametru na verzi pro záznam do logu
        if not is_negative:
            ITEM_TYPE = dict(
                std = u'mvs_dk_std',
                cancel = u'mvs_zk_cancel',
                complaint = u'mvs_zk_complaint',
                complaint_after_objection = u'mvs_zk_complaint_after_objection',
                )
        else:
            ITEM_TYPE = dict(
                cancel = u'mvs_dk_cancel_rollback',
                complaint = u'mvs_dk_complaint_rollback',
                complaint_after_objection = u'mvs_dk_complaint_after_objection_rollback',
                mvs_dk_fix_259 = u'mvs_dk_fix_259',
                )
        item_type = ITEM_TYPE.get(action_type, action_type)

        if payment_timestamp is None:
            # může se mírně lišit od automatického timestampu, jen pro manuální
            # kontrolu
            payment_timestamp = timestamp

        # credit_ziskej log
        item_log = dict(
            version = 1,
            item_category = u'mvs_dk',  # see above
            item_type = item_type,  # u'mvs_dk_std',
            hid = hid,
            amount = amount,
            credit_ziskej = credit_ziskej,
            report_entry_id = report_entry.getId(),
            report_entry_type = report_entry.entry_type,  # tady vždy std
            payment_id = payment_id,  # jen pro kontrolu a spárování s platbou z NTK kreditu ŽK
            payment_timestamp = payment_timestamp,
            )
        self.credit_log_add(item_log)

    def credit_ziskej_invoice_add(self, invoice_amount, invoice_id):
        """Operátor zanesl fakturu od knihovny na vybrání kreditu nebo jeho
        části.
        """

        # změnit credit_ziskej_invoiced
        credit_ziskej_invoiced = self.Credit_ziskej_invoiced
        amount = invoice_amount
        credit_ziskej_invoiced += amount
        self.credit_ziskej_invoiced = credit_ziskej_invoiced
        self._p_changed = True
        logger.info('{} credit_ziskej_invoiced changed {} / {} (credit_ziskej_invoice_add)'.format(self.sigla, amount, credit_ziskej_invoiced))

        # credit_ziskej log
        item_log = dict(
            version = 1,
            item_category = u'invoice_credit_ziskej_invoiced',
            item_type = u'invoice_std',
            amount = amount,
            credit_ziskej_invoiced = credit_ziskej_invoiced,
            invoice_id = invoice_id,
            )
        self.credit_log_add(item_log)

    def mvs_payment_paysys(self, subticket, report_entry, payment_id, timestamp, action_type=u'std'):
        """Zapsat platbu pomocí Paysys (NTK kredit).

        action_type=u'std'

        Proběhlo odeslání z DK do ŽK.  Došlo k vytvoření report_entry.  Podařilo
        se zaplatit pomocí NTK kreditu.  Nyní je potřeba zapsat v rámci Získej
        pro ŽK.

        action_type=u'cancel'
        action_type=u'complaint'
        action_type=u'complaint_after_objection'

        Proběhla příslušná akce: malé storno po odeslání z DK do ČK, uznaná
        reklamace, reklamace uznaná po odvolání.  Došlo k vytvoření opravného
        report_entry.  Podařilo se zaplatit pomocí NTK kreditu.  Nyní je potřeba
        zapsat v rámci Získej pro DK.
        """

        # změnit credit
        hid = subticket.hid
        credit_ntk = self.Credit_ntk
        # amount musí být vždy 70, ale pro testování může být nastaveno na 1
        # report_entry.cena je pro storno a reklamaci záporná, ale pro platby
        # nás zajímá absolutní hodnota a opačného směru je docíleno nikoli
        # zápornou částkou, ale záměnou knihoven library_paysys / library_credit
        amount = abs(report_entry.cena)
        credit_ntk += amount
        self.credit_ntk = credit_ntk
        self._p_changed = True
        logger.info('{} {} credit_ntk changed {} / {} (mvs_payment_paysys) for {}'.format(self.sigla, action_type, amount, credit_ntk, hid))

        # item_category .. hodnota u'mvs_zk_ntk' znamená, že původní poplatek je
        #                  v rámci platby MVS strhnut žádající knihovně užitím
        #                  Paysys (NTK kredit)

        # item_type .. každý typ záznamu má vlastní item_type, ten ale není
        #              totožný s hodnotou parametru action_type, je proto nutné
        #              transformovat, každopádně je snaha udržet v logu
        #              item_type jednoznačné
        # transformace parametru na verzi pro záznam do logu
        ITEM_TYPE = dict(
            std = u'mvs_zk_ntk_std',
            std_clearing = u'mvs_zk_ntk_std_clearing',
            cancel = u'mvs_dk_cancel',
            complaint = u'mvs_dk_complaint',
            complaint_after_objection = u'mvs_dk_complaint_after_objection',
            )
        item_type = ITEM_TYPE.get(action_type, action_type)

        # credit log
        item_log = dict(
            version = 1,
            item_category = u'mvs_zk_ntk',  # see above
            item_type = item_type,  # u'mvs_zk_ntk_std',
            hid = hid,
            amount = amount,
            credit_ntk = credit_ntk,
            report_entry_id = report_entry.getId(),
            report_entry_type = report_entry.entry_type,  # tady vždy std
            payment_id = payment_id,  # jen pro kontrolu a spárování s platbou z NTK kreditu ŽK
            payment_timestamp = timestamp,  # může se mírně lišit od automatického timestampu, jen pro manuální kontrolu
            )
        self.credit_log_add(item_log)

    def mvs_payment_paysys_todo(self,
                                subticket,
                                report_entry,
                                payment_id,
                                timestamp,
                                action_type=u'std',
                                message=u''):
        """Zapsat platbu pomocí Paysys (NTK kredit), která nebyla provedena.

        action_type=u'std'

        Proběhlo odeslání z DK do ŽK.  Došlo k vytvoření report_entry.
        Nepodařilo se zaplatit pomocí NTK kreditu.  Nyní je potřeba zapsat
        v rámci Získej pro ŽK.

        action_type=u'cancel'
        action_type=u'complaint'
        action_type=u'complaint_after_objection'

        Proběhla příslušná akce: malé storno po odeslání z DK do ČK, uznaná
        reklamace, reklamace uznaná po odvolání.  Došlo k vytvoření opravného
        report_entry.  Nepodařilo se zaplatit pomocí NTK kreditu.  Nyní je
        potřeba zapsat v rámci Získej pro DK.
        """

        # změnit credit
        hid = subticket.hid
        credit_ntk_todo = self.Credit_ntk_todo
        # amount musí být vždy 70, ale pro testování může být nastaveno na 1
        # report_entry.cena je pro storno a reklamaci záporná, ale pro platby
        # nás zajímá absolutní hodnota a opačného směru je docíleno nikoli
        # zápornou částkou, ale záměnou knihoven library_paysys / library_credit
        amount = abs(report_entry.cena)
        # výjimkou je clearing, kdy tady naopak cena tady musí být záporná
        if action_type == u'std_clearing':
            amount = - amount
        credit_ntk_todo += amount
        self.credit_ntk_todo = credit_ntk_todo
        self._p_changed = True
        logger.info('{} {} credit_ntk_todo changed {} / {} (mvs_payment_paysys_todo) for {}'.format(self.sigla, action_type, amount, credit_ntk_todo, hid))

        # item_category .. hodnota u'mvs_zk_ntk_todo' znamená, že původní
        #                  poplatek měl být v rámci platby MVS strhnut žádající
        #                  knihovně užitím Paysys (NTK kredit), ale nebyl, byl
        #                  proto zaznamenán zvlášť

        # item_type .. každý typ záznamu má vlastní item_type, ten ale není
        #              totožný s hodnotou parametru action_type, je proto nutné
        #              transformovat, každopádně je snaha udržet v logu
        #              item_type jednoznačné
        # transformace parametru na verzi pro záznam do logu
        ITEM_TYPE = dict(
            std = u'mvs_zk_ntk_todo_std',
            std_clearing = u'mvs_zk_ntk_todo_std_clearing',
            cancel = u'mvs_dk_todo_cancel',
            complaint = u'mvs_dk_todo_complaint',
            complaint_after_objection = u'mvs_dk_todo_complaint_after_objection',
            )
        item_type = ITEM_TYPE.get(action_type, action_type)

        # credit log
        item_log = dict(
            version = 1,
            item_category = u'mvs_zk_ntk_todo',  # see above
            item_type = item_type,  # u'mvs_zk_ntk_todo_std',
            hid = hid,
            amount = amount,
            credit_ntk_todo = credit_ntk_todo,
            report_entry_id = report_entry.getId(),
            report_entry_type = report_entry.entry_type,  # tady vždy std
            payment_id = payment_id,  # jen pro kontrolu a spárování s platbou z NTK kreditu ŽK
            payment_timestamp = timestamp,  # může se mírně lišit od automatického timestampu, jen pro manuální kontrolu
            message = message,
            )
        self.credit_log_add(item_log)

    # Library credit: log

    def credit_log_add(self, item):
        """Přidej záznam do logu změn credit_ziskej.

        item .. dict() s následujícícmi hodnotami
            version .. 1, int
            item_category .. 'mvs_reader_fee', 'mvs_dk'
            item_type for item_category mvs_reader_fee:
                'mvs_reader_fee_std' .. standardní platba čtenáře pomocí
                                        platebátoru připsaná na credit_ziskej
            item_type for item_category mvs_dk:
                'mvs_dk_std' .. standardní platba ŽK pro DK zaplacená z NTK
                                kreditu ŽK a připsaná na credit_ziskej
            amount .. 70, int
            credit_ziskej .. credit_ziskej po přičtení amount, int
            timestamp .. nemusí být součástí vstupního parametru item a v každém
                         případě je nastaven až zde
        """

        timestamp = datetime.now().isoformat()[:19].replace('T', ' ')
        item['timestamp'] = timestamp
        self._credit_log_add_base(item, timestamp)
        return timestamp

    def credit_log_data(self, category):
        data = self._get_credit_log()
        if category is None:
            return data['items']
        return [x for x in data['items'] if x['item_category'] == category]

    def credit_log_item_parsed(self, item):
        description = u''
        url = None
        ticket = None
        subticket = None

        # pro vše kromě item_category mvs_reader_fee máme report_entry_id a tím
        # pádem subticket - pro určení subticketu nestačí hid
        report_entry_id = item.get('report_entry_id', u'')
        if report_entry_id:
            subticket = get_subticket_by_report_entry(report_entry_id=report_entry_id)
        if subticket:
            url = u'{}?report_entry_id={}'.format(subticket.absolute_url(), report_entry_id)

        if item['item_category'] == u'mvs_reader_fee':
            # item_log = dict(
            #     version = 1,
            #     item_category = u'mvs_reader_fee',
            #     item_type = u'mvs_reader_fee_std',
            #     amount = amount,
            #     credit_ziskej = credit_ziskej,
            #     payment_id = ticket.payment_id,
            #     platebator_id = ticket.payment_platebator_uuid,
            #     )

            # týká se vždy ticketu a tam stačí znát hid
            hid = item.get('hid', u'')
            if hid:
                ticket = get_ticket_by_hid(hid)
            if ticket:
                url = u'{}'.format(ticket.absolute_url())

            if item['item_type'] == u'mvs_reader_fee_std':
                description = u'Připsána platba od čtenáře.  <span class="formHelp">Detaily: {payment_id} prostřednictvím platebátoru (uuid {platebator_id}), celkový kredit knihovny je nově {credit_ziskej} Kč</span>'.format(
                    payment_id = item.get('payment_id', None),
                    platebator_id = item.get('platebator_id', None),
                    credit_ziskej = item.get('credit_ziskej', None),
                    )
            result = dict(
                hid = item.get('hid', u''),
                url = url,
                amount = u'{} Kč'.format(item['amount']),
                total = u'{} Kč'.format(item['credit_ziskej']),
                timestamp = u'{}'.format(item['timestamp']),
                title = u'Kredit v ZÍSKEJ',
                description = description,
                )

        elif item['item_category'] == u'mvs_dk':
            # item_log = dict(
            #     version = 1,
            #     item_category = u'mvs_dk',
            #     item_type = u'mvs_dk_std',
            #     amount = amount,
            #     credit_ziskej = credit_ziskej,
            #     report_entry_id = report_entry.getId(),
            #     report_entry_type = report_entry.entry_type,  # tady vždy std
            #     payment_id = payment_id,  # jen pro kontrolu a spárování s platbou z NTK kreditu ŽK
            #     payment_timestamp = timestamp,  # může se mírně lišit od automatického timestampu, jen pro manuální kontrolu
            #     )
            ITEM_DESC = dict(
                mvs_dk_std = u'MVS - připsána platba od ŽK',
                mvs_zk_cancel = u'MVS - vrácena platba DK z důvodu akceptované žádosti o storno požadavku',
                mvs_zk_complaint = u'MVS - vrácena platba DK z důvodu akceptované reklamace požadavku',
                mvs_zk_complaint_after_objection = u'MVS - vrácena platba DK z důvodu reklamace požadavku akceptovaná Servisním centrem ZÍSKEJ',
                mvs_dk_cancel_rollback = u'MVS - strhnuta vrácena platba DK z důvodu akceptované žádosti o storno požadavku',
                mvs_dk_complaint_rollback = u'MVS - strhnuta vrácena platba DK z důvodu akceptované reklamace požadavku',
                mvs_dk_complaint_after_objection_rollback = u'MVS - strhnuta vrácena platba DK z důvodu reklamace požadavku akceptovaná Servisním centrem ZÍSKEJ',
                mvs_dk_fix_259 = u'MVS - anulováno připsání platby od ŽK, opravný prostředek SC',
                )
            item_desc = ITEM_DESC.get(item['item_type'], item['item_type'])

            description = u'{item_desc}.  <span class="formHelp">Detaily: platba {payment_id} pro report_entry {report_entry_id}, celkový kredit knihovny je nově {credit_ziskej} Kč</span>'.format(
                item_desc = item_desc,
                payment_id = item.get('payment_id', None),
                report_entry_id = item.get('report_entry_id', None),
                credit_ziskej = item.get('credit_ziskej', None),
                )
            result = dict(
                hid = item.get('hid', u''),
                url = url,
                amount = u'{} Kč'.format(item['amount']),
                total = u'{} Kč'.format(item['credit_ziskej']),
                timestamp = u'{}'.format(item['timestamp']),
                title = u'Kredit v ZÍSKEJ',
                description = description,
                )

        elif item['item_category'] == u'invoice_credit_ziskej_invoiced':
            # item_log = dict(
            #     version = 1,
            #     item_category = u'invoice_credit_ziskej_invoiced',
            #     item_type = u'invoice_std',
            #     amount = amount,
            #     credit_ziskej_invoiced = credit_ziskej_invoiced,
            #     invoice_id = invoice_id,
            #     )
            if item['item_type'] == u'invoice_std':
                description = u'Zaúčtovaná faktura {invoice_id} od knihovny.  <span class="formHelp">Detaily: celková fakturace kreditu knihovny je nově {credit_ziskej_invoiced} Kč</span>'.format(
                    invoice_id = item.get('invoice_id', None),
                    credit_ziskej_invoiced = item.get('credit_ziskej_invoiced', None),
                    )
            result = dict(
                hid = item.get('hid', u''),
                url = url,
                amount = u'{} Kč'.format(item['amount']),
                total = u'{} Kč'.format(item['credit_ziskej_invoiced']),
                timestamp = u'{}'.format(item['timestamp']),
                title = u'Fakturovaný kredit v ZÍSKEJ',
                description = description,
                )

        elif item['item_category'] == u'mvs_zk_ntk':
            # item_log = dict(
            #     version = 1,
            #     item_category = u'mvs_zk_ntk',
            #     item_type = u'mvs_zk_ntk_std',
            #     amount = amount,
            #     credit_ntk = credit_ntk,
            #     report_entry_id = report_entry.getId(),
            #     report_entry_type = report_entry.entry_type,  # tady vždy std
            #     payment_id = payment_id,  # jen pro kontrolu a spárování s platbou z NTK kreditu ŽK
            #     payment_timestamp = timestamp,  # může se mírně lišit od automatického timestampu, jen pro manuální kontrolu
            #     )
            ITEM_DESC = dict(
                mvs_zk_ntk_std = u'MVS - poplatek pro DK zaplacen z finančního konta',
                mvs_zk_ntk_std_clearing = u'MVS - poplatek pro DK zaplacen z finančního konta (dodatečně)',
                mvs_dk_cancel = u'MVS - vrácení poplatku pro DK zaplaceno z finančního konta z důvodu akceptované žádosti o storno požadavku',
                mvs_dk_complaint = u'MVS - vrácení poplatku pro DK zaplaceno z finančního konta z důvodu akceptované reklamace požadavku',
                mvs_dk_complaint_after_objection = u'MVS - vrácení poplatku pro DK zaplaceno z finančního konta z důvodu reklamace požadavku akceptovaná Servisním centrem ZÍSKEJ',
                )
            item_desc = ITEM_DESC.get(item['item_type'], item['item_type'])

            description = u'{item_desc}.  <span class="formHelp">Detaily: platba {payment_id} pro report_entry {report_entry_id}, celkově čerpáno z finančního konta -{credit_ntk} Kč</span>'.format(
                item_desc = item_desc,
                payment_id = item.get('payment_id', None),
                report_entry_id = item.get('report_entry_id', None),
                credit_ntk = item.get('credit_ntk', None),
                )
            result = dict(
                hid = item.get('hid', u''),
                url = url,
                amount = u'-{} Kč'.format(item['amount']),
                total = u'-{} Kč'.format(item['credit_ntk']),
                timestamp = u'{}'.format(item['timestamp']),
                title = u'Čerpáno z finančního konta',
                description = description,
                )

        elif item['item_category'] == u'mvs_zk_ntk_todo':
            # item_log = dict(
            #     version = 1,
            #     item_category = u'mvs_zk_ntk_todo',
            #     item_type = u'mvs_zk_ntk_todo_std',
            #     amount = amount,
            #     credit_ntk_todo = credit_ntk_todo,
            #     report_entry_id = report_entry.getId(),
            #     report_entry_type = report_entry.entry_type,  # tady vždy std
            #     payment_id = payment_id,  # jen pro kontrolu a spárování s platbou z NTK kreditu ŽK
            #     payment_timestamp = timestamp,  # může se mírně lišit od automatického timestampu, jen pro manuální kontrolu
            #     )
            ITEM_DESC = dict(
                mvs_zk_ntk_todo_std = u'MVS - poplatek pro DK měl být zaplacen (ale nebyl) z finančního konta',
                mvs_zk_ntk_todo_std_clearing = u'MVS - poplatek pro DK měl být zaplacen (ale nebyl) z finančního konta, nyní ale byl dodatečně zaplacen',
                mvs_dk_todo_cancel = u'MVS - vrácení poplatku pro DK mělo být zaplaceno (ale nebylo) z finančního konta z důvodu akceptované žádosti o storno požadavku',
                mvs_dk_todo_complaint = u'MVS - vrácení poplatku pro DK mělo být zaplaceno (ale nebylo) z finančního konta z důvodu akceptované reklamace požadavku',
                mvs_dk_todo_complaint_after_objection = u'MVS - vrácení poplatku pro DK mělo být zaplaceno (ale nebylo) z finančního konta z důvodu reklamace požadavku akceptovaná Servisním centrem ZÍSKEJ',
                )

            if item.get('hid', u'') and \
                    item.get('hid', u'') in (100262, 100259, ) and \
                    item['item_type'] == 'mvs_zk_ntk_todo_std_clearing':
                # NTK-277
                item_desc = u'MVS - poplatek pro DK měl být zaplacen (ale nebyl) z finančního konta, nyní ale byl dodatečně zaplacen, opravný prostředek SC'
            else:
                item_desc = ITEM_DESC.get(item['item_type'], item['item_type'])

            description = u'{item_desc}.  <span class="formHelp">Detaily: platba {payment_id} pro report_entry {report_entry_id}, celkově mělo být čerpáno z finančního konta a nebylo -{credit_ntk_todo} Kč</span>'.format(
                item_desc = item_desc,
                payment_id = item.get('payment_id', None),
                report_entry_id = item.get('report_entry_id', None),
                credit_ntk_todo = item.get('credit_ntk_todo', None),
                )
            if item['item_type'] != u'mvs_zk_ntk_todo_std_clearing':
                amount = u'-{} Kč'.format(item['amount'])
            else:
                amount = u'{} Kč'.format(-item['amount'])
            if item['credit_ntk_todo']:
                total = u'-{} Kč'.format(item['credit_ntk_todo'])
            else:
                total = u'{} Kč'.format(item['credit_ntk_todo'])
            result = dict(
                hid = item.get('hid', u''),
                url = url,
                amount = amount,
                total = total,
                timestamp = u'{}'.format(item['timestamp']),
                title = u'Mělo být čerpáno z finančního konta a nebylo',
                description = description,
                message = item.get('message', u''),
                )

        else:
            result = dict(
                hid = item.get('hid', u''),
                url = url,
                amount = u'{} Kč'.format(item.get('amount', None)),
                total = u'?',
                timestamp = u'{}'.format(item.get('timestamp', None)),
                title = item['item_category'],
                description = item['item_type'],
                )

        return result

    # def credit_log_add(self, entry_type, who, amount, text, reportentry_id, timestamp=None, details=None):
    #     """Jenom pro MVS ŽK- DK, ne pro Reader - ŽK."""
    #     if timestamp is None:
    #         timestamp = datetime.now().isoformat()[:19].replace('T', ' ')
    #     item = dict(
    #         version = 1,
    #         log_type = u'mvs-zk-dk',
    #         entry_type = entry_type,
    #         who = who,
    #         amount = amount,
    #         timestamp = timestamp,
    #         text = text,
    #         reportentry_id = reportentry_id,
    #         details = details,
    #         )
    #     return self._credit_log_add_base(item, timestamp)

    # def credit_log_reader_add(self, who, amount, text, reportentry_id, timestamp=None, details=None):
    #     """Jenom pro Reader - ŽK."""
    #     if timestamp is None:
    #         timestamp = datetime.now().isoformat()[:19].replace('T', ' ')
    #     item = dict(
    #         version = 1,
    #         log_type = u'mvs-r-zk',
    #         entry_type = entry_type,
    #         who = u'reader_by_ziskej_api_for_cpk',
    #         amount = amount,
    #         timestamp = timestamp,
    #         text = text,
    #         details = details,
    #         )
    #     return self._credit_log_add_base(item, timestamp)

    def _get_credit_log(self):
        data_json = self.credit_log
        if data_json:
            try:
                data = json.loads(data_json)
            except:
                data = None
        else:
            data = None
        if data is None:
            timestamp = datetime.now().isoformat()[:19].replace('T', ' ')
            data = dict(
                version = 1,
                modified = timestamp,
                items = [],
                )
        return data

    def _set_credit_log(self, data):
        data_json = json.dumps(data, ensure_ascii=False)
        self.credit_log = data_json

    def _credit_log_add_base(self, item, timestamp):
        data = self._get_credit_log()
        data['items'].append(item)
        data['modified'] = timestamp
        self._set_credit_log(data)

    # -------------------------------------------------------------
    # Provozní doba a dovolená, vliv na AutoLB a na nabídku služeb.
    # -------------------------------------------------------------

    def Vacation_info(self, is_html=False):
        """Vrátí text s informací o dovolené, použitelné pro seznam knihoven
        atp.
        """

        default = u''
        # Dovolená, start 0:00, end 23:59, tz naive
        vacation_start_dt, vacation_end_dt = self._parse_vacation()
        if not vacation_start_dt or not vacation_end_dt:
            return default
        value = u'{} - {}'.format(
            format_normalized_dt(vacation_start_dt, is_html=is_html),
            format_normalized_dt(vacation_end_dt, is_html=is_html),
            )
        if DEBUG_VAC:
            print("Vacation_info for", self.sigla, ":", value)
        return value

    def is_able_to_react_in_1d(self, now_dt=None):
        """Vrátí True, právě tehdy když knihovna má dnes otevřeno ještě 
        alespoň 1 h (parametr) nebo má otevřeno zítra.  Bere v potaz dovolenou
        a provozní dobu.  Výjimka je pro víkendy, víkendové dny se berou v úvahu jen je-li otevřeno, jinak jakoby nebyly.

        Příklady:

        Je pátek 11:59, knihovna má otevřeno v pátek do 13:00, nemá dnes 
        dovolenou, vrátí True bez ohledu na ostatní.

        Je pátek 12:01, knihovna má otevřeno v pátek do 13:00, nemá dnes 
        dovolenou, v sobotu i v neděli má zavřeno a od pondělí je dovolená, 
        vrátí False.  Dovolená je od úterý, vrátí True.

        Je sobota 12:00, knihovna nemá otevřeno v sobotu ani v neděli, má 
        otevřeno pondělí i úterý a od úterý má dovolenou, vrátí True.
        """

        # Dovolená, start 0:00, end 23:59, tz naive
        vacation_start_dt, vacation_end_dt = self._parse_vacation()
        exist_vacation = False
        # Dovolená není nastavena nebo není nastavena korektně
        if vacation_start_dt and vacation_end_dt:
            # if DEBUG_VAC or DEBUG_VAC24:
            #     print("vacation exists")
            exist_vacation = True

        # Provozní doba
        open_days_list = None
        if self.opening_hours:
            # if DEBUG_VAC or DEBUG_VAC24:
            #     print("self.opening_hours", repr(self.opening_hours))
            open_days_list = self.open_days_list(default=None)
        # if DEBUG_VAC or DEBUG_VAC24:
        #     print("open_days_list", repr(open_days_list), repr(bool(open_days_list)))
        exist_open = bool(open_days_list)

        # Minimální doba, jak dlouho musí být otevřeno.
        # 0.0416, txn. 1 h
        open_today_min = ziskej_parameter('open_today_min_days')

        # Default pro now.
        if not now_dt:
            now_dt = DateTime()

        # Normalizace zachová čas, ale vrátí tz naive DateTime now_dt není
        # nebo nemusí být.  Všechna porovnání se ale provádí v tz naive
        # DateTime.
        now_dt = datetime_normalize(now_dt, preserve_time=True)
        if DEBUG_VAC or DEBUG_VAC24:
            print("is_able_to_react_in_1d for {}".format(repr(now_dt)))

        # Zkontrolovat zda dnes je otevřeno ještě alespoň 1 h (parametr)
        # z hlediska provozní doby i z hlediska dovolené, pokud ano, vrátí
        # True, jinak pokračuje.  Nezáleží na tom, zda je dnes víkend.
        if exist_open:
            end_open_hours = self._end_open_hours(
                now_dt=now_dt, use_open_today_default=False)
            # if DEBUG_VAC or DEBUG_VAC24:
            #     print("exist_open, end_open_hours:", end_open_hours)
        else:
            end_open_hours = ziskej_parameter('open_today_default')
            # if DEBUG_VAC or DEBUG_VAC24:
            #     print("not exist_open, end_open_hours default:", end_open_hours)
        if end_open_hours is None:
            is_open_today_min = False
        else:
            is_open_today_min = bool(
                now_dt + open_today_min <
                datetime_normalize(now_dt, time=end_open_hours))
            # if DEBUG_VAC or DEBUG_VAC24:
            #     print("is_open_today_min", is_open_today_min)
        if is_open_today_min:
            if DEBUG_VAC or DEBUG_VAC24:
                print("today ok")
            # Pokud je dnes otevřeno dostatečně dlouho (při zanedbání
            # dovolené), stačí nyní zkontrolovat, jestli dnes není dovolená.
            if not self._is_vacation(
                    now_dt, vacation_start_dt, vacation_end_dt):
                return True
        else:
            if DEBUG_VAC or DEBUG_VAC24:
                print("today not ok")

        # Zkontrolovat zda je zítra otevřeno z hlediska provozní doby
        # i dovolené, pokud ano, vrátí True, pokud ne, tak zkontroluje, zda se
        # jedná o víkendový den, pokud ne, vrátí False, pokud ano, pokračuje
        # o den později stejným testem.
        open_days_list = self.open_days_list()
        # if DEBUG_VAC or DEBUG_VAC24:
        #     print("open_days_list", open_days_list)
        day_dt = datetime_normalize(now_dt+1)  # 12:00
        while True:
            day_dt_dow = day_dt.dow()
            if day_dt_dow in open_days_list and not self._is_vacation(
                    day_dt, vacation_start_dt, vacation_end_dt):
                if DEBUG_VAC or DEBUG_VAC24:
                    print("True open and not vacation on day", day_dt, day_dt_dow)
                return True
            if day_dt.dow() not in (0, 6):
                if DEBUG_VAC or DEBUG_VAC24:
                    print("False closed or vacation on working day", day_dt, day_dt_dow)
                return False
            day_dt = day_dt + 1
            # if DEBUG_VAC or DEBUG_VAC24:
            #     print("try next day", day_dt)

    def _is_vacation(self, day_dt, vacation_start_dt, vacation_end_dt):
        """Vrátí True, pokud v den day_dt je dovolená."""
        # Není nastavena dovolená.
        if not vacation_start_dt or not vacation_end_dt:
            return False
        # Pokud dovolená skončila dřív jak dnes.
        if day_dt > vacation_end_dt:
            return False
        # Pokud dovolená ještě nezačala ani brzy nezačne.
        if day_dt < vacation_start_dt:
            return False
        return True

    def _cleanup_vacation(self, vacation):
        """Smazat dovolenou v minulosti, seřadit dovolené, nedovolit překryvy.
        Žádná jednotlivá dovolená nikdy nekončí dříve než začíná, to už bylo
        zkontrolováno.
        """

        starts = []
        ends = []
        for idx in ['0', '1', '2', ]:
            if not vacation.get(idx, None):
                continue
            if not vacation[idx]['0'] or not vacation[idx]['1']:
                continue
            if vacation[idx]['1'] < DateTime().ISO()[:10]:
                continue
            if not len(starts) or starts[-1] < vacation[idx]['0']:
                starts.append(vacation[idx]['0'])
                ends.append(vacation[idx]['1'])
            else:
                insert_idx = 0
                while starts[insert_idx] < vacation[idx]['0']:
                    insert_idx += 1
                starts.insert(insert_idx, vacation[idx]['0'])
                ends.insert(insert_idx, vacation[idx]['1'])
        if len(starts) > 1:
            if starts[1] < ends[0]:
                return None
        if len(starts) > 2:
            if starts[2] < ends[1]:
                return None
        result = dict()
        for idx in range(len(starts)):
            result[str(idx)] = {'0': starts[idx], '1': ends[idx], }
        return result

    def _parse_vacation(self):
        """Vrátí začátek a konec dovolené v DateTime tz naive.  Začátek 0:00,
        konec 23:59.

        Je vyžadována existence zahájení i ukončení.

        V případě, že dojde v prohlížeči k vymazání dovolené vrátí prázdný
        string.
        """

        default = (u'', u'', )
        if not self.vacation:
            if DEBUG_VAC:
                print("vacation default (empty)")
            return default
        vacation_full = json.loads(self.vacation)

        if vacation_full['version'] == 1:
            vacation = vacation_full['vacation']
            # Zbavit minulých a seřadit, takže pak stačí brát v úvahu jen
            # první.
            vacation = self._cleanup_vacation(vacation)
            if not vacation:
                if DEBUG_VAC:
                    print("vacation default (empty after cleanup)")
                return default
            vacation_start_iso = vacation['0']['0']
            vacation_end_iso = vacation['0']['1']
            if vacation_start_iso is not u'' and vacation_end_iso is not u'':
                vacation_start_dt = datetime_normalize(
                    vacation_start_iso, time="00:00")
                vacation_end_dt = datetime_normalize(vacation_end_iso, time="23:59")
                if DEBUG_VAC:
                    print("vacation_start_dt", vacation_start_dt)
                    print("vacation_end_dt", vacation_end_dt)
                return vacation_start_dt, vacation_end_dt
        else:
            raise Exception('Invalid vacation data format version: {}'.format(vacation_full['version']))

        if DEBUG_VAC:
            print("vacation default (empty)")
        return default

    def open_days_list(self, default=[1, 2, 3, 4, 5]):
        """Vrátí seznam dnů, kdy je otevřeno, ve formátu dow, např. knihovna má
        otevřeno po ut st, tak vrátí [1, 2, 3] pokud knihovna nemá vyplněnou
        pracovní dobu počítá se default, který je každý pracovní den.
        """

        open_days = []
        if not self.opening_hours:
            return default
        open_days_full = json.loads(self.opening_hours)
        days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"]
        x = 0
        for i in days:
            day = open_days_full["opening_hours"][i]  # FIXME
            afternoon = day["1"]["1"]
            morning = day["0"]["1"]
            if not afternoon and not morning:
                pass
            else:
                open_days.append(x)
            x = x + 1
        if open_days == []:
            return default
        return open_days

    def _what_is_now(self, now, ignore_open_hour_min=False):
        """Vrátí trojici now_dt_exact, now_dt_norm,
        is_today_open_by_opening_hours.  Kde now_dt_exact je normalizovaný čas
        now (možno nechat None), normalizovaný ve smyslu zaokrouhlení a
        odřezání time zone, aby všechny časy pro porovnávání byly tz naive.
        now_dt_norm je navíc s korekcí, kdy pro den, kdy knihovna nemá
        otevřeno, nebo už má zavřeno a v případě, kdy není nastaveno
        ignore_open_hour_min, tak i v případě, kdy nemá alespoň 1 h otevřeno,
        tak následující den 0:01.  A is_today_open_by_opening_hours je True,
        pokud dnes měla nebo ještě má otevřeno - z pohledu provozní doby, bez
        ohledu na dovolenou.
        """

        if ignore_open_hour_min:
            open_today_min = 0
        else:
            # 0.0416, txn. 1 h
            open_today_min = ziskej_parameter('open_today_min_days')

        if not now:
            now_dt = DateTime()
        elif isinstance(now, DateTime):
            now_dt = now
        else:
            now_dt = DateTime(now)
        now_dt_exact = datetime_normalize(now_dt, preserve_time=True)
        now_dt_norm = now_dt_exact

        end_open_hours = self._end_open_hours(now_dt=now_dt)
        if not end_open_hours:
            is_today_open_by_opening_hours = False
            is_today_ok = False
            # Není dnes otevřeno vůbec, dnešek se nepočítá a normalizovaný čas
            # se nastaví na zítřek 0:01.
            now_dt_norm = datetime_normalize(now_dt + 1, time = "00:01")
        else:
            is_today_open_by_opening_hours = True
            is_today_ok = True
            now_dt_end = datetime_normalize(now_dt, time=end_open_hours)
            # Pokud je už po zavírací době nebo zbývá méně než 1 h (viz
            # open_today_min) normalizovaný čas se nastaví na zítřek 0:01.
            if now_dt_exact > now_dt_end - open_today_min:
                now_dt_norm = datetime_normalize(now_dt + 1, time = "00:01")
                # Už je zavřeno, nebo během hodiny bude, ale dnes bylo
                # otevřeno, proto nemá vliv na is_today_open_by_opening_hours
                # Ale má vliv na is_today_ok
                is_today_ok = False

        return dict(
            now_dt_exact=now_dt_exact,
            now_dt_norm=now_dt_norm,
            is_today_open_by_opening_hours=is_today_open_by_opening_hours,
            is_today_ok=is_today_ok,
            )

    def _count_open_days(self, start_dt=None, end_dt=None):
        """Vrátí počet dnů,kdy má knihovna otevřeno mezi dvěma daty,
        započítá i oba krajní dny.
        """

        if not start_dt:
            raise Exception ("Missing value 'start_dt' ")
        if not end_dt:
            raise Exception ("Missing value 'end_dt'")
        if DEBUG_VAC:
            print("_count_open_days before normalization:", repr(start_dt),
                  repr(end_dt))
        start_dt = datetime_normalize(start_dt)
        end_dt = datetime_normalize(end_dt)
        if DEBUG_VAC:
            print("_count_open_days after normalization:", repr(start_dt),
                  repr(end_dt))
        value = 0
        open_days = self.open_days_list()
        dt = start_dt
        while dt <= end_dt:
            if dt.dow() in open_days:
                value = value + 1
            dt = dt + 1
        if DEBUG_VAC:
            print("_count_open_days result", value)
        return value

    def _count_close_days(self, start_dt=None, end_dt=None):
        """Vrátí počet dnů,kdy nemá knihovna otevřeno mezi dvěma daty."""

        if not start_dt:
            raise Exception("Missing value 'start_dt' ")
        if not end_dt:
            raise Exception("Missing value 'end_dt'")
        start_dt = datetime_normalize(start_dt)
        end_dt = datetime_normalize(end_dt)
        value = 0
        open_days = self.open_days_list()
        dt = start_dt
        while dt <= end_dt:
            if dt.dow() not in open_days:
                value = value + 1
            dt = dt + 1
        return value

    def _end_open_hours(self, now_dt=None, use_open_today_default=True):
        """Vrátí konec pracovní doby pro dnešek nebo pro now_dt.  Pokud
        pracovní doba není vyplněná žádný den vrátí open_today_default.
        Pokud není vyplněna jen dnes (resp. now_dt), vrátí None.
        """

        DEBUG_LOCAL = False

        open_days_list_real = self.open_days_list(default=None)

        # Default pro nevyplněnou provozní dobu
        if open_days_list_real is None:
            if use_open_today_default:
                open_today_default = ziskej_parameter('open_today_default')
                if DEBUG_LOCAL:
                    print("returning default parameter", open_today_default)
                return open_today_default
            if DEBUG_LOCAL:
                print("returning default None")
            return None

        # Den v týdnu jako string key do struktury v opening_hours
        if now_dt is None:
            now_dt = DateTime()
        now_dow = now_dt.dow()
        now_dow_str_list = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"]
        now_dow_str = now_dow_str_list[now_dow]

        # Načíst dnešní opening_hours
        open_days_full = json.loads(self.opening_hours)
        opening_hours_day = open_days_full["opening_hours"][now_dow_str]
        afternoon = opening_hours_day["1"]["1"]
        morning = opening_hours_day["0"]["1"]

        if DEBUG_LOCAL:
            print("returning based on", afternoon, morning)

        # Do kdy je dnes provozní doba
        if not afternoon and not morning:
            return None  # dnes je zavřeno
        if not afternoon:
            return morning
        if not morning:
            return afternoon
        if afternoon < morning:
            return morning
        return afternoon

    # def _get_open_days_coef(self, now=None):
    #     """Vrátí koeficient 0 až 1 pro penalizaci v AutoLB podle otevřených
    #     dnů.
    #     """

    #     DEBUG_LOCAL = False

    #     near_open_days_count = ziskej_parameter('near_open_days_count_float')
    #     rating_near_open_days = ziskej_parameter('rating_near_open_days')
    #     nearest_open_day_count = ziskej_parameter('nearest_open_day_count')
    #     rating_nearest_open_day = ziskej_parameter('rating_nearest_open_day')
    #     if DEBUG_LOCAL:
    #         print 
    #         print "_get_open_days_coef for ", self.sigla
    #         print "near_open_days_count", near_open_days_count
    #         print "rating_near_open_days", rating_near_open_days
    #         print "nearest_open_day_count", nearest_open_day_count
    #         print "rating_nearest_open_day", rating_nearest_open_day

    #     now_dt_exact, now_dt_norm, is_today_open_by_opening_hours = \
    #         self._what_is_now(now)

    #     # n_lib je počet otevřených dnů v near_open_days_count _count_open_days
    #     # počítá i krajní dny
    #     n_lib_end = now_dt_norm + near_open_days_count - 1
    #     n_lib = self._count_open_days(start_dt=now_dt_norm, end_dt=n_lib_end)

    #     rat_near_open_day = float(
    #         (1 - n_lib/near_open_days_count) * rating_near_open_days)

    #     if DEBUG_LOCAL:
    #         print "rat_near_open_day", rat_near_open_day
    #         print "n_lib", n_lib

    #     # m_lib je nejbližší den, kdy je otevřeno,
    #     # Dnešek je 0 zítra je 1 atd.
    #     m_lib_end = now_dt_norm + nearest_open_day_count

    #     open_days_list = self.open_days_list()
    #     m_lib = 0
    #     item_dt = now_dt_norm
    #     while m_lib_end > item_dt:
    #         if item_dt.dow() not in open_days_list:
    #             m_lib = m_lib+1
    #             item_dt = item_dt + 1
    #         else:
    #             break

    #     rat_not_open = m_lib * rating_nearest_open_day

    #     if DEBUG_LOCAL:
    #         print "rat_not_open", rat_not_open
    #         print "m_lib", m_lib

    #     value = rat_not_open + rat_near_open_day

    #     if DEBUG_LOCAL:
    #         print "value", value
    #         print " "

    #     return value

    def _get_lb_coef(self, reader_date_iso=None, now=None, penalize_days=None):
        """Vrátí koeficient mezi 0.0 a 1.0 (včetně) pro penalizaci v AutoLB
        podle provozní doby a dovolené v knihovně.

        Vrátit 1.0 pro penalizaci za 24 h limit pro DK zohledňující provozní
        dobu i dovolenou.

        Příklady: Je pondělí 15:00, pokud dovolená teď není, umožnit objednat.
        Pokud dovolená začíná dnes, neumožnit. Pokud začíná zítra, umožnit.
        Pokud má dnes dovolenou, ale zítra nemá, umožnit.

        Pokud nic z toho nenastane, pak teprve počítat koeficient.

        Spočítat všechny pracovní dny mezi now a reader_date_iso odečíst počet 
        dní dovolené mezi now a reader_date_iso odečíst získej parametr kolik 
        dní má knihovna na požadavek

        - Pokud bude výsledek větší jak 0: Vrátit číslo menší jak 0.5, ale
          větší jak 0 podle počtu "volných dní"

        - Pokud se výsledek bude rovnat 0: Vrátit 0,75

        - Pokud výsledek bude menší jak 0: Vrátit 1

        Parametry:

        reader_date_iso: string obsahující den v ISO 2020-08-30; den do kdy je
                         požadavek aktualní.

        penalize_days: Získej parametr určující jak dlouho před dovolenou má
                       být DK penalizována.
        """

        DEBUG_LOCAL = False

        default = 0.0
        default_forbidden = 1.0

        # Nové zadání - podstatná změna je implementována pomocí
        # is_able_to_react_in_1d.
        if not self.is_able_to_react_in_1d(now_dt=now):
            if DEBUG_VAC or DEBUG_VAC24 or DEBUG_LOCAL:
                print('debug_vac', self.sigla,
                      'return default_forbidden due to vacation and open days '
                      'and DK limit 24h')
            return default_forbidden

        # Parametry, jejich defaulty
        if not reader_date_iso:
            x = ziskej_parameter('reader_date_default')
            reader_date_dt = datetime_normalize(datetime_add_wd(x))
            if DEBUG_LOCAL:
                print('debug_vac', self.sigla,
                      'reader_date_dt use reader_date_default', reader_date_dt)
        else:
            # reader_date_iso je string 
            reader_date_dt = datetime_normalize(reader_date_iso)
            if DEBUG_LOCAL:
                print('debug_vac', self.sigla, 'reader_date_dt specified',
                      reader_date_dt)

        if penalize_days is None:
            penalize_days = ziskej_parameter(
                'mvs_dk_lb_penalty_day_before_holiday')

        # Pokud je po zavírací době nastaví now jako následující den 00:01
        # now_dt je DateTime
        # win .. dict: now_dt_exact, now_dt_norm,
        #              is_today_open_by_opening_hours, is_today_ok
        win = self._what_is_now(now)

        # Dovolená, start 0:00, end 23:59, tz naive
        vacation_start_dt, vacation_end_dt = self._parse_vacation()

        if DEBUG_LOCAL:
            print("now_dt_exact", repr(win['now_dt_exact']))
            print("now_dt_norm", repr(win['now_dt_norm']))
            print("is_today_open_by_opening_hours",
                  repr(win['is_today_open_by_opening_hours']))
            print("is_today_ok", repr(win['is_today_ok']))
            print("vacation_start_dt", repr(vacation_start_dt))
            print("vacation_end_dt", repr(vacation_end_dt))

        value = 0
        if not win['is_today_ok']:
            value += 10
            open_days_list_real = self.open_days_list(default=None)
            open_days_list = self.open_days_list()
            day_dt = win['now_dt_norm']
            while True:
                day_dt_dow = day_dt.dow()
                if open_days_list_real:
                    is_open_by_opening_hours = day_dt_dow in open_days_list
                else:
                    is_open_by_opening_hours = day_dt_dow in open_days_list
                if is_open_by_opening_hours and not self._is_vacation(
                        day_dt, vacation_start_dt, vacation_end_dt):
                    if DEBUG_LOCAL:
                        print("open and not vacation on day", day_dt,
                              day_dt_dow)
                    break
                value += 10
                day_dt = day_dt + 1
        value = 0.1 * value / 30.0

        if DEBUG_VAC or DEBUG_VAC24 or DEBUG_LOCAL:
            print('debug_vac', self.sigla, 'return value', value)
        return value
