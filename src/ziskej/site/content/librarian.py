# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): Librarian.

Používá se v membrane pro propojení s Zope / Plone PAS jako zdroj uživatelů a
skupin.
"""

import logging
from zope import schema
from zope.interface import Interface, implements, Invalid, invariant
from z3c.form.interfaces import IAddForm, IEditForm
from plone.dexterity.content import Container
from plone.directives import form
from plone.autoform.directives import read_permission, write_permission

from ziskej.site import _
from ziskej.site.utils import make_searchable_text
from ziskej.site.validators import validate_email
from ziskej.site.validators import validate_username, validate_unique_username
from ziskej.site.content.base import IBaseUser, BaseUser


logger = logging.getLogger("ziskej")


class ILibrarian(IBaseUser):
    """ILibrarian aka Knihovník is membrane source of members."""

    #form.mode(username='display')
    form.mode(IEditForm, username='display')
    write_permission(username='ziskej.manage_librarians')
    username = schema.ASCIILine(
        title=_(u'Username'),
        description=_(u'Přihlašovací jméno'),
        required=True,
        constraint=validate_username,  # see comment for username_unique
    )

    first_name = schema.TextLine(
        title=_(u"Jméno"),
        required=True,
    )

    last_name = schema.TextLine(
        title=_(u"Příjmení"),
        required=True,
    )

    email = schema.ASCIILine(
        title=_(u"E-mailová adresa pro notifikace"),
        required=True,
        constraint=validate_email,
    )

    form.mode(phone_mvs='display')
    phone_librarian = schema.TextLine(
        title=_(u'Telefonní číslo'),
        description=_(u''),
        required=False,
    )

    write_permission(is_library_admin='ziskej.manage_librarians')
    is_library_admin = schema.Bool(
        title=_(u'Správce knihovny'),
        required=False,
        missing_value=False,
    )

    @invariant
    def username_unique(data):
        """The username must be unique, as it is the login name (user name).

        The tricky thing is to make sure editing a user and keeping
        his username the same actually works.
        """

        user = data.__context__
        if user is not None:
            if hasattr(user, 'username') and user.username == data.username:
                # No change, fine.
                return
        error = validate_unique_username(data.username, context=user)
        if error:
            raise Invalid(error)


class Librarian(BaseUser):
    implements(ILibrarian)

    # nechceme getter z membrane
    # def getUserName(self):
    #     if self.username != self.getId():
    #         logger.warning('librarian invalid username {username} - it differs from id {id}'.format(
    #             id = self.getId(),
    #             username = self.username,
    #             ))
    #     return self.getId()

    def set_password(self, value):
        self.password = value
        self._p_changed = True

    def update_attrs_if_changed(self, **obj_dict):
        ALL_ATTRS = ('first_name', 'last_name', 'email', 'is_library_admin',
                     'phone_librarian', )
        ALLOWED_ATTRS = ('first_name', 'last_name', 'email', 'is_library_admin',
                         'phone_librarian', )
        is_updated = False
        for k in obj_dict:
            if k not in ALLOWED_ATTRS:
                if k in ALL_ATTRS:
                    continue
                logger.error('update_attrs_if_changed key {} is not allowed'.format(k))
                raise Exception('Internal error')
            v_orig = getattr(self, k, None)
            v = obj_dict.get(k, None)
            if v_orig != v:
                setattr(self, k, v)
                #logger.info('doit for {k}: {v_orig} to {v}'.format(k=k, v_orig=v_orig, v=v))
                is_updated = True
        return is_updated

    def Library_sigla(self):
        value = u''
        try:
            parent = self.__parent__
            while parent.portal_type not in ('Library', 'LibraryCT', 'Plone Site', ):
                parent = parent.__parent__
            if parent.portal_type == 'Library':
                value = parent.sigla.upper()
        except:
            pass
        return value
