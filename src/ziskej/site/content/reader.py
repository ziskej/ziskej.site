# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): Reader.

Používá se v membrane pro propojení s Zope / Plone PAS jako zdroj uživatelů a
skupin.
"""

import logging

from plone import api
from plone.autoform.directives import read_permission, write_permission
from plone.dexterity.content import Container
from plone.directives import form
from z3c.form.interfaces import IAddForm, IEditForm
from zope import schema
from zope.interface import Interface, implements, Invalid, invariant

from ziskej.site import _
from ziskej.site.content.base import IBaseUser, BaseUser
from ziskej.site.utils import make_searchable_text
from ziskej.site.utils_lib import pprint, safe_utf8, safe_unicode
from ziskej.site.validators import (
    validate_email,
    validate_username,
    validate_unique_username,
    )


logger = logging.getLogger("ziskej")


class IReader(IBaseUser):
    """Content type Reader for Membrane - provider of an user authentication and
    user attributes.
    """

    # user_ids

    # username aka login or user_id or id
    # GENERATED
    # membrane index metadata - no action needed - getUserName getUserName
    #form.mode(username='display')
    write_permission(username='cmf.ManagePortal')
    username = schema.ASCIILine(
        title=_(u'Username'),
        description=_(u'ADMIN-ONLY: Přihlašovací jméno, použito pro id, mělo by být založeno na NTK user id'),
        required=True,
        constraint=validate_username,  # see comment for username_unique
    )

    # usertype
    # free vocabulary - expected values: shibboleth, cpk, zope
    # membrane index 
    #form.mode(usertype='display')
    write_permission(usertype='cmf.ManagePortal')
    usertype = schema.ASCIILine(
        title=_(u'User type'),
        required=False,
        missing_value='',
    )

    # membrane index metadata
    #form.mode(ntk_user_id='display')
    write_permission(ntk_user_id='cmf.ManagePortal')
    ntk_user_id = schema.ASCIILine(
        title=_(u'NTK user id'),
        description=_(u'ADMIN-ONLY: NTK přihlašovací jméno uživatele.'),
        required=False,
        missing_value='',
    )

    # Shibboleth

    # HTTP_SHIB_PERSON_GIVENNAME
    # membrane in fullname
    #form.mode(first_name='display')
    write_permission(first_name='cmf.ManagePortal')
    first_name = schema.TextLine(
        title=_(u"Jméno"),
        required=True,
    )

    # HTTP_SHIB_PERSON_SN
    # membrane in fullname
    #form.mode(last_name='display')
    write_permission(last_name='cmf.ManagePortal')
    last_name = schema.TextLine(
        title=_(u"Příjmení"),
        required=True,
    )

    # HTTP_SHIB_PERSON_MAIL
    # membrane
    #form.mode(email='display')
    write_permission(email='cmf.ManagePortal')
    email = schema.ASCIILine(
        title=_(u"ADMIN-ONLY: E-mail z Shibboleth / Perun"),
        required=True,
        constraint=validate_email,
    )

    # Notifications

    # User e-mail for notifications
    # Based on self.email based on HTTP_SHIB_PERSON_MAIL.  User is able to
    # change this one.  It should be vefified before use.
    # membrane
    #form.mode(email_user='display')
    write_permission(email_user='cmf.ManagePortal')
    email_user = schema.ASCIILine(
        title=_(u"E-mailová adresa pro notifikace"),
        required=False,
        constraint=validate_email,
        missing_value='',
    )

    # Helper for email_user verification
    # membrane
    #form.mode(email_user_verification_code='display')
    write_permission(email_user_verification_code='cmf.ManagePortal')
    email_user_verification_code = schema.ASCIILine(
        title=_(u"Verifikační kód"),
        required=False,
        missing_value='',
    )

    # Marker whether email_user is verified
    # membrane
    #form.mode(is_email_user_validated='display')
    write_permission(is_email_user_validated='cmf.ManagePortal')
    is_email_user_validated = schema.Bool(
        title=_(u'E-mailová adresa - verifikace'),
        required=False,
        missing_value=False,
    )

    # Marker whether email_user is enabled
    # membrane
    #form.mode(is_notification_enabled='display')
    write_permission(is_notification_enabled='cmf.ManagePortal')
    is_notification_enabled = schema.Bool(
        title=_(u'Notifikace e-mailem'),
        required=False,
        missing_value=False,
    )

    # Library affiliation

    # Last library (as ŽK) - sigla lower case
    # membrane index metadata
    #form.mode(library='display')
    write_permission(library='cmf.ManagePortal')
    library = schema.ASCIILine(
        title=_(u'Poslední knihovna'),
        required=False,
        missing_value='',
    )

    # Čtenář ID v knihovně z CPK ze Shibbolethu
    write_permission(reader_library_id='cmf.ManagePortal')
    reader_library_id = schema.TextLine(
        title=_(u'Čtenář ID v knihovně z CPK ze Shibbolethu'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # Reader ID in last library (as ŽK)
    # membrane index metadata
    #form.mode(reader_lid='display')
    write_permission(reader_lid='cmf.ManagePortal')
    reader_lid = schema.TextLine(
        title=_(u'Čtenář ID v poslední knihovně'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # Marker whether email_user is enabled
    # membrane
    #form.mode(is_library_confirmed='display')
    write_permission(is_library_confirmed='cmf.ManagePortal')
    is_library_confirmed = schema.Bool(
        title=_(u'Potvrzeno knihovníkem'),
        required=False,
        missing_value=False,
    )

    # All libraries (as ŽK) including theirs reader_lid
    # before multiple identities useless
    # membrane
    #form.mode(libraries='display')
    write_permission(libraries='cmf.ManagePortal')
    libraries = schema.List(
        title=_(u'Mateřské knihovny'),
        required=False,
        default=[],
        value_type=schema.ASCIILine(),
    )

    # Library claim (as ŽK) - sigla case unsensitive but lower case is preferred
    # membrane index metadata
    #form.mode(library_claim='display')
    write_permission(library_claim='cmf.ManagePortal')
    library_claim = schema.ASCIILine(
        title=_(u'Čtenář prohlašuje, že toto je jeho mateřská knihovna'),
        required=False,
        missing_value='',
    )

    # Perun version

    # write_permission(perun_id='cmf.ManagePortal')
    # perun_id = schema.ASCIILine(
    #     title=_(u"Perun id"),
    #     description=_(u'ADMIN-ONLY: Perun user id.'),
    #     required=False,
    # )

    # write_permission(eduPersonPrincipalName='cmf.ManagePortal')
    # eduPersonPrincipalName = schema.ASCIILine(
    #     title=_(u"eduPersonPrincipalName"),
    #     description=_(u'ADMIN-ONLY: eduPersonPrincipalName.'),
    #     required=False,
    # )

    # # eduID.cz může být více
    # write_permission(eduid_data='cmf.ManagePortal')
    # eduid_data = schema.List(
    #     title=_(u"eduID.cz data"),
    #     description=_(u'ADMIN-ONLY: eppn a EntityID oddělenou středníkem.'),
    #     required=False,
    #     default=[],
    #     value_type=schema.ASCIILine(),
    # )

    # Shibboleth version

    # eduPersonPrincipalName = urn:oid:1.3.6.1.4.1.5923.1.1.1.6
    # aka eppn
    # Example: 95494@mzk.cz
    # membrane index metadata
    #form.mode(eppn='display')
    write_permission(eppn='cmf.ManagePortal')
    eppn = schema.ASCIILine(
        title=_(u"eppn"),
        description=_(u'ADMIN-ONLY: eppn aka eduPersonPrincipalName.'),
        required=False,
    )

    # # eduPersonTargetedID = urn:oid:1.3.6.1.4.1.5923.1.1.1.10
    # # aka persistent_id
    # # Example: https://shibboleth.mzk.cz/simplesaml/metadata.xml!https://ziskej.techlib.cz/shibboleth!b2d74e80e63dcc71fa978a4a6a87bd27a582d3b4
    # form.mode(persistent_id='display')
    # write_permission(persistent_id='cmf.ManagePortal')
    # persistent_id = schema.ASCIILine(
    #     title=_(u"persistent_id"),
    #     description=_(u'ADMIN-ONLY: persistent_id aka eduPersonTargetedID.'),
    #     required=False,
    # )

    # EntityID of IdP
    # Example: https://shibboleth.mzk.cz/simplesaml/metadata.xml
    # membrane index metadata
    #form.mode(idp_entityid='display')
    write_permission(idp_entityid='cmf.ManagePortal')
    idp_entityid = schema.ASCIILine(
        title=_(u"EntityID IdP"),
        description=_(u'ADMIN-ONLY: EntityID IdP.'),
        required=False,
    )

    # json
    # Example:
    """
    {version: 1,
    items: [{eppn: …,
    idp_entityid: …,
    persistent_id: …,
    first_login: …,
    last_login: …,
    etc}, …]}
    """
    # membrane
    #form.mode(identities_json='display')
    write_permission(identities_json='cmf.ManagePortal')
    identities_json = schema.TextLine(
        title=_(u'Identity čtenáře, JSON'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # GDPR

    # Marker whether user agreed with registration in Získej and NTK
    # membrane index metadata
    #form.mode(gdpr_registration='display')
    write_permission(gdpr_registration='cmf.ManagePortal')
    gdpr_registration = schema.Bool(
        title=_(u'GDPR: Registrace'),
        required=False,
        missing_value=False,
    )

    # Timestamp
    # membrane index metadata
    #form.mode(gdpr_registration_date='display')
    write_permission(gdpr_registration_date='cmf.ManagePortal')
    gdpr_registration_date = schema.Datetime(
        title=_(u'GDPR: Registrace - datum souhlasu'),
        required=False,
        missing_value=None,
    )

    # Marker whether user agreed with data saved in Získej and in NTK
    # membrane index metadata
    #form.mode(gdpr_data='display')
    write_permission(gdpr_data='cmf.ManagePortal')
    gdpr_data = schema.Bool(
        title=_(u'GDPR: Data'),
        required=False,
        missing_value=False,
    )

    # Timestamp
    # membrane index metadata
    #form.mode(gdpr_data_date='display')
    write_permission(gdpr_data_date='cmf.ManagePortal')
    gdpr_data_date = schema.Datetime(
        title=_(u'GDPR: Data - datum souhlasu'),
        required=False,
        missing_value=None,
    )

    # Last login through plone_login from auth_shibboleth
    #form.mode(last_login_date='display')
    write_permission(last_login_date='cmf.ManagePortal')
    last_login_date = schema.Datetime(
        title=_(u'Poslední přihlášení'),
        required=False,
        missing_value=None,
    )

    # cpkpost_json
    # membrane
    #form.mode(cpkpost_json='display')
    write_permission(cpkpost_json='cmf.ManagePortal')
    cpkpost_json = schema.TextLine(
        title=_(u'CPK POST, json'),
        required=False,
        missing_value=u"",
    )


    @invariant
    def username_unique(data):
        """The username must be unique, as it is the login name (user name).

        The tricky thing is to make sure editing a user and keeping
        his username the same actually works.
        """

        user = data.__context__
        if user is not None:
            if hasattr(user, 'username') and user.username == data.username:
                # No change, fine.
                return
        error = validate_unique_username(data.username, context=user)
        if error:
            raise Invalid(error)


class Reader(BaseUser):
    implements(IReader)

    # nechceme getter z membrane
    # def getUserName(self):
    #     if self.username != self.getId():
    #         logger.warning('reader invalid username {username} - it differs from id {id}'.format(
    #             id = self.getId(),
    #             username = self.username,
    #             ))
    #     return self.getId()

    @property
    def is_notify_reader(self):
        if not self.is_notify_important_reader:
            return False
        if not self.is_notification_enabled:
            return False
        return True

    @property
    def is_notify_important_reader(self):
        # NTK user, nepoužívá se
        #if not self.ntk_user_id
        #    return False
        if not self.email_user:
            return False
        # Validace email_user neprobíhá, takže ji nelze vyžadovat
        # if not self.is_email_user_validated:
        #     return False
        if not self.gdpr_registration:
            return False
        if not self.gdpr_data:
            return False
        return True

    def notification_email(self):
        if not self.is_notify_reader:
            return None
        return self.email_user

    # reader_lid / Reader_lid - unicode issue
    # https://reinout.vanrees.org/weblog/2008/10/14/unicodedecodeerror-in-plones-catalog.html
    def Reader_lid(self):
        value = self.reader_lid
        if not value:
            value = u''
        #value = safe_unicode(value)
        return value

    @property
    def is_active(self):
        """Ctenář je aktivní a v rámci Získej API má přístup minimálně k již
        vytvořeným objednávkám.  Může nastat i v případě, kdy jeho mateřská
        knihovna není aktivní.
        """
        return not self.check_active(full=False)

    @property
    def is_active_full(self):
        """Ctenář je aktivní v plném rozsahu, tedy i jeho mateřská knihovna je
        aktivní.
        """
        return not self.check_active(full=True)

    def can_create(self, service=None):
        """Ctenář může vytvářet nové objednávky.
        """
        return not self.check_active(service=service, full=True)

    def check_active(self, service=None, full=True):
        """Vrací None, pokud je aktivní, jinak srozumitelnou hlášku pro čtenáře,
        proč není aktivní.

        Parametr full zda se má etstovat, že čtenářova knihovna je aktivní.
        """

        if not self.library:
            return u"Čtenář nemá nastavenu žádnou knihovnu."

        # Snaží se opravit stará data, kdy self.library není sigla, ale přímo
        # objekt - toto nenastane nikdy na PROD nebo DEMO, pouze speciální
        # stará testovací data
        if not isinstance(self.library, basestring):
            try:
                sigla = reader.library.sigla.lower()
                reader.library = sigla
                reader.reindexObject()
            except Exception as e:
                logger.exception('Try to fix broken data format when reader.library exists and is not string for reader {} failed: {}.'.format(reader.getId(), str(e)))
                # Toto by nemělo nikdy nastat
                return u"Čtenář nebyl vytvořen správně, nastavení knihovny je chybné."
            logger.warning('Try to fix broken data format when reader.library exists and is not string for reader {} - success.'.format(reader.getId()))

        portal = api.portal.get()
        library = getattr(portal.libraries, self.library.lower(), None)
        if not library:
            return u"Čtenář má nastavenu knihovnu neexistující v Získej."
        if full:
            if not library.is_active:
                return u"Čtenář má nastavenu knihovnu, která není aktivní v Získej."

        if not self.gdpr_registration:
            return u"Čtenář nedal souhlas s registrací."
        if not self.gdpr_data:
            return u"Čtenář nedal souhlas s ukládáním dat."

        # SUGGESTION Zkontrtolovat, zda je služba (např. MVS) v knihovně
        # povolena
        if service is not None:
            pass

        # Čtenář je aktivní a může používat Získej
        return None

    def set_password(self, value):
        self.password = value
        self._p_changed = True

    def update_attrs_if_changed(self, **obj_dict):
        ALL_ATTRS = ('eppn', 'first_name', 'last_name', 'email_user', 'is_notification_enabled', 'library', 'reader_library_id', 'gdpr_registration', 'gdpr_data', 'reader_email')
        ALLOWED_ATTRS = ('first_name', 'last_name', 'email_user', 'is_notification_enabled', 'reader_library_id', 'gdpr_registration', 'gdpr_data', 'reader_email')
        is_updated = False
        for k in obj_dict:
            if k not in ALLOWED_ATTRS:
                if k in ALL_ATTRS:
                    continue
                logger.error('update_attrs_if_changed key {} is not allowed'.format(k))
                raise Exception('Internal error')
            v_orig = getattr(self, k, None)
            v = obj_dict.get(k, None)
            if v_orig != v:
                setattr(self, k, v)
                #logger.info('doit for {k}: {v_orig} to {v}'.format(k=k, v_orig=v_orig, v=v))
                is_updated = True
        return is_updated

    def Library_sigla(self):
        if not self.library:
            return u''
        return self.library.upper()

    def eppn_library(self):
        if not self.eppn:
            return u''
        if '@' not in self.eppn:
            return self.eppn
        eppn_library = self.eppn[self.eppn.find('@')+1:]
        return eppn_library
