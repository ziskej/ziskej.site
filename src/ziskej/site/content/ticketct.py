# -*- coding: utf-8 -*-
"""Dexterity CT interface a třída pro typ obsahu (CT): TicketCT.

Kontejner pro objednávky (Ticket), právě jeden a pevné umístění /tickets.
"""

from zope.interface import implements

from ziskej.site.content.base import IBaseCT, BaseCT


class ITicketCT(IBaseCT):
    pass


class TicketCT(BaseCT):
    implements(ITicketCT)
