# -*- coding: utf-8 -*
"""Dexterity CT interface a třída pro typ obsahu (CT): Ticket.

Společný základ pro objednávky různých typů.
"""

from __future__ import print_function

import json
import logging

from DateTime import DateTime
from plone import api
from plone.autoform.directives import read_permission, write_permission
from plone.dexterity.content import Container
from plone.directives import form
from plone.supermodel import model
from zope import schema
from zope.interface import Interface, implements

from ziskej.site import (
    _,
    ENFORCE_PAYSYS,
    USE_PLATEBATOR,
    PLATEBATOR_BASE_URL,
    )
from ziskej.site.content.base import (
    IBase,
    Base,
    ITicketBase,
    TicketBase,
    IPMItemBase,
    PMItemBase,
    )
from ziskej.site.utils import (
    make_searchable_text,
    format_historylog_entry,
    safe_unicode,
    safe_utf8,
    datetime_add_wd,
    datetime_add_d,
    sanitize_input_text,
    format_date,
    nl2br,
    )
from ziskej.site.utils_lib import pprint
from ziskej.site.ticket_helpers import (
    TICKET_HID_MIN,
    TICKET_PORTAL_TYPES,
    SUBTICKET_PORTAL_TYPES,
    TICKET_SUBTICKET_PORTAL_TYPES,
    get_ticket_wf_label,
    wf_action,
    )

from ziskej.site.validators import validate_email


logger = logging.getLogger('ziskej')

DEBUG = False


class ITicket(ITicketBase):
    """Base class pro všechny typy Ticket*

    Atributy:

    Base attrs
    ----------

    id .. náhodné kontextově jedinečné id
    fullname .. (generated)

    Human ID
    --------

    hid = max_hid(all_tickets) + 1
    1st hid = 100001
    tickety se nemažou (vzít právo mazat všem), potřebujeme "softdelete"?

    shortname .. u'MV100001' - generated, catalog-metadata
    hid_prefix .. u'MV' - TicketMVS, fixed
    hid .. 100001 - catalog-index, catalog-metadata

    Refs
    ----

    reader .. reader id
    library_zk .. library sigla

    Document
    --------

    Aktuálně poptávaný / dodaváný dokument (titul) by měl být vždy právě jeden.
    Nicméně může se v čase měnit.  Aktuální subticket a ticket by měly být vždy
    v souladu.  Nicméně dokument v ticketu bude nastavený i před vytvořením
    prvního subticketu a subticket zůstane nastavený i ve chvíli kdy pžřestane
    být aktuálním subticketem.

    doc_data pak obsahuje kompletní data o dokumentu ve formátu json.

    docs_data pak obsahuje kompletní data o dokumentu i všech alternativních
    dokumentech ve formátu json.

    doc_id .. 'nkp.NKC01-002179378'
    doc_title .. 'Zaklínač. I., Poslední přání'
    doc_fullname .. 'Zaklínač. I., Poslední přání / Vojtěch Jarník, 1897-1970, 1984'
    doc_data .. (json)
    docs_data .. (json)

    Audit
    -----

    historylog

    Workflow
    --------

    Standard CMF workflow

    Ticket subclass specific

    status:
    - draft: reader created ticket
    - pending: reader submitted ticket waiting for ŽK approval
    - 
    """

    # Příklad pro fieldset
    # model.fieldset(
    #     'internal',
    #     label=_(u"Internal data"),
    #     fields=['hid', 'hid_prefix', ]
    # )

    # --------------------------------------------------------------------------
    # MVS, MRS, EDD

    # --------------------------------------------------------------------------
    # subtype

    # ticket_type
    form.mode(ticket_type='display')
    write_permission(ticket_type='cmf.ManagePortal')
    ticket_type = schema.ASCIILine(
        title=_(u'Typ objednávky'),
        description=_(u'Typ objednávky: mvs, mrs, edd.'),
        required=True,
        default="mvs",
    )

    # created_by
    form.mode(created_by='display')
    write_permission(created_by='cmf.ManagePortal')
    created_by = schema.ASCIILine(
        title=_(u'Způsob vytvoření'),
        description=_(u'Způsob vytvoření: librarian, reader, cpk_reader.'),
        required=False,
        default="",
        missing_value="",
    )

    # --------------------------------------------------------------------------
    # identifikace ticketu

    # jednoznačné a neměnitelné člověkem čitelné označení požadavku
    # dohromady s hid_prefix, který se může změnit, tvoří shortname
    form.mode(hid='display')
    write_permission(hid='cmf.ManagePortal')
    hid = schema.Int(
        title=_(u'HID'),
        description=_(u'Human ID'),
        required=True,
        min=TICKET_HID_MIN,
    )

    # # MVA = MVS ŽK, MVP = MVS DK
    # form.mode(hid_prefix='display')
    # write_permission(hid_prefix='cmf.ManagePortal')
    # hid_prefix = schema.ASCIILine(
    #     title=_(u'HID prefix'),
    #     description=_(u'Human ID'),
    #     required=False,
    #     missing_value=u"",
    #     #default=u"",  # ASCIILine neumožňuje default u""
    # )

    # sigla ŽK
    form.mode(library_zk='display')
    write_permission(library_zk='cmf.ManagePortal')
    library_zk = schema.ASCIILine(
        title=_(u'Žádající knihovna'),
        required=False,
        missing_value=u"",
        #default=u"",
    )

    # sigla DK
    form.mode(library_dk='display')
    write_permission(library_dk='cmf.ManagePortal')
    library_dk = schema.ASCIILine(
        title=_(u'Dožádaná knihovna'),
        required=False,
        missing_value=u"",
        #default=u"",
    )

    # seznam sigla DK - použité pro přístup k požadavku
    form.mode(library_dks='display')
    write_permission(library_dks='cmf.ManagePortal')
    library_dks = schema.List(
        title=_(u'Dožádané knihovny'),
        required=False,
        missing_value=[],
        default=[],
        value_type=schema.ASCIILine(),
    )

    # název ŽK
    form.mode(library_zk_title='display')
    write_permission(library_zk_title='cmf.ManagePortal')
    library_zk_title = schema.TextLine(
        title=_(u'Název žádající knihovny'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # název DK
    form.mode(library_dk_title='display')
    write_permission(library_dk_title='cmf.ManagePortal')
    library_dk_title = schema.TextLine(
        title=_(u'Název dožádané knihovny'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # id aktuálního podpožadavku, pokud nějaký je
    form.mode(subticket_id='display')
    write_permission(subticket_id='cmf.ManagePortal')
    subticket_id = schema.ASCIILine(
        title=_(u'Hlavní podpožadavek ID'),
        required=False,
        missing_value=u"",
        #default=u"",
    )

    # seznam id všech podpožadavků
    form.mode(subticket_ids='display')
    write_permission(subticket_ids='cmf.ManagePortal')
    subticket_ids = schema.List(
        title=_(u'Podpožadavky IDs'),
        required=False,
        missing_value=[],
        default=[],
        value_type=schema.ASCIILine(),
    )

    # Knihovník zpracovávající požadavek
    form.mode(librarian='display')
    write_permission(librarian='cmf.ManagePortal')
    librarian = schema.TextLine(
        title=_(u'Knihovník'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # --------------------------------------------------------------------------
    # Dates as TextLine with date as ISO
    # see TicketCoreView.setup_timeline()

    form.mode(created_date='display')
    write_permission(created_date='cmf.ManagePortal')
    created_date = schema.TextLine(
        title=_(u'Datum vytvoření'),
        description=_(u'Den, kdy čtenář nebo knihovník vytvořil požadavek.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(reader_date='display')
    write_permission(reader_date='cmf.ManagePortal')
    reader_date = schema.TextLine(
        title=_(u'Datum požadované čtenářem'),
        description=_(u'Poslední den, kdy je pro čtenáře ještě přijatelné zahájení výpůjčky.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(accepted_date='display')
    write_permission(accepted_date='cmf.ManagePortal')
    accepted_date = schema.TextLine(
        title=_(u'Datum přijetí'),
        description=_(u'Pro čtenáře, den, kdy knihovník žádající knihovny ověřil čtenáře a zároveň je objednávka zaplacena.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(approved_date='display')
    write_permission(approved_date='cmf.ManagePortal')
    approved_date = schema.TextLine(
        title=_(u'Datum schválení'),
        description=_(u'Den, kdy knihovník žádající knihovny schválil požadavek.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(reader_from_date='display')
    write_permission(reader_from_date='cmf.ManagePortal')
    reader_from_date = schema.TextLine(
        title=_(u'K dispozici od'),
        description=_(u'Den, od kterého je k dispozici pro čtenáře.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(reader_to_date='display')
    write_permission(reader_to_date='cmf.ManagePortal')
    reader_to_date = schema.TextLine(
        title=_(u'Vrátit do'),
        description=_(u'Den, do kterého by čtenář měl vrátit resp. do kterého je k dispozici pro čtenáře.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(reader_mail_date='display')
    write_permission(reader_mail_date='cmf.ManagePortal')
    reader_mail_date = schema.TextLine(
        title=_(u'Odeslání mailu čtenáři'),
        description=_(u'Den odeslání mailu s informací o vyzvednutí čtenáři.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(reader_lent_date='display')
    write_permission(reader_lent_date='cmf.ManagePortal')
    reader_lent_date = schema.TextLine(
        title=_(u'Půjčeno čtenáři'),
        description=_(u'Den půjčení čtenářem.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(return_date='display')
    write_permission(return_date='cmf.ManagePortal')
    return_date = schema.TextLine(
        title=_(u'Vráceno čtenářem'),
        description=_(u'Den vrácení čtenářem, pro čtenáře je to i uzavření požadavku.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(closed_date='display')
    write_permission(closed_date='cmf.ManagePortal')
    closed_date = schema.TextLine(
        title=_(u'Datum uzavření'),
        description=_(u'Den uzavření požadavku.'),
        required=False,
        missing_value=None,
        default=None,
    )

    # Date + time as zope DateTime

    # Datum pro reporty
    form.mode(report_date='display')
    write_permission(report_date='cmf.ManagePortal')
    report_date = schema.Datetime(
        title=_(u'Datum pro reporty'),
        description=_(u''),
        required=False,
        missing_value=None,
        default=None,
    )

    # Datum, kdy se naposledy změnilo něco významného
    form.mode(modified_date='display')
    write_permission(modified_date='cmf.ManagePortal')
    modified_date = schema.Datetime(
        title=_(u'Poslední významná změna'),
        description=_(u''),
        required=False,
        missing_value=None,
        default=None,
    )

    # --------------------------------------------------------------------------
    # reader

    # id čtenáře, zatím je totožné s tím, zda zadal čtenář
    form.mode(reader='display')
    write_permission(reader='cmf.ManagePortal')
    reader = schema.ASCIILine(
        title=_(u'Čtenář'),
        required=False,
        missing_value=u"",
        #default=u"",
    )

    #form.mode(reader='display')
    #write_permission(reader_lid='cmf.ManagePortal')
    reader_lid = schema.TextLine(
        title=_(u'Čtenář ID'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # "" .. pending
    # "ok" .. approved
    # "stop" .. not approved and stop ticket
    # "reject" .. not approved and reject ticket
    form.mode(reader_approval='display')
    write_permission(reader_approval='cmf.ManagePortal')
    reader_approval = schema.TextLine(
        title=_(u'Ověření čtenáře'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(reader_email='display')
    write_permission(reader_email='cmf.ManagePortal')
    reader_email = schema.ASCIILine(
        title=_(u"E-mailová adresa pro notifikace"),
        required=False,
        constraint=validate_email,
        missing_value='',
    )

    # --------------------------------------------------------------------------
    # obsahové atributy

    # skutečná cena požadavku - stanovená DK a nepřímo schválená ŽK
    form.mode(cena='display')
    write_permission(cena='cmf.ManagePortal')
    cena = schema.Int(
        title=_(u'Cena'),
        description=_(u'Cena, za kterou dožádaná knihovna poskytne MVS.'),
        required=False,
        min=0,
        max=1000,
        missing_value=None,
        default=None,
    )

    form.mode(reader_fee='display')
    write_permission(reader_fee='cmf.ManagePortal')
    reader_fee = schema.TextLine(
        title=_(u'Cena čtenáři'),
        description=_(u'Cena, za kterou posktyne MVS žádající knihovna čtenáři.'),
        required=False,
        missing_value=None,
        default=None,
    )

    # poznámka ŽK, viditelná jen ŽK
    library_zk_note = schema.Text(
        title=_(u'Poznámka (knihovník)'),
        description=_(u'Tato poznámka je viditelná pouze knihovníku ŽK.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # Toto není poznámka čtenáře v rámci vytváření objednávky v Získej UI
    # (resp. jinak).  Toto je informace pro čtenáře od knihovníka spolu
    # s reader_place, jak a kde si má vyzvednout objednávku.
    form.mode(reader_note='display')
    write_permission(reader_note='cmf.ManagePortal')
    reader_note = schema.TextLine(
        title=_(u'Poznámka'),
        description=_(u'Poznámka čtenáři'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(reader_place='display')
    write_permission(reader_place='cmf.ManagePortal')
    reader_place = schema.TextLine(
        title=_(u'Místo vyzvednutí'),
        description=_(u'Místo, kde vydají čtenáři dokument '),
        required=False,
        missing_value=None,
        default=None,
    )

    reader_firstname = schema.Text(
        title=_(u'Jméno čtenáře'),
        description=_(u''),
        required=False,
        missing_value=u"",
        default=u"",
    )

    reader_lastname = schema.Text(
        title=_(u'Příjmení čtenáře'),
        description=_(u''),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(reader_token='display')
    write_permission(reader_token='cmf.ManagePortal')
    reader_token = schema.TextLine(
        title=_(u'Token čtenáře'),
        description=_(u'Token čtenáře pro EDD download resp. jakoby přihlášení.'),
        required=False,
        missing_value=None,
        default=None,
    )

    # --------------------------------------------------------------------------
    # platby čtenáře

    # Platebátor API: ziskej_payment_id
    form.mode(payment_id='display')
    write_permission(payment_id='cmf.ManagePortal')
    payment_id = schema.TextLine(
        title=_(u'Platba čtenáře: ID'),
        description=_(u'Interní payment_id, vhodné pro reklamace (SC).'),
        required=False,
        missing_value=None,
        default=None,
    )

    # Platebátor API: uuid
    form.mode(payment_platebator_uuid='display')
    write_permission(payment_platebator_uuid='cmf.ManagePortal')
    payment_platebator_uuid = schema.TextLine(
        title=_(u'Platba čtenáře: Platebátor UUID'),
        description=_(u'UUID platebátoru.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(payment_service_id='display')
    write_permission(payment_service_id='cmf.ManagePortal')
    payment_service_id = schema.TextLine(
        title=_(u'Platba čtenáře: Služba ID'),
        description=_(u'Z číselníku NTK pro napojení na NTK systémy.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(payment_amount='display')
    write_permission(payment_amount='cmf.ManagePortal')
    payment_amount = schema.Int(
        title=_(u'Platba čtenáře: Cena'),
        description=_(u'Cena, kterou čtenář platí.'),
        required=False,
        min=0,
        max=1000,
        missing_value=None,
        default=None,
    )

    form.mode(payment_created_date='display')
    write_permission(payment_created_date='cmf.ManagePortal')
    payment_created_date = schema.Datetime(
        title=_(u'Platba čtenáře: Datum vytvoření'),
        description=_(u'Datum vytvoření v platebátoru, nikoli datum zaplacení.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(payment_paid_date='display')
    write_permission(payment_paid_date='cmf.ManagePortal')
    payment_paid_date = schema.Datetime(
        title=_(u'Platba čtenáře: Datum zaplacení'),
        description=_(u'Datum zaplacení, převzato z platebátoru, znamená, že proběhlo úspěšné zaplacení.'),
        required=False,
        missing_value=None,
        default=None,
    )

    # --------------------------------------------------------------------------
    # log hlavních změn objektu

    # audit log pro servisní středisko
    form.mode(historylog='display')
    write_permission(historylog='cmf.ManagePortal')
    historylog = schema.Text(
        title=_(u'History log'),
        description=_(u'Toto je zobrazováno pouze servisnímu středisku.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # audit log pro servisní středisko
    form.mode(historylog_zk='display')
    write_permission(historylog_zk='cmf.ManagePortal')
    historylog_zk = schema.Text(
        title=_(u'History log ŽK'),
        description=_(u'Toto je zobrazováno pouze ŽK.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # audit log pro servisní středisko
    form.mode(historylog_r='display')
    write_permission(historylog_r='cmf.ManagePortal')
    historylog_r = schema.Text(
        title=_(u'History log čtenář'),
        description=_(u'Toto je zobrazováno pouze čtenáři.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # --------------------------------------------------------------------------
    # Source for doc

    form.mode(doc_manual='display')
    write_permission(doc_manual='cmf.ManagePortal')
    doc_manual = schema.Bool(
        title=_(u'Manuální zdroj dat'),
        required=False,
        missing_value=False,
        default=False,
    )

    # nkp.NKC01-002179378
    form.mode(doc_id='display')
    write_permission(doc_id='cmf.ManagePortal')
    doc_id = schema.TextLine(
        title=_(u'Titul ID'),
        description=_(u'Titul CPK ID, např. nkp.NKC01-002179378.'),
        required=False,
        missing_value=u"",
    )

    # Zaklínač. I., Poslední přání
    form.mode(doc_title='display')
    write_permission(doc_title='cmf.ManagePortal')
    doc_title = schema.TextLine(
        title=_(u'Název titulu'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # Zaklínač. I., Poslední přání / Vojtěch Jarník, 1897-1970, 1984'
    form.mode(doc_fullname='display')
    write_permission(doc_fullname='cmf.ManagePortal')
    doc_fullname = schema.TextLine(
        title=_(u'Citace'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_author='display')
    write_permission(doc_author='cmf.ManagePortal')
    doc_author = schema.TextLine(
        title=_(u'Autor/autoři díla'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_number='display')
    write_permission(doc_number='cmf.ManagePortal')
    doc_number = schema.TextLine(
        title=_(u'Rok/ročník/číslo'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_issuer='display')
    write_permission(doc_issuer='cmf.ManagePortal')
    doc_issuer = schema.TextLine(
        title=_(u' Rok a místo vydání'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_isbn='display')
    write_permission(doc_isbn='cmf.ManagePortal')
    doc_isbn = schema.TextLine(
        title=_(u'ISBN'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_issn='display')
    write_permission(doc_issn='cmf.ManagePortal')
    doc_issn = schema.TextLine(
        title=_(u'ISSN'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_signature='display')
    write_permission(doc_signature='cmf.ManagePortal')
    doc_signature = schema.TextLine(
        title=_(u'Signatura'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_note='display')
    write_permission(doc_note='cmf.ManagePortal')
    doc_note = schema.TextLine(
        title=_(u'Poznámka k dokumentu'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_data='display')
    write_permission(doc_data='cmf.ManagePortal')
    doc_data = schema.Text(
        title=_(u'Dokument data'),
        description=_(u'Toto je zobrazováno pouze administrátorům a je určeno pro ladění.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(docs_data='display')
    write_permission(docs_data='cmf.ManagePortal')
    docs_data = schema.Text(
        title=_(u'Dokumenty data'),
        description=_(u'Toto je zobrazováno pouze administrátorům a je určeno pro ladění.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(doc_dk_id='display')
    write_permission(doc_dk_id='cmf.ManagePortal')
    doc_dk_id = schema.TextLine(
        title=_(u'ID dokumentu v AKS'),
        description=_(u'ID dokumentu v AKS dožádané knihovny.'),
        required=False,
        missing_value=u"",
    )

    form.mode(rfid='display')
    write_permission(rfid='cmf.ManagePortal')
    rfid = schema.TextLine(
        title=_(u'RFID'),
        description=_(u'RFID jednotky, exempláře.'),
        required=False,
        missing_value=u"",
    )

    form.mode(barcode='display')
    write_permission(barcode='cmf.ManagePortal')
    barcode = schema.TextLine(
        title=_(u'Čárový kód'),
        description=_(u'Čárový kód jednotky, exempláře.'),
        required=False,
        missing_value=u"",
    )

    # --------------------------------------------------------------------------
    # Auto LB, manual DK, no DK

    form.mode(lb_state='display')
    write_permission(lb_state='cmf.ManagePortal')
    lb_state = schema.TextLine(
        title=_(u'LoadBalancing state'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(lb_manual_library='display')
    write_permission(lb_manual_library='cmf.ManagePortal')
    lb_manual_library = schema.TextLine(
        title=_(u'Manuálně zvolená DK'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(lb_manual_library_sent='display')
    write_permission(lb_manual_library_sent='cmf.ManagePortal')
    lb_manual_library_sent = schema.TextLine(
        title=_(u'Poslat nebo vyzvednout'),
        description=_(u'Poslat poštou nebo vyzvednout osobně - pro manuálně zvolenou DK.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # list, sep=', '
    # SUGGESTION změnit na schema.List
    # nkp.NKC01-002179378, nkp.NKC01-002179378, nkp.NKC01-002179378
    form.mode(doc_order='display')
    write_permission(doc_order='cmf.ManagePortal')
    doc_order = schema.TextLine(
        title=_(u'Pořadí DK'),
        description=_(u'Zvolené pořadí dožádaných knihoven.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # list, sep=', '
    form.mode(doc_order_sent='display')
    write_permission(doc_order_sent='cmf.ManagePortal')
    doc_order_sent = schema.TextLine(
        title=_(u'Způsob dodání'),
        description=_(u'Způsob dodání pro jednotlivě zvolené dožádané knihovny.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # list, sep=', '
    form.mode(doc_order_suggested='display')
    write_permission(doc_order_suggested='cmf.ManagePortal')
    doc_order_suggested = schema.TextLine(
        title=_(u'Navržené pořadí DK'),
        description=_(u'Navržené pořadí dožádaných knihoven - výsledek load balancing alg.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # json
    form.mode(doc_order_suggested_data='display')
    write_permission(doc_order_suggested_data='cmf.ManagePortal')
    doc_order_suggested_data = schema.Text(
        title=_(u'Data pro navržené pořadí DK'),
        description=_(u'Navržené pořadí dožádaných knihoven - podrobná data příslušná poslednímu výsledku load balancing alg.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # list, sep=', '
    form.mode(doc_order_blacklist='display')
    write_permission(doc_order_blacklist='cmf.ManagePortal')
    doc_order_blacklist = schema.TextLine(
        title=_(u'Pořadí DK: blacklist'),
        description=_(u'Které DK (dokumenty) už neopakovat.'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # --------------------------------------------------------------------------
    # pm (personal messaging) data

    # sender - obvyklé hodnoty jsou '', 'library_zk', 'reader'
    form.mode(pm='display')
    write_permission(pm='cmf.ManagePortal')
    pm = schema.List(
        title=_(u'Diskuze ŽK a čtenáře'),
        required=False,
        missing_value=[],
        default=[],
        value_type=schema.Text(),
        #value_type=IPMItemBase,  # {date, sender, text}
        #value_type=schema.Object(IPMItemBase),  # {date, sender, text}
    )

    # pm: datum a čas poslední odpovědi
    form.mode(pm_date='display')
    write_permission(pm_date='cmf.ManagePortal')
    pm_date = schema.Datetime(
        title=_(u'Datum a čas poslední zprávy'),
        required=False,
        missing_value=None,
        default=None,
    )

    # pm: kdo má nepřečtenou zprávu - obvyklé hodnoty jsou '', 'library_zk', 'reader'
    form.mode(pm_unread='display')
    write_permission(pm_unread='cmf.ManagePortal')
    pm_unread = schema.TextLine(
        title=_(u'Nepřečtená zpráva - příjemce'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # ŽK nepřečtený požadavek
    form.mode(unread_zk='display')
    write_permission(unread_zk='cmf.ManagePortal')
    unread_zk = schema.Bool(
        title=_(u'Nepřečtený požadavek - ŽK'),
        required=False,
        missing_value=False,
        default=False,
    )

    # Reader nepřečtený požadavek
    form.mode(unread_r='display')
    write_permission(unread_r='cmf.ManagePortal')
    unread_r = schema.Bool(
        title=_(u'Nepřečtený požadavek - Čtenář'),
        required=False,
        missing_value=False,
        default=False,
    )

    # Operator nepřečtený požadavek
    form.mode(unread_op='display')
    write_permission(unread_op='cmf.ManagePortal')
    unread_op = schema.Bool(
        title=_(u'Nepřečtený požadavek - Operátor'),
        required=False,
        missing_value=False,
        default=False,
    )

    # --------------------------------------------------------------------------
    # lhůty - další akce název

    # Na rozdíl od subticket.next_action_title není aktivně používáno.
    form.mode(next_action_title='display')
    write_permission(next_action_title='cmf.ManagePortal')
    next_action_title = schema.TextLine(
        title=_(u'Další akce'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # lhůty - další akce termín
    # Na rozdíl od subticket.next_action_title není aktivně používáno.
    form.mode(next_action_date='display')
    write_permission(next_action_date='cmf.ManagePortal')
    next_action_date = schema.Datetime(
        title=_(u'Termín další akce'),
        required=False,
        missing_value=None,
        default=None,
    )

    # lhůty - další akce poslední mailová notifikace
    # Je používáno pro cron check_tickets_paid_uncorfimed.
    form.mode(next_action_last_notification='display')
    write_permission(next_action_last_notification='cmf.ManagePortal')
    next_action_last_notification = schema.Datetime(
        title=_(u'Termín poslední notifikace'),
        required=False,
        missing_value=None,
    )

    # --------------------------------------------------------------------------
    # storno, reklamace

    form.mode(cancellation_big_ask_date='display')
    write_permission(cancellation_big_ask_date='cmf.ManagePortal')
    cancellation_big_ask_date = schema.TextLine(
        title=_(u'Datum požádání o velké storno'),
        description=_(u'Den, kdy čtenář nebo knihovník žádající knihovny požádal o velké storno.'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(cancellation_big_ask_authorized='display')
    write_permission(cancellation_big_ask_authorized='cmf.ManagePortal')
    cancellation_big_ask_authorized = schema.Bool(
        title=_(u'Knihovník ŽK provedl nebo autorizoval žádost o velké storno.'),
        required=False,
        missing_value=False,
        default=False,
    )

    form.mode(cancellation_big_answer_date='display')
    write_permission(cancellation_big_answer_date='cmf.ManagePortal')
    cancellation_big_answer_date = schema.TextLine(
        title=_(u'Datum odmítnutí žádosti o velké storno'),
        description=_(u'Den, kdy knihovník žádající knihovny odmítl žádost o velké storno, např. z důvodu odmítnutí žádosti o malé storna.'),
        required=False,
        missing_value=None,
        default=None,
    )

    # --------------------------------------------------------------------------
    # Velká reklamace (reklamace objednávky)
    # FIXME-COMPLAINT velká reklamace je částečně deprecated, protože pro EDD
    # i čtenář může vytvořit malou reklamaci a jiné případy nenastanou, protože
    # se čtenář nepřihlásí.  A knihovník může vše řešit pomocí malé reklamace
    # a institut velké reklamace je overkill v současné podobě ZÍSKEJ.

    form.mode(complaint_date='display')
    write_permission(complaint_date='cmf.ManagePortal')
    complaint_date = schema.TextLine(
        title=_(u'Reklamace - datum a čas'),
        required=False,
        missing_value=None,
        default=None,
    )

    form.mode(complaint_text='display')
    write_permission(complaint_text='cmf.ManagePortal')
    complaint_text = schema.TextLine(
        title=_(u'Reklamace - text'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # reader | zk
    form.mode(complaint_who='display')
    write_permission(complaint_who='cmf.ManagePortal')
    complaint_who = schema.TextLine(
        title=_(u'Reklamace - kým'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # new | accepted | declined | objected | objection_accepted | objection_declined
    form.mode(complaint_state='display')
    write_permission(complaint_state='cmf.ManagePortal')
    complaint_state = schema.TextLine(
        title=_(u'Reklamace - kým'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(objection_text='display')
    write_permission(objection_text='cmf.ManagePortal')
    objection_text = schema.TextLine(
        title=_(u'Námitka - text'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    form.mode(objection_decision_text='display')
    write_permission(objection_decision_text='cmf.ManagePortal')
    objection_decision_text = schema.TextLine(
        title=_(u'Rozhodnutí o námitce - text'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # --------------------------------------------------------------------------
    # důvody (např. zamítnutí) a upřesnění

    form.mode(reject_reason='display')
    write_permission(reject_reason='cmf.ManagePortal')
    reject_reason = schema.TextLine(
        title=_(u'Důvod zamítnutí ŽK'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # --------------------------------------------------------------------------
    # MVS

    # --------------------------------------------------------------------------
    # MRS

    # json
    form.mode(mrs_data='display')
    write_permission(mrs_data='cmf.ManagePortal')
    mrs_data = schema.Text(
        title=_(u'Data pro MRS'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # --------------------------------------------------------------------------
    # EDD

    # json
    # EDD data json dict keys
    # edd: is_article, pages_from, pages_to, pages_number, pages_html,
    #      pages_str, doc_title_in, doc_quote
    #      doc_number_year, doc_number_pyear, doc_number_pnumber
    # edd selection: doc_volume
    # edd auto: doc_id_in, is_container
    # počítané: pages_number, pages_html, pages_str
    form.mode(edd_data='display')
    write_permission(edd_data='cmf.ManagePortal')
    edd_data = schema.Text(
        title=_(u'Data pro EDD'),
        required=False,
        missing_value=u"",
        default=u"",
    )

    # json
    # EDD Dília data json dict keys
    # dilia: rok_vydani, zeme_vydani, vydavatel,
    #        is_kolektiv,
    #        autor_jmeno_1, autor_prijmeni_1,
    #        autor_jmeno_2, autor_prijmeni_2,
    #        autor_jmeno_3, autor_prijmeni_3,
    form.mode(edd_dilia_data='display')
    write_permission(edd_dilia_data='cmf.ManagePortal')
    edd_dilia_data = schema.Text(
        title=_(u'Data pro Dília XML report (EDD)'),
        required=False,
        missing_value=u"",
        default=u"",
    )


class Ticket(TicketBase):
    implements(ITicket)

    # --------------------------------------------------------------------------
    # identification

    @property
    def ticket_id(self):
        return u'{}'.format(self.getId())

    @property
    def hid_prefix(self):
        LABELS = dict(
            mvs = u'MVA',
            mrs = u'MVA',  # FIXME-MRS
            edd = u'EDA',
        )
        return LABELS.get(self.ticket_type, self.ticket_type)

    @property
    def ticket_type_label(self):
        LABELS = dict(
            mvs = u'MVS',
            mrs = u'MRS',
            edd = u'EDD',
        )
        return LABELS.get(self.ticket_type, self.ticket_type)

    @property
    def is_mvs(self):
        return bool(self.ticket_type == 'mvs')

    @property
    def is_mrs(self):
        return bool(self.ticket_type == 'mrs')

    @property
    def is_edd(self):
        return bool(self.ticket_type == 'edd')

    @property
    def library_and_reader(self):
        value = self.library
        if self.reader:
            value.append(self.reader)
        return value

    @property
    def library(self):
        value = []
        if self.library_zk:
            value.append(self.library_zk)
        if self.library_dks:
            for sigla in self.library_dks:
                value.append(sigla)
        return value

    # SUGGESTION aby fungovalo subticket a subtickets včetně absolute_url, je
    # asi nutné dodat i request, ale je možné odejít, pokud se nepoužije self,
    # ale portal pomocí plone.api a následně getattr:
    # ...
    # ticket = getattr(portal.tickets, self.getId(), None)
    # subticket = getattr(ticket, self.subticket_id, None)

    # @property
    # def subticket(self):
    #     if not self.subticket_id:
    #         return None
    #     value = getattr(self, self.subticket_id, None)
    #     print('subticket ' + str(value))
    #     return value

    # @property
    # def subtickets(self):
    #     if not self.subticket_ids:
    #         return []
    #     value = []
    #     for subticket_id in self.subticket_ids:
    #         subticket = getattr(self, subticket_id, None)
    #         if not subticket:
    #             continue
    #         value.append(subticket)
    #     print('subtickets ' + str(value))
    #     try:
    #         print('tmp ' + value[0].absolute_url())
    #         print('tmp ' + value[0].getId())
    #     except:
    #         print('tmp failed')
    #     return value

    @property
    def fullname(self):
        doc_title = self.doc_title
        if not doc_title and self.is_edd:
            doc_title = self.doc_fullname
        LIMIT = 48
        if len(doc_title) > LIMIT:
            doc_title = doc_title[:LIMIT] + u'...'
        return u'{shortname}: {doc_title}'.format(
                shortname=self.shortname,
                doc_title=doc_title,
                )

    @property
    def shortname(self):
        return u'{hid_prefix}{hid}'.format(
            hid_prefix=self.hid_prefix,
            hid=self.hid,
            )

    # --------------------------------------------------------------------------
    # computed attr

    @property
    def created_dt(self):
        """Nyní se pro vrácení této hodnoty používají Zope metadata, to se
        může v budoucnu změnit, pokud se změní, pak stačí změnit jen zde.
        """

        return self.created()

    @property
    def created_iso(self):
        return self.created_dt.ISO()

    @property
    def modified_dt(self):
        """Nyní se pro vrácení této hodnoty používají Zope metadata, to se
        může v budoucnu změnit, pokud se změní, pak stačí změnit jen zde.
        """

        return self.modified()

    @property
    def modified_iso(self):
        return self.modified_dt.ISO()

    # --------------------------------------------------------------------------
    # reader attr

    @property
    def is_created_by_reader(self):
        """Whether this ticket is created by reader or not.

        Right now it cannot have reader attr unless it is created by reader but
        it could be changed in future.
        """

        return bool(self.reader)

    @property
    def is_reader(self):
        return bool(self.reader)

    @property
    def is_reader_approved(self):
        return bool(self.reader_approval == u'ok')

    @property
    def reader_approval_label(self):
        labels = {u'': u'zatím neověřen',
                  u'ok': u'ověřen',
                  u'stop': u'neověřen, ale objednávka nezamítnuta',
                  u'reject': u'neověřen',
                  }
        return labels.get(self.reader_approval, u'?')
    

    # @property
    # def is_notify_reader(self):
    #     if not self.reader:
    #         return False
    #     portal = api.portal.get()
    #     reader = getattr(portal.readers, self.reader, None)
    #     if not reader:
    #         return False
    #     if not reader.is_notify_reader:
    #         return False
    #     return True

    def reader_notification_email(self):
        #FIXME
        if not self.reader:
            return None
        portal = api.portal.get()
        reader = getattr(portal.readers, self.reader, None)
        if not reader:
            return None
        return reader.notification_email()

    @property
    def reader_fullname(self):
        if self.reader:
            portal = api.portal.get()
            reader = getattr(portal.readers, self.reader, None)
        if self.reader and reader:
            return reader.fullname
        if self.reader_firstname or self.reader_lastname:
            value = u' '.join((self.reader_firstname, self.reader_lastname, ))
            return value.strip()
        if self.reader:
            return self.reader
        return u''

    def reader_library_id(self):
        if self.reader:
            portal = api.portal.get()
            reader = getattr(portal.readers, self.reader, None)
        if self.reader and reader:
            return reader.reader_library_id
        return u''

    def eppn_library(self):
        """Vrací eppn čtenáře, část za @."""
        if self.reader:
            portal = api.portal.get()
            reader = getattr(portal.readers, self.reader, None)
        if self.reader and reader:
            return reader.eppn_library
        return u''

    # --------------------------------------------------------------------------
    # lb_state

    @property
    def lb_type(self):
        if self.is_lb_manual:
            return 'manual'
        if self.is_lb_auto:
            return 'auto'
        if self.is_lb_nodk:
            return 'nodk'
        return 'none'

    @property
    def is_lb_manual(self):
        return self.lb_state.startswith('manual:')

    @property
    def is_lb_auto(self):
        return self.lb_state.startswith('auto:')

    @property
    def is_lb_nodk(self):
        return self.lb_state.startswith('nodk:')

    @property
    def is_lb_undecided(self):
        return bool(self.lb_type == 'none')

    @property
    def is_lb_auto_and_can_order(self):
        return bool(
            self.lb_state in (u'auto: load availability finished OK',
                                u'auto: load availability order accepted',
                                u'auto: last closed', ) and
            (self.doc_order or self.doc_order_suggested)
            )

    @property
    def is_lb_ready_for_assignment(self):
        if self.is_lb_manual:
            return bool(self.lb_manual_library)
        if self.is_lb_auto:
            return bool(self.doc_order)
        return False

    def set_lb_state(self, state):
        # Intial state
        if state == u'initial state':  # ToDo
            self.lb_state = u''

        # # No DK
        # elif state == u'no dk':
        #     self.lb_state = u'nodk: current'

        # Manual DK
        elif state == u'manual library chosen':  # OK
            self.lb_state = u'manual: chosen'
            # remove dk order (approved by librarian)
            self.doc_order = u''
            self.doc_order_sent = u''
            # remove dk order (suggested by LB alg)
            self.doc_order_suggested = u''
            self.doc_order_suggested_data = u''
        elif state == u'manual library removed':  # OK
            self.lb_state = u''
            self.lb_manual_library = u''
            self.lb_manual_library_sent = u''
        elif state == u'manual library created':  # OK
            self.lb_state = u'manual: current'
            self.lb_manual_library = u''
            self.lb_manual_library_sent = u''
        elif state == u'manual library closed':  # OK
            self.lb_state = u'manual: last closed'
            self.lb_manual_library = u''
            self.lb_manual_library_sent = u''

        # Auto LB
        elif state == u'load availability init':  # OK
            self.lb_state = u'auto: load availability init'
            self.lb_manual_library = u''
            self.lb_manual_library_sent = u''
        elif state == u'load availability finished':  # OK
            if self.doc_order_suggested:
                lb_state_new = u'auto: load availability finished OK'
            else:
                lb_state_new = u'auto: load availability finished failed'
            if self.lb_state != lb_state_new:
                if self.doc_order_suggested:
                    doc_order_suggested = self.doc_order_suggested.replace(u',', u', ')
                    self.add_historylog(u'Knihovník zvolil automatické přidělování požadavků a systém načetl dostupnost a navrhl pořadí: {}'.format(doc_order_suggested), who=['zk'])
                else:
                    self.add_historylog(u'Knihovník zvolil automatické přidělování požadavků a nejsou k dispozici žádné dokumenty k zapůjčení, které by vyhovovaly nastavení systému ZÍSKEJ.', who=['zk'])

            self.lb_state = lb_state_new
            self.lb_manual_library = u''
            self.lb_manual_library_sent = u''
        elif state == u'load availability order accepted':  # OK
            self.lb_state = u'auto: load availability order accepted'
            self.lb_manual_library = u''
            self.lb_manual_library_sent = u''
        elif state == u'auto lb created':  # OK
            self.lb_state = u'auto: current'
            self.lb_manual_library = u''
            self.lb_manual_library_sent = u''
        elif state == u'auto lb closed':  # OK
            self.lb_state = u'auto: last closed'

        else:
            logger.error('Uknown state for ticket.set_lb_state')
            logger.info(state)
            raise Exception('Invalid internal state')

    # --------------------------------------------------------------------------
    # substate based on attrs

    @property
    def is_reader_lent(self):
        return bool(self.reader_lent_date)

    @property
    def is_reader_return(self):
        return bool(self.return_date)

    @property
    def is_complaint(self):
        return bool(self.complaint_date)

    @property
    def complaint_who_label(self):
        who_label = dict(
            reader = u'Čtenář',
            zk = u'Žádající knihovna za čtenáře',
            )
        return who_label.get(self.complaint_who, u'')

    @property
    def complaint_text_label(self):
        text_label = dict(
            complaint_wrong = u'došlo něco jiného',
            )
        return text_label.get(self.complaint_text, u'')

    @property
    def complaint_label(self):
        if not self.is_complaint:
            return u''
        value = u'{who} reklamuje: {text}.'.format(
            who = self.complaint_who_label,
            text = self.complaint_text_label,
            )
        if self.complaint_state == u'new':
            value += u'  Čeká se na rozhodnutí.'
        elif self.complaint_state == u'accepted':
            value += u'  Reklamace přijata.'
        elif self.complaint_state == u'declined':
            value += u'  Reklamace odmítnuta.'
        elif self.complaint_state == u'objected':
            value += u'  Reklamace odmítnuta, což bylo rozporováno,  Nyní vyřizuje Servisní centrum ZÍSKEJ.'
        elif self.complaint_state == u'objection_accepted':
            value += u'  Reklamace odmítnuta, což bylo rozporováno a Servisní centrum ZÍSKEJ reklamaci uznalo.'
        elif self.complaint_state == u'objection_declined':
            value += u'  Reklamace odmítnuta, což bylo rozporováno a Servisní centrum ZÍSKEJ reklamaci zadmítlo.'
        return value

    @property
    def is_big_complaint_undecided(self):
        """Effectivelly stop any important changes.
        """

        if self.wf_review_state in ('closed', 'cancelled', 'rejected', ):
            return False
        return bool(self.complaint_state in (u'new', u'objected', ))

    # can "velké storno" directly
    @property
    def can_cancel_directly(self):
        if self.wf_review_state in ('created', 'pending', ):
            return True

        if self.wf_review_state in ('assigned', ):
            wf_status = self.wf_status
            subticket_review_state = wf_status['subticket_review_state']
            if subticket_review_state in ('queued', 'conditionally_accepted', ):
                return True

        return False

    # can "velké storno" directly - reader
    @property
    def can_cancel_directly_reader(self):
        if self.wf_review_state not in ('created', ):
            return False

        # Není potřeba testovat, zda je objednávka vytvořena čtenářem a pokud
        # ano, zda je ve stavu (pro čtenáře) paid nebo accepted, stačí otestovat
        # na podmínklu, která by v takových případech byla splněna vždy a pokud
        # splněna není, tak velké storno čtenářem možné je.
        if self.payment_paid_date:
            return False

        return True

    # can "velké storno" even if only ask for it
    @property
    def can_cancel_ask_or_directly(self):
        if self.is_big_cancel_declined:
            return False
        if self.wf_review_state in ('created', 'assigned', 'pending', ):
            return True
        return False

    def deactivate_subticket(self):
        self.subticket_id = None
        self.library_dk = u''
        self.library_dk_title = u''
        # assuming reindex will be done

    @property
    def is_big_cancel_asked(self):
        """Effectivelly stop any important changes and accept or decline
        cancellation request.
        """

        if self.wf_review_state in ('closed', 'cancelled', 'rejected', ):
            return False
        if self.cancellation_big_ask_date is None:
            return False
        if self.cancellation_big_answer_date is None:
            return True
        if self.cancellation_big_answer_date >= self.cancellation_big_ask_date:
            return False
        return True

    @property
    def is_big_cancel_declined(self):
        """Byla žádost o storno odmítnuta?
        
        Používá se pro: Žádost o velké storno odmítnutá - neumožnit zažádat
        znovu.
        """

        if self.cancellation_big_answer_date and \
                self.cancellation_big_ask_date and \
                self.cancellation_big_answer_date >= self.cancellation_big_ask_date and \
                self.wf_review_state != 'cancelled':
            return True
        return False

    @property
    def cancel_label(self):
        value = u''
        if self.wf_review_state == 'cancelled' and not self.is_complaint:
            value = u'Objednávka byla stornována'
        elif self.wf_review_state == 'cancelled' and self.is_complaint:
            pass
        elif self.is_big_cancel_declined:
            value = u'Žádost o storno objednávky byla zamítnuta.'
            # u'Žádost o storno objednávky byla zamítnuta.  Ale nyní není žádný aktivní požadavek, zvažte zda nepřehodnotit zamítnutí.'
        elif self.is_big_cancel_asked:
            value = u'Žádost o storno objednávky byla podána a čeká se na odpověď.'
        return value

    @property
    def complaint_cancel_label(self):
        values = []
        # Storno
        value = self.cancel_label
        if value:
            values.append(value)
        # Reklamace
        value = self.complaint_label
        if value:
            values.append(value)
        # Formátování
        value = u', '.join(values)
        if not value:
            value = u''
        return value

    # --------------------------------------------------------------------------
    # workflow state

    @property
    def wf_status(self):
        """Vrátí workflow status dict doplněný o workflow review_state label"""

        # ticket
        wf_tool = api.portal.get_tool('portal_workflow')
        status = wf_tool.getStatusOf("ticketmvs_workflow", self)
        review_state = status["review_state"]
        status["review_state_label"] = get_ticket_wf_label(review_state, review_state)
        status["review_state_label_only_ticket"] = status["review_state_label"]
        dt = status["time"]
        status["ts"] = dt.strftime('%d.%m.%Y %H:%M')

        # subticket
        subticket_id = self.subticket_id
        if subticket_id:
            subticket = getattr(self, subticket_id, None)
        else:
            subticket = None
        if subticket:
            status['subticket'] = subticket.wf_status
        else:
            status['subticket'] = None
        if status['subticket']:
            status["subticket_review_state"] = status['subticket']['review_state']
        else:
            status["subticket_review_state"] = None

        # subticket proto změna ticket (jen label)
        if status['subticket_review_state']:
            status["subticket_review_state_label"] = status['subticket']['review_state_label']

            INCLUDE_TICKET_WF_STATE_LABEL = True
            if INCLUDE_TICKET_WF_STATE_LABEL:
                # label = u'4) je dodáváno: 2) objednáno, čeká na schválení dožádanou knihovnou'
                status["review_state_label"] = u'{t}: {st}'.format(
                    t=status["review_state_label"], 
                    st=status["subticket_review_state_label"])
            else:
                # label = u'2) objednáno, čeká na schválení dožádanou knihovnou'
                if review_state == 'subticket':
                    status["review_state_label"] = status["subticket_review_state_label"]

        else:
            status["subticket_review_state_label"] = None

        return status

    @property
    def wf_review_state(self):
        """Vrátí workflow review_state."""
        return self.wf_status['review_state']

    @property
    def wf_review_state_label(self):
        """Vrátí workflow review_state label."""
        return self.wf_status['review_state_label']

    @property
    def wf_review_state_label_reader(self):
        """Vrátí workflow review_state label."""
        return self.wf_review_state_label

    @property
    def wf_review_state_label_only_ticket(self):
        """Vrátí workflow review_state label."""
        return self.wf_status['review_state_label_only_ticket']

    @property
    def is_open_for_reader(self):
        """Vrátí zda je objednávka stále otevřená z hlediska čtenáře.  Jen pro
        Získej API pro CPK,
        """

        return self.status_reader_cpkapi['is_open']

    @property
    def review_state_reader_label(self):
        return self.status_reader_cpkapi['status_label']

    @property
    def status_reader_cpkapi(self):
        """Vrátí hodnoty pro detail ticketu v Získej API pro CPK.

        Dokumentace Získej API pro CPK

        Mapování na status_label:

            {
            'created': 'Vytvořená',
            'paid': 'Uhrazená',
            'accepted': 'Ve zpracování',
            'prepared': 'Připravena',
            'lent': 'Vypůjčena',
            'closed': 'Uzavřena',
            'cancelled': 'Stornována',
            'rejected': 'Zamítnuta',
            }

        Z pohledu čtenáře je typický průběh následující:

            1) podána žádost o objednávku, zatím nepřijata = created
            2) zaplacen poplatek = paid
            3) žádost přijata = accepted
            4) objednávka připravena k vyzvednutí = prepared
            5) objednávka vyzvednuta, zatím nevrácena = lent
            6) objednávka vrácena = closed

        Alternativní průběh zahrnuje stavy:

            - žádost o objednávku zamítnuta = rejected
            - žádost o objednávku / objednávka stornována = cancelled

        API obsahuje položku is_open, která určuje, zda je objednávka otevřená nebo uzavřená.  Uzavřená je po vrácení knihy, stornu, odmítnutí.
        """

        # FIXME-EDD-II

        # Získej API pro CPK používá tyto status_reader a příslušný label
        API_LABELS = {
            'created': u'Vytvořená',  # CPK UI: Vytvořená
            'paid': u'Uhrazená',  # CPK UI: Uhrazená
            'accepted': u'Ve zpracování',  # CPK UI: Ve zpracování
            'prepared': u'Připravena',  # CPK UI: Připravena
            'lent': u'Vypůjčena',  # CPK UI: Vypůjčena
            'closed': u'Uzavřena',  # CPK UI: Uzavřena
            'cancelled': u'Stornována',  # CPK UI: ?
            'rejected': u'Zamítnuta',  # CPK UI: ?
            }

        # ticket workflow status nabývá hodnot
        # created, assigned, pending, prepared, closed, cancelled, rejected
        # proto je potřeba ticket workflow status namapovat na status_reader,
        # stačí jen rozdíly
        STATUS_READER_MAPPING = dict(
            assigned = u'accepted',
            pending = u'accepted',
            )

        status_reader = self.wf_review_state
        status_reader = STATUS_READER_MAPPING.get(status_reader, status_reader)

        # navíc je potřeba načíst dodatečné informace pro rozlišení nová
        # nezaplacená a zaplacená)
        if status_reader == u'created':
            if self.payment_paid_date:
                status_reader = u'paid'

        # pro čtenáře created a zaplacená a schválený čtenář
        # nastav accepted (ve zpracování)
        if status_reader == u'paid' and \
                self.reader_approval in  (u'ok', u'stop', ):
            status_reader = u'accepted'

        # navíc je potřeba načíst dodatečné informace pro rozlišení připraveno,
        # půjčeno a vráceno tzn. uzavřenp)
        if status_reader == u'prepared':
            if self.is_reader_lent:
                status_reader = u'lent'
            if self.is_reader_return:
                status_reader = u'closed'

        # připraveno pro čtenáře nezačíná převodem do stavu připraveno, ale
        # začátkem dne, kdy může vyzvednout, pokud ještě nenastalo, změnit na
        # accepted / Ve zpracování, ale jen pokud už mezitím nebylo půjčeno,
        # protoje tato úprava až zde
        if status_reader == u'prepared':
            if not self.reader_from_date:  # nemělo by nastat
                status_reader = u'accepted'
            elif self.reader_from_date > DateTime().ISO()[:10]:  # today
                status_reader = u'accepted'

        # nyní už máme vše připraveno
        return dict(
            status_reader = status_reader,
            status_label = API_LABELS.get(status_reader, status_reader),
            is_open = status_reader not in ('rejected', 'cancelled', 'closed', ),
            )

    def status_reader_history_cpkapi(self, status_reader=None):
        # FIXME-EDD-II

        result = []

        if status_reader is None:
            status_reader = self.status_reader_cpkapi['status_reader']

        # created = Nová, nezaplacená = Vytvořená
        # created_date, Datum vytvoření
        result.append(dict(
            id = u'created',
            date = self.created_date,
            ))

        # paid = Nová, zaplacená = Uhrazená
        # payment_paid_date dt
        if self.payment_paid_date:
            result.append(dict(
                id = u'paid',
                date = u'{}'.format(self.payment_paid_date.ISO()[:10]),
                ))

        # accepted = Ve zpracování
        # accepted_date, Datum přijetí
        if self.accepted_date:
            result.append(dict(
                id = u'accepted',
                date = self.accepted_date,
                ))

        # prepared = Vyřízena = Připravena
        # reader_from_date, K dispozici od
        # Pokud je reader_from_date v budoucnu, status_reader zůstává
        # na accepted
        if status_reader != 'accepted' and self.reader_from_date:
            result.append(dict(
                id = u'prepared',
                date = self.reader_from_date,
                ))

        # lent = Vypůjčena
        # reader_lent_date, Půjčeno čtenáři
        if self.reader_lent_date:
            result.append(dict(
                id = u'lent',
                date = self.reader_lent_date,
                ))

        # final state:
        # closed = Uzavřena
        # cancelled = Stornována = ?
        # rejected = Zamítnuta = ?
        # return_date, Vráceno čtenářem
        # closed_date, Datum uzavření
        if self.return_date:
            result.append(dict(
                id = u'closed',
                date = self.return_date,
                ))
        elif self.closed_date:
            status_reader = self.status_reader_cpkapi['status_reader']
            if status_reader in ('rejected', 'cancelled', 'closed', ):
                result.append(dict(
                    id = status_reader,
                    date = self.closed_date,
                    ))

        return result

    @property
    def status_apiaks(self):
        API_LABELS = {
            'created': u'Vytvořená',
            'assigned': u'Ve zpracování',
            'pending': u'Ve zpracování',
            'prepared': u'Připravena',
            'lent': u'Vypůjčena',
            'closed': u'Uzavřena',
            'cancelled': u'Stornována',
            'rejected': u'Zamítnuta',
            }

        status = self.wf_review_state

        # je potřeba načíst dodatečné informace pro rozlišení připraveno,
        # půjčeno a vráceno tzn. uzavřenp)
        if status == u'prepared':
            if self.is_reader_lent:
                status = u'lent'
            if self.is_reader_return:
                status = u'returned'

        # nyní už máme vše připraveno
        return dict(
            status = status,
            status_label = API_LABELS.get(status, status),
            is_open = status not in ('rejected', 'cancelled', 'closed', ),
            )

    @property
    def is_open_apiaks(self):
        return bool(
            self.wf_review_state not in ('rejected', 'cancelled', 'closed', ))

    def is_open_or_closed_in_apiaks(self, closed_in):
        wf_review_state = self.wf_review_state
        if wf_review_state not in ('rejected', 'cancelled', 'closed', ):
            return True
        return bool(DateTime(self.closed_date) + closed_in + 1 > DateTime())

    def wf_history(self):
        """Vrátí workflow history - seznam jednotlivých workflow status, každý
        z nich dict doplněný o workflow review_state label.
        """

        wf_tool = api.portal.get_tool('portal_workflow')
        history = wf_tool.getHistoryOf("ticketmvs_workflow", self)
        result = []
        for one in history:
            # one = {'action': None, 'review_state': 'draft', 'actor': 'admin', 'comments': '', 'time': DateTime('2017/10/23 22:15:59.599496 GMT+2')}
            review_state = one["review_state"]
            one['review_state_label'] = get_ticket_wf_label(review_state, review_state)
            dt = one["time"]
            one["ts"] = dt.strftime('%d.%m.%Y %H:%M')
            result.append(one)
        return result

    # unused
    # def ts_wf_last(self):
    #     """ts_wf_last aka timestamp workflow last change"""
    #     wf_tool = api.portal.get_tool('portal_workflow')
    #     status = wf_tool.getStatusOf("ticketmvs_workflow", self)
    #     return status["time"]

    # unused
    # def ts_wf_limit(self):
    #     """ts_wf_limit aka timestamp workflow limit for next action"""
    #     return None

    # --------------------------------------------------------------------------
    # next action

    def Next_action_date(self):
        dt = self.next_action_date
        if not dt:
            return u''
        return format_date(dt, is_time=True, is_html=True)

    def set_next_action(self, na_type=None, na_value=None, na_text=None):
        """Nastaví další akci na text na_text a datum podle na_type resp. pro
        některé na_type i podle na_value.

        na_type = None .. vymaže další akci
        na_type = 'date_iso' .. nastaví datum na datum podle na_value, kde
                                očekává string ve tvaru '2017-11-15'
        na_type = '1wd' .. nastaví datum na datum +1 pracovní den od teď (včetně
                           času)
        na_type = '28d' .. nastaví datum na datum +28 dbů od teď (včetně času)

        do budoucna přidat:
        na_type = 'wd' .. nastaví datum na datum podle na_value na na_value
                          pracovních dnů ode dneška
        na_type = 'd' .. nastaví datum na datum podle na_value na na_value dnů
                         ode dneška

        SUGGESTION ZIS-54
        """

        if na_type not in (None, 'date_iso', '1wd', '28d', ):
            logger.error('Unknown next action type ' + na_type)
            raise Exception('Unknown next action type')
        if not na_type:
            na_date = None
            na_text = u''
        elif na_type == 'date_iso':
            # na_value = '2017-11-15'
            na_date = DateTime(na_value)
        elif na_type == '1wd':
            na_date = datetime_add_wd(1)
        elif na_type == '28d':
            na_date = datetime_add_d(28)

        self.next_action_title = na_text
        self.next_action_date = na_date
        self.reindexObject()

    # --------------------------------------------------------------------------
    # helpers

    def add_historylog(self, value, who=None):
        # vždy zapisujeme do hlavního audit logu pro servisní středisko
        self.historylog = format_historylog_entry(
            prev=self.historylog,
            value=value,
            )
        # a někdy i do historie pro ŽK
        if who and 'zk' in who:
            self.historylog_zk = format_historylog_entry(
                prev=self.historylog_zk,
                value=value,
                )
        # nebo dokonce i do historie pro čtenáře
        if who and 'r' in who:
            self.historylog_r = format_historylog_entry(
                prev=self.historylog_r,
                value=value,
                )

    def get_historylog(self, who=None):
        if not who:
            return u''
        value = u''
        if who == 'operator':
            value = self.historylog
        elif who == 'library_zk':
            value = self.historylog_zk
        elif who == 'reader':
            value = self.historylog_r
        if not value:
            return u''
        values = value.split('\n')
        values.reverse()
        result = u''
        for value in values:
            if not value:
                continue
            result += u'<li>{}</li>\n'.format(value)
        result = u'<ul>{}</ul>\n'.format(result)
        return result

    def update_edd_data_if_changed(self, **obj_dict):
        """EDD článek i výňatek: pages_from, pages_to, doc_title_in, doc_quote,
        doc_number_year, doc_number_pyear, doc_number_pnumber.
        """

        # EDD data json dict keys
        # edd: is_article, pages_from, pages_to, pages_number, pages_html,
        #      pages_str, doc_title_in, doc_quote
        #      doc_number_year, doc_number_pyear, doc_number_pnumber
        # edd selection: doc_volume
        # edd auto: doc_id_in, is_container
        # počítané: pages_number, pages_html, pages_str

        # nedovolit změnit: is_article, doc_id_in, is_container
        # měnit automaticky: pages_number, pages_html, pages_str
        #   viz self._get_edd_calculated_attr()

        ALLOWED_ATTRS = (
            'pages_from', 'pages_to',
            'doc_title_in',
            'doc_number_year', 'doc_number_pyear', 'doc_number_pnumber',
            'doc_volume',
            'doc_quote',
            )

        edd_data = self.Edd_data()
        is_updated = False
        for k in obj_dict:
            if k not in ALLOWED_ATTRS:
                logger.error('update_attrs_if_changed key {} is not allowed'.format(k))
                raise Exception('Internal error')
            v_orig = getattr(edd_data, k, None)
            v = obj_dict.get(k, None)
            if v_orig != v:
                edd_data[k] = v
                is_updated = True
        if is_updated:
            edd_data = self._get_edd_calculated_attr(edd_data)
            edd_data_json = json.dumps(edd_data)
            self.edd_data = edd_data_json
        return is_updated

    def update_edd_dilia_data_if_changed(self, **obj_dict):
        """Keys: rok_vydani, zeme_vydani, vydavatel, is_kolektiv,  autor_prijmeni_1,
        autor_jmeno_1, autor_prijmeni_2, autor_jmeno_2, autor_prijmeni_3, autor_jmeno_3
        """

        print("obj_dict", obj_dict)
        ALLOWED_ATTRS = (
            'autor_jmeno_1', 'autor_prijmeni_1',
            'autor_jmeno_2', 'autor_prijmeni_2',
            'autor_jmeno_3', 'autor_prijmeni_3',
            'is_kolektiv',
            'vydavatel', 'rok_vydani', 'zeme_vydani',
            )

        dilia = self.Edd_Dilia_data()
        if dilia is None:
            dilia = dict()
        is_updated = False
        for k in obj_dict:
            if k not in ALLOWED_ATTRS:
                logger.error('update_attrs_if_changed key {} is not allowed'.format(k))
                raise Exception('Internal error')
            v_orig = getattr(dilia, k, None)
            v = obj_dict.get(k, None)
            if v_orig != v:
                dilia[k] = v
                is_updated = True
        if is_updated:
            dilia_json = json.dumps(dilia)
            self.edd_dilia_data = dilia_json
        return is_updated

    def update_attrs_if_changed(self, **obj_dict):
        ALLOWED_ATTRS = (
            'reader_lid', 'library_zk_note', 'reader_firstname',
            'reader_from_date','reader_lastname', 'reader_to_date',
            'reader_place','reader_fee','reader_note', 'cena', 'report_date',
            'reader_approval', 'doc_title', 'doc_number', 'doc_author',
            'doc_issuer', 'doc_isbn', 'doc_issn', 'doc_signature', 'doc_dk_id',
            'rfid', 'barcode', 'reader_email',)
        is_updated = False
        for k in obj_dict:
            if k not in ALLOWED_ATTRS:
                print('update_attrs_if_changed key {} is not allowed'.format(k))
                logger.error('update_attrs_if_changed key {} is not allowed'.format(k))
                raise Exception('Internal error')
            v_orig = getattr(self, k, None)
            v = obj_dict.get(k, None)
            if v_orig != v:
                setattr(self, k, v)
                #logger.info('doit for {k}: {v_orig} to {v}'.format(k=k, v_orig=v_orig, v=v))
                is_updated = True
        return is_updated

    # --------------------------------------------------------------------------
    # pm

    def add_pm_auto(self, text):
        return self.add_message_txt(u'library_zk', text)

    def add_message_txt(self, sender, text):
        """Přidat zprávu mezi ŽK a čtenářem.  Neposílá notifikace."""
        if DEBUG:
            print('add_message_txt({sender}, text)'.format(sender=sender))
            print('text:')
            pprint(text)
            print(' ')

        # validation, sanitization
        text = sanitize_input_text(text)
        if DEBUG:
            print('sanitize_input_text(text):')
            pprint(text)
            print(' ')
        now = DateTime()

        # save message
        #obj_str = u'{sender}\n{iso}\n{text}'.format(sender=sender, iso=safe_utf8(now.ISO()), text=text)
        obj_str = u'{sender}\n{iso}\n{text}'.format(sender=sender, iso=safe_unicode(now.ISO()), text=text)
        if self.pm is None:
            self.pm = []

        # self.pm.append() nefunguje
        pm = [x for x in self.pm]
        pm.append(obj_str)
        self.pm = pm

        self.pm_date = now
        if sender == 'library_zk':
            self.pm_unread = u'reader'
        else:
            self.pm_unread = u'library_zk'

        return True

    def get_messages(self, render_html=True, ziskej_api=False):
        if DEBUG:
            print('get_messages(render_html={render_html})'.format(render_html=render_html))

        if self.pm is None or len(self.pm) == 0:
            return []
        messages = []
        for item in self.pm:
            lines = item.split('\n', 2)
            sender = lines[0]
            date = lines[1]
            text = lines[2]
            if ziskej_api:
                messages_item = dict(
                    sender = sender,
                    created_datetime = DateTime(date).ISO(),
                    text = text,
                    )
            elif render_html:
                if sender == u'reader':
                    sender = u'Čtenář'
                else:
                    sender = u'Knihovna'
                date = DateTime(date).strftime('%-d.%-m.%Y %-H:%M')
                text = nl2br(text)
                messages_item = dict(
                    sender = sender,
                    date = date,
                    text = text,
                    )
            messages.append(messages_item)
            if ziskej_api:
                messages[-1]['unread'] = False
        messages.reverse()
        if ziskej_api and len(messages) and self.pm_unread == 'reader':
            for idx in range(len(messages)):
                if messages[idx]['sender'] == 'library_zk':
                    messages[idx]['unread'] = True
                    break
        return messages

    def get_messages_counts(self, unread_by='reader'):
        count_messages = 0
        count_messages_unread = 0
        if self.pm is not None:
            count_messages = len(self.pm)
            if self.pm_unread == unread_by:
                count_messages_unread = 1
        return dict(
            count_messages = count_messages,
            count_messages_unread = count_messages_unread,
            )

    # --------------------------------------------------------------------------
    # Plone and getters

    def Title(self):
        return self.fullname

    def Mrs_data(self):
        if self.ticket_type != 'mrs':
            return None
        if not self.mrs_data:
            return None
        try:
            value = json.loads(self.mrs_data)
        except:
            value = None
        return value

    def Edd_data(self):
        """EDD článek i výňatek: pages_from, pages_to, doc_title_in, doc_quote,
        doc_number_year, doc_number_pyear, doc_number_pnumber.
        """

        if self.ticket_type != 'edd':
            return None
        if not self.edd_data:
            return None
        try:
            edd = json.loads(self.edd_data)
        except:
            edd = None
        return edd

    def Edd_Dilia_data(self):
        """Keys: rok_vydani, zeme_vydani, vydavatel,is_kolektiv, autor_prijmeni_1 autor_jmeno_1
        autor_prijmeni_2 autor_jmeno_2, autor_prijmeni_3, autor_jmeno_3
        """

        if self.ticket_type != 'edd':
            return None
        if not self.edd_dilia_data:
            return None
        try:
            dilia = json.loads(self.edd_dilia_data)
        except:
            dilia = None
        return dilia

    def Is_Dilia_data(self):
        dilia = self.Edd_Dilia_data()
        if not dilia:
            return False
        MANDATORY_FIELDS = (
            'rok_vydani', 'zeme_vydani', 'vydavatel',
            'autor_prijmeni_1', 'autor_jmeno_1', )
        for k in MANDATORY_FIELDS:
            if not dilia.get(k, None):
                return False
        return True

    def ticket_doc_data_source(self):
        if self.doc_manual:
            return "manual"
        else:
            return "auto"

    @property
    def is_edd_article(self):
        edd = self.Edd_data()
        if not edd:
            return True
        return edd.get('is_article', True)

    @property
    def edd_subtype(self):
        edd = self.Edd_data()
        if not edd:
            return None
        if edd.get('is_article', True):
            return u'article'
        else:
            return u'selection'

    @property
    def is_edd(self):
        return self.ticket_type == 'edd'

    @property
    def is_mvs(self):
        return self.ticket_type == 'mvs'

    def edd_reader_url(self):
        token = self.reader_token
        if not token:
            return None
        result = u'{}/edd_download?token={}'.format(self.absolute_url(), token)
        # print("self.absolute_url():", self.absolute_url())
        # print("edd_reader_url:", result)
        return result

    # reader_lid / Reader_lid - unicode issue
    # https://reinout.vanrees.org/weblog/2008/10/14/unicodedecodeerror-in-plones-catalog.html
    def Reader_lid(self):
        value = self.reader_lid
        if not value:
            value = u''
        #value = safe_utf8(value)
        return value

    def Library_zk_note(self):
        value = self.library_zk_note
        if not value:
            value = u''
        #value = safe_utf8(value)
        value = nl2br(value)
        return value

    def Reader_firstname(self):
        value = self.reader_firstname
        if not value:
            value = u''
        #value = safe_utf8(value)
        value = nl2br(value)
        return value

    def Reader_lastname(self):
        value = self.reader_lastname
        if not value:
            value = u''
        #value = safe_utf8(value)
        value = nl2br(value)
        return value

    # --------------------------------------------------------------------------
    # Plone and getters

    def _render_html_attr(self, data, key, labels, is_bool=False, lov=None,
            default=u''):
        html = u''
        html += u'<span class="render_key">{}:<span>\n'.format(labels.get(key, key))
        if is_bool:
            if data.get(key, default):
                value = u'Ano'
            else:
                value = u'Ne'
        elif lov:
            value = data.get(key, default)
            value = lov.get(value, value)
        else:
            value = data.get(key, default)
        html += u'<span class="render_value">{}<span>\n'.format(value)
        html = u'<p>{}<p>\n\n'.format(html)
        return html

    def render_mrs(self, snippet_id=None, attrs=None):
        html = u''
        if not self.is_mrs:
            return html

        LABELS = dict(
            sent = u'Způsob dodání',
            orgname = u'Jméno organizace',
            name = u'Jméno a příjmení žadatele',
            street = u'Ulice a č.p.',
            city = u'Město',
            zip = u'PSČ',
            mail = u'E-mail',
            phone = u'Telefon',
            ic = u'IČ',
            article = u'Název článku / kapitoly / (části dokumentu)',
            range = u'Rozsah stran',
            color = u'Provedení kopií: černobíle / barevně',
            singleside = u'Provedení kopií: jednostranné / oboustranné',
            keepsize = u'Provedení kopií: zachovat měřítko ano / ne',
            )

        mrs = self.Mrs_data()
        if not mrs:
            return html

        if snippet_id == u'ticket_reader':
            if mrs.get('sent', None) == u'sent':
                attrs = ['sent', 'orgname', 'name', 'street', 'city', 'zip', 'mail', 'phone', 'ic', ]
            else:
                attrs = ['sent', 'mail', 'phone', 'ic', ]
        elif snippet_id == u'ticket_doc':
            attrs = ['article', 'range', 'color', 'singleside', 'keepsize', ]

        for attr in attrs:
            lov = None
            if attr == 'sent':
                lov = dict(
                    pick = u'Osobní vyzvednutí (ve své knihovně)',
                    sent = u'Zaslání poštou',
                    )
            html += self._render_html_attr(mrs, attr, LABELS, 
                is_bool=attr in (u'color', u'singleside', u'keepsize', ),
                lov=lov)
        return html

    def render_edd(self, snippet_id=None, attrs=None):
        html = u''
        if not self.is_edd:
            return html

        if snippet_id == u'ticket_reader':
            subticket_id = self.subticket_id
            if subticket_id:
                subticket = getattr(self, subticket_id, None)
            else:
                subticket = None

            html += u'<i class="fa fa-credit-card" aria-hidden="true"></i>&nbsp;\n'
            if self.cena and subticket and subticket.pages_number_dk:
                fee = self.cena
                pages_number = subticket.pages_number_dk
                html += u'Cena pro čtenáře: {}&nbsp;Kč<br>' \
                    u'<small>(z toho pro DK {}&nbsp;Kč, pro Dílii ' \
                    u'{}&nbsp;Kč)</small><br>\n'.format(
                        fee, pages_number, fee - pages_number)
            else:
                edd_data = self.Edd_data()
                if edd_data['pages_number']:
                    estimate_fee = edd_data['pages_number'] * 5
                    if estimate_fee < 10:
                        estimate_fee = 10
                    elif estimate_fee > 75:
                        estimate_fee = 75
                    estimate_fee_dilie = estimate_fee
                    estimate_fee_dk = edd_data['pages_number']
                    estimate_fee += estimate_fee_dk
                    html += u'Odhadovaná cena pro čtenáře je: {}&nbsp;Kč<br>' \
                        u'<small>(z toho pro DK {}&nbsp;Kč, pro Dílii ' \
                        u'{}&nbsp;Kč)</small><br>\n'.format(
                            estimate_fee, estimate_fee_dk, estimate_fee_dilie)
                else:
                    html += u'Cena pro čtenáře zatím nebyla stanovena<br>\n'
            if self.payment_id:
                # html += u'[Testování] Platba id: {}<br>\n'.format(
                #     self.payment_id)
                if self.payment_paid_date:
                    html += u'Zaplaceno: {}<br>\n'.format(
                        self.payment_paid_date.strftime('%-d.%-m.%Y'))
                if subticket:
                    print("download_expiration_date", subticket.download_expiration_date)
                # PDF soubor a jeho expirace - pokud neprobíhá reklamace
                if subticket and subticket.complaint_text and \
                        subticket.complaint_state not in (
                            'new', 'declined', 'objected',
                            'objection_declined', 'soft_done', ):
                    pass
                elif subticket and subticket.download_expiration_date:
                    html += u'<i class="fa fa-file-pdf-o" aria-hidden="true"></i> '
                    if subticket.is_pdf_file_unexpired():
                        html += u'PDF soubor<br><small>expiruje dne: '
                    else:
                        html += u'PDF soubor<br><small>expiroval dne: '
                    # download_expiration_date .. python datetime
                    html += u'{}</small><br>\n'.format(
                        format_date(
                            DateTime(
                                subticket.download_expiration_date.strftime(
                                    '%Y-%m-%d %-H:%M:%S')),
                            is_time=True, is_html=True, omit_year=False))
                    if subticket.complaint_state == 'soft_done':
                        edd_url = u'{}/edd_download?download=zk'.format(
                            self.absolute_url())
                        html += u'<small>Ověření kvality po reklamaci – ' \
                            u'<a href="{}">stáhnout pdf</a></small>' \
                            u'<br>\n'.format(edd_url)
                    elif not subticket.downloaded_number:
                        pass
                    elif subticket.is_pdf_file_complaintable():
                        edd_url = u'{}/edd_download?download=zk'.format(
                            self.absolute_url())
                        html += u'<small>Ověření kvality pro reklamaci – ' \
                            u'<a href="{}">stáhnout pdf</a></small>' \
                            u'<br>\n'.format(edd_url)
                    else:
                        html += u'<small>Ověření kvality pro reklamaci už ' \
                            u'není k dispozici, uplynula lhůta od expirace.' \
                            u'</small><br>\n'
                if self.payment_paid_date:
                    downloaded_number = 0
                    if not subticket:
                        pass
                    elif subticket.complaint_state == 'soft':
                        html += u'Reklamace byla přijata a čeká se na nové ' \
                            u'dodání<br>\n'
                    elif subticket.complaint_state == 'soft_ready':
                        html += u'Reklamace byla přijata a čeká se na nové ' \
                            u'dodání<br>\n'
                    else:
                        if subticket.complaint_state == 'soft_done':
                            html += u'Reklamace byla přijata a vyřízena<br>\n'
                        if subticket.downloaded_number:
                            downloaded_number = subticket.downloaded_number
                        html += u'[Testování] Počet stažení: {}<br>\n'.format(
                            downloaded_number,
                            )
                        if subticket.downloaded_number:
                            downloaded_date_str = u''
                            if subticket.downloaded_date:
                                downloaded_date_str = \
                                    subticket.downloaded_date.strftime(
                                        '%d.%m.%Y')
                            html += u'[Testování] Naposledy staženo: {}<br>\n' \
                                u''.format(
                                    downloaded_date_str,
                                    )
                elif self.payment_platebator_uuid:
                    html += u'[Testování] Zatím nezaplaceno, <a href="{}?set_platebator_paid_edd=test">simulovat zaplacení</a><br>\n'.format(
                        self.absolute_url())
                edd_reader_url = self.edd_reader_url()
                if edd_reader_url:
                    html += u'[Testování] <a href="{}" target="_blank">' \
                        u'pohled čtenáře</a><br>\n'.format(edd_reader_url)
            else:
                html += u'[Testování] Předpis platby zatím nebyl vytvořen<br>\n'

        elif snippet_id in (u'ticket_doc', u'subticket_doc'):

            edd = self.Edd_data()

            # if edd['is_article']:
            #     html += u'Typ: Článek<br>\n'
            # else:
            #     html += u'Typ: Výňatek z monografie<br>\n'
            if edd.get('doc_quote', None):
                html += u'<i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp;\n'
                html += u'Zadaná citace: {}<br>\n'.format(edd['doc_quote'])
            # if self.doc_author:
            #     html += u'Autor: {}<br>\n'.format(self.doc_author)
            # if self.doc_title:
            #     html += u'Název: {}<br>\n'.format(self.doc_title)
            if edd.get('pages_html', None):
                html += u'<i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;\n'
                html += u'Strany: {}<br>\n'.format(edd['pages_html'])

            # if edd.get('doc_title_in', None):
            #     if edd['is_article']:
            #         html += u'Název periodika: {}<br>\n'.format(edd['doc_title_in'])
            #     else:
            #         html += u'Název monografie: {}<br>\n'.format(edd['doc_title_in'])
            # if self.doc_issuer:
            #     html += u'Místo a rok vydání: {}<br>\n'.format(self.doc_issuer)
            # if edd['is_article']:
            #     if edd.get('doc_number_year', None):
            #         html += u'Rok: {}<br>\n'.format(edd['doc_number_year'])
            #     if edd.get('doc_number_pyear', None):
            #         html += u'Ročník: {}<br>\n'.format(edd['doc_number_pyear'])
            #     if edd.get('doc_number_pnumber', None):
            #         html += u'Číslo: {}<br>\n'.format(edd['doc_number_pnumber'])
            #     if self.doc_issn:
            #         html += u'ISSN: {}<br>\n'.format(self.doc_issn)
            # else:
            #     if self.doc_isbn:
            #         html += u'ISBN: {}<br>\n'.format(self.doc_isbn)
            if snippet_id == u'ticket_doc':
                subticket_id = self.subticket_id
                if subticket_id:
                    subticket = getattr(self, subticket_id, None)
                else:
                    subticket = None
                if subticket and subticket.pages_number_dk:
                    html += u'<i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;\n'
                    html += u'Skutečný počet stran: {}'.format(subticket.pages_number_dk)

        elif snippet_id == u'ticket_main_subticket':
            subticket_id = self.subticket_id
            if subticket_id:
                subticket = getattr(self, subticket_id, None)
            else:
                subticket = None
            if subticket:
                html += u'<i class="fa fa-credit-card" aria-hidden="true"></i>&nbsp;\n'
                if subticket.cena:
                    html += u'Cena DK: {} Kč<br>\n'.format(subticket.cena)
                else:
                    html += u'Cena DK zatím nebyla stanovena<br>\n'.format(self.cena)

        return html

    def _set_edd_cena_if_changed(self, cena):
        """Nastavit EDD cenu, pokud došlo k její změně."""

        if self.cena == cena:
            return False
        self.cena = cena
        return True

    def update_edd_cena(self):
        """Spočítat aktuální cenu EDD pro čtenáře na zákaldě aktivního
        subticketu a jeho skutečného počtu stránek.

        Cena u EDD je složená ze dvou částí.

        Poplatek Dília:  Články i výňatky: 5 Kč/str, za objednávku min. 10 Kč
        a max 75 Kč.  Pro výňatky limit 20 stran.

        Samotná knihovna 1 Kč / stranu.  A pouze tuto část připsat DK.
        """

        if not self.is_edd:
            return

        subticket_id = self.subticket_id
        if subticket_id:
            subticket = getattr(self, subticket_id, None)
        else:
            subticket = None
        if not subticket or not subticket.pages_number_dk:
            self._set_edd_cena_if_changed(None)
            return

        pages_number = subticket.pages_number_dk
        cena = pages_number * 5
        if cena < 10:
            cena = 10
        elif cena > 75:
            cena = 75

        # Cena pro čtenáře
        cena += pages_number
        self._set_edd_cena_if_changed(cena)

    def calculate_and_update_edd_data(self):
        """edd_data attr (json) má některé key:value hodnoty počítané z jiných.
        Zahodit dříve dopočítané a dopočítat je.  Pracuje přímo s edd_data attr.
        """

        edd_data = self.Edd_data()
        edd_data = self._get_edd_calculated_attr(edd_data)
        edd_data_json = json.dumps(edd_data)
        self.edd_data = edd_data_json

    def _get_edd_calculated_attr(self, edd_data):
        """Pro edd_data dict aktualizovat pages_number, pages_html a pages_str
        na základě pages_from a pages_to.  Nepracuje s ticket.edd_data attr.
        """

        edd_data['pages_number'] = None
        edd_data['pages_html'] = u''
        edd_data['pages_str'] = u''
        pages_from = edd_data.get('pages_from', None)
        pages_to = edd_data.get('pages_to', None)

        if not pages_from:
            return edd_data

        if pages_to:
            pages_number = pages_to - pages_from + 1
            edd_data['pages_html'] = u'{} &ndash; {} ({})'.format(
                pages_from,
                pages_to,
                pages_number,
                )
            edd_data['pages_str'] = u'{} - {} ({})'.format(
                pages_from,
                pages_to,
                pages_number,
                )
        else:
            pages_number = 1
            edd_data['pages_html'] = u'{} ({})'.format(
                pages_from,
                pages_number,
                )
            edd_data['pages_str'] = edd_data['pages_html']
        edd_data['pages_number'] = pages_number

        return edd_data

    def fix(self):
        result = u''
        if self.docs_data:
            docs_data = json.loads(self.docs_data)
            if 'None' in docs_data['doc_ids']:
                print("fix", self.hid, " ", docs_data['doc_ids'])
                doc_ids = []
                for item in docs_data['doc_ids']:
                    if item == u'None':
                        result += u'docs_data doc_ids string None removed'
                        continue
                    doc_ids.append(item)
                docs_data['doc_ids'] = doc_ids
                self.docs_data = json.dumps(docs_data, ensure_ascii=False)
                self._p_changed = True
        return result

    def export_as_dict(self):
        data = dict(id = self.id)
        names = [
            'ticket_id', 'ticket_type', 'ticket_type_label',
            'hid', 'hid_prefix',
            'library_zk', 'library_dk', 'library_dks',
            'library_zk_title', 'library_dk_title',
            'subticket_id', 'subticket_ids',
            'created_by', 'is_created_by_cpk_for_reader', 'is_paid_in_advance',
            'reader_notification_email',
            'librarian',
            'created_iso', 'modified_iso',
            'created_date', 'reader_date', 'approved_date', 'reader_from_date',
            'reader_to_date', 'reader_mail_date', 'reader_lent_date',
            'return_date', 'closed_date',
            'report_date',
            'modified_date',
            'reader', 'reader_library_id', 'reader_lid', 'reader_approval',
            'reader_email',
            'cena', 'reader_fee',
            'library_zk_note',
            'reader_note', 'reader_place', 'reader_firstname', 'reader_lastname',
            'historylog', 'historylog_zk', 'historylog_r',
            'doc_manual', 'doc_id', 'doc_title', 'doc_fullname', 'doc_author',
            'doc_number', 'doc_issuer', 'doc_isbn', 'doc_issn', 'doc_signature',
            'doc_note',
            'doc_data', 'docs_data',
            'get_unit_ids',
            'doc_dk_id', 'rfid', 'barcode',
            'lb_state', 'lb_manual_library', 'lb_manual_library_sent',
            'doc_order', 'doc_order_sent', 'doc_order_suggested',
            'doc_order_suggested_data', 'doc_order_blacklist',
            'pm', 'pm_date', 'pm_unread',
            'unread_zk', 'unread_r', 'unread_op',
            'next_action_title', 'next_action_date',
            'cancellation_big_ask_date', 'cancellation_big_ask_authorized',
            'cancellation_big_answer_date',
            'complaint_date', 'complaint_text', 'complaint_who', 'complaint_state',
            'objection_text', 'objection_decision_text',
            'reject_reason',
            'mrs_data', 'edd_data', 'edd_dilia_data',
            'payment_id', 'payment_platebator_uuid', 'payment_service_id',
            'payment_amount',
            'payment_created_date', 'payment_paid_date',
            ]
        names_dt = [
            'report_date', 'modified_date', 'pm_date', 'next_action_date',
            'payment_created_date', 'payment_paid_date',
            ]
        names_json = ['doc_data', 'docs_data', 'doc_order_suggested_data',
            'mrs_data', 'edd_data', 'edd_dilia_data', ]
        for name in names:
            value = getattr(self, name, None)
            if callable(value):
                value = value()
            if name in names_dt and value is not None:
                value = value.strftime('%Y-%m-%d %-H:%M:%S')
            if name in names_json and value is not None:
                try:
                    value = json.loads(value)
                except:
                    pass
            data[name] = value
        return data

    # --------------------------------------------------------------------------
    # Získej API pro CPK pro čtenáře

    def is_created_by_cpk_for_reader(self):
        """Vytvořeno čtenářem pomocí CPK prostřednictvím Získej API pro CPK pro
        čtenáře.
        """

        return self.created_by and self.created_by == "cpk_reader"

    def is_paid_in_advance(self):
        """Zaplaceno čtenářem pomocí Platebátoru po vytvoření objednávky.
        """

        return bool(self.payment_paid_date is not None)

    # --------------------------------------------------------------------------
    # Platby a platebátor

    def reader_payment_create_mvs(self, payment_id):
        """Vytvořit předpis platby čtenáře za objednávku MVS."""
        if self.payment_id:
            return

        self.payment_id = payment_id
        self.payment_platebator_uuid = None
        self.payment_service_id = u'MVS'  # MVS, REPRO, EDD, MMS
        self.payment_amount = 70
        self.payment_created_date = DateTime()
        self.payment_paid_date = None

    def reader_pay_by_platebator_mvs(self, date_dt):
        """Právě bylo zjištěno, že už bylo zaplaceno čtenářem.  Parametr date_dt
        obsahuje aktuální datum, ale kdyby později Platebátor API začalo
        poskytovat skutečné datum zaplacení, bylo by ho možné použít a nastavit
        datum zaplacení i zpětně.

        Vrací seznam notifikací, které se mají poslat.
        """

        notification_ids = []

        if self.payment_paid_date:
            self.add_historylog(u'Objednávka byla zaplacena již dříve a systém se snaží o nastavení datumu zaplacení znovu, objednávka beze změny.')
            return notification_ids

        # datum platby
        self.payment_paid_date = date_dt
        self.add_historylog(u'Objednávka byla čtenářem zaplacena.', who=['zk', 'r'])

        notification_ids.append('wf_t_r_paid_reader')
        if self.reader_approval == u'ok':
            self.accepted_date = DateTime().ISO()[:10]
            self.add_historylog(u'Stav pro čtenáře byl změněn na accepted.')
            notification_ids.append('wf_t_r_paid_and_accepted_reader')
        return notification_ids

    def reader_payment_create_edd(self, payment_id, amount):
        """Vytvořit předpis platby čtenáře za objednávku EDD."""
        if self.payment_id:
            return

        self.payment_id = payment_id
        self.payment_platebator_uuid = None
        self.payment_service_id = u'EDD'  # MVS, REPRO, EDD, MMS
        self.payment_amount = amount
        self.payment_created_date = DateTime()
        self.payment_paid_date = None

    def reader_pay_by_platebator_edd(self, date_dt):
        """Právě bylo zjištěno, že už bylo zaplaceno čtenářem.  Parametr date_dt
        obsahuje aktuální datum, ale kdyby později Platebátor API začalo
        poskytovat skutečné datum zaplacení, bylo by ho možné použít a nastavit
        datum zaplacení i zpětně.

        Vrací seznam notifikací, které se mají poslat.
        """

        notification_ids = []

        if self.payment_paid_date:
            self.add_historylog(u'Objednávka byla zaplacena již dříve a systém se snaží o nastavení datumu zaplacení znovu, objednávka beze změny.')
            return notification_ids

        # datum platby
        self.payment_paid_date = date_dt
        self.add_historylog(u'Objednávka byla čtenářem zaplacena.', who=['zk', 'r'])

        # notifikace
        notification_ids.append('wf_t_edd_r_paid_reader')
        return notification_ids

    def LibraryZk(self):
        """Vrátí objekt knihovny ŽK k tomuto ticketu."""
        portal = api.portal.get()
        library = getattr(portal.libraries, self.library_zk, None)
        return library

    def mvs_zk_enough_available(self):
        """Může být vytvořen požadavek z hlediska, zda je dostatek kreditu
        v Paysys (NTK krtedit).  Nebo je placení z kreditu vypnuto.  Např.
        protože čtenář zaplatil předem.
        """

        # Pokud zaplatí čtenář předem 70 Kč pomocí Platebátoru, jsou všechny
        # kontroly a upozornění ohledně kreditu ŽK vypnuty.
        if self.is_paid_in_advance():
            return True

        # if not ENFORCE_PAYSYS:
        #     return True

        library = self.LibraryZk()
        if not library:
            return False

        # NTK nepoužívá NTK kredit
        if library.sigla == 'ABA013':
            return True
        disponibilni = library.paysys_credit_info()
        print("mvs_zk_enough_available disponibilni:", repr(disponibilni))
        if not disponibilni:
            return False
        return bool(disponibilni >= 70)

    # SUGGESTION je na dvou místech, dát jen na jedno
    def platebator_url(self):
        """Vygeneruje URL, kam přesměrovat čtenáře pro zaplacení.  Nejedná se
        o adresu Platebátor API.

        Příklad TEST:

        http://platebator.test.ntkcz.cz/app/0532f320-15bd-11ea-982d-525400fed44e
        """

        platebator_uuid = self.payment_platebator_uuid
        if not platebator_uuid:
            #logger.info('platebator_url empty because platebator_uuid empty')
            return u''

        if not USE_PLATEBATOR:
            #logger.info('platebator_url empty because not USE_PLATEBATOR')
            return u''

        value = u'{}{}'.format(PLATEBATOR_BASE_URL, platebator_uuid)
        #logger.info(u'platebator_url: {}'.format(value))
        return value

    def reader_payment_create_edd_dummy(self, payment_id):
        """Nastavit data pro předpis platby čtenáře za objednávku EDD."""
        if self.payment_id:
            return

        self.payment_id = payment_id
        self.payment_platebator_uuid = None
        self.payment_service_id = u'EDD'  # MVS, REPRO, EDD, MMS
        self.payment_amount = self.cena
        self.payment_created_date = DateTime()
        self.payment_paid_date = None

    def reader_payment_created_edd_dummy(self, payment_platebator_uuid):
        """Simulovat vytvoření předpisu platby za objednávku EDD čtenářem."""
        self.payment_platebator_uuid = payment_platebator_uuid

    def reader_pay_by_platebator_edd_dummy(self, date_dt):
        """Simulovat zaplacení předpisu platby za objednávku EDD čtenářem."""

        if self.payment_paid_date:
            self.add_historylog(u'Objednávka byla zaplacena již dříve a systém se snaží o nastavení datumu zaplacení znovu, objednávka beze změny.')
            return

        # datum platby
        self.payment_paid_date = date_dt
        self.add_historylog(u'Objednávka byla čtenářem zaplacena.  (jen simulace)', who=['zk', 'r'])


    # --------------------------------------------------------------------------
    # Dostupnost (AutoLB, unit_ids, availability...)

    def get_unit_ids(self):
        """Vrátit všechny id jednotek jako prostý seznam."""
        unit_ids = []
        if self.ticket_doc_data_source() != 'auto':
            return unit_ids
        doc_data = None
        if self.doc_data:
            try:
                doc_data = json.loads(self.doc_data)
            except:
                pass
        docs_data = None
        if self.docs_data:
            try:
                docs_data = json.loads(self.docs_data)
            except:
                pass
        if doc_data and 'unit_ids' in doc_data:
            for unit_id in doc_data['unit_ids']:
                unit_ids.append(unit_id)
        if docs_data and 'data' in docs_data and 'doc_ids' in docs_data:
            for doc_id in docs_data['doc_ids']:
                item = docs_data['data'].get(doc_id, None)
                if item is None or 'unit_ids' not in item:
                    continue
                for unit_id in item['unit_ids']:
                    unit_ids.append(unit_id)
        return unit_ids
