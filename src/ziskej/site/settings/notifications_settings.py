# -*- coding: utf-8 -*-
"""Wrapper nad notifications_settings.json poskytující notifications_settings
dictionary.
"""

import codecs
import json
import os.path
import logging

from ziskej.site import ZISKEJ_SITE_DIR


logger = logging.getLogger("notifications_settings")

notifications_settings = dict()
is_error = False

# Načíst notifications_settings.json
notifications_settings_json = None
if not ZISKEJ_SITE_DIR:
    logger.error('Missing buildout configuration, using default for ZISKEJ_SITE_DIR')
    is_error = True
    ZISKEJ_SITE_DIR = '/zope/ziskej/src/ziskej.site/src/ziskej/site'
filename = os.path.join(ZISKEJ_SITE_DIR, 'settings', 'notifications_settings.json')
if not os.path.isfile(filename):
    logger.error('File notifications_settings.json does not exist.')
    is_error = True
else:
    try:
        with codecs.open(filename, 'r', encoding='utf-8') as json_file:
            notifications_settings_json = json_file.read()
    except Exception, e:
        logger.error('Failed to open notifications_settings.json: {}'.format(e))
        is_error = True

# Obvyklé TYPO v notifications_settings.json
if notifications_settings_json and '{[hid]}' in notifications_settings_json:
    logger.error('Invalid notifications_settings.json, contains {[hid]} instead of [{hid}], fix it.')
    is_error = True

# Parsovat json z notifications_settings.json
notifications_settings_data = None
try:
    if notifications_settings_json:
        notifications_settings_data = json.loads(notifications_settings_json)
except Exception, e:
    logger.error('Failed to parse json for notifications_settings: {}'.format(e))
    is_error = True

# Vzít z něj seznam notifikací
notifications = []
try:
    if notifications_settings_data and \
            'notifications' in notifications_settings_data:
        notifications = notifications_settings_data['notifications']
        if not isinstance(notifications, list):
            notifications = []
except Exception, e:
    logger.error('Invalid data in json for notifications_settings: {}'.format(e))
    is_error = True

# Zpracovat tento seznam vč. validace
for notification in notifications:
    notification_id = notification.get('id', None)
    # Validovat notification_id
    if not notification_id:
        logger.error('Missing id in notifications_settings')
        is_error = True
        continue
    if notification_id in notifications_settings:
        logger.error('Duplicit id in notifications_settings: {}'.format(notification_id))
        is_error = True
        continue
    # Vytvořit novou položku, přejmenovat
    item = dict(
        obj = notification.get('context', None),
        recipients = [notification.get('recipient', None)],
        subject = notification.get('subject', None),
        text_plain = notification.get('message', None),
        )
    if notification.get('message_en', None):
        item['en_text_plain'] = notification.get('message_en', None)
    # Validovat
    if item['obj'] not in ('ticket', 'subticket', ):
        logger.error('Skip invalid item (invalid context) in notifications_settings for {}'.format(notification_id))
        is_error = True
        continue
    if item['recipients'][0] not in ('reader', 'library_zk', 'library_dk', 'operator', ):
        logger.error('Skip invalid item (invalid recipient) in notifications_settings for {}'.format(notification_id))
        is_error = True
        continue
    if not item['subject'] or not isinstance(item['subject'], basestring):
        logger.error('Skip invalid item (missing, empty or invalid subject) in notifications_settings for {}'.format(notification_id))
        is_error = True
        continue
    if not item['text_plain'] or not isinstance(item['text_plain'], basestring):
        logger.error('Skip invalid item (missing, empty or invalid message) in notifications_settings for {}'.format(notification_id))
        is_error = True
        continue
    if 'en_text_plain' in item and not isinstance(item['en_text_plain'], basestring):
        logger.error('Skip invalid item (invalid message_en) in notifications_settings for {}'.format(notification_id))
        continue
    # Přidat další validní notifikaci
    notifications_settings[notification_id] = item

if is_error:
    logger.info('------------------------------------------------------------------------')
    logger.info('Loading from notifications_settings.json finished with errors, see above.')
    logger.info('------------------------------------------------------------------------')
else:
    logger.info('Loading from notifications_settings.json finished successfully.')
