# -*- coding: utf-8 -*-
"""Pomocný nástroj pro rozesílání e-mailových notifikací ze Získej app.  V tuto
chvíli obsahuje jak funkcionalitu (logiku), tak konfiguraci a texty a to formou
Python dict struktury.  Pro snadnější spolupráci zvážit vyjmutí textů
a konfigurace a udržování v json / XML formátu.
"""

from __future__ import print_function

import logging
from types import ListType, TupleType

from DateTime import DateTime
from plone import api

from ziskej.site import BUILDOUT_TYPE, ZISKEJ_BASE_URL
from ziskej.site.library_helpers import lib_helper
from ziskej.site.settings.notifications_settings import notifications_settings
from ziskej.site.utils import send_full_mail_message
from ziskej.site.utils_lib import pprint
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger("ziskej.notifications")


DEBUG = False

SERVICE_SHORTNAMES = dict(
    mvs=u'MVS',
    mrs=u'MRS',
    edd=u'EDD',
    )

# Vypnout posílání mailů - vypnutí se aplikuje jen v development (local).
DEBUG_DISABLE_SENDMAIL = True
DEBUG_DISABLE_SENDMAIL = DEBUG_DISABLE_SENDMAIL and \
    bool(BUILDOUT_TYPE in ('development', ))

DEBUG_MISSING_NID = False

USE_REAL_EMAILS = bool(BUILDOUT_TYPE in ('prod', 'development', ))
ADD_AOW_MAILS = False
ADD_GMAIL_MAILS = bool(BUILDOUT_TYPE in ('test', 'development', ))
ADD_MZK_READER_MAIL = bool(BUILDOUT_TYPE in ('test', 'development', ))

PLAIN_MAIL_FORMAT = True

print(" ")
print("notifications DEBUG:", DEBUG)
print("notifications DEBUG_DISABLE_SENDMAIL:", DEBUG_DISABLE_SENDMAIL)
print("notifications DEBUG_MISSING_NID:", DEBUG_MISSING_NID)
print("notifications USE_REAL_EMAILS:", USE_REAL_EMAILS)
print("notifications ADD_AOW_MAILS:", ADD_AOW_MAILS)
print("notifications ADD_GMAIL_MAILS:", ADD_GMAIL_MAILS)
print("notifications ADD_MZK_READER_MAIL:", ADD_MZK_READER_MAIL)
print("notifications PLAIN_MAIL_FORMAT:", PLAIN_MAIL_FORMAT)
print(" ")


GMAIL_MAIL = dict(
    aba013 = u'ziskej.ntk+ntk@gmail.com',
    boa001 = u'ziskej.ntk+mzk@gmail.com',
    aba001 = u'ziskej.ntk+nk@gmail.com',
    operator = u'ziskej.ntk+operator@gmail.com',
    reader = u'ziskej.ntk+reader@gmail.com',
    )

MZK_READER_MAIL = u'paulusova@mzk.cz'


def get_gmail_recipient(id):
    if id in GMAIL_MAIL:
        return GMAIL_MAIL[id]
    return u'ziskej.ntk+{id}@gmail.com'.format(id=id)

class BaseNotifications(object):

    def __init__(self):
        self.portal = api.portal.get()
        self.sender = 'ziskej@techlib.cz'
        self.reply_to = self.sender
        self.bcc = 'auda.aow+bcc@gmail.com'
        self.url_base = ZISKEJ_BASE_URL

    def _send_mail(self, sender, recipient, subject, reply_to=None,
                   body_plain=None, body_html=None, attachments=None,
                   format=None, bcc=None, mail_format='PLAIN'):
        logger.info(u'Sending mail {subject} to {recipient}'.format(subject=subject, recipient=recipient))
        send_full_mail_message(sender,
                               recipient,
                               subject,
                               reply_to=reply_to,
                               body_plain=body_plain,
                               body_html=body_html,
                               attachments=attachments,
                               format=mail_format,
                               bcc=bcc)


class ErrorNotifications(BaseNotifications):

    def __init__(self):
        super(ErrorNotifications, self).__init__()

    def send_notifications(self, message):
        sender = self.sender
        recipient = 'auda.aow+error@gmail.com'
        subject = 'Ziskej error'
        reply_to = self.reply_to
        bcc = self.bcc

        mail_format = 'PLAIN'
        body_plain = message
        body_html = None
        attachments = None

        try:
            self._send_mail(sender, recipient, subject, reply_to=reply_to,
                            body_plain=body_plain, body_html=body_html,
                            attachments=attachments, format=mail_format,
                            bcc=bcc, mail_format=mail_format)
        except Exception, e:
            logger.exception('ErrorNotifications cannot notify: ' + str(e))
            return False
        return True


class ZiskejNotifications(BaseNotifications):

    def __init__(self, ticket=None, subticket=None):
        super(ZiskejNotifications, self).__init__()
        self.ticket = ticket
        self.subticket = subticket
        self._set_nid_cfg()

    def _set_nid_cfg(self):
        """hlavní konfigurace notifikací
        obj: ticket, subticket
        recipients: reader, library_zk, library_dk, operator (systém nahradí
                    za reálné e-mailové adresy)
        subject: předmět mailu
        text_plain: text mailu bez html
        text_html: text html verze mailu

        Nemělo by dojít k tomu, že je recipient reader a někdo jiný, protože je
        požadavek na speciální patičku mailu pro čtenáře.  Kromě toho by měl mít
        jinou terminologii.

        Legenda

        hid: Číslo objednávky / požadavku – Human ID
        service_shortname: Zkratka služby, pro MVS: MVS
        url: Adresa objednávky / požadavku (Získej UI)
        doc_title: Citace
        reader_shortname: Čtenář – krátké jméno, nyní nastaveno na reader_lid –
                          Library ID, tedy ID v rámci knihovny a jen v případě,
                          že není vyplněno, tak je použito reader_fullname.
        reader_fullname: Čtenář – plné jméno, nyní nastaveno na
                         Jméno Příjmení (reader_lid), např. tedy
                         Pepa Vonásek (12345)
        library_zk_sigla: Sigla žádající knihovny
        library_zk_shortname: Krátké jméno žádající knihovny, není-li nastaveno,
                              použije se plné jméno knihovny
        library_zk_fullname: Plné jméno žádající knihovny
        library_zk_card: Vizitka žádající knihovny:
                         Plné jméno\n\n  mail\n\n  tel
        library_dk_sigla: Sigla dožádané knihovny
        library_dk_shortname: Krátké jméno dožádané knihovny, není-li nastaveno,
                              použije se plné jméno knihovny
        library_dk_fullname: Plné jméno dožádané knihovny
        library_dk_card: Vizitka dožádané knihovny:
                         Plné jméno\n\n  mail\n\n  tel
        reject_reason: Důvod zamítnutí objednávky
        refuse_reason: Důvod odmítnutí požadavku
        sent_date: Datum odeslání / připravení požadavku dožádanou knihovnou
        back_date: Termín vrácení do dožádané knihovny
        fee: Skutečná cena požadavku - stanovená DK a nepřímo schválená ŽK
        created_date: Datum vytvoření objednávky
        reader_from_date: Den, od kterého je k dispozici pro čtenáře
        reader_to_date: Den, do kdy by měl čtenář vrátit
        reader_fee: Cena, za kterou posktyne MVS žádající knihovna čtenáři
        reader_fee_text: Pro objednávky vytvořené čtenářem (CPK UI) text:
                         Cena byla zaplacena předem.
                         pro ostatní text:
                         Cena za poskytnutou službu je {reader_fee} Kč.
        cpk_ui_url: https://www.knihovny.cz/MyResearch/Ziskej
        cpk_ui_hid_link: 100234 (https://www.knihovny.cz/MyResearch/ZiskejTicket/techlib.cz/84b607ab7eda4a90)
        platebator_url: https://platebator.test.ntkcz.cz/app/0b25f4c8-8942-11ea-b789-525400fed44e

        Patička mailu
        Patička mailu pro čtenáře: Děkujeme za využití služby ZÍSKEJ, kterou provozuje Národní technická knihovna.\n\nServisní centrum ZÍSKEJ můžete kontaktovat na ziskej@techlib.cz.\n
        Patička mailu pro ostatní: S pozdravem Servisní centrum ZÍSKEJ\n\nTel.: 232 002 777\nMail: ziskej@techlib.cz\n
        """

        # Nepoužívat NID_ALIAS pro api wf_t_cancel_by_reader_zk_api --> wf_t_cancel_by_reader_zk

        self.NID_ALIAS = dict(

            # schváleno knihovníkem ŽK - pro čtenáře a pro DK
            #wf_t_assign = ['wf_t_zk_assign_reader', 'wf_t_zk_assign_dk', ],

            # automaticky schváleno (např. po error_sent) - pro DK
            wf_t_assign_auto = 'wf_t_zk_assign_dk',

            # cron: check_tickets_paid_uncorfimed
            t_paid_uncorfimed_cron = ['t_paid_uncorfimed_cron_zk', 't_paid_uncorfimed_cron_op', ],

            # cron: check_subtickets_queued
            wf_st_queued_cron = ['wf_st_queued_cron_dk', 'wf_st_queued_cron_zk', 'wf_st_queued_cron_op', ],

            # automaticky schváleno (např. po retract) - pro DK
            wf_st_ca_retract_new = 'wf_t_zk_assign_dk',

            # automaticky schváleno (např. po refuse) - pro DK
            wf_st_refuse_new = 'wf_t_zk_assign_dk',

            # # objednávka stornována na základě jiné akce
            wf_t_cancel_r_zk = ['wf_t_cancel_r', 'wf_t_cancel_zk', ],

            # odmítnuto knihovníkem DK, ale až po přijetí a před odesláním - pro ŽK
            wf_st_accepted_refuse = 'wf_st_refuse',  # diskutovat

            # automaticky schváleno (např. po refuse, které bylo po accept) - pro DK
            wf_st_accepted_refuse_new = 'wf_t_zk_assign_dk',

            # odmítnuto knihovníkem DK, ale až po odeslání - pro ŽK
            wf_st_sent_refuse = 'wf_st_refuse',  # diskutovat

            # automaticky schváleno (např. po refuse, které bylo po odeslání) - pro DK
            wf_st_sent_refuse_new = 'wf_t_zk_assign_dk',

            )

        self.NID = notifications_settings

    def _format_date(self, date_str):
        if date_str:
            value = DateTime(date_str).strftime('%d.%m.%Y')
        else:
            value = u''
        return value


    def send_notifications(self, nid):
        """Konfiguraci a texty pro notifikace je užitečné mít na jednom místě.
        Jsou dva zdroje volání:

        1) akce uživatele: změna stavu, zpráva

        2) cron: blížící se termín, termín, po termínu

        Ad 1) akce uživatele:

        nid je notification id konkrétní akce uživatele, je srozumitelné, např.
        reader_creates_mvst, všechny texty a akce jsou v této metodě.

        příklad volání:

        self.send_notifications('reader_creates_mvst')
        self.send_notifications('reader_creates_mvst')
        """

        if nid in self.NID_ALIAS:
            nid = self.NID_ALIAS[nid]

        if isinstance(nid, (ListType, TupleType)):
            for nid_item in nid:
                self.send_notifications(nid_item)
            return

        NID = self.NID
        if nid not in NID:
            logger.warning('notify: Missing nid ' + nid)
            if DEBUG_MISSING_NID:
                raise Exception('notify: Missing nid ' + nid)
            return
        nid_cfg = NID[nid]

        if not nid_cfg['recipients']:
            logger.warning('notify: Ignoring nid ' + nid)
            return

        # build message
        message = dict()

        # obj
        if nid_cfg['obj'] == 'ticket':
            obj = self.ticket
        elif nid_cfg['obj'] == 'subticket':
            obj = self.subticket
        else:
            raise Exception('notify: Misconfigured nid ' + nid)
        if obj is None:
            raise Exception('notify: Missing (sub)ticket')

        service = obj.ticket_type
        if not service in SERVICE_SHORTNAMES:
            service = u'mvs'

        # ŽK
        library_zk_sigla = obj.library_zk
        if not library_zk_sigla:
            raise Exception('notify: Missing library_zk sigla')
        library_zk_sigla = library_zk_sigla.upper()
        library_zk = lib_helper.library_by_sigla(library_zk_sigla)
        if not library_zk:
            raise Exception('notify: Invalid library_zk for sigla {}'.format(library_zk_sigla))

        # DK
        library_dk = None
        library_dk_fullname = u''
        library_dk_shortname = u''
        library_dk_sigla = obj.library_dk
        if not library_dk_sigla:
            library_dk_sigla = u''
        if library_dk_sigla:
            library_dk_sigla = library_dk_sigla.upper()
            library_dk = lib_helper.library_by_sigla(obj.library_dk)
            if library_dk:
                library_dk_fullname = library_dk.fullname
                library_dk_shortname = library_dk.ShortName()

        # ticket_dict
        # notification_sc_contact = u'S pozdravem Servisní centrum ZÍSKEJ\n\nTel.: 232 002 777\nMail: ziskej@techlib.cz\n'
        # notification_sc_contact_reader = u'Děkujeme za využití služby ZÍSKEJ, kterou provozuje Národní technická knihovna.\n\nServisní centrum ZÍSKEJ můžete kontaktovat na ziskej@techlib.cz.\n'

        # Cron je volán pomocí localhost, potřebujeme tedy nepoužívat
        # absolute_url, pokud je v něm localhost.
        obj_url = obj.absolute_url()
        if 'localhost' in obj_url:
            obj_url = u'{}{}'.format(
                self.url_base,
                u'/'.join(obj.getPhysicalPath()[2:]),
                )

        library_zk_card = u'{fullname}\n\n  {mail}\n\n  {tel}'.format(
            fullname=library_zk.fullname,
            mail=library_zk.email,  # Obecný e-mail knihovny
            tel=library_zk.phone,  # Obecný telefon knihovny
            )
        if library_dk:
            library_dk_card = u'{fullname}\n\n  {mail}\n\n  {tel}'.format(
                fullname=library_dk.fullname,
                mail=library_dk.email,  # Obecný e-mail knihovny
                tel=library_dk.phone,  # Obecný telefon knihovny
                )
        else:
            library_dk_card = u''

        ticket_dict = dict(
            hid = obj.hid,
            service_shortname = SERVICE_SHORTNAMES.get(service, service),
            shortname = obj.shortname,
            url = obj_url,
            url_html = u'<a href="{url}">{url}</a>'.format(url=obj_url),
            sc_contact = ziskej_parameter('notification_sc_contact'),
            sc_contact_reader = ziskej_parameter('notification_sc_contact_reader'),
            reader_shortname = u'',
            reader_fullname = u'',
            library_zk_sigla = library_zk_sigla,
            library_zk_fullname = library_zk.fullname,
            library_zk_shortname = library_zk.ShortName(),
            library_zk_card = library_zk_card,
            library_dk_sigla = library_dk_sigla,
            library_dk_fullname = library_dk_fullname,
            library_dk_shortname = library_dk_shortname,
            library_dk_card = library_dk_card,
            doc_title = u'',
            reject_reason = u'',
            cpk_ui_url = u'https://www.knihovny.cz/MyResearch/Ziskej',
            )

        # ticket attributes
        if self.ticket:
            ticket_dict['doc_title'] = self.ticket.doc_title
            # Datum odeslání / připravení požadavku dožádanou knihovnou
            ticket_dict['created_date'] = self._format_date(self.ticket.created_date)
            # Den, od kterého je k dispozici pro čtenáře.
            ticket_dict['reader_from_date'] = self._format_date(self.ticket.reader_from_date)
            if self.ticket.is_mvs:
                # Den, do kdy by měl čtenář vrátit.
                ticket_dict['reader_to_date'] = self._format_date(self.ticket.reader_to_date)
                # Cena, za kterou posktyne MVS žádající knihovna čtenáři.
                ticket_dict['reader_fee'] = self.ticket.reader_fee
                if self.ticket.is_created_by_cpk_for_reader():
                    ticket_dict['reader_fee_text'] = u'Cena byla zaplacena předem.'
                else:
                    ticket_dict['reader_fee_text'] = u'Cena za poskytnutou službu je {reader_fee} Kč.'.format(
                        reader_fee=ticket_dict['reader_fee'])
                ticket_dict['platebator_url'] = self.ticket.platebator_url()
            elif self.ticket.is_edd:
                edd_reader_url = self.ticket.edd_reader_url()
                if edd_reader_url:
                    # edd_reader_url http://localhost:14180/ziskej/tickets/...
                    if 'localhost' in edd_reader_url:
                        edd_reader_url_idx = edd_reader_url.find('/', 16)
                        ticket_dict['edd_reader_url'] = u'{}{}'.format(
                            # https://ziskej.techlib.cz/
                            self.url_base,
                            # ziskej/tickets/...
                            edd_reader_url[edd_reader_url_idx+1:],
                            )
                    else:
                        ticket_dict['edd_reader_url'] = edd_reader_url
                else:
                    ticket_dict['edd_reader_url'] = u''
                print("ticket_dict['edd_reader_url']", ticket_dict['edd_reader_url'])
    
            # reader
            reader_id = self.ticket.reader
            if reader_id:
                reader = getattr(self.portal.readers, reader_id, None)
                if reader:
                    reader_lid = reader.reader_lid
                    if reader_lid:
                        ticket_dict['reader_shortname'] = reader_lid
                        ticket_dict['reader_fullname'] = \
                            u'{first_name} {last_name} ({reader_lid})'.format(
                                first_name = reader.first_name,
                                last_name = reader.last_name,
                                reader_lid = reader_lid,
                                )
                    else:
                        ticket_dict['reader_fullname'] = \
                            u'{first_name} {last_name}'.format(
                                first_name = reader.first_name,
                                last_name = reader.last_name,
                                reader_lid = reader_lid,
                                )
                        ticket_dict['reader_shortname'] = ticket_dict['reader_fullname']
                    ticket_dict['cpk_ui_hid_link'] = u'{hid} (https://www.knihovny.cz/MyResearch/ZiskejTicket/{eppn_library}/{ticket_id})'.format(
                        hid=self.ticket.hid,
                        eppn_library=reader.eppn_library(),
                        ticket_id=self.ticket.getId(),
                        )

            # reject_reason
            value = self.ticket.reject_reason
            if value == 'reject-reason-zk-available':
                ticket_dict['reject_reason'] = u'dostupné ve fondu'
            elif value == 'reject-reason-zk-impossible':
                ticket_dict['reject_reason'] = u'chybná citace a nepodaří se dohledat správně'
            elif value == 'reject-reason-zk-unavailable':
                ticket_dict['reject_reason'] = u'nelze realizovat'
            elif value == 'reject-reason-r-denied':
                ticket_dict['reject_reason'] = u'neověření čtenáře'

        # subticket attributes
        if self.subticket:
            # Datum odeslání / připravení požadavku dožádanou knihovnou
            ticket_dict['sent_date'] = self._format_date(self.subticket.sent_date)
            # Termín vrácení do dožádané knihovny
            ticket_dict['back_date'] = self._format_date(self.subticket.back_date)
            # Skutečná cena požadavku - stanovená DK a nepřímo schválená ŽK
            ticket_dict['fee'] = self.subticket.cena

            # Důvod odmítnutí požadavku by DK
            ticket_dict['refuse_reason'] = u''
            refuse_reason = self.subticket.refuse_reason
            if refuse_reason == 'refuse-reason-wrong-citation':
                ticket_dict['refuse_reason'] = u'chybná citace a nepodaří se dohledat správně'
            elif refuse_reason == 'refuse-reason-dk-impossible':
                ticket_dict['refuse_reason'] = u'nelze realizovat'
            elif refuse_reason == 'refuse-reason-dk-unavailable':
                ticket_dict['refuse_reason'] = u'momentálně nedostupné'

        # recipients
        message['recipients'] = []

        for recipient in nid_cfg['recipients']:

            if recipient == 'reader':
                if USE_REAL_EMAILS:
                    # FIXME-EDD ověřit, že pro asistované se použije e-mail
                    # zadaný při vytvářeníá objednávky
                    reader_mail = self.ticket.reader_notification_email()
                    if reader_mail:
                        message['recipients'].append(reader_mail)
                if ADD_AOW_MAILS:
                    message['recipients'].append('auda.aow+reader@gmail.com')
                if ADD_GMAIL_MAILS:
                    message['recipients'].append(
                        get_gmail_recipient(recipient))
                if ADD_MZK_READER_MAIL and self.ticket.library_zk == 'boa001':
                    message['recipients'].append(MZK_READER_MAIL)

            elif recipient == 'library_zk':
                if USE_REAL_EMAILS:
                    library = lib_helper.library_by_sigla(obj.library_zk)
                    if library:
                        message['recipients'].extend(
                            library.notify_emails_by_service_role(
                                service=service, role='zk'))
                if ADD_AOW_MAILS:
                    message['recipients'].append('auda.aow+library_zk@gmail.com')
                if ADD_GMAIL_MAILS:
                    message['recipients'].append(
                        get_gmail_recipient(obj.library_zk))

            elif recipient == 'library_dk':
                if USE_REAL_EMAILS:
                    library = lib_helper.library_by_sigla(obj.library_dk)
                    if library:
                        message['recipients'].extend(
                            library.notify_emails_by_service_role(
                                service=service, role='dk'))
                if ADD_AOW_MAILS:
                    message['recipients'].append('auda.aow+library_dk@gmail.com')
                if ADD_GMAIL_MAILS:
                    message['recipients'].append(
                        get_gmail_recipient(obj.library_dk))

            elif recipient == 'operator':
                if USE_REAL_EMAILS:
                    message['recipients'].append('ziskej@techlib.cz')
                if ADD_AOW_MAILS:
                    message['recipients'].append('auda.aow+operator@gmail.com')
                if ADD_GMAIL_MAILS:
                    message['recipients'].append(
                        get_gmail_recipient(recipient))

            else:
                logger.warning('Invalid recipient: ' + str(recipient))

        if not len(message['recipients']):
            logger.warning('No recipient, skipped')

        # subject
        message['subject'] = nid_cfg['subject'].format(**ticket_dict)

        # text
        if nid_cfg['recipients'] == ['reader', ]:
            text_plain = nid_cfg['text_plain'] + u'\n\n{sc_contact_reader}'
            if nid_cfg.get('en_text_plain', u''):
                text_plain += u'\n--------\n\n' + nid_cfg.get('en_text_plain', u'')
        else:
            text_plain = nid_cfg['text_plain'] + u'\n\n{sc_contact}'
        message['text_plain'] = text_plain.format(**ticket_dict)
        if not PLAIN_MAIL_FORMAT:
            text_html_tmpl = text_plain
            text_html_tmpl = text_html_tmpl.replace(u'{url}', u'{url_html}')
            message['text_html'] = text_html_tmpl.format(**ticket_dict)

        if DEBUG:
            print(" ")
            print("NOTIFICATIONS DEBUG")
            print(" ")
            print("message dict:")
            print("------------------------------------")
            pprint(message)
            print("------------------------------------")
            print(" ")

        # mail
        sender = self.sender
        recipient = None
        subject = message['subject']
        reply_to = self.reply_to
        attachments = None
        bcc = self.bcc

        if PLAIN_MAIL_FORMAT:
            mail_format = 'PLAIN'
            body_plain = message['text_plain']
            body_html = None
        else:
            mail_format = 'HTML'
            #body_plain = None
            body_plain = message['text_plain']
            body_html = message['text_html']

        if DEBUG:
            print("nid:", nid)
            print(" ")
            print("nid_cfg dict:")
            print("------------------------------------")
            pprint(nid_cfg)
            print("------------------------------------")
            print(" ")
            print("recipients:", message['recipients'])
            print(" ")

        if DEBUG_DISABLE_SENDMAIL:
            return

        for recipient in message['recipients']:
            self._send_mail(sender, recipient, subject, reply_to=reply_to,
                            body_plain=body_plain, body_html=body_html,
                            attachments=attachments, format=mail_format,
                            bcc=bcc, mail_format=mail_format)
