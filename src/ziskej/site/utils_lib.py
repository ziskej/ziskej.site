# -*- coding: utf-8 -*-
"""Pomocné nástroje a utility, nezávislé na Plone.
"""

import cgi
from datetime import datetime, date, timedelta
from datetime import time as dttime
import email
import io
import logging
import os
import os.path
import pprint as pprint_original
import re
import shutil
import subprocess
import time
import urllib


logger = logging.getLogger("ziskej")

# https://www.regular-expressions.info/email.html
#EMAIL_REGEXP = re.compile(u"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}", re.IGNORECASE)

EMAIL_REGEXP = re.compile(r"([0-9a-zA-Z_&.'+-]+!)*[0-9a-zA-Z_&.'+-]+"
    "@(([0-9a-zA-Z]([0-9a-zA-Z-]*[0-9a-z-A-Z])?\.)+"
    "[a-zA-Z]{2,6}|([0-9]{1,3}\.){3}[0-9]{1,3})$")


def safe_unicode(value):
    if isinstance(value, unicode):
        return value
    elif isinstance(value, basestring):
        try:
            value = unicode(value, 'utf-8')
        except (UnicodeDecodeError):
            value = value.decode('utf-8', 'replace')
    return value

def safe_utf8(value):
    if isinstance(value, str):
        try:
            value = unicode(value, 'utf-8')
        except (UnicodeDecodeError):
            value = value.decode('utf-8', 'replace')
    elif not isinstance(value, unicode):
        value = str(value)
    return value.encode('utf-8')

def format_date(when, is_time=False, is_html=False, omit_year=False):
    if is_html:
        if omit_year:
            fmt = '%-d.&nbsp;%-m.'
        else:
            fmt = '%-d.&nbsp;%-m.&nbsp;%Y'
        if is_time:
            fmt += '&nbsp;%-H:%M'
    else:
        if omit_year:
            fmt = '%-d.%-m.'
        else:
            fmt = '%-d.%-m.%Y'
        if is_time:
            fmt += ' %-H:%M'
    val = when.strftime(fmt)
    #print '-- ' + str((val, when, fmt, ))
    return val

def address_encode(value):
    if not value:
        return None
    value = safe_utf8(value)
    value_encoded = urllib.urlencode(dict(q=value))
    #logger.info('{value} --> {value_encoded}'.format(value=value, value_encoded=value_encoded))
    return value_encoded

def format_historylog_entry(prev=None, value=None):
    if not prev:
        prev = u''
    if not value:
        value = u''
    timestamp = datetime.now().isoformat()[:19].replace('T', ' ')
    return u'{prev}[{timestamp}] {value}\n'.format(
        prev=prev,
        value=value,
        timestamp=timestamp)

def nl2br(text, encode=True):
    #text = cgi.escape(text)
    return text.replace('\n', '<br />')

def sanitize_input_text(text):
    text = cgi.escape(text)
    return text

def sizeDisplay(size):
    """Returns a size with the correct unit (KB, MB), given the size in bytes.
    The output should be given to zope.i18n.translate().
    """

    if size == 0:
        return '0 KB'
    if size <= 1024:
        return '1 KB'
    if size > 1048576:
        return '{0} MB'.format('%0.02f' % (size / 1048576.0))
    return '{0} KB'.format('%d' % (size / 1024.0))

def default_today():
    return date.today()  # + date.timedelta(7)

def default_today_14d():
    return date.today() + date.timedelta(14)

# https://stackoverflow.com/questions/10883399/unable-to-encode-decode-pprint-output#10883893
class MyPrettyPrinter(pprint_original.PrettyPrinter):
    def format(self, object, context, maxlevels, level):
        if isinstance(object, unicode):
            return (object.encode('utf8'), True, False)
        return pprint_original.PrettyPrinter.format(self, object, context, maxlevels, level)

def pprint(value):
    MyPrettyPrinter().pprint(value)

def get_time0():
    return time.time()

def get_time_ms(time0):
    return u'%5.0f ms' % round((time.time()-time0) * 1000.0, 0)

def edd_add_file(edd_dir, filename, content):
    if not os.path.isdir(edd_dir):
        aow_os_utils_mkdir(edd_dir)
    filepath = os.path.join(edd_dir, filename)
    with open(filepath, 'wb') as f:
        f.write(content)
    return True

def edd_get_file(edd_dir, filename):
    if not os.path.isdir(edd_dir):
        return None
    filepath = os.path.join(edd_dir, filename)
    with open(filepath, 'rb') as f:
        content = f.read()
    return content

def edd_remove_file(edd_dir, filename):
    if not os.path.isdir(edd_dir):
        return False
    filepath = os.path.join(edd_dir, filename)
    aow_os_utils_rm(filepath)
    return True

def aow_os_utils_mkdir(filepath):
    return os.makedirs(filepath, 0750)

def aow_os_utils_rm(filepath):
    args = ['rm', filepath, ]
    return aow_os_utils_popen(args)

def aow_os_utils_popen(args, cwd=None, shell=False, env=None):
    print '-- Calling cwd=%s shell=%s env=%s: %s' % (cwd, shell, str(env), ' '.join(args))
    result = subprocess.Popen(
        args, 
        stdout=subprocess.PIPE, 
        stderr=subprocess.PIPE, 
        cwd=cwd, 
        shell=shell, 
        env=env).communicate()
    err = result[1]
    print '-- with result:\n%s' % result[0]
    print '-- with error:\n%s' % result[1]
    return result[0]
