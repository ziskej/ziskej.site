# -*- coding: utf-8 -*-
"""Pomocné nástroje a utility, závislé na Plone.
"""

from datetime import datetime, date, timedelta
from datetime import time as dttime
from types import DictType
from types import ListType
from types import TupleType
import cgi
import email
import re
import logging
import urllib

from AccessControl import ClassSecurityInfo, getSecurityManager
from AccessControl.SecurityManagement import newSecurityManager, setSecurityManager
from AccessControl.User import UnrestrictedUser as BaseUnrestrictedUser
from DateTime import DateTime
from zope.interface import Invalid
from plone.app.textfield.interfaces import ITransformer
from plone.app.textfield.value import RichTextValue
from plone.app.textfield.interfaces import IRichTextValue
from plone.app.textfield.interfaces import TransformError
from plone import api
from plone.namedfile.file import NamedBlobFile
import transaction

from ziskej.site import BUILDOUT_TYPE
from ziskej.site import _
from ziskej.site.utils_lib import *  # utility nezávislé na Plone


logger = logging.getLogger('Ziskej')


def log_exception(exception):
    try:
        logger.exception(unicode.encode(unicode(exception), 'ascii', 'replace'))
    except:
        logger.exception('Error in exception message')


class UnrestrictedUser(BaseUnrestrictedUser):
    """Unrestricted user that still has an id.
    """
    def getId(self):
        """Return the ID of the user.
        """
        return self.getUserName()


def delete_objects(portal, request, folder, ids):
    # https://docs.plone.org/develop/plone/content/deleting.html
    # https://docs.plone.org/develop/plone/security/permissions.html#bypassing-permission-checks
    # https://github.com/ned14/Easyshop/blob/master/src/easyshop.order/easyshop/order/adapters/order_management.py

    old_sm = getSecurityManager()
    tmp_user = UnrestrictedUser(old_sm.getUser().getId(), '', ['Manager'], '')
    tmp_user = tmp_user.__of__(portal.acl_users)
    try:
        newSecurityManager(None, tmp_user)
        folder.manage_delObjects(ids)
        #for obj_id in ids:
        #    folder._delObject(obj_id, suppress_events=True)
        #    # nesmaže z catalog
        transaction.commit()
    finally:
        setSecurityManager(old_sm)


def get_instance_type_label():
    instance_type = BUILDOUT_TYPE
    if BUILDOUT_TYPE == 'prod':
        instance_label = u''
        instance_title_postfix = u' PROD'
    elif BUILDOUT_TYPE == 'demo':
        instance_label = u'Školící verze'
        instance_title_postfix = u' DEMO'
    elif BUILDOUT_TYPE == 'test':
        instance_label = u'Testovací verze'
        instance_title_postfix = u' TEST'
    elif BUILDOUT_TYPE == 'dev':
        instance_label = u'Vývojová verze'
        instance_title_postfix = u' DEV'
    elif BUILDOUT_TYPE == 'development':
        instance_label = u'Lokální verze'
        instance_title_postfix = u' LOCAL'
    else:
        instance_type = u''
        instance_label = u'Neznámá verze'
        instance_title_postfix = u' UNKNOWN'
    return instance_type, instance_label, instance_title_postfix

def set_owner_role(obj, user_id, old_userid=None):
    """Set Owner role to specified user.

    Optionally, remove Owner role from previous user but add a Reader role
    """

    roles = obj.__ac_local_roles__
    if roles is None:
        obj.__ac_local_roles__ = roles = {}
    if user_id:
        roles[user_id] = ['Owner']
    if old_userid and (old_userid in roles):
        del roles[old_userid]
    if old_userid:
        roles[old_userid] = ['Reader']
    obj._p_changed = True

def send_full_mail_message(sender, recipient, subject, reply_to=None, 
        body_plain=None, body_html=None, attachments=None, format='PLAIN', 
        bcc=None):
    """attachments - list of dictionaries with filename and data keys."""
    mainMsg = email.MIMEMultipart.MIMEMultipart()
    mainMsg["Subject"] = subject
    if reply_to:
        mainMsg["Reply-To"] = reply_to

    mainMsg["Date"] = email.Utils.formatdate(localtime=1)
    mainMsg["Message-ID"] = email.Utils.make_msgid()
    if bcc is not None:
        mainMsg["Bcc"] = bcc

    charset = 'utf-8'

    if format == 'HTML':
        new_htmlTpl = body_html
        new_plaintextTpl = body_plain
        mainMsg["Content-type"] = "multipart/alternative"
        #  mainMsg.preamble="This is the preamble.\n"
        mainMsg.epilogue = "\n"  # To ensure that message ends with newline

        # plain
        secondSubMsg = email.Message.Message()
        secondSubMsg.add_header("Content-Type", "text/plain", charset=charset)
        secondSubMsg["Content-Disposition"] = "inline"
        secondSubMsg["Content-Transfer-Encoding"] = '8BIT'
        secondSubMsg.set_payload(safe_unicode(new_plaintextTpl).encode(charset), 
            charset)
        mainMsg.attach(secondSubMsg)
        # html
        subMsg = email.Message.Message()
        subMsg.add_header("Content-Type", "text/html", charset=charset)
        subMsg["Content-Disposition"] = "inline"
        subMsg["Content-Transfer-Encoding"] = '8BIT'
        subMsg.set_payload(safe_unicode(new_htmlTpl).encode(charset), charset)
        mainMsg.attach(subMsg)

    else:
        subMsg = email.Message.Message()
        subMsg.add_header("Content-Type", "text/plain", charset=charset)
        subMsg["Content-Disposition"] = "inline"
        subMsg["Content-Transfer-Encoding"] = '8BIT'
        subMsg.set_payload(safe_unicode(body_plain).encode(charset), charset)
        mainMsg.attach(subMsg)
        mainMsg.epilogue = "\n"  # To ensure that message ends with newline

    if attachments:
        for f in attachments:
            part = email.MIMEBase.MIMEBase('application', "octet-stream")
            if isinstance(f, NamedBlobFile):
                filename = f.filename
                data = f.data
            else:
                filename = f['filename']
                data = f['data']
            part.set_payload(str(data))
            email.Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 
                'attachment; filename="%s"' % filename)
            mainMsg.attach(part)

    api.portal.send_email(sender=sender, recipient=recipient, subject=subject, 
        body=mainMsg.as_string())

# FIXME deprecated
def datify(s, to_zone='+0200'):
    """Like dexterity Datify, but automatically convert everything to specified
    timezone instead of UTC.
    """

    if not isinstance(s, DateTime):
        if s == 'None':
            s = None
        elif isinstance(s, datetime):
            iso = s.isoformat()
            if s.tzinfo is None and to_zone:
                iso = iso + to_zone
            s = DateTime(iso)
        elif isinstance(s, date):
            s = DateTime(s.year, s.month, s.day)
        elif s is not None:
            s = DateTime(s)
    return s

def safe_plain_text(text):
    """Accepts text (string) or RichTextValue. Returns plain text without HTML.
    """

    if not text:
        return u''

    site = api.portal.getSite()
    transformer = ITransformer(site, None)
    if IRichTextValue.providedBy(text):
        rtext = text
    else:
        rtext = RichTextValue(text, 'text/html', 'text/plain')

    if transformer is not None:
        try:
            ttext = transformer(rtext, 'text/plain')
        except TransformError:
            ttext = rtext.output
    else:
        ttext = rtext.output

    return safe_unicode(ttext)

def make_searchable_text(obj, *fields):
    """Returns a string concatenated from selected fields."""
    result = u''
    if 'title' not in fields:
        value = getattr(obj, 'title', None)
        if value:
            result += safe_unicode(value) + u' '
    if 'description' not in fields:
        value = getattr(obj, 'description', None)
        if value:
            result += value + ' '

    for name in fields:
        value = getattr(obj, name, None)
        if callable(value):
            value = value()
        if isinstance(value, RichTextValue) or isinstance(value, basestring):
            value = safe_plain_text(value)
        elif isinstance(value, (ListType, TupleType)):
            value = u' '.join(value)
        elif isinstance(value, DictType):
            value = u' '.join([str(x) for x in value.values() if x])
        elif not value:
            value = u''
        else:
            value = safe_unicode(value)

        if value:
            result += value + ' '

    if isinstance(result, unicode):
        result = result.encode("utf-8")
    return result

def datetime_add_wd(value, start_date=None):
    """Vrátí datum a čas nyní (resp. start_date) plus value pracovních dnů
    (wd = working day) jako DateTime (Zope).
    """

    # SUGGESTION ZIS-54

    # validation
    if value < 0:
        raise Exception('Value must be 0 or more')

    # start_date
    if start_date is not None:
        dt = start_date  # DateTime
    else:
        dt = DateTime()

    # initial weekend
    while dt.dow() in (0, 6, ):
        dt = dt + 1

    # weeks
    if value >= 5:
        number_of_weeks = value / 5
        value = value % 5
        dt = dt + 7 * number_of_weeks

    if value:
        for i in range(value):
            # add wd
            dt = dt + 1

            # weekend
            while dt.dow() in (0, 6, ):
                dt = dt + 1

    return dt

def format_normalized_dt(value, is_time=False, is_html=False):
    """Pro normalizovaný DateTime vrátí správný formát."""
    if is_time:
        raise Exception('NIY')
    d = value.day()
    m = value.month()
    y = value.year()
    if is_html:
        formatted_value = u'{}.&nbsp;{}.&nbsp;{}'.format(d, m, y)
    else:
        formatted_value = u'{}. {}. {}'.format(d, m, y)
    return formatted_value

def datetime_normalize(value, time=None, preserve_time=False):
    """Normalizuje hodnotu v DateTime, datetime, date nebo string v ISO formátu
    na DateTime stejný den, ale time (default 12:00), bez timezone.
    """

    if preserve_time:
        # To znamená jen ignorovat časovou zónu, aby z normalizace byl vždy
        # výstup timezone naivní v podobě GMT+0, viz DateTime(string_bez_tz)
        if isinstance(value, DateTime):
            value = value.ISO()[:19]
        elif isinstance(value, datetime):
            value = value.isoformat()[:19]
        elif isinstance(value, date):
            if time is None:
                time = '12:00'
            value = '{}-{}-{} {}'.format(s.year, s.month, s.day, time)
        elif isinstance(value, basestring):
            value = value[:19]
        else:
            # Nemělo by nastat, pokud nastane, nechceme měnit hodnotu
            return value
        # value je nyní string v ISO formátu, jen část den
        value = value
        return DateTime(value)

    # FIXME je potřeba dodělat validaci

    if time is None:
        time = '12:00'
    if isinstance(value, DateTime):
        value = value.ISO()[:10]
    elif isinstance(value, datetime):
        value = value.isoformat()[:10]
    elif isinstance(value, date):
        value = '{}-{}-{}'.format(s.year, s.month, s.day)
    elif isinstance(value, basestring):
        value = value[:10]
    else:
        # Nemělo by nastat, pokud nastane, nechceme měnit hodnotu
        return value
    # value je nyní string v ISO formátu, jen část den
    value = value + ' ' + time
    return DateTime(value)

def count_wd(start_dt, end_dt):
    """Vrátí počet pracovních dní mezi dvěmi daty (obě DateTime), započítat
    krajní dny.
    """

    start_dt = datetime_normalize(start_dt)
    end_dt = datetime_normalize(end_dt)

    # Započítaní krajních dnů
    days = int(end_dt - start_dt + 1)
    weeks = int(days / 7)
    days_full_weeks = 7 * weeks
    days_remaining = days - days_full_weeks

    value = 0

    # Přidat pracovní dny v celých týdnech
    value += weeks * 5

    # Přidat dny zbývající do celého týdne, ale jen ty pracovní
    if days_remaining != 0:
        for i in range(0, days_remaining):
            x = start_dt + i
            if x.dow() not in (0, 6, ):
                value += 1

    return value

def datetime_add_d(value, start_date=None):
    """Vrátí datum a čas nyní (resp. start_date) plus value kalendářních dnů
    jako DateTime (Zope).
    """

    # start_date
    if start_date is not None:
        dt = start_date  # DateTime
    else:
        dt = DateTime()

    dt = dt + value

    return dt

def datetime_last_month_iso():
    """Vrátí poslední celý měsíc jako tuple, tedy např. pro kterýkoli den
    v prosinci vrátí tuple (u'2017-11-01', u'2017-11-30').

    Implementace využívá Zope DateTime, který poskytuje použitelnou aritmetiku
    a byl k dispozici před datetime (Python 2.4).

    https://pypi.python.org/pypi/DateTime
    """

    # DateTime aritmetika - získáme DateTime uvnitř posledního dne předchozího měsíce
    dt_now = DateTime()
    dt_now_d = dt_now.day()
    dt_last = dt_now - dt_now_d  # pro např. 5.3. vždy vrátí správně 28.2./29.2. atp.
    dt_last_str = dt_last.ISO()[:10]
    # první den předchozího měsíce stačí i takto...
    dt_first = DateTime(dt_last_str[:7] + '-01 12:00 GMT+1')  # CET x CEST můžeme ignorovat
    dt_first_str = dt_first.ISO()[:10]
    return (dt_first_str, dt_last_str, )
