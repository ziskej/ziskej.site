# -*- coding: utf-8 -*-
"""Funkcionalita pro zpracování plateb mezi knihovnami a mezi čtenářem
a knihovnou, pokud i tato platba jde přes Získej.  Nezahrnuje samotné volání
Získej async pro Platebátor API a NTK kredit API.
"""

from __future__ import print_function

from datetime import datetime
import logging
import uuid

from DateTime import DateTime
from plone import api
from plone.protect.interfaces import IDisableCSRFProtection
from zope.interface import alsoProvides

from ziskej.site import BUILDOUT_TYPE
from ziskej.site.payment_api import (
    ZiskejAsyncCredit,
    ZiskejAsyncPlatebator,
    ZiskejAsyncException,
    DEBUG_PLATEBATOR_DUMMY,
    DEBUG_PLATEBATOR_FORCE,
    PLATEBATOR_DISABLED,
    LOG_PLATEBATOR,
    )
from ziskej.site.ticket_helpers import create_reportentry
from ziskej.site.utils_lib import pprint


logger = logging.getLogger("ziskej.site.library_payment_helpers")

DEBUG = False


class PaymentException(Exception):
    pass


class LibraryPaymentBase(object):
    """Funkcionalita pro zpracování plateb."""

    def __init__(self, subticket, report_entry):
        super(LibraryPaymentBase, self).__init__()
        self.request = None
        self.portal = api.portal.get()
        self.ticket = subticket.ticket
        self.subticket = subticket
        self.report_entry = report_entry
        self.library_zk_id = subticket.library_zk
        self.library_zk = getattr(self.portal.libraries, self.library_zk_id, None)
        self.library_dk_id = subticket.library_dk
        self.library_dk = getattr(self.portal.libraries, self.library_dk_id, None)
        self.library_paysys = None
        self.library_credit = None

    def _next_payment_id(self):
        #return str(uuid.uuid4())  # .replace('-', '')[:16]
        return u'{random_part}-{hid}'.format(
            random_part = str(uuid.uuid4()).replace('-', '')[-12:],
            hid = self.subticket.hid,
            )


class LibraryPaymentEDD(LibraryPaymentBase):
    """Funkcionalita pro zpracování plateb pro EDD.  Wrapper pro nastavení
    dodatečných detailů, základní funkcionalita je implementována
    v PlatebatorWrapperEDD.
    """

    def __init__(self, subticket, report_entry):
        super(LibraryPaymentEDD, self).__init__(subticket, report_entry)

    def _debug_print(self):
        print(" ")
        print("ticket: ", self.ticket)
        print("subticket: ", self.subticket)
        print("report_entry: ", self.report_entry)
        print("hid: ", self.report_entry.hid)
        print("library_zk: ", self.library_zk)
        print("library_dk: ", self.library_dk)
        print("library_paysys: ", self.library_paysys)
        print("library_credit: ", self.library_credit)
        print("amount: ", self.amount)
        print("payment_id: ", self.payment_id)
        print("timestamp: ", self.timestamp)
        print(" ")

    def edd_create_payment(self):
        """Vytvořit platbu v platebátoru."""
        # payment_id = self._next_payment_id()
        if self.report_entry is None:
            self.amount = abs(self.ticket.cena)
        else:
            self.amount = abs(self.report_entry.cena)
        helper = PlatebatorWrapperEDD()
        is_ok = helper.platebator_create(self.ticket, self.amount)
        if not is_ok:
            # FIXME-EDD Platby
            raise PaymentException(helper.error_message)

    def edd_check_payment(self):
        """Zkontrolovat zaplacenost platby a pokud dochází ke změně stavu tak
        provést příslušné změny."""

        helper = PlatebatorWrapperEDD()
        helper.send_notifications = self.send_notifications
        helper.request = self.request

        now_dt = DateTime()

        # Pro testování zpalacení platebátorem (ne simulace), ale bez
        # Platebátoru, jen na TESTu (a LOCAL)
        force_paid = False
        if (DEBUG_PLATEBATOR_DUMMY or DEBUG_PLATEBATOR_FORCE) and \
                BUILDOUT_TYPE in ('test', 'dev', 'development', ):
            force_paid = bool(self.request.get('force_paid', None))

        # PlatebatorWrapperEDD
        # Pokud změna stavu zaplacení, pak nastaví
        #     alsoProvides(self.request, IDisableCSRFProtection)
        # a provede základní sadu změn objednávky
        try:
            is_changed = helper.platebator_check(
                self.ticket, now_dt, force_paid=force_paid)
        except ZiskejAsyncException, e:
            # FIXME-EDD Platby
            raise

        if is_changed:
            # Prozatím se jedná o now_dt, ale až bude Platebátor vracet
            # timestamp zaplacení, pak tady bude jeho hodnota
            report_date_dt = self.ticket.payment_paid_date
            # Vytvořit záznamy pro reporty
            self.edd_payment_reports(report_date_dt)

    def edd_payment_reports(self, report_date_dt):
        """Vytvořit záznamy pro reporty."""

        # Záznam pro report: Poplatek DK
        obj_dict = dict(
            report_date = report_date_dt,
            cena = self.subticket.cena,
            user_id = u'system',  # nemá pro EDD smysl
            entry_type = u'std',
            entry_subtype = u'edd-dk-std',
            is_paid_in_advance = False,  # nemá pro EDD smysl
            note = u'',
            )
        report_entry = create_reportentry(
            self.subticket, obj_dict)  # is_std = True

        # Záznam pro report: Poplatek Dílii
        obj_dict = dict(
            report_date = report_date_dt,
            cena = self.ticket.cena - self.subticket.cena,  # Dília poplatek
            user_id = u'system',  # nemá pro EDD smysl
            entry_type = u'std',
            entry_subtype = u'edd-dilia-std',
            is_paid_in_advance = False,  # nemá pro EDD smysl
            note = u'',
            edd_dilia_data = self.ticket.edd_dilia_data,  # Dília XML
            )
        report_entry = create_reportentry(
            self.subticket, obj_dict)  # is_std = True

        # Záznam pro report: Platby čtenářů
        # FIXME-EDD Platby
        logger.info('FIXME-EDD Platby: Vytvorit zaznam pro log plateb ctenaru')

        # Audit log pro vytvoření záznamů pro reporty
        self.ticket.add_historylog(u'Byly vytvořeny záznamy pro reporty.')

    def edd_payment_complaint(self):
        """Zapracovat přijatou reklamaci s vrácením peněz."""
        pass  # FIXME-EDD Platby


class LibraryPaymentMVS(LibraryPaymentBase):
    """Funkcionalita pro zpracování plateb mezi knihovnami."""

    def __init__(self, subticket, report_entry):
        super(LibraryPaymentMVS, self).__init__(subticket, report_entry)
        self.fee = int(self.report_entry.cena)

    def _debug_print(self):
        print(" ")
        print("ticket: ", self.ticket)
        print("subticket: ", self.subticket)
        print("report_entry: ", self.report_entry)
        print("hid: ", self.report_entry.hid)
        print("library_zk: ", self.library_zk)
        print("library_dk: ", self.library_dk)
        print("library_paysys: ", self.library_paysys)
        print("library_credit: ", self.library_credit)
        print("amount: ", self.amount)
        print("payment_id: ", self.payment_id)
        print("timestamp: ", self.timestamp)
        print(" ")

    def mvs_payment_clearing(self, payment_item, is_admin=False):
        """Pokusí se o clearing jedné nepodařené platby z kreditu knihovny
        v NTK.

        Vrací None, pokud by se mělo strhávat z kreditu NTK, u'' pokud se vše
        podařilo a paysys error message, pokud se nepodařilo.
        """

        self.payment_id = payment_item.get('payment_id', None)
        if not self.payment_id:
            self.payment_id = self._next_payment_id()
        self.timestamp = datetime.now().isoformat()[:19].replace('T', ' ')
        self.amount = abs(self.report_entry.cena)
        self.vs = u'{}'.format(self.subticket.hid)  # variabilní symbol v Paysys
        self.library_paysys = self.library_zk
        self.library_credit = self.library_dk

        # NTK jako knihovna v Získej
        if self.library_paysys.sigla == 'ABA013':
            #print("self.library_paysys was NTK")
            #self.library_paysys = None
            return None

        #self._debug_print()

        # validace
        if not isinstance(self.amount, int):
            logger.error('Invalid value amount for report_entry {id} HID {hid}'.format(
                id = self.report_entry.getId(),
                hid = self.report_entry.hid,
                ))
            raise PaymentException('Invalid value amount')

        # Výjimka pro clearing těchto požadavků
        if payment_item.get('hid', None) in (100262, 100259, ):
            if not is_admin:
                logger.info(
                    'mvs_payment_clearing skip hid {} for operator'.format(
                        payment_item.get('hid', None)))
                return None
            # Audit log
            self.ticket.add_historylog(
                u'Byl anulován závazek ke stržení MVS poplatku {} Kč z kreditu u NTK z důvodu vyřešení mimo Získej.'.format(
                    self.amount),
                who = ['zk', ])
            # Anulovat poplatek z ntk_todo ŽK, kde byl předtím. 
            # Tzn. dojde k zapsání -70 Kč a v ideálmím případě na konci
            # clearingu bude celková částka 0 Kč.
            self.library_paysys.mvs_payment_paysys_todo(
                self.subticket,
                self.report_entry,
                self.payment_id,
                self.timestamp,
                action_type = u'std_clearing',  # toto zajistí potřebné změny proti std
                message = u'Anulování dřívějšího záznamu pro tento požadavek, bylo vyřešeno mimo Získej.',
                )
            # Jen pro 259 anulovat i protistranu, pro 262 výslovně zadáno toto
            # nědělat.
            if payment_item.get('hid', None) in (100259, ):
                # Audit log
                self.subticket.add_historylog(
                    u'Bylo anulováno připsání MVS poplatku {} Kč z důvodu vyřešení mimo Získej.'.format(
                        self.amount),
                    who = ['dk', ])
                # Anulovat poplatek.
                self.library_credit.mvs_payment_credit_ziskej(
                    self.subticket, self.report_entry, self.payment_id,
                    self.timestamp, action_type=u'mvs_dk_fix_259',
                    is_negative=True)
                self.library_credit.reindexObject()
            return u''

        # Strhnout poplatek z NTK kreditu ŽK
        paysys_result = self._mvs_payment_paysys(self.library_paysys)

        if not paysys_result:
            return self.paysys_error_message

        # Pokud se podařila platba, zaeviduj nový závazek

        # Zaevidovat poplatek na ntk ŽK
        self.library_paysys.mvs_payment_paysys(
            self.subticket,
            self.report_entry,
            self.payment_id,
            self.timestamp,
            action_type=u'std_clearing',  # toto zajistí potřebné změny proti std
            )

        # Audit log
        # MVS ŽK std
        self.ticket.add_historylog(
            u'Byl zaplacen MVS poplatek {} Kč z kreditu u NTK.'.format(
                self.amount),
            who = ['zk', ])

        # Odepsat dodatečně zaplacený poplatek z ntk_todo ŽK, kde byl předtím. 
        # Tzn. dojde k zapsání -70 Kč a v ideálmím případě na konci clearingu
        # bude celková částka 0 Kč.
        self.library_paysys.mvs_payment_paysys_todo(
            self.subticket,
            self.report_entry,
            self.payment_id,
            self.timestamp,
            action_type = u'std_clearing',  # toto zajistí potřebné změny proti std
            message = u'Dodatečně zaplaceno z finančního konta.',
            )

        # Zapsat do report_entry
        # Už bylo zapsáno dříve, payment_id by mělo zůstat stejné a timestamp
        # necháme původní, proto žádná akce
        # self.report_entry.payment_entry_id = self.payment_id
        # self.report_entry.payment_timestamp = self.timestamp

        return u''

    def _mvs_payment_base(self, action_type=None):
        """Nastavit vše potřebné pro MVS platbu nebo její vrácení.

        Společná část pro std i rollback.
        """

        # parametry
        self.payment_id = self._next_payment_id()
        self.timestamp = datetime.now().isoformat()[:19].replace('T', ' ')

        self.amount = abs(self.report_entry.cena)
        self.vs = u'{}'.format(self.subticket.hid)  # variabilní symbol v Paysys

        if action_type ==u'std':
            # std
            self.library_paysys = self.library_zk
            self.library_credit = self.library_dk
        else:
            # std rollback
            self.library_paysys = self.library_dk
            self.library_credit = self.library_zk

        # NTK jako knihovna v Získej
        if self.library_paysys.sigla == 'ABA013':
            self.library_paysys = None
        if self.library_credit.sigla == 'ABA013':
            self.library_credit = None

        # Čtenářem předem zaplacená objednávka
        if self.ticket.is_paid_in_advance():
            if action_type == u'std' and self.library_paysys:
                # Audit log
                # MVS ŽK std, čtenářem předem
                self.ticket.add_historylog(
                    u'Nebyl zaplacen MVS poplatek, protože objednávka byla předem zaplacena čtenářem.',
                    who = ['zk', ])
                self.library_paysys = None
            elif action_type != u'std' and self.library_credit:
                # Audit log
                # MVS ŽK std, čtenářem předem
                self.ticket.add_historylog(
                    u'Nebyl vrácen MVS poplatek, protože objednávka byla předem zaplacena čtenářem.',
                    who = ['zk', ])
                self.library_credit = None

        if DEBUG:
            self._debug_print()

        # validace
        if not isinstance(self.amount, int):
            logger.error('Invalid value amount for report_entry {id} HID {hid}'.format(
                id = self.report_entry.getId(),
                hid = self.report_entry.hid,
                ))
            raise PaymentException('Invalid value amount')
        if self.report_entry.has_payment_entry():
            logger.error('Trying to do second payment for already paid report_entry {id} HID {hid}'.format(
                id = self.report_entry.getId(),
                hid = self.report_entry.hid,
                ))
            raise PaymentException('Internal error')

    def mvs_payment_rollback(self, action_type=None):
        """Malé storno nebo uznaná reklamace (přímo DK nebo SC) se nově liší od
        opačného mvs_payment.
        """

        # Nastavit vše potřebné pro MVS platbu nebo její vrácení
        self._mvs_payment_base(action_type=action_type)

        if self.library_paysys:

            # Zapsat záporně do Získej kredit
            # strhnout poplatek z credit_ziskej DK, může tedy jít i do záporu
            self.library_paysys.mvs_payment_credit_ziskej(
                self.subticket,
                self.report_entry,
                self.payment_id,
                self.timestamp,
                action_type=action_type,
                is_negative=True,
                )

            # Audit log
            # MVS ŽK malé storno po odeslání nebo uznaná reklamace
            self.ticket.add_historylog(
                u'Z kreditu v Získej byl čerpán vrácený MVS poplatek {} Kč.'.format(
                    self.amount),
                who = ['dk', ])

        if self.library_credit:

            # Připsat poplatek na credit_ziskej ŽK
            self.library_credit.mvs_payment_credit_ziskej(
                self.subticket,
                self.report_entry,
                self.payment_id,
                self.timestamp,
                action_type=action_type,
                )

            # Audit log
            # MVS ŽK malé storno po odeslání nebo uznaná reklamace
            self.ticket.add_historylog(
                u'Do kreditu v Získej byl připsán vrácený MVS poplatek {} Kč.'.format(
                    self.amount),
                who = ['zk', ])

        # Zapsat do report_entry
        if self.library_paysys:
            self.report_entry.payment_entry_id = self.payment_id
            self.report_entry.payment_timestamp = self.timestamp

    def mvs_payment(self):
        """Udělat vše potřebné pro MVS platbu (tzn. action_type=u'std').

        Pro všechny tyto typy je subticket a report_entry.  Všechny se platí
        z kreditu v NTK, případně zapíší do budoucna, že mělo být placeno
        a nebylo.  Všechny se připočítavají do kreditu v Získej.
        """

        action_type = u'std'

        # Nastavit vše potřebné pro MVS platbu nebo její vrácení
        self._mvs_payment_base(action_type=action_type)

        if self.library_paysys:

            # Strhnout poplatek z NTK kreditu ŽK
            paysys_result = self._mvs_payment_paysys(self.library_paysys)

            # Pokud se podařila platba, zaeviduj nový závazek
            if paysys_result:
                # Zaevidovat poplatek na ntk ŽK
                self.library_paysys.mvs_payment_paysys(
                    self.subticket,
                    self.report_entry,
                    self.payment_id,
                    self.timestamp,
                    action_type=action_type,
                    )
                # Audit log
                # MVS ŽK std
                self.ticket.add_historylog(
                    u'Byl zaplacen MVS poplatek {} Kč z kreditu u NTK.'.format(
                        self.amount),
                    who = ['zk', ])

            # Pokud nebyl dostatečný zůstatek nebo se nezdařila platba, zaeviduj
            # nový závazek
            else:
                # Připsat poplatek na ntk_todo ŽK
                self.library_paysys.mvs_payment_paysys_todo(
                    self.subticket,
                    self.report_entry,
                    self.payment_id,
                    self.timestamp,
                    action_type = action_type,
                    message = self.paysys_error_message,
                    )
                # Audit log
                # MVS ŽK std - nepodařilo se strhnout
                self.ticket.add_historylog(
                    u'Měl být zaplacen MVS poplatek {} Kč z kreditu u NTK, pouze zaevidováno pro pozdější platbu.'.format(
                        self.amount),
                    who = ['zk', ])

        if self.library_credit:

            # Připsat poplatek na credit_ziskej DK
            self.library_credit.mvs_payment_credit_ziskej(
                self.subticket,
                self.report_entry,
                self.payment_id,
                self.timestamp,
                action_type=action_type,
                )

            # Audit log
            # MVS DK std
            self.subticket.add_historylog(
                u'Do kreditu v Získej byl připsán MVS poplatek {} Kč.'.format(
                    self.amount),
                who = ['dk', ])

        # Zapsat do report_entry
        if self.library_paysys:
            self.report_entry.payment_entry_id = self.payment_id
            self.report_entry.payment_timestamp = self.timestamp

    def _mvs_payment_paysys(self, library):
        """Zaplatit prostřednictvím platebního systému, pokud je to možné,
        jinak vytvořit platbu pro budoucnost, což znamená, že se bude evidovat
        jako dluh, samozřejmě těmto situacím je potřeba se vyhnout, je-li to
        možné.
        """

        self.paysys_error_message = u''
        paysys = PaysysWrapper()

        ntk_user_id = library.Credit_ntk_user_id()
        # if DEBUG:
        #     logger.info('_mvs_payment_paysys replacing ntk_user_id {} with test'.format(ntk_user_id))
        #     ntk_user_id = u'test'

        # disponibilní zůstatek
        available_amount = paysys.credit_info(ntk_user_id)
        #available_amount = 1000

        if available_amount >= self.amount:
            paysys_result = paysys.pay(ntk_user_id, self.amount, vs=self.vs)
            if not paysys_result:
                substr = u'Paysys request failed with message '
                if paysys.error_message.startswith(substr):
                    paysys.error_message = u'Paysys chybová hláška: {}'.format(
                        paysys.error_message[len(substr):])
                substr = u'Paysys request failed with code '
                if paysys.error_message.startswith(substr):
                    paysys.error_message = u'Požadavek odmítnut, kód: {}'.format(
                        paysys.error_message[len(substr):])
                self.paysys_error_message = paysys.error_message
        else:
            self.paysys_error_message = u'Nedostatečný kredit'
            paysys_result = None

        if DEBUG:
            print("self.paysys_error_message:", self.paysys_error_message)
        return paysys_result


class PaysysWrapper(ZiskejAsyncCredit):
    """NTK kredit aka Paysys, Plone wrapper pro ZiskejAsync napojení na Získej
    async napojení na NTK Kredit API.
    """

    def credit_info(self, ntk_user_id):
        try:
            return self.credit_info_base(ntk_user_id)
        except ZiskejAsyncException, e:
            logger.warning('ZiskejAsyncCreditWrapper: {}'.format(e))
            return None

    def pay(self, ntk_user_id, amount, vs=None):
        try:
            self.pay_base(ntk_user_id, amount, vs=vs)
        except ZiskejAsyncException, e:
            e_str = str(e)
            if e_str == 'error_message':
                e_str = self.error_message
            logger.warning(u'ZiskejAsyncCreditWrapper: {}'.format(e_str))
            if e_str == 'PROD is disabled (403)':
                self.error_message = u'Nebylo možné vytvořit platbu (prod)'
            elif e_str == 'Invalid instance info (422)':
                self.error_message = u'Nebylo možné vytvořit platbu (instance info)'
            elif e_str:
                self.error_message = u'{}'.format(e_str)
            else:
                self.error_message = u'Nebylo možné vytvořit platbu'
            return False
        return True


class PlatebatorWrapperBase(ZiskejAsyncPlatebator):
    """Platebátor, Plone wrapper pro ZiskejAsync napojení na Získej async
    napojení na Platebátor API.  Společná část"""

    def _next_payment_id(self, obj):
        return u'{random_part}-{hid}'.format(
            random_part = str(uuid.uuid4()).replace('-', '')[-12:],
            hid = obj.hid,
            )

    def _validate(self, ticket):

        if ticket.payment_id:
            raise ZiskejAsyncException('Cannot create new payment in Platebator because of existing one.')

        # ŽK
        library_zk_id = ticket.library_zk
        if library_zk_id:
            portal = api.portal.get()
            library_zk = getattr(portal.libraries, library_zk_id, None)
        if not library_zk_id or not library_zk:
            logger.warning('platebator_create missing library for playment {}'.format(ticket.payment_id))
            raise ZiskejAsyncException('Missing ZK for ticket {}'.format(ticket.ticket_id))

        # Zabránit vytvoření platby v platebátoru pro ŽK bez MVS jako ŽK
        if not library_zk.Is_mvs_zk():
            logger.warning('platebator_create library does not provide MVS as ZK, cannot proceed with playment {}'.format(ticket.payment_id))
            raise ZiskejAsyncException('Library does not provide MVS for ticket {}'.format(ticket.ticket_id))

    def platebator_check(self, ticket, now_dt, force_paid=False):
        """Pokud nebylo zjištěno zaplacení dříve, zjistí pomocí Platebátor API,
        zda už není zaplaceno.

        Vrací True, pokud se mění stav zaplacení, jinak False.
        """

        if not ticket.payment_id:
            raise ZiskejAsyncException('Cannot check payment in Platebator because of missing payment_id.')
        if not ticket.payment_platebator_uuid:
            raise ZiskejAsyncException('Cannot check payment in Platebator because of missing payment_platebator_uuid.')
        # Už bylo zjištěno dříve
        if ticket.payment_paid_date:
            return False

        # FIXME možná časem prověřit a případně sjednotit s EDD
        if ticket.is_mvs and DEBUG_PLATEBATOR_DUMMY:
            if force_paid:
                ticket.payment_paid_date = now_dt
                if LOG_PLATEBATOR:
                    logger.info('Platebator dummy: check - force_paid')
                return True
            if LOG_PLATEBATOR:
                logger.info('Platebator dummy: check - nothing')
            return False
        # Pro EDD DEBUG_PLATEBATOR_DUMMY jen umožní použít force_paid
        if ticket.is_edd:
            if DEBUG_PLATEBATOR_DUMMY or DEBUG_PLATEBATOR_FORCE:
                if LOG_PLATEBATOR:
                    logger.info('Platebator dummy: check - force_paid {}'.format(force_paid))
            elif force_paid:
                force_paid = False

        if PLATEBATOR_DISABLED:
            return False

        payment_platebator_paid = None

        # Volat Platebátor a zjistit stav
        if not force_paid:
            try:
                payment_platebator_paid = self.platebator_check_base(ticket.payment_platebator_uuid)
                if LOG_PLATEBATOR:
                    logger.info('Platebator real: check - paid {}'.format(payment_platebator_paid))
            except ZiskejAsyncException, e:
                if str(e) == 'error_message':
                    try:
                        error_message = self.error_message
                    except:
                        error_message = 'Error in Platebator'
                logger.error(error_message)
                # Platebátor DEBUG – pokud chyba v ověření stavu platby, pak
                # jako nezaplacené.
                if DEBUG_PLATEBATOR_DUMMY:
                    payment_platebator_paid = False
                    if LOG_PLATEBATOR:
                        logger.info('Platebator real: check - failure – therefore unpaid')
                # Platebátor TEST – pokud chyba v ověření stavu platby, pak
                # simulovat zaplacení.
                elif BUILDOUT_TYPE in ('test', 'dev', 'development', ):
                    payment_platebator_paid = True
                    if LOG_PLATEBATOR:
                        logger.info('Platebator real: check - failure – therefore paid')
                # Jinak naopak brát jako nezaplacené.
                else:
                    payment_platebator_paid = False
                    if LOG_PLATEBATOR:
                        logger.info('Platebator real: check - failure – therefore unpaid')

        # Platebátor vrátil změnu stavu na zaplaceno nebo testujeme a je
        # vynuceno zaplacení
        if force_paid or payment_platebator_paid:

            # ŽK
            library_zk_id = ticket.library_zk
            if library_zk_id:
                portal = api.portal.get()
                library_zk = getattr(portal.libraries, library_zk_id, None)
            if not library_zk_id or not library_zk:
                logger.warning('platebator_check missing library for playment {}'.format(ticket.payment_id))
                raise ZiskejAsyncException('Missing ZK for ticket {}'.format(ticket.ticket_id))

            # Nezabraňovat zpracování platby z platebátoru pro ŽK bez MVS (EDD)
            # jako ŽK, jen zalogovat.
            if ticket.is_mvs and not library_zk.Is_mvs_zk():
                logger.warning('platebator_check library does not provide MVS as ZK for playment {}'.format(ticket.payment_id))
            elif ticket.is_edd and not library_zk.Is_edd_zk():
                logger.warning('platebator_check library does not provide EDD as ZK for playment {}'.format(ticket.payment_id))

            # Víme, že je zaplaceno a že před tímto voláním Platebátor API
            # Získej o zaplacení ještě nevědělo, proto je potřeba nastavit vše
            # potřebné pro událost zaplacení.  Parametr date_dt (now_dt) je
            # aktuální datum, ale teoreticky by mohlo jít o datum z Platebátor
            # API, kdyby API toto datum poskytovalo.
            if force_paid:
                if LOG_PLATEBATOR:
                    logger.info('Platebator dummy: after paid trigger')
            else:
                if LOG_PLATEBATOR:
                    logger.info('Platebator real: after paid trigger')
            alsoProvides(self.request, IDisableCSRFProtection)
            if ticket.is_mvs:
                notification_ids = ticket.reader_pay_by_platebator_mvs(now_dt)
            elif ticket.is_edd:
                # Nastaví vše potřebné, vrátí jaké notifikace je potřeba
                # poslat, nevytváří záznamy pro reporty
                notification_ids = ticket.reader_pay_by_platebator_edd(now_dt)
            else:
                notification_ids = []
            for notification_id in notification_ids:
                # send_notifications je wrapper nad send_notifications
                # z notifications a má volitelné parametry ticket a subticket,
                # které lze většinou ignorovat - kromě tohoto místa, zde je
                # ticket nutný předat, protože není v rámci cron volání
                # převzatelný z kontextu.
                self.send_notifications(notification_id, ticket=ticket)

            return True

        return False


class PlatebatorWrapperEDD(PlatebatorWrapperBase):
    """Platebátor, Plone wrapper pro ZiskejAsync napojení na Získej async
    napojení na Platebátor API.  Pro EDD objednávky, tedy samoosblužné přes
    CPK UI a pomocí Získej API pro CPK pro čtenáře i asistované a vytváří se
    při odeslání požadavku z DK, protože až tehdy známe počet stran a skutečnou
    cenu.
    """


    def _platebator_create(self, ticket, amount):
        # validovat, společná validace MVS i EDD
        self._validate(ticket)

        # jednoznačný identifikátor platby v Získej
        payment_id = self._next_payment_id(ticket)

        # nastavit data v ticketu (amount, payment_id, reset jiných)
        ticket.reader_payment_create_edd(payment_id, amount)

        # data pro Platebátor API
        firstname = ticket.Reader_firstname()
        if not firstname:
            firstname = u''
        lastname = ticket.Reader_lastname()
        if not lastname:
            lastname = ticket.reader_email
        if not lastname:
            lastname = u'Nezmámé'
        data = dict(
            amount = ticket.payment_amount,
            fields = dict(
                name = firstname,
                surname = lastname,
                ),
            eppn = None,
            ticket_id = ticket.ticket_id,
            payment_id = ticket.payment_id,
            )

        # testování bez Platebátor API
        if DEBUG_PLATEBATOR_DUMMY:
            if LOG_PLATEBATOR:
                logger.info('Platebator dummy: create')
            ticket.payment_platebator_uuid = u'{}'.format(str(uuid.uuid4()))
            return

        # instance bez platebátoru
        if PLATEBATOR_DISABLED:
            return

        # vytvořit pomocí Platebátor API novou platbu, vrátí její Platebátor
        # uuid aka platebator_id v Získej
        # FIXME otestovat, zda je správně
        if LOG_PLATEBATOR:
            logger.info('Platebator real: create')
        ticket.payment_platebator_uuid = self.platebator_create_base(data)

    def platebator_create(self, ticket, amount):
        try:
            # až z _platebator_create se volá platebator_create_base
            self._platebator_create(ticket, amount)
            return True
        except ZiskejAsyncException, e:
            e_str = str(e)
            if e_str == 'error_message':
                e_str = self.error_message
            logger.warning(u'PlatebatorWrapperEDD: {}'.format(e_str))
            if e_str:
                self.error_message = u'{}'.format(e_str)
            else:
                self.error_message = u'Cannot create new payment in Platebator.'
            return False


class PlatebatorWrapperMVS(PlatebatorWrapperBase):
    """Platebátor, Plone wrapper pro ZiskejAsync napojení na Získej async
    napojení na Platebátor API.  Pro MVS objednávky, tedy jen samoosblužné přes
    CPK UI a pomocí Získej API pro CPK pro čtenáře a součást vytváření
    objednávky, přesněji ta je už vytvořena, ale vše v rámci jednoho volání API.
    """

    def _platebator_create(self, ticket, eppn):
        # validovat, společná validace MVS i EDD
        self._validate(ticket)

        # jednoznačný identifikátor platby v Získej
        payment_id = self._next_payment_id(ticket)

        # nastavit data v ticketu (amount, payment_id, reset jiných)
        ticket.reader_payment_create_mvs(payment_id)

        # data pro Platebátor API
        data = dict(
            amount = ticket.payment_amount,
            fields = dict(
                name = ticket.Reader_firstname(),
                surname = ticket.Reader_lastname(),
                ),
            eppn = eppn,
            ticket_id = ticket.ticket_id,
            payment_id = ticket.payment_id,
            )

        # testování bez Platebátor API
        if DEBUG_PLATEBATOR_DUMMY:
            ticket.payment_platebator_uuid = u'{}'.format(str(uuid.uuid4()))
            return

        # instance bez platebátoru
        if PLATEBATOR_DISABLED:
            return

        # vytvořit pomocí Platebátor API novou platbu, vrátí její Platebátor
        # uuid aka platebator_id v Získej
        ticket.payment_platebator_uuid = self.platebator_create_base(data)

    def platebator_create(self, ticket, eppn):
        try:
            # až z _platebator_create se volá platebator_create_base
            self._platebator_create(ticket, eppn)
            return True
        except ZiskejAsyncException, e:
            e_str = str(e)
            if e_str == 'error_message':
                e_str = self.error_message
            logger.warning(u'PlatebatorWrapperMVS: {}'.format(e_str))
            if e_str:
                self.error_message = u'{}'.format(e_str)
            else:
                self.error_message = u'Cannot create new payment in Platebator.'
            return False


class PaysysClearing(object):

    def __init__(self, catalog):
        self.catalog = catalog

    def _subticket(self, subticket_id):
        brains = self.catalog.unrestrictedSearchResults(dict(
            portal_type = 'SubticketMVS',
            getId = subticket_id,
            ))
        if not len(brains):
            return None
        if len(brains) > 1:
            raise Exception('Too many Subticket objects')
        return brains[0]._unrestrictedGetObject()

    def _report_entry(self, report_entry_id):
        brains = self.catalog.unrestrictedSearchResults(dict(
            portal_type = 'ReportEntry',
            getId = report_entry_id,
            ))
        if not len(brains):
            return None
        if len(brains) > 1:
            raise Exception('Too many ReportEntry objects')
        return brains[0]._unrestrictedGetObject()

    def _libraries(self):
        brains = self.catalog.unrestrictedSearchResults(dict(
            portal_type = 'Library',
            review_state = 'published',
            sort_on = 'sigla',
            sort_order = 'reverse',
            ))
        return [brain._unrestrictedGetObject() for brain in brains]

    def paysys_clearing_admin(self, is_admin=False, libraries=None):
        result = []
        clearing_counter = 0
        if not libraries:
            libraries = self._libraries()
        for library in libraries:
            # if library.sigla not in ('KVG501', ):
            #     continue
            res = u'<p class="clearing-header">Knihovna: <strong>{}</strong> {}</p>\n'.format(library.sigla, library.fullname)

            data = library.credit_log_data(None)

            cleared_payment_ids = []
            for item in data:
                if item.get('item_type', None) != 'mvs_zk_ntk_todo_std_clearing':
                    continue
                payment_id = item.get('payment_id', None)
                if not payment_id:
                    continue
                cleared_payment_ids.append(payment_id)

            for item in data:
                # item .. payment_item

                if item.get('item_type', None) != 'mvs_zk_ntk_todo_std':
                    continue

                payment_id = item.get('payment_id', None)
                if payment_id in cleared_payment_ids:
                    continue

                # print(" ")
                # print("platby item:")
                # pprint(item)

                report_entry_id = item.get('report_entry_id', None)
                if not report_entry_id:
                    pprint(item)
                    logger.warning('Missing report_entry_id')
                    continue
                report_entry = self._report_entry(report_entry_id)
                if not report_entry:
                    pprint(item)
                    logger.warning('Missing report_entry for {}'.format(
                        report_entry_id))
                    res += u'<p class="clearing-item">Platbu pro report_entry {} nelze strhnout, protože příslušný požadavek (a všechno příslušenství vč. podkladů pro reporty) byl smazán v rámci obnovy testovací instance.</p>\n'.format(report_entry_id)
                    continue
                subticket = self._subticket(report_entry.subticket_id)
                if not subticket:
                    logger.warning('Missing subticket')
                    continue

                helper = LibraryPaymentMVS(subticket, report_entry)
                paysys_error_message = helper.mvs_payment_clearing(
                    item, is_admin=is_admin)

                spec_message = u''
                if item.get('hid', None) in (100262, 100259, ) and not is_admin:
                    spec_message = u'pro tento požadavek se čeká na rozhodnutí, jak vypořádat.'
                elif paysys_error_message is None:
                    continue

                res += u'<p class="clearing-item">ŽK <a href="{}/admin_log">{}</a>'.format(
                    library.absolute_url(),
                    library.sigla,
                    )
                library_dk_sigla = subticket.library_dk
                if library_dk_sigla:
                    res += u' / DK <a href="{}/{}/admin_log">{}</a>'.format(
                        library.absolute_url()[:-6],
                        library_dk_sigla.lower(),
                        library_dk_sigla.upper(),
                        )
                res += u' pro HID {}, '.format(
                    subticket.hid,
                    )
                # res += u'<a href="{}">report entry</a>, '.format(report_entry.absolute_url())
                res += u'report entry id {}, '.format(report_entry_id)
                res += u'<a href="{}">požadavek</a>, '.format(subticket.absolute_url())
                res += u'<a href="{}">objednávka</a>, '.format(subticket.ticket.absolute_url())
                if spec_message:
                    res += u'{}<br>'.format(spec_message)
                elif paysys_error_message:
                    res += u'paysys_error_message: {}</p>\n'.format(paysys_error_message)
                else:
                    res += u'úspěšně strženo</p>\n'
                clearing_counter += 1

                # if clearing_counter > 0:
                #     break
            result.append(res)
            # if clearing_counter > 0:
            #     break

        return result

# Frontpage Paysys clearing (admin (pak vč. výjimek 259 a 262) nebo operator)
def paysys_clearing_admin(is_admin=False, libraries=None):
    catalog = api.portal.get_tool(name='portal_catalog')
    paysys_clearing = PaysysClearing(catalog)
    return paysys_clearing.paysys_clearing_admin(
        is_admin=is_admin, libraries=libraries)

# Interface vůči Získej app, MVS
def library_payment_mvs(subticket, report_entry):
    helper = LibraryPaymentMVS(subticket, report_entry)
    helper.mvs_payment()

def library_payment_mvs_complaint(subticket, report_entry, after_objection):
    helper = LibraryPaymentMVS(subticket, report_entry)
    if not after_objection:
        helper.mvs_payment_rollback(action_type=u'complaint')
    else:
        helper.mvs_payment_rollback(action_type=u'complaint_after_objection')

def library_payment_mvs_cancel(subticket, report_entry):
    helper = LibraryPaymentMVS(subticket, report_entry)
    helper.mvs_payment_rollback(action_type=u'cancel')

# Interface vůči Získej app, EDD
def library_payment_edd_create(subticket):
    helper = LibraryPaymentEDD(subticket, None)
    helper.edd_create_payment()

def library_payment_edd_check(ticket, send_notifications=None, request=None):
    subticket_id = ticket.subticket_id
    if subticket_id:
        subticket = getattr(ticket, subticket_id, None)
    else:
        subticket = None
    if not subticket:
        raise PaymentException('Internal error: Missing subticket')
    helper = LibraryPaymentEDD(subticket, None)
    helper.send_notifications = send_notifications
    helper.request = request
    return helper.edd_check_payment()

# def library_payment_edd_pay(subticket, report_entry):
#     helper = LibraryPaymentEDD(subticket, report_entry)
#     helper.edd_payment()

# def library_payment_edd_complaint(subticket, report_entry):
#     helper = LibraryPaymentEDD(subticket, report_entry)
#     helper.edd_payment_complaint()

# Library profil
def credit_info(ntk_user_id):
    helper = PaysysWrapper()
    return helper.credit_info(ntk_user_id)

# Získej API, vytvoření objednávky
def platebator_create(ticket, eppn):
    helper = PlatebatorWrapperMVS()
    is_ok = helper.platebator_create(ticket, eppn)
    if not is_ok:
        return helper.error_message
    return None

# Získej API, detail objednávky
def platebator_check(ticket, now_dt, force_paid=False, send_notifications=None, request=None):
    helper = PlatebatorWrapperMVS()
    helper.send_notifications = send_notifications
    helper.request = request
    return helper.platebator_check(ticket, now_dt, force_paid=force_paid)
