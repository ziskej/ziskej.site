# -*- coding: utf-8 -*-

"""ziskej.site je hlavním balíčkem Ziskej app a obsahuje funkcionalitu (logiku)
aplikace.  Silně závisí na Zope a/nebo Plone.

Struktura balíčku je standardní:

content
-------

Dexterity CT (Content Types, typy obsahu), definice.  Pro detailní dokumentaci
viz příslušné soubory, jako např. content/library.py pro Library CT resp.
content/libraryct.py for LibraryCT CT (první CT je pro Container, v tomto
případě kontejner pro libraries, druhé CT je pro Content Type).

browser
-------

Definice pohledů, browser views.  Browser views jsou seskupeny ve složkách jako
třeba admin, content_view atp.

browser/viewlet
---------------

Speciální složkou uvnitř browser je složka viewlet obsahující viewlety, třídy
(py) a šablony (pt).  Viewlet je část stránky, jako třeba footer (patička
stránky) nebo header (hlavička stránky), obvykle obdobný na všech stránkách. 
Může být statický, jak je obvyklé pro footer nebo závislý na kontextu, jak je
obvyklé pro navigaci, záložky v hlavičce, breadcrumbs, volba jazyka atp.

profiles
--------

Úvodní konfigurace je obsažena v profilu pro portal_setup v profiles/default
jak je obvyklé.  Viz portal_setup tool ve ZMI.

helpers
-------

Balíček bsahuje značné množství pomocných nástrojů (helpers) v kořenové složce.
Některé jsou závislé na Zope / CMF / Plone a některé jsou nezávislé.  Nezávislé by mohly být separovány v samostatném balíčku, nezávislém na Zope / Plone.

Viz dokumentaci v každém z nich.

Příklady: ticket_helpers, ticket_load_balancing, cpk_api, marc21, notifications.

auth
----

Obsahuje autentizaci a autorizaci a příslušné nástroje.

behavior
--------

Definuje bahaviors pro Dexterity CT definice.

data
----

Obsahuje příklady dat z CPK API atp.

locales
-------

Placeholder pro lokalizaci balíčku.

migration
---------

Někdy je změněna definice obsahu nbeo worlflow nebo portal_catalog a je potřebné změnit existující obsah nebo provést jednorázové kroky (přidat index, rozšířit metadata každého brain výsledku hledání v portal_catalog atp.) - tyto situace pokrývají migrace.  Používají portal_setup.

tests
-----

Placeholder pro testy.

"""

import time
import os.path
import logging

from App.config import getConfiguration
from zope.i18nmessageid import MessageFactory


logger = logging.getLogger('Ziskej')

_ = MessageFactory('ziskej.site')


# Options

# Pokud operátor zobrazí ticket či subticket, má se resetovat nepřečten
# operátorem?  Pro čtenáře, ŽK i DK to platí, ale pro operátora možná nemusí.
# Důvodem může být třeba použití nepřečten operátorem jako filtr pro
# nerozhodnuté, které vyžadují reakci atp., a tam samotné přečtení nestačí, je
# potřeba opravdu vyřešit.
VISIT_RESETS_UNREAD_OP = True

# Lokální verze může pro ladění NTK Payment a NTK Platebátor využívat Získej
# async LOCAL instanci (True)
LOCAL_USING_ASYNC_LOCAL = False


# Specificky pro každou instanci, zde default

# Používat platebátor, zejména v Získej API pro CPK pro čtenáře
USE_PLATEBATOR = False

# Adresa platebátoru
PLATEBATOR_BASE_URL = None

# Nutnost platit MVS jako ZK z NTK kreditu
ENFORCE_PAYSYS = False

# Zobrazovat zprávy o nedostatku kreditu
PAYSYS_MESSAGE = True

# Simulovat zaplacení v platebátoru
SIMULATE_PLATEBATOR_PAID = False

# Používat MRS
IS_MRS = False

# Používat EDD
IS_EDD = False
DEBUG_EDD = False
IS_EDD_AT_TEST = False


# Buildout
BUILDOUT_DIR = None
BUILDOUT_TYPE = None
IS_LIVE_INSTANCE = False
ZISKEJ_VAR = None
ZISKEJ_VAR_DOWNLOAD = None
ZISKEJ_DATA = None
ZISKEJ_BASE_URL = ''
ZISKEJ_ASYNC_BASE_URL = ''
ZISKEJ_ASYNC_BASE_URL_LOCAL = ''
ZISKEJ_ASYNC_NTK_INSTANCE = None
ZISKEJ_ASYNC_APP_ID = None
ZISKEJ_SITE_DIR = None

zope_conf = getConfiguration()
if hasattr(zope_conf, 'product_config'):
    product_config = zope_conf.product_config.get('ziskej.site', {})
    if product_config is not None:
        BUILDOUT_DIR = product_config.get('buildout_dir', BUILDOUT_DIR)
        BUILDOUT_TYPE = product_config.get('buildout_type', BUILDOUT_TYPE)

        IS_LIVE_INSTANCE = bool(BUILDOUT_TYPE == 'live')

        ZISKEJ_VAR = os.path.join(BUILDOUT_DIR, 'var')
        ZISKEJ_VAR_DOWNLOAD = os.path.join(ZISKEJ_VAR, 'files_download')
        ZISKEJ_VAR_EDD = os.path.join(ZISKEJ_VAR, 'files_edd')

        ZISKEJ_SITE_DIR = os.path.join(BUILDOUT_DIR, 'src/ziskej.site/src/ziskej/site')
        ZISKEJ_DATA_DIR = os.path.join(ZISKEJ_SITE_DIR, 'data')

# Získej settings
USE_ASYNC_PORT_9080 = False
CPK_DEV_BASE_URL = 'https://www.knihovny.cz/'
CPK_DEV_RECORDE_URL = 'Record/'
SHARED_SECRET_ZISKEJ_APP = 'DELETED'  # CREDENTIALS-IN-CODE
SHARED_SECRET_ZISKEJ_APP_PYRAMID = 'DELETED'  # CREDENTIALS-IN-CODE
SECRETS_DIR = '/data/apiaks-secrets'

# Nastavení specifické pro instance (zejména async instance na základě zope
# instance)

# PROD zope instance
if BUILDOUT_TYPE == 'prod':
    ZISKEJ_ASYNC_MODE = 'PROD'
    ZISKEJ_ASYNC_NTK_INSTANCE = 'prod'
    ZISKEJ_ASYNC_APP_ID = 'ziskej-async.{mode}'.format(mode='PROD')
    USE_PLATEBATOR = True
    PLATEBATOR_BASE_URL = u'http://platebator.ntkcz.cz/app/'
    USE_ASYNC_PORT_9080 = False
    ZISKEJ_ASYNC_BASE_URL = 'https://ziskej.techlib.cz:9080/'
    ENFORCE_PAYSYS = True
    ZISKEJ_BASE_URL = 'https://ziskej.techlib.cz/'
    ZISKEJ_PYRAMID_MODE = None
    APIAKS_ENABLED = False

# DEMO zope instance
elif BUILDOUT_TYPE == 'demo':
    ZISKEJ_ASYNC_MODE = 'DEMO'
    ZISKEJ_ASYNC_APP_ID = 'ziskej-async.{mode}'.format(mode='DEMO')
    USE_PLATEBATOR = False
    USE_ASYNC_PORT_9080 = False
    ZISKEJ_ASYNC_BASE_URL = 'https://ziskej-demo.techlib.cz:9080/'
    PAYSYS_MESSAGE = False
    ZISKEJ_BASE_URL = 'https://ziskej-demo.techlib.cz/'
    ZISKEJ_PYRAMID_MODE = None
    APIAKS_ENABLED = False

# TEST zope instance
elif BUILDOUT_TYPE == 'test':
    ZISKEJ_ASYNC_MODE = 'TEST'
    ZISKEJ_ASYNC_NTK_INSTANCE = 'test'
    ZISKEJ_ASYNC_APP_ID = 'ziskej-async.{mode}'.format(mode='TEST')
    USE_PLATEBATOR = True
    PLATEBATOR_BASE_URL = u'http://platebator.test.ntkcz.cz/app/'
    USE_ASYNC_PORT_9080 = False
    ZISKEJ_ASYNC_BASE_URL = 'https://ziskej-test.techlib.cz:9080/'
    #ZISKEJ_ASYNC_BASE_URL = 'http://127.0.0.1:9080/'
    IS_MRS = True
    IS_EDD = True
    IS_EDD_AT_TEST = True
    ZISKEJ_BASE_URL = 'https://ziskej-test.techlib.cz/'
    ZISKEJ_PYRAMID_MODE = 'TEST'
    APIAKS_ENABLED = True

# DEV zope instance
elif BUILDOUT_TYPE == 'dev':
    ZISKEJ_ASYNC_MODE = 'DEV'
    ZISKEJ_ASYNC_NTK_INSTANCE = 'test'
    ZISKEJ_ASYNC_APP_ID = 'ziskej-async.{mode}'.format(mode='TEST')
    USE_ASYNC_PORT_9080 = False
    #ZISKEJ_ASYNC_BASE_URL = 'https://ziskej-dev.techlib.cz:9080/'
    ZISKEJ_ASYNC_BASE_URL = 'https://ziskej-test.techlib.cz:9080/'
    #ZISKEJ_ASYNC_BASE_URL = 'http://127.0.0.1:9080/'
    IS_MRS = True
    IS_EDD = True
    IS_EDD_AT_TEST = True
    ZISKEJ_BASE_URL = 'https://ziskej-dev.techlib.cz/'
    ZISKEJ_PYRAMID_MODE = None
    APIAKS_ENABLED = False

# LOCAL zope instance
elif BUILDOUT_TYPE == 'development':
    ZISKEJ_ASYNC_MODE = 'LOCAL'
    ZISKEJ_ASYNC_NTK_INSTANCE = 'test'
    ZISKEJ_ASYNC_APP_ID = 'ziskej-async.{mode}'.format(mode='TEST')
    USE_PLATEBATOR = True
    PLATEBATOR_BASE_URL = u'http://platebator.test.ntkcz.cz/app/'
    USE_ASYNC_PORT_9080 = False
    ZISKEJ_ASYNC_BASE_URL = 'https://ziskej-test.techlib.cz:9080/'
    if LOCAL_USING_ASYNC_LOCAL:
        ZISKEJ_ASYNC_BASE_URL_LOCAL = 'http://localhost:8000/'
        ZISKEJ_ASYNC_APP_ID = 'ziskej-async.{mode}'.format(mode='LOCAL')
    IS_MRS = True
    IS_EDD = True
    IS_EDD_AT_TEST = True
    DEBUG_EDD = True
    # ZISKEJ_BASE_URL = 'http://localhost:14180/'
    ZISKEJ_BASE_URL = 'http://ziskej.private/'
    ZISKEJ_PYRAMID_MODE = 'LOCAL'
    APIAKS_ENABLED = True

ZISKEJ_API_ID = 'ziskej-async.{mode}'.format(mode=ZISKEJ_ASYNC_MODE)
ZISKEJ_API_AKS_ID = 'ziskej-apiaks.{mode}'.format(mode=ZISKEJ_PYRAMID_MODE)

SHOW_OP_DK_ORDER_OVERVIEW_DETAILS = bool(
    BUILDOUT_TYPE in ('development', 'test', ))

print 'BUILDOUT_TYPE:', BUILDOUT_TYPE
print 'BUILDOUT_DIR:', BUILDOUT_DIR
print 'ZISKEJ_VAR:', ZISKEJ_VAR
print 'ZISKEJ_VAR_DOWNLOAD:', ZISKEJ_VAR_DOWNLOAD
print 'ZISKEJ_VAR_EDD:', ZISKEJ_VAR_EDD
print 'ZISKEJ_SITE_DIR:', ZISKEJ_SITE_DIR
print "ZISKEJ_BASE_URL:", ZISKEJ_BASE_URL
print 'ZISKEJ_ASYNC_MODE:', ZISKEJ_ASYNC_MODE
print 'ZISKEJ_ASYNC_NTK_INSTANCE:', ZISKEJ_ASYNC_NTK_INSTANCE
print 'ZISKEJ_ASYNC_APP_ID:', ZISKEJ_ASYNC_APP_ID
print 'ZISKEJ_API_ID:', ZISKEJ_API_ID
print "USE_ASYNC_PORT_9080:", USE_ASYNC_PORT_9080
print "ZISKEJ_ASYNC_BASE_URL:", ZISKEJ_ASYNC_BASE_URL
print "ZISKEJ_ASYNC_BASE_URL_LOCAL:", ZISKEJ_ASYNC_BASE_URL_LOCAL
print "IS_MRS:", IS_MRS
print "IS_EDD:", IS_EDD
print "IS_EDD_AT_TEST:", IS_EDD_AT_TEST
print "DEBUG_EDD:", DEBUG_EDD
print "USE_PLATEBATOR:", USE_PLATEBATOR
print "PLATEBATOR_BASE_URL:", PLATEBATOR_BASE_URL
print "ENFORCE_PAYSYS:", ENFORCE_PAYSYS
print "SHOW_OP_DK_ORDER_OVERVIEW_DETAILS:", SHOW_OP_DK_ORDER_OVERVIEW_DETAILS
print "ZISKEJ_PYRAMID_MODE:", ZISKEJ_PYRAMID_MODE
print "ZISKEJ_API_AKS_ID:", ZISKEJ_API_AKS_ID
print "SHARED_SECRET_ZISKEJ_APP_PYRAMID:", SHARED_SECRET_ZISKEJ_APP_PYRAMID
print "SECRETS_DIR:", SECRETS_DIR
print "APIAKS_ENABLED:", APIAKS_ENABLED
print " "
