# -*- coding: utf-8 -*-
"""Obsahuje nástroje pro práci s formátem MARC21 z Python2, používané zejména
z library_helpers,v ostatních případech je CPK API json a v něm obsažené XML
MARC21 zpracováno už v ziskej.cpk_api.  Pro knihovny je ale potřeba plná verze
dat přímo v Získej app.
"""

import logging

from ziskej.site.utils_lib import pprint, safe_utf8


logger = logging.getLogger("ziskej.site.marc21")


class ZiskejMarc(object):
    """
    """

    def __init__(self):
        super(ZiskejMarc, self).__init__()

    def _parse_field_string(self, field_value, order=None, do_not_use_others=False, only_first=True):
        """Na výstupu chceme string, ale ten může být přímo, nebo v dict nebo
        v listu, kde jednotlivé položky jsou string nebo dict.
        """

        DEBUG = False

        if DEBUG:
            print '_parse_field_string'
            print field_value
            print ' '

        if field_value is None:
            return None
        result = []

        # chceme list
        if not isinstance(field_value, list):
            field_value = [field_value, ]

        # najdi všechny nebo první rozumný a skonči
        for fvi in field_value:
            if DEBUG:
                print ' '
                print fvi

            # string, není co řešit
            if isinstance(fvi, basestring):
                if DEBUG:
                    print '- basestring'
                result_item = fvi.strip()

            # dict je zajímavější
            elif isinstance(fvi, dict):
                if DEBUG:
                    print '- dict'

                # nejprve specifikované pořadí subfields
                if order is None:
                    subfields_keys = []
                else:
                    subfields_keys = [x for x in order]

                # pokud nejsou výlučně jen ty v pořadí, pak všechny, které máme
                # v datech, ledaže bychom je už použili díky specifikovanému
                # pořadí
                if not do_not_use_others:
                    for k in fvi.keys():
                        if k in subfields_keys:
                            continue
                        subfields_keys.append(k)
                if DEBUG:
                    print 'subfields_keys'
                    print subfields_keys

                # najdi první neprázdný
                for k in subfields_keys:
                    result_item = fvi.get(k, None)
                    if isinstance(result_item, basestring):
                        result_item = result_item.strip()
                    if result_item:
                        break  # only_first?

            if DEBUG:
                print 'result_item'
                print result_item

            # pokud stačí první, tak ho máme a končíme
            if only_first and result_item:
                if DEBUG:
                    print 'only_first'
                return result_item

            # jinak hledáme postupně všechny
            result.append(result_item)

        return result

    def _parse_field_onedict(self, field_value, grace_key=None, prefer_string=True, dict_kv_pair=None):

        DEBUG = False

        if DEBUG:
            print '_parse_field_pseudodict'
            print field_value
            print ' '

        if field_value is None:
            return None
        result = dict()

        # chceme list
        if not isinstance(field_value, list):
            field_value = [field_value, ]

        # najdi všechny nebo první rozumný a skonči
        for fvi in field_value:
            if DEBUG:
                print ' '
                print fvi

            # fvi string - pokud nneí grace, tak skip, jinak zpracovat jako
            # key:value grace_key:fvi
            if isinstance(fvi, basestring):
                if DEBUG:
                    print '- basestring'

                fvi_v = fvi.strip()
                if DEBUG:
                    print 'fvi_v'
                    print fvi_v

                if not grace_key:
                    if DEBUG:
                        print 'skipped - no grace key'
                    continue

                k = grace_key
                v = fvi_v

                # místo jednoprvkových seznamů rovnou string
                if prefer_string:
                    if k not in result:
                        result[k] = v
                    else:
                        if not isinstance(result[k], list):
                            result[k] = [result[k], ]
                        result[k].append(v)
                # vždy seznam
                else:
                    if k not in result:
                        result[k] = [v]
                    else:
                        result[k].append(v)

                continue

            # fvi dict
            if not isinstance(fvi, dict):
                if DEBUG:
                    print '- uknown, skipped'
                continue

            # fvi dict odpovídající dict_kv_pair, konvertujme ho tedy na
            # jeden key:value pair podle dict_kv_pair
            if dict_kv_pair and set(fvi.keys()) == set(dict_kv_pair):

                if DEBUG:
                    print '- dict as dict_kv_pair'

                k = fvi[dict_kv_pair[0]]
                v = fvi[dict_kv_pair[1]]

                # místo jednoprvkových seznamů rovnou string
                if prefer_string:
                    if k not in result:
                        result[k] = v
                    else:
                        if not isinstance(result[k], list):
                            result[k] = [result[k], ]
                        result[k].append(v)
                # vždy seznam
                else:
                    if k not in result:
                        result[k] = [v]
                    else:
                        result[k].append(v)

                continue

            if DEBUG:
                print '- dict'

            for k in fvi.keys():
                v = fvi.get(k, None)
                if not v:
                    continue

                # místo jednoprvkových seznamů rovnou string
                if prefer_string:
                    if k not in result:
                        result[k] = v
                    else:
                        if not isinstance(result[k], list):
                            result[k] = [result[k], ]
                        result[k].append(v)
                # vždy seznam
                else:
                    if k not in result:
                        result[k] = [v]
                    else:
                        result[k].append(v)

        return result

    def _parse_field_listofdict(self, field_value, grace_key=None, prefer_string=True, dict_kv_pair=None):

        DEBUG = False

        if DEBUG:
            print '_parse_field_pseudodict'
            print field_value
            print ' '

        if field_value is None:
            return None
        result = []

        # chceme list
        if not isinstance(field_value, list):
            field_value = [field_value, ]

        # najdi všechny nebo první rozumný a skonči
        for fvi in field_value:
            if DEBUG:
                print ' '
                print fvi

            # fvi string - pokud nneí grace, tak skip, jinak zpracovat jako
            # key:value grace_key:fvi
            if isinstance(fvi, basestring):
                if DEBUG:
                    print '- basestring'

                fvi_v = fvi.strip()
                if DEBUG:
                    print 'fvi_v'
                    print fvi_v

                if not grace_key:
                    if DEBUG:
                        print 'skipped - no grace key'
                    continue

                k = grace_key
                v = fvi_v

                result.append({k: v})

                continue

            # fvi dict
            if not isinstance(fvi, dict):
                if DEBUG:
                    print '- uknown, skipped'
                continue

            # fvi dict odpovídající dict_kv_pair, konvertujme ho tedy na
            # jeden key:value pair podle dict_kv_pair
            if dict_kv_pair and set(fvi.keys()) == set(dict_kv_pair):

                if DEBUG:
                    print '- dict as dict_kv_pair'

                k = fvi[dict_kv_pair[0]]
                v = fvi[dict_kv_pair[1]]

                result.append({k: v})

                continue

            if DEBUG:
                print '- dict'

            result.append(dict(fvi))

        return result

    def parse_library_std(self, field_value):
        parsed = self._parse_field_string(field_value)
        if not parsed:
            parsed = u''
        if not isinstance(parsed, basestring):
            print field_value
            pprint(parsed)
            raise Exception('Explore data')
        return parsed

    def parse_library_url(self, field_value):
        DEBUG = False

        #parsed = self._parse_field_listofdict(field_value)
        parsed = self._parse_field_listofdict(field_value,
                                              grace_key = 'web',
                                              prefer_string = True,
                                              dict_kv_pair = ['z', 'u'])

        if DEBUG:
            print 'parse_library_url'
            print field_value
            pprint(parsed)

        if not parsed:
            parsed = []

        parsed_web = []
        for parsed_item in parsed:
            if parsed_item.keys() != ['web']:
                continue
            parsed_web.append(parsed_item['web'])
        if not len(parsed_web):
            for parsed_item in parsed:
                parsed_web.append(parsed_item[parsed_item.keys()[0]])
        if not len(parsed_web):
            result = u''
        else:
            result = parsed_web[0]

        if DEBUG:
            print result
            print ' '
        return result

    def parse_library_address(self, field_value):
        DEBUG = False

        parsed = self._parse_field_listofdict(field_value)

        if DEBUG:
            print 'parse_library_address'
            print field_value
            pprint(parsed)

        result = []
        if not parsed:
            parsed = [dict()]

        for parsed_item in parsed:
            if not parsed_item:
                parsed_item = dict()
            pi_keys = parsed_item.keys()
            if 'c' in pi_keys and 'm' in pi_keys:
                if 'u' in pi_keys:
                    result_item = u'{u}, {c} {m}'.format(**parsed_item)
                else:
                    result_item = u'{c} {m}'.format(**parsed_item)
            else:
                print field_value
                pprint(parsed)
                raise Exception('Explore data')
            if 'p' in pi_keys:
                result_item += u' ({p})'.format(**parsed_item)
            result.append(result_item)
        result = u', '.join(result)

        if DEBUG:
            print result
            print ' '
        return result
