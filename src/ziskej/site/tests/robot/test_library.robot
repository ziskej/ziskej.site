# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s ziskej.site -t test_library.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src ziskej.site.testing.ZISKEJ_SITE_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot src/plonetraining/testing/tests/robot/test_library.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a Library
  Given a logged-in site administrator
    and an add library form
   When I type 'My Library' into the title field
    and I submit the form
   Then a library with the title 'My Library' has been created

Scenario: As a site administrator I can view a Library
  Given a logged-in site administrator
    and a library 'My Library'
   When I go to the library view
   Then I can see the library title 'My Library'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add library form
  Go To  ${PLONE_URL}/++add++Library

a library 'My Library'
  Create content  type=Library  id=my-library  title=My Library


# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form.widgets.title  ${title}

I submit the form
  Click Button  Save

I go to the library view
  Go To  ${PLONE_URL}/my-library
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a library with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the library title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
