# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from ziskej.site.testing import ZISKEJ_SITE_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that ziskej.site is properly installed."""

    layer = ZISKEJ_SITE_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if ziskej.site is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'ziskej.site'))

    def test_browserlayer(self):
        """Test that IZiskejSiteLayer is registered."""
        from ziskej.site.interfaces import (
            IZiskejSiteLayer)
        from plone.browserlayer import utils
        self.assertIn(IZiskejSiteLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = ZISKEJ_SITE_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['ziskej.site'])

    def test_product_uninstalled(self):
        """Test if ziskej.site is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'ziskej.site'))

    def test_browserlayer_removed(self):
        """Test that IZiskejSiteLayer is removed."""
        from ziskej.site.interfaces import \
            IZiskejSiteLayer
        from plone.browserlayer import utils
        self.assertNotIn(IZiskejSiteLayer, utils.registered_layers())
