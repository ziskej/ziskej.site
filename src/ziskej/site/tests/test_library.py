# -*- coding: utf-8 -*-
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from ziskej.site.interfaces import ILibrary
from ziskej.site.testing import ZISKEJ_SITE_INTEGRATION_TESTING  # noqa
from zope.component import createObject
from zope.component import queryUtility

import unittest


class LibraryIntegrationTest(unittest.TestCase):

    layer = ZISKEJ_SITE_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='Library')
        schema = fti.lookupSchema()
        self.assertEqual(ILibrary, schema)

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='Library')
        self.assertTrue(fti)

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='Library')
        factory = fti.factory
        obj = createObject(factory)
        self.assertTrue(ILibrary.providedBy(obj))

    def test_adding(self):
        obj = api.content.create(
            container=self.portal,
            type='Library',
            id='Library',
        )
        self.assertTrue(ILibrary.providedBy(obj))
