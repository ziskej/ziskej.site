# -*- coding: utf-8 -*-
"""Pomocný nástroj a základní funkcionalita Auto LB (automatický Load
Balancing) pro přidělování požadavků a celkové napojení na CPK pro
potřeby zjištění metadat pro poptávaný dokument atp.

Gateways:

Gateways, aneb propojení se zbytkem systému Získej, víceméně API.

Modul poskytuje pro zbytek systému funkcionalitu týkající se načtení informací
z CPK při vytváření objednávky (včetně přípravy pro pozdější Auto LB alg)
a zejména Auto LB alg pro načtení dostupnosti dokumentů v jednotlivých
knihovnách a určení jejich navrženého pořadí.

# voláno z ticket_create
def get_doc_title(doc_id):

# voláno z ticket_create, dvakrát
def parse_cpk_post(portal,

# voláno z ticket při přípravě nebo zpracování formuláře - tam jsou potřeba
# všechny možnosti
def load_balancing_dk_list(portal, ticket, restrict_doc_ids=None, include_doc_data=False):

# voláno z ticket ajax init, vrací trojici a/f/t
def load_balancing_availability_init(portal, ticket):

# voláno z ticket ajax get, vrací trojici a/f/t
def load_balancing_availability_retrieve(portal, ticket):


Datová struktura, verze 2:

- _doc_data: version, doc_id, sigla, unit_uds, fullname

- _docs_data: version, doc_ids, data: doc_id, sigla, unit_uds


Podrobnější popis datových struktur a propojení s CT ticket viz metody:

- _load_ticket
  ticket.doc_data: json verze _doc_data
  ticket.docs_data: json verze _docs_data
  ticket.doc_order_blacklist: string obsahující seznam doc_id oddělený
                               pomocí ',' - blacklist této objednávky, např. kde
                               DK odmítla dodat, protože je dokument ztracen
                               načte se do _history_blacklist jako list doc_id

- _save_ticket
  ticket._doc_data: json verze _doc_data
  ticket._docs_data: json verze _docs_data

- _set_suggested
  ticket.doc_order_suggested: string obsahující seznam doc_id oddělený
                              pomocí ','
  ticket.doc_order_suggested_data: json verze kompletního dk_list (tedy vč.
                                   nedostupných položek)


Užití:


1) Create new ticket

Při vytváření ticketu se používá class LoadBalancingBase obsahující k tomu
potřebnou funkciolaniltu, ale bez Auto LB a jeho podpůrných metod.

- gateway get_doc_title
  voláno z CreateView aka ticket_create_view
  tam se přihlášený z cpk_fake vůbec nedostane - jde o stránku před přihlášením

  CpkDocument(doc_id) jen pro hlavní doc_id

  ziskej-async
  GET /cpkd/document?id=mzk.MZK01-000935298

  CPK API
  url: https://www.knihovny.cz/api/v1/record
  params: {'id': 'mzk.MZK01-000935298'}
  url: https://www.knihovny.cz/api/v1/record
  params: {'id': 'mzk.MZK01-000935298', 'field[]': 'fullRecord'}

- gateway parse_cpk_post
  voláno dvakrát z CreateSummaryView aka ticket_create_summary_view:
  - default action - entry point when coming from CPK, from Shibboleth etc.
  - is_submit and not doc_manual .. tedy skutečné vytvoření objednávky s vazbou na CPK

  CpkDocument(doc_id) postupně pro všechny doc_id

  ziskej-async

  CPK API


2) Auto LB - AJAX

Používá class LoadBalancing pomocí gateways, viz výše.

Angular frontend aplikace nejprve inicializuje proces pomocí
load_balancing_availability_init a potom se postupně ptá na mezivýsledky pomocí
load_balancing_availability_retrieve.

Systém o stavu odpovídá pomocí aft (available, finished, total), kde available
je počet nalezených dostupných doc_id (ne jednotek, ale dokumentů), finished je
počet doc_id, pro které je hledání dostupnosti ukončeno a total je celkový počet
doc_id včetně knihoven, které jsou v Získej.

Proces je ukončen ve chvíli, kdy finished >= total a/nebo jej uživatel v UI
ukončí.  Nicméně každý mezivýsledek je ukončený ve smyslu, že je na něm možné
dál stavět - budovat dk_list, suggested etc.


Auto LB Revize 2.2 (tzv. ideální řešení) - specifikace

1) Revize dotazů před dostupností

   get_doc_title

       Nechat beze změny dotaz na main doc_id.

   parse_cpk_post

       Je zbytečné získávat všechny detaily pro všechny doc_id včetně
       alternativních, stačí detaily pro main a pro alternativní jen doc_id,
       sigla a unit_ids - využitelné pro optimalizaci Auto LB.

2) Auto LB základní filtr a příprava (bez komunikace s CPK).  Auto LB má
   k dispozici pro každé doc_id siglu a unit_ids (a tím i jejich počet).
   Podle toho se odfiltruje co ano a co ne (není v Získej atp.) a připraví se
   data pro další kroky.

3) Load balancing následně začne pálit dotazy na dostupnost tak, aby pro jedno
   doc_id se ptal vždy jen na jednu jednotku - postupně to začne upřesňovat
   výsledky a knihovník to může zarazit, když už je spokojený, nebo nechat dojet
   delší dobu či až do konce, pokud chce co nejpřesnější výsledky

4) Pořadí DK bude zobrazovat informace ke všem doc_id a to i těm, které nejde
   přiřadit pomocí Auto LB (protože není dostupné etc.), protože i ta informace
   je zajímavá (důvod proč ne, může zavolat a zadat manuálně DK etc.

5) docs_data a doc_data struktura nově a začít verzovat.  Verze 1 (bez verze) je
   nekompatibilní.  Verze 2 (současná) bude zpětně kompatibilní i v dalších
   verzích nebo bude implementována migrace dat.

"""

from __future__ import print_function

import json
import logging
import random

from ziskej.site import BUILDOUT_TYPE
from ziskej.site.cpk_api import (
    CpkDocument,
    CpkDocumentsOverview,
    CpkApiException,
    )
from ziskej.site.cpk_api_v2 import (
    CpkUnitAsync,
    CpkApiException2,
    )
from ziskej.site.storage import (
    storage_set,
    storage_get,
    storage_del,
    storage_reset,
    )
from ziskej.site.utils_lib import (
    pprint,
    get_time0,
    get_time_ms,
    )
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger('ziskej.ticket_load_balancing')
logger_lbts = logging.getLogger('loglbts')


# Verze datové struktury _doc_data a _docs_data
VERSION = 2

# Dočasný debug pomocí print a pprint
DEBUG = False
# Vypisovat výpočty ratingu pomocí print
DEBUG_RATING = False
DEBUG_RATING_DK = False
DEBUG_RATING_VAC = False
# Vypisovat availability parse print
DEBUG_AVAILABILITY = False

# Pouze knihovny aktivované v Získej mohou být dožádanou knihovnou
ONLY_ZISKEJ_DK = True

# Přidávat rating do sekce
#     "Doplňující informace z automatického načtení dostupnosti:"
# viditelné v Získej UI, ale jen ve vývojové a testovací instanci
RATING_DESC_DEBUG = bool(BUILDOUT_TYPE in ('development', 'test', ))


class LoadBalancingException(Exception):
    pass


class LoadBalancingBase(object):
    """Load balancing base obsahuje základní funkcionalitu pro parse_cpk_post.
    Nebsahuje tedy samotný load balancing, ale pouze přípravu pro něj.
    """

    def __init__(self, portal):
        super(LoadBalancingBase, self).__init__()

        if DEBUG:
            print('-- LoadBalancingBase init')

        self.portal = portal

        self.par = dict(

            # Rating limit viz LB alg dokumentaci
            limit = 1000,

            # Penalizace za DK

            # ŽK
            lib_zk = 2000,

            # Souborný katalog
            lib_caslin = 2000,

            # DK není v CPK ai pasivně (neznáme ji)
            lib_not_cpk = 2000,

            # DK není aktivní v Získej
            lib_not_ziskej = 1000,

            # DK je na blacklistu ŽK
            lib_blacklist = 80,

            # DK má dovolenou
            load_balancing_holiday_penalty = 1000,

            # Default
            lib = 50,

            # DK je na blacklistu ŽK
            lib_whitelist = 10,

            # Penalizace za (ne)dostupnost

            # jednotka je nedostupná
            unavailable = 10000,

            # dostupnost není známa
            unknown = 1000,

            # je dostupné později
            #available_later = 500,

            # dostupné jen do studovny
            available_stud = 200,

            # dostupné
            available = 100,

            # SUGGESTION penalizace za rychlost dodání - par

            # Bonus za počet jednotek
            additional_unit = -1,

            # Limit pro tento bonus
            additional_unit_limit = -9,

            # Penalizace za historii
            history_m = 5,  # poslední měsíc
            history_q = 2,  # poslední 3 měsíce
            history_s = 1,  # posledních 6 měsíců
            history_limit = 100,  # limit pro tuto penalizaci

            )

        # pokud má být součástí cfg přes web, pak přidat jako parametr init
        # scriptu a začít používat - zbytek je připraven
        self.settings = None

        self._state = u''

        self.service = u'mvs'

        # main doc_data = ticket.doc_data
        self._doc_data = dict(
            version = VERSION,
            service = self.service,
            doc_id = u'',  # document id v CPK
            fullname = u'',  # citace
            sigla = u'',  # sigla knihovny
            unit_ids = [],  # seznam id jednotek v dané knihovně pro tento dokument
            #availability = dict(),  # dostupnost jednotlivých jednotek
            #orig_doc_data = None,  # doc_data z CPK
            )

        # alt doc_data = ticket.docs_data
        # víceméně napodobeniny ordered dict
        # jednotlivé value v doc_alt_data odpovídají self._doc_data pro hlavní doc_id
        self._docs_data = dict(
            version = VERSION,
            service = self.service,
            doc_alt_ids = [],
            doc_alt_data = dict(),
            )

        # odhad poplatku MVS
        self.fee_est = dict(min=None, avg=None, max=None, avgsum=0, avgcount=0, count=0)

        self.testing_now_dt = ziskej_parameter(
            'testing_now_str', portal=self.portal)

        # Ignorovat chybějící unit_ids
        self.ignore_missing_unit_ids = False

    @property
    def doc_id(self):
        """doc_id hlavního dokumentu"""
        if DEBUG:
            print('-- LoadBalancingBase doc_id')
        if not self._state:
            raise LoadBalancingException('LB is not initalized')
        return self._doc_data['doc_id']

    @property
    def fullname(self):
        """doc_id hlavního dokumentu"""
        if DEBUG:
            print('-- LoadBalancingBase fullname')
        if not self._state:
            raise LoadBalancingException('LB is not initalized')
        return self._doc_data['fullname']

    def _validate_doc_data(self, doc_id, doc_data):
        if DEBUG:
            print('-- LoadBalancingBase _validate_doc_data')
        # validate doc_data
        if not doc_data:
            return 'Not enough data for main doc'
        if not doc_data.get('id', u''):
            return 'Missing doc_id for main doc'
        if doc_id != doc_data.get('id', u''):
            return 'Invalid doc_id for main doc'
        if doc_id.startswith('caslin'):
            return 'Invalid doc_id - caslin'
        if not doc_data.get('citation', u''):
            return 'Missing fullname'
        if not doc_data.get('sigla', u''):
            return 'Missing sigla'
        if not len(doc_data.get('unit_ids', [])):
            if not self.ignore_missing_unit_ids:
                return 'Missing unit ids'
        return None

    def _set_main_doc(self, doc_id, doc_data):
        if DEBUG:
            print('-- LoadBalancingBase _set_main_doc')
        # doc_data už je zvalidováno (_validate_doc_data)

        self._doc_data['version'] = VERSION
        self._doc_data['service'] = self.service
        self._doc_data['doc_id'] = doc_id
        #self._doc_data['fullname'] = self._generate_fullname_from_doc_data(doc_data)
        self._doc_data['fullname'] = doc_data.get('citation', u'')

        # dříve nebylo používáno, přidáno pro EDD, zatím využit jen title
        self._doc_data['fullname_source'] = doc_data.get('citation_dict', dict())
        # self._doc_data['title'] = doc_data.get('title', u'')
        # self._doc_data['author'] = doc_data.get('author', u'')
        # self._doc_data['year_publication'] = doc_data.get('year_publication', u'')

        self._doc_data['sigla'] = doc_data.get('sigla', u'')
        self._doc_data['unit_ids'] = doc_data.get('unit_ids', [])
        #self._doc_data['availability'] = dict()
        #self._doc_data['orig_doc_data'] = doc_data
        self._doc_data['caslin_info'] = doc_data.get('caslin_info', None)

        self._docs_data = dict(
            version = VERSION,
            service = self.service,
            doc_ids = [],
            data = dict(),
            )
        self._add_doc(doc_id, doc_data)

        self._state = u'initialized'

    def _add_doc(self, doc_alt_id, doc_alt_data):
        if not doc_alt_id or doc_alt_id == 'None':
            logger.info('-- LoadBalancingBase _add_doc({}) - skipped due to invalid doc_id'.format(doc_alt_id))
            return
        if doc_alt_id in self._docs_data['doc_ids']:
            if DEBUG:
                print('-- LoadBalancingBase _add_doc({}) - skipped due to duplicated doc_id'.format(doc_alt_id))
            return
        if DEBUG:
            print('-- LoadBalancingBase _add_doc({})'.format(doc_alt_id))
        self._docs_data['doc_ids'].append(doc_alt_id)
        if doc_alt_data:
            self._docs_data['data'][doc_alt_id] = dict(
                service = self.service,
                doc_id = doc_alt_id,
                sigla = doc_alt_data.get('sigla', u''),
                unit_ids = doc_alt_data.get('unit_ids', []),
                caslin_info = doc_alt_data.get('caslin_info', None),
                )

    def _get_cpk_data(self, doc_id):
        """Získat cpk_data pomocí volání ziskej-async pro zadané doc_id.

        Příklad doc_data:

        {
            'id': mzk.MZK01-000935298,
            'title': Deník malého poseroutky : zápisky Grega Heffleyho,
            'author': Jeff Kinney, 1971-,
            'year_publication': 2009
            'citation': KINNEY, Jeff. Deník malého poseroutky. 1. vyd. Praha: Albatros, 2009. 218 s. ISBN 978-80-00-02225-3.,
            'availability': {},
            'sigla': BOA001,
            'unit_ids': [BOA001.MZK01000935298.MZK50000935298000010,
                      BOA001.MZK01000935298.MZK50000935298000020,
                      BOA001.MZK01000935298.MZK50000935298000030],
        }
        """

        if DEBUG:
            print('-- LoadBalancingBase _get_cpk_date')
        cpk_doc = CpkDocument(self.settings, doc_id)
        cpk_doc.ignore_missing_unit_ids = self.ignore_missing_unit_ids
        try:
            doc_data = cpk_doc.call()
        except CpkApiException, e:
            # SUGGESTION zvážit, zda kontrolu chyb z async wrapperu pro CPK API
            # neodchytávat a nezpracovávat už tady
            # FIXME-EDD Pro debug EDD nahradit čistě jen raise
            raise LoadBalancingException(str(e))

        if DEBUG:
            print(' ')
            print('doc_data')
            pprint(doc_data)
            print(' ')

        return doc_data

    def _get_cpk_docs_overview(self, doc_ids):
        if not doc_ids:
            return []
        if DEBUG:
            print('-- LoadBalancingBase _get_cpk_docs_overview')
        cpk_doc = CpkDocumentsOverview(self.settings, doc_ids)
        try:
            docs_data = cpk_doc.call()
        except CpkApiException, e:
            raise

        if DEBUG:
            print(' ')
            print('docs_data')
            pprint(docs_data)
            print(' ')

        return docs_data

    def _get_siglas(self):
        result = []
        for doc_id in self._docs_data['data']:
            sigla = self._docs_data['data'][doc_id].get('sigla', None)
            if sigla:
                result.append(sigla)
        return result

    def _export_parsed_cpk_post(self, include_doc_data=False):
        if DEBUG:
            print('-- LoadBalancingBase _export_parsed_cpk_post')
        if not self._state:
            raise LoadBalancingException('LB is not initalized')

        lb_data = dict(
            id = self._doc_data['doc_id'],
            service = self.service,
            fullname = self._doc_data['fullname'],
            )
        if self.service == 'mvs':
            lb_data['fee_est'] = self.fee_est

        if include_doc_data:

            # dříve nebylo používáno, přidáno pro EDD, zatím využit jen title
            lb_data['fullname_source'] = self._doc_data['fullname_source']
            # lb_data['title'] = self._doc_data['title']
            # lb_data['author'] = self._doc_data['author']
            # lb_data['year_publication'] = self._doc_data['year_publication']

            lb_data['doc_data_json'] = json.dumps(self._doc_data, ensure_ascii=False)
            lb_data['docs_data_json'] = json.dumps(self._docs_data, ensure_ascii=False)
            lb_data['siglas'] = self._get_siglas()

        return lb_data

    def get_doc_title(self, doc_id):
        """Vrátí citaci pro daný dokument."""
        if DEBUG:
            print('-- LoadBalancingBase get_doc_title')
        doc_data = self._get_cpk_data(doc_id)
        if doc_data is None:
            raise LoadBalancingException('Missing doc_data')
        #fullname = self._generate_fullname_from_doc_data(doc_data)
        fullname = doc_data.get('citation', u'')
        if not fullname:
            raise LoadBalancingException('Missing fullname')
        sigla = doc_data.get('sigla', None)
        if not sigla:
            raise LoadBalancingException('Missing sigla')
        return fullname

    def parse_cpk_post(self, cpk_post_raw, library_zk_sigla=None,
                       include_fee_est=False, include_doc_data=False):
        if DEBUG:
            print('-- LoadBalancingBase parse_cpk_post')
        # cpk_post_raw
        # {doc_alt_ids: [u'vkol.SVK01-000887781', u'nkp.NKC01-001866320', u'kvkl.LiUsCat_0234552', u'svkul.KN3148000000413883', u'mzk.MZK01-000935298', u'cbvk.CbvkUsCat_0126579', u'svkkl.KlUsCat_0670218', u'kkvy.KN3170000000391293', u'kfbz.445566', u'kjdpb.KN3151000000144290', u'mkkh.168066', u'caslin.SKC01-003767416', u'okpb.KN3183000000224688', u'svkos.MVK01-000530432', u'tre.56714', u'vkta.KN3171000000253701', u'mkchodov.119563', u'mkpr.KN3168000000205852', u'mkuo.62155', u'mkp.2775336'],
        #  doc_id: mzk.MZK01-000935298,
        #  source_id: knihovny.cz,
        #  user_id: }

        if not cpk_post_raw:
            raise LoadBalancingException('Missing cpk_post_raw')

        # service
        self.service = cpk_post_raw.get('service', u'mvs')
        if self.service not in ('mvs', 'edd', ):
            logger.info('Invalid cpk_post_raw, invalid service {}'.format(repr(self.service)))
            raise LoadBalancingException('Invalid cpk_post_raw, invalid service')

        # main doc_id
        doc_id = cpk_post_raw.get('doc_id', None)
        if not doc_id:
            raise LoadBalancingException('Invalid cpk_post_raw, missing doc_id')

        # alternativní doc_id
        # parsování z CPK POST
        if cpk_post_raw['doc_alt_ids']:
            try:
                doc_alt_ids = str(cpk_post_raw['doc_alt_ids'])
                if '[' not in doc_alt_ids:
                    doc_alt_ids = '["{doc_alt_ids}"]'.format(doc_alt_ids=doc_alt_ids)
                else:
                    doc_alt_ids = doc_alt_ids.replace("'", '"')
                    if '[u"' in doc_alt_ids:
                        doc_alt_ids = doc_alt_ids.replace('[u"', '["')
                    if ', u"' in doc_alt_ids:
                        doc_alt_ids = doc_alt_ids.replace(', u"', ', "')
                doc_alt_ids = json.loads(doc_alt_ids)
            except Exception, e:
                logger.info("cpk_post_raw['doc_alt_ids']")
                logger.info(cpk_post_raw['doc_alt_ids'])
                logger.exception(e)
                doc_alt_ids = []
        else:
            doc_alt_ids = []

        # Najít primární dokument a nastavit doc_data pro něj
        doc_data = self._get_cpk_data(doc_id)
        error = self._validate_doc_data(doc_id, doc_data)
        if error:
            # Nastala situace, kdy primární dokument není validní, např. nemá
            # jednotky nebo se jedná o souborný katalog.  Nyní je potřeba najít
            # první validní alternativní dokument a ten použít jako primární.
            errors = [error]
            doc_id_original = doc_id
            doc_data_original = doc_data
            doc_alt_ids_original = [x for x in doc_alt_ids]  # deep copy
            doc_ids_not_valid = [doc_id, ]
            doc_ids_to_check = [x for x in doc_alt_ids]  # deep copy
            while len(doc_ids_to_check):
                doc_id = doc_ids_to_check[0]
                doc_data = self._get_cpk_data(doc_id)
                error = self._validate_doc_data(doc_id, doc_data)
                del doc_ids_to_check[0]
                if error:
                    errors.append(error)
                    doc_ids_not_valid.append(doc_id)
                else:
                    break
            if not error:
                # doc_id a doc_data už jsou správně na novém primárním dokumentu
                doc_alt_ids = [x for x in doc_ids_not_valid]  # deep copy
                if len(doc_ids_to_check):
                    doc_alt_ids.extend(doc_ids_to_check)

        # Nepodařilo se najít žádný validní dokument, reportovat a skončit
        if error:
            logger.info("LoadBalancingException debug doc_id: {}".format(str(doc_id_original)))
            logger.info("LoadBalancingException debug doc_data: {}".format(str(doc_data_original)))
            logger.info("LoadBalancingException debug doc_alt_ids: {}".format(str(doc_alt_ids_original)))
            for item in errors:
                logger.info("LoadBalancingException debug error: {}".format(str(item)))
            # Reportovat první chybu.
            raise LoadBalancingException(errors[0])
            # Případná alternativa:
            # Reportovat první významnou chybu, jinak první chybu.  Významnou
            # chybou není souborný katalog a absence jednotek.
            # valid_errors = [x for x in errors 
            #                 if x not in ('Invalid doc_id - caslin',
            #                              'Missing unit ids, ')]
            # if len(valid_errors):
            #     raise LoadBalancingException(valid_errors[0])
            # else:
            #     raise LoadBalancingException(errors[0])

        # Nastavit primární dokument
        # Už dříve bylo validováno, viz _validate_doc_data()
        self._set_main_doc(doc_id, doc_data)

        if DEBUG:
            logger.info("LoadBalancing debug doc_id: {}".format(str(doc_id)))
            logger.info("LoadBalancing debug doc_data: {}".format(str(doc_data)))
            logger.info("LoadBalancing debug doc_alt_ids: {}".format(str(doc_alt_ids)))

        # set alt docs

        # dotazovat se na doc_data k alternativám?
        REQUEST_ALT_DOC_DATA = True

        # dotazovat se na doc_data k alternativám pomocí co nejmenšího počtu
        # requestů?  caslin potřebujeme zvlášť, po jednom, v každém případě
        REQUEST_ALT_DOC_DATA_USING_ONE_CALL = True

        if not REQUEST_ALT_DOC_DATA:
            # bez doc_data
            for doc_alt_id in doc_alt_ids:
                self._add_doc(doc_alt_id, None)

        elif REQUEST_ALT_DOC_DATA_USING_ONE_CALL:
            # doc_data pomocí co nejmenšího počtu requestů, netýká se caslin
            doc_alt_ids_wo_caslin = [x for x in doc_alt_ids if not x.startswith('caslin.')]
            doc_alt_ids_caslin = [x for x in doc_alt_ids if x.startswith('caslin.')]
            # vše kromě caslin v rámci jednoho requestu
            docs_alt_data = self._get_cpk_docs_overview(doc_alt_ids_wo_caslin)
            for doc_alt_id in doc_alt_ids_wo_caslin:
                if doc_alt_id in docs_alt_data:
                    self._add_doc(doc_alt_id, docs_alt_data[doc_alt_id])
                else:
                    self._add_doc(doc_alt_id, None)
            # caslin samostatně po jednom
            for doc_alt_id in doc_alt_ids_caslin:
                doc_alt_data = self._get_cpk_data(doc_alt_id)
                self._add_doc(doc_alt_id, doc_alt_data)

        else:
            # samostatně po jednom
            for doc_alt_id in doc_alt_ids:
                doc_alt_data = self._get_cpk_data(doc_alt_id)
                self._add_doc(doc_alt_id, doc_alt_data)

        # export as lb_data
        lb_data = self._export_parsed_cpk_post(include_doc_data=include_doc_data)

        return lb_data


class LoadBalancing(LoadBalancingBase):

    def __init__(self, portal):
        if DEBUG:
            print('-- LoadBalancing init')
        super(LoadBalancing, self).__init__(portal)

        # ticket.doc_history
        # [dict(id='foo', can_repeat=False, ts=<timestamp as iso>)]
        # self._history = dict(
        #     title = u'',
        #     ids = [],
        #     docs = dict()
        #     )

        # doc ids blacklist - na základě už proběhlých přidělení a jejich
        # výsledku - někdy podpožadavek skončí, ale příslušný dokument je možné
        # znovu použít, v jiných případech ne, tak se o to není možné snažit
        # Ex. ['foo', ]
        self._history_blacklist = []

        self.library_zk = None
        self.whitelist = []
        self.blacklist = []

        self.aft = None

        self.service = u'mvs'


    def _get_library(self, sigla):
        if DEBUG:
            print('-- LoadBalancing _get_library({})'.format(sigla))
        portal = self.portal
        if not sigla:
            return None
        library_id = sigla.lower()
        library = getattr(portal.libraries, library_id, None)
        if library is None:
            logger.warning('Library {} is not in Ziskej'.format(library_id))
        return library

    def _setup_whitelist_blacklist(self, library_zk_sigla):
        if DEBUG:
            print('-- LoadBalancing _setup_whitelist_blacklist')

        self.library_zk = self._get_library(library_zk_sigla)

        if self.library_zk is None:
            self.whitelist = []
            self.blacklist = []
            print('-- LoadBalancing _setup_whitelist_blacklist missing library_zk')
            return

        self.whitelist = self.library_zk.whitelist
        if not self.whitelist:
            self.whitelist = []
        items = []
        for item in self.whitelist:
            sigla = item.strip()
            if not sigla:
                continue
            library = self._get_library(sigla)
            if not library:
                continue
            items.append(sigla)
        self.whitelist = items

        self.blacklist = self.library_zk.blacklist
        if not self.blacklist:
            self.blacklist = []
        items = []
        for item in self.blacklist:
            sigla = item.strip()
            if not sigla:
                continue
            library = self._get_library(sigla)
            if not library:
                continue
            items.append(sigla)
        self.blacklist = items

        if DEBUG:
            print('-- LoadBalancing _setup_whitelist_blacklist END:')
            print("whitelist: ", self.whitelist)
            print("blacklist: ", self.blacklist)
            print(" ")

    def lb_ts(self, txt, time0=None):
        if time0 is None:
            time0 = self.time0
        msg = u'lbts {ts} {txt}'.format(ts=get_time_ms(time0), txt=txt)
        logger_lbts.info(msg)

    def _parse_availability(self, availability_json):
        """"
        availability_json example

        {'json': {
            u'availability_note': u'Jen do studovny',
            u'duedate': None,
            u'availability': u'available',
            u'queue': u'',
            u'ext': {
                u'opac_status': u'available'
                },
            u'id': u'BOA001.MZK01000667059.MZK50000667059000010'
            },
        'duedate': None,
        'id': u'BOA001.MZK01000667059.MZK50000667059000010',
        'availability': u'available'
        }
        """

        if DEBUG:
            print('-- LoadBalancing _parse_availability')

        availability = availability_json.get('availability', None)
        duedate = availability_json.get('duedate', None)

        # parsování availability_note je připravené pro známé příklady obsahu,
        # je proto velmi nepřesné a prochází postupným zpřesňováním
        availability_note = u''
        availability_note_raw = availability_json.get('json', dict()).get('availability_note', None)
        if DEBUG or DEBUG_AVAILABILITY:
            print(' ')
            print('availability_note_raw: ', repr(availability_note_raw))
        if availability_note_raw is None:
            pass
        elif isinstance(availability_note_raw, basestring):
            availability_note += u'{} '.format(availability_note_raw)
        elif isinstance(availability_note_raw, list):
            for availability_note_item in availability_note_raw:
                if isinstance(availability_note_item, basestring):
                    availability_note += u'{}, '.format(availability_note_item)
                elif isinstance(availability_note_item, dict) and u'@text' in availability_note_item:
                    # NTK-147
                    # [{u'@attributes': {u'ns1:Scheme': u'http://www.niso.org/ncip/v1_0/imp1/schemes/itemuserestrictiontype/itemuserestrictiontype.scm'}, u'@text': u'In Library Use Only'}, {u'@attributes': {u'ns1:Scheme': u'http://www.niso.org/ncip/v1_0/imp1/schemes/itemuserestrictiontype/itemuserestrictiontype.scm'}, u'@text': u'Renewals Not Permitted'}]
                    availability_note += u'{}, '.format(availability_note_item[u'@text'])
                else:
                    logger.warning('availability_note for item in list is unexpected type, skipped: {availability_note_raw}'.format(
                            availability_note_raw = repr(availability_note_raw),
                            ))
        else:
            logger.warning('availability_note for is unexpected type, skipped: {availability_note_raw}'.format(
                availability_note_raw = repr(availability_note_raw),
                ))
        if DEBUG or DEBUG_AVAILABILITY:
            print('availability_note: ', repr(availability_note))
            print(' ')

        studovna = False
        studovna = bool(u'studovn' in availability_note.lower())

        available_bool = bool(availability in (u'available', ))
        if available_bool:
            if self.service == 'mvs':
                if not studovna:
                    available_lov = u'available'
                else:
                    available_lov = u'available_stud'
            elif self.service == 'edd':
                available_lov = u'available'
            else:
                raise LoadBalancingException('Invalid service')
        else:
            available_lov = u'unavailable'

        result = dict(
            available = available_bool,
            available_lov = available_lov,
            # duedate = duedate,
            # rating = self.par[available_lov],
            )
        if DEBUG or DEBUG_AVAILABILITY:
            print(availability_json)
        if DEBUG or DEBUG_AVAILABILITY:
            print(available_lov)
        return result

    def _load_ticket(self, ticket):
        if DEBUG:
            print('-- LoadBalancing _load_ticket')
        doc_data_json = ticket.doc_data
        docs_data_json = ticket.docs_data
        self._doc_data = json.loads(doc_data_json)
        self._docs_data = json.loads(docs_data_json)
        if self._doc_data is None:
            raise LoadBalancingException('Missing internal data')
        if self._doc_data.get('version', 1) != VERSION:
            pprint(self._doc_data)
            raise LoadBalancingException('Data structure version error')
        if self._docs_data.get('version', 1) != VERSION:
            pprint(self._docs_data)
            raise LoadBalancingException('Data structure version error')
        self.service = u'{}'.format(self._doc_data.get('service', u'mvs'))
        if self.service not in ('mvs', 'edd', ):
            raise LoadBalancingException('Invalid service in internal data')
        self._state = u'ticket_loaded'

        self._history_blacklist = []
        if ticket.doc_order_blacklist:
            items = ticket.doc_order_blacklist.split(',')
            self._history_blacklist = [x.strip() for x in items if x.strip()]

        self.doc_ids = self._docs_data['doc_ids']
        self.doc_ids = [x for x in self.doc_ids if x != self._doc_data['doc_id']]
        self.doc_ids.insert(0, self._doc_data['doc_id'])

    def _save_ticket(self, ticket):
        if DEBUG:
            print('-- LoadBalancing _save_ticket')
        doc_data_json = json.dumps(self._doc_data, ensure_ascii=False)
        docs_data_json = json.dumps(self._docs_data, ensure_ascii=False)
        ticket.doc_data = doc_data_json
        ticket.docs_data = docs_data_json
        ticket._p_changed = True

    def _load_balancing_alg(self, ticket, restrict_doc_ids=None,
                            include_doc_data=False, check_availability=False,
                            try_anything = False, force_new_availability=False):
        """Load balancing alg"""

        self.time0 = get_time0()

        if DEBUG:
            print(' ')
            print('-- LoadBalancing._load_balancing_alg(ticket={ticket}, restrict_doc_ids={restrict_doc_ids}, include_doc_data={include_doc_data})'.format(ticket=ticket.hid, restrict_doc_ids=restrict_doc_ids, include_doc_data=include_doc_data))
            print(' ')

        cpkunitasync = CpkUnitAsync()

        self._load_ticket(ticket)
        self._setup_whitelist_blacklist(ticket.library_zk)

        self.lb_ts(u'before for doc_ids')

        # pro všechny dokumenty, které CPK považuje za záměnné
        doc_counter = 0
        for doc_id in self.doc_ids:

            # přeskočit ty, pro které nemáme doc data (nemělo by nastat, ale
            # může, není to v moci Získej zaručit, že v CPK nenastala situaci,
            # kdy zadala jako doc_alt_ids doc_id, které neexistuje)
            if doc_id not in self._docs_data['data']:
                continue

            doc_counter += 1

            if DEBUG:
                print(' ')
                print('doc_id: ', doc_id, ' (', doc_counter, '/', len(self.doc_ids), ')')

            if not force_new_availability and self._docs_data['data'][doc_id].get('lb_used', None) == 'forbidden':
                if DEBUG or DEBUG_RATING:
                    print('skip already skipped')
                continue

            # načíst dříve zjištěná data (zatím jsme offline)
            doc_data = self._docs_data['data'].get(doc_id, None)
            if not doc_data:
                # přeskočit ty, pro které nemáme doc data (nemělo by nastat, ale
                # může, není to v moci Získej zaručit, že v CPK nenastala
                # situaci, kdy zadala jako doc_alt_ids doc_id, které
                # neexistuje)
                logger.warning('Missing data for {}'.format(doc_id))
                continue

            # knihovna a její kontrola - ale jen poprvé, což poznáme podle
            # force_new_availability
            sigla = doc_data['sigla'].lower()
            if force_new_availability:

                # 1) je-li ŽK, přeskočit
                if sigla == ticket.library_zk:
                    self._docs_data['data'][doc_id]['lb_used'] = 'forbidden'
                    self._docs_data['data'][doc_id]['lb_used_reason'] = 'zk'
                    if DEBUG or DEBUG_RATING:
                        print('1) zk, skip', sigla)
                    continue

                # 2) neexistuje-li v Získej, přeskočit
                library = self._get_library(sigla)
                if not library:
                    self._docs_data['data'][doc_id]['lb_used'] = 'forbidden'
                    self._docs_data['data'][doc_id]['lb_used_reason'] = 'missing_library'
                    if DEBUG or DEBUG_RATING:
                        print('2) missing_library, skip', sigla)
                    continue

                # 3) je-li neaktivní v Získej, přeskočit
                if not library.is_active or not library.Is_mvs_dk():
                    self._docs_data['data'][doc_id]['lb_used'] = 'forbidden'
                    self._docs_data['data'][doc_id]['lb_used_reason'] = 'not_active_library'
                    if DEBUG or DEBUG_RATING:
                        print('3) not_active_library, skip', sigla)
                    continue

                # 4) je-li nastavená restrikce na jeden či více doc_id, respektujeme
                #    ji, přeskočit
                if restrict_doc_ids and doc_id not in restrict_doc_ids:
                    self._docs_data['data'][doc_id]['lb_used'] = 'forbidden'
                    self._docs_data['data'][doc_id]['lb_used_reason'] = 'restricted_doc_ids'
                    if DEBUG or DEBUG_RATING:
                        print('4) restricted_doc_ids, skip', sigla)
                    continue

                # 5) je-li doc_id použito už dříve a to způsobem, který vylučuje
                #    znovupoužití (to se nastavuje během ukončování subticketu),
                #    přeskočit
                if self._history_blacklist and doc_id in self._history_blacklist:
                    self._docs_data['data'][doc_id]['lb_used'] = 'forbidden'
                    self._docs_data['data'][doc_id]['lb_used_reason'] = 'history_blacklist'
                    if DEBUG or DEBUG_RATING:
                        print('5) history_blacklist, skip', sigla)
                    continue

            # je-li kniohovna na blacklistu ŽK, odložit a případně použít až
            # když se nic jiného nenajde
            # if sigla in self.blacklist and not try_anything:
            #     self._docs_data['data'][doc_id]['lb_used'] = 'postponed'
            #     self._docs_data['data'][doc_id]['lb_used_reason'] = 'library_blacklist'
            #     if DEBUG or DEBUG_RATING:
            #         print('6) library_blacklist, skip', sigla)
            #     continue

            # existuje datová struktura pro zaznamenání právě probíhajících
            # async požadavků přes ziskej-async na CPK API?  pokud ne, vytvořit
            if 'async_ids' not in self._docs_data['data'][doc_id] or \
                    force_new_availability:
                self._docs_data['data'][doc_id]['async_ids'] = dict()

            # existuje datová struktura pro zaznamenání výsledků async
            # požadavků přes ziskej-async na CPK API?  pokud ne, vytvořit
            if 'best_availability' not in self._docs_data['data'][doc_id] or \
                    force_new_availability:
                self._docs_data['data'][doc_id]['best_availability'] = 'unknown'
            if 'unit_availability' not in self._docs_data['data'][doc_id] or \
                    force_new_availability:
                self._docs_data['data'][doc_id]['unit_availability'] = dict()

            # available finished total - pro ajax
            self.aft['total'] += 1

            # je pro tento dokument nějaký async dotaz na dostupnost některé
            # z jednotek (jedné či více)?
            if self._docs_data['data'][doc_id]['async_ids']:

                if DEBUG:
                    print('async_ids not empty')

                # zkontrolovat výsledek (resp. všechny)
                unit_ids = self._docs_data['data'][doc_id]['async_ids'].keys()
                for unit_id in unit_ids:
                    async_id = self._docs_data['data'][doc_id]['async_ids'][unit_id]

                    if DEBUG:
                        print('unit_id: ', unit_id, ' async_id: ', async_id)

                    time0 = get_time0()
                    try:
                        availability_json = cpkunitasync.call_retrieve(async_id)
                    except CpkApiException2:
                        availability_json = u'Error'

                    # pokud je výsledek chyba, smazat tento dotaz z aktivních
                    # a poznamenat, že se už na tuto jednotku neptat
                    if availability_json == u'Error':
                        del self._docs_data['data'][doc_id]['async_ids'][unit_id]
                        self._docs_data['data'][doc_id]['unit_availability'][unit_id] = 'error'
                        if DEBUG:
                            print('error')
                        continue  # next unit_id

                    # pokud zatím neskončilo a není náš timeout, jít dál
                    # a s ohlededm na zvolenou Auto LB strategii max jednoho
                    # aktivního požadavku na dokument jít na další dokument
                    if availability_json is None:
                        # SUGGESTION implementovat rozumný timeout
                        if DEBUG:
                            print('None')
                        continue  # next unit_id

                    # pokud se už podaří získat výsledek, zaznamenat jej
                    availability = self._parse_availability(availability_json)
                    available_lov = availability['available_lov']
                    # dict(
                    #     available = available_bool,
                    #     available_lov = available_lov,
                    #     # duedate = duedate,
                    #     # rating = self.par[available_lov],
                    #     )
                    del self._docs_data['data'][doc_id]['async_ids'][unit_id]
                    self._docs_data['data'][doc_id]['unit_availability'][unit_id] = available_lov
                    best = self._docs_data['data'][doc_id]['best_availability']
                    if available_lov == u'available':
                        self._docs_data['data'][doc_id]['best_availability'] = available_lov
                    # má smysl jen pro MVS, ošetřeno už při načítání dat
                    elif available_lov == u'available_stud':
                        if best not in (u'available'):
                            self._docs_data['data'][doc_id]['best_availability'] = available_lov
                    elif available_lov == u'unavailable':
                        if best not in (u'available', u'available_stud', ):
                            self._docs_data['data'][doc_id]['best_availability'] = available_lov
                    else:
                        raise Exception('Internal error')

                    if DEBUG:
                        print('available_lov: ', available_lov)

                    # a pokud je dostupný, tak s ohledem na zvolenou strategii
                    # AutoLB se už na jinou jednotku neptat

                    # pokud není dostupný, tak smazat tento dotaz z aktivních
                    # a zaznamenat neúspěch (nebo částečný úspěch typu jen do
                    # studovny) a pokračovat dalším dotazem na další jednotku, pokud
                    # nějaká ještě je, pokud není, jít dál

                    # obojí je implementováno níže - až se projdou všechny
                    # aktivní dotazy na CPK a žádný nezůstane aktivní, tak...

            if self._docs_data['data'][doc_id]['best_availability'] == 'available':
                self._docs_data['data'][doc_id]['lb_used'] = 'available'
                # available finished total - pro ajax
                self.aft['available'] += 1
                self.aft['finished'] += 1

            # pokud je stále nějaký aktivní nebo máme nějaký výsledek plnou
            # dostupnost, nevytváříme žádný nový požadavek a jdeme dál
            if self._docs_data['data'][doc_id]['async_ids'] or \
                    self._docs_data['data'][doc_id]['best_availability'] == 'available':
                if DEBUG:
                    print('existing async or is already available')
                continue

            # už máme výsledky pro všechny jednotky, nevytváříme žádný nový
            # požadavek a jdeme dál
            unit_id = None
            for unit_id_ in self._docs_data['data'][doc_id]['unit_ids']:
                if unit_id_ in self._docs_data['data'][doc_id]['unit_availability']:
                    continue
                unit_id = unit_id_
                break
            if not unit_id:
                if DEBUG:
                    print('all unit_ids have result')
                self._docs_data['data'][doc_id]['lb_used'] = 'unavailable'
                # available finished total - pro ajax
                self.aft['finished'] += 1
                continue  # next doc_id

            # vytvořit nový async dotaz pro první unit_id bez výsledku (error je
            # výsledek)
            time0 = get_time0()
            async_id = cpkunitasync.call_init(unit_id)
            self._docs_data['data'][doc_id]['async_ids'][unit_id] = async_id
            self._docs_data['data'][doc_id]['lb_used'] = 'async_in_progress'
            self.lb_ts(u'cpkunitasync.call_init', time0=time0)
            if DEBUG:
                print('new async for unit_id: ', unit_id, ' async_id: ', async_id)
            continue  # next doc_id

        if self._state and not force_new_availability:
            if DEBUG:
                print('we have some results even if not full (auto lb is not finished)')
            self._set_dk_list(ticket)
            self._set_suggested(ticket)
        else:
            if DEBUG:
                print('no results of auto lb at all - ajax init')
            self.dk_list = []
            self._set_suggested(ticket, [])
        self._save_ticket(ticket)

        return

    def _get_rating_desc(self, rating_desc_ids, library):
        if DEBUG:
            print('-- LoadBalancing _get_rating_desc')
        value_list = []
        LABELS = dict(
            available = u'Dokument je dostupný',
            # má smysl jen pro MVS, ošetřeno už při načítání dat
            available_stud = u'Dokument je dostupný jen do studovny',
            unavailable = u'Dokument není dostupný',
            unknown = u'Dostupnost dokumentu nebyla zjištěna',
            lib_zk = u'jedná se o žádající knihovnu',
            lib_caslin = u'informace jsou ze souborného katalogu',
            lib_not_cpk = u'knihovna není v CPK',
            lib_blacklist = u'knihovna je v nepreferovaných',
            lib_not_ziskej = u'knihovna není aktivní v ZÍSKEJ nebo nemá aktivovanou službu jako DK',
            lib_whitelist = u'knihovna je v preferovaných',
            #lib_open_days = u'knihovna má omezenou provozní dobu',
            lib_vacation = u'knihovna má provozní dobu nebo dovolenou, která zabraňuje přijetí požadavku do 24 h',
            lib_vacation_near = u'knihovna má provozní dobu nebo dovolenou, která může ztížit dodání',
            # lib = u'???',
            )
        for rating_desc_id in rating_desc_ids:
            # if rating_desc_id == 'lib_open_days' and \
            #         'lib_vacation' in rating_desc_ids:
            #     continue
            value_item = LABELS.get(rating_desc_id, rating_desc_id)
            if rating_desc_id in ('lib_vacation', 'lib_vacation_near') and \
                    library:
                vacation_info = library.Vacation_info()
                if vacation_info:
                    value_item += u' ({})'.format(vacation_info)
                # else:
                #     value_item = u'knihovna má provozní dobu, která zabraňuje přijetí požadavku do 24 h'
            value_list.append(value_item)
        value = u', '.join(value_list)
        return value

    def _set_dk_list(self, ticket, restrict_doc_ids=None):
        if DEBUG:
            print('-- LoadBalancing _set_dk_list')

        # self.suggested_data = dict(
        #     ids = [],
        #     data = dict(),
        #     )

        self.dk_list = []
        fullname = self.fullname

        if DEBUG:
            print("whitelist: ", self.whitelist)
            print("blacklist: ", self.blacklist)

        # pro všechny dokumenty, které CPK považuje za záměnné
        for doc_id in self.doc_ids:

            # V některých případech (např. vytváření požadavku při schválení
            # objednávky po načtení dostupnosti a uložení pořadí) je
            # restrict_doc_ids = [doc_id, ] - v tom případě nás ostatní
            # nezajímají.
            if restrict_doc_ids and doc_id not in restrict_doc_ids:
                continue

            #načíst dříve zjištěná data (zatím jsme offline)
            doc_data = self._docs_data['data'].get(doc_id, None)
            if not doc_data:
                continue

            is_caslin = False
            sigla = doc_data['sigla']  # např. ABA013
            if sigla:
                library_id = sigla.lower()  # např. aba013
            else:  # zejména None - nemá attribut lower
                library_id = u''
            library = self._get_library(library_id)
            library_fullname = u'Neznámá knihovna ({})'.format(sigla)
            if library:
                library_fullname = library.fullname
            elif doc_id.startswith('caslin'):
                is_caslin = True
                library_fullname = u'Souborný katalog'

            # rating - kompletní výpočet
            best_id = doc_data.get('best_availability', u'unknown')
            rating_desc_ids = [best_id]
            rating = self.par[best_id]
            if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                print("   base by best", sigla, rating)

            # Souborný katalog - jen info, nelze půjčovat
            if is_caslin:
                rating += self.par['lib_caslin']
                rating_desc_ids.append('lib_caslin')
                if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                    print("   lib_caslin", sigla, rating_desc_ids[-1], self.par['lib_caslin'], rating)

            # Chybí knihovna
            elif library is None:
                rating += self.par['lib_not_cpk']
                rating_desc_ids.append('lib_not_cpk')
                if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                    print("   lib_not_cpk", sigla, rating_desc_ids[-1], self.par['lib_not_cpk'], doc_id, rating)
                logger.warning('AutoLB lib_not_cpk for {}'.format(doc_id))

            # Knihovna je ŽK
            elif sigla.lower() == ticket.library_zk.lower():
                rating += self.par['lib_zk']
                rating_desc_ids.append('lib_zk')
                if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                    print("   lib_zk", sigla, rating_desc_ids[-1], self.par['lib_zk'], rating)

            # Knhovna není aktivní jako MVS DK pro MVS
            elif self.service == 'mvs' and not library.Is_mvs_dk():
                rating += self.par['lib_not_ziskej']
                rating_desc_ids.append('lib_not_ziskej')
                if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                    print("   lib_not_ziskej", sigla, rating_desc_ids[-1], self.par['lib_not_ziskej'], rating)

            # Knhovna není aktivní jako EDD DK pro EDD
            elif self.service == 'edd' and not library.Is_edd_dk():
                rating += self.par['lib_not_ziskej']
                rating_desc_ids.append('lib_not_ziskej')
                if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                    print("   lib_not_ziskej", sigla, rating_desc_ids[-1], self.par['lib_not_ziskej'], rating)

            # Ostatní případy mají společné prvky, proto "else"
            else:

                # Penalizace za provozní dobu, rozdíl oproti dovolené je, že se
                # velikost penalizace zjištuje ze ziskej parametru ne z AutoLB
                # param.
                # open_days_penalty = library._get_open_days_coef(
                #     now=self.testing_now_dt)
                # if open_days_penalty > 0.1:
                #     rating += open_days_penalty
                #     rating_desc_ids.append('lib_open_days')
                #     if DEBUG or DEBUG_RATING or DEBUG_RATING_DK or \
                #             DEBUG_RATING_VAC:
                #         print("   lib_open_days", sigla, rating_desc_ids[-1], open_days_penalty)

                # get_vacation_coef() vraci číslo mezi 0 a 1
                reader_date = ticket.reader_date
                lb_coef = library._get_lb_coef(
                    reader_date_iso=reader_date, now=self.testing_now_dt)
                if lb_coef > 0.0:
                    rating += self.par['load_balancing_holiday_penalty']*lb_coef
                    if lb_coef > 0.99:
                        rating_desc_ids.append('lib_vacation')
                    else:
                        rating_desc_ids.append('lib_vacation_near')
                    rating_desc_ids_last = rating_desc_ids[-1]
                else:
                    rating_desc_ids_last = 'no_vacation'
                if DEBUG or DEBUG_RATING or DEBUG_RATING_DK or DEBUG_RATING_VAC:
                    print("   lb_coef", sigla, rating_desc_ids_last, lb_coef, self.par['load_balancing_holiday_penalty']*lb_coef, rating)

                # Blacklist a Whitelist
                if library_id in self.blacklist:  # aba013
                    rating += self.par['lib_blacklist']
                    rating_desc_ids.append('lib_blacklist')
                    if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                        print("   lib_blacklist", sigla, rating_desc_ids[-1], self.par['lib_blacklist'], rating)
                elif library_id in self.whitelist:  # aba013
                    rating += self.par['lib_whitelist']
                    rating_desc_ids.append('lib_whitelist')
                    if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                        print("   lib_whitelist", sigla, rating_desc_ids[-1], self.par['lib_whitelist'], rating)
                else:
                    rating += self.par['lib']
                    #rating_desc_ids.append('lib')
                    if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                        print("   lib_else", sigla, 'lib', self.par['lib'], rating)

            rating_desc = self._get_rating_desc(rating_desc_ids, library)
            if RATING_DESC_DEBUG:
                if rating >= 1000.0:
                    rating_desc_ok = u'X'
                else:
                    rating_desc_ok = u'OK'
                rating_desc += u' (penalizace <strong>{} {}</strong>)'.format(
                    int(round(rating, 0)),
                    rating_desc_ok,
                    )
            if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                print("   rating_desc:", unicode.encode(rating_desc, 'utf-8', 'replace'))

            if sigla:
                label = u'{fullname} ({sigla})'.format(fullname=library_fullname, sigla=sigla)
            else:
                label = u'{fullname}'.format(fullname=library_fullname)

            if DEBUG or DEBUG_RATING or DEBUG_RATING_DK:
                print("   sigla/rating: {} {}".format(sigla, rating))
                print("   ------------  ------")
                print(" ")

            self.dk_list.append(
                dict(
                    id = doc_data['doc_id'],  # used
                    label = label,  # used
                    title = fullname,  # used but useless
                    fullname = fullname,  # used but useless
                    sigla = sigla,  # used
                    rating = rating,  # used
                    rating_desc = rating_desc,  # used
                    # units = doc_data['unit_ids'],
                    # doc_data = doc_data,
                    # library = library,
                    # library_unknown = library_unknown,
                    # library_active = library_active,
                    # availability_unit_id = availability_unit_id,
                    # availability_json = availability_json,  # jen pro ladění
                    )
                )

        # sort
        if len(self.dk_list) > 1:
            self.dk_list = sorted(self.dk_list, key=lambda item: item['rating'])

        if DEBUG:
            print('self.dk_list')
            pprint(self.dk_list)

    def _get_filtered_dk_list(self, dk_list=None, restrict_doc_ids=None):
        if DEBUG:
            print('-- LoadBalancing _get_filtered_dk_list')

        if dk_list is None:
            dk_list = self.dk_list
        if dk_list is None:
            dk_list = []

        LIMIT = 0  # 3

        suggested = []
        dk_list_filtered = []
        for dk_item in dk_list:
            doc_id = dk_item['id']
            if restrict_doc_ids and doc_id not in restrict_doc_ids:
                continue
            if LIMIT and len(suggested) >= LIMIT:
                break
            if dk_item['rating'] > self.par['limit']:
                break  # je to seřazené podle limit
            suggested.append(doc_id)
            dk_list_filtered.append(dk_item)

        return suggested, dk_list_filtered

    def _set_suggested(self, ticket, dk_list=None):
        if DEBUG:
            print('-- LoadBalancing _set_suggested')

        if dk_list is None:
            dk_list = self.dk_list
        if dk_list is None:
            dk_list = []

        suggested, dk_list_filtered = self._get_filtered_dk_list(dk_list=dk_list)

        ticket.doc_order = u''
        ticket.doc_order_suggested = u','.join(suggested)
        ticket.doc_order_suggested_data = json.dumps(dk_list, ensure_ascii=False)

        if DEBUG or DEBUG_RATING:
            print('dk_list')
            pprint(dk_list)
            print('ticket.doc_order_suggested')
            print(ticket.doc_order_suggested)
            print('ticket.doc_order_suggested_data')
            print(ticket.doc_order_suggested_data)

        return dk_list_filtered

    def availability_init(self, ticket):
        if DEBUG:
            print('-- LoadBalancing availability_init')
        storage_reset(ticket)
        self.aft = dict(available=0, finished=0, total=0)
        self._load_balancing_alg(ticket, check_availability=True,
            force_new_availability=True)
        #self._set_suggested(ticket, [])
        if DEBUG:
            pprint(self.aft)
        return self.aft

    def availability_retrieve(self, ticket):
        if DEBUG:
            print('-- LoadBalancing availability_retrieve')
        self.aft = dict(available=0, finished=0, total=0)
        self._load_balancing_alg(ticket, check_availability=True)
        #self._set_suggested(ticket)
        if DEBUG:
            pprint(self.aft)
        return self.aft

    def get_dk_list(self, ticket, restrict_doc_ids=None):
        """Příprava dat pro pořadí DK."""
        if DEBUG:
            print('-- LoadBalancing get_dk_list')
        self._load_ticket(ticket)
        self._setup_whitelist_blacklist(ticket.library_zk)
        self._set_dk_list(ticket, restrict_doc_ids=restrict_doc_ids)
        suggested, dk_list_filtered = self._get_filtered_dk_list(dk_list=None,
            restrict_doc_ids=None)
        return dk_list_filtered


# gateways

# voláno z ticket_create
def get_doc_title(portal, doc_id, ignore_missing_unit_ids=False):
    if DEBUG:
        print('-- get_doc_title()')
    lb = LoadBalancingBase(portal)
    lb.ignore_missing_unit_ids = ignore_missing_unit_ids
    doc_title = lb.get_doc_title(doc_id)
    return doc_title

# voláno z ticket_create, dvakrát
def parse_cpk_post(portal,
                   cpk_post,
                   library_zk_sigla=None,
                   include_fee_est=False,
                   include_doc_data=False,
                   ignore_missing_unit_ids=False):
    if DEBUG:
        print('-- parse_cpk_post()')
    lb = LoadBalancingBase(portal)
    lb.ignore_missing_unit_ids = ignore_missing_unit_ids
    lb_data = lb.parse_cpk_post(cpk_post,
                                library_zk_sigla=library_zk_sigla,
                                include_fee_est=include_fee_est,
                                include_doc_data=include_doc_data)
    return lb_data

# voláno z ticket při přípravě nebo zpracování formuláře - tam jsou potřeba
# všechny možnosti
def load_balancing_dk_list(portal, ticket, restrict_doc_ids=None):
    if DEBUG:
        print('-- load_balancing_dk_list(ticket={hid})'.format(hid=ticket.hid))
    lb = LoadBalancing(portal)
    dk_list = lb.get_dk_list(ticket, restrict_doc_ids=restrict_doc_ids)
    return dk_list

# voláno z ticket ajax init, vrací trojici a/f/t
def load_balancing_availability_init(portal, ticket):
    if DEBUG:
        print('-- load_balancing_availability_init(ticket={hid})'.format(hid=ticket.hid))
    lb = LoadBalancing(portal)
    aft = lb.availability_init(ticket)
    return aft

# voláno z ticket ajax get, vrací trojici a/f/t
def load_balancing_availability_retrieve(portal, ticket):
    if DEBUG:
        print('-- load_balancing_availability_retrieve(ticket={hid})'.format(hid=ticket.hid))
    lb = LoadBalancing(portal)
    aft = lb.availability_retrieve(ticket)
    return aft
