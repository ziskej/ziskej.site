# -*- coding: utf-8 -*-
"""Parametry systému, hlavní místo.  Část může být přesunuta do parametrů
systému editovatelných adminem, viz class System_Parameters.
"""

from __future__ import print_function

from datetime import datetime
import logging

from DateTime import DateTime
from plone import api

from ziskej.site import BUILDOUT_TYPE


logger = logging.getLogger("ziskej")

PARAMETERS = dict(
    t_unpaid_hours = 1,  # FIXME dočasně 1, později 24
    complaint_st_late_days = 6,
    st_queued_dk_wdays = 1,
    st_queued_dk_notification_step_h = 1,
    st_return_default_days = 28,
    # používáno i jako cond_a_return_d_value_max, protože musí být stejné
    st_return_min_days = 28,
    reader_date_default = 21,
    cond_a_return_d_value_min = 5,
    notification_sc_contact = u'S pozdravem Servisní centrum ZÍSKEJ\n\nTel.: 232 002 777\nMail: ziskej@techlib.cz\n',
    notification_sc_contact_reader = u'Děkujeme za využití služby ZÍSKEJ, kterou provozuje Národní technická knihovna.\n\nServisní centrum ZÍSKEJ můžete kontaktovat na ziskej@techlib.cz.\n',
    enabled_show_library_credit_general = True,
    enabled_show_library_credit_ntk = True,
    enabled_show_library_credit_ziskej = True,
    # AutoLB: DK má dovolenou
    load_balancing_holiday_penalty = 900,
    # jak dlouho před dovolenou nesmí čtenář objednat
    mvs_zk_deactivate_before_holiday = 1,
    # za jak dlouho může čtenář objednat
    reader_cannot_order = 3,
    mvs_dk_lb_penalty_day_before_holiday = 3,
    # DateTime tudíž 1 = den a 0,0416 rovná se 1h
    open_today_min_days = 0.0416,
    open_today_default = "18:00",
    near_open_days_count_float = 3.0,
    rating_near_open_days = 100,
    nearest_open_day_count = 5,
    rating_nearest_open_day = 50,
    # EDD: expirace pro čtenáře
    edd_expire_days = 21,
    # EDD: jak dlouho reklamovatelné, počet dnů po expiraci
    edd_complaint_days = 31,
    # EDD: smazání ze systému později kvůli reklamacím, počet dnů po expiraci
    edd_delete_days = 35,
    )

PARAMETER_NAMES = PARAMETERS.keys()

HOME_PARAMETER_NAMES = (
    'emergency_header',
    # 'mvs_ticket_hid_prefix',
    # 'mvs_subticket_hid_prefix',
    'testing_now_str',
    'edd_selection_label',
    )

ONLY_TESTING = ('testing_now_str', )

DEBUG = False


def ziskej_parameter(name, portal=None):
    # Některé parametry nejsou mimo testing povoleny.
    if name in ONLY_TESTING:
        if BUILDOUT_TYPE not in ('development', 'dev', 'test', ):
            if DEBUG:
                print("ziskej_parameter", name, repr(value),
                      "due to ONLY_TESTING")
            return None

    # Zpřístupnit touto cestou editovatelné parametry.
    if name in HOME_PARAMETER_NAMES:
        if portal is None:
            portal = api.portal.get()
        try:
            home = portal.home
        except:
            if DEBUG:
                print("ziskej_parameter", name, repr(value), "due to no home")
            return None
        value = home.get_parameters(name)
        # Konvertovat datum a čas string 6.11.2020 11:10 do DateTime
        if name in ('testing_now_str', ):
            if not value:
                if DEBUG:
                    print("ziskej_parameter", name, "None due to", repr(value))
                return None
            try:
                value_pydt = datetime.strptime(value, '%d.%m.%Y %H:%M')
                value_iso = value_pydt.isoformat()
                value_dt = DateTime(value_iso)
                if DEBUG:
                    print("ziskej_parameter", name, repr(value_dt))
                return value_dt
            except:
                logger.info('Ziskej parameter: Ignored invalid DateTime for value: {value}'.format(value=repr(value)))
                return None
        if DEBUG:
            print("ziskej_parameter", name, repr(value))
        return value

    if name not in PARAMETER_NAMES:
        logger.warning('Uknown parameter: {}'.format(str(name)))
        raise Exception('Uknown parameter')
    if name == 'example':
        value = u'find a value somehow'
    elif name in PARAMETERS:
        value = PARAMETERS[name]
    else:
        logger.warning('Undefined parameter: {}'.format(str(name)))
        raise Exception('Undefined parameter')
    if DEBUG:
        print("ziskej_parameter", name, repr(value))
    return value
