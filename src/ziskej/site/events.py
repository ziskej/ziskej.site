# -*- coding: utf-8 -*-
"""Zope event handlers.
"""

from DateTime import DateTime


# Not used
def ticket_wf_transition_event_cmf(context, event):
    """ITicket transaction success.

    Products.CMFCore.interfaces.IActionSucceededEvent

    This is a higher level event that is more commonly used to react after
    a workflow action has completed.

    https://docs.plone.org/develop/addons/components/events.html#workflow-events
    """

    print "ticket_wf_transition_event_cmf"

    #print context
    #print event

    # ['__class__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', '__hash__', '__implemented__', '__init__', '__module__', '__new__', '__providedBy__', '__provides__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'action', 'object', 'result', 'workflow']
    #print dir(event)

    # request
    #print event.action

    # <TicketMVS at 6c24fb166f0d4694>
    #print event.object

    # None
    #print event.result

    # <DCWorkflowDefinition at ticketmvs_workflow>
    #print event.workflow

def ticket_wf_transition_event_dc(context, event):
    """ITicket transaction success.

    Products.DCWorkflow.interfaces.IAfterTransitionEvent

    This is triggered after a workflow transition has been executed.

    The DCWorkflow events are low-level events that can tell you a lot about
    the previous and current states.

    https://docs.plone.org/develop/addons/components/events.html#workflow-events
    """

    pass
    
    #now = DateTime()

    #print "ticket_wf_transition_event_dc"

    #print context
    #print event

    # ['__class__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', '__hash__', '__implemented__', '__init__', '__module__', '__new__', '__providedBy__', '__provides__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'kwargs', 'new_state', 'object', 'old_state', 'status', 'transition', 'workflow']
    #print dir(event)

    # {'comment': ''}
    #print event.kwargs

    # <StateDefinition at requested>
    #print event.new_state

    # <StateDefinition at requested>
    #print str(event.new_state)

    # ['COPY', 'COPY__roles__', 'DELETE', 'DELETE__roles__', 'HEAD', 'HEAD__roles__', 'LOCK', 'LOCK__roles__', 'MKCOL', 'MOVE', 'MOVE__roles__', 'OPTIONS', 'OPTIONS__roles__', 'PROPFIND', 'PROPFIND__roles__', 'PROPPATCH', 'PROPPATCH__roles__', 'PUT', 'REQUEST', 'TRACE', 'TRACE__roles__', 'UNLOCK', 'UNLOCK__roles__', '__ac_local_roles__', '__ac_permissions__', '__ac_roles__', '__allow_access_to_unprotected_subobjects__', '__class__', '__class_init__', '__dav_resource__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', '__getstate__', '__hash__', '__http_methods__', '__implemented__', '__init__', '__len__', '__module__', '__name__', '__new__', '__of__', '__propsets__', '__providedBy__', '__provides__', '__reduce__', '__reduce_ex__', '__repr__', '__roles__', '__setattr__', '__setstate__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_addRole', '_canCopy', '_cleanupCopy', '_delRoles', '_deleteOwnershipAfterAdd', '_getCopy', '_get_request_var_or_attr', '_has_user_defined_role', '_isBeingUsedAsAMethod', '_manage_editedDialog', '_normal_manage_access', '_notifyOfCopyTo', '_old_wl_lockmapping', '_p_activate', '_p_changed', '_p_deactivate', '_p_delattr', '_p_estimated_size', '_p_getattr', '_p_invalidate', '_p_jar', '_p_mtime', '_p_oid', '_p_serial', '_p_setattr', '_p_state', '_permissions_form', '_postCopy', '_properties_form', '_setId', '_setRoles', '_variables_form', 'absolute_url', 'absolute_url__roles__', 'absolute_url_path', 'absolute_url_path__roles__', 'ac_inherited_permissions', 'ac_inherited_permissions__roles__', 'access_debug_info', 'access_debug_info__roles__', 'aclAChecked', 'aclEChecked', 'aclPChecked', 'acquiredRolesAreUsedBy', 'acquiredRolesAreUsedBy__roles__', 'addVariable', 'bobobase_modification_time', 'cb_isCopyable', 'cb_isMoveable', 'cb_userHasCopyOrMovePermission', 'changeOwnership', 'changeOwnership__roles__', 'dav__init', 'dav__simpleifhandler', 'dav__validate', 'deleteVariables', 'description', 'filtered_manage_options', 'filtered_manage_options__roles__', 'getAttribute', 'getAttributeNode', 'getAttributeNode__roles__', 'getAttribute__roles__', 'getAttributes', 'getAttributes__roles__', 'getAvailableRoles', 'getAvailableTransitionIds', 'getAvailableVarIds', 'getChildNodes', 'getChildNodes__roles__', 'getElementsByTagName', 'getElementsByTagName__roles__', 'getFirstChild', 'getFirstChild__roles__', 'getGroupInfo', 'getId', 'getId__roles__', 'getLastChild', 'getLastChild__roles__', 'getManagedPermissions', 'getNextSibling', 'getNextSibling__roles__', 'getNodeName', 'getNodeName__roles__', 'getNodeType', 'getNodeValue', 'getNodeValue__roles__', 'getOwner', 'getOwnerDocument', 'getOwnerDocument__roles__', 'getOwnerTuple', 'getOwnerTuple__roles__', 'getOwner__roles__', 'getParentNode', 'getParentNode__roles__', 'getPermissionInfo', 'getPhysicalPath', 'getPhysicalPath__roles__', 'getPhysicalRoot', 'getPhysicalRoot__roles__', 'getPreviousSibling', 'getPreviousSibling__roles__', 'getTagName', 'getTagName__roles__', 'getTransitionTitle', 'getTransitions', 'getVariableValues', 'getWorkflow', 'getWorkflowVariables', 'getWrappedOwner', 'getWrappedOwner__roles__', 'get_local_roles', 'get_local_roles_for_userid', 'get_valid_userids', 'group_roles', 'hasChildNodes', 'hasChildNodes__roles__', 'has_local_roles', 'http__etag', 'http__parseMatchList', 'http__processMatchHeaders', 'http__refreshEtag', 'icon', 'id', 'isPrincipiaFolderish', 'isTopLevelPrincipiaApplicationObject', 'listDAVObjects', 'listDAVObjects__roles__', 'manage', 'manage_DAVget', 'manage_DAVget__roles__', 'manage_FTPlist', 'manage_FTPlist__roles__', 'manage_FTPstat', 'manage_FTPstat__roles__', 'manage_UndoForm', 'manage_UndoForm__roles__', 'manage__roles__', 'manage_access', 'manage_access__roles__', 'manage_acquiredForm', 'manage_acquiredForm__roles__', 'manage_acquiredPermissions', 'manage_acquiredPermissions__roles__', 'manage_addLocalRoles', 'manage_addLocalRoles__roles__', 'manage_afterAdd', 'manage_afterAdd__roles__', 'manage_afterClone', 'manage_afterClone__roles__', 'manage_beforeDelete', 'manage_beforeDelete__roles__', 'manage_changeOwnershipType', 'manage_changeOwnershipType__roles__', 'manage_changePermissions', 'manage_changePermissions__roles__', 'manage_defined_roles', 'manage_defined_roles__roles__', 'manage_delLocalRoles', 'manage_delLocalRoles__roles__', 'manage_editLocalRoles', 'manage_editLocalRoles__roles__', 'manage_editRoles', 'manage_editRoles__roles__', 'manage_editedDialog', 'manage_editedDialog__roles__', 'manage_fixupOwnershipAfterAdd', 'manage_fixupOwnershipAfterAdd__roles__', 'manage_getPermissionMapping', 'manage_getPermissionMapping__roles__', 'manage_getUserRolesAndPermissions', 'manage_getUserRolesAndPermissions__roles__', 'manage_groups', 'manage_groups__roles__', 'manage_listLocalRoles', 'manage_listLocalRoles__roles__', 'manage_options', 'manage_owner', 'manage_owner__roles__', 'manage_permission', 'manage_permissionForm', 'manage_permissionForm__roles__', 'manage_permission__roles__', 'manage_permissions', 'manage_permissions__roles__', 'manage_properties', 'manage_properties__roles__', 'manage_reportUserPermissions', 'manage_reportUserPermissions__roles__', 'manage_role', 'manage_roleForm', 'manage_roleForm__roles__', 'manage_role__roles__', 'manage_setLocalRoles', 'manage_setLocalRoles__roles__', 'manage_setPermissionMapping', 'manage_setPermissionMapping__roles__', 'manage_tabs', 'manage_tabs__roles__', 'manage_takeOwnership', 'manage_takeOwnership__roles__', 'manage_undo_transactions', 'manage_undo_transactions__roles__', 'manage_variables', 'manage_variables__roles__', 'manage_workspace', 'manage_workspace__roles__', 'meta_type', 'objectIds', 'objectItems', 'objectValues', 'owner_info', 'owner_info__roles__', 'permissionMappingPossibleValues', 'permission_roles', 'permission_settings', 'permission_settings__roles__', 'permissionsOfRole', 'permissionsOfRole__roles__', 'possible_permissions', 'raise_standardErrorMessage', 'restrictedTraverse', 'restrictedTraverse__roles__', 'rolesOfPermission', 'rolesOfPermission__roles__', 'selectedRoles', 'setGroups', 'setPermission', 'setPermissions', 'setProperties', 'setVariables', 'smallRolesWidget', 'tabs_path_default', 'tabs_path_info', 'this', 'title', 'title_and_id', 'title_or_id', 'tpURL', 'tpValues', 'transitions', 'undoable_transactions', 'undoable_transactions__roles__', 'unrestrictedTraverse', 'unrestrictedTraverse__roles__', 'userCanTakeOwnership', 'userdefined_roles', 'userdefined_roles__roles__', 'users_with_local_role', 'validRoles', 'valid_roles', 'validate_roles', 'var_values', 'virtual_url_path', 'virtual_url_path__roles__', 'wl_clearLocks', 'wl_clearLocks__roles__', 'wl_delLock', 'wl_delLock__roles__', 'wl_getLock', 'wl_getLock__roles__', 'wl_hasLock', 'wl_isLocked', 'wl_isLockedByUser__roles__', 'wl_isLocked__roles__', 'wl_lockItems', 'wl_lockItems__roles__', 'wl_lockTokens', 'wl_lockTokens__roles__', 'wl_lockValues', 'wl_lockValues__roles__', 'wl_lockmapping', 'wl_lockmapping__roles__', 'wl_setLock', 'wl_setLock__roles__']
    #print dir(event.new_state)

    # requested
    #print event.new_state.getId()

    # requested
    #print event.new_state.id

    # <TicketMVS at 6c24fb166f0d4694>
    #print event.object

    # <StateDefinition at pending>
    #print event.old_state

    # {'action': 'request', 'review_state': 'requested', 'comments': '', 'actor': 'admin', 'time': DateTime('2017/10/28 14:27:25.996773 GMT+2')}
    #print event.status

    # 2017/10/28 14:49:37.748612 GMT+2
    #print event.status.get('time', now)

    # <TransitionDefinition at request>
    #print event.transition

    # <DCWorkflowDefinition at ticketmvs_workflow>
    #print event.workflow
