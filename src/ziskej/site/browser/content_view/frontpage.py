# -*- coding: utf-8 -*-
"""BrowserView: Frontpage.
"""

import logging

from DateTime import DateTime
from plone import api
from plone.dexterity.utils import createContent
from plone.protect.utils import addTokenToUrl
from plone.protect.interfaces import IDisableCSRFProtection
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.interface import alsoProvides

from ziskej.site.auth.auth_base import create_librarian
from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.library_payment_helpers import (
    library_payment_mvs,
    paysys_clearing_admin,
    )
from ziskej.site.utils import address_encode
from ziskej.site.utils_lib import pprint
from ziskej.site.ziskej_hotfix import hotfix_all


logger = logging.getLogger("ziskej")

DEBUG = False


class FrontpageView(ZiskejBrowserView):

    template = ViewPageTemplateFile('frontpage.pt')

    def call(self):
        year = DateTime().year()
        self.tickets_chart_text = u'Objednávky v roce {}'.format(year)
        self.subtickets_chart_text = u'Požadavky v roce {}'.format(year)

        self.ctenar_login_url = '{portal_url}/shibLogin?n=1'.format(portal_url=self.portal_url)
        self.knihovnik_login_url = '{portal_url}/login_form'.format(portal_url=self.portal_url)
        self.libraries_url = '{portal_url}/libraries'.format(portal_url=self.portal_url)
        if self.is_reader and self.request.get('howto', None) != u'r':
            url = self.portal_url + '/tickets/listing_reader'
            self.redirect(url)

