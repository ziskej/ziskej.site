# -*- coding: utf-8 -*-
"""BrowserView: Library.
"""

import json
import logging

from DateTime import DateTime
from plone import api
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.protect.utils import addTokenToUrl

from ziskej.site import APIAKS_ENABLED
#from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.browser.admin.admin_library import LibraryAdminViewBase
from ziskej.site.utils import address_encode, pprint
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger("ziskej")

DEBUG = False


class LibraryView(LibraryAdminViewBase):

    template = ViewPageTemplateFile('library.pt')

    def call(self):
        self.unauthorized = True
        if self.is_anon:
            self.redirect(self.portal_url)
            return
        self.unauthorized = False

        self.apiaks_enabled = APIAKS_ENABLED and \
            self.is_library_admin_or_operator
        if self.apiaks_enabled:
            self.apiaks_aks_token = self.context.apiaks_generate_token()

        enabled_show_library_credit_general = ziskej_parameter(
            'enabled_show_library_credit_general')
        enabled_show_library_credit_ntk = ziskej_parameter(
            'enabled_show_library_credit_ntk')
        enabled_show_library_credit_ziskej = ziskej_parameter(
            'enabled_show_library_credit_ziskej')

        self.is_panel_activate = self.is_ziskej_operator and \
            api.content.get_state(obj=self.context, default=None) == 'private'

        self.show_library_profile = self.is_ziskej_operator or \
            (self.is_librarian and self.library == self.context)
        self.show_edit_profile = self.is_ziskej_operator or \
            (self.is_library_admin and self.library == self.context)

        # Library credit: show
        self.show_library_credit_general = \
            enabled_show_library_credit_general and \
            (self.is_ziskej_operator or
                (self.is_librarian and self.library == self.context))
        self.show_library_credit_ntk = enabled_show_library_credit_ntk and \
            (self.is_ziskej_operator or
                (self.is_librarian and self.library == self.context))
        self.show_library_credit_ziskej = \
            enabled_show_library_credit_ziskej and \
            (self.is_ziskej_operator or
                (self.is_librarian and self.library == self.context))

        # NTK jako knihovna v Získej
        if self.context.sigla == 'ABA013':
            self.show_library_credit_ntk = False
            self.show_library_credit_ziskej = False
            self.show_library_credit_exception_for_ntk = True

        # Library credit: amount
        self.library_credit_ntk = u''
        if self.show_library_credit_general and self.show_library_credit_ntk:
            self.library_credit_ntk = u'?'

        if self.show_library_credit_ntk:
            credit_disponibilni = self.context.paysys_credit_info()
            if credit_disponibilni is not None:
                # credit_disponibilni = int(0)
                self.library_credit_ntk = u'{}'.format(credit_disponibilni)

        if self.context.mvs_zk_cena is None:
            self.feezk = u'var'
        elif self.context.mvs_zk_cena == 0:
            self.feezk = u'free'
        else:
            self.feezk = u'fix'

        if self.is_panel_activate:
            self.validation_result = self.context.validate_for_activation()
            if not self.validation_result:
                self.activate_url = addTokenToUrl(
                    self.context_url + \
                    '/content_status_modify?workflow_action=publish')

        self.address_encoded = address_encode(self.context.address)

        query = dict(
            path = '/'.join(self.context.getPhysicalPath()),
            portal_type = 'Branch',
            )
        brains = self.catalog(query)
        self.branches = []
        for brain in brains:
            address = brain.getObject().address
            self.branches.append(
                dict(
                    title = brain.Title,
                    address = brain.address,
                    address_encoded = address_encode(brain.address),
                    url = brain.getURL(),
                    )
                )

        # self.branches = [
        #     dict(title=u'Brno', address='Leninova 1, Brno', url="#"),
        #     dict(title=u'Praha', address='Stalinova 45, Praha', url="#"),
        #     ]
        # for item in self.branches:
        #     item['address_endoded'] = urllib.urlencode(
        #         dict(q=item['address']))

        self.add_librarian_url = addTokenToUrl(
            self.context_url + '/++add++Librarian')

        # Provozní doba a dovolená v profilu
        library = self.context
        DWS = (u'mon', u'tue', u'wed', u'thu', u'fri', u'sat', u'sun', )
        DW_LABEL = dict(
            mon=u'Pondělí',
            tue=u'Úterý',
            wed=u'Středa',
            thu=u'Čtvrtek',
            fri=u'Pátek',
            sat=u'Sobota',
            sun=u'Neděle',
            )
        self.provozni_doba_as_html = u''
        self.dovolena_as_html = u''

        opening_hours_full_json = library.opening_hours
        if opening_hours_full_json:
            opening_hours_full = json.loads(opening_hours_full_json)
            opening_hours = opening_hours_full['opening_hours']
            if DEBUG:
                pprint(opening_hours)
            for dws in DWS:
                txts = []
                for dw_idxs in ('0', '1', ):
                    oh_start = opening_hours[dws][dw_idxs]['0']
                    oh_end = opening_hours[dws][dw_idxs]['1']
                    if not (oh_start and oh_end):
                        continue
                    txts.append(u'{} – {}'.format(oh_start, oh_end))
                self.provozni_doba_as_html += u'{}: {}<br>'.format(
                    DW_LABEL[dws], u', '.join(txts), )
        if self.provozni_doba_as_html and \
                self.provozni_doba_as_html.endswith(u'<br>'):
            self.provozni_doba_as_html = self.provozni_doba_as_html[:-4]

        vacation_full_json = library.vacation
        if vacation_full_json:
            vacation_full = json.loads(vacation_full_json)

            if vacation_full['version'] == 1:
                vacation = vacation_full['vacation']
                if DEBUG:
                    pprint(vacation)
                if vacation != {}:
                    dovolena = []
                    for idx in ('0', '1', '2', ):
                        if idx not in vacation:
                            continue
                        vac_0 = vacation[idx]['0']
                        if vac_0:
                            vac_0 = self.dt_as(
                                DateTime(vac_0), result_type='string')
                        vac_1 = vacation[idx]['1']
                        if vac_1:
                            vac_1 = self.dt_as(
                                DateTime(vac_1), result_type='string')
                        if vac_0 and vac_1:
                            dovolena.append(u'{} – {}'.format(vac_0, vac_1))
                        elif vac_0:
                            dovolena.append(u'{}'.format(vac_0))
                    if dovolena:
                        self.dovolena_as_html = u'<br>'.join(dovolena)
