# -*- coding: utf-8 -*-
"""Library default view"""

import logging
import urllib

from plone import api
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.utils import safe_utf8
from ziskej.site.browser import ZiskejBrowserView


logger = logging.getLogger("ziskej")


class LibrarianView(ZiskejBrowserView):

    template = ViewPageTemplateFile('librarian.pt')

    def call(self):
        self.unauthorized = True
        library = self.context.__parent__

        if self.is_anon:
            self.redirect(self.portal_url)
            return
    

        self.unauthorized = False
        if self.context.is_library_admin:
            self.title = u'Profil správce knihovny: {}'.format(library.fullname)
        else:
            self.title = u'Profil knihovníka knihovny: {}'.format(
                library.fullname)


        self.description = u''
