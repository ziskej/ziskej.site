# -*- coding: utf-8 -*-
"""BrowserView: Reader.
"""

import logging
import urllib

from plone import api
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.utils import safe_utf8
from ziskej.site.browser import ZiskejBrowserView


logger = logging.getLogger("ziskej")


class ReaderView(ZiskejBrowserView):

    template = ViewPageTemplateFile('reader.pt')

    def call(self):
        self.unauthorized = True

        if self.is_reader:
            reader = self.ziskej_user_info['user']
            url = reader.absolute_url() + '/reader_update'
            self.redirect(url)
            return

        if not self.is_ziskej_operator:
            self.redirect(self.portal_url)
            return

        self.unauthorized = False

        self.title = u'Profil čtenáře: {fullname} ({user_id})'.format(user_id=self.context.username, fullname=self.context.fullname)
        self.description = u''
