# -*- coding: utf-8 -*-
"""BrowserView: Branch.
"""

import logging
import urllib

from plone import api
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.utils import safe_utf8


logger = logging.getLogger("ziskej")


class BranchView(BrowserView):

    template = ViewPageTemplateFile('branch.pt')

    def AddressEndoded(self):
        value = self.context.address
        if not value:
            return None
        value = safe_utf8(value)
        #value = safe_unicode(value)
        value_encoded = urllib.urlencode(dict(q=value))
        logger.info('{value} --> {value_encoded}'.format(value=value, value_encoded=value_encoded))
        self.address_endoded = value_encoded

    def call(self):
        self.AddressEndoded()
        self.parent = self.context.__parent__.absolute_url()

    def __call__(self):
        self.call()
        return self.template()
