# -*- coding: utf-8 -*-
"""BrowserView: Reader.
"""

import logging
import urllib
import json
#from datetime import datetime

#from Acquisition import aq_inner
from DateTime import DateTime
from plone import api
#from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.protect.utils import addTokenToUrl
from zope.interface import Invalid

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.utils import safe_utf8, safe_unicode
from ziskej.site.utils_lib import pprint
from ziskej.site.validators import validate_email, validate_idp_entityid
from ziskej.site.validators import validate_username, validate_unique_username


logger = logging.getLogger("ziskej")


class ReaderBaseView(ZiskejBrowserView):

    def _check_perm(self, message=None):
        if self.is_reader and self.ziskej_user_info['user'] == self.context:
            return True
        if not message:
            message = u'Pouze čtenář může upravit svůj profil na předchozí adrese.'
        self.add_message(message)
        self.redirect(url=self.context_url)
        return False


class UpdateView(ReaderBaseView):

    template = ViewPageTemplateFile('reader_update.pt')

    def call(self):
        super(UpdateView, self).call()

        self.unauthorized = True
        if self.ziskej_user_info.get('user_id', None) == 'admin':
            pass  # Umožnit uživateli admin vidět formulář.
        elif not self._check_perm():
            return
        self.unauthorized = False

        reader = self.context

        self.action_url = self.context_url + '/reader_update'
        self.errors = []

        if self.ziskej_user_info.get('user_id', None) == 'admin':
            pass  # Umožnit uživateli admin vidět formulář, ale jenom vidět.
        elif self.request.get('submit', None):
            changed = False

            email_user = self.text_from_request('email_user')
            if email_user:
                try:
                    validate_email(email_user)
                except Invalid, e:
                    self.errors.append(e)
                    email_user = None
            else:
                email_user = u''
            if email_user is not None and reader.email_user != email_user:
                reader.email_user = email_user
                reader.is_email_user_validated = False
                changed = True

            is_notification_enabled = self.bool_from_request('is_notification_enabled')
            if reader.is_notification_enabled != is_notification_enabled:
                reader.is_notification_enabled = is_notification_enabled
                changed = True

            dt_now = DateTime()

            gdpr_registration = self.bool_from_request('gdpr_registration')
            if reader.gdpr_registration != gdpr_registration:
                reader.gdpr_registration = gdpr_registration
                if gdpr_registration:
                    reader.gdpr_registration_date = dt_now
                else:
                    reader.gdpr_registration_date = None
                changed = True

            gdpr_data = self.bool_from_request('gdpr_data')
            if reader.gdpr_data != gdpr_data:
                reader.gdpr_data = gdpr_data
                if gdpr_data:
                    reader.gdpr_data_date = dt_now
                else:
                    reader.gdpr_data_date = None
                changed = True

            if changed:
                reader.reindexObject()
                self.add_message(u'Změny byly uloženy.')
            elif not len(self.errors):
                self.add_message(u'Nebyla zadána žádná změna.')

            if not reader.gdpr_registration or not reader.gdpr_data:
                self.add_message(u'Bez souhlasu s registrací a s uchovávním dat není možné systém ZÍSKEJ používat.')
                self.redirect(self.portal_url + '/logout')
                return
            if changed:
                self.redirect(self.action_url)
                return


class InitBaseView(ReaderBaseView):

    def call(self):
        super(InitBaseView, self).call()

        self.unauthorized = True
        if not self._check_perm():
            return
        self.unauthorized = False


class InitRegView(InitBaseView):

    template = ViewPageTemplateFile('reader_init_reg.pt')

    def call(self):
        super(InitRegView, self).call()

        if self.unauthorized:
            return

        reader = self.context

        self.action_url = self.context_url + '/reader_init_reg'

        if self.request.get('submit', None):
            changed = False

            dt_now = DateTime()

            gdpr_registration = self.request.get('gdpr_registration', False)
            if reader.gdpr_registration != gdpr_registration:
                reader.gdpr_registration = gdpr_registration
                if gdpr_registration:
                    reader.gdpr_registration_date = dt_now
                else:
                    reader.gdpr_registration_date = None
                changed = True

            if changed:
                reader.reindexObject()
            if reader.gdpr_registration and changed:
                self.add_message(u'Váš souhlas byl zaznamenán.')
            if reader.gdpr_registration:
                next_url = self.context_url + '/reader_init_data'
            else:
                self.add_message(u'Bez souhlasu s registrací není možné systém ZÍSKEJ používat.')
                next_url = self.portal_url + '/logout'
            self.redirect(next_url)
            return


class InitDataView(InitBaseView):

    template = ViewPageTemplateFile('reader_init_data.pt')

    def call(self):
        super(InitDataView, self).call()

        if self.unauthorized:
            return

        reader = self.context

        self.action_url = self.context_url + '/reader_init_data'

        if self.request.get('submit', None):
            changed = False

            dt_now = DateTime()

            gdpr_data = self.request.get('gdpr_data', False)
            if reader.gdpr_data != gdpr_data:
                reader.gdpr_data = gdpr_data
                if gdpr_data:
                    reader.gdpr_data_date = dt_now
                else:
                    reader.gdpr_data_date = None
                changed = True

            if changed:
                reader.reindexObject()
            if reader.gdpr_data and changed:
                self.add_message(u'Váš souhlas byl zaznamenán.')
            if reader.gdpr_data:
                if reader.cpkpost_json:
                    next_url = self.portal_url + '/ticket_create_summary'
                else:
                    next_url = self.context_url + '/reader_update'
            else:
                self.add_message(u'Bez souhlasu s uložením dat není možné systém ZÍSKEJ používat.')
                next_url = self.portal_url + '/logout'
            self.redirect(next_url)
            return
