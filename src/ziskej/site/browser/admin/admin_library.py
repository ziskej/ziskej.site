# -*- coding: utf-8 -*-
"""BrowserView: Administrace knihoven
"""

from __future__ import print_function

import json
import logging
import uuid

from DateTime import DateTime
from plone import api
from plone.protect.interfaces import IDisableCSRFProtection
from plone.protect.utils import addTokenToUrl
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.interface import Invalid, alsoProvides

from ziskej.site import (
    BUILDOUT_TYPE,
    APIAKS_ENABLED,
    )
from ziskej.site.auth.auth_base import (
    create_librarian,
    get_user_obj_by_user_id,
    )
from ziskej.site.browser.admin.admin import AdminViewBase, AdminException
from ziskej.site.library_payment_helpers import paysys_clearing_admin
from ziskej.site.utils import (
    delete_objects,
    pprint,
    safe_unicode,
    safe_utf8,
    )
from ziskej.site.validators import (
    validate_email,
    validate_idp_entityid,
    validate_phone,
    validate_unique_username,
    validate_username,
    )


logger = logging.getLogger("ziskej")

DEBUG = False
DEBUG_OPEN_FULL = False
DEBUG_OPEN_PRINTS = False


class LibraryAdminViewBase(AdminViewBase):

    def call(self):
        super(LibraryAdminViewBase, self).call()

    def _check_perm(self, lvl):
        try:
            if lvl == 'manager':
                if not self.is_manager:
                    raise AdminException('Unauthorized')
            elif lvl == 'admin':
                if not self.is_ziskej_admin:
                    raise AdminException('Unauthorized')
            elif lvl == 'operator':
                if not self.is_ziskej_operator:
                    raise AdminException('Unauthorized')
            elif lvl == 'library_admin':
                if self.is_ziskej_operator:
                    pass
                elif self.is_library_admin and self.library == self.context:
                    pass
                else:
                    raise AdminException('Unauthorized')
            else:
                raise AdminException('Unauthorized')
        except AdminException:
            self.add_message(u'Neautorizovaný přístup.')
            self.redirect(url=self.portal_url)
            return False

        return True

    def admin_view_links(self):
        links = []
        print("self.library", self.library)
        print("self.context.sigla", self.context.sigla)
        if not ((self.is_library_admin and self.library == self.context) or
                self.is_ziskej_operator):
            return links

        links = [
            dict(
                url = self.context_url + '/view',
                title = u'Profil',
                btncss = u'btn btn-info',
                ),
            dict(
                url = self.context_url + '/admin_log',
                title = u'Platby',
                btncss = u'btn btn-info',
                ),
            dict(
                url = self.context_url + '/admin_update',
                title = u'Editace profilu',
                btncss = u'btn btn-info',
                ),
            dict(
                url = self.context_url + '/admin_lbconf',
                title = u'Přidělování požadavků',
                btncss = u'btn btn-info',
                ),
            dict(
                url = self.context_url + '/admin_open',
                title = u'Provozní doba a dovolená',
                btncss = u'btn btn-info',
                ),
            dict(
                url = self.context_url + '/admin_librarians',
                title = u'Knihovníci',
                btncss = u'btn btn-info',
                ),
            dict(
                url = self.context_url + '/admin_sync',
                title = u'Synchronizace',
                btncss = u'btn btn-info',
                ),
            ]

        url = self.request.URL
        any_selected = False
        for link in links:
            if link['url'] in url:
                link['btncss'] = u'btn btn-primary'
                any_selected = True
                break

        if self.is_ziskej_operator:
            links.append(
                dict(
                    url = self.context_url + '/admin',
                    title = u'Operátor: (de)aktivace',
                    btncss = u'btn btn-danger',
                    )
                )
            if not any_selected:
                links[-1]['btncss'] = u'btn btn-primary'
        else:
            if not any_selected:
                links[0]['btncss'] = u'btn btn-primary'

        return links

    def _update(self, obj_dict, historylog=None):
        obj = self.context
        is_updated = False
        for k in obj_dict.keys():
            v_orig = getattr(obj, k, None)
            v = obj_dict.get(k, None)
            if v_orig != v:
                if DEBUG:
                    print(' - {}: {} --> {}'.format(k, safe_utf8(v_orig), safe_utf8(v)))
                setattr(obj, k, v)
                is_updated = True
            elif DEBUG:
                print(' - {}: {} --> {} xxx'.format(k, safe_utf8(v_orig), safe_utf8(v)))
        if is_updated:
            if historylog is None:
                historylog = u'Changed'
            obj.add_historylog(value=historylog)
        return is_updated

    def _get_validate_librarian_from_request(self, validation_type):
        librarian_dict = dict()
        all_fields_empty = True

        attr_names = ('first_name', 'last_name', 'email', 'phone_librarian',
                      'is_library_admin', )
        for k in attr_names:
            if k == 'is_library_admin':
                librarian_dict[k] = self.bool_from_request(k)
            else:
                librarian_dict[k] = self.text_from_request(k)
            if all_fields_empty and librarian_dict[k]:
                all_fields_empty = False
        if validation_type == 'library_activation':
            librarian_dict['is_library_admin'] = True
        else:
            librarian_dict['is_library_admin'] = bool(librarian_dict['is_library_admin'])

        is_optional = False
        if validation_type == 'library_activation':
            # neexistuje a není vše vyplněno - chyba
            # existuje a je vyplněna jen část - chyba
            # existuje a není nic vyplněno - ok
            if self.exist_library_admin:
                is_optional = True

        if all_fields_empty and is_optional:
            return None

        MANDATORY_FIELDS = ('first_name', 'last_name', 'email', )
        FIELD_LABELS = dict(
            first_name = u'Jméno',
            last_name = u'Příjmení',
            email = u'E-mail',
            )

        any_missing = False
        for k in MANDATORY_FIELDS:
            if not librarian_dict[k].strip():
                any_missing = True
                if not is_optional:
                    self.errors.append(
                        u'Chybějící pole {}.'.format(
                            FIELD_LABELS.get(k, k)
                            )
                        )
        if any_missing and validation_type == 'library_activation':
            if not is_optional:
                self.errors.append(u'Je nutné vytvořit i správce knihovny, všechna příslušná pole musí být vyplněna.')
            elif not all_fields_empty:
                self.errors.append(u'Je možné vytvořit i správce knihovny, v tom případě všechna příslušná pole musí být vyplněna.')

        if librarian_dict['email']:
            try:
                validate_email(librarian_dict['email'])
            except Invalid, e:
                self.errors.append(u'E-mailová adresa pro notifikace: {}'.format(e))

        if librarian_dict['phone_librarian']:
            try:
                validate_phone(librarian_dict['phone_librarian'])
            except Invalid, e:
                self.errors.append(u'{}'.format(e))

        # if username:
        #     try:
        #         validate_username(username)
        #         validate_unique_username(username, self.context)
        #     except Invalid, e:
        #         self.errors.append(e)

        # if password and password != confirm_password:
        #     self.errors.append(u'Heslo a jeho potvrzení se neshodují.')

        if len(self.errors) == 0:
            return librarian_dict
        if all_fields_empty:
            return None

    def _validate_unique_username(self, username):
        user = get_user_obj_by_user_id(username, warnings=False)
        return bool(not user)

    def _username_normalization(self, text, default=None):
        """Custom normalizace příjmení resp. příjmení a jména na validní
        username.

        Parametr text je očekáván unicode string, vrací string.
        """

        NORMALIZATION_ASCII = 'abcdefghijklmnopqrstuvwxyz'
        NORMALIZATION_UTF8_0 = u'áčďéěíňóřšťúůýž'
        NORMALIZATION_UTF8_1 = 'acdeeinorstuuyz'

        if not isinstance(text, basestring):
            return default

        text = text.lower()

        value = ''
        for x in text:
            if x in NORMALIZATION_ASCII:
                value += str(x)
            elif x in NORMALIZATION_UTF8_0:
                idx = NORMALIZATION_UTF8_0.index(x)
                value += NORMALIZATION_UTF8_1[idx]
            else:
                pass
                # nepodporovaný znak ve jménmu, např. slovenské měkké l apod.

        if len(value) < 2:
            return default

        return value

    def _username_librarian(self, library_shortname, librarian_dict, is_reserved=False):
        """Vrátí username (ascii, string) knihovníka.

        Algoritmus:

        1) Přednostně příjmení knihovníka bez diakritiky a spec. znaků.

        2) Pokud existuje, tak jmeno_prijmeni.

        3) Pokud existuje, tak zkratkaknihovny_prijmeni.

        4) Jinak zkratkaknihovny_prijmeni_999

        Parametr is_reserved je určen pro případy, kdy samotné vytváření
        uživatele vrací chybu, že se jedná o rezervované slovo, v takovém
        případě se přeskočí varianty 1, 2 a 3.

        Původní specifikace změny: Při vytváření loginu u knihovníka, jehož
        příjmení už v systému existuje, se použije jmeno_prijmeni, pokud
        existuje knihovník stejného jména i příjmení, použít
        zkratkaknihovny_prijmeni.
        """

        username_blacklist = self.context.objectIds()

        try:
            last_name = librarian_dict.get('last_name', None)
        except:
            last_name = None
        last_name = self._username_normalization(last_name)

        try:
            first_name = librarian_dict.get('first_name', None)
        except:
            first_name = None
        first_name = self._username_normalization(first_name)

        # 1) Přednostně příjmení knihovníka bez diakritiky a spec. znaků.
        if not is_reserved:
            username = last_name
            if username not in username_blacklist and \
                    self._validate_unique_username(username):
                return username

        # 2) Pokud existuje, tak jmeno_prijmeni.
        if not is_reserved:
            username = '{first_name}_{last_name}'.format(
                first_name=first_name,
                last_name=last_name,
                )
            if username not in username_blacklist and \
                    self._validate_unique_username(username):
                return username

        # 3) Pokud existuje, tak zkratkaknihovny_prijmeni.
        if not is_reserved:
            username = '{library_shortname}_{last_name}'.format(
                library_shortname=library_shortname,
                last_name=last_name,
                )
            if username not in username_blacklist and \
                    self._validate_unique_username(username):
                return username

        # 4) Jinak zkratkaknihovny_prijmeni_999
        counter = 0
        while counter < 1000:
            counter += 1
            username = '{library_shortname}_{last_name}_{counter:03d}'.format(
                library_shortname=library_shortname,
                last_name=last_name,
                counter=counter,
                )
            if username not in username_blacklist and \
                    self._validate_unique_username(username):
                return username

        # Původní anonymizovaná varianta k_boa001_001
        # username = 'k_{sigla}_{counter:03d}'.format(
        #     sigla=sigla, counter=counter)

        # Nepodařilo se najít žádné rozumné username
        return None

    def _create_librarian(self, librarian_dict):
        try:
            sigla = self.context.sigla.lower()
        except Exception, e:
            logger.exception('_create_librarian sigla error: ' + str(e))
            return None

        try:
            library_shortname = self.context.shortname.lower()
        except:
            logger_message = '_create_librarian shortname error for {sigla}: '
            logger.warning(logger_message.format(sigla=sigla) + str(e))
            library_shortname = sigla

        username = self._username_librarian(library_shortname, librarian_dict)

        if not username:
            logger_message = '_create_librarian cannot find new username for' \
                             ' {sigla}'
            logger.warning(logger_message.format(sigla=sigla))
            return None

        password = unicode(uuid.uuid4())[:28]

        librarian_dict['id'] = username
        librarian_dict['username'] = username
        librarian_dict['password'] = password
        librarian_dict['confirm_password'] = password

        try:
            obj = create_librarian(self.context, librarian_dict)
        except Exception, e:
            logger_message = '_create_librarian {username} failed' \
                             ' for {sigla} with: '
            logger.warning(logger_message.format(
                sigla=sigla,
                username=username,
                ) + str(e))
            if 'reserved' not in str(e):
                raise

            # Pokud příjmení po normalizaci trefí rezervované id, tak zkusit
            # ještě jednou.  Příklad:
            # BadRequest: The id "test" is reserved.

            username = self._username_librarian(
                library_shortname, librarian_dict, is_reserved=True)

            if not username:
                logger_message = '_create_librarian cannot find new username' \
                                 'after reserved issue for {sigla}'
                logger.warning(logger_message.format(sigla=sigla))
                return None

            librarian_dict['id'] = username
            librarian_dict['username'] = username

            try:
                obj = create_librarian(self.context, librarian_dict)
            except Exception, e:
                logger_message = '_create_librarian {username} failed' \
                                 ' for {sigla} with: '
                logger.error(logger_message.format(
                    sigla=sigla,
                    username=username,
                    ) + str(e))
                return None

        return obj

    def _setup_70(self):
        SETUP_70 = True
        if not SETUP_70:
            return False
        obj = self.context
        is_updated = False
        if obj.mvs_zk_cena not in (0, 70, ):
            obj.mvs_zk_cena = 70
            is_updated = True
        if obj.mvs_zk_dk_cena is not None:
            obj.mvs_zk_dk_cena = 70
            is_updated = True
        if obj.mvs_dk_cena != 70:
            obj.mvs_dk_cena = 70
            is_updated = True
        if obj.mvs_dk_cena_min != 70:
            obj.mvs_dk_cena_min = 70
            is_updated = True
        if obj.mvs_dk_cena_max != 70:
            obj.mvs_dk_cena_max = 70
            is_updated = True
        return is_updated


# Operator

class Libraries_Operator(AdminViewBase):
    template = ViewPageTemplateFile('admin_libraries_operator.pt')

    def call(self):
        super(Libraries_Operator, self).call()

        if not self._check_perm('operator'):
            return
        self.unauthorized = False

        self.action_url = self.context_url + '/admin'
        self.is_libraries_links = False
        if self.request.get('submit_activation', None):
            sigla = self.text_from_request('sigla')
            if sigla:
                library = self.library_by_sigla(sigla)
                if not library:
                    return None
                else:
                    url = '{}/admin'.format(library.absolute_url())
                    self.redirect(url=url)
                    return url

            search_fullname_library = self.text_from_request(
                'search_fullname_library')
            if not search_fullname_library:
                return None
            siglas_as_html = self._get_siglas_by_fullname(search_fullname_library, as_html=True)
            if not siglas_as_html:
                self.add_message(u'Žádná knihovna nenalezena.')
                return None

            self.is_libraries_links = True
            libraries = self._get_siglas_by_fullname(search_fullname_library, as_obj=True)

            self.libraries_links = []
            for library in libraries:
                url = '{}/admin'.format(library.absolute_url())
                self.libraries_links.append(
                    u'<a href="{}">{}</a>'.format(
                        url, library.Title(short=False))
                    )

class Libraries_OperatorUpdate(AdminViewBase):
    template = ViewPageTemplateFile('admin_libraries_operator_update.pt')

    def call(self):
        super(Libraries_OperatorUpdate, self).call()

        if not self._check_perm('operator'):
            return
        self.unauthorized = False

        self.action_url = self.context_url + '/admin_update'

        if self.request.get('submit', None):
            is_all_update = self.is_ziskej_admin and self.request.get('all', None)
            is_full_update = self.is_ziskej_admin and self.request.get('fullall', None)

            query = dict(
                path = '/'.join(self.context.getPhysicalPath()),
                portal_type = 'Library',
                sort_on = 'sigla',
                )
            if not is_all_update:
                query['review_state'] = 'published'
            brains = self.catalog(query)
            for brain in brains:
                library = brain.getObject()
                is_updated = self.lib_helper.library_sync(library, is_full_update=is_full_update)
                if is_updated:
                    logger.info('Knihovna aktualizovana z cpk {}'.format(library.sigla))


class Libraries_OperatorInfo(AdminViewBase):
    template = ViewPageTemplateFile('admin_libraries_operator_info.pt')

    def call(self):
        super(Libraries_OperatorInfo, self).call()

        if not self._check_perm('operator'):
            return
        self.unauthorized = False

        self.action_url = self.context_url + '/admin_info'

        query_library = dict(
            path = '/'.join(self.context.getPhysicalPath()),
            portal_type = 'Library',
            sort_on = 'sigla',
            review_state = 'published',
            )
        brains_library = self.catalog(query_library)
        siglas = [x.sigla for x in brains_library]

        query = dict(
            path = '/'.join(self.context.getPhysicalPath()),
            portal_type = 'Librarian',
            sort_on = 'path',
            )
        brains = self.catalog(query)

        deactivated_mails = []
        libadmin_mails = []
        all_mails = []
        for brain in brains:
            librarian = brain.getObject()
            mail = librarian.email
            if not mail:
                logger.warning('Librarian {} has empty email.'.format(
                    brain.getPath()))
                continue
            sigla = librarian.Library_sigla()
            if sigla not in siglas:
                deactivated_mails.append(mail)
                continue
            if librarian.is_library_admin:
                libadmin_mails.append(mail)
            all_mails.append(mail)

        self.deactivated_mails = u';'.join(deactivated_mails)
        self.libadmin_mails = u';'.join(libadmin_mails)
        self.all_mails = u';'.join(all_mails)


class Library_Operator(LibraryAdminViewBase):
    template = ViewPageTemplateFile('admin_library_operator.pt')

    def _exist_library_admin(self):
        query = dict(
            path = '/'.join(self.context.getPhysicalPath()),
            portal_type = 'Librarian',
            )
        brains = self.catalog(query)
        self.librarians = []
        for brain in brains:
            if brain.getObject().is_library_admin:
                return True
        return False

    def call(self):
        super(Library_Operator, self).call()

        if not self._check_perm('operator'):
            return
        self.unauthorized = False

        self.action_url = self.context_url + '/admin'
        self.deactivate_url = addTokenToUrl(self.action_url + '?deactivate=1')
        self.add_librarian_url = addTokenToUrl(self.context_url + '/++add++Librarian')
        self.active = bool(api.content.get_state(obj=self.context, default=None) == 'published')
        self.exist_library_admin = self._exist_library_admin()
        self.errors = []

        if self.request.get('deactivate', None):
            self.lib_helper.invalidate_cpkapi_including_deactivated_cache()
            api.content.transition(obj=self.context, transition='reject', comment=u'')
            # není potřeba, protože redirect
            # self.lib_helper.update_lid_ids()
            self.add_message(u'Knihovna byla deaktivována.')
            self.redirect(self.action_url)
            return

        if self.request.get('submit_activation', None):
            IS_SHIBBOLETH_MANDATORY = False
            idp_entityid = self.text_from_request('idp_entityid')
            if IS_SHIBBOLETH_MANDATORY and not idp_entityid:
                self.errors.append('IdP EntityID je povinné.')
            elif not validate_idp_entityid(idp_entityid):  # empty is valid
                self.errors.append('IdP EntityID není validní.')

            shortname = self.text_from_request('shortname')
            if not shortname:
                self.errors.append('Zkratka je povinná.')

            credit_ntk_user_id = self.text_from_request('credit_ntk_user_id')
            if not credit_ntk_user_id:
                self.errors.append(u'NTK kredit id (přihlašovací jméno uživatele pro NTK kredit) nemůže být prázdné.')
            else:
                try:
                    credit_ntk_user_id = str(credit_ntk_user_id)
                except:
                    self.errors.append(u'NTK kredit id (přihlašovací jméno uživatele pro NTK kredit) obsahuje nepovolené znaky.')

            librarian_dict = self._get_validate_librarian_from_request('library_activation')

            activate = self.request.get('activate', None)

            # valid
            if len(self.errors) == 0:
                changed = False
                is_updated = self._update(
                    dict(
                        idp_entityid = idp_entityid,
                        shortname = shortname,
                        credit_ntk_user_id = credit_ntk_user_id,
                        ),
                    historylog = u'Updated by operator during activation',
                    )

                is_updated = is_updated or self._setup_70()

                if is_updated:
                    changed = True
                    self.add_message(u'Nastavení knihovny bylo aktualizováno.')
                if librarian_dict:
                    try:
                        obj = self._create_librarian(librarian_dict)
                    except Exception, e:
                        obj = None
                        logger.exception(str(e))
                    if obj:
                        changed = True
                        self.add_message(u'Správce knihovny byl vytvořen.')
                    else:
                        self.add_message(u'Správce knihovny se nepodařilo vytvořit.')
                if activate:
                    self.lib_helper.invalidate_cpkapi_including_deactivated_cache()
                    api.content.transition(obj=self.context, transition='publish', comment=u'')
                    # není potřeba, protože redirect
                    # self.lib_helper.update_lid_ids()
                    changed = True
                    self.add_message(u'Knihovna byla aktivována.')
                if changed:
                    self.context.reindexObject()
                    self.redirect(self.action_url)
                    return


# Library admin

class Library_Log(LibraryAdminViewBase):
    template = ViewPageTemplateFile('admin_library_log.pt')

    def call(self):
        super(Library_Log, self).call()
        self.display_clearing = False

        if not self._check_perm('library_admin'):
            return
        self.unauthorized = False

        if self.is_ziskej_admin:
            self.display_clearing = bool(self.context.Credit_ntk_todo)

        self.action_url = self.context_url + '/admin_log'
        self.errors = []

        # Přidat clearing plateb z kreditu v NTK, které nemohly být strženy
        if self.display_clearing and self.request.get('submit_clearing', None):
            library = self.context
            self.clearing_result = paysys_clearing_admin(
                is_admin=self.is_manager and self.user_id == 'admin',
                libraries=[library]
                )
            self.add_message(u'Systém se pokusil strhnout platby, které dříve nemohly být strženy, výsledek je zobrazen na konci stránky.')
            library.reindexObject()

        self.logs = dict()
        self.logs['all'] = dict(
            title = u'Všechny transakce',
            description = u'Záznam všech transakcí ovlivňujících finanční konto ze Získej nebo ovlivňujících získaný kredit v ZÍSKEJ.',
            data = self._data(None),
            )

        # NTK jako knihovna v Získej
        if self.context.sigla == 'ABA013':
            self.show_library_credit_exception_for_ntk = True
            return

        # Přidat fakturaci kreditu Získej
        if self.request.get('submit_invoice', None):
            library = self.context
            # amount
            name = 'amount'
            value = self.request.get(name, None)
            if value is None:
                self.errors.append(u'Nebyla zadána částka')
            if value is not None:
                value = value.strip()
                try:
                    value = int(value)
                except ValueError:
                    self.errors.append(u'Částka není celé číslo.')
                    value = None
            if value is not None:
                if value <= 0:
                    self.errors.append(u'Částka je příliš nízká.')
                    value = None
                if value > library.Credit_ziskej_available:
                    self.errors.append(u'Částka je příliš vysoká.')
                    value = None
            amount = value

            invoice_id = self.text_from_request('invoice_id')

            if len(self.errors) == 0:
                library.credit_ziskej_invoice_add(amount, invoice_id)
                self.add_message(u'Faktura byla zaregistrována v Získej.')
                library.reindexObject()
                self.redirect(self.action_url)
                return

    def _data(self, category):
        library = self.context
        data = library.credit_log_data(category)
        data.reverse()
        result = []
        for item in data:
            result.append(library.credit_log_item_parsed(item))
        return result

class Library_Update(LibraryAdminViewBase):
    template = ViewPageTemplateFile('admin_library_update.pt')

    def call(self):
        super(Library_Update, self).call()

        if not self._check_perm('library_admin'):
            return
        self.unauthorized = False

        library = self.context
        request = self.request

        self.action_url = self.context_url + '/admin_update'
        self.errors = []

        if self.context.mvs_zk_cena is None:
            self.feezk = u'fix'
        elif self.context.mvs_zk_cena == 0:
            self.feezk = u'free'
        else:
            self.feezk = u'fix'

        self.apiaks_enabled = APIAKS_ENABLED and \
            self.is_library_admin_or_operator
        if self.apiaks_enabled:
            self.apiaks_aks_token = library.apiaks_generate_token()

        if self.request.get('submit', None):
            changed = False

            text_names = [
                'email', 'phone',
                'email_mvs', 'email_mvs_zk', 'email_mvs_dk', 'phone_mvs',
                'email_edd', 'email_edd_zk', 'email_edd_dk', 'phone_edd',
                'invoice_name', 'invoice_address', 'ico',
                'reader_lid_label', 'reader_lid_help', 'mvs_zk_cena',
                'mvs_zk_dk_cena', 'mvs_dk_cena', 'mvs_dk_cena_min',
                'mvs_dk_cena_max', ]
            bool_names = ['mvs_zk', 'mvs_dk', 'edd_zk', 'edd_dk',]
            if APIAKS_ENABLED:
                text_names.append('apiaks_enabled')
                bool_names.append('apiaks_enabled')
            if self.is_ziskej_operator:
                text_names.extend([
                    'shortname', 'credit_ntk_user_id', 'idp_entityid',
                    'deeplink_base_url', ])
                bool_names.extend([
                    'is_cpk','mvs_zk_allowed', 'mvs_dk_allowed', 'edd_zk_allowed', 'edd_dk_allowed'])
                if APIAKS_ENABLED:
                    text_names.append('apiaks_allowed')
                    bool_names.append('apiaks_allowed')

            data = dict()
            for k in text_names:
                data[k] = self.text_from_request(k)
            feezk = self.select_from_request('feezk', ['free', 'fix', ])

            for k in bool_names:
                value = self.text_from_request(k)
                data[k] = bool(value)

            if data['email'] is not None:
                try:
                    validate_email(data['email'])
                except Invalid, e:
                    self.errors.append(u'E-mail správce knihovny: {}'.format(e))

            if data['phone'] is not None:
                try:
                    validate_phone(data['phone'])
                except Invalid, e:
                    self.errors.append(u'Telefon správce knihovny: {}'.format(e))
                
            if data['phone_mvs'] is not None:
                try:
                    validate_phone(data['phone_mvs'])
                except Invalid, e:
                    self.errors.append(u'Telefon oddělení MVS: {}'.format(e))

            if data['email_mvs'] is not None:

                emails_mvs_list = [
                    x.strip() for x in data['email_mvs'].split(';')
                    if x.strip()]
                for email in emails_mvs_list:
                    try:
                        validate_email(email)
                    except Invalid, e:
                        self.errors.append(u'E-mail oddělení MVS: {}'.format(e))
                        break

            if data['email_mvs_dk'] is not None:
                email_mvs_dk_list = [
                    x.strip() for x in data['email_mvs_dk'].split(';')
                    if x.strip()]
                for email in email_mvs_dk_list:
                    try:
                        validate_email(email)
                    except Invalid, e:
                        self.errors.append(u'E-mail oddělení MVS jako Dožádaná knihovna: {}'.format(e))
                        break

            if data['email_mvs_zk'] is not None:
                email_mvs_zk_list = [
                    x.strip() for x in data['email_mvs_zk'].split(';')
                    if x.strip()]
                for email in email_mvs_zk_list:
                    try:
                        validate_email(email)
                    except Invalid, e:
                        self.errors.append(u'E-mail oddělení MVS jako Žádající knihovna: {}'.format(e))
                        break

            if data['phone_edd'] is not None:
                try:
                    validate_phone(data['phone_edd'])
                except Invalid, e:
                    self.errors.append(u'Telefon oddělení EDD: {}'.format(e))

            if data['email_edd'] is not None:

                emails_edd_list = [
                    x.strip() for x in data['email_edd'].split(';')
                    if x.strip()]
                for email in emails_edd_list:
                    try:
                        validate_email(email)
                    except Invalid, e:
                        self.errors.append(u'E-mail oddělení EDD: {}'.format(e))
                        break

            if data['email_edd_dk'] is not None:
                email_edd_dk_list = [
                    x.strip() for x in data['email_edd_dk'].split(';')
                    if x.strip()]
                for email in email_edd_dk_list:
                    try:
                        validate_email(email)
                    except Invalid, e:
                        self.errors.append(u'E-mail oddělení EDD jako Dožádaná knihovna: {}'.format(e))
                        break

            if data['email_edd_zk'] is not None:
                email_edd_zk_list = [
                    x.strip() for x in data['email_edd_zk'].split(';')
                    if x.strip()]
                for email in email_edd_zk_list:
                    try:
                        validate_email(email)
                    except Invalid, e:
                        self.errors.append(u'E-mail oddělení EDD jako Žádající knihovna: {}'.format(e))
                        break

            if self.is_ziskej_operator and not data['credit_ntk_user_id']:
                self.errors.append(u'NTK kredit id (přihlašovací jméno uživatele pro NTK kredit) nemůže být prázdné.')

            data['mvs_zk_cena'] = 70
            if feezk == 'free':
                data['mvs_zk_cena'] = 0

            if len(self.errors) == 0:
                Is_mvs_zk = self.context.Is_mvs_zk()

                is_updated = self._update(data, historylog=u'Changed')

                if APIAKS_ENABLED and self.bool_from_request(
                        'generate_apiaks_shared_secret'):
                    library.apiaks_generate_secret()
                    is_updated = True

                is_updated = is_updated or self._setup_70()

                if is_updated:
                    changed = True
                    self.add_message(u'Nastavení knihovny bylo aktualizováno.')

                    if self.context.Is_mvs_zk() != Is_mvs_zk:
                        self.lib_helper.invalidate_cpkapi_including_deactivated_cache()

            if changed:
                self.context.reindexObject()
                self.redirect(self.action_url)
                return


class Library_LBConf(LibraryAdminViewBase):
    template = ViewPageTemplateFile('admin_library_lbconf.pt')

    def call(self):
        super(Library_LBConf, self).call()

        if not self._check_perm('library_admin'):
            return
        self.unauthorized = False

        library = self.context
        request = self.request

        self.action_url = self.context_url + '/admin_lbconf'
        self.errors = []

        ignore_sigla = library.sigla.lower()

        libs_url = self.portal_url + '/libraries'
        whitelist = []
        self.whitelist = []
        for sigla in library.whitelist:
            sigla = sigla.lower().strip()
            if sigla == ignore_sigla:
                continue
            whitelist.append(sigla)
            title = self.lib_helper.get_title(sigla)
            #description = u'{sigla} - <a href="{libs_url}/{lib_id}" target="_blank">detail knihovny v novém okně</a>'.format(sigla=sigla.upper(), libs_url=libs_url, lib_id=sigla)
            description = sigla.upper()
            self.whitelist.append(
                dict(
                    id = sigla,
                    title = title,
                    description = description,
                    )
                )

        blacklist = []
        self.blacklist = []
        for sigla in library.blacklist:
            sigla = sigla.lower().strip()
            if sigla == ignore_sigla:
                continue
            blacklist.append(sigla)
            title = self.lib_helper.get_title(sigla)
            #description = u'{sigla} - <a href="{libs_url}/{lib_id}" target="_blank">detail knihovny v novém okně</a>'.format(sigla=sigla.upper(), libs_url=libs_url, lib_id=sigla)
            description = sigla.upper()
            self.blacklist.append(
                dict(
                    id = sigla,
                    title = title,
                    description = description,
                    )
                )

        otherlist = []
        self.otherlist = []
        # toto je potřeba kvůli aktuálním lib_ids a lib_ids_active,
        self.lib_helper.update_lid_ids()
        for sigla in self.lib_helper.lib_ids_active:
            sigla = sigla.lower().strip()
            if sigla == ignore_sigla:
                continue
            if sigla in whitelist or sigla in blacklist:
                continue
            otherlist.append(sigla)
            title = self.lib_helper.get_title(sigla)
            #description = u'{sigla} - <a href="{libs_url}/{lib_id}" target="_blank">detail knihovny v novém okně</a>'.format(sigla=sigla.upper(), libs_url=libs_url, lib_id=sigla)
            description = sigla.upper()
            self.otherlist.append(
                dict(
                    id = sigla,
                    title = title,
                    description = description,
                    )
                )

        alllist = [x for x in whitelist]
        alllist.extend(blacklist)
        alllist.extend(otherlist)

        if self.request.get('submit', None):
            changed = False

            whitelist = self.js_multiselect_from_request('whitelist', alllist)
            blacklist = self.js_multiselect_from_request('blacklist', alllist)

            if set(library.whitelist) != set(whitelist):
                library.whitelist = whitelist
                changed = True

            if set(library.blacklist) != set(blacklist):
                library.blacklist = blacklist
                changed = True

            if changed:
                self.context.reindexObject()
                self.redirect(self.action_url)
                return


class Library_Open(LibraryAdminViewBase):
    template = ViewPageTemplateFile('admin_library_open.pt')

    def _debug_open(self):
        self.result_debug_open = u''
        self.result_debug_open += u'<h4>Výsledek testů</h4>\n'
        library = self.context

        # Univerzální, beze změn data a času teď a provozní doby
        # a dovolené.
        self.result_debug_open += u'<h5>Výsledek testů s aktuálním nastavením provozní doby a dovolené</h5>\n'

        if DEBUG_OPEN_PRINTS:
            print("self.testing_now_dt", self.testing_now_dt)
        self._debug_open_json_now_item(now=self.testing_now_dt)

        if not DEBUG_OPEN_FULL:
            return

        # Měnit provozní dobu a dovolenou, pak měnit datum a času teď.
        self.result_debug_open += u'<h5>Výsledek testů s testovacím nastavením provozní doby a dovolené</h5>\n'

        NOW_LIST_DEFAULT = (
            u'2020-10-19T17:00',
            u'2020-10-19T17:01',
            u'2020-10-20T17:00',
            u'2020-10-20T17:01',
            u'2020-10-23T17:00',
            u'2020-10-23T17:01',
            # u'2020-10-22T17:00',
            # u'2020-10-22T17:01',
            # u'2020-10-21T17:00',
            # u'2020-10-21T17:01',
            )

        opening_hours_original = library.opening_hours
        vacation_original = library.vacation

        if DEBUG_OPEN_PRINTS:
            print(repr(opening_hours_original))
            print(repr(vacation_original))

        NO_VACATION = u'{"version": 1, "vacation": {"0": {"1": "", "0": ""}}}'

        json_test_data = [
            dict(
                label = u'Standard: Po-Pá 8:30-16:30, no vacation',
                open_json = u'{"opening_hours": {"wed": {"1": {"1": "", "0": ""}, "0": {"1": "16:30", "0": "08:30"}}, "sun": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "fri": {"1": {"1": "", "0": ""}, "0": {"1": "16:30", "0": "08:30"}}, "tue": {"1": {"1": "", "0": ""}, "0": {"1": "16:30", "0": "08:30"}}, "mon": {"1": {"1": "", "0": ""}, "0": {"1": "16:30", "0": "08:30"}}, "thu": {"1": {"1": "", "0": ""}, "0": {"1": "19:00", "0": "08:30"}}, "sat": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}}, "version": 1}',
                vacation_json = NO_VACATION,
                now_list = NOW_LIST_DEFAULT,
                ),
            dict(
                label = u'Provozní doba, penalizace, řádek 2-7, no vacation',
                open_json = u'{"opening_hours": {"wed": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "sun": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "fri": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "tue": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "mon": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "thu": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "sat": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}}, "version": 1}',
                vacation_json = NO_VACATION,
                now_list = NOW_LIST_DEFAULT,
                ),
            dict(
                label = u'Provozní doba, penalizace, řádek 8 - 13',
                open_json = u'{"opening_hours": {"wed": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "sun": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "fri": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "tue": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "mon": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "thu": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "sat": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}}, "version": 1}',
                vacation_json = NO_VACATION,
                now_list = (u'2020-10-22T17:00', u'2020-10-22T17:01',
                            u'2020-10-21T17:00', u'2020-10-21T17:01',
                            u'2020-10-20T17:00', u'2020-10-20T17:01', ),
                ),
            dict(
                label = u'Provozní doba, penalizace, řádek 14 - 17',
                open_json = u'{"opening_hours": {"wed": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "sun": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "fri": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "tue": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "mon": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "thu": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "sat": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}}, "version": 1}',
                vacation_json = NO_VACATION,
                now_list = (u'2020-10-19T17:00', u'2020-10-19T17:01',
                            u'2020-10-21T17:00',
                            u'2020-10-25T17:01', ),
                ),
            dict(
                label = u'Tabulka Dovolená a otevírací doba, penalizace, start date = 2020-10-19T17:00 end_date = 2020-10-26T17:00, Řádek 2',
                open_json = u'{"opening_hours": {"wed": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "sun": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "fri": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "tue": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "mon": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "thu": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "sat": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}}, "version": 1}',
                vacation_json = u'{"version": 1, "vacation": {"0": {"1": "2020-10-23", "0": "2020-10-20"}}}',
                now_list = (u'2020-10-19T17:00', ),
                reader_date_list = (u'2020-10-26', ),
                ),
            dict(
                label = u'Tabulka Dovolená a otevírací doba, penalizace, start date = 2020-10-19T17:00 end_date = 2020-10-26T17:00, Řádek 3',
                open_json = u'{"opening_hours": {"wed": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "sun": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "fri": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "tue": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "mon": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "thu": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "sat": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}}, "version": 1}',
                vacation_json = u'{"version": 1, "vacation": {"0": {"1": "2020-10-26", "0": "2020-10-19"}}}',
                now_list = (u'2020-10-19T17:00', ),
                reader_date_list = (u'2020-10-26', ),
                ),
            dict(
                label = u'Tabulka Dovolená a otevírací doba, penalizace, start date = 2020-10-19T17:00 end_date = 2020-10-26T17:00, Řádek 4',
                open_json = u'{"opening_hours": {"wed": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "sun": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}, "fri": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "tue": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "mon": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "thu": {"1": {"1": "", "0": ""}, "0": {"1": "18:00", "0": "12:00"}}, "sat": {"1": {"1": "", "0": ""}, "0": {"1": "", "0": ""}}}, "version": 1}',
                vacation_json = u'{"version": 1, "vacation": {"0": {"1": "2020-10-21", "0": "2020-10-20"}}}',
                now_list = (u'2020-10-19T17:00', ),
                reader_date_list = (u'2020-10-26', ),
                ),
            ]

        idx = -1
        for json_test_data_item in json_test_data:
            idx += 1
            if DEBUG_OPEN_PRINTS:
                print(" ")
                print("json {}".format(idx))
            library.opening_hours = json_test_data_item['open_json']
            library.vacation = json_test_data_item['vacation_json']

            self.result_debug_open += \
                u'<br><strong>---- JSON {}. {}----</strong><br><br>\n'.format(
                    idx,
                    json_test_data_item['label'],
                    )

            self.result_debug_open += u'open_json={}<br>\n'.format(
                json_test_data_item['open_json'])
            self.result_debug_open += u'vacation_json={}<br>\n'.format(
                json_test_data_item['vacation_json'])

            now_list = json_test_data_item.get('now_list', NOW_LIST_DEFAULT)
            reader_date_list = json_test_data_item.get(
                'reader_date_list', [None])

            for now in now_list:
                if DEBUG_OPEN_PRINTS:
                    print("  - {}".format(now))

                for reader_date in reader_date_list:

                    self.result_debug_open += \
                        u'<br><strong>now: {}, reader_date {}</strong><br><br>\n'.format(now, reader_date)

                    self._debug_open_json_now_item(
                        now=DateTime(now), reader_date=reader_date)

        library.opening_hours = opening_hours_original
        library.vacation = vacation_original

    def _debug_open_json_now_item(self, now=None, reader_date=None):
        if now is None:
            now = DateTime()

        library = self.context
        result = u'open_days_list: {}'.format(
            repr(library.open_days_list()))
        self.result_debug_open += u'<div>\n{result}\n</div>\n'.format(
            result=result)

        # #start_end_deltas = ((0, 10), (0, 4), )
        # start_end_deltas = ((0, 1), )
        # for start_delta, end_delta in start_end_deltas:
        #     start_dt = now + start_delta
        #     end_dt = now + end_delta
        #     result = u'_count_open_days({}, {}): {}'.format(
        #         start_delta,
        #         end_delta,
        #         repr(library._count_open_days(
        #             start_dt=start_dt, end_dt=end_dt)))
        #     self.result_debug_open += u'<div>\n{result}\n</div>\n'.format(
        #         result=result)
        #     result = u'_count_close_days({}, {}): {}'.format(
        #         start_delta,
        #         end_delta,
        #         repr(library._count_close_days(
        #             start_dt=start_dt, end_dt=end_dt)))
        #     self.result_debug_open += u'<div>\n{result}\n</div>\n'.format(
        #         result=result)

        result = u'_end_open_hours(now_dt={}): {}'.format(
            repr(now),
            repr(library._end_open_hours(now_dt=now)))
        self.result_debug_open += u'<div>\n{result}\n</div>\n'.format(
            result=result)

        result = u'_parse_vacation: {}'.format(
            repr(library._parse_vacation()))
        self.result_debug_open += u'<div>\n{result}\n</div>\n'.format(
            result=result)

        result = u'is_able_to_react_in_1d(now_dt={}): {}'.format(
            repr(now),
            repr(library.is_able_to_react_in_1d(now_dt=now)))
        self.result_debug_open += u'<div>\n{result}\n</div>\n'.format(
            result=result)

        self.result_debug_open += u'<br>\n'

        # result = u'_get_open_days_coef({now}): {res}'.format(
        #     now=repr(now),
        #     res=repr(library._get_open_days_coef(now=now)))
        # self.result_debug_open += u'<div>\n{result}\n</div>\n'.format(
        #     result=result)

        load_balancing_holiday_penalty = 1000
        result = u'_get_lb_coef({reader_date}, {now}, None): {res}'.format(
            reader_date=reader_date,
            now=repr(now),
            res=repr(library._get_lb_coef(
                reader_date_iso=reader_date, now=now, penalize_days=None) * \
                load_balancing_holiday_penalty))
        self.result_debug_open += u'<div>\n{result}\n</div>\n'.format(
            result=result)

    def _validate_vacation(self, vacation):
        """Jednotlivé dovolené už byly zkontrolovány, že nekončí dřív než
        začínají a mají vyplněný začátek i konec.  Nyní zkontrolovat, že se
        nepřekrývají.
        """

        vacation = self.context._cleanup_vacation(vacation)
        if vacation is None:
            self.add_message(u'Jednotlivé dovolené se překrývají, to není možné, změny dovolené nebyly uloženy.')
        return vacation

    def call(self):
        super(Library_Open, self).call()

        if not self._check_perm('library_admin'):
            return
        self.unauthorized = False

        self.action_url = self.context_url + '/admin_open'

        library = self.context
        request = self.request

        self.week_day_labels = dict(
            mon = u'Pondělí',
            tue = u'Úterý',
            wed = u'Středa',
            thu = u'Čtvrtek',
            fri = u'Pátek',
            sat = u'Sobota',
            sun = u'Neděle',
            )

        self.dws = (u'mon', u'tue', u'wed', u'thu', u'fri', u'sat', u'sun', )
        self.dw_idxs = ('0', '1', )
        self.cell_idxs = ('0', '1', )

        opening_hours_full_json = library.opening_hours
        if opening_hours_full_json:
            opening_hours_full = json.loads(opening_hours_full_json)
            opening_hours = opening_hours_full['opening_hours']
        else:
            opening_hours_full = dict(version=1, opening_hours=None)
            opening_hours = dict()
            for dw in self.dws:
                #opening_hours[dw] = [[u'', u'', ], [u'', u'', ], ]
                opening_hours[dw] = dict()
                for dw_idx in self.dw_idxs:
                    opening_hours[dw][dw_idx] = dict()
                    for cell_idx in self.cell_idxs:
                        opening_hours[dw][dw_idx][cell_idx] = u''
                        #opening_hours[dw][dw_idx][cell_idx] = u'dummy {dw}/{dw_idx}/{cell_idx}'.format(dw=dw, dw_idx=dw_idx, cell_idx=cell_idx)
        self.vac_days = (u'vac_0_0', u'vac_0_1,' u'vac_1_0',
                         u'vac_1_1', u'vac_2_0', u'vac_2_1')
        self.vac_0_0 = u''
        self.vac_0_1 = u''
        self.vac_1_0 = u''
        self.vac_1_1 = u''
        self.vac_2_0 = u''
        self.vac_2_1 = u''
        self.vacation_date_min = self.today_datapicker()
        #self.vacation_date_min = self.dt_as(DateTime(), result_type='datepicker')
        vacation_full_json = library.vacation
        if vacation_full_json:
            vacation_full = json.loads(vacation_full_json)

            if vacation_full['version'] == 1:
                vacation = vacation_full['vacation']
                if vacation != {}:
                    self.vac_0_0 = vacation['0']['0']
                    if self.vac_0_0:
                        self.vac_0_0 = self.dt_as(DateTime(self.vac_0_0), result_type='datepicker')
                    self.vac_0_1 = vacation['0']['1']
                    if self.vac_0_1:
                        self.vac_0_1 = self.dt_as(DateTime(self.vac_0_1), result_type='datepicker')
                    self.vac_1_0 = vacation.get('1', {}).get('0', u'')
                    if self.vac_1_0:
                        self.vac_1_0 = self.dt_as(DateTime(self.vac_1_0), result_type='datepicker')
                    self.vac_1_1 = vacation.get('1', {}).get('1', u'')
                    if self.vac_1_1:
                        self.vac_1_1 = self.dt_as(DateTime(self.vac_1_1), result_type='datepicker')
                    self.vac_2_0 = vacation.get('2', {}).get('0', u'')
                    if self.vac_2_0:
                        self.vac_2_0 = self.dt_as(DateTime(self.vac_2_0), result_type='datepicker')
                    self.vac_2_1 = vacation.get('2', {}).get('1', u'')
                    if self.vac_2_1:
                        self.vac_2_1 = self.dt_as(DateTime(self.vac_2_1), result_type='datepicker')

                # value = vacation['0']['0']
                # if value:
                #     self.vacation_from = self.dt_as(DateTime(value), result_type='datepicker')
                # value = vacation['0']['1']
                # if value:
                #     self.vacation_to = self.dt_as(DateTime(value), result_type='datepicker')
            else:
                raise Exception('Invalid vacation data format version: {}'.format(vacation_full['version']))
        else:
            vacation_full = dict(version=1, vacation=None)
            vacation = dict()
            for dw in ['0','1','2', ]:
                vacation[dw] = dict()
                for dw_idx in ['0', '1', ]:
                    vacation[dw][dw_idx] = u''
                    #vacation[dw][dw_idx] = u'dummy {dw}/{dw_idx}'.format(dw=dw, dw_idx=dw_idx)

        # Debug provozní doba a dovolená, AutoLB
        # Jen na local a test, možná omezit na admina
        self.show_debug_open = bool(BUILDOUT_TYPE in ('development', 'test', ))
        if self.show_debug_open and \
                request.get('submit', None) == 'admin_library_open_debug':
            alsoProvides(self.request, IDisableCSRFProtection)
            self._debug_open()

        # Standardní formulář
        elif request.get('submit', None):
            changed = False

            # Provozní doba

            for dw in self.dws:
                for dw_idx in self.dw_idxs:

                    # Volný text, ale je očekáváno 11:45 atp.
                    value_0 = self.text_from_request(dw+'_'+dw_idx+'_0')
                    if not value_0:
                        value_0 = u''
                    value_1 = self.text_from_request(dw+'_'+dw_idx+'_1')
                    if not value_1:
                        value_1 = u''

                    # Validace vč. případné změny formátu na 09:15
                    value_0_orig = value_0
                    value_0 = value_0.strip()
                    if value_0 and u':' not in value_0:
                        value_0 = u''
                        self.add_message(u'Řetězec "{}" není validní čas ve formátu 9:15.'.format(value_0_orig))
                    if value_0:
                        value_0_h, value_0_m = value_0.split(u':', 1)
                        try:
                            value_0_h = int(value_0_h)
                            value_0_m = int(value_0_m)
                        except:
                            value_0 = u''
                            self.add_message(u'Řetězec "{}" není validní čas ve formátu 9:15.'.format(value_0_orig))
                    if value_0 and (value_0_h < 0 or value_0_h >= 24):
                        value_0 = u''
                        self.add_message(u'Řetězec "{}" není validní čas ve formátu 9:15.'.format(value_0_orig))
                    if value_0 and (value_0_m < 0 or value_0_m >= 60):
                        value_0 = u''
                        self.add_message(u'Řetězec "{}" není validní čas ve formátu 9:15.'.format(value_0_orig))
                    if value_0 and len(value_0) == 4:
                        value_0 = u'0' + value_0

                    value_1_orig = value_1
                    value_1 = value_1.strip()
                    if value_1 and u':' not in value_1:
                        value_1 = u''
                        self.add_message(u'Řetězec "{}" není validní čas ve formátu 9:15.'.format(value_1_orig))
                    if value_1:
                        value_1_h, value_1_m = value_1.split(u':', 1)
                        try:
                            value_1_h = int(value_1_h)
                            value_1_m = int(value_1_m)
                        except:
                            value_1 = u''
                            self.add_message(u'Řetězec "{}" není validní čas ve formátu 9:15.'.format(value_1_orig))
                    if value_1 and (value_1_h < 0 or value_1_h >= 24):
                        value_1 = u''
                        self.add_message(u'Řetězec "{}" není validní čas ve formátu 9:15.'.format(value_1_orig))
                    if value_1 and (value_1_m < 0 or value_1_m >= 60):
                        value_1 = u''
                        self.add_message(u'Řetězec "{}" není validní čas ve formátu 9:15.'.format(value_1_orig))
                    if value_1 and len(value_1) == 4:
                        value_1 = u'0' + value_1

                    if (value_0 and not value_1) or (not value_0 and value_1):
                        value_0 = u''
                        value_1 = u''
                        self.add_message(u'Nelze zadat jen začátek nebo konec provozní doby.')
                    if value_0 and value_1 and value_0 > value_1:
                        value_0 = u''
                        value_1 = u''
                        self.add_message(u'Nelze zadat začátek provozní doby později než konec.')

                    # Bylo změněno?  Změnit ve zdroji pro json.
                    if opening_hours[dw][dw_idx]['0'] != value_0:
                        opening_hours[dw][dw_idx]['0'] = value_0
                        changed = True
                    if opening_hours[dw][dw_idx]['1'] != value_1:
                        opening_hours[dw][dw_idx]['1'] = value_1
                        changed = True

            # Dovolená

            # Datum ve formátu datapicker, viz get_DateTime_from_date_str
            message_invalid = u'Formát data pro začátek dovolené je neplatný.'
            for vac in range(3):
                value_0 = self.date_from_request('vac_{}_0'.format(vac),
                          result_type='iso',
                          message_invalid=message_invalid)
                if not value_0:
                    value_0 = u''
                message_invalid = u'Formát data pro konec dovolené je neplatný.'
                value_1 = self.date_from_request('vac_{}_1'.format(vac),
                          result_type='iso',
                          message_invalid=message_invalid)
                if not value_1:
                    value_1 = u''

                # Validace
                if (value_0 and not value_1) or (not value_0 and value_1):
                    value_0 = u''
                    value_1 = u''
                    self.add_message(u'Nelze zadat jen začátek nebo konec dovolené.')
                if value_0 and value_1 and value_0 > value_1:
                    value_0 = u''
                    value_1 = u''
                    self.add_message(u'Nelze zadat začátek dovolené později než konec.')

                # Bylo změněno?  Změnit ve zdroji pro json.
                if not vacation.get('{}'.format(vac), None):
                    vacation[str(vac)] = {'0': u'', '1': u'', }
                    changed = True
                if vacation['{}'.format(vac)]['0'] != value_0:
                    vacation['{}'.format(vac)]['0'] = value_0
                    changed = True
                if vacation['{}'.format(vac)]['1'] != value_1:
                    vacation['{}'.format(vac)]['1'] = value_1
                    changed = True
            vacation_validated = self._validate_vacation(vacation)

            # Provozní doba a/nebo dovolená - došlo ke změně?  Pak uložit json.
            if changed:
                opening_hours_full['opening_hours'] = opening_hours
                opening_hours_full_json = json.dumps(opening_hours_full, ensure_ascii=False)
                library.opening_hours = opening_hours_full_json
                if vacation_validated is not None:
                    vacation_full['vacation'] = vacation_validated
                    vacation_full_json = json.dumps(vacation_full, ensure_ascii=False)
                    library.vacation = vacation_full_json
                library.reindexObject()
                self.add_message(u'Změny byly uloženy.')
                self.redirect(self.action_url)
                self.opening_hours = opening_hours
                return
            else:
                self.add_message(u'Nedošlo k žádné změně.')

        self.opening_hours = opening_hours
        #self.vacation = vacation


class Library_Librarians(LibraryAdminViewBase):
    template = ViewPageTemplateFile('admin_library_librarians.pt')

    def call(self):
        super(Library_Librarians, self).call()

        if not self._check_perm('library_admin'):
            return
        self.unauthorized = False

        self.action_url = self.context_url + '/admin_librarians'

        detail_url = self.context_url + '/admin_librarians_detail'
        self.librarian_create_url = detail_url
        url_edit_base = detail_url + '?id='
        url_remove_base = self.action_url + '?action=remove&id='

        self.librarians = []

        # remove user
        action = self.request.get('action', None)
        lid = None
        if action == 'remove':
            lid = self.text_from_request('id', None)
            # validace
            if not lid:
                self.add_message(u'Není vybrán žádný uživatel ke smazání.')
            elif self.user_id == lid:
                lid = None
                self.add_message(u'Není možné smazat sám sebe.')
        if action == 'remove' and lid:
            delete_objects(self.portal, self.request, self.context, [lid, ])
            self.add_message(u'Uživatel {} byl smazán.'.format(lid))
            self.redirect(self.action_url)
            return

        query = dict(
            path = '/'.join(self.context.getPhysicalPath()),
            portal_type = 'Librarian',
            )
        brains = self.catalog(query)
        for brain in brains:
            obj = brain.getObject()
            is_library_admin = bool(obj.is_library_admin)
            lid = obj.getId()
            self.librarians.append(
                dict(
                    id = lid,
                    title = obj.Title(),
                    fullname = obj.fullname,
                    url = obj.absolute_url(),
                    url_edit = url_edit_base + lid,
                    url_remove = addTokenToUrl(url_remove_base + lid),
                    enabled = (brain.review_state == 'enabled'),
                    is_library_admin = is_library_admin,
                    )
                )
            if self.user_id == lid:
                self.librarians[-1]['url_remove'] = u''


class Library_Librarians_detail(LibraryAdminViewBase):
    template = ViewPageTemplateFile('admin_library_librarians_detail.pt')

    def call(self):
        super(Library_Librarians_detail, self).call()

        if not self._check_perm('library_admin'):
            return
        self.unauthorized = False

        lid = self.text_from_request('id', None)
        if not lid:
            lid = u''
        detail_url = self.context_url + '/admin_librarians_detail'
        self.action_url = detail_url
        self.current_url = detail_url + u'?id={lid}'.format(lid=lid)

        self.librarian = None
        self.librarian_label = u''
        if lid:
            query = dict(
                path = '/'.join(self.context.getPhysicalPath()),
                portal_type = 'Librarian',
                getId = lid,
                )
            brains = self.catalog(query)
            if len(brains) == 0:
                self.add_message(u'Knihovník nenalezen.')
                self.redirect()
                return
            self.librarian = brains[0].getObject()
            self.is_library_admin = bool(self.librarian.is_library_admin)
            self.librarian_label = self.librarian.fullname

        if self.request.get('submit', None):
            self.errors = []
            if not lid:
                # create
                librarian_dict = self._get_validate_librarian_from_request('librarian_create')
                if not librarian_dict:
                    message = u'Nepodařilo se  vytvořit knihovníka / správce knihovny.  Opravte prosím následující chyby:  ' + u'  '.join(self.errors)
                    self.add_message(message)
                    return
                if DEBUG_OPEN_PRINTS:
                    print('librarian_dict: ', librarian_dict)
                obj = self._create_librarian(librarian_dict)
                if not obj:
                    self.add_message(u'Nepodařilo se vytvořit knihovníka / správce knihovny.')
                    self.redirect(self.current_url)
                    return
                self.add_message(u'Knihovník / správce knihovny byl vytvořen.')
                self.redirect(self.current_url + u'{}'.format(obj.getId()))
                return
            else:
                # update
                librarian_dict = self._get_validate_librarian_from_request('librarian_update')
                if not librarian_dict:
                    message = u'Nedošlo k žádné změně.'
                    self.add_message(message)
                    return
                if self.librarian.update_attrs_if_changed(**librarian_dict):
                    self.add_message(u'Nastavení knihovníka bylo změněno')
                    self.librarian.reindexObject()
                    self.redirect(self.current_url)
                    return
                self.add_message(u'Nebyly zadány žádné změny')


class Library_Sync(LibraryAdminViewBase):
    template = ViewPageTemplateFile('admin_library_sync.pt')

    def call(self):
        super(Library_Sync, self).call()

        if not self._check_perm('library_admin'):
            return
        self.unauthorized = False

        self.action_url = self.context_url + '/admin_sync'

        if self.request.get('submit', None):
            is_full_update = self.is_ziskej_operator and self.request.get('full', None)
            is_updated = self.lib_helper.library_sync(self.context, is_full_update=is_full_update)
            if is_updated:
                self.add_message(u'Knihovna byla aktualizována na základě dat z ADR.')
                self.redirect(self.action_url)
            else:
                self.add_message(u'Knihovna nebyla aktualizována, protože nedošlo k žádné změně v datech z ADR.')
