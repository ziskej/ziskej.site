# -*- coding: utf-8 -*-
"""
"""

import json
import logging
import uuid

from DateTime import DateTime
from plone import api
from plone.protect.interfaces import IDisableCSRFProtection
from plone.protect.utils import addTokenToUrl
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.interface import Invalid, alsoProvides

from ziskej.site import BUILDOUT_TYPE
from ziskej.site.auth.auth_base import get_user_obj_by_user_id
from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.utils import (
    delete_objects,
    pprint,
    safe_unicode,
    safe_utf8,
    )


logger = logging.getLogger("ziskej")


class Reader_Update(ZiskejBrowserView):

    template = ViewPageTemplateFile('admin_reader_update.pt')

    def _check_perm(self):
        if not self.is_ziskej_operator:
            self.add_message(u'Neautorizovaný přístup.')
            self.redirect(url=self.portal_url)
            return False
        return True

    def call(self):
        self.reader = None
        self.library_url = None
        self.library_fullname = None
        self.display_edit_form = False

        if not self._check_perm():
            return

        self.action_url = u'{}/admin_reader'.format(self.context_url)
        if self.is_ziskej_admin:
            self.display_edit_form = True

        self.reader = self.context
        sigla = self.reader.library
        if sigla:
            sigla = sigla.lower()
            library = getattr(self.portal.libraries, sigla, None)
        if sigla and library:
            self.library_url = library.absolute_url()
            self.library_fullname = library.Title()

        if self.display_edit_form:
            self.password_reset_url = u'{}/passwd_reset_init?u={}'.format(
                self.portal_url, self.reader.getId())

        if self.display_edit_form and self.text_from_request('submit'):
            obj_dict = dict()
            obj_dict['first_name'] = self.text_from_request('first_name')
            obj_dict['last_name'] = self.text_from_request('last_name')
            obj_dict['email_user'] = self.text_from_request('email_user')
            is_updated = self.reader.update_attrs_if_changed(**obj_dict)
            if is_updated:
                self.reader.reindexObject()
                self.add_message(u'Profil čtenáře byl upraven.')
                self.redirect(url=self.action_url)
