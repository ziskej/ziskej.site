# -*- coding: utf-8 -*-
"""
"""

import logging
import requests

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.cpk_api import (
    CpkDocument,
    # CpkDocumentsOverview,
    CpkApiException,
    )
from ziskej.site.utils_lib import safe_utf8, safe_unicode


logger = logging.getLogger("ziskej")

DEBUG = False


class AdminException(Exception):
    pass


class TestViewBase(ZiskejBrowserView):

    def call(self):
        super(TestViewBase, self).call()
        self.unauthorized = True

    def _check_perm(self, lvl, sigla=None):
        try:
            if lvl == 'manager':
                if not self.is_manager:
                    raise AdminException('Unauthorized')
            elif lvl == 'admin':
                if not self.is_ziskej_admin:
                    raise AdminException('Unauthorized')
            elif lvl == 'operator':
                if not self.is_ziskej_operator:
                    raise AdminException('Unauthorized')
            elif lvl == 'library_admin':
                if self.is_ziskej_operator:
                    pass
                elif self.is_library_admin and self.library.sigla == sigla:
                    pass
                else:
                    raise AdminException('Unauthorized')
            else:
                raise AdminException('Unauthorized')
        except AdminException:
            self.add_message(u'Neautorizovaný přístup.')
            self.redirect(url=self.portal_url)
            return False

        return True


class TestCpkApi(TestViewBase):
    template = ViewPageTemplateFile('test_cpk_api.pt')

    def call(self):
        super(TestCpkApi, self).call()
        self.result = u''

        if not self._check_perm('admin'):
            return
        self.unauthorized = False

        self.action_url = self.context_url + '/test_cpk_api'

        submit = self.request.get('submit', None)
        if submit:
            doc_id = self.text_from_request('doc_id')
            try:
                str(doc_id)
            except:
                self.add_message(u'Chybné doc_id, použijte doc_id z CPK, určitě neobsahuje diakritiku.')
                return
        if submit and doc_id:
            cpk_doc = CpkDocument(None, doc_id)
            try:
                doc_data = cpk_doc.call()
            except CpkApiException, e:
                self.result += u'{}'.format(str(e))
                return

            if not doc_data:
                logger.info('Missing doc_data for doc_id {}'.format(doc_id))
                self.result = u'<strong>CPK API neposkytuje dostatek informací o dokumentu – zcela chybí nebo není validní.</strong>'
                return

            result = u''
            unit_ids = doc_data.get('unit_ids', [])
            for unit_id in unit_ids:
                res = self.cpkapi_request(unit_id)
                result += u'<li>{}</li>'.format(res)
            if result:
                result = u'<ul>{}</ul>'.format(result)
                self.result = result
            else:
                logger.info('Missing or empty unit_ids for doc_id {}'.format(doc_id))
                self.result = u'<strong>CPK API neposkytuje dostatek informací o dokumentu – chybí informace o jednotkách (996 t) nebo nejsou validní.</strong>'

    def cpkapi_request(self, unit_id):
        result = u''
        url = 'https://www.knihovny.cz/api/v1/item'
        params = dict(id=unit_id, ext='1')
        full_url = '{}?id={}&ext=1'.format(url, unit_id)
        logger.info(full_url)
        result_obj = requests.get(url, params=params)
        status_code = result_obj.status_code
        if not result_obj.ok:
            try:
                data = result_obj.json()
            except:
                data = None
            result = u'<strong>CHYBA {} pro <u title="unit_id jednotky">{}</u>:</strong>\n'.format(status_code, unit_id)
            res = u''
            if data:
                for k in data:
                    res += u'<li><span class="dt">{}:</span> {}</li>\n'.format(k, safe_unicode(data[k]))
            result += u'<ul>{}</ul>\n'.format(res)
        else:
            data = result_obj.json()
            result = u'OK {} pro <u title="unit_id jednotky">{}</u>:\n'.format(status_code, unit_id)
            res = u''
            if data:
                for k in data:
                    res += u'<li><span class="dt">{}:</span> {}</li>\n'.format(k, safe_unicode(data[k]))
            result += u'<ul>{}</ul>\n'.format(res)
        return result
