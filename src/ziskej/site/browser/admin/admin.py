# -*- coding: utf-8 -*-
"""BrowserView: Administrace
"""

import logging

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.browser import ZiskejBrowserView


logger = logging.getLogger("ziskej")

DEBUG = False


class AdminException(Exception):
    pass


class AdminViewBase(ZiskejBrowserView):

    def call(self):
        super(AdminViewBase, self).call()
        self.unauthorized = True

    def _check_perm(self, lvl, sigla=None):
        try:
            if lvl == 'manager':
                if not self.is_manager:
                    raise AdminException('Unauthorized')
            elif lvl == 'admin':
                if not self.is_ziskej_admin:
                    raise AdminException('Unauthorized')
            elif lvl == 'operator':
                if not self.is_ziskej_operator:
                    raise AdminException('Unauthorized')
            elif lvl == 'library_admin':
                if self.is_ziskej_operator:
                    pass
                elif self.is_library_admin and self.library.sigla == sigla:
                    pass
                else:
                    raise AdminException('Unauthorized')
            else:
                raise AdminException('Unauthorized')
        except AdminException:
            self.add_message(u'Neautorizovaný přístup.')
            self.redirect(url=self.portal_url)
            return False

        return True


class System_Parameters(AdminViewBase):
    template = ViewPageTemplateFile('system_parameters.pt')

    def call(self):
        super(System_Parameters, self).call()

        if not self._check_perm('admin'):
            return
        self.unauthorized = False

        self.action_url = self.context_url + '/system_parameters'

        # Auto LB
        # Nyní se používají přímo parametry definované v ticket_load_balancing.

        # autolb_default = dict(

        #     # Rating limit viz LB alg dokumentaci
        #     limit = 1000,

        #     # Penalizace za DK

        #     # DK není v CPK ai pasivně (neznáme ji)
        #     lib_not_cpk = 2000,

        #     # DK není aktivní v Získej
        #     lib_not_ziskej = 1000,

        #     # DK je na blacklistu ŽK
        #     lib_blacklist = 80,

        #     # Default
        #     lib = 50,

        #     # DK je na blacklistu ŽK
        #     lib_whitelist = 10,

        #     # Penalizace za (ne)dostupnost

        #     # jednotka je nedostupná
        #     unavailable = 10000,

        #     # dostupnost není známa
        #     unknown = 1000,

        #     # je dostupné později
        #     #available_later = 500,

        #     # dostupné jen do studovny
        #     available_stud = 200,

        #     # dostupné
        #     available = 100,

        #     # SUGGESTION penalizace za rychlost dodání - par

        #     # Bonus za počet jednotek
        #     additional_unit = -1,

        #     # Limit pro tento bonus
        #     additional_unit_limit = -9,

        #     # Penalizace za historii
        #     history_m = 5,  # poslední měsíc
        #     history_q = 2,  # poslední 3 měsíce
        #     history_s = 1,  # posledních 6 měsíců
        #     history_limit = 100,  # limit pro tuto penalizaci

        #     )

        self.pars = self.home.get_parameters()

        if self.request.get('submit', None):
            changed = False
            parameters = dict()
            for par in self.pars:
                value_orig = self.pars.get(par, None)
                value_new = self.text_from_request(par)
                if value_orig != value_new:
                    changed = True
                    parameters[par] = value_new
            if changed:
                self.home.set_parameters(parameters)
                self.add_message(u'Parametry byly uloženy.')
                self.redirect(url=self.action_url)
                return self.action_url
            else:
                self.add_message(u'Nebyla provedena žádná změna.')

