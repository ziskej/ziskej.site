# -*- coding: utf-8 -*-
"""BrowserView: Správa hesel.
"""

import json
import logging
import random
import uuid

from plone.protect.interfaces import IDisableCSRFProtection
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.interface import alsoProvides

from ziskej.site.auth.auth_base import get_user_obj_by_user_id
from ziskej.site.browser.admin.admin import AdminViewBase

logger = logging.getLogger("ziskej.site")


class PasswdResetBaseView(AdminViewBase):

    def _set_user(self):
        user_id = self.text_from_request('u')
        self.user = get_user_obj_by_user_id(user_id)
        if not self.user:
            logger.info('passwd_reset: no user')
            self.add_message(u'Chybný požadavek na reset hesla.')
            self.redirect()
            return False
        if self.user.portal_type not in ('Librarian', 'Reader', ):
            logger.info('passwd_reset: not Librarian or reader')
            self.add_message(u'Chybný požadavek na reset hesla.')
            self.redirect()
            return False
        self.user_librarian = bool(self.user.portal_type == 'Librarian')
        self.user_reader = bool(self.user.portal_type == 'Reader')
        if self.user_librarian:
            # Použije se jen v _check_perm jen pro heslo knihovníka
            self.user_library_sigla = self.user.Library_sigla()
        return True


class PasswdResetInitView(PasswdResetBaseView):

    template = ViewPageTemplateFile('passwd_reset_init.pt')

    def set_user(self):
        if not self._set_user():
            return False
        return True

    def call(self):
        self.unauthorized = True

        self.action_url = self.portal_url + '/passwd_reset_init'

        if not self.set_user():
            return

        if self.user_librarian:
            if not self._check_perm('library_admin', sigla=self.user_library_sigla):
                return
        if self.user_reader:
            if not self._check_perm('admin'):
                return
        self.unauthorized = False

        self.token = self.user.passwd_reset_token
        if not self.token:
            self.token = u''

        self.user_role_label = u''
        if self.user.portal_type == 'Reader':
            self.user_role_label = u'Čtenář'
        elif self.user.portal_type == 'Librarian':
            if self.user.is_library_admin:
                self.user_role_label = u'Správce knihovny (a knihovník)'
            else:
                self.user_role_label = u'Knihovník'

        submit = self.text_from_request('submit')
        if not submit:
            return

        alsoProvides(self.request, IDisableCSRFProtection)

        #rnd_uuid = str(uuid.uuid4()).replace('-', '')[:12]
        self.token = unicode(uuid.uuid4())[:28]
        self.user.passwd_reset_token = self.token
        # SUGGESTION zvážit audit log
        self.user._p_changed = True
        self.add_message(u'Bylo umožněno zobrazit stránku s nastavením nového hesla.')
        user_id = self.text_from_request('u')
        self.token_url = self.portal_url + '/passwd_reset?u={u}&token={token}'.format(
            u = user_id,
            token = self.token,
            )


class PasswdResetView(PasswdResetBaseView):

    template = ViewPageTemplateFile('passwd_reset.pt')

    def validate_password(self, password):
        if len(password) < 8:
            return u'Příliš krátké heslo'
        return None

    def set_user_token(self):
        if not self._set_user():
            return False

        if not self.user.passwd_reset_token:
            logger.info('passwd_reset: user wo token')
            self.add_message(u'Chybný požadavek na reset hesla.')
            self.redirect()
            return False

        token = self.text_from_request('token')
        if self.user.passwd_reset_token != token:
            logger.info('passwd_reset: invalid token')
            self.add_message(u'Chybný požadavek na reset hesla.')
            self.redirect()
            return False

        return True

    def call(self):
        self.show_log_in = False
        self.action_url = self.portal_url + '/passwd_reset'

        if not self.set_user_token():
            return

        submit = self.text_from_request('submit')
        if not submit:
            return

        password = self.text_from_request('password', sanitize=False)
        confirm_password = self.text_from_request(
            'confirm_password', sanitize=False)
        if not password:
            self.add_message(u'Nelze zvolit prázdné heslo.')
            return
        if password != confirm_password:
            self.add_message(u'Heslo a jeho ověření nejsou stejné.')
            return
        message = self.validate_password(password)
        if message:
            self.add_message(message)
            return

        alsoProvides(self.request, IDisableCSRFProtection)

        self.user.set_password(password)
        self.user.passwd_reset_token = u''
        self.user._p_changed = True
        self.add_message(u'Heslo bylo nastaveno')
        if not self.user_id:
            self.show_log_in = True
            self.login_form_url = self.build_url(
                self.portal_url + '/login_form',
                dict(
                    __ac_name = self.user.getId(),
                    came_from = self.portal_url,
                    ))
