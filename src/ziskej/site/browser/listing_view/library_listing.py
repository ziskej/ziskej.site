# -*- coding: utf-8 -*-
"""BrowserView Listing: Libraries.

Seznam knihoven.
"""

import logging

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.protect.utils import addTokenToUrl
from plone.dexterity.utils import createContent

from ziskej.site.auth.auth_sudo import SUDO_ENABLED, PARAMETER_SUDO
from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.utils import address_encode
#from ziskej.site.cpk_api import cpkapi_library_update_or_create
#from ziskej.site.library_helpers import lib_helper

logger = logging.getLogger("ziskej")


class LibraryListingView(ZiskejBrowserView):

    template = ViewPageTemplateFile('library_listing.pt')

    def call(self):

        self.unauthorized = True
        if self.is_anon:
            self.redirect(self.portal_url)
            return
        elif self.is_librarian:
            pass
        # elif self.is_reader:
        #     pass
        elif self.is_ziskej_operator:
            pass
        else:
            self.redirect(self.portal_url)
            return
        self.unauthorized = False

        query = None
        if self.is_ziskej_operator or self.is_librarian:
            # self.cpkapi_url = addTokenToUrl(self.context_url + '?cpkapi=1')
            # self.nonactive_url = self.context_url + '?na=1'
            #
            # # load libraries z CPK API
            # if self.is_manager and self.request.get('cpkapi', None):
            #     objs = cpkapi_library_update_or_create(self.portal, self.request, library_ids=None, createContent=createContent)
            #     log_message = u'cpkapi_library_update_or_create: {nr} records created or updated if changed'.format(nr=len(objs))
            #     logger.info(log_message)
            #     message = u'Byly načteny záznamy o knihovnách z CPK API, v systému Získej byly příslušné knihovny vytvořeny nebo aktualizovány pokud záznamy obsahovaly nové či odlišné údaje. Počet načtených záznamů: {nr}'.format(nr=len(objs))
            #     #self.redirect(message=message)
            #     ptool = self.portal.plone_utils
            #     ptool.addPortalMessage(message, 'info')
            #     self.redirect()

            # list
            query = dict(
                path = '/'.join(self.context.getPhysicalPath()),
                portal_type = 'Library',
                review_state = 'published',
                sort_on = 'sigla',
                )
            if self.is_ziskej_operator and self.request.get('na', None):
                query['review_state'] = 'private'
        else:
            query = None
        if query is not None:
            brains = self.catalog(query)
        else:
            brains = []
        base_url = self.context_url
        self.libraries = []
        for brain in brains:
            obj = brain.getObject()
            services_list =[] 
            if obj.Is_mvs_zk():
                services_list.append(u'MVS ZK')
            if obj.Is_mvs_dk():
                services_list.append(u'MVS DK')
            if obj.Is_edd_zk():
                services_list.append(u'EDD ZK')
            if obj.Is_edd_dk():
                services_list.append(u'EDD DK')

            services_str = u', '.join(services_list)

            # sudo
            op_su_sigla_url = None
            if SUDO_ENABLED and self.is_ziskej_admin and \
                    not self.ziskej_user_info.get('sudo', None) and \
                    brain.review_state == 'published':
                op_su_sigla_url = u'{}?{}={}'.format(
                    base_url, PARAMETER_SUDO, brain.sigla.lower())

            self.libraries.append(
                dict(
                    sigla=brain.sigla,
                    op_su_sigla_url=op_su_sigla_url,
                    name=brain.fullname,
                    address=brain.address,
                    address_encoded=address_encode(brain.address),
                    url=brain.getURL(),
                    services_list=services_str,
                    vacation_info=obj.Vacation_info(is_html=True),
                    )
                )
