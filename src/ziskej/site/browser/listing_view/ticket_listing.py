# -*- coding: utf-8 -*-
"""BrowserView Listing: Tickets / Subtickets.

Seznam objednávek / požadavků.
"""

from __future__ import print_function

import logging
import urllib

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.ticket_helpers import (
    TICKET_PORTAL_TYPES,
    SUBTICKET_PORTAL_TYPES,
    TICKETMVS_WF_S_LABELS,
    SUBTICKETMVS_WF_S_LABELS,
    get_ticket_wf_label,
    get_subticket_wf_label,
    )
from ziskej.site.utils import (
    datify,
    pprint,
    )


logger = logging.getLogger("ziskej")

DEBUG = False
DEBUG_LEN_CATALOG_QUERY = True
BATCH_SIZE = 50
BATCH_SIZE_ALL = 5000


class TicketListingBaseView(ZiskejBrowserView):

    def call(self):
        self.tickets_len = 0
        self.subtickets_len = 0
        self.unauthorized = True
        self.library_zk_obj = None
        self.library_dk_obj = None
        self.context_url_view = self.context_url
        self.batch_init()

    def batch_init(self):
        if DEBUG:
            print("-- batch_init")
        self.batch_disable = bool(self.text_from_request('batch_disable', None))
        if self.batch_disable:
            # ve skutečnosti ne vypnout batch, ale nastavit na velký počet
            self.batch_size = BATCH_SIZE_ALL
        else:
            self.batch_size = BATCH_SIZE
        self.batch_start = 0
        batch_start = self.int_from_request('batch_start')
        if batch_start is not None:
            self.batch_start = batch_start
        self.batch_end = self.batch_start + self.batch_size
        self.batch_start_prev = None
        if self.batch_start > 0:
            self.batch_start_prev = self.batch_start - self.batch_size
            if self.batch_start_prev < 0:
                self.batch_start_prev = 0
        # set to None later if end is reached already
        self.batch_start_next = self.batch_end
        self.batch_prev_url = None
        self.batch_next_url = None
        self.batch_all_url = None
        self.batch_show = False
        self.batch_start_nr = u''
        self.batch_end_nr = u''
        self.batch_total_nr = u''

    def set_batch(self):
        if DEBUG:
            print("-- set_batch")
            print(self.request.QUERY_STRING)
            pprint(dict(self.request.form))

        PARAMS = (
            # Filter
            'filter', 'filter_rs',
            # Search (ZK)
            'sigla', 'search_id', 'search_is_reader', 'search_reader_lid',
            'search_reader_username', 'search_sigla_dk', 'search_fullname_dk',
            'submit_search',
            # Search (DK)
            'search_sigla_zk', 'search_fullname_zk',
            # EDD
            'search_service',
            )
        params = dict()
        for k in PARAMS:
            v = self.request.form.get(k, None)
            if v is not None:
                params[k] = v
        query_string = ''
        if params.keys():
            query_string = urllib.urlencode(params) + '&'
        batch_base_url = '{}?{}'.format(self.context_url_view, query_string)
        if DEBUG:
            print("batch_base_url:", batch_base_url)

        tst_len = self.tickets_len + self.subtickets_len

        if tst_len <= self.batch_end:
            self.batch_start_next = None
            self.batch_end = tst_len
        if self.batch_start_prev is not None:
            self.batch_show = True
            self.batch_prev_url = u'{}batch_start={}'.format(
                batch_base_url, self.batch_start_prev)
        if self.batch_start_next is not None:
            self.batch_show = True
            self.batch_next_url = u'{}batch_start={}'.format(
                batch_base_url, self.batch_start_next)
        self.batch_start_nr = u'{}'.format(self.batch_start + 1)
        self.batch_end_nr = u'{}'.format(self.batch_end)
        self.batch_total_nr = u'{}'.format(tst_len)
        self.batch_info = u'{} – {} / {}'.format(
            self.batch_start_nr, self.batch_end_nr, self.batch_total_nr)
        self.batch_all_url = u'{}batch_disable=1'.format(batch_base_url)

    def ticket_get_review_state_label(self, review_state):
        return TICKETMVS_WF_S_LABELS.get(review_state, review_state)

    def subticket_get_review_state_label(self, review_state):
        return SUBTICKETMVS_WF_S_LABELS.get(review_state, review_state)

    def display_date(self, date_str, is_time=True):
        """date_str .. formát z katalogu, např. created / CreationDate
        """

        if not date_str:
            return u''
        # Zope DateTime
        dt = datify(date_str)  # FIXME deprecated
        #value = dt.strftime('%-d.%-m.%Y %-H:%M')
        value = self.date_format(dt, is_time=is_time, is_html=True)
        return value


class TicketListingView(TicketListingBaseView):

    template = ViewPageTemplateFile('ticket_listing_redirect.pt')

    def call(self):
        super(TicketListingView, self).call()

        if self.is_anon:
            self.redirect(self.portal_url)
            return
        self.unauthorized = False
        if self.is_reader:
            self.redirect(self.context_url + '/listing_reader')
            return
        if self.is_librarian or self.is_ziskej_operator:
            self.redirect(self.context_url + '/listing_zk')
            return
        self.redirect(self.portal_url)


class TicketListingReaderView(TicketListingBaseView):

    template = ViewPageTemplateFile('ticket_listing_reader.pt')

    def call(self):
        super(TicketListingReaderView, self).call()

        self.context_url_view = self.context_url + '/listing_reader'

        self.unauthorized = True
        if self.is_anon:
            self.redirect(self.portal_url)
            return
        self.unauthorized = False
        if not self.is_reader:
            if self.is_librarian or self.is_ziskej_operator:
                self.redirect(self.context_url + '/listing_zk')
                return
            self.redirect(self.portal_url)
            return

        user_id = self.ziskej_user_info['user_id']
        self.title = u'Objednávky čtenáře'

        # list
        query = dict(
            path = '/'.join(self.context.getPhysicalPath()),
            portal_type = TICKET_PORTAL_TYPES,
            reader = user_id,
            sort_on = 'hid',
            sort_order = 'reverse',
            )

        brains = self.catalog(query)

        self.tickets = []
        for brain in brains:
    
            unread = False
            if self.is_reader:
                unread = brain.unread_r

            url = brain.getURL()

            obj = brain.getObject()

            substate_label = u''
            if obj.is_created_by_cpk_for_reader() and brain.review_state == 'created':
                if obj.is_paid_in_advance():
                    substate_label = u'(zaplacená)'
                else:
                    substate_label = u'(nezaplacená)'

            self.tickets.append(
                dict(
                    hid = brain.hid,
                    title = brain.Title,
                    url = url,
                    review_state = brain.review_state,
                    review_state_label = self.ticket_get_review_state_label(brain.review_state),
                    date_created = self.display_date(brain.CreationDate),
                    date_modified = self.display_date(brain.ModificationDate),
                    date_pick = '15.11.2017',
                    date_return = '22.11.2017',
                    unread = unread,
                    doc_title = brain.doc_title,
                    complaint_cancel_label = obj.complaint_cancel_label,
                    substate_label = substate_label,
                    )
                )

        self.tickets_len = len(self.tickets)


class TicketListingZKView(TicketListingBaseView):

    template = ViewPageTemplateFile('ticket_listing_zk.pt')

    def call(self):
        super(TicketListingZKView, self).call()

        self.context_url_view = self.context_url + '/listing_zk'

        self.unauthorized = True
        if self.is_anon:
            self.redirect(self.portal_url)
            return
        self.unauthorized = False
        if not self.is_librarian and not self.is_ziskej_operator:
            if self.is_reader:
                self.redirect(self.context_url + '/listing_reader')
                return
            self.redirect(self.portal_url)
            return

        if DEBUG:
            print(" ")

        self.library_zk_obj = self.library
        if self.library_zk_obj is None and self.is_ziskej_operator:
            sigla_str = self.text_from_request('sigla')
            if DEBUG:
                print("sigla_str:", sigla_str)
            if sigla_str is not None:
                self.library_zk_obj = self.library_by_sigla(sigla_str)

        self.reader_lid_label = u'Čtenář ID'
        if self.library:
            reader_lid_label = self.library.reader_lid_label
            if reader_lid_label:
                self.reader_lid_label = reader_lid_label

        search_is_reader = u''
        search_service = u''

        filter_id = self.text_from_request('filter')
        filter_rs_id = self.text_from_request('filter_rs')
        if DEBUG:
            print("filter_id:", filter_id)
            print("filter_rs_id:", filter_rs_id)

        # list
        is_st_for_t = False  # Hledat subtickets a pak převést na tickets
        query = dict(
            path = '/'.join(self.context.getPhysicalPath()),
            portal_type = TICKET_PORTAL_TYPES,
            sort_on = 'hid',
            sort_order = 'reverse',
            # Neomezovat třídění, filtrování je nejen na úrovni query
            # sort_limit = self.batch_end,  
            )

        self.title = u'Objednávky'
        if self.library_zk_obj:
            sigla = self.library_zk_obj.sigla
            self.description = u'Zobrazují se objednávky žádající knihovny {sigla}.\n'.format(sigla=sigla)
            query['library_zk'] = sigla.lower()
        else:
            sigla = None
            self.description = u'Zobrazují se objednávky všech knihoven.\n'

        filter_todo_ids = ['created', 'assigned', 'prepared', 'pending', ]
        filter_open_ids = [x for x in filter_todo_ids]
        #filter_open_ids.extend([])
        filter_closed_ids = ['closed', 'rejected', 'cancelled', ]
        filter_rs_ids = [x for x in filter_open_ids]
        filter_rs_ids.extend(filter_closed_ids)
        self.filter_rs_data = []

        self.filter_rs_data.append(
            dict(
                id = "",
                title = u"",
                readonly = True,
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "",
                title = u"Neuzavřené:",
                readonly = True,
                )
            )

        for x in filter_rs_ids:
            # if x == 'cancelled':
            #     continue
            if x == 'closed':
                self.filter_rs_data.append(
                    dict(
                        id = "",
                        title = u"",
                        readonly = True,
                        )
                    )
                self.filter_rs_data.append(
                    dict(
                        id = "",
                        title = u"<strong>Uzavřené:</strong>",
                        readonly = True,
                        )
                    )
            self.filter_rs_data.append(
                dict(
                    id = x,
                    title = self.ticket_get_review_state_label(x),
                    )
                )

        self.filter_rs_data.append(
            dict(
                id = "",
                title = u"",
                readonly = True,
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint",
                title = u"Obsahuje požadavek, který byl reklamován",
                description = u"Obsahuje požadavek, který byl reklamován, zahrnuje všechny možnosti reklamace (přijatá reklamace, nepřijatá reklamace, nerozhodnutá reklamace, nerozhodnutá námitka).",
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint-undecided",
                title = u" - probíhající reklamace",
                description = u"Obsahuje požadavek, který byl reklamován a zatím nebylo rozhodnuto o reklamaci.",
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint-accepted",
                title = u" - přijaté reklamace - vrácení peněz",
                description = u"Obsahuje požadavek, který byl reklamován a reklamace byla přijata (resp. byla přijata námitka Servisním centrem).",
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint-accepted-soft",
                title = u" - přijaté reklamace - bez vrácení peněz",
                description = u"Obsahuje požadavek, který byl reklamován a reklamace byla přijata. Nepožaduje se vrácení pěněz, ale nové dodání.",
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint-declined",
                title = u" - odmítnuté reklamace",
                description = u"Obsahuje požadavek, který byl reklamován a reklamace nebyla přijata (resp. nebyla přijata ani námitka Servisním centrem).",
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint-objected",
                title = u" - probíhající námitka",
                description = u"Obsahuje požadavek, který byl reklamován a reklamace byla odmítnuta, následně byla podána námitka k Servisnímu centru, o které zatím nebylo rozhodnuto."
                )
            )

        if filter_id == 'rs-todo':
            query['review_state'] = filter_todo_ids
            self.description += u'<br>\nFiltr: Vyžadována reakce.\n'
        elif filter_id == 'rs-closed':
            query['review_state'] = filter_closed_ids
            self.description += u'<br>\nFiltr: Uzavřené objednávky.\n'
        elif filter_rs_id and filter_rs_id.startswith('rs-cancel-'):
            query['review_state'] = 'cancelled'
            if filter_rs_id == 'rs-cancel-all':
                self.description += u'<br>\nFiltr: Stornované, protože byla přijata reklamace, nebo protože byl požadavek stornován.\n'
            elif filter_rs_id == 'rs-cancel-complaint-accepted':
                query['complaint_state'] = ('accepted', 'objection_accepted', )
                self.description += u'<br>\nFiltr: Stornované, protože byla přijata reklamace.\n'
            elif filter_rs_id == 'rs-cancel-other':
                self.description += u'<br>\nFiltr: Ostatní stornované (nebyla přijata reklamace).\n'
        elif filter_rs_id and filter_rs_id.startswith('rs-complaint'):
            # Reklamace v tomto kontextu znamenají malé reklamace, tedy
            # reklamace požadavku.  Je potřeba tedy najít příslušné požadavky
            # a následně převést na objednávky.  Restrikce na ŽK je stejná, není
            # potřeba nic měnit.
            is_st_for_t = True
            query['portal_type'] = SUBTICKET_PORTAL_TYPES
            if filter_rs_id == 'rs-complaint':
                query['complaint_state'] = (
                    'new', 'accepted', 'declined', 'objected',
                    'objection_accepted', 'objection_declined', )
                self.description += u'<br>\nFiltr: Obsahuje požadavek, který byl reklamován, zahrnuje všechny možnosti reklamace (přijatá reklamace, nepřijatá reklamace, nerozhodnutá reklamace, nerozhodnutá námitka).\n'
            elif filter_rs_id == 'rs-complaint-undecided':
                query['complaint_state'] = 'new'
                self.description += u'<br>\nFiltr: Obsahuje požadavek, který byl reklamován a zatím nebylo rozhodnuto o reklamaci.\n'
            elif filter_rs_id == 'rs-complaint-accepted':
                query['review_state'] = 'cancelled'
                query['complaint_state'] = ('accepted', 'objection_accepted', )
                self.description += u'<br>\nFiltr: Obsahuje požadavek, který byl reklamován a reklamace byla přijata (resp. byla přijata námitka Servisním centrem).\n'
            elif filter_rs_id == 'rs-complaint-accepted-soft':
                # query['complaint_state'] = ''
                query['complaint_accepted_is_soft'] = True
                
                self.description += u'<br>\nFiltr: Obsahuje požadavek, který byl reklamován a reklamace byla přijata (resp. byla přijata námitka Servisním centrem).\n'
            elif filter_rs_id == 'rs-complaint-declined':
                query['complaint_state'] = ('declined', 'objection_declined', )
                self.description += u'<br>\nFiltr: Obsahuje požadavek, který byl reklamován a reklamace nebyla přijata (resp. nebyla přijata ani námitka Servisním centrem).\n'
            elif filter_rs_id == 'rs-complaint-objected':
                query['complaint_state'] = 'objected'
                self.description += u'<br>\nFiltr: Obsahuje požadavek, který byl reklamován a reklamace byla odmítnuta, následně byla podána námitka k Servisnímu centru, o které zatím nebylo rozhodnuto.\n'

        elif filter_rs_id in filter_rs_ids:
            query['review_state'] = filter_rs_id
            self.description += u'<br>\nFiltr: Objednávky ve stavu {review_state_label}.\n'.format(review_state_label=self.ticket_get_review_state_label(filter_rs_id))
        elif filter_id == 'rs-decide':
            query['complaint_state'] = 'objected'
            self.description += u'<br>\nFiltr: Vyžadováno rozhodnutí SC.\n'
        else:
            query['review_state'] = filter_open_ids
            self.description += u'<br>\nFiltr: Neuzavřené objednávky.\n'

        submit_search = self.text_from_request('submit_search')
        if DEBUG:
            print("submit_search:", submit_search)
        if submit_search:
            search_id = self.text_from_request('search_id')
            search_sigla_dk = self.text_from_request('search_sigla_dk')
            search_sigla_dk_valid = self.valid_sigla(search_sigla_dk)
            search_fullname_dk = self.text_from_request('search_fullname_dk')
            search_reader_username = self.text_from_request('search_reader_username')
            search_reader_lid = self.text_from_request('search_reader_lid')
            search_is_reader = self.text_from_request(
                'search_is_reader', lov=(u'', u'true', u'false'))
            search_service = self.text_from_request(
                'search_service', lov=(u'', u'mvs', u'edd'))
            if DEBUG:
                print("search_id:", search_id)
                print("search_sigla_dk:", search_sigla_dk)
                print("search_sigla_dk_valid:", search_sigla_dk_valid)
                print("search_fullname_dk:", search_fullname_dk)
                print("search_reader_username:", search_reader_username)
                print("search_reader_lid:", search_reader_lid)
                print("search_is_reader:", search_is_reader)
                print("search_service:", search_service)

            if search_sigla_dk_valid:
                query['library_dk'] = search_sigla_dk_valid
                self.description += u'<br>\nHledání: Dožádaná knihovna {}.\n'.format(search_sigla_dk)
            elif search_fullname_dk:
                sigla_dks = self._get_siglas_by_fullname(search_fullname_dk)
                if DEBUG:
                    print("sigla_dks", sigla_dks)
                if not sigla_dks:
                    self.add_message(u'Knihovna nenalezena')
                # elif len(sigla_dks) > 16:
                #     self.add_message(u'Nalezeno příliš mnoho knihoven, prosím upřesněte své hledání.')
                #     sigla_dks = None
                if sigla_dks:
                    query['library_dk'] = [x.lower() for x in sigla_dks if x]
                    self.description += u'<br>\nHledání: Dožádané knihovny s názvem obsahujícím výraz <em>{}</em>, počet nalezených knihoven {}.\n'.format(
                        search_fullname_dk,
                        len(sigla_dks),
                        )

            if search_reader_username:
                query['reader'] = search_reader_username
                self.description += u'<br>\nHledání: Čtenář uživatelské jméno {}.\n'.format(search_reader_username)

            if search_reader_lid:
                query['reader_lid'] = search_reader_lid
                self.description += u'<br>\nHledání: {reader_lid_label} {search_reader_lid}.\n'.format(reader_lid_label=self.reader_lid_label, search_reader_lid=search_reader_lid)

            if search_is_reader == 'true':
                # Filtr je aplikován dodatečně
                self.description += u'<br>\nHledání: Vytvořené samoobslužně čtenářem.\n'
            elif search_is_reader == 'false':
                # Filtr je aplikován dodatečně
                self.description += u'<br>\nHledání: Vytvořené knihovníkem.\n'

            if search_service == 'mvs':
                # Filtr je aplikován dodatečně
                self.description += u'<br>\nHledání: Typ objednávky: MVS\n'
            elif search_service == 'edd':
                # Filtr je aplikován dodatečně
                self.description += u'<br>\nHledání: Typ objednávky: EDD\n'

            # jakmile je search_id, tak zapomenout vše z query a použít vlastní
            if search_id:
                try:
                    search_id = int(search_id)
                except:
                    pass
                query = dict(
                    path = '/'.join(self.context.getPhysicalPath()),
                    portal_type = TICKET_PORTAL_TYPES,
                    hid = search_id,
                    )
                self.description = u'Hledání: HID {}'.format(search_id)
                if self.is_librarian:
                    query['library_zk'] = self.library.sigla.lower()

        if query:
            brains = self.catalog(query)
        else:
            brains = []
        self.tickets = []

        if DEBUG or DEBUG_LEN_CATALOG_QUERY:
            print(len(brains), "for query", query)

        # Pro hledání (dolní formulář) ještě navíc informovat o tom, že nebylo
        # nic nalezeno.
        if submit_search and search_id and len(brains) == 0:
            message = u'Objednávka ID {hid} nenalezena.'.format(hid=search_id)
            self.add_message(message)
        elif submit_search and len(brains) == 0:
            message = u'Nenalezena žádná objednávka.'
            self.add_message(message)

        # Pro hledání podle ID objednávky (číselná část) přímo přesměrovat na
        # výsledek
        if submit_search and search_id and len(brains) == 1:
            brain = brains[0]
            url = brain.getURL()

            message = u'Objednávka ID {hid} nalezena.'.format(hid=search_id)
            self.add_message(message)

            self.redirect(url)
            return

        if filter_rs_id and filter_rs_id.startswith('rs-complaint'):
            count_open = 0
            count_closed = 0

        self.tickets_len = 0
        batch_idx = 0
        for brain in brains:

            if is_st_for_t:
                st_brain = brain
                t_brains = self.catalog(dict(
                    portal_type=TICKET_PORTAL_TYPES,
                    hid=st_brain.hid,
                    ))
                if not t_brains:
                    continue
                brain = t_brains[0]

            if brain.subticket_id:
                subticket_url = brain.getURL() + u'/' + brain.subticket_id
            else:
                subticket_url = u''

            library_zk_title = brain.library_zk_title
            if DEBUG and not library_zk_title:
                print("library_zk_title for", brain.hid, ":", repr(library_zk_title))
            if library_zk_title is None:
                library_zk_title = u''

            library_dk_title = brain.library_dk_title
            if DEBUG and not library_dk_title and brain.library_dk:
                print("library_dk_title for", brain.hid, ":", repr(library_dk_title))
            if library_dk_title is None:
                library_dk_title = u''

            css = u''
            unread = False
            if self.is_librarian:
                unread = brain.unread_zk
            if unread:
                css += u'unread'

            url = brain.getURL()

            # FIXME
            obj = brain.getObject()

            title = brain.doc_title
            if not title:
                title = brain.doc_fullname
            # TypeError: object of type 'Missing.Value' has no len() for 
            # /tickets/c61f2aab89a84334
            try:
                len(title)
            except:
                title = u''
            if len(title) > 51:
                label = title
                idx = title.find('.')
                if idx > 0 and idx < len(title) * 2/3:
                    label = title[idx+1:idx+48+1]
                else:
                    idx = title.find(' ', 48)
                    if idx > 0 and idx < len(title) * 4/5:
                        label = title[:idx]
                    else:
                        label = title[:48+1]
                title = u'<span title="{title}">{label}... ' \
                    u'<i aria-hidden="true" class="fa fa-info-circle" ' \
                    u'title="{title}"></i><span>'.format(
                        title=title, label=label)

            is_created_by_cpk_for_reader = obj.is_created_by_cpk_for_reader()

            # Dodatečný filtr
            if search_is_reader == 'true' and not is_created_by_cpk_for_reader:
                continue
            if search_is_reader == 'false' and is_created_by_cpk_for_reader:
                continue

            # Dodatečný filtr
            if search_service == 'mvs' and not obj.is_mvs:
                continue
            elif search_service == 'edd' and not obj.is_edd:
                continue

            subticket = None
            date_back_raw = u''
            date_back = u''
            subticket_complaint_cancel_label = u''

            if obj.subticket_id:
                subticket = getattr(obj, obj.subticket_id, None)
            if subticket and subticket.back_date:
                date_back_raw = subticket.back_date
                date_back = self.display_date(date_back_raw, is_time=False)

            wf_status = obj.wf_status
            ticket_review_state = wf_status['review_state']
            ticket_review_state_label = get_ticket_wf_label(ticket_review_state, lower=True, short=True)
            subticket_review_state = wf_status['subticket_review_state']
            if subticket_review_state:
                subticket_review_state_label = get_subticket_wf_label(subticket_review_state, lower=True, short=True)
            else:
                subticket_review_state_label = u''
            if subticket:
                subticket_complaint_cancel_label = subticket.complaint_cancel_label

            if ticket_review_state in ('created', ) and unread:
                css += u' unread-new'
            
            if self.is_ziskej_operator:
                if brain.complaint_state == 'objected':
                    css += u' unread'
                    if brain.unread_op:
                        css += u' unread-new'

            librarian = obj.librarian
            if not librarian:
                librarian = u''

            pm_r_unread = bool(obj.reader and obj.pm_unread == 'library_zk')
            pm_dk_unread = 0
            for subticket_id in obj.subticket_ids:
                one = getattr(obj, subticket_id, None)
                if not one:
                    continue
                if one.pm_unread == 'library_zk':
                    pm_dk_unread += 1

            if obj.reader_lid:
                reader_lid = obj.reader_lid
                reader_lid_short = reader_lid[:12]
                if len(reader_lid) > 12:
                    reader_lid_short += u'...'
            else:
                reader_lid = u''
                reader_lid_short = u''

            reader_fullname = obj.reader_fullname

            subtickets_count = len(obj.subticket_ids)

            substate_label = u''
            if is_created_by_cpk_for_reader and brain.review_state == 'created':
                if obj.is_paid_in_advance():
                    substate_label = u'(zaplacená)'
                else:
                    substate_label = u'(nezaplacená)'

            if batch_idx >= self.batch_start and batch_idx < self.batch_end:
                self.tickets.append(
                    dict(
                        hid = brain.hid,
                        hid_prefix = obj.hid_prefix,
                        shortname = brain.shortname,
                        url = url,
                        ticket_review_state = ticket_review_state,
                        ticket_review_state_label = ticket_review_state_label,
                        subticket_review_state = subticket_review_state,
                        subticket_review_state_label = subticket_review_state_label,
                        subticket_url = subticket_url,
                        library_zk_title = library_zk_title,
                        library_dk_title = library_dk_title,
                        library_dk_sigla = obj.library_dk and obj.library_dk.upper() or u'',
                        na_title = brain.next_action_title,
                        na_date = self.display_date(brain.next_action_date),
                        date_created = self.display_date(brain.CreationDate, is_time=False),
                        date_created_raw = str(brain.CreationDate)[:10],
                        #date_modified = self.display_date(brain.ModificationDate, is_time=False),
                        date_reader = self.display_date(obj.reader_date, is_time=False),
                        date_reader_raw = obj.reader_date or u'',
                        date_back = date_back,
                        date_back_raw = date_back_raw,
                        unread = unread,
                        css = css,
                        librarian = librarian,
                        doc_title = title,
                        reader = reader_fullname,
                        reader_lid = reader_lid,
                        reader_lid_short = reader_lid_short,
                        is_created_by_cpk_for_reader = is_created_by_cpk_for_reader,
                        pm_r_unread = pm_r_unread,
                        pm_dk_unread = pm_dk_unread,
                        subtickets_count = subtickets_count,
                        complaint_cancel_label = obj.complaint_cancel_label,
                        subticket_complaint_cancel_label = subticket_complaint_cancel_label,
                        substate_label = substate_label,
                        )
                    )

            batch_idx += 1
            self.tickets_len += 1

            if filter_rs_id and filter_rs_id.startswith('rs-complaint'):
                if ticket_review_state in filter_closed_ids:
                    count_closed += 1
                else:
                    count_open += 1

        # self.tickets_len = len(self.tickets)
        self.set_batch()

        if DEBUG:
            print(self.tickets_len, "tickets")
            print(" ")

        # if DEBUG:
        #     print('self.tickets[:1]')
        #     pprint(self.tickets[:1])
        #     print(' ')

        if 'sort_order' in query:
            del query['sort_order']
        if 'sort_on' in query:
            del query['sort_on']
        if 'library_dk' in query:
            del query['library_dk']

        # Přidat čísla pro všechny filtry
        self.filter_counter = dict()
        self.filter_counter_str = dict()
        if filter_rs_id and filter_rs_id.startswith('rs-complaint'):
            query['portal_type'] = TICKET_PORTAL_TYPES
        if filter_id is None and filter_rs_id is not None:
            pass
        elif filter_id is None:
            self.filter_counter['rs-open'] = self.tickets_len
        else:
            self.filter_counter[filter_id] = self.tickets_len
        # fids = list(filter_rs_ids)
        # fids.extend(['rs-todo', 'rs-closed', 'rs-decide'])
        fids = ['rs-open', 'rs-closed', 'rs-todo', 'rs-complaint']
        if self.is_ziskej_operator:
            fids.append('rs-decide')
        if DEBUG:
            print("fids", fids)
        for fid in fids:
            is_active_fid = bool(fid in self.filter_counter)
            if 'review_state' in query:
                del query['review_state']
            if 'complaint_state' in query:
                del query['complaint_state']

            if fid == 'rs-open':
                query['review_state'] = filter_open_ids
            elif fid == 'rs-closed':
                query['review_state'] = filter_closed_ids
            elif fid == 'rs-todo':
                query['review_state'] = filter_todo_ids
            elif fid == 'rs-decide':
                query['complaint_state'] = 'objected'
            else:
                logger.warning('Unknown fid {} for filter_counter'.format(fid))
                continue

            brains = self.catalog(query)
            if DEBUG:
                print(fid, len(brains), 'for query', query)
            self.filter_counter[fid] = len(brains)
            if is_active_fid:
                if self.filter_counter[fid] == self.tickets_len:
                    self.tickets_len_str = self.tickets_len
                else:
                    self.tickets_len_str = u'{}/{}'.format(
                        self.tickets_len,
                        self.filter_counter[fid],
                        )
            elif fid in ('rs-open', 'rs-closed', ) and filter_rs_id and \
                    filter_rs_id.startswith('rs-complaint'):
                if fid == 'rs-open':
                    count = count_open
                elif fid == 'rs-closed':
                    count = count_closed
                else:
                    count = -1  # Nemělo by nastat
                self.filter_counter_str[fid] = u'{}/{}'.format(
                    count,
                    self.filter_counter[fid],
                    )
            elif 'review_state' not in query or not filter_rs_id or \
                    filter_rs_id not in query['review_state'] or \
                    self.filter_counter[fid] == self.tickets_len:
                self.filter_counter_str[fid] = u'{}'.format(
                    self.filter_counter[fid],
                    )
            else:
                self.filter_counter_str[fid] = u'{}/{}'.format(
                    self.tickets_len,
                    self.filter_counter[fid],
                    )

        if DEBUG:
            print(" ")
            print("self.filter_counter")
            print(self.filter_counter)
            print(" ")
            print("self.filter_counter_str")
            print(self.filter_counter_str)
            print(" ")


class TicketListingDKView(TicketListingBaseView):

    template = ViewPageTemplateFile('ticket_listing_dk.pt')

    def call(self):
        super(TicketListingDKView, self).call()
        self.filter_rs_id = None

        self.context_url_view = self.context_url + '/listing_dk'

        self.unauthorized = True
        if self.is_anon:
            self.redirect(self.portal_url)
            return
        if not self.is_librarian and not self.is_ziskej_operator:
            if self.is_reader:
                self.redirect(self.context_url + '/listing_reader')
                return
            self.redirect(self.portal_url)
            return
        self.unauthorized = False

        if DEBUG:
            print(" ")

        search_service = u''

        self.library_dk_obj = self.library
        if self.library_dk_obj is None and self.is_ziskej_operator:
            sigla_str = self.text_from_request('sigla')
            if DEBUG:
                print("sigla_str:", sigla_str)
            self.library_dk_obj = self.library_by_sigla(sigla_str)

        filter_id = self.text_from_request('filter')
        filter_rs_id = self.text_from_request('filter_rs')
        if DEBUG:
            print("filter_id:", filter_id)
            print("filter_rs_id:", filter_rs_id)

        # Pro stav cancelled zobrazujeme speciální nápovědy atp., nicméně
        # z grafu na titulce je odkaz striktně na stav, proto imitovat výběr
        # ze selectu z fronty požadavků.
        if filter_rs_id == 'cancelled':
            filter_rs_id = 'rs-cancel-all'
            if DEBUG:
                print("filter_rs_id changed to:", filter_rs_id)
        self.filter_rs_id = filter_rs_id  # pro pt, aby reflektovalo tuto změnu

        # list
        query = dict(
            path = '/'.join(self.context.getPhysicalPath()),
            portal_type = SUBTICKET_PORTAL_TYPES,
            sort_on = 'hid',
            sort_order = 'reverse',
            # Neomezovat třídění, filtrování je nejen na úrovni query
            # sort_limit = self.batch_end,  
            )

        self.title = u'Požadavky DK'
        if self.library_dk_obj:
            sigla = self.library_dk_obj.sigla
            self.description = u'Zobrazují se požadavky dožádané knihovny {sigla}.\n'.format(sigla=sigla)
            query['library_dk'] = sigla.lower()
        else:
            sigla = None
            self.description = u'Zobrazují se požadavky všech knihoven.\n'

        filter_todo_ids = ['queued', 'accepted', 'sent_back', ]
        filter_open_ids = [x for x in filter_todo_ids]
        filter_open_ids.extend(['conditionally_accepted', 'sent', ])
        filter_closed_ids = ['closed', 'cancelled', 'refused', ]
        filter_rs_ids = [x for x in filter_open_ids]
        filter_rs_ids.extend(filter_closed_ids)
        self.filter_rs_data = []

        self.filter_rs_data.append(
            dict(
                id = "",
                title = u"",
                readonly = True,
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "",
                title = u"Neuzavřené:",
                readonly = True,
                )
            )

        for x in filter_rs_ids:
            if x == 'cancelled':
                continue
            if x == 'closed':
                self.filter_rs_data.append(
                    dict(
                        id = "",
                        title = u"",
                        readonly = True,
                        )
                    )
                self.filter_rs_data.append(
                    dict(
                        id = "",
                        title = u"<strong>Uzavřené:</strong>",
                        readonly = True,
                        )
                    )
            self.filter_rs_data.append(
                dict(
                    id = x,
                    title = self.subticket_get_review_state_label(x),
                    )
                )

        # self.filter_rs_data.append(
        #     dict(
        #         id = "",
        #         title = u"",
        #         readonly = True,
        #         )
        #     )
        self.filter_rs_data.append(
            dict(
                id = "rs-cancel-all",
                title = u"Stornováno",
                description = u"Všechny stornované požadavky."
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-cancel-other",
                title = u" - běžně stornované",
                description = u"Běžně stornované."
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-cancel-sc",
                title = u" - odebrané požadavky",
                description = u"Stornované, protože bylo odebráno Servisním centrem."
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-cancel-complaint-accepted",
                title = u" - přijaté reklamace",
                description = u"Stornované, protože byla přijata reklamace."
                )
            )

        # NTK-19
        # 3. Mezi textem ostatní stornované a Reklamováno odstranit mezeru.
        # FIXME diskutovat
        # self.filter_rs_data.append(
        #     dict(
        #         id = "",
        #         title = u"",
        #         readonly = True,
        #         )
        #     )

        self.filter_rs_data.append(
            dict(
                id = "rs-complaint",
                title = u"Reklamováno",
                description = u"Reklamováno, zahrnuje všechny možnosti reklamace (přijatá reklamace, nepřijatá reklamace, nerozhodnutá reklamace, nerozhodnutá námitka).",
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint-undecided",
                title = u" - probíhající reklamace",
                description = u"Reklamováno a zatím nebylo rozhodnuto o reklamaci.",
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint-accepted",
                title = u" - přijaté reklamace",
                description = u"Reklamováno a reklamace byla přijata (resp. byla přijata námitka Servisním centrem).",
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint-declined",
                title = u" - odmítnuté reklamace",
                description = u"Reklamováno a reklamace nebyla přijata (resp. nebyla přijata ani námitka Servisním centrem).",
                )
            )
        self.filter_rs_data.append(
            dict(
                id = "rs-complaint-objected",
                title = u" - probíhající námitka",
                description = u"Reklamováno, reklamace byla odmítnuta, následně byla podána námitka k Servisnímu centru, o které zatím nebylo rozhodnuto."
                )
            )

        if filter_id == 'rs-todo':
            query['review_state'] = filter_todo_ids
            self.description += u'<br>\nFiltr: Vyžadována reakce.\n'
        elif filter_id == 'rs-closed':
            query['review_state'] = filter_closed_ids
            self.description += u'<br>\nFiltr: Uzavřené požadavky.\n'
        elif filter_rs_id and filter_rs_id.startswith('rs-cancel-'):
            query['review_state'] = 'cancelled'
            if filter_rs_id == 'rs-cancel-all':
                self.description += u'<br>\nFiltr: Stornované, vč. stornovaných z důvodu přijaté reklamace nebo odebraných Servisním centrem.\n'
            elif filter_rs_id == 'rs-cancel-complaint-accepted':
                query['complaint_state'] = ('accepted', 'objection_accepted', )
                self.description += u'<br>\nFiltr: Stornované, protože byla přijata reklamace.\n'
            elif filter_rs_id == 'rs-cancel-sc':
                self.description += u'<br>\nFiltr: Stornované, protože bylo odebráno Servisním centrem.\n'
            elif filter_rs_id == 'rs-cancel-other':
                self.description += u'<br>\nFiltr: Běžně stornované.\n'
        elif filter_rs_id and filter_rs_id.startswith('rs-complaint'):
            if filter_rs_id == 'rs-complaint':
                query['complaint_state'] = (
                    'new', 'accepted', 'declined', 'objected',
                    'objection_accepted', 'objection_declined', )
                self.description += u'<br>\nFiltr: Reklamováno, zahrnuje všechny možnosti reklamace (přijatá reklamace, nepřijatá reklamace, nerozhodnutá reklamace, nerozhodnutá námitka).\n'
            elif filter_rs_id == 'rs-complaint-undecided':
                query['complaint_state'] = 'new'
                self.description += u'<br>\nFiltr: Reklamováno a zatím nebylo rozhodnuto o reklamaci.\n'
            elif filter_rs_id == 'rs-complaint-accepted':
                query['review_state'] = 'cancelled'
                query['complaint_state'] = ('accepted', 'objection_accepted', )
                self.description += u'<br>\nFiltr: Reklamováno a reklamace byla přijata (resp. byla přijata námitka Servisním centrem).\n'
            elif filter_rs_id == 'rs-complaint-declined':
                query['complaint_state'] = ('declined', 'objection_declined', )
                self.description += u'<br>\nFiltr: Reklamováno a reklamace nebyla přijata (resp. nebyla přijata ani námitka Servisním centrem).\n'
            elif filter_rs_id == 'rs-complaint-objected':
                query['complaint_state'] = 'objected'
                self.description += u'<br>\nFiltr: Reklamováno, reklamace byla odmítnuta, následně byla podána námitka k Servisnímu centru, o které zatím nebylo rozhodnuto.\n'
        elif filter_rs_id in filter_rs_ids:
            query['review_state'] = filter_rs_id
            self.description += u'<br>\nFiltr: Požadavky ve stavu {review_state_label}.\n'.format(review_state_label=self.subticket_get_review_state_label(filter_rs_id))
        elif filter_id == 'rs-decide':
            query['complaint_state'] = 'objected'
            self.description += u'<br>\nFiltr: Vyžadováno rozhodnutí SC.\n'
        else:
            query['review_state'] = filter_open_ids
            self.description += u'<br>\nFiltr: Neuzavřené požadavky.\n'

        submit_search = self.text_from_request('submit_search')
        if submit_search:
            search_id = self.text_from_request('search_id')
            search_sigla_zk = self.text_from_request('search_sigla_zk')
            search_sigla_zk_valid = self.valid_sigla(search_sigla_zk)
            search_fullname_zk = self.text_from_request('search_fullname_zk')
            search_service = self.text_from_request(
                'search_service', lov=(u'', u'mvs', u'edd'))
            if DEBUG:
                print("search_id:", search_id)
                print("search_sigla_zk:", search_sigla_zk)
                print("search_sigla_zk_valid:", search_sigla_zk_valid)
                print("search_fullname_zk:", search_fullname_zk)
                print("search_service:", search_service)

            if search_sigla_zk_valid:
                query['library_zk'] = search_sigla_zk_valid
                self.description += u'<br>\nHledání: Žádající knihovna {}.\n'.format(search_sigla_zk)
            elif search_fullname_zk:
                sigla_zks = self._get_siglas_by_fullname(search_fullname_zk)
                if DEBUG:
                    print("sigla_zks", sigla_zks)
                if not sigla_zks:
                    self.add_message(u'Knihovna nenalezena')
                # elif len(sigla_zks) > 16:
                #     self.add_message(u'Nalezeno příliš mnoho knihoven, prosím upřesněte své hledání.')
                #     sigla_zks = None
                if sigla_zks:
                    query['library_zk'] = [x.lower() for x in sigla_zks if x]
                    self.description += u'<br>\nHledání: Žádající knihovny s názvem obsahujícím výraz <em>{}</em>, počet nalezených knihoven {}.\n'.format(
                        search_fullname_zk,
                        len(sigla_zks),
                        )

            if search_service == 'mvs':
                # Filtr je aplikován dodatečně
                self.description += u'<br>\nHledání: Typ požadavku: MVS\n'
            elif search_service == 'edd':
                # Filtr je aplikován dodatečně
                self.description += u'<br>\nHledání: Typ požadavku: EDD\n'

            # jakmile je search_id, tak zapomenout vše z query a použít vlastní
            if search_id:
                try:
                    search_id = int(search_id)
                except:
                    pass
                query = dict(
                    path = '/'.join(self.context.getPhysicalPath()),
                    portal_type = SUBTICKET_PORTAL_TYPES,
                    hid = search_id,
                    )
                self.description = u'Hledání: HID {}'.format(search_id)
                if self.is_librarian:
                    query['library_dk'] = self.library.sigla.lower()

        if query:
            brains = self.catalog(query)
        else:
            brains = []
        if DEBUG or DEBUG_LEN_CATALOG_QUERY:
            print(len(brains), "for query", query)

        # Pro hledání (dolní formulář) ještě navíc informovat o tom, že nebylo
        # nic nalezeno.
        if submit_search and search_id and len(brains) == 0:
            message = u'Požadavek ID {hid} nenalezena.'.format(hid=search_id)
            self.add_message(message)
        elif submit_search and len(brains) == 0:
            message = u'Nenalezen žádný požadavek.'
            self.add_message(message)

        # Pro hledání podle ID požadavku (číselná část) přímo přesměrovat na
        # výsledek
        if submit_search and search_id and len(brains) == 1:
            brain = brains[0]
            url = brain.getURL()

            message = u'Požadavek ID {hid} nalezen.'.format(hid=search_id)
            self.add_message(message)

            self.redirect(url)
            return

        if filter_rs_id in ('rs-complaint', 'rs-complaint-declined', ):
            count_open = 0
            count_closed = 0
        self.subtickets = []
        self.subtickets_len = 0
        batch_idx = 0
        for brain in brains:

            subticket = brain.getObject()  # FIXME

            if filter_rs_id == 'rs-cancel-sc' and not subticket.is_wf_cancel_by_operator:
                continue
            elif filter_rs_id == 'rs-cancel-other':
                if subticket.is_wf_cancel_by_operator:
                    continue
                elif subticket.complaint_state in (
                        'accepted', 'objection_accepted', ):
                    continue

            ticket = subticket.__parent__ 
            if not ticket:
                # nenastane a pokud, tak je to chyba, která si vyžaduje pozornost
                logger.error('subticket browser view: no parent for ' + str(subticket))
                self.redirect(self.portal_url)

            # Dodatečný filtr
            if search_service == 'mvs' and not ticket.is_mvs:
                continue
            elif search_service == 'edd' and not ticket.is_edd:
                continue

            ticket_url = brain.getURL()
            ticket_url = ticket_url[:ticket_url.rfind('/')]

            library_zk_title = brain.library_zk_title
            if not library_zk_title:
                print("library_zk_title for", brain.hid, ":", repr(library_zk_title))
            if library_zk_title is None:
                library_zk_title = u''

            library_dk_title = brain.library_dk_title
            if not library_dk_title:
                print("library_dk_title for", brain.hid, ":", repr(library_dk_title))
            if library_dk_title is None:
                library_dk_title = u''

            css = u''
            unread = False
            if self.is_librarian:
                unread = brain.unread_dk
            if unread:
                css += u'unread'

            if DEBUG:
                if unread:
                    print("unread: " + str(brain.hid))
                if str(brain.hid) == '100267':
                    print("hid: " + str(brain.hid))
                    print("unread_dk: " + str(subticket.unread_dk))
                    main_subticket = getattr(ticket, ticket.subticket_id, None)
                    if main_subticket:
                        print("main subticket unread_dk: " + str(main_subticket.unread_dk))
                    else:
                        print("main subticket unread_dk: NONE")

            url = brain.getURL()

            date_back = self.display_date(subticket.back_date, is_time=False)

            ticket_wf_status = ticket.wf_status
            ticket_review_state = ticket_wf_status['review_state']
            ticket_review_state_label = get_ticket_wf_label(ticket_review_state, lower=True, short=True)
            subticket_wf_status = subticket.wf_status
            subticket_review_state = subticket_wf_status['review_state']
            subticket_review_state_label = get_subticket_wf_label(subticket_review_state, lower=True, short=True)

            if subticket_review_state in ('queued', ) and unread:
                css += u' unread-new'

            if self.is_ziskej_operator:
                if brain.complaint_state == 'objected':
                    css += u' unread'
                    if brain.unread_op:
                        css += u' unread-new'

            librarian = subticket.librarian
            if not librarian:
                librarian = u''

            title = brain.doc_title
            if not title:
                title = brain.doc_fullname
            # TypeError: object of type 'Missing.Value' has no len() for 
            # /tickets/c61f2aab89a84334
            try:
                len(title)
            except:
                title = u''
            if len(title) > 51:
                label = title
                idx = title.find('.')
                if idx > 0 and idx < len(title) * 2/3:
                    label = title[idx+1:idx+48+1]
                else:
                    idx = title.find(' ', 48)
                    if idx > 0 and idx < len(title) * 4/5:
                        label = title[:idx]
                    else:
                        label = title[:48+1]
                title = u'<span title="{title}">{label}... ' \
                    u'<i aria-hidden="true" class="fa fa-info-circle" ' \
                    u'title="{title}"></i><span>'.format(
                        title=title, label=label)

            pm_zk_unread = bool(subticket.pm_unread == 'library_dk')
            # Pro operátora označit existenci nepřečtených zpráv i z druhé
            # strany
            if self.is_ziskej_operator:
                pm_zk_unread = pm_zk_unread or \
                    bool(subticket.pm_unread == 'library_zk')

            if batch_idx >= self.batch_start and batch_idx < self.batch_end:
                self.subtickets.append(
                    dict(
                        hid = brain.hid,
                        hid_prefix = subticket.hid_prefix,
                        shortname = brain.shortname,
                        url = url,
                        ticket_review_state = ticket_review_state,
                        ticket_review_state_label = ticket_review_state_label,
                        subticket_review_state = subticket_review_state,
                        subticket_review_state_label = subticket_review_state_label,
                        ticket_url = ticket_url,
                        library_zk_title = library_zk_title,
                        library_dk_title = library_dk_title,
                        library_zk_sigla = subticket.library_zk and subticket.library_zk.upper() or u'',
                        library_dk_sigla = subticket.library_dk and subticket.library_dk.upper() or u'',
                        na_title = brain.next_action_title,
                        na_date = self.display_date(brain.next_action_date),
                        date_created = self.display_date(brain.CreationDate, is_time=False),
                        date_created_raw = str(brain.CreationDate)[:10],
                        #date_modified = self.display_date(brain.ModificationDate, is_time=False),
                        date_reader = self.display_date(ticket.reader_date, is_time=False),
                        date_reader_raw = ticket.reader_date or u'',
                        date_back = date_back,
                        back_date_row = subticket.back_date or u'',
                        unread = unread,
                        css = css,
                        librarian = librarian,
                        doc_title = title,
                        pm_zk_unread = pm_zk_unread,
                        complaint_cancel_label = subticket.complaint_cancel_label,
                        )
                    )

            batch_idx += 1
            self.subtickets_len += 1

            if filter_rs_id in ('rs-complaint', 'rs-complaint-declined', ):
                if subticket_review_state in filter_closed_ids:
                    count_closed += 1
                else:
                    count_open += 1


        # self.subtickets_len = len(self.subtickets)
        self.set_batch()

        if DEBUG:
            print(self.subtickets_len, "subtickets")
            print(" ")

        if 'sort_order' in query:
            del query['sort_order']
        if 'sort_on' in query:
            del query['sort_on']
        if 'library_zk' in query:
            del query['library_zk']

        # Přidat čísla pro všechny filtry
        self.filter_counter = dict()
        self.filter_counter_str = dict()
        # filter_rs_id_real se používá pro zjištění, zda tento filtr je
        # podmnožinou některého ze speciálních filtrů a má proto smysl
        # zobrazovat aktuální_počet / celkový_počet pro tento speciální filtr
        if not filter_rs_id:
            filter_rs_id_real = filter_rs_id
        elif filter_rs_id.startswith('rs-cancel'):
            filter_rs_id_real = 'cancelled'
        elif filter_rs_id == 'rs-complaint-accepted':
            filter_rs_id_real = 'cancelled'
        elif filter_rs_id in ('rs-complaint-undecided',
                              'rs-complaint-objected', ):
            filter_rs_id_real = 'sent'
        else:
            filter_rs_id_real = filter_rs_id
        if filter_id is None and filter_rs_id_real is not None:
            pass
        elif filter_id is None:
            self.filter_counter['rs-open'] = self.tickets_len
        else:
            self.filter_counter[filter_id] = self.tickets_len
        # fids = list(filter_rs_ids)
        # fids.extend(['rs-todo', 'rs-closed', 'rs-decide'])
        fids = ['rs-open', 'rs-closed', 'rs-todo', ]
        if self.is_ziskej_operator:
            fids.append('rs-decide')
        if DEBUG:
            print("fids", fids)
        for fid in fids:
            is_active_fid = bool(fid in self.filter_counter)
            if 'review_state' in query:
                del query['review_state']
            if 'complaint_state' in query:
                del query['complaint_state']

            if fid == 'rs-open':
                query['review_state'] = filter_open_ids
            elif fid == 'rs-closed':
                query['review_state'] = filter_closed_ids
            elif fid == 'rs-todo':
                query['review_state'] = filter_todo_ids
            elif fid == 'rs-decide':
                query['complaint_state'] = 'objected'
            else:
                logger.warning('Unknown fid {} for filter_counter'.format(fid))
                continue

            brains = self.catalog(query)

            if DEBUG:
                print(fid, len(brains), 'for query', query)
            self.filter_counter[fid] = len(brains)
            if is_active_fid:
                if self.filter_counter[fid] == self.subtickets_len:
                    self.subtickets_len_str = self.subtickets_len
                else:
                    self.subtickets_len_str = u'{}/{}'.format(
                        self.subtickets_len,
                        self.filter_counter[fid],
                        )
            elif fid in ('rs-open', 'rs-closed', ) and filter_rs_id in (
                    'rs-complaint', 'rs-complaint-declined', ):
                if fid == 'rs-open':
                    count = count_open
                elif fid == 'rs-closed':
                    count = count_closed
                else:
                    count = -1  # Nemělo by nastat
                self.filter_counter_str[fid] = u'{}/{}'.format(
                    count,
                    self.filter_counter[fid],
                    )
            elif 'review_state' not in query or not filter_rs_id_real or \
                    filter_rs_id_real not in query['review_state'] or \
                    self.filter_counter[fid] == self.subtickets_len:
                self.filter_counter_str[fid] = u'{}'.format(
                    self.filter_counter[fid],
                    )
            else:
                self.filter_counter_str[fid] = u'{}/{}'.format(
                    self.subtickets_len,
                    self.filter_counter[fid],
                    )

        if DEBUG:
            print(" ")
            print("self.filter_counter")
            print(self.filter_counter)
            print(" ")
            print("self.filter_counter_str")
            print(self.filter_counter_str)
            print(" ")
