# -*- coding: utf-8 -*-
"""BrowserView Listing: Readers.

Seznam čtenářů.
"""

import logging

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.protect.utils import addTokenToUrl

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.utils import address_encode, pprint


logger = logging.getLogger("ziskej")


class ReaderListingView(ZiskejBrowserView):

    template = ViewPageTemplateFile('reader_listing.pt')

    def call(self):
        self.unauthorized = True
        if self.is_reader:
            user_obj = self.ziskej_user_info['user']
            if user_obj:
                self.redirect(user_obj.absolute_url())
            else:
                self.redirect(self.portal_url)
            return
        elif self.is_ziskej_operator:
            pass
        else:
            self.redirect(self.portal_url)
            return
        self.unauthorized = False

        query = None
        if self.is_ziskej_operator:

            query = dict(
                portal_type = 'Reader',
                sort_on = 'getId',
                )

            # Rychlá diagnostika pro existující uživatele s:
            #     getUserName() != getId()
            if self.is_ziskej_admin:
                if self.request.get('fix_librarians', None):
                    query['portal_type'] = 'Librarian'
                    mcatalog = self.portal.membrane_tool
                    brains = mcatalog(query)
                    for brain in brains:
                        if brain.getId == brain.getUserName:
                            continue
                        logger.info('fix_librarians: {username} -> {id}'.format(
                            id = brain.getId,
                            username = brain.getUserName,
                            ))
                        obj = brain.getObject()
                        obj.username = brain.getId
                        obj.reindexObject()
                    self.redirect(self.context_url + '?show_librarians=1')
                    self.items_len = 0
                    return
                elif self.request.get('show_librarians', None):
                    query['portal_type'] = 'Librarian'

        if query is not None:
            mcatalog = self.portal.membrane_tool
            brains = mcatalog(query)
        else:
            brains = []

        self.data_cols = [u'ID', u'Uživatelské jméno', u'Stav', u'NTK user id',
            u'Sigla knihovny', u'Čtenář ID', u'Potvrzeni knihovníkem',
            u'library_claim', u'E-mail', u'eppn', u'EntityID IdP',
            u'GDPR registrace', u'GDPR registrace datum', u'GDPR data',
            u'GDPR data datum', u'Naposledny přihlášen', ]

        self.attr_list = ['fullname', 'review_state', 'ntk_user_id', 'library',
            'Reader_lid', 'is_library_confirmed', 'library_claim', 'email_user',
            'eppn', 'idp_entityid', 'gdpr_registration', 
            'gdpr_registration_date', 'gdpr_data', 'gdpr_data_date',
            'last_login_date', ]
        #pprint(self.attr_list)

        self.data = []
        for brain in brains:
            reader_id = brain.getId
            if brain.getUserName != reader_id:
                logger.warning('membrane user username {username} differs from id {id}'.format(
                    id=reader_id,
                    username=brain.getUserName,
                    ))
            data_item = dict(
                id = reader_id,
                username = brain.getUserName,
                review_state_label = brain.review_state == 'enabled' and u'Aktivní' or u'Neaktivní',
                url = '{}/admin_reader'.format(brain.getURL()),
                )
            # Reset hesla jen pro testovací čtenáře, kteří jsou bez Shibbolethu
            if not brain['eppn']:
                data_item['url_reset'] = self.portal_url + '/passwd_reset_init?u={}'.format(reader_id)
            attrs = ['review_state', 'ntk_user_id', 'fullname', 'library',
                'Reader_lid', 'is_library_confirmed', 'library_claim', 
                'email_user', 'eppn', 'idp_entityid', 'gdpr_registration',
                'gdpr_registration_date', 'gdpr_data', 'gdpr_data_date',
                'last_login_date', ]
            for k in attrs:
                try:
                    value = brain[k]
                except:
                    value = u'?'
                if k == 'library':
                    if value and len(value.strip()) > 6:
                        logger.info('Invalid reader.library for reader {}'.format(reader_id))
                        value = u'invalid'
                if k in ('gdpr_registration_date', 'gdpr_data_date', 'last_login_date'):
                    if value is not None:
                        value = value.strftime('%-H:%M %-d.%-m.%Y ')
                if k in ('is_library_confirmed', 'gdpr_registration', 'gdpr_data'):
                    if value:
                        value=u'Ano'
                    else:
                        value=u'Ne'
                    
                data_item[k] = value
            self.data.append(data_item)

        self.items_len = len(self.data)
