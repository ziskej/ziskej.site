# -*- coding: utf-8 -*-
"""
Ticket

Create
"""

from __future__ import print_function

import logging
import json

from ziskej.site import DEBUG_EDD
from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.utils_lib import pprint


logger = logging.getLogger("ziskej.ticket_create")
logger_dmd = logging.getLogger("DEBUG_MISSING_DOC")

SHIBBOLETH_ENABLED = True
USE_SESSION_STORAGE = False
COOKIE_CPK_POST = 'cpk_post_storage'
EXPIRES = 'Wed, 15 Jan 2030 12:00:00 GMT'  # FIXME May be do not use long time cookies for CPK POST

DEBUG_SHIBBOLETH = SHIBBOLETH_ENABLED and True
DEBUG = False
DEBUG_CPK_POST_VALUE = DEBUG or False
DEBUG_MISSING_DOC = False


class TicketView(ZiskejBrowserView):

    def call(self):
        self.shibboleth_enabled = SHIBBOLETH_ENABLED and (
            ('ziskej.aow.cz' in self.portal_url) or
            ('ziskej.techlib.cz' in self.portal_url) or
            ('ziskej.ntkcz.cz' in self.portal_url)
            )
        if self.request.get('expire', None):
            return self.expire_cookie_cpkpost()
        return False

    def expire_cookie_cpkpost(self):
        if self.request.cookies.get(COOKIE_CPK_POST, None):
            self.request.response.expireCookie(COOKIE_CPK_POST, path="/")
            url = self.request.URL
            try:
                qs = self.request.QUERY_STRING
                if qs:
                    url = url + '?' + qs
            except:
                logger.warning('expire_cookie_cpkpost qs issue')
            self.redirect(url)
            return True
        return False

    def debug_session(self):
        sdm = self.context.session_data_manager
        session = sdm.getSessionData(create=False)
        if session is None:
            logger_dmd.info('debug_session: session does not exist')
            return

        cpk_post = session.get("cpk_post", None)
        if not cpk_post:
            logger_dmd.info('debug_session: session exists but no cpk_post in session')
            #return
        else:
            cpk_post_keys = cpk_post.keys()
            if not len(cpk_post_keys):
                logger_dmd.info('debug_session: session exists, cpk_post exists but is empty (no key)')
                #return
            else:
                logger_dmd.info('debug_session: cpk keys: {}'.format(str(cpk_post_keys)))
                logger_dmd.info('debug_session: cpk["doc_id"]: {}'.format(cpk_post.get('doc_id', None)))

        cpk_post_lb_data = session.get("cpk_post_lb_data", None)
        if not cpk_post_lb_data:
            logger_dmd.info('debug_session: session exists but no cpk_post_lb_data in session')
            #return
        else:
            cpk_post_lb_data_keys = cpk_post_lb_data.keys()
            if not len(cpk_post_lb_data_keys):
                logger_dmd.info('debug_session: session exists, cpk_post_lb_data exists but is empty (no key)')
                #return
            else:
                logger_dmd.info('debug_session: cpk lb keys: {}'.format(str(cpk_post_lb_data_keys)))

    def store_cpk_post(self, cpk_post):
        if USE_SESSION_STORAGE:
            # see https://docs.plone.org/develop/plone/sessions/session_variables.html
            sdm = self.context.session_data_manager
            session = sdm.getSessionData(create=True)
            session.set("cpk_post", cpk_post)
            if DEBUG:
                print('session.set("cpk_post", value), where value is:')
                pprint(cpk_post)
                print(' ')
            elif DEBUG_CPK_POST_VALUE:
                logger.info('session.set("cpk_post", value), where value is: {value}'.format(value=cpk_post))
            if DEBUG_MISSING_DOC:
                logger_dmd.info('session.set("cpk_post", value), where value is: {value}'.format(value=cpk_post))
        else:
            logger.info('storing CPK POST into cookie {}'.format(COOKIE_CPK_POST))
            cookie_value = json.dumps(cpk_post, ensure_ascii=False)
            self.request.response.setCookie(COOKIE_CPK_POST, cookie_value, expires=EXPIRES, path="/")

    def load_cpk_post(self):
        cpk_post = None
        if USE_SESSION_STORAGE:
            # read session
            # see https://docs.plone.org/develop/plone/sessions/session_variables.html
            sdm = self.context.session_data_manager
            session = sdm.getSessionData(create=False)
            if session is not None:
                cpk_post = session.get("cpk_post", None)
                if DEBUG:
                    print('session.get(cpk_post):')
                    pprint(cpk_post)
                    print(' ')
                if DEBUG_MISSING_DOC:
                    if cpk_post:
                        logger_dmd.info('cpk keys: {}'.format(cpk_post.keys()))
                    else:
                        logger_dmd.info('session exists but no cpk_post in session')
            else:
                if DEBUG:
                    print('no session (should be and should contain cpk_post')
                if DEBUG_MISSING_DOC:
                    logger_dmd.info('session with cpk does not exist')
        else:
            cookie_value = self.request.cookies.get(COOKIE_CPK_POST, None)
            #logger.info('loading CPK POST from cookie {}: {}'.format(COOKIE_CPK_POST, cookie_value))
            if not cookie_value:
                logger.info('loading CPK POST: Missing or empty')
                return None
            try:
                cpk_post = json.loads(cookie_value)
                #logger.info('loading CPK POST:')
                #logger.info(str(cpk_post))
            except Exception, e:
                logger.info(u'{}'.format(e))
                logger.error('Invalid cpk_post json in cookie')
                return None

        return cpk_post
