# -*- coding: utf-8 -*-
"""Ticket Create before form"""

from __future__ import print_function

import json

from plone import api
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.ticket_helpers import get_cpk_url
from ziskej.site.ticket_load_balancing import (
    get_doc_title,
    LoadBalancingException,
    )
from ziskej.site.browser.ticket_create.ticket_create_base import (
    TicketView,
    logger,
    logger_dmd,
    DEBUG,
    DEBUG_CPK_POST_VALUE,
    DEBUG_MISSING_DOC,
    DEBUG_EDD,
    DEBUG_SHIBBOLETH,
    )


class CreateView(TicketView):
    template = ViewPageTemplateFile('ticket_create_view.pt')

    def parse_cpk_post_raw_doc_alt_ids(self, cpk_post_raw):
        # parsování z CPK POST
        if cpk_post_raw['doc_alt_ids']:
            try:
                doc_alt_ids = str(cpk_post_raw['doc_alt_ids'])
                if '[' not in doc_alt_ids:
                    doc_alt_ids = '["{doc_alt_ids}"]'.format(doc_alt_ids=doc_alt_ids)
                else:
                    doc_alt_ids = doc_alt_ids.replace("'", '"')
                    if '[u"' in doc_alt_ids:
                        doc_alt_ids = doc_alt_ids.replace('[u"', '["')
                    if ', u"' in doc_alt_ids:
                        doc_alt_ids = doc_alt_ids.replace(', u"', ', "')
                doc_alt_ids = json.loads(doc_alt_ids)
            except Exception, e:
                logger.info("cpk_post_raw['doc_alt_ids']")
                logger.info(cpk_post_raw['doc_alt_ids'])
                logger.exception(e)
                doc_alt_ids = []
        else:
            doc_alt_ids = []
        return doc_alt_ids

    def unparse_cpk_post_raw_doc_alt_ids(self, doc_alt_ids):
        # reverzní parsování zpět do podoby CPK POST
        # "[u'mzk.MZK01-001647274']"
        result = ""
        if doc_alt_ids is None:
            doc_alt_ids = []
        if DEBUG:
            print(" ")
            print("unparse_cpk_post_raw_doc_alt_ids")
            print(" ")
        for item in doc_alt_ids:
            if DEBUG:
                print(str(item))
            result += "u'{}', ".format(str(item))
            if DEBUG:
                print(result)
                print(" ")
        if result:
            result = result[:-2]  # smazat závěrečné ', '
        result = "[{}]".format(result)
        if DEBUG:
            print(" ")
            print("unparse_cpk_post_raw_doc_alt_ids result")
            print(repr(result))
            print(" ")
        return result

    def get_cpk_post(self):
        if DEBUG:
            print('-- CreateView.get_cpk_post')
        if DEBUG_MISSING_DOC:
            logger_dmd.info('-- CreateView.get_cpk_post')
            self.debug_session()

        # FIXME upravit podle poslední verze z CPK
        cpk_post_keys = [
            'service', 'source_id', 'doc_id', 'doc_alt_ids', 'entity_id',
            'user_id', ]
        cpk_post_keys_dict = dict(
            doc_alt_ids = 'doc_alt_ids[]',
            )
        cpk_post = dict()
        for k in cpk_post_keys:
            value = self.request.form.get(cpk_post_keys_dict.get(k, k), None)
            if value is None:
                continue
            # FIXME add validation
            cpk_post[k] = value
        if len(cpk_post.keys()) == 0:
            if DEBUG_MISSING_DOC:
                logger_dmd.warning('get_cpk_post keys 0')
            return None
        if DEBUG_MISSING_DOC:
            logger_dmd.info('get_cpk_post cpk_post')
            if cpk_post:
                logger_dmd.info('cpk["doc_id"]: {}'.format(cpk_post.get('doc_id', None)))
                logger_dmd.info('cpk keys: {}'.format(cpk_post.keys()))
            logger_dmd.info(str(cpk_post))

        # Souborný katalog jako hlavní dokument nemůže být, je potřeba tuto
        # situaci ošetřit pro vytváření objednávky v Získej i pomocí Získej API
        # pro CPK pro čtenáře.  Použij první doc_alt_id, který není caslin
        # (souborný katalog) jako main doc (doc_id).
        doc_id = cpk_post['doc_id']
        if doc_id.startswith('caslin'):
            if DEBUG:
                print("main doc_id is caslin based: ", doc_id)
                print("cpk_post['doc_alt_ids']: ", repr(cpk_post['doc_alt_ids']))
            doc_alt_ids = self.parse_cpk_post_raw_doc_alt_ids(cpk_post)
            doc_id_caslin = doc_id
            doc_alt_ids_ = [doc_id_caslin, ]
            for item in doc_alt_ids:
                if doc_id_caslin == doc_id and not item.startswith('caslin'):
                    if DEBUG:
                        print("take first not caslin based alternative: ", item)
                    doc_id = item
                else:
                    doc_alt_ids_.append(item)
            if doc_id_caslin != doc_id:
                if DEBUG:
                    print("probably solved")
                cpk_post['doc_id'] = doc_id
                cpk_post['doc_alt_ids'] = self.unparse_cpk_post_raw_doc_alt_ids(doc_alt_ids_)
                if DEBUG:
                    print("main doc_id was caslin based: ", doc_id)
                    print("cpk_post['doc_alt_ids']: ", repr(cpk_post['doc_alt_ids']))
            else:
                if DEBUG:
                    print("cannot solve")
                raise LoadBalancingException('Invalid request: cannot process only Souborny katalog')

        return cpk_post

    def call(self):

        if DEBUG:
            print('-- CreateView.call')
        if DEBUG_MISSING_DOC:
            logger_dmd.info('-- CreateView.call')
            self.debug_session()

        super(CreateView, self).call()

        # 1) Default settings
        self.ctenar_login_enabled = False
        self.knihovnik_login_enabled = False
        self.doc_info_enabled = False
        self.missing_cpkpost = True
        self.error_cpk = False

        # 2) Get CPK POST, no parsing yet
        try:
            cpk_post = self.get_cpk_post()
        except LoadBalancingException, e:
            if str(e) == 'Invalid request: cannot process only Souborny katalog':
                self.add_message(u'Tento dokument je podle CPK pouze v souborném katalogu, což neposkytuje dostatek informací pro vytvoření objednávky.', message_type='warning')
                self.redirect()
                return
            else:
                logger.info('CreateView.call get_cpk_post LoadBalancingException: {}'.format(str(e)))
                raise

        # 3) Create session with CPK POST
        if cpk_post is not None:
            if DEBUG_MISSING_DOC:
                logger_dmd.info('cpk_post is not None')

            self.store_cpk_post(cpk_post)

        # 4) Build doc info based on CPK POST
        if cpk_post is not None:
            self.doc_info_enabled = True
            self.missing_cpkpost = False
            doc_id = cpk_post['doc_id']
            self.doc_cpk_url = get_cpk_url(doc_id=doc_id)
            if DEBUG_MISSING_DOC:
                logger_dmd.info('doc_id: {}'.format(doc_id))
            ignore_missing_unit_ids = False
            if cpk_post.get('service', 'mvs') == 'edd':
                ignore_missing_unit_ids = True
            try:
                # fullname aka citace
                self.doc_title = get_doc_title(
                    self.portal,
                    doc_id,
                    ignore_missing_unit_ids=ignore_missing_unit_ids,
                    )
            except LoadBalancingException, e:
                logger.exception('LoadBalancingException exception in ticket_create get_doc_title: ' + str(e))
                # self.add_message(u'Pro tento dokument neposkytuje CPK dostatek informací pro vytvoření objednávky.', message_type='warning')
                # self.redirect()
                # return
                self.error_cpk = True
                self.doc_info_enabled = False
                self.error_message = u'{}'.format(str(e))
                return
            except Exception, e:
                logger.exception('Uncatched exception in ticket_create get_doc_title: ' + str(e))
                raise

        # 4) Handle already logged in users - particular use cases are handled
        #    in ticket_create_summary
        self.is_anon = api.user.is_anonymous()
        if not self.is_anon:
            url = '{portal_url}/ticket_create_summary?authok=1'.format(portal_url=self.portal_url)
            if DEBUG_MISSING_DOC:
                logger_dmd.info('redirect to: {}'.format(url))
            self.redirect(url)
            return

        # 5) Build reader welcome and log in link, build librarian info
        #self.ctenar_login_enabled = True
        self.knihovnik_login_enabled = True

        self.ctenar_login_url = '{portal_url}/shibLogin?n=1'.format(portal_url=self.portal_url)
        if DEBUG_SHIBBOLETH:
            logger.info('ctenar_login_url: {url}'.format(url=self.ctenar_login_url))
        #if not self.shibboleth_enabled:  # enable dev without shibboleth
        #    self.ctenar_login_url = '{portal_url}/ticket_create_summary?token=1'.format(portal_url=self.portal_url)

        self.knihovnik_login_url = '{portal_url}/login_form'.format(portal_url=self.portal_url)
