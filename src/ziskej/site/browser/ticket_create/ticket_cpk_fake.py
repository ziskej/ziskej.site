# -*- coding: utf-8 -*-
"""Ticket Create CPK Fakd"""

from __future__ import print_function

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.ticket_helpers import get_cpk_url
from ziskej.site.browser.ticket_create.ticket_create_base import (
    TicketView,
    logger,
    logger_dmd,
    DEBUG,
    DEBUG_CPK_POST_VALUE,
    DEBUG_MISSING_DOC,
    DEBUG_EDD,
    )


class CpkFakeView(TicketView):
    template = ViewPageTemplateFile('ticket_cpk_fake_view.pt')

    def call(self):

        if DEBUG:
            print('-- CpkFakeView.call')
        if DEBUG_MISSING_DOC:
            logger_dmd.info('-- CpkFakeView.call')
            self.debug_session()

        # hraničářův učeň
        #self.doc_id = u'nkp.NKC01-002610829'
        #self.doc_alt_ids = u"['nkp.NKC01-002610829']"

        # jarník
        self.doc_id = u'nkp.NKC01-000021007'
        self.doc_alt_ids = u"['nkp.NKC01-000021007', 'mzk.MZK01-000084745']"

        # test chyba
        # {'source_id': 'knihovny.cz', 'user_id': '', 'doc_id': 'nkp.NKC01-002592509', 'doc_alt_ids': ['ntk.STK01-000954834', 'vkol.SVK01-001100971', 'svkpk.PNA01-000670456', 'nkp.NKC01-002592509', 'svkul.KN3148000000547751', 'svkkl.KlUsCat_0978274', 'kvkl.LiUsCat_0485287', 'mzk.MZK01-001418216', 'cbvk.CbvkUsCat_0531446', 'kfbz.517572', 'kkkv.KKV01-000526136', 'kkvy.KN3170000000487322', 'kkpc.PAK01-000329734', 'caslin.SKC01-006320779', 'pkjak.430187', 'svkhk.HKA01-000829978', 'svkos.MVK01-000609853', 'vkta.KN3171000000310478', 'mkp.4073071']}
        #self.doc_id = u'nkp.NKC01-002592509'
        #self.doc_alt_ids = u"['ntk.STK01-000954834', 'vkol.SVK01-001100971', 'svkpk.PNA01-000670456', 'nkp.NKC01-002592509', 'svkul.KN3148000000547751', 'svkkl.KlUsCat_0978274', 'kvkl.LiUsCat_0485287', 'mzk.MZK01-001418216', 'cbvk.CbvkUsCat_0531446', 'kfbz.517572', 'kkkv.KKV01-000526136', 'kkvy.KN3170000000487322', 'kkpc.PAK01-000329734', 'caslin.SKC01-006320779', 'pkjak.430187', 'svkhk.HKA01-000829978', 'svkos.MVK01-000609853', 'vkta.KN3171000000310478', 'mkp.4073071']"

        # MZK příklad - nic není dostupné
        #self.doc_id = u'svkos.MVK01-000079968'
        #self.doc_alt_ids = u"['svkos.MVK01-000079968', 'svkkl.KlUsCat_c019637', 'caslin.SKC01-000481596']"

        # kram issue
        #self.doc_id = u'mkp.3563508'
        ##self.doc_alt_ids = u"['nkp.NKC01-002367222', 'nlk.179125', 'vkol.SVK01-000982451', 'svkpk.PNA01-000548677', 'kvkl.LiUsCat_0372426', 'svkul.KN3148000000468015', 'mzk.MZK01-001150885', 'svkkl.KlUsCat_0848847', 'ntk.STK01-000941470', 'kkkv.KKV01-000436846', 'okpb.KN3183000000253702', 'kjdpb.KN3151000000166870', 'kpsys.oai:471325', 'kkvy.KN3170000000426780', 'kfbz.477686', 'caslin.SKC01-004810075', 'tre.54855', 'mkchodov.125907', 'pkjak.347532', 'mkkh.198378', 'caslin.SKC01-005315066', 'mkfm.KN3173000000209822', 'cbvk.CbvkUsCat_0312074', 'vkta.KN3171000000273019', 'svkhk.HKA01-000760711', 'svkos.MVK01-000516968', 'mkpr.KN3168000000244322', 'knav.KNA01-000925619', 'kkpc.PAK01-000268006', 'mkp.3563508', 'kram-nkp.uuid:c63d69e0-d1ef-11e4-b880-005056825209']"
        #self.doc_alt_ids = u"['mkp.3563508', 'kram-nkp.uuid:c63d69e0-d1ef-11e4-b880-005056825209']"

        # ntk.STK01-000926252
        #self.doc_id = u'ntk.STK01-000926252'
        #self.doc_alt_ids = u"ntk.STK01-000926252"

        # filter example
        # {'source_id': 'knihovny.cz', 'user_id': '', 'entity_id': '', 'doc_id': 'cbvk.CbvkUsCat_0712018', 'doc_alt_ids': ['nkp.NKC01-002892761', 'vkol.SVK01-001200067', 'mzk.MZK01-001571497', 'cbvk.CbvkUsCat_0712018', 'kvkl.LiUsCat_0600212', 'svkkl.KlUsCat_1131188', 'svkul.KN3148000001140565', 'kkvy.KN3170000001288576', 'kfbz.559453', 'kkkv.KKV01-000549816', 'kjdpb.KN3151000001351549', 'caslin.SKC01-007392361', 'okpb.KN3183000000465899', 'kkpc.PAK01-000328845', 'svkhk.HKA01-000884367', 'vkta.KN3171000000695926', 'svkos.MVK01-000711592', 'mkp.4339153', 'mkpr.KN3168000000467299', 'tre.74596']}
        self.doc_id = u'cbvk.CbvkUsCat_0712018'
        self.doc_alt_ids = u"['nkp.NKC01-002892761', 'vkol.SVK01-001200067', 'mzk.MZK01-001571497', 'cbvk.CbvkUsCat_0712018', 'kvkl.LiUsCat_0600212', 'svkkl.KlUsCat_1131188', 'svkul.KN3148000001140565', 'kkvy.KN3170000001288576', 'kfbz.559453', 'kkkv.KKV01-000549816', 'kjdpb.KN3151000001351549', 'caslin.SKC01-007392361', 'okpb.KN3183000000465899', 'kkpc.PAK01-000328845', 'svkhk.HKA01-000884367', 'vkta.KN3171000000695926', 'svkos.MVK01-000711592', 'mkp.4339153', 'mkpr.KN3168000000467299', 'tre.74596']"

        # 4 available
        # {'source_id': 'knihovny.cz', 'user_id': '', 'entity_id': '', 'doc_id': 'mzk.MZK01-001318874', 'doc_alt_ids': ['svkpk.PNA01-000679265', 'nkp.NKC01-002457963', 'vkol.SVK01-001041413', 'mzk.MZK01-001318874', 'svkul.KN3148000000519893', 'cbvk.CbvkUsCat_0467234', 'svkkl.KlUsCat_0915818', 'kkvy.KN3170000000459232', 'kjdpb.KN3151000000187046', 'kfbz.504612', 'kkkv.KKV01-000509921', 'caslin.SKC01-005800848', 'okpb.KN3183000000274891', 'kvkl.LiUsCat_0427687', 'svkos.MVK01-000576859', 'rkka.723514', 'mkchodov.127249', 'tre.60590', 'vkta.KN3171000000291107', 'mkkh.214091', 'mkuo.72453', 'kkpc.PAK01-000287986', 'mkpr.KN3168000000273653', 'mkp.3945288']}

        self.doc_id = u'mzk.MZK01-001318874'
        self.doc_alt_ids = u"[]"
        #self.doc_alt_ids = u"['svkpk.PNA01-000679265', 'nkp.NKC01-002457963', 'vkol.SVK01-001041413', 'mzk.MZK01-001318874', 'svkul.KN3148000000519893', 'cbvk.CbvkUsCat_0467234', 'svkkl.KlUsCat_0915818', 'kkvy.KN3170000000459232', 'kjdpb.KN3151000000187046', 'kfbz.504612', 'kkkv.KKV01-000509921', 'caslin.SKC01-005800848', 'okpb.KN3183000000274891', 'kvkl.LiUsCat_0427687', 'svkos.MVK01-000576859', 'rkka.723514', 'mkchodov.127249', 'tre.60590', 'vkta.KN3171000000291107', 'mkkh.214091', 'mkuo.72453', 'kkpc.PAK01-000287986', 'mkpr.KN3168000000273653', 'mkp.3945288']"

        #self.doc_id = u'vkol.SVK01-000786669'
        #self.doc_id = u'bmc.784961'
        #self.doc_alt_ids = u"[]"

        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'nkp.NKC01-002956896', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'nkp.NKC01-002956896', u'vkol.SVK01-001221411', u'kvkl.LiUsCat_0630934', u'svkul.KN3148000001328703', u'svkkl.KlUsCat_1158185', u'kfbz.745355', u'kkvy.KN3170000002103806', u'kjdpb.KN3151000002813233', u'okpb.KN3183000000621930', u'mzk.MZK01-001606249', u'mkchodov.147564', u'mkkh.251911', u'caslin.SKC01-007620383', u'cbvk.CbvkUsCat_0751427', u'svkhk.HKA01-000903183', u'svkos.MVK01-000727031', u'vkta.KN3171000001137364', u'mkp.4379940', u'mkpr.KN3168000000497037', u'tre.76796']}
        #self.doc_id = u'nkp.NKC01-002956896'
        #self.doc_alt_ids = u"[u'nkp.NKC01-002956896', u'vkol.SVK01-001221411', u'kvkl.LiUsCat_0630934', u'svkul.KN3148000001328703', u'svkkl.KlUsCat_1158185', u'kfbz.745355', u'kkvy.KN3170000002103806', u'kjdpb.KN3151000002813233', u'okpb.KN3183000000621930', u'mzk.MZK01-001606249', u'mkchodov.147564', u'mkkh.251911', u'caslin.SKC01-007620383', u'cbvk.CbvkUsCat_0751427', u'svkhk.HKA01-000903183', u'svkos.MVK01-000727031', u'vkta.KN3171000001137364', u'mkp.4379940', u'mkpr.KN3168000000497037', u'tre.76796']"

       # self.doc_id = u'nkp.NKC01-002573667'
        #self.doc_alt_ids = u"doc_alt_ids': [u'nkp.NKC01-002573667', u'vkol.SVK01-001082510', u'mzk.MZK01-001382417', u'svkul.KN3148000000533084', u'svkkl.KlUsCat_0952974', u'kvkl.LiUsCat_0467766', u'cbvk.CbvkUsCat_0506401', u'kjdpb.KN3151000000208436', u'kkvy.KN3170000000479123', u'kfbz.513484', u'okpb.KN3183000000282717', u'caslin.SKC01-006188628', u'mkkh.220541', u'tre.63022', u'svkos.MVK01-000602309', u'vkta.KN3171000000297313', u'mkpr.KN3168000000286584', u'mkuo.74124', u'mkp.4033801']"

        self.doc_id = u'mzk.MZK01-000935298'
        self.doc_alt_ids = u"[u'vkol.SVK01-000887781', u'nkp.NKC01-001866320', u'kvkl.LiUsCat_0234552', u'svkul.KN3148000000413883', u'mzk.MZK01-000935298', u'cbvk.CbvkUsCat_0126579', u'svkkl.KlUsCat_0670218', u'kkvy.KN3170000000391293', u'kfbz.445566', u'kjdpb.KN3151000000144290', u'mkkh.168066', u'caslin.SKC01-003767416', u'okpb.KN3183000000224688', u'svkos.MVK01-000530432', u'tre.56714', u'vkta.KN3171000000253701', u'mkchodov.119563', u'mkpr.KN3168000000205852', u'mkuo.62155', u'mkp.2775336']"
        #{u'source_id': u'www.knihovny.cz', u'doc_id': u'mzk.MZK01-000935298', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'vkol.SVK01-000887781', u'nkp.NKC01-001866320', u'kvkl.LiUsCat_0234552', u'svkul.KN3148000000413883', u'mzk.MZK01-000935298', u'cbvk.CbvkUsCat_0126579', u'svkkl.KlUsCat_0670218', u'kkvy.KN3170000000391293', u'kfbz.445566', u'kjdpb.KN3151000000144290', u'mkkh.168066', u'caslin.SKC01-003767416', u'okpb.KN3183000000224688', u'svkos.MVK01-000530432', u'tre.56714', u'vkta.KN3171000000253701', u'mkchodov.119563', u'mkpr.KN3168000000205852', u'mkuo.62155', u'mkp.2775336']}
        
        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'nkp.NKC01-002573667', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'nkp.NKC01-002573667', u'vkol.SVK01-001082510', u'mzk.MZK01-001382417', u'svkul.KN3148000000533084', u'svkkl.KlUsCat_0952974', u'kvkl.LiUsCat_0467766', u'cbvk.CbvkUsCat_0506401', u'kjdpb.KN3151000000208436', u'kkvy.KN3170000000479123', u'kfbz.513484', u'okpb.KN3183000000282717', u'caslin.SKC01-006188628', u'mkkh.220541', u'tre.63022', u'svkos.MVK01-000602309', u'vkta.KN3171000000297313', u'mkpr.KN3168000000286584', u'mkuo.74124', u'mkp.4033801']}

        # Hořící most, je dostupné jen do studovny
        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'mzk.MZK01-001407480', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'nkp.NKC01-002610830', u'svkul.KN3148000000725140', u'vkol.SVK01-001096973', u'mzk.MZK01-001407480', u'caslin.SKC01-006260954', u'mkp.4064716']}
        self.doc_id = u'mzk.MZK01-001407480'
        self.doc_alt_ids = u"[u'nkp.NKC01-002610830', u'svkul.KN3148000000725140', u'vkol.SVK01-001096973', u'mzk.MZK01-001407480', u'caslin.SKC01-006260954', u'mkp.4064716']"

        # NEZBEDA, Ivo; KOLAFA, Jiří; KOTRLA, Miroslav. Úvod do počítačových simulací. 2. upr. vyd. Praha: Karolinum, 2003. 204 s. ISBN 80-246-0649-6. 
        # https://www.knihovny.cz/Record/mzk.MZK01-000667059
        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'mzk.MZK01-000667059', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'vkol.SVK01-000657320', u'svkpk.PNA01-000164704', u'nkp.NKC01-001245938', u'svkul.KN3148000000256623', u'svkos.MVK01-000353364', u'mzk.MZK01-000667059', u'svkkl.KlUsCat_c297084', u'kfbz.292116', u'knav.KNA01-000031649', u'caslin.SKC01-001016930', u'cbvk.CbvkUsCat_m0309558', u'ntk.STK01-000927957', u'svkhk.HKA01-000314756', u'mkp.2345334']}
        self.doc_id = u'mzk.MZK01-000667059'
        self.doc_alt_ids = u"[u'vkol.SVK01-000657320', u'svkpk.PNA01-000164704', u'nkp.NKC01-001245938', u'svkul.KN3148000000256623', u'svkos.MVK01-000353364', u'mzk.MZK01-000667059', u'svkkl.KlUsCat_c297084', u'kfbz.292116', u'knav.KNA01-000031649', u'caslin.SKC01-001016930', u'cbvk.CbvkUsCat_m0309558', u'ntk.STK01-000927957', u'svkhk.HKA01-000314756', u'mkp.2345334']"

        # Bez dostupných dokumentů - pro testování NTK-29
        # https://www.knihovny.cz/Record/svkpk.PNA01-000138726
        #self.doc_id = u'svkpk.PNA01-000138726'
        #self.doc_alt_ids = u"[u'svkpk.PNA01-000138726', u'kvkl.LiUsCat_c240633', u'mkhk.KN3566000000240989']"

        # NTK-49
        #self.doc_id = u'mzk.MZK01-000571677'
        #self.doc_alt_ids = u"[u'nkp.NKC01-001023509', u'vkol.SVK01-000550391', u'mzk.MZK01-000571677', u'svkos.MVK01-000043467', u'svkul.KN3148000000017889', u'svkkl.KlUsCat_c266369', u'kkvy.KN3170000000248787', u'kfbz.262859', u'kkkv.KKV01-000008221', u'okpb.KN3183000000053111', u'mkpel.KN4202900000037607', u'mkzn.KN3162000000050402', u'mkhod.KN3181000000056260', u'knep.KN4208400000139795', u'kvkl.LiUsCat_c242313', u'cbvk.CbvkUsCat_m0022718', u'caslin.SKC01-000995091', u'svkhk.HKA01-000286451', u'mkpr.KN3168000000030412', u'mkp.2275852']"

        # NTK-77
        #self.doc_id = u'svkpk.PNA01-000332351'
        #self.doc_alt_ids = u"[u'svkpk.PNA01-000332351', u'nkp.NKC01-000679851', u'kvkl.LiUsCat_c272938', u'mkck.KN3154000000004559', u'mzk.MZK01-000541020', u'cbvk.CbvkUsCat_m0257879', u'vkol.SVK01-001084848', u'mkhod.KN3181000000018041', u'nkp.SLK01-000491569', u'caslin.SKC01-000580098', u'svkhk.HKA01-000656967', u'mkuo.21702', u'knav.KNA01-001055011']"

        # NTK-121
        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'mzk.MZK01-001542205', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'nlk.69370', u'nkp.NKC01-000266332', u'mzk.MZK01-001542205']}
        self.doc_id = u'mzk.MZK01-001542205'
        self.doc_alt_ids = u"[u'nlk.69370', u'nkp.NKC01-000266332', u'mzk.MZK01-001542205']"

        # NTK-63
        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'mzk.MZK01-000180923', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'vkol.SVK01-000389892', u'nkp.NKC01-000129766', u'kfbz.62084', u'svkos.MVK01-000149848', u'mzk.MZK01-000180923', u'caslin.SKC01-000388580', u'cbvk.CbvkUsCat_0458477']}
        self.doc_id = u'mzk.MZK01-000180923'
        self.doc_alt_ids = u"[u'vkol.SVK01-000389892', u'nkp.NKC01-000129766', u'kfbz.62084', u'svkos.MVK01-000149848', u'mzk.MZK01-000180923', u'caslin.SKC01-000388580', u'cbvk.CbvkUsCat_0458477']"

        # NTK-147
        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'vkol.SVK01-000651639', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'vkol.SVK01-000651639', u'svkpk.PNA01-000165663', u'nkp.NKC01-001246094', u'mzk.MZK01-000665782', u'svkul.KN3148000000059844', u'svkos.MVK01-000095666', u'kvkl.LiUsCat_c267312', u'kkkv.KKV01-000027842', u'kkvy.KN3170000000300591', u'mkhk.KN3566000000305314', u'caslin.SKC01-001016994', u'cbvk.CbvkUsCat_m0074465', u'svkhk.HKA01-000329720', u'vkta.KN3171000000145399', u'knav.KNA01-000032021', u'mkuo.46816', u'mkp.2343063']}
        self.doc_id = u'vkol.SVK01-000651639'
        self.doc_alt_ids = u"[u'vkol.SVK01-000651639', u'svkpk.PNA01-000165663', u'nkp.NKC01-001246094', u'mzk.MZK01-000665782', u'svkul.KN3148000000059844', u'svkos.MVK01-000095666', u'kvkl.LiUsCat_c267312', u'kkkv.KKV01-000027842', u'kkvy.KN3170000000300591', u'mkhk.KN3566000000305314', u'caslin.SKC01-001016994', u'cbvk.CbvkUsCat_m0074465', u'svkhk.HKA01-000329720', u'vkta.KN3171000000145399', u'knav.KNA01-000032021', u'mkuo.46816', u'mkp.2343063']"

        # caslin s právě jednou alternativou
        self.doc_id = u'caslin.SKC01-007852889'
        self.doc_alt_ids = u"[u'mzk.MZK01-001647274']"

        # # caslin bez alternativy
        # self.doc_id = u'caslin.SKC01-007852889'
        # self.doc_alt_ids = u"[]"

        # ["mkzn.KN3162000000064908", "svkpk.PNA01-000797168", "nkp.NKC01-000612670", "ntk.STK01-000245341", "cbvk.CbvkUsCat_0673124", "kvkl.LiUsCat_0879089", "mzk.MZK01-000053336", "svkos.MVK01-000395799", "svkhk.HKA01-000384290", "knav.KNA01-002294784", "caslin.SKC01-000560877"]
        self.doc_id = u'mkzn.KN3162000000064908'
        self.doc_alt_ids = u"[u'mkzn.KN3162000000064908', u'svkpk.PNA01-000797168', u'nkp.NKC01-000612670', u'ntk.STK01-000245341', u'cbvk.CbvkUsCat_0673124', u'kvkl.LiUsCat_0879089', u'mzk.MZK01-000053336', u'svkos.MVK01-000395799', u'svkhk.HKA01-000384290', u'knav.KNA01-002294784', u'caslin.SKC01-000560877']"

        # self.doc_id = u'mzk.MZK01-001407480'
        # self.doc_alt_ids = u"[]"

        # NTK-154 A
        # doc_id: mkkh.206157
        # doc_data: {'year_publication': u'2010', 'unit_ids': [u'KHG001.316400196549', u'KHG001.316400221401'], 'author': u'', 'citation': u'; BARTO\u0160, Martin; \u0160IM\u016eNEK, Robert. Kutn\xe1 Hora. Praha: Historick\xfd \xfastav Akademie v\u011bd \u010cR, 2010. 1 atlas (28, xxi s., 51 mapov\xfdch list\u016f). ISBN 978-80-7286-170-5.', 'availability': {}, 'citation_dict': {'main_author': u'', 'isbn': u'978-80-7286-170-5', 'issue_year': u'2010', 'other_authors': [u'Barto\u0161, Martin', u'\u0160im\u016fnek, Robert'], 'title': u'Kutn\xe1 Hora', 'issue_number': u'', 'page_number': u'1 atlas (28, xxi s., 51 mapov\xfdch list\u016f)', 'issue_place': u'Praha', 'issuer': u'Historick\xfd \xfastav Akademie v\u011bd \u010cR'}, 'caslin_info': None, 'title': u'Kutn\xe1 Hora', 'id': u'mkkh.206157', 'sigla': u''}
        # doc_alt_ids: [u'svkpk.PNA01-000545024', u'nkp.NKC01-002158271', u'vkol.SVK01-000972382', u'svkkl.KlUsCat_0777614', u'kvkl.LiUsCat_0308583', u'cbvk.CbvkUsCat_0307327', u'svkul.3555084', u'mzk.MZK01-001133668', u'kkpc.PAK01-000263698', u'mkck.KN3154000000102074', u'caslin.SKC01-004541549', u'mkkh.206157', u'svkos.MVK01-000507046', u'svkhk.HKA01-000754856', u'knav.KNA01-000885426', u'mkp.3516736']
        # self.doc_id = u'mkkh.206157'
        # self.doc_alt_ids = u"[]"

        # NTK-154 B
        # self.doc_id = u'kkdvy.VyUsCat_0154865'
        # self.doc_alt_ids = u"[]"

        # NTK-243
        # self.doc_alt_ids = u"[]"
        # self.doc_id = u'ntk.STK01-001870241'
        # self.doc_id = u'svkul.3720276'
        # self.doc_id = u'rkka.892403'

        # # NTK-249
        # self.doc_alt_ids = u"[]"
        # self.doc_id = u'kkkv.KKV01-000521611'

        # # NTK-248
        # self.doc_alt_ids = u"[]"
        # self.doc_id = u'rkka.42015'

        # # NTK-152 A
        # # Původní doc_ids, nicméně to vše funguje.
        # self.doc_alt_ids = u"[u'svkul.3209554', u'nkp.NKC01-000490807', u'mzk.MZK01-000529634', u'caslin.SKC01-000495665', u'svkpk.PNA01-000366495', u'kkpc.PAK01-000321083', u'kfbz.465696', u'cbvk.CbvkUsCat_m0137513', u'mkjar.94776', u'knihkm.234635', u'mkplzen.673', u'kvkl.LiUsCat_c002185', u'svkos.MVK01-000159482', u'mkcl.413846', u'mktrut.211567', u'kkdvy.VyUsCat_c079810', u'knep.106765', u'mkhnm.KN4222300000006054', u'svkkl.KlUsCat_0485735', u'mkbrec.KN3169000000007206', u'mkkh.41739', u'mkhod.KN3181000000010932', u'mkpr.1286595', u'mkzn.KN3162000000003301', u'vkol.SVK01-001063860', u'rkka.615045', u'rkka.655002', u'vkta.1524', u'mkuo.23586', u'svkhk.HKA01-000223786', u'nkp.SLK01-000000917']"
        # # Část doc_ids - pouze ty, kde AutoLB vrátí Neznámá knihovna.
        # if True:
        #     self.doc_alt_ids = u"[u'knihkm.234635', u'kkdvy.VyUsCat_c079810', u'rkka.655002']"
        # self.doc_id = u'mkkh.41739'

        # # NTK-152 B
        # # {u'source_id': u'www.knihovny.cz', u'doc_id': u'kfbz.449009', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'ntk.STK01-000676723', u'nkp.NKC01-001964916', u'vkol.SVK01-000896995', u'svkpk.PNA01-000457625', u'svkul.3521061', u'kvkl.LiUsCat_0254474', u'cbvk.CbvkUsCat_0149489', u'mzk.MZK01-000956010', u'kfbz.449009', u'caslin.SKC01-003844422', u'kkvy.2171341', u'svkkl.KlUsCat_0684123', u'knihkm.102230', u'mkhk.2157408', u'kkpc.PAK01-000248800', u'kkdvy.VyUsCat_0154865', u'svkhk.HKA01-000679861', u'svkos.MVK01-000478684', u'mkp.2791652']}
        # self.doc_alt_ids = u"[u'ntk.STK01-000676723', u'nkp.NKC01-001964916', u'vkol.SVK01-000896995', u'svkpk.PNA01-000457625', u'svkul.3521061', u'kvkl.LiUsCat_0254474', u'cbvk.CbvkUsCat_0149489', u'mzk.MZK01-000956010', u'kfbz.449009', u'caslin.SKC01-003844422', u'kkvy.2171341', u'svkkl.KlUsCat_0684123', u'knihkm.102230', u'mkhk.2157408', u'kkpc.PAK01-000248800', u'kkdvy.VyUsCat_0154865', u'svkhk.HKA01-000679861', u'svkos.MVK01-000478684', u'mkp.2791652']"
        # self.doc_id = u'kfbz.449009'

        # NTK-135
        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'nlk.173883', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'nkp.NKC01-002105781', u'nlk.173883', u'svkpk.PNA01-000519382', u'vkol.SVK01-000956295', u'svkul.3540817', u'mzk.MZK01-001095651', u'caslin.SKC01-004229307', u'svkkl.KlUsCat_0737245', u'svkos.MVK01-000487825', u'svkhk.HKA01-000734110', u'mkp.2867609']}
        self.doc_alt_ids = u"[u'nkp.NKC01-002105781', u'nlk.173883', u'svkpk.PNA01-000519382', u'vkol.SVK01-000956295', u'svkul.3540817', u'mzk.MZK01-001095651', u'caslin.SKC01-004229307', u'svkkl.KlUsCat_0737245', u'svkos.MVK01-000487825', u'svkhk.HKA01-000734110', u'mkp.2867609']"
        self.doc_id = u'nlk.173883'

        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'nkp.NKC01-000082516', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'nkp.NKC01-000082516', u'svkpk.PNA01-000036220', u'mzk.MZK01-000009181', u'vkol.SVK01-000036320', u'svkul.3280514', u'kkvy.2054226', u'okpb.KN3183000000003456', u'svkos.MVK01-000251292', u'kkpc.PAK01-000179509', u'svkkl.kl_us_cat*c020458', u'mkplzen.69686', u'mkchom.544426', u'svkhk.HKA01-000018338', u'mksvit.116120', u'cbvk.cbvk_us_cat*m0221138', u'caslin.SKC01-001878760', u'mkp.49369']}
        self.doc_alt_ids = u"[u'nkp.NKC01-000082516', u'svkpk.PNA01-000036220', u'mzk.MZK01-000009181', u'vkol.SVK01-000036320', u'svkul.3280514', u'kkvy.2054226', u'okpb.KN3183000000003456', u'svkos.MVK01-000251292', u'kkpc.PAK01-000179509', u'svkkl.kl_us_cat*c020458', u'mkplzen.69686', u'mkchom.544426', u'svkhk.HKA01-000018338', u'mksvit.116120', u'cbvk.cbvk_us_cat*m0221138', u'caslin.SKC01-001878760', u'mkp.49369']"
        self.doc_id = u'nkp.NKC01-000082516'

        # {u'source_id': u'www.knihovny.cz', u'doc_id': u'nkp.NKC01-000306330', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'nkp.NKC01-000306330', u'svkos.MVK01-000088766', u'svkul.3581279', u'mzk.MZK01-001144690', u'caslin.SKC01-002284512', u'vkol.SVK01-001000265', u'mkplzen.41627', u'mkp.2641017']}
        #self.doc_alt_ids = u"[u'nkp.NKC01-000306330', u'svkos.MVK01-000088766', u'svkul.3581279', u'mzk.MZK01-001144690', u'caslin.SKC01-002284512', u'vkol.SVK01-001000265', u'mkplzen.41627', u'mkp.2641017']"
        #self.doc_id = u'nkp.NKC01-000306330'

        # EDD

        # {u'service': u'edd', u'source_id': u'cpk-front.mzk.cz', u'doc_id': u'mzk.MZK01-001407480', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'vkol.SVK01-001096973', u'nkp.NKC01-002610830', u'svkul.3644803', u'mzk.MZK01-001407480', u'mkkl.205604', u'caslin.SKC01-006260954', u'kmol.1223710', u'mvk.201373', u'kjm.kjm_us_cat*71473325', u'mkp.4064716']}
        self.service = u'edd'
        self.doc_alt_ids = u"[u'vkol.SVK01-001096973', u'nkp.NKC01-002610830', u'svkul.3644803', u'mzk.MZK01-001407480', u'mkkl.205604', u'caslin.SKC01-006260954', u'kmol.1223710', u'mvk.201373', u'kjm.kjm_us_cat*71473325', u'mkp.4064716']"
        self.doc_id = u'mzk.MZK01-001407480'

        # EDD monografie
        # {u'entity_id': u'', u'user_id': u'', u'service': u'edd', u'doc_alt_ids': [u'svkpk.PNA01-000091445', u'nlk.44876', u'nkp.NKC01-000318970', u'svkul.3180497', u'mzk.MZK01-000310564', u'svkkl.kl_us_cat*c212271'], u'source_id': u'cpk-front.mzk.cz', u'doc_id': u'nlk.44876'}
        self.service = u'edd'
        self.doc_alt_ids = u"[u'svkpk.PNA01-000091445', u'nlk.44876', u'nkp.NKC01-000318970', u'svkul.3180497', u'mzk.MZK01-000310564', u'svkkl.kl_us_cat*c212271']"
        self.doc_id = u'nlk.44876'

        # # EDD chyba 1
        # # https://cpk-front.mzk.cz/Record/kkpc.PAK01-000373142/ZiskejEdd
        # # Zcela chybí 996, viz
        # # https://cpk-front.mzk.cz/Record/kkpc.PAK01-000373142/Details
        # self.service = u'edd'
        # self.doc_alt_ids = u"[u'anl.ANL01-001855139']"
        # self.doc_id = u'kkpc.PAK01-000373142'

        # # EDD chyba 2.1
        # # https://cpk-front.mzk.cz/Record/mzk.MZK01-000812182/ZiskejEdd
        # self.service = u'edd'
        # self.doc_alt_ids = u""  # FIXME maybe
        # self.doc_id = u'mzk.MZK01-000812182'

        # # EDD chyba 2.2
        # # https://cpk-front.mzk.cz/Record/nkp.NKC01-003166173/ZiskejEdd
        # self.service = u'edd'
        # self.doc_alt_ids = u""  # FIXME maybe
        # self.doc_id = u'nkp.NKC01-003166173'

        # # EDD chyba 2.3
        # # https://cpk-front.mzk.cz/Record/nkp.NKC01-003327330/ZiskejEdd
        # self.service = u'edd'
        # self.doc_alt_ids = u""  # FIXME maybe
        # self.doc_id = u'nkp.NKC01-003327330'

        # # EDD was OK
        # # https://cpk-front.mzk.cz/Record/nkp.NKC01-003305242/ZiskejEdd
        # self.service = u'edd'
        # self.doc_alt_ids = u""  # FIXME maybe
        # self.doc_id = u'nkp.NKC01-003305242'

        # EDD 645
        # {u'entity_id': u'', u'user_id': u'', u'service': u'edd', u'doc_alt_ids': [u'svkpk.PNA01-000091445', u'nlk.44876', u'nkp.NKC01-000318970', u'svkul.3180497', u'mzk.MZK01-000310564', u'svkkl.kl_us_cat*c212271'], u'source_id': u'www.knihovny.cz', u'doc_id': u'nlk.44876'}
        self.service = u'edd'
        self.doc_alt_ids = u"[u'svkpk.PNA01-000091445', u'nlk.44876', u'nkp.NKC01-000318970', u'svkul.3180497', u'mzk.MZK01-000310564', u'svkkl.kl_us_cat*c212271']"
        self.doc_id = u'nlk.44876'

        self.service = u'edd'
        self.doc_alt_ids = u""
        self.doc_id = u'nlk.182407'

        # EDD 663
        # {u'source_id': u'www.knihovny.cz', u'entity_id': u'', u'doc_id': u'svkhk.HKA01-000545971', u'service': u'edd', u'doc_alt_ids': [u'anl.ANL01-000443932', u'svkhk.HKA01-000545971']}
        self.service = u'edd'
        self.doc_alt_ids = u"[u'anl.ANL01-000443932', u'svkhk.HKA01-000545971']"
        self.doc_id = u'svkhk.HKA01-000545971'

        # EDD článek x časopis
        self.service = u'edd'
        self.doc_alt_ids = u""
        self.doc_id = u'anl.ANL01-000573786'

        # # Hořící most
        # # {u'source_id': u'www.knihovny.cz', u'doc_id': u'mzk.MZK01-001647274', u'entity_id': u'', u'user_id': u'', u'doc_alt_ids': [u'nkp.NKC01-003027724', u'vkol.SVK01-001251169', u'mzk.MZK01-001647274', u'mkkh.297732', u'caslin.SKC01-007852889', u'okpb.KN3183000005734146', u'svkos.MVK01-000753512', u'mkuo.90702', u'mkp.4429622']}
        # self.service = u'mvs'
        # self.doc_id = u'mzk.MZK01-001647274'
        # self.doc_alt_ids = u"[u'nkp.NKC01-003027724', u'vkol.SVK01-001251169', u'mzk.MZK01-001647274', u'mkkh.297732', u'caslin.SKC01-007852889', u'okpb.KN3183000005734146', u'svkos.MVK01-000753512', u'mkuo.90702', u'mkp.4429622']"


        self.doc_url = get_cpk_url(doc_id=self.doc_id)

        if DEBUG_MISSING_DOC:
            logger_dmd.info('doc_id: {}'.format(self.doc_id))

        super(CpkFakeView, self).call()
