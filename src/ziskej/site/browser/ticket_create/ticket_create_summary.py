# -*- coding: utf-8 -*-
"""Ticket Create form"""

from __future__ import print_function

import json

from DateTime import DateTime
from plone.protect.interfaces import IDisableCSRFProtection
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.interface import alsoProvides, Invalid

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.browser.ticket_create.ticket_create_base import (
    TicketView,
    logger,
    logger_dmd,
    DEBUG,
    DEBUG_CPK_POST_VALUE,
    DEBUG_MISSING_DOC,
    DEBUG_EDD,
    USE_SESSION_STORAGE,
    )
from ziskej.site.utils import (
    count_wd,
    datetime_add_wd,
    datetime_normalize,
    )
from ziskej.site.utils_lib import pprint
from ziskej.site.ticket_helpers import (
    create_ticketmvs,
    get_cpk_url,
    generate_fullname_manual,
    generate_quote_edda,
    generate_quote_eddb,
    )
from ziskej.site.ticket_load_balancing import (
    parse_cpk_post,
    CpkApiException,
    LoadBalancingException,
    )
from ziskej.site.validators import validate_email
from ziskej.site.ziskej_parameters import ziskej_parameter


class CreateSummaryView(TicketView):
    template = ViewPageTemplateFile('ticket_create_summary_view.pt')

    def call(self):
        """Základní metoda Získej Browser View, v okamžiku jejího volání je
        vše obecné nastaveno a čeká se na zpracování zde.
        """

        if DEBUG:
            print('-- CreateSummaryView.call')
        if DEBUG_MISSING_DOC:
            logger_dmd.info('-- CreateSummaryView.call')
            self.debug_session()

        super(CreateSummaryView, self).call()

        self._initial_settings()
        self._vacation_setup()

        if self.is_submit:
            self.is_manual = self.request.get('doc_manual', None)

            # Zpracovat submit včetně validace a vytvoření objednávky
            # submit_core obsahuje add_message
            obj = self.submit_any(not self.is_manual)

            # Podařilo se, přesměrovat na novou objednávku
            if obj:
                self.next_url = obj.absolute_url()
                self.redirect(self.next_url)
                return

            # Validace selhala, nastavit speciální akci a zobrazit formulář
            self.action = 'submit'
            if self.is_manual:
                self.show_form = True
                self.show_form_manual = True
            else:
                self.show_form = True
                self.show_form_edd_auto = True

        # prepare form - initial or after validation failure

        # default action - entry point when coming from CPK, from Shibboleth etc.
        if self.action is None:
            self.show_cpk_search_another = True
            return self.action_auto()

        # action when reader or librarian uses Vytvořit požadavek icon in
        # header
        if self.action == 'create':
            self.action_manual()
            self.show_cpk_search = True
            if self.expire_cookie_cpkpost():
                return
            if self.is_librarian:
                self.show_create_ticket = True
                self.is_manual = True
                self.show_form = True
                self.show_form_manual = True
                if self.IS_EDD_AT_TEST:
                    self.show_test_edd_helper = True

        # validation failed
        if self.action == 'submit':
            self.show_cpk_search = False
            if self.is_librarian:
                self.show_create_ticket = True
                if self.IS_EDD_AT_TEST:
                    self.show_test_edd_helper = True

        if DEBUG:
            print('-- CreateSummaryView.call END')

    def _initial_settings(self):
        """Úvodní nastavení pro tento pohled, všechny jeho varianty, zejména
        defaulty.
        """

        self.next_url = self.portal_url + '/ticket_create_summary'
        self.error = False
        self.is_expired = False
        self.url_manual_create = None
        self.show_cpk_search = False
        self.show_cpk_search_another = False
        self.show_doc_info = False
        self.show_create_ticket = False
        self.show_ticket_type = True
        self.show_test_edd_helper = False
        # plný formulář pro manuální nebo jen doplnění pro edd auto
        self.show_form = False
        # plný formulář pro manuální
        self.show_form_manual = False
        # jen doplnění pro edd auto
        self.show_form_edd_auto = False
        self.title = u'Vytvořit objednávku'
        self.ticket_created = False
        self.error_message = u''
        self.is_manual = None
        self.is_submit = bool(self.request.get('submit', None))
        self.action = self.request.get('action', None)
        if self.action not in (None, 'create', ):
            self.action = None

        # Zobrazit povolené služby pro ŽK
        self.is_mvs_zk = bool(self.library and self.library.Is_mvs_zk())
        self.is_mrs_zk = self.IS_MRS and False
        # FIXME-MRS
        # self.is_mrk_zk = bool(
        #     self.IS_MRS and self.library and self.library.Is_mrs_zk())
        self.is_edd_zk = bool(
            self.IS_EDD and self.library and self.library.Is_edd_zk())
        if not self.library:
            # V tuto chvíli je správně, ale v minulosti by nebylo a možná
            # v budoucnu opět nebude – v závislosti na Shibboleth přihlášení
            # čtenáře přímo do Získej UI.
            self.add_message(
                u'Nejste přihlášeni, nelze vytvářet objednávky.',
                message_type='error')
            self.redirect()
            return
        if not self.is_mvs_zk and not self.is_mrs_zk and not self.is_edd_zk:
            self.add_message(
                u'Vaše knihovna nenabízí žádnou službu jako ŽK, nelze vytvářet '
                u'objednávky.',
                message_type='error')
            self.redirect()
            return

        self.reader_lid_label = u'Čtenář ID'
        if self.is_librarian or self.is_reader:
            reader_lid_label = self.library.reader_lid_label
            if reader_lid_label:
                self.reader_lid_label = reader_lid_label

        self.reader_lid_help = u'Čtenář ID nápověda'
        if self.is_librarian or self.is_reader:
            reader_lid_help = self.library.reader_lid_help
            if reader_lid_help:
                self.reader_lid_help = reader_lid_help

        self.show_reader_lid = self.is_librarian or (self.is_reader and
                not self.library.get_reader_lid(self.user_id))

    def _vacation_setup(self):
        """Obecné nastavení dovolených, zabraňující vytvořit objednávku těsně
        před dovolenou.
        """

        # Čtenář nemůže vytvořit objednávku v době dovolené a 3 dny před.

        # Ziskej parametr za jak dlouho může čtenář objednat.
        rd_cannt_order = ziskej_parameter('reader_cannot_order')
        # Získej parametr jak dlouho před dovolenou nesmí čtenář objednat.
        days_before = ziskej_parameter('mvs_zk_deactivate_before_holiday')

        vacation_start_dt = None
        if self.is_librarian or self.is_reader:
            # Dovolená, start 0:00, end 23:59, tz naive
            vacation_start_dt, vacation_end_dt = self.library._parse_vacation()

        # Knihovna má nastavenou dovolenou.
        if vacation_start_dt:
            now = DateTime()
            x = self.library._count_open_days(
                start_dt=now, end_dt=vacation_end_dt)

            # Počet pracovních dnů dovolené
            num_days_vac = count_wd(vacation_start_dt, vacation_end_dt)
            max_days = days_before + rd_cannt_order + num_days_vac

            # Zvláštní případ, kdy čtenář nemůže objednat o jeden den navíc.
            vacation_order = rd_cannt_order + 1
            vacation_order_dt = datetime_normalize(
                datetime_add_wd(vacation_order))
            rd_cannt_order_dt = datetime_normalize(
                datetime_add_wd(rd_cannt_order))

            # Pokud dovolená začíná za méně než rd_cannt_order dnů, tak celkový
            # počet wd dnů je počet dnů dovolené a rd_cannt_order.
            if rd_cannt_order_dt > vacation_start_dt or \
                    rd_cannt_order_dt == vacation_start_dt:
                all_days = rd_cannt_order + num_days_vac
                self.today_add_xwd = self.today_add_wd_datapicker(all_days)

            # Pokud začíná za rd_cannt_order + 1 den tak použije max_days.
            elif vacation_start_dt == vacation_order_dt:
                self.today_add_xwd = self.today_add_wd_datapicker(max_days)

            # Pokud dovolená začíná za dlouho tak čtenář nesmí objednat 3wd
            # dopředu a jinak bez omezení.
            else:
                self.today_add_xwd = self.today_add_wd_datapicker(rd_cannt_order)

        # Knihovna nemá nastavenou dovolenou.
        else:
            self.today_add_xwd = self.today_add_wd_datapicker(rd_cannt_order)

    def action_auto(self):
        """Základní zpracování přicházejícího požadavku z CPK, tedy auto,
        nezávisle na službě, příprava dat pro zobrazení formuláře.
        """

        portal = self.portal

        # CPK POST
        self.cpk_post = self.load_cpk_post()

        if DEBUG or DEBUG_EDD:
            print(" ")
            print("cpk_post")
            pprint(self.cpk_post)
            print(" ")

        if self.is_anon:
            if self.request.get('postauth', None):
                raise Exception('Log in failed')

            self.add_message(u'Je nutné se nejdříve přihlásit.')
            url = '{portal_url}/ticket_create?n=2'.format(portal_url=portal.absolute_url())
            self.redirect(url)
            return

        if not self.library:
            logger.exception('ticket_create no self.library for user: ' + str(self.user_id))
            self.add_message(u'Není nastavena žádná knihovna, nelze vytvářet objednávky.')
            self.redirect()
            return

        service = self.cpk_post.get('service', u'mvs')
        if service == 'mvs':
            if not self.library.Is_mvs_zk():
                self.add_message(u'Vaše knihovna nenabízí MVS jako ŽK, nelze vytvářet objednávky.', message_type='error')
                self.redirect()
                return None
        elif service == 'edd':
            if not self.library.Is_edd_zk():
                self.add_message(u'Vaše knihovna nenabízí EDD jako ŽK, nelze vytvářet objednávky.', message_type='error')
                self.redirect()
                return None
        else:
            # Nemělo by nastat a pokud nastane, je chyba v Získej, že nechalo
            # invalidní service dojít až sem
            raise Exception('Invalid service in CPK POST')

        # if self.is_reader and not self.library.Is_any_zk(
        #         ignore_vacation=True, now_dt=self.testing_now_dt):
        #     logger.exception('ticket_create library openhours and/or vacation prevent service as ZK for user: ' + str(self.user_id))
        #     self.add_message(u'Knihovna má provozní dobu a/nebo dovolenou, která znemožňuje vytvořit objednávku v tuto chvíli.')
        #     self.redirect()
        #     return

        if self.is_reader:
            reader = self.ziskej_user_info['user']
            if not reader.gdpr_registration or not reader.gdpr_data:
                if USE_SESSION_STORAGE:
                    if self.cpk_post:
                        if DEBUG:
                            print('alsoProvides(self.request, IDisableCSRFProtection)')
                        alsoProvides(self.request, IDisableCSRFProtection)
                        reader.cpkpost_json = json.dumps(self.cpk_post, ensure_ascii=False)
                if not reader.gdpr_registration:
                    next_url = reader.absolute_url() + '/reader_init_reg'
                else:
                    next_url = reader.absolute_url() + '/reader_init_data'
                self.redirect(next_url)
                return
            if USE_SESSION_STORAGE:
                if self.cpk_post and reader.cpkpost_json:
                    logger.warning('self.cpk_post and reader.cpkpost_json')
                    if DEBUG_MISSING_DOC:
                        logger_dmd.info('self.cpk_post')
                        if self.cpk_post:
                            logger_dmd.info('cpk keys: {}'.format(self.cpk_post.keys()))
                        logger_dmd.info('reader.cpkpost_json')
                        logger_dmd.info('cpk json: {}'.format(reader.cpkpost_json))
                if reader.cpkpost_json:
                    self.cpk_post = json.loads(reader.cpkpost_json)
                    session.set("cpk_post", self.cpk_post)
                    if DEBUG:
                        print('alsoProvides(self.request, IDisableCSRFProtection)')
                    alsoProvides(self.request, IDisableCSRFProtection)
                    reader.cpkpost_json = u''
                    if DEBUG_MISSING_DOC:
                        logger_dmd.info('reader with cpk_json')
                        if self.cpk_post:
                            logger_dmd.info('cpk keys: {}'.format(self.cpk_post.keys()))

        self.is_expired = True
        self.doc_id = None
        self.doc_title = None
        self.doc_fullname = None
        self.doc_url = None
        self.fee_est = u''
        ignore_missing_unit_ids = False

        if self.cpk_post:

            self.service = self.cpk_post.get('service', None)
            if self.service is None:
                self.service = u'mvs'
            elif self.service == 'mvs':
                self.service = u'mvs'
            elif self.service == 'edd':
                self.service = u'edd'
            else:
                raise Exception('Invalid CPK POST')  # FIXME
            if self.service == u'mvs':
                self.service_label = u'MVS'
            elif self.service == u'edd':
                self.service_label = u'EDD'
                ignore_missing_unit_ids = True  # FIXME pokusit se to více omezit, pokud lze

            include_fee_est = False
            if self.service == 'mvs':
                include_fee_est = bool(self.library.mvs_zk_cena is None)

            library_zk_sigla = None
            if include_fee_est:
                try:
                    library_zk_sigla = self.library.sigla.lower()
                except AttributeError:
                    pass

            if DEBUG or DEBUG_EDD:
                print('calling lb_data = parse_cpk_post(self.cpk_post)')

            self.debug_cpk_post = '{value}'.format(value=self.cpk_post)
            if DEBUG:
                pprint(self.debug_cpk_post)
                # <span style="color:White" id="debug_cpk_post"></span>

            self.log_time('transaction before parse_cpk_post')
            self._transaction()

            try:
                lb_data = parse_cpk_post(
                    self.portal,
                    self.cpk_post,
                    library_zk_sigla=library_zk_sigla,
                    include_fee_est=include_fee_est,
                    ignore_missing_unit_ids=ignore_missing_unit_ids,
                    )
            except LoadBalancingException, e:
                lb_data = None
                self.error = True
                self.error_message = u'{e}'.format(e=e)
                logger.warning(u'LoadBalancingException in parse_cpk_post: ' + self.error_message)
            except CpkApiException, e:
                lb_data = None
                self.error = True
                detail_error = 'Nepodarilo se nacist data, je mozne, ze nebezi async pro CPK API nebo doslo ke zmene rozhrani.'
                self.error_message = u'Interní chyba v komunikaci s CPK nebo výpadek CPK, operátoři byli informováni.  Zkuste akci opakovat později.  Detaily: {error_message}'.format(error_message=str(e))
                self.error_notify('CpkApiException', e, 'parse_cpk_post', detail_error=detail_error)
                logger.exception('CpkApiException in parse_cpk_post: ' + str(e))
                raise
            except Exception, e:
                lb_data = None
                self.error = True
                self.error_message = u'Interní chyba v komunikaci s CPK, operátoři byli informováni.  Zkuste akci opakovat později.'
                detail_error = 'Nepodarilo se nacist data, je mozne, ze nebezi async pro CPK API nebo doslo ke zmene rozhrani nebo jina, zatim neznama chyba.'
                self.error_notify('Exception', e, 'parse_cpk_post', detail_error=detail_error)
                logger.exception('Exception in parse_cpk_post: ' + str(e))
                raise

            self.log_time('transaction after parse_cpk_post')
            self._transaction()

            if lb_data:
                if DEBUG or DEBUG_EDD:
                    print(' ')
                    print('lb_data:')
                    pprint(lb_data)
                    print(' ')
                self.doc_id = lb_data['id']
                self.doc_title = lb_data['fullname']
                self.doc_fullname = lb_data['fullname']
                self.doc_url = get_cpk_url(doc_id=lb_data['id'])

                if self.service == 'mvs':
                    if self.library.mvs_zk_cena is not None:
                        self.fee_est_label = u'Cena:'
                        self.fee_est = u'{} Kč'.format(self.library.mvs_zk_cena)
                    else:
                        self.fee_est_label = u'Odhadovaná cena:'
                        fee_est = lb_data['fee_est']
                        if DEBUG:
                            print('fee_est')
                            pprint(fee_est)
                            print(' ')
                        if fee_est['count'] > 0:
                            if fee_est['min'] is None:
                                fee_est['min'] = 0
                            if fee_est['max'] is None:
                                fee_est['max'] = fee_est['avg']
                            self.fee_est = u'{avg} Kč ({min} - {max} Kč)'.format(**fee_est)

                if USE_SESSION_STORAGE:
                    session.set("cpk_post_lb_data", lb_data)
                    if DEBUG or DEBUG_EDD:
                        print('session.set("cpk_post_lb_data", lb_data)')

                self.is_expired = not bool(self.doc_id)
                if not self.is_expired:
                    self.show_create_ticket = True

            self.is_manual = False
            if self.service == 'edd':
                self.show_form = True
                self.show_form_edd_auto = True
                if self.IS_EDD_AT_TEST:
                    self.show_test_edd_helper = True
            self.show_doc_info = bool(self.is_reader_or_librarian and not self.error)
            self.url_manual_create = self.portal_url + '/ticket_create_summary?action=create'

        elif self.is_librarian:
            self.doc_id = None
            self.doc_title = u''
            self.doc_fullname = u''
            self.doc_url = u''
            self.fee_est = u''
            self.is_expired = False
            self.is_manual = True
            self.show_form = True
            self.show_form_manual = True
            self.show_doc_info = False

        else:
            logger.info('create ticket cpk_post expired')

    def action_manual(self):
        """Základní zpracování interního odkazu Nová objednávka, tedy manual,
        nezávisle na službě, jen validace kontextu.
        """

        portal = self.portal

        if self.is_anon:
            if self.request.get('postauth', None):
                raise Exception('Log in failed')

            self.add_message(u'Je nutné se nejdříve přihlásit.')
            url = '{portal_url}/ticket_create?n=2'.format(portal_url=portal.absolute_url())
            self.redirect(url)
            return

        if not self.library:
            logger.exception('ticket_create no self.library for user: ' + str(self.user_id))
            self.add_message(u'Není nastavena žádná knihovna, nelze vytvářet objednávky.')
            self.redirect()
            return

        #print("self.library.Is_any_zk(): ", self.library.Is_any_zk())
        if not self.library.Is_any_zk():
            logger.exception('ticket_create library has no service as ZK for user: ' + str(self.user_id))
            self.add_message(u'Knihovna neposkytuje žádnou službu jako ŽK, nelze vytvářet objednávky.')
            self.redirect()
            return

    def submit_any(self, is_auto):
        """Základní zpracování odeslaného formuláře nezávisle na auto / manual
        a na službě.
        """

        is_manual = not is_auto
        if not self.is_reader and not self.is_librarian:
            self.add_message(u'Vaše přihlášení expirovalo, přihlašte se znovu.', message_type='warning')
            self.redirect()
            return None
        if self.is_reader and is_manual:
            raise Exception('Invalid input')

        reader = u''
        reader_note = u''

        if self.is_reader:
            created_by = 'reader'
            reader = self.user_id
            reader_note = self.text_from_request('reader_note')
            if not reader_note:
                reader_note = u''

        librarian = u''
        reader_lastname = u''
        reader_firstname = u''
        library_zk_note = u''
        reader_approval = u''

        if self.is_librarian:
            created_by = 'librarian'

            librarian = self.user_id

            reader_firstname = self.text_from_request('reader_firstname')
            if not reader_firstname:
                reader_firstname = u''

            reader_lastname = self.text_from_request('reader_lastname')
            if not reader_lastname:
                reader_lastname = u''

            library_zk_note = self.text_from_request('library_zk_note')
            if not library_zk_note:
                library_zk_note = u''

            reader_email = self.text_from_request('reader_email')
            if not reader_email:
                reader_email = u''

            reader_approval_lov = (u'', u'ok', u'stop', u'reject', )
            reader_approval = self.text_from_request('reader_approval', lov=reader_approval_lov)

        reader_lid = self.text_from_request('reader_lid')
        if not reader_lid:
            reader_lid = u''

        reader_date = self.date_from_request('reader_date', result_type='iso')

        # Validation

        try:
            validate_email(reader_email)
        except Invalid, e:
            self.error = True
            self.error_message = u'E-mail pro čtenáře: {} '.format(e)
        
        # reader_email FIXME

        # obj dict
        obj_dict = dict(
            # default mvs, na edd případně nastaveno v _submit_edd_any
            # nebo _submit_doc_cpk
            ticket_type = 'mvs',
            created_by = created_by,
            created_date = self.dt_as(DateTime(), 'iso'),
            reader = reader,
            reader_lid = reader_lid,
            reader_firstname = reader_firstname,
            reader_lastname = reader_lastname,
            reader_approval = reader_approval,
            reader_email = reader_email,
            library_zk = self.ziskej_user_info['library'].sigla.lower(),
            library_zk_title = self.ziskej_user_info['library'].Title(),
            librarian = librarian,
            reader_date = reader_date,
            library_zk_note = library_zk_note,
            unread_zk = self.is_reader,
            lb_state = u'',
            lb_manual_library = u'',
            lb_manual_library_sent = u'',
            )

        # reader_lid
        if not self.show_reader_lid:
            if reader:
                value = self.library.get_reader_lid(reader)
                if value is None:
                    value = u''
            else:
                # FIXME Další vylepšení by bylo odhadnout reader_lid ze známých
                # údajů
                value = u''
            obj_dict['reader_lid'] = value
        if is_manual:
            obj_dict = self._submit_manual(obj_dict)
        else:
            obj_dict = self._submit_auto(obj_dict)

        if not obj_dict:
            return None

        # create and return ticket
        obj = self._create_ticket(obj_dict, reader_note=reader_note)
        if obj.is_edd:
            obj.calculate_and_update_edd_data()
        return obj

    def _submit_manual(self, obj_dict):
        """Základní zpracování odeslaného formuláře pro manual, nezávisle na
        službě.
        """

        if not self.is_librarian:
            raise Exception('Invalid input')

        # MVS , MRS, EDD
        ticket_type = self.text_from_request('ticket_type',
            lov=('mvs', 'mrs', 'edda', 'eddb', ))
        if not ticket_type:
            ticket_type = 'mvs'

        if ticket_type == 'mvs':
            if not self.library.Is_mvs_zk():
                self.add_message(u'Vaše knihovna nenabízí MVS jako ŽK, nelze vytvářet objednávky.',message_type='error')
                self.redirect()
                return None
        elif 'edd' in ticket_type:
            if not self.library.Is_edd_zk():
                self.add_message(u'Vaše knihovna nenabízí EDD jako ŽK, nelze vytvářet objednávky.',message_type='error')
                self.redirect()
                return None
        # MVS, MRS
        if ticket_type in (u'mvs', u'mrs'):

            # MVS celé, MRS část
            obj_dict = self._submit_mvs_mrs_base_manual(
                obj_dict, ticket_type)

            if ticket_type == u'mrs':
                # MRS zbývající část
                obj_dict = self._submit_doc_manual_mrs(obj_dict)
            else:
                obj_dict['mrs_data'] = u''

            if self.error:
                return None

            # citace vytvořená z manuálních dat
            obj_dict['doc_fullname'] = generate_fullname_manual(obj_dict)

        # EDD
        elif ticket_type in ('edda', 'eddb', ):
            if DEBUG or DEBUG_EDD:
                print(" ")
                print("self.request.form")
                pprint(self.request.form)
                print(" ")

            obj_dict = self._submit_edd_any(
                obj_dict,
                is_article=bool(ticket_type == 'edda'),
                is_manual=True)

            if self.error:
                return None

        return obj_dict

        # """
        # MRS

        # {'_authenticator': '4ba6f91bd596602976469f9ed9c77a1d2267964b',
        # 'doc_author': 'Autor',
        # 'doc_isbn': 'ISBN',
        # 'doc_issn': 'ISSN',
        # 'doc_issuer': 'Tady 2019',
        # 'doc_manual': '1',
        # 'doc_note': 'Poznámka k dokumentu ',
        # 'doc_number': '2019',
        # 'doc_signature': 'Signatura',
        # 'doc_title': 'Název',
        # 'is_manual': 'true',
        # 'library_zk_note': '',
        # 'mrs_par_article': 'Článek',
        # 'mrs_par_city': 'Praha',
        # 'mrs_par_color': 'on',
        # 'mrs_par_ic': '12345678',
        # 'mrs_par_keepsize': 'on',
        # 'mrs_par_mail': 'info@example.com',
        # 'mrs_par_name': 'Jméno Příjmení',
        # 'mrs_par_orgname': 'Organizace',
        # 'mrs_par_phone': '123456789',
        # 'mrs_par_range': '12-34',
        # 'mrs_par_sent': 'sent',
        # 'mrs_par_singleside': 'on',
        # 'mrs_par_street': 'Ulice 123',
        # 'mrs_par_zip': '10000',
        # 'reader_approval': '',
        # 'reader_date': '13.02.2019',
        # 'reader_firstname': 'Pepa',
        # 'reader_lastname': 'Vonásek',
        # 'reader_lid': '',
        # 'submit': 'submit',
        # 'ticket_type': 'mrs'}
        # """

    def _submit_mvs_mrs_base_manual(self, obj_dict, ticket_type):
        """Zpracování části formuláře společné pro MVS a MRS, vždy jen manual.
        """

        # manual 01: Název knihy/časopisu*
        doc_title = self.text_from_request('doc_title')
        if not doc_title:
            doc_title = u''

        # manual 02: Rok/ročník/číslo
        doc_number = self.text_from_request('doc_number')
        if not doc_number:
            doc_number = u''

        # manual 03: Autor *
        doc_author = self.text_from_request('doc_author')
        if not doc_author:
            doc_author = u''

        # manual 04: Místo a rok vydání
        doc_issuer = self.text_from_request('doc_issuer')
        if not doc_issuer:
            doc_issuer = u''

        # manual 05: ISBN
        doc_isbn = self.text_from_request('doc_isbn')
        if not doc_isbn:
            doc_isbn = u''

        # manual 06: ISSN
        doc_issn = self.text_from_request('doc_issn')
        if not doc_issn:
            doc_issn = u''

        # manual 07: Signatura
        doc_signature = self.text_from_request('doc_signature')
        if not doc_signature:
            doc_signature = u''

        # manual 08: Poznámka k dokumentu
        doc_note = self.text_from_request('doc_note')
        if not doc_note:
            doc_note = u''

        # manual 09: Datum požadované čtenářem - viz submit_core
        # reader_date = self.date_from_request('reader_date', result_type='iso')

        # manual 10: Přihlašovací jméno do knihovny (toto může být pro každou
        # knihovnu jiné) - viz submit_core
        # reader_lid = self.text_from_request('reader_lid')

        # manual 11: Jméno čtenáře
        # reader_firstname - viz submit_core

        # manual 12: Příjmení čtenáře
        # reader_lastname - viz submit_core

        # manual 13: Ověření čtenáře - viz submit_core
        # reader_approval_lov = (u'', u'ok', u'stop', u'reject', )
        # reader_approval = self.text_from_request('reader_approval', lov=reader_approval_lov)

        # manual 14: Poznámka knihovníka - viz submit_core
        # library_zk_note = self.text_from_request('library_zk_note')
        # if not library_zk_note:
        #     library_zk_note = u''

        obj_dict['ticket_type'] = ticket_type
        obj_dict['created_by'] = 'librarian'

        obj_dict['doc_manual'] = True
        obj_dict['doc_id'] = u''
        obj_dict['doc_title'] = doc_title
        obj_dict['doc_number'] = doc_number
        obj_dict['doc_author'] = doc_author
        obj_dict['doc_issuer'] = doc_issuer
        obj_dict['doc_isbn'] = doc_isbn
        obj_dict['doc_issn'] = doc_issn
        obj_dict['doc_signature'] = doc_signature
        obj_dict['doc_note'] = doc_note
        obj_dict['doc_data'] = u''
        obj_dict['docs_data'] = u''

        # validation
        if not doc_title or not doc_author:
            self.error = True
            self.error_message += u'Název titulu a autor jsou vyžadovány.  '

        return obj_dict

    def _submit_mrs_manual(self, obj_dict):
        """Zpracování části formuláře specifické pro MRS, vždy jen manual.
        """


        if not self.IS_MRS:
            raise Exception('MRS is diabled in this instance')

        """
        'mrs_par_ic': '12345678',
        'mrs_par_mail': 'info@example.com',
        'mrs_par_phone': '123456789',

        'mrs_par_article': 'Článek',
        'mrs_par_range': '12-34',
        """

        mrs = dict()

        mrs['color'] = bool(self.text_from_request('mrs_par_color'))
        mrs['singleside'] = bool(self.text_from_request('mrs_par_singleside'))
        mrs['keepsize'] = bool(self.text_from_request('mrs_par_keepsize'))

        mrs['sent'] = self.text_from_request('mrs_par_sent', lov=(u'sent', u'pick', ))
        if not mrs['sent']:
            raise Exception('Invalid request')

        if mrs['sent'] == u'sent':

            mrs['orgname'] = self.text_from_request('mrs_par_orgname')
            if not mrs['orgname']:
                mrs['orgname'] = u''

            mrs['name'] = self.text_from_request('mrs_par_name')
            if not mrs['name']:
                mrs['name'] = u''

            mrs['street'] = self.text_from_request('mrs_par_street')
            if not mrs['street']:
                mrs['street'] = u''

            mrs['city'] = self.text_from_request('mrs_par_city')
            if not mrs['city']:
                mrs['city'] = u''

            mrs['zip'] = self.text_from_request('mrs_par_zip')
            if not mrs['zip']:
                mrs['zip'] = u''

            if not (mrs['orgname'] and mrs['name'] and mrs['street'] and 
                    mrs['city'] and mrs['zip']):
                self.error = True
                self.error_message += u'Jméno organizace i žadatele a plná adresa jsou vyžadovány.  '

        mrs['ic'] = self.text_from_request('mrs_par_ic')
        if not mrs['ic']:
            mrs['ic'] = u''

        mrs['mail'] = self.text_from_request('mrs_par_mail')
        if not mrs['mail']:
            mrs['mail'] = u''

        mrs['phone'] = self.text_from_request('mrs_par_phone')
        if not mrs['phone']:
            mrs['phone'] = u''

        mrs['article'] = self.text_from_request('mrs_par_article')
        if not mrs['article']:
            mrs['article'] = u''

        mrs['range'] = self.text_from_request('mrs_par_range')
        if not mrs['range']:
            mrs['range'] = u''

        # FIXME use reader_email
        if not mrs['mail']:
            self.error = True
            self.error_message += u'E-mail žadatele je vyžadován.  '

        if not mrs['article'] or not mrs['range']:
            self.error = True
            self.error_message += u'Název článku i rozsah jsou vyžadovány.  '

        obj_dict['mrs_data'] = json.dumps(mrs, ensure_ascii=False)

        return obj_dict

    def _submit_auto(self, obj_dict):
        """Zpracování formuláře pro auto (tedy po příchodu z CPK), nezávisle na
        službě.
        """

        # CPK POST, CPK docs (bez dostupnosti)
        self.cpk_post = None
        self.lb_data = None
        if USE_SESSION_STORAGE:
            # FIXME do not use session
            # read session
            # see https://docs.plone.org/develop/plone/sessions/session_variables.html
            sdm = self.context.session_data_manager
            session = sdm.getSessionData(create=False)
            if session is not None:
                self.cpk_post = session.get("cpk_post", None)
                self.lb_data = session.get("cpk_post_lb_data", None)
                if DEBUG:
                    print('self.cpk_post and self.lb_data loaded from session')
                    print('self.cpk_post:')
                    pprint(self.cpk_post)
                    print(' ')
                    print('self.lb_data:')
                    pprint(self.lb_data)
                    print(' ')
            else:
                if DEBUG:
                    print('session is missing')
        else:
            self.cpk_post = self.load_cpk_post()

        if not self.ziskej_user_info or self.ziskej_user_info['library'] is None:
            self.error = True
            self.error_message = u'Chybné přihlášení, nelze identifikovat knihovnu'
            return None

        if not self.cpk_post:
            self.error = True
            self.error_message = u'Váš výběr expiroval, zahajte celý proces znovu prosím'
            return None

        service = self.cpk_post.get('service', u'mvs')
        if service == 'mvs':
            if not self.library.Is_mvs_zk():
                self.error = True
                self.error_message = u'Vaše knihovna nenabízí MVS'
                return None
        elif service == 'edd':
            if not self.library.Is_edd_zk():
                self.error = True
                self.error_message = u'Vaše knihovna nenabízí EDD'
                return None
        else:
            # Nemělo by nastat a pokud nastane, je chyba v Získej, že nechalo
            # invalidní service dojít až sem
            raise Exception('Invalid service in CPK POST')

        # if self.lb_data is None:
        #     if USE_SESSION_STORAGE:
        #         # nemělo by nastat, ale nic se neděje
        #         logger.warning('session has cpk_post and does not have cpk_docs')

        #     include_fee_est = self.library.mvs_zk_cena is None

        #     library_zk_sigla = None
        #     if include_fee_est:
        #         try:
        #             library_zk_sigla = self.library.sigla.lower()
        #         except AttributeError:
        #             pass

        #     lb_data = parse_cpk_post(self.portal,
        #                              self.cpk_post,
        #                              library_zk_sigla=library_zk_sigla,
        #                              include_fee_est=include_fee_est)

        # else:
        #     lb_data = self.lb_data

        include_fee_est = False
        if service == 'mvs':
            include_fee_est = self.library.mvs_zk_cena is None
        library_zk_sigla = None
        if include_fee_est:
            try:
                library_zk_sigla = self.library.sigla.lower()
            except AttributeError:
                pass

        ignore_missing_unit_ids = False
        # EDD článek
        if service == 'edd' and self.text_from_request(
                'ticket_type', lov=('edda', 'eddb', )) == 'edda':
            ignore_missing_unit_ids = True

        self.log_time('transaction before parse_cpk_post')
        self._transaction()

        lb_data = parse_cpk_post(
            self.portal,
            self.cpk_post,
            library_zk_sigla=library_zk_sigla,
            include_fee_est=include_fee_est,
            include_doc_data=True,
            ignore_missing_unit_ids=ignore_missing_unit_ids,
            )
        if DEBUG:
            print(' ')
            print('lb_data:')
            pprint(lb_data)
            print(' ')

        self.log_time('transaction after parse_cpk_post')
        self._transaction()

        # ukládat unicode string
        obj_dict['ticket_type'] = u'{}'.format(service)
        obj_dict['doc_manual'] = False
        obj_dict['doc_id'] = lb_data['id']

        if service == 'mvs':
            obj_dict['doc_fullname'] = lb_data['fullname']
            obj_dict['doc_title'] = lb_data['fullname']  # FIXME Proč takto?
            #obj_dict['doc_title'] = lb_data['title']
            #obj_dict['doc_author'] = lb_data['author']
            #obj_dict['doc_number'] = lb_data['year_publication']  # FIXME doc_number nebo doc_issuer?

        elif service == 'edd':
            fullname_source = dict()
            if lb_data['fullname_source']:
                fullname_source = lb_data['fullname_source']
            obj_dict['fullname_source'] = fullname_source

        obj_dict['doc_data'] = lb_data['doc_data_json']
        obj_dict['docs_data'] = lb_data['docs_data_json']

        if service == 'edd':
            ticket_type = self.text_from_request('ticket_type',
                lov=('edda', 'eddb', ))
            if not ticket_type:
                raise Exception('Invalid ticket_type')
            obj_dict = self._submit_edd_any(
                obj_dict,
                is_article=bool(ticket_type == 'edda'),
                is_manual=False)

            if self.error:
                return None

        return obj_dict

    def _submit_edd_any(self, obj_dict, is_article=None, is_manual=True):
        """Zpracovat EDD formulář, verze pro manuální i pro automatickou
        variantu.

        Manuální objednávka

        Formulář pro článek z časopisu (* jsou označeny povinné položky): 
        Název časopisu*, Rok, Ročník, Číslo, Název článku*,
        Stránky od - do, Autor, ISSN, Místo a rok vydání, Citace, Poznámka.

        Formulář pro část knihy (kapitolu, * jsou označeny povinné položky): 
        Název knihy*, Název kapitoly*, Svazek, Stránky od - do, Autor, ISBN,
        Místo a rok vydání, Citace, Poznámka.

        Pokud je zadána citacem, jsou všechny ostatní položky volitelné.  
        Systém neprovádí žádnou validaci citace.
        """

        # Firmulář
        if is_article:
            prefix = 'edda_'
        else:
            prefix = 'eddb_'
        # Název časopisu* / Název knihy*
        doc_title_in = self.text_from_request(
            prefix+'doc_title_in', default=u'')
        # Název článku* / # Název kapitoly*
        doc_title = self.text_from_request(prefix+'doc_title', default=u'')
        # Stránky od - do
        pages_from = self.int_from_request(prefix+'pages_from')
        pages_to = self.int_from_request(prefix+'pages_to')
        # Autor
        doc_author = self.text_from_request(prefix+'doc_author', default=u'')
        # Místo a rok vydání
        doc_issuer = self.text_from_request(prefix+'doc_issuer', default=u'')
        # Svazek
        doc_volume = self.text_from_request(prefix+'doc_volume', default=u'')
        # Citace
        doc_quote = self.text_from_request(prefix+'doc_quote', default=u'')
        # Poznámka
        doc_note = self.text_from_request(prefix+'doc_note', default=u'')

        # Rok*
        doc_number_year = self.text_from_request(
            prefix+'doc_number_year', default=u'')
        # Ročník*
        doc_number_pyear = self.text_from_request(
            prefix+'doc_number_pyear', default=u'')
        # Číslo*
        doc_number_pnumber = self.text_from_request(
            prefix+'doc_number_pnumber', default=u'')

        if is_article:
            # ISSN
            doc_issn = self.text_from_request(prefix+'doc_issn', default=u'')
        else:
            # Svazek
            doc_volume = self.text_from_request(
                prefix+'doc_volume', default=u'')
            # ISBN
            doc_isbn = self.text_from_request(prefix+'doc_isbn', default=u'')
            # ISSN
            doc_issn = self.text_from_request(prefix+'doc_issn', default=u'')

        # edd článek i výňatek: pages_from, pages_to, doc_title_in, doc_quote
        # edd článek a nově i výňatek: doc_number_year, doc_number_pyear,
        #                              doc_number_pnumber
        edd = dict()

        if is_manual:
            obj_dict['doc_id'] = u''
            obj_dict['doc_data'] = u''
            obj_dict['docs_data'] = u''

        else:

            if DEBUG or DEBUG_EDD:
                print(" ")
                print("obj_dict")
                pprint(obj_dict)
                print(" ")

            fullname_source = obj_dict.get('fullname_source', dict())

            if DEBUG or DEBUG_EDD:
                print("fullname_source")
                pprint(fullname_source)
                print(" ")

            # {'isbn': 978-80-252-3007-7,
            # 'issue_number': 4. vyd.,
            # 'issue_place': V Praze,
            # 'issue_year': 2014,
            # 'issuer': Egmont,
            # 'main_author': Flanagan, John,
            # 'other_authors': [Tenklová, Zdena],
            # 'page_number': 258 s.,
            # 'title': Hraničářův učeň.}

            edd['doc_title_in'] = fullname_source.get('title', u'')

            # obj_dict['author'] = fullname_source.get('main_author', u'')
            # FLANAGAN, John. Hraničářův učeň. Kniha druhá, Hořící most. 4. vyd. V Praze: Egmont, 2014. ISBN 978-80-252-3007-7.
            # 4. vyd. V Praze: Egmont, 2014
            issuer = u''
            for name in ('issue_number', 'issue_place', 'issuer', 'issue_year'):
                if fullname_source.get(name, u''):
                    issuer += u'{}'.format(
                        fullname_source.get(name, u''))
                    if name == 'issue_number':
                        issuer += u' '
                    elif name == 'issue_place':
                        issuer += u': '
                    elif name == 'issuer':
                        issuer += u', '
                    elif name == 'issue_year':
                        issuer += u''
            if issuer.endswith(', '):
                issuer = issuer[:-2]
            elif issuer.endswith(': '):
                issuer = issuer[:-2]
            elif issuer.endswith('. '):
                issuer = issuer[:-2]
            obj_dict['doc_issuer'] = issuer.strip()

            obj_dict['doc_isbn'] = fullname_source.get('isbn', u'')
            obj_dict['doc_issn'] = fullname_source.get('issn', u'')

            if 'fullname_source' in obj_dict:
                del obj_dict['fullname_source']

        obj_dict['ticket_type'] = u'edd'
        obj_dict['created_by'] = 'librarian'
        obj_dict['doc_manual'] = is_manual
        edd['is_article'] = is_article

        if is_manual:
            edd['doc_title_in'] = doc_title_in
            obj_dict['doc_issuer'] = doc_issuer
            edd['doc_quote'] = doc_quote

        obj_dict['doc_author'] = doc_author
        obj_dict['doc_title'] = doc_title
        edd['pages_from'] = pages_from
        edd['pages_to'] = pages_to
        obj_dict['doc_note'] = doc_note
        # edd['is_container'] = None  # Má smysl jen pro auto

        edd['doc_number_year'] = doc_number_year
        edd['doc_number_pyear'] = doc_number_pyear
        edd['doc_number_pnumber'] = doc_number_pnumber

        if is_article:
            if is_manual:
                obj_dict['doc_issn'] = doc_issn
        else:
            edd['doc_volume'] = doc_volume
            if is_manual:
                obj_dict['doc_isbn'] = doc_isbn
                obj_dict['doc_issn'] = doc_issn

        # Validovat
        if not (is_manual and doc_quote):
            if not doc_title:
                self.error = True
                if is_article:
                    self.error_message += u'Název časopisu je vyžadován.  '
                else:
                    self.error_message += u'Název knihy je vyžadován.  '

        # Validovat strany
        if pages_from is None and pages_to is None:
            pass
        elif pages_from is None and pages_to is not None:
            self.error = True
            self.error_message += u'Nelze zadat jen poslední stránku.  '
        elif pages_from < 1:
            self.error = True
            self.error_message += u'Nelze zadat první stranu menší jak 1'
        elif pages_from is not None and pages_to is not None:
            if pages_from == pages_to:
                # Zjednodušit bez informování uživatele.
                pages_to = None
            elif pages_from > pages_to:
                self.error = True
                self.error_message += u'První stránka nemůže následovat za poslední.  '
        if pages_from is not None and pages_to is not None and \
                not is_article and pages_to - pages_from + 1 > 20:
            self.error = True
            self.error_message += u'Pro výňatek z monografie je maximální počet stran 20.'
        #FIXME-pocet-stran

        # Validovat EDD manual výňatek nelze ISBN a ISSN
        if not is_article and is_manual:
            if doc_isbn and doc_issn:
                self.error = True
                self.error_message += u'Nedává smysl zadat ISBN a ISSN, zadejte jen jedno z nich, podle typu dokumentu.  '

        if obj_dict['reader_approval'] == 'ok' and obj_dict['reader_email'] == u'':
            self.error = True
            self.error_message += u'Bez validního e-mailu nelze čtenáře ověřit'

        if DEBUG or DEBUG_EDD:
            print("obj_dict")
            pprint(obj_dict)
            print(" ")
            print("edd")
            pprint(edd)
            print(" ")
            print("self.error", repr(self.error))
            print("self.error_message", repr(self.error_message.encode('utf-8', 'replace')))
            print(" ")

        # Validace neprošla, nemá smysl dělat další přípravy pro vytvoření
        # ticketu.
        if self.error:
            return obj_dict

        # Připravit citaci
        if doc_quote:
            obj_dict['doc_fullname'] = doc_quote
        elif is_article:
            obj_dict['doc_fullname'] = generate_quote_edda(obj_dict, edd)
        else:
            obj_dict['doc_fullname'] = generate_quote_eddb(obj_dict, edd)

        # Nastavit EDD json pro vytvoření ticketu.
        obj_dict['edd_data'] = json.dumps(edd, ensure_ascii=False)

        return obj_dict

    def _create_ticket(self, obj_dict, reader_note=u''):
        """Samotné vytvoření objednávky."""

        if DEBUG:
            print(' ')
            print('CreateDoneView.call obj_dict (before create_ticketmvs)')
            pprint(obj_dict)
            print(' ')
            #raise Exception('Debug')

        self.log_time('transaction before create_ticketmvs')
        self._transaction()

        obj = create_ticketmvs(obj_dict)

        if obj:
            if reader_note:
                obj.add_message_txt(u'reader', reader_note)
            if obj_dict['library_zk_note']:
                obj.add_message_txt(u'library_zk', obj_dict['library_zk_note'])
            self.ticket_url = obj.absolute_url()
            self.ticket_created = True

            unit_ids = obj.get_unit_ids()
            additional_message = u''
            if obj.ticket_doc_data_source() == 'auto' and not len(unit_ids):
                additional_message = u'  Z CPK nejsou k dispozici žádné informace o jednotkách, nebude možné použít automatické navržení dožádaných knihoven.'

            # print(" ")
            # print("--------------------------------")
            # print("doc_data:", repr(obj.doc_data))
            # print("--------------------------------")
            # print("docs_data:", repr(obj.docs_data))
            # print("--------------------------------")
            # print("unit_ids:", repr(unit_ids))
            # print("--------------------------------")
            # print(" ")

            user_id = self.ziskej_user_info['user_id']
            # FIXME add fee estimate to history log
            if self.is_reader:
                message = u'Čtenář {user_id} vytvořil objednávku.{additional_message}'.format(
                    user_id=user_id,
                    additional_message=additional_message,
                    )
                obj.add_historylog(message, who=['zk', 'r'])
            elif self.is_librarian:
                message = u'Knihovník {user_id} vytvořil objednávku.{additional_message}'.format(
                    user_id=user_id,
                    additional_message=additional_message,
                    )
                obj.add_historylog(message, who=['zk', ])
            else:
                raise Exception('Invalid internal state')

            # unread
            obj.unread_zk = True

            self.log_time('transaction before send_notifications')
            self._transaction()

            # if self.is_reader:
            # nastaveni notifikaci viz notify()
            self.ticket = obj
            self.send_notifications('reader_creates_mvst')

            self.add_message(u'Objednávka byla vytvořena.  O důležitých změnách budete informován(a) pomocí e-mailu na vaši notifikační adresu, pokud máte zapnuté notifikace.')
            self.redirect(self.ticket_url)

        return obj
