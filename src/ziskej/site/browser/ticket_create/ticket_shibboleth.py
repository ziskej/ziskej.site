# -*- coding: utf-8 -*-
"""Ticket Create Shibboleth"""

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from plone import api

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.auth.auth import (
    auth_wrapper,
    get_user_obj_by_user_id,
    )
from ziskej.site.browser.ticket_create.ticket_create_base import (
    TicketView,
    logger,
    logger_dmd,
    DEBUG,
    DEBUG_CPK_POST_VALUE,
    DEBUG_MISSING_DOC,
    )


class ShibbolethView(TicketView):
    """URI: /shibLogin
    """

    template = ViewPageTemplateFile('ticket_shibboleth.pt')

    def call(self):
        if DEBUG:
            print '-- ShibbolethView.call'
        if DEBUG_MISSING_DOC:
            logger_dmd.info('-- ShibbolethView.call')
            self.debug_session()

        super(ShibbolethView, self).call()

        url = '{portal_url}/ticket_create_summary?postauth=1'.format(portal_url=self.portal_url)
        self.next_url = url

        if api.user.is_anonymous():

            # Helper auth_wrapper do nothing for already authenticated otherwise
            # log in if Shibboleth authentication is present.  Without
            # Shibboleth Zope auth is used and needs url for redirect after log
            # in.
            user = auth_wrapper(self.context, self.request, url=url)

        else:
            member = api.user.get_current()
            user_id = str(member)
            user = get_user_obj_by_user_id(user_id)

        if user is None:
            return

        # FIXME enable redirect to self.next_url
