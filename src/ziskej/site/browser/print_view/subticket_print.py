# -*- coding: utf-8 -*-
"""BrowserView Print: Subticket.

Tisková podoba požadavku.
"""

import logging

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.browser.print_view.print_base import PrintBaseView


logger = logging.getLogger("ziskej")


class SubticketPrintView(PrintBaseView):

    template = ViewPageTemplateFile('subticket_print.pt')

    def call(self):
        super(SubticketPrintView, self).call()

        self.subticket = self.context
        self.ticket = self.subticket.ticket
        self.messages = self.subticket.get_messages()
        self.set_up_terms()
        self.doc_sent = self.subticket.doc_sent
    
    def format_terms(self,value, date):
        html_terms =u'''
            <p> <strong>{value}</strong> 
            <span >{date} </span>  
            </p>
        '''.format(value=value,date=date)
        return html_terms
         
    def set_up_terms(self):
        terms = u''
        if self.subticket.accepted_date:
            value = u"Datum přijetí:"
            date = self.date_format(self.subticket.accepted_date,)
            terms += self.format_terms(value,date)

        if self.subticket.back_date:
            value = u"Požadováno zpět:"
            date = self.date_format(self.subticket.back_date, )
            terms += self.format_terms(value,date)

        if self.subticket.sent_date:
            value = u"Datum odeslání:"
            date = self.date_format(self.subticket.sent_date, )
            terms += self.format_terms(value,date)

        if self.subticket.closed_date:
            value = u"Datum odeslání:"
            date = self.date_format(self.subticket.closed_date, )
            terms += self.format_terms(value,date)
        
        self.html_terms = terms

