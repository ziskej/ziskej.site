# -*- coding: utf-8 -*-
"""BrowserView Print: Ticket.

Tisková podoba objednávky.
"""

import logging

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from ziskej.site.browser.print_view.print_base import PrintBaseView


logger = logging.getLogger("ziskej")


class TicketPrintView(PrintBaseView):

    template = ViewPageTemplateFile('ticket_print.pt')

    def call(self):
        super(TicketPrintView, self).call()

        # Nastavit ticket, subticket, inspirace viz
        # setup_ticket_subticket_subtickets

        self.ticket = self.context
        self.subticket = None
        if self.ticket.subticket_id is not None:
            self.subticket = getattr(self.ticket, self.ticket.subticket_id, None)
        self.set_up_terms()
        #print "self.subticket:", self.subticket

    def format_terms(self,value, date):
        html_terms =u'''
            <p> <strong>{value}</strong> 
            <span >{date} </span>  
            </p>
        '''.format(value=value,date=date)
        return html_terms

    def set_up_terms(self):
        terms = u''
        value = u"Objednávka přijata:"
        date = self.date_format(self.ticket.approved_date,)
        terms += self.format_terms(value,date)

        if self.ticket.reader_from_date:
            value = u"Připraveno pro čtenáře od:"
            date = self.date_format(self.ticket.reader_from_date,)
            terms += self.format_terms(value,date)

        if self.ticket.reader_date:
            value = u"Požádovano čtenářem:"
            date = self.date_format(self.ticket.reader_date, )
            terms += self.format_terms(value,date)

        if self.ticket.reader_to_date:
            value = u"Čtenář má vrátit do:"
            date = self.date_format(self.ticket.reader_to_date, )
            terms += self.format_terms(value,date)

        if self.subticket:
            #validace do pt
            self.is_subticket = True
            if self.subticket.back_date:
                value = u"Požadováno vrátit do DK:"
                date = self.date_format(self.subticket.back_date, )
                terms += self.format_terms(value,date)
        #validace do pt
        else:
            self.is_subticket = False

        self.html_terms = terms

    def set_up_state(self):
        self.state = dict(
            queued = u'Nový',
            conditionally_accepted = u'Přijato s podmínkou',
            accepted = u'Ve zpracování',
            sent = u'Vyřízeno - odesláno',
            sent_back = u'Vyřízeno - Odesláno zpět knihovně',
            closed = u'Uzavřeno',
            cancelled = u'Stornována',
            refused = u'Odmítnuto',
    )
