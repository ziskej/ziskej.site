# -*- coding: utf-8 -*-
"""BrowserView Print: Ticket / Subticket.

Tisková podoba objednávky / požadavku.
"""

from ziskej.site.browser import ZiskejBrowserView


class PrintBaseView(ZiskejBrowserView):
    pass
