# -*- coding: utf-8 -*-
"""Report views"""

from __future__ import print_function

import logging

from plone import api
from Products.Five import BrowserView


logger = logging.getLogger("ziskej")


class FileTooBigView(BrowserView):
    """FIXME
    """

    def __call__(self):
        portal = api.portal.get()
        ptool = portal.plone_utils
        portal_url = portal.absolute_url()
        url = self.request.get('REDIRECT_URL', None)
        if not url:
            url = self.request.get('HTTP_REFERER', None)
        if url and not url.startswith(portal_url):
            logger.info('file-too-big invalid url {}'.format(repr(url)))
            url = None
        if url:
            url = url[len(portal_url):]
            if url.startswith('/'):
                url = url[1:]
            logger.info('file-too-big part url {}'.format(repr(url)))
            try:
                obj = portal.unrestrictedTraverse(url)
            except Exception, e:
                logger.exception(e)
                obj = None
            if obj and obj.portal_type != 'SubticketMVS':
                logger.info(
                    'file-too-big invalid portal_type {} for {}'.format(
                        obj.portal_type, repr(url)))
                obj = None
        if url and obj:
            url = obj.absolute_url()
        else:
            url = portal_url
        logger.info('file-too-big url {}'.format(repr(url)))
        ptool.addPortalMessage(u'Překročen limit maximální velikosti souboru.', 'error')
        self.request.response.redirect(url)
        return 'redirecting'
