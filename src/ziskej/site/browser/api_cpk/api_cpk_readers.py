# -*- coding: utf-8 -*-
"""BrowserView: Získej API pro CPK, část Získej app, čtenáři.
"""

import logging

from ziskej.site.auth.auth_ziskejapi import auth_ziskejapi
from ziskej.site.utils import delete_objects
from ziskej.site.validators import validate_email, Invalid

from api_cpk_base import (
    ApiCpkView,
    ApiException404,
    ApiException422,
    ApiException500,
    )


logger = logging.getLogger('ziskej.api')

DEBUG = False
DEBUG_REMOVE_READER_BEFORE_CREATION = True  # just for sigla REMOVE_READER


class View_eppn_get(ApiCpkView):
    """readers/(eppn) GET

    Vrací profil čtenář a volitelně i status.

    Status čtenáře přihlášeného v CPK – počet nepřečtených zpráv, počet všech
    zpráv, počet neuzavřených požadavků, počet uzavřených požadavků.

    Pokud není dán některý ze souhlasů, vrací se jen reader_id a stav souhlasů.

    HTTP 200

    reader_id .. id čtenáře v Získej
    is_active .. true pokud může používat Získej
    can_create .. true pokud může používat Získej i vytvářet nové objednávky
    first_name .. jméno
    last_name .. příjmení
    email .. e-mail pro notifikace
    notification_enabled .. true pokud jsou notifikace zapnuty
    sigla .. SIGLA mateřské knihovny, upper case
    reader_library_id .. id čtenáře v knihovně z CPK ze Shibbolethu
    is_gdpr_reg .. true, pokud je dán souhlas s registrací
    is_gdpr_data .. true, pokud je dán souhlas s uchováváním dat

    (jen expand=status)
    count_tickets_open .. počet neuzavřených požadavků
    count_tickets .. počet požadavků
    count_messages_unread .. počet nepřečtených zpráv
    count_messages .. počet zpráv
    """

    def call(self):

        # málokdy má smysl volat get_reader s check_active=False, protože
        # potřebujeme inactive_message z reader.check_active a ne
        # ApiException422('Inactive reader')
        self.reader = self.get_reader(check_active=False)
        reader = self.reader

        # check_active s full False provede všechna ověření kromě testu na
        # aktivní knihovnu
        inactive_message = self.reader.check_active(full=False)
        if inactive_message:
            data = dict(
                reader_id = reader.getId(),
                is_active = False,
                can_create = False,
                inactive_message = inactive_message,
                is_gdpr_reg = reader.gdpr_registration,
                is_gdpr_data = reader.gdpr_data,
                )
            return 200, data

        # nyní je zaručeno i s self.get_reader(check_active=False), že je
        # reader.is_active
        self.initial_reader_setup()

        expand = self.text_from_request('expand', lov=[None, 'status', ])

        # Základní info o objektu - restrikce pro Získej API
        # přičemž reader.is_active je True, tedy can_create tedy záleží čistě
        # jen na tom, zda je mateřská knihovna aktivní
        data = dict(
            reader_id = reader.getId(),
            is_active = True,
            can_create = self.reader.can_create(),
            first_name = reader.first_name,
            last_name = reader.last_name,
            email = reader.email_user,
            notification_enabled = reader.is_notification_enabled,
            sigla = reader.library.upper() if reader.library else u'',
            reader_library_id = reader.reader_library_id,
            is_gdpr_reg = reader.gdpr_registration,
            is_gdpr_data = reader.gdpr_data,
            )

        # expand status: přidej počty objednávek, otevřených objedávek,
        # zpráv a nepřečtených zpráv
        if expand == 'status':
            counters = self.reader_status(reader)
            if DEBUG:
                print counters
            for k in ('count_tickets', 'count_tickets_open', 'count_messages', 'count_messages_unread', ):
                data[k] = counters.get(k, None)

        return 200, data


class View_eppn_put(ApiCpkView):
    """readers/(eppn) PUT
    """

    def call(self):

        # nastavení

        # mapování reader atributů na jméno v api
        MAP_PARAMS = dict(
            eppn = 'eppn',
            email_user = 'email',
            is_notification_enabled = 'notification_enabled',
            library = 'sigla',
            reader_library_id = 'reader_library_id',
            gdpr_registration = 'is_gdpr_reg',
            gdpr_data = 'is_gdpr_data',
            )
        # restrikce a zároveň povinné, není-li označeno jako volitelné
        param_keys = ('eppn', 'first_name', 'last_name', 'email_user',
            'is_notification_enabled', 'library', 'reader_library_id',
            'gdpr_registration', 'gdpr_data', )
        # volitelné
        param_keys_optional = ('email_user', 'reader_library_id', )
        # boolean
        param_keys_bool = ('is_notification_enabled', 'gdpr_registration',
            'gdpr_data', )
        # email
        param_keys_email = ('email_user', )

        # načtení parametrů a validace

        obj_dict = dict()
        for k in param_keys:
            value = self.text_from_request(MAP_PARAMS.get(k, k))

            # validace po položkách

            # mandatory
            if not value and k not in param_keys_optional:
                if DEBUG:
                    print self.request.form
                raise ApiException422('Missing parameter {}'.format(MAP_PARAMS.get(k, k)))

            # transformace:
            # boolean
            if k in param_keys_bool:
                value = bool(value.lower() == 'true')
            # sigla
            elif k == 'library':
                value = value.lower()

            # validace e-mailů, prázdný string je validní pro validate_email
            if k in param_keys_email:
                try:
                    validate_email(value)
                except Invalid:
                    raise ApiException422('Invalid value of parameter {}'.format(MAP_PARAMS.get(k, k)))

            obj_dict[k] = value

        # validace celkově
        if obj_dict['is_notification_enabled']:
            if not obj_dict['email_user']:
                raise ApiException422('Missing parameter email')

        if DEBUG:
            print "obj_dict:", obj_dict

        reader = self.get_reader(obj_dict['eppn'], none404=False, check_active=False)
        existing = bool(reader)

        # validace

        # existujícímu čtenáři není možné změnit knihovnu
        if existing and obj_dict['library'] != reader.library:
            raise ApiException422('Cannot change library sigla')

        # vytvářený čtenář musí mít knihovnu, která je aktivní
        if not existing and obj_dict['library'] not in \
                self.get_libraries(lower_case=True):
            raise ApiException422('Library is not active')

        # create / update
        if not existing:
            reader = auth_ziskejapi(self.context, self.request, obj_dict)
        else:
            reader.update_attrs_if_changed(**obj_dict)

        # Základní info o objektu - restrikce pro Získej API
        inactive_message = reader.check_active()
        data = dict(
            reader_id = reader.getId(),
            is_active = not inactive_message,
            first_name = reader.first_name,
            last_name = reader.last_name,
            email = reader.email_user,
            notification_enabled = reader.is_notification_enabled,
            sigla = reader.library.upper() if reader.library else u'',
            reader_library_id = reader.reader_library_id,
            is_gdpr_reg = reader.gdpr_registration,
            is_gdpr_data = reader.gdpr_data,
            )
        if inactive_message:
            data['inactive_message'] = inactive_message

        # notifikace neposílat

        if existing:
            http_code = 200
        else:
            http_code = 201
        return http_code, data


class View_eppn_delete(ApiCpkView):

    def call(self):

        # není povoleno na produkci
        if self.is_instance_prod:
            raise ApiException422(u'Not allowed in production instance')

        # málokdy má smysl volat get_reader s check_active=False, protože
        # potřebujeme inactive_message z reader.check_active a ne
        # ApiException422('Inactive reader')
        self.reader = self.get_reader(check_active=False)

        # umožnit smazat jen čtenáře vytvořehého přes Získej API pro CPK
        if self.reader.usertype != 'ziskejapi':
            raise ApiException404(u'User not found')

        # smazat všechny tickety pro tohoto uživatele
        query = dict(
            portal_type='TicketMVS',
            reader=self.reader.getId(),
            sort_on='hid',
            sort_order='reverse',
            )
        brains = self.catalog_search(query)
        ticket_ids = []
        for brain in brains:
            ticket_ids.append(brain.getId)
        delete_objects(self.portal, self.request, self.portal.tickets, ticket_ids)

        # smazat čtenáře
        delete_objects(self.portal, self.request, self.reader.__parent__, [self.reader.getId(), ])

        return 200, dict(message=u'Reader and all his tickets have been removed')
