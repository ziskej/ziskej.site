# -*- coding: utf-8 -*-
"""BrowserView: Získej API pro CPK, část Získej app, společný základ a utility.
"""

from __future__ import print_function

from datetime import datetime
import json
import jwt
import logging
import random

from DateTime import DateTime
from plone import api
#from plone.protect.interfaces import IDisableCSRFProtection
#from zope.interface import alsoProvides
from Products.Five import BrowserView

from ziskej.site import (
    BUILDOUT_TYPE,
    USE_PLATEBATOR,
    PLATEBATOR_BASE_URL,
    SIMULATE_PLATEBATOR_PAID,
    ZISKEJ_ASYNC_MODE,
    ZISKEJ_API_ID,
    SHARED_SECRET_ZISKEJ_APP,
    )
from ziskej.site.auth.auth_base import get_reader_by_eppn
from ziskej.site.library_helpers import lib_helper
from ziskej.site.notifications import (
    ZiskejNotifications,
    ErrorNotifications,
    )
from ziskej.site.utils import (
    get_instance_type_label,
    get_time0,
    get_time_ms,
    safe_unicode,
    sanitize_input_text,
    pprint,
    )
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger('ziskej.apicpk')
logger_time = logging.getLogger("profiling")

PROFILING_ON = True

SERVICE_LOV = ['mvszk', 'mvsdk', 'mvs',
               'eddzk', 'edddk', 'edd',
               'anyzk', 'anydk', 'any', ]
SERVICE_LOV_DEFAULT = u'mvszk'

API_EXCEPTION_MESSAGES = {
    'Library does not provide MVS': u'Objednávku není možné vytvořit, protože vaše knihovna neposkytuje MVS.',
    'Missing doc_id.': u'Tento dokument není možné přes službu ZÍSKEJ objednat.',
    'Invalid request: cannot create ticket for doc_id Souborny katalog without any alternative.': u'Tento dokument není možné přes službu ZÍSKEJ objednat.',
    'Invalid request: cannot create ticket for doc_id and doc_alt_ids due to no corrensponding library is providing MVS.': u'Objednávku není možné vytvořit, protože zadaný dokument žádná knihovna neposkytuje.',
    'Ticket cannot be cancelled.': u'Objednávka už nemůže být stornována.',
    # 'Library does not provide MVS due to vacation': u'Objednávku není možné vytvořit, protože vaše knihovna nemá v nejbližších dnech provozní dobu nebo má dovolenou nebo dovolená brzy začne a proto nyní neposkytuje MVS.',
    'Library does not provide MVS due to vacation': u'Není k dispozici žádná knihovna, která Vám může požadovaný titul v dané lhůtě dodat. Zkuste to prosím později.',
    }

DEBUG = False
DEBUG_DUMMY_EPPN = False
DEBUG_SKIP_JWT = False


class ApiException(Exception):
    pass


class ApiException403(ApiException):
    pass


class ApiException404(ApiException):
    pass


class ApiException422(ApiException):
    pass


class ApiException500(ApiException):
    pass


class ApiCpkView(BrowserView):
    """Handle common tasks for API such as parset JWT, catch exceptions, jsonify
    result and set headers for API using json.
    """

    def __call__(self):
        """Handle BrowserView for each API call."""
        self.time0 = get_time0()

        self.portal = api.portal.get()
        self.catalog = api.portal.get_tool(name='portal_catalog')
        self.catalog_search = self.catalog.unrestrictedSearchResults

        # typ instance
        self.instance_type, self.instance_type_label, instance_title_postfix = get_instance_type_label()
        self.is_instance_prod = bool(self.instance_type == 'prod')
        self.is_instance_local = bool(self.instance_type == 'development')
        self.is_instance_testing = bool(self.instance_type in ('development', 'dev', 'test'))

        self.testing_now_dt = ziskej_parameter(
            'testing_now_str', portal=self.portal)

        # lib_helper - inicializováno při startu bez portal, pokud ještě nikdy
        # nedostal portal, inicializuje se pomocí bind_portal, jinak levný call
        self.lib_helper = lib_helper
        self.lib_helper.bind_portal(self.portal)

        self.reader = None
        self.user_id = None
        self.library = None
        self.ticket = None

        print(" ")
        print(self.request.URL)
        print(self.request.form)
        print(" ")

        try:
            self.parse_jwt()
            http_code, result_data = self.call()
            result_dict = dict(
                status = u'OK',
                http_code = http_code,
                result = result_data,
                )
            result_json = json.dumps(result_dict, ensure_ascii=False)

        except ApiException403, e:
            error_dict = dict(
                status = u'Error',
                http_code = 403,
                error_message = API_EXCEPTION_MESSAGES.get(str(e), unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        except ApiException404, e:
            error_dict = dict(
                status = u'Error',
                http_code = 404,
                error_message = API_EXCEPTION_MESSAGES.get(str(e), unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        except ApiException422, e:
            error_dict = dict(
                status = u'Error',
                http_code = 422,
                error_message = API_EXCEPTION_MESSAGES.get(str(e), unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        except ApiException, e:
            logger.warning(e)
            error_dict = dict(
                status = u'Error',
                http_code = 500,
                error_message = API_EXCEPTION_MESSAGES.get(str(e), unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        except Exception, e:
            logger.exception(e)
            error_dict = dict(
                status = u'Error',
                http_code = 500,
                error_message = u'Unexpected exception: {}'.format(unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        # no diazo
        self.request.response.setHeader('X-Theme-Disabled', '1')

        # json content type
        self.request.response.setHeader('Content-Type', 'application/json')

        # integration
        self.request.response.setHeader('Access-Control-Allow-Origin', '*')

        # no cache
        self.request.response.setHeader('Cache-Control', 'no-store,no-cache,must-revalidate,post-check=0,pre-check=0')
        self.request.response.setHeader('Pragma', 'no-cache')
        now = DateTime().ISO()
        self.request.response.setHeader('Expires', '-1')
        self.request.response.setHeader('Last-modified', now)

        self.log_time('end of request (cpkapi)')

        return result_json

    def log_time(self, txt, time0=None):
        if not PROFILING_ON:
            return
        if time0 is None:
            time0 = self.time0
        msg = u'{ts} {txt}'.format(ts=get_time_ms(time0), txt=txt)
        logger_time.info(msg)

    def parse_jwt(self):
        """Do the basic JWT check and parse it.  Each class could and should
        check JWT content with provided parameters.
        """

        if DEBUG_SKIP_JWT:
            return None
        USE_BEARER = False
        if USE_BEARER:
            header_authorization = self.request.get_header('Authorization', None)
            if not header_authorization:
                raise ApiException403('Missing authorization')
            if not header_authorization.startswith('bearer '):
                raise ApiException403('Invalid authorization format')
            token = header_authorization[7:]
        else:
            token = self.request.form.get('token', None)
            if not token:
                raise ApiException403('Missing authorization')
        try:
            token_dict = jwt.decode(token, SHARED_SECRET_ZISKEJ_APP, algorithms=['HS256'])
        except jwt.ExpiredSignatureError, e:
            logger.warning('ExpiredSignatureError: {msg}'.format(msg=str(e)))
            raise ApiException403('Expired authorization token')
        except jwt.exceptions.InvalidTokenError, e:
            logger.error('InvalidTokenError: {msg}'.format(msg=str(e)))
            raise ApiException403('Invalid authorization token')
        if not token_dict.get('iat', ''):
            raise ApiException403('Missing iat in authorization token')
        if not token_dict.get('exp', ''):
            raise ApiException403('Missing exp in authorization token')
        if token_dict.get('iss', '') != ZISKEJ_API_ID:
            raise ApiException403('Invalid iss')
        if token_dict.get('app', '') != ZISKEJ_API_ID:
            raise ApiException403('Invalid app')
        return token_dict

    def call(self):
        """Each BrowserView class must define this method."""
        raise ApiException500('Not implemented')

    def get_DateTime_from_date_str(self, value_str):
        dt = None
        try:
            #value = datetime.strptime(value_str + ' 12:00', '%d.%m.%Y %H:%M')
            value = datetime.strptime(value_str + ' 12:00', '%Y-%m-%d %H:%M')
            value = value.strftime('%Y-%m-%d')
            dt = DateTime(value)
        except:
            logger.info('Ziskej API: Ignored invalid DateTime for value: {value_str}'.format(value_str=repr(value_str)))
            raise
        return dt

    def dt_as(self, dt, result_type='string'):
        if not dt:
            return None
        if result_type in ('string', 'datepicker', ):
            result = dt.strftime('%-d.%-m.%Y')
        elif result_type == 'iso':
            result = dt.strftime('%Y-%m-%d')
        elif result_type == 'isodt':
            result = dt.strftime('%Y-%m-%d %-H:%M:%S')
        elif result_type == 'DateTime':
            result = dt
        elif result_type == 'datetime':
            result = dt.asdatetime()
        elif result_type == 'datepicker':
            result = dt.asdatetime()
        return result

    def date_from_request(self, name, result_type='string', is_exception=True):
        value_str = self.request.get(name, None)
        if not value_str:
            return None
        if not isinstance(value_str, basestring):
            if isinstance(value_str, list):
                raise ApiException422('Expecting string but get list for {name}'.format(name=name))
            else:
                raise ApiException422('Expecting string but get not string for {name}'.format(name=name))
        value_str = sanitize_input_text(value_str)
        value_str = value_str.replace(' ', '')
        if not value_str:
            return None
        if DEBUG:
            print(" ")
            print(value_str)
            print(" ")
        try:
            dt = self.get_DateTime_from_date_str(value_str)
        except:
            if is_exception:
                raise ApiException422('Invalid {}? is not valid date format'.format(name))
            return None
        return self.dt_as(dt, result_type=result_type)

    def int_from_request(self, name, min=0, max=None, is_exception=True, not_empty=False):
        value = self.request.get(name, None)
        if not_empty and not value:
            raise ApiException422("Missing {}".format(name))
        if value is None:
            return None
        value = value.strip()
        try:
            value = int(value)
        except ValueError:
            logger.warning(name + ' is not int: ' + str(value))
            if is_exception:
                raise ApiException422("Invalid {}: is not int".format(name))
            value = None
        if min is not None and value < min:
            if is_exception:
                raise ApiException422("Invalid {}: cannot be less than {}".format(name, min))
            value = None
        if max is not None and value > max:
            if is_exception:
                raise ApiException422("Invalid {}: cannot be greater than {}".format(name, max))
            value = None
        return value

    def text_from_request(self, name, lov=None, is_exception=True, not_empty=False):
        value = self.request.get(name, None)
        if not_empty and not value:
            raise ApiException422("Missing {}".format(name))
        if value is None:
            return None
        if not isinstance(value, basestring):
            if isinstance(value, list):
                raise ApiException422('Expecting string but get list for {name}'.format(name=name))
            else:
                raise ApiException422('Expecting string but get not string for {name}'.format(name=name))
        #value = safe_utf8(value)
        value = safe_unicode(value)
        value = sanitize_input_text(value)  # cgi.escape
        if lov is not None and value not in lov:
            logger.info('ignored unexpected value for lov based input {name}'.format(name=name))
            if is_exception:
                raise ApiException422("Invalid {}: unexpected value".format(name))
            value = None
        return value

    def list_of_texts_from_request(self, name, lov=None, is_exception=True):
        values = self.request.get(name, None)
        if values is None:
            return None
        if isinstance(values, list):
            pass
        elif isinstance(values, basestring):
            # je chybou tady vrátit chybu, protože pro jednoprvkový seznam je
            # values string a převod na jednoprvkový seznam je snadný
            values = [values]
            #raise ApiException422('Expecting list of strings but get string for {name}'.format(name=name))
        else:
            raise ApiException422('Expecting list of strings for {name}'.format(name=name))

        data = []
        for value in values:
            #value = safe_utf8(value)
            value = safe_unicode(value)
            value = sanitize_input_text(value)  # cgi.escape
            if lov is not None and value not in lov:
                if is_exception:
                    raise ApiException422('Invalid {}: unexpected value'.format(name))
                logger.info('ignored unexpected value for lov based input {name}'.format(name=name))
                continue
            if not value:
                continue
            if value in data:
                continue
            data.append(value)

        return data

    def get_reader(self, eppn=None, none404=True, check_active=True,
                   full=False):
        """Ověří a vrátí čtenáře na základě eppn.  Většinou ověřuje, zda je
        aktivní (vč. zda má GDPR souhlasy), parametr ,check_active při vytváření
        objednávky i zda je aktivní plně, tzn. i jeho mateřská knihovna je
        právě teď aktivní, parametr full.
        """

        if DEBUG_DUMMY_EPPN:
            self.eppn = u'ntk_ctenar'
            return self.portal.readers.ntk_ctenar
        if not eppn:
            eppn = self.text_from_request('eppn')
        if not eppn:
            raise ApiException422(u'Missing parameter eppn')
        self.eppn = u'{}'.format(eppn)

        reader = None

        if eppn == 'ntk_ctenar':
            reader = self.portal.readers.ntk_ctenar

        ENABLE_TEST_USERS = False
        if ENABLE_TEST_USERS:
            TEST_USERS = {
                '1185@mzk.cz': '',
                '1184@mzk.cz': '',
                'rasputin@techlib.cz': '',
                'dostojevskij@techlib.cz': '',
                }
            if eppn in TEST_USERS:
                user_id = TEST_USERS[eppn]
                reader = getattr(self.portal.readers, user_id, None)

        if not reader:
            reader = get_reader_by_eppn(eppn)

        if not none404 and not reader:
            return None
        if not reader:
            raise ApiException404('Reader not found')
        if full and not reader.is_active_full:
            raise ApiException422('Inactive reader')
        elif check_active and not reader.is_active:
            raise ApiException422('Inactive reader')
        return reader

    def initial_reader_setup(self, full=False):
        """Očekává, že je nastaven self.reader a že je aktivní"""

        try:
            if not self.reader:
                raise ApiException500('Expecting reader object')
        except:
            raise ApiException500('Expecting reader object')

        self.user_id = self.reader.getId()

        self.library_zk_sigla = self.reader.library
        if not self.library_zk_sigla:
            # Nemělo by nastat, protože pak není aktivní
            raise ApiException500('Reader has no library')
        self.library_zk_sigla = self.library_zk_sigla.lower()
        self.library = getattr(self.portal.libraries, self.library_zk_sigla, None)
        if not self.library:
            # Nemělo by nastat, protože pak není aktivní
            raise ApiException500('Reader has invalid library')
        if full and not self.library.is_active:
            # Nemělo by nastat, protože pak není aktivní
            raise ApiException500('Reader has no library active in Ziskej')

    def get_ticket(self, ticket_id, reader):
        """Get ticket by ticket_id and check ticket reader."""
        if not ticket_id:
            raise ApiException422('Missing ticket_id')
        ticket = getattr(self.portal.tickets, ticket_id, None)
        if not ticket:
            raise ApiException404('Ticket not found')
        if not (ticket.reader and ticket.reader == reader.username):
            raise ApiException422('Invalid reader for ticket')
        return ticket

    def get_libraries(self, lower_case=False, service=None,
                      include_deactivated=False):
        """Vrací seznam sigel aktivních knihoven.  Aktivní knihovnou je v tomto
        smyslu jakákoli aktivní knihovna, která poskytuje službu service.

        V případě include_deactivated True vrátí všechny knihovny, které mají
        v rámci specifikované služby vytvořenou objednávku pomocí Získej API pro
        CPK pro čtenáře.

        lower_case False (default) .. Vrátit seznam sigel ve tvaru ABA013,
                                      BOA001, v rámci volání API
                                      api_cpk_libraries_get.

        lower_case True .. Vrátit seznam sigel ve tvaru aba013, boa001, v rámci
                           interního volání, používá se pro určení, zda má
                           čtenář aktivní knihovnu.
        """

        DEBUG_LOCAL = False

        if service not in SERVICE_LOV:
            service = SERVICE_LOV_DEFAULT  # mvszk
        if DEBUG_LOCAL:
            print('service', service)

        if include_deactivated:
            # cache je potřeba, použít, je-li k dispozici
            if self.lib_helper.is_cpkapi_including_deactivated_cache(service):
                return self.lib_helper.get_cpkapi_including_deactivated_cache(
                    service)

        query = dict(
            portal_type = 'Library',
            sort_on = 'sigla',
            )
        if service in ('mvszk', 'eddzk', 'anyzk', ):
            query['Was_any_zk'] = True  # FIXME-EDD deployment vyžaduje reindex
        if not include_deactivated:
            query['review_state'] = 'published'
        brains = self.catalog_search(query)
        if DEBUG_LOCAL:
            print(len(brains), 'for', repr(query))
        items = []
        for brain in brains:
            obj = brain._unrestrictedGetObject()
            if not brain.sigla:
                if DEBUG_LOCAL:
                    print('sigla ??? SKIPPED')
                continue
            if DEBUG_LOCAL:
                print('sigla', brain.sigla)

            is_active = False
            if not include_deactivated:

                # Knihovna je aktivní a poskytuje MVS jako ŽK
                if not is_active and \
                        service in ('mvszk', 'mvs', 'anyzk', 'any', ):
                    # Pokud má knihovna dnes i zítra dovolenou nebo je dnes méně
                    # než 1 h do konce provozní doby a zítra začíná dovolená,
                    # tak deaktivovat pro účely seznamu aktivních knihoven,
                    # který CPK používá pro určení, zda lze vytvořit novou
                    # objednávku.

                    # NTK-254 Do seznamu knihoven zahrnout nezávisle na
                    # dovolené.
                    # if not obj.Is_mvs_zk(
                    #     ignore_vacation=False, now_dt=self.testing_now_dt):
                    #     continue
                    if obj.Is_mvs_zk(
                        ignore_vacation=True, now_dt=self.testing_now_dt):
                        is_active = True
                        if DEBUG_LOCAL:
                            print('  because Is_mvs_zk')

                # Knihovna je aktivní a poskytuje MVS jako DK
                if not is_active and \
                        service in ('mvsdk', 'mvs', 'anydk', 'any', ):
                    if obj.Is_mvs_dk():
                        is_active = True
                        if DEBUG_LOCAL:
                            print('  because Is_mvs_dk')

                # Knihovna je aktivní a poskytuje EDD jako ŽK
                if not is_active and \
                        service in ('eddzk', 'edd', 'anyzk', 'any', ):
                    if obj.Is_edd_zk():
                        is_active = True
                        if DEBUG_LOCAL:
                            print('  because Is_edd_zk')

                # Knihovna je aktivní a poskytuje EDD jako DK
                if not is_active and \
                        service in ('edddk', 'edd', 'anydk', 'any', ):
                    if obj.Is_edd_dk():
                        is_active = True
                        if DEBUG_LOCAL:
                            print('  because Is_edd_dk')

            else:

                # Knihovna je aktivní a poskytuje MVS jako ŽK
                if not is_active and \
                        service in ('mvszk', 'mvs', 'anyzk', 'any', ):
                    if obj.Was_mvs_zk():
                        is_active = True
                        if DEBUG_LOCAL:
                            print('  because Was_mvs_zk')

                # Knihovna je aktivní a poskytuje MVS jako DK
                if not is_active and \
                        service in ('mvsdk', 'mvs', 'anydk', 'any', ):
                    if obj.Was_mvs_dk():
                        is_active = True
                        if DEBUG_LOCAL:
                            print('  because Was_mvs_dk')

                # Knihovna je aktivní a poskytuje EDD jako ŽK
                if not is_active and \
                        service in ('eddzk', 'edd', 'anyzk', 'any', ):
                    if obj.Was_edd_zk():
                        is_active = True
                        if DEBUG_LOCAL:
                            print('  because Was_edd_zk')

                # Knihovna je aktivní a poskytuje EDD jako DK
                if not is_active and \
                        service in ('edddk', 'edd', 'anydk', 'any', ):
                    if obj.Was_edd_dk():
                        is_active = True
                        if DEBUG_LOCAL:
                            print('  because Was_edd_dk')

            if not is_active:
                if DEBUG_LOCAL:
                    print('    skipped')
                continue

            if not lower_case:
                items.append(brain.sigla.upper())
            else:
                items.append(brain.sigla.lower())

        if include_deactivated:
            # aktualziovat cache
            self.lib_helper.set_cpkapi_including_deactivated_cache(
                service, items)

        return items

    def reader_status(self, reader):
        data = dict(
            count_tickets=0,
            count_tickets_open=0,
            count_messages=0,
            count_messages_unread=0,
            )

        query = dict(
            portal_type='TicketMVS',
            reader=reader.getId(),
            sort_on='hid',
            sort_order='reverse',
            )
        brains = self.catalog_search(query)

        for brain in brains:
            ticket = brain.getObject()

            data['count_tickets'] += 1

            if ticket.is_open_for_reader:
                data['count_tickets_open'] += 1

            messages_counts = ticket.get_messages_counts()
            data['count_messages'] += messages_counts['count_messages']
            data['count_messages_unread'] += messages_counts['count_messages_unread']

        return data

    def date_format(self, date):
        """Firmat date for json response.  Nothing to do now but this is the
        place where it could happen if needed in future.
        """

        return date

    def fix_reader_library(self, reader):
        """Try to fix broken data format when reader.library exists and is not
        string.
        """

        print(reader.library)
        try:
            sigla = reader.library.sigla.lower()
            reader.library = sigla
            reader.reindexObject()
        except Exception as e:
            logger.exception('Try to fix broken data format when reader.library exists and is not string for reader {} failed: {}.'.format(reader.getId(), str(e)))
            return False
        logger.warning('Try to fix broken data format when reader.library exists and is not string for reader {} - success.'.format(reader.getId()))
        return True

    def add_historylog(self, message, who=None):
        """Zapíše do audit logu ticketu message zobrazitelnou pro operátora a
        pro who (list).
        """

        if not self.ticket:
            return
        user_id = self.user_id and self.user_id or u'?'
        library = self.library and self.library.sigla or u'?'
        message = u'{} (uživatel {}, knihovna {})'.format(message, user_id, library)
        self.ticket.add_historylog(message, who=who)

    def send_notifications(self, nid, ticket=None, subticket=None):
        """Wrapper pro notifikace, pošle notifikaci s id nid.  Parametry ticket
        a subticket jsou tady záměrně ignorovány a místo nich se použijí objekty
        nastavené z kontextu.
        """

        ziskej_notifications = ZiskejNotifications(ticket=self.ticket,
                                                   subticket=None)
        ziskej_notifications.send_notifications(nid)

    def error_notify(self, error_class_id, e, method_id, detail_error=None):
        try:
            user_id = self.user_id
        except:
            user_id = '?'
        message = '{error_class_id} in {method_id} for user {user_id}: {e}'.format(
            error_class_id = error_class_id,
            e = str(e),
            method_id = method_id,
            user_id = user_id)
        if detail_error:
            message += ' [more info] ' + detail_error
        logger.warning('[error_notify] ' + message)

        error_notifications = ErrorNotifications()
        error_notifications.send_notifications(message)
