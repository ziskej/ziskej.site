# -*- coding: utf-8 -*-

from api_cpk_base import ApiCpkView, SERVICE_LOV

from api_cpk_base import (
    ApiCpkView,
    ApiException404,
    ApiException422,
    ApiException500,)


class View_edd_estimate_get(ApiCpkView):
    """
    Z dokumentace:

    Cena u EDD je složená ze dvou částí.
    Sazebník: Články i výňatky: 5 Kč/str, za objednávku min. 10 Kč a max 75 Kč.
    Pro výňatky je limit 20 stran.

    Samotná knihovna bude chtít za 1 stranu 1 Kč. A pouze tato část jde DK.
    """

    def call(self):

        number_of_pages = self.int_from_request(
            'number_of_pages', min=1, not_empty=True)

        edd_subtype = self.text_from_request(
            'edd_subtype', lov=['article', 'selection', ], not_empty=True)

        min_limit_dilia = 10
        max_limit_dilia = 75
        selection_max_pages = 20
        fee_page_dilia = 5
        fee_page_library = 1

        estimate_fee = dict()
        fee_dilia = fee_page_dilia * number_of_pages

        #Kontrola limitů
        if fee_dilia > max_limit_dilia:
            fee_dilia = max_limit_dilia
        if fee_dilia < min_limit_dilia:
            fee_dilia = min_limit_dilia

        # Celková cena včetě poplatku pro dk
        fee = fee_dilia + (fee_page_library * number_of_pages)
        estimate_fee['fee'] = fee

        if edd_subtype == "selection" and number_of_pages > selection_max_pages:
             estimate_fee['is_valid'] = False
        else:
            estimate_fee['is_valid'] = True

        return 200, estimate_fee
