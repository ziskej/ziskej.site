# -*- coding: utf-8 -*-
"""BrowserView: Získej API pro CPK, část Získej app, objednávky.
"""

from __future__ import print_function

import json
import logging

from DateTime import DateTime

from ziskej.site.library_payment_helpers import (
    platebator_create,
    platebator_check,
    ZiskejAsyncException,
    )
from ziskej.site.ticket_helpers import (
    TICKET_PORTAL_TYPES,
    TICKETMVS_WF_S_OPEN,
    create_ticketmvs,
    wf_action,
    get_cpk_url,
    )
from ziskej.site.ticket_load_balancing import (
    parse_cpk_post,
    CpkApiException,
    LoadBalancingException,
    )

from api_cpk_base import (
    ApiCpkView,
    ApiException404,
    ApiException422,
    ApiException500,
    USE_PLATEBATOR,
    PLATEBATOR_BASE_URL,
    SIMULATE_PLATEBATOR_PAID,
    pprint,
    )


logger = logging.getLogger('ziskej.api')


class ApiCpkTicketsBaseView(ApiCpkView):
    """Provide common utils for BrowserView classes for tickets*."""

    # SUGGESTION je na dvou místech, dát jen na jedno
    def platebator_url(self, platebator_uuid):
        """Vygeneruje URL, kam přesměrovat čtenáře pro zaplacení.  Nejedná se
        o adresu Platebátor API.

        Příklad TEST:

        http://platebator.test.ntkcz.cz/app/0532f320-15bd-11ea-982d-525400fed44e
        """

        if not USE_PLATEBATOR:
            return u''

        return u'{}{}'.format(PLATEBATOR_BASE_URL, platebator_uuid)

    def get_ticket_detail(self, ticket):
        """Build dict with item for one ticket."""

        item = dict()
        item['ticket_id'] = ticket.getId()
        item['ticket_type'] = ticket.ticket_type
        if item['ticket_type'] == "edd":
            item['edd_subtype'] = ticket.edd_subtype
            if ticket.doc_manual:
                item['ticket_doc_data_source'] = "manual"
            else:
                item['ticket_doc_data_source'] = "auto"
        item['hid'] = ticket.hid
        item['sigla'] = ticket.library_zk.upper()

        # pages_from, pages_to, doc_title_in, doc_quote
        # edd článek: doc_number_year, doc_number_pyear, doc_number_pnumber
        if item['ticket_type'] == "edd":
            edd = ticket.Edd_data()
            # print(' ')
            # print('edd for {}:'.format(ticket.getId()))
            # pprint(edd)
            # print(' ')
            item['doc_title'] = ticket.doc_title
            item['doc_author'] = ticket.doc_author
            item['doc_issuer'] = ticket.doc_issuer
            item['doc_note'] = ticket.doc_note
            item['date_requested'] = ticket.date_requested
            item['doc_author'] = ticket.doc_author
            item['doc_title_in'] = edd['doc_title_in']
            item['doc_citation'] = edd['doc_quote']
            item['pages_from'] = edd['pages_from']
            item['pages_to'] = edd['pages_to']
            item['doc_number_year'] = edd['doc_number_year']
            item['doc_number_pyear'] = edd['doc_number_pyear']
            item['doc_number_pnumber'] = edd['doc_number_pnumber']

            if item['edd_subtype'] == "article":
                item['doc_issn'] = ticket.doc_issn

            elif item['edd_subtype'] == "selection":
                item['doc_isbn'] = ticket.doc_isbn
                item['doc_issn'] = ticket.doc_issn
                item['doc_volume'] = edd['doc_volume']

            else:
                logger.warning('Wrong edd_subtype')
                raise ApiException500('Request failed')

            if item['ticket_doc_data_source'] == "manual":
                item['doc_title_in'] = edd['doc_title_in']

            elif item['ticket_doc_data_source'] == "auto":
                item['doc_id'] = ticket.doc_id

            else:
                logger.warning('Wrong ticket_doc_data_source')
                raise ApiException500('Request failed')

        item['doc_id'] = ticket.doc_id
        # item['unit_id'] = None

        status_reader_cpkapi = ticket.status_reader_cpkapi
        item['status_reader'] = status_reader_cpkapi['status_reader']
        item['status_reader_history'] = ticket.status_reader_history_cpkapi(
            status_reader=status_reader_cpkapi['status_reader'])
        item['status_label'] = status_reader_cpkapi['status_label']
        item['is_open'] = status_reader_cpkapi['is_open']

        item['created_datetime'] = ticket.created_iso
        item['updated_datetime'] = ticket.modified_iso

        item['date_created'] = self.date_format(ticket.created_date)
        item['date_requested'] = self.date_format(ticket.reader_date)
        item['date_return'] = self.date_format(ticket.reader_to_date)

        item['payment_id'] = ticket.payment_id
        # URL pro platbu v platebátoru zobrazovat pro nezaplacené objednávky
        item['payment_url'] = None
        if (item['status_reader'] == u'created') and \
                ticket.payment_platebator_uuid:
            item['payment_url'] = self.platebator_url(ticket.payment_platebator_uuid)

        # item['reader_note'] = ticket.reader_note

        messages_counts = ticket.get_messages_counts()
        item['count_messages_unread'] = messages_counts['count_messages_unread']
        item['count_messages'] = messages_counts['count_messages']

        return item



class View_get(ApiCpkTicketsBaseView):
    """Return list of ticket ids for reader by eppn in query string.

    When expand parameter is used expand each ticket according to value in
    expand parameter.
    """

    def call(self):

        self.reader = self.get_reader()
        self.initial_reader_setup()

        expand = self.request.get('expand', None)
        #expand_lov = (None, u'list', u'detail', )  # list NIY
        expand_lov = (None, u'detail', )
        if expand not in expand_lov:
            raise ApiException422('Invalid value for expand parameter.')

        include_closed = bool(self.request.get('include_closed', None))
        ticket_type = self.request.get('ticket_type', None)
        ticket_doc_data_source = self.request.get('ticket_doc_data_source', None)
        edd_subtype = self.request.get('edd_subtype', None)

        query = dict(
            portal_type = TICKET_PORTAL_TYPES,
            reader = self.user_id,
            sort_on = 'hid',
            sort_order = 'reverse',
            )
        if not include_closed:
            query['review_state'] = TICKETMVS_WF_S_OPEN

        if ticket_type == "mvs" or ticket_type == "edd":
            query['ticket_type'] = ticket_type
        elif ticket_type is not None:
            raise ApiException422('Invalid value for ticket_type parameter.')

        if ticket_doc_data_source == "auto" or ticket_doc_data_source == "manual":
            query['ticket_doc_data_source'] = ticket_doc_data_source
        elif ticket_doc_data_source is not None:
            raise ApiException422('Invalid value for ticket_doc_data_source parameter.')

        if edd_subtype == "article" or edd_subtype == "selection":
            query['edd_subtype'] = edd_subtype
        elif edd_subtype is not None:
            raise ApiException422('Invalid value for edd_subtype parameter.')

        brains = self.catalog_search(query)
        items = []
        for brain in brains:
            if expand is None:
                items.append(brain.getId)
            # elif expand == u'list':
            #     unread = brain.unread_r

            #     url = brain.getURL()
            #     items.append(
            #         dict(
            #             hid = brain.hid,
            #             title = brain.Title,
            #             url = url,
            #             review_state = brain.review_state,
            #             review_state_label = ticket_get_review_state_label(brain.review_state),
            #             date_created = self.date_format(brain.CreationDate),
            #             date_modified = self.date_format(brain.ModificationDate),
            #             unread = unread,
            #             doc_title = brain.doc_title,
            #             )
            #         )

            elif expand == u'detail':
                ticket = brain.getObject()
                item = self.get_ticket_detail(ticket)
                items.append(item)

        items_len = len(items)

        data = dict(
            items = items,
            #length = items_len,
            )

        return 200, data


class View_id_get(ApiCpkTicketsBaseView):
    """Return ticket details for reader by eppn in query string.

    Detail MVS požadavku čtenáře

    GET https://ziskej.techlib.cz/api/v1/tickets/(ticket_id)

    eppn .. eppn čtenáře

    HTTP 200

    ticket_id .. id
    ticket_type .. 'mvs'
    hid .. human id (lidsky čitelné id požadavku)
    library .. sigla
    doc_id .. id dokumentu v CPK
    unit_id .. id jednotky v CPK (na základě které byla určena dostupnost)
    is_open .. je to otevřený požadavek?
    status_label .. stav požadavku ke zobrazení
    date_created .. datum vytvoření
    date_requested .. datum požadované čtenářem
    date_return .. datum vrácení
    count_messages_unread .. počet nepřečtených zpráv k tomuto požadavku
    count_messages .. počet zpráv k tomuto požadavku
    """

    def call(self):

        additional_info = u''

        self.reader = self.get_reader()
        self.initial_reader_setup()

        self.ticket = self.get_ticket(self.text_from_request('id'), self.reader)

        # Platebátor
        if USE_PLATEBATOR:
            # force_paid True přepíše návratovou hodnotu paid z Platebátor API
            # na True, vhodné pro testování bez placení kartou
            force_paid = SIMULATE_PLATEBATOR_PAID or False
            try:
                platebator_check(self.ticket, DateTime(), force_paid=force_paid, send_notifications=self.send_notifications)
            except ZiskejAsyncException, e:
                if str(e) == 'Cannot check payment in Platebator because of missing payment_id.':
                    additional_info = u'Missing payment_id'
                    logger.warning('Missing payment_id for ticket {} at {}'.format(
                        self.ticket.hid,
                        self.ticket.getId(),
                        ))
                elif str(e) == 'Cannot check payment in Platebator because of missing payment_platebator_uuid.':
                    additional_info = u'Missing payment_platebator_uuid'
                    logger.warning('Missing payment_platebator_uuid for ticket {} at {}'.format(
                        self.ticket.hid,
                        self.ticket.getId(),
                        ))
                else:
                    raise

        item = self.get_ticket_detail(self.ticket)
        if additional_info:
            item['additional_info'] = additional_info

        return 200, item


class View_id_delete(ApiCpkTicketsBaseView):

    def call(self):

        self.reader = self.get_reader()
        self.initial_reader_setup()

        self.ticket = self.get_ticket(self.text_from_request('id'), self.reader)

        # pokud může čtenář stornovat, tak stornovat, jinak vrátit chybu
        # s vysvětlením

        # kód pro inspiraci z ticket_core_cancel_complaint.py
        # # zjistíme, zda aktuální uživatel může přímo udělat velké resp. malé
        # # storno a vše podle toho nastavíme
        # if not self.ticket.is_big_cancel_asked and (
        #         (self.is_reader and self.ticket.can_cancel_directly_reader) or \
        #         (self.is_librarian_zk and self.ticket.can_cancel_directly)
        #         ):

        if not self.ticket.is_big_cancel_asked and \
                self.ticket.can_cancel_directly_reader:
            # inspirováno _ticket_cancel z ticket_core_cancel_complaint
            # can_cancel_directly_reader znamená, že wf_state je created, takže
            # zejména neexistuje žádný subticket - to umožňuje vše zjednodušit
            # velké storno
            wf_action(self.ticket, 'cancel', '')
            now_iso = self.dt_as(DateTime(), 'iso')
            self.ticket.closed_date = now_iso
            self.ticket.unread_zk = True
            self.send_notifications('wf_t_cancel_by_reader_zk_api')
            self.send_notifications('wf_t_cancel_by_reader_r_api_self')
            self.add_historylog(
                u'Objednávka byla stornována.',
                who=['zk', 'r'],
                )
            self.ticket.reindexObject()
        else:
            # nelze stornovat
            raise ApiException422('Ticket cannot be cancelled.')

        return 200, dict(message=u'Ticket has been cancelled')


class View_post(ApiCpkTicketsBaseView):
    """Create new ticket for reader by eppn.

    {"eppn": "1185@mzk.cz", "ticket_type": "mvs", "doc_id": "mzk.MZK01-001579506", "date_requested": "2019-01-23"}

    {'url': 'http://ziskej.private/api_cpk_tickets_post',
    'params': {'eppn': '1185@mzk.cz', 'ticket_type': 'mvs', 'doc_id': 'mzk.MZK01-001579506', 'date_requested': '2019-01-23', 'token': '...'}
    }
    """

    def call(self):

        # Pro vytvoření nové objednávky je potřeba plná kontrola vč. zda je
        # mateřská knihovna aktivní, parametr full.
        self.reader = self.get_reader(full=True)
        self.initial_reader_setup(full=True)

        ticket_type = self.text_from_request(
            'ticket_type', lov=['mvs', 'edd', ], not_empty=True)
        ticket_type
        if ticket_type == 'mvs':
            obj_id = self.mvs_body(ticket_type)
        elif ticket_type == "edd":
            obj_id = self.edd_body(ticket_type)

        return 201, dict(id=obj_id)


    def mvs_body(self, ticket_type):
        obj_dict = dict()

        #obj_dict['reader_lid'] = self.text_from_request('reader_lid')

        # Validovat, zda ŽK poskytuje MVS jako ŽK
        if not self.library.Is_mvs_zk(now_dt=self.testing_now_dt):
            logger.warning('api_cpk create ticket library does not provide MVS as ZK')
            raise ApiException422('Library does not provide MVS')

        # Validovat, zda ŽK poskytuje MVS jako ŽK i s přihlédnutím k
        # dovolené (vrátí jiný důvod)
        if not self.library.Is_mvs_zk(
                ignore_vacation=False, now_dt=self.testing_now_dt):
            logger.warning('api_cpk create ticket library does not provide MVS as ZK')
            raise ApiException422('Library does not provide MVS due to vacation')

        obj_dict['created_date'] = self.dt_as(DateTime(), 'iso')
        obj_dict['ticket_type'] = u'mvs'
        obj_dict['created_by'] = 'cpk_reader'

        obj_dict['reader'] = self.reader.getId()
        obj_dict['reader_lid'] = u''
        obj_dict['reader_firstname'] = self.reader.first_name
        obj_dict['reader_lastname'] = self.reader.last_name
        # FIXME
        # obj_dict['reader_approval'] = reader.is_library_confirmed
        obj_dict['reader_approval'] = u''
        obj_dict['library_zk'] = self.library_zk_sigla
        obj_dict['library_zk_title'] = self.library.Title()
        obj_dict['librarian'] = u''
        obj_dict['reader_date'] = self.date_from_request('date_requested', result_type='iso')
        obj_dict['library_zk_note'] = u''
        obj_dict['unread_zk'] = True
        obj_dict['lb_state'] = u''
        obj_dict['lb_manual_library'] = u''
        obj_dict['lb_manual_library_sent'] = u''

        cpk_post_api = dict(
            service=u'mvs',
            source_id=u'ziskej_api_cpk',
            )
        cpk_post_api['doc_id'] = self.text_from_request('doc_id')
        if not cpk_post_api['doc_id']:
            raise ApiException422('Missing doc_id.')

        doc_alt_ids = self.list_of_texts_from_request('doc_alt_ids')
        if doc_alt_ids is not None:
            cpk_post_api['doc_alt_ids'] = repr(doc_alt_ids)
        else:
            # pro prázdný seznam list_of_texts_from_request vrátí None, my
            # ale potřebujeme prázdný seznam a ne None
            cpk_post_api['doc_alt_ids'] = []
        #print("doc_alt_ids:", repr(doc_alt_ids))
        #print("doc_alt_ids:", cpk_post_api['doc_alt_ids'])
        # cpk_post_api['doc_alt_ids'] = u'['
        # for doc_alt_id in doc_alt_ids:
        #     cpk_post_api['doc_alt_ids'] += u"u'{}', ".format(doc_alt_id)
        # if len(cpk_post_api['doc_alt_ids']) > 1:
        #     cpk_post_api['doc_alt_ids'] = cpk_post_api['doc_alt_ids'][:-2]
        # cpk_post_api['doc_alt_ids'] += u']'

        # FIXME Smazat před nasazením na produkci
        try:
            logger.info("api_cpk_tickets_post cpk_post_api: {}".format(str(cpk_post_api)))
        except:
            raise

        try:
            lb_data = parse_cpk_post(self.portal,
                                        cpk_post_api,
                                        library_zk_sigla=self.library_zk_sigla,
                                        include_fee_est=False,
                                        include_doc_data=True)
        except LoadBalancingException, e:
            error_message = str(e)

            # Jen Souborný katalog
            if error_message == 'Invalid request: cannot process only Souborny katalog':
                raise ApiException422('Invalid request: cannot create ticket for doc_id Souborny katalog without any alternative.')

            # CPK neposkytuje dost informací pro hlavní dokument, např.
            # nemá jednotky
            if error_message == 'Not enough data for main doc':
                raise ApiException422('Invalid request: cannot create ticket due to invalid main record in CPK such as missing unit ids etc.')

            # Ošetřit chyby z CPK, např. not found:
            # http status is not OK (400) with message Error loading record
            skip_str = u'CPK Request failed: '
            if error_message[:len(skip_str)] == skip_str:
                error_message = error_message[len(skip_str):]
                raise ApiException500('Cannot create ticket due to error given by CPK: ' + error_message)

            # Další výjimečné situace jsou (zatím) interní chybou
            raise ApiException500('Cannot create ticket due to internal error: ' + error_message)

        #print("lb_data:", lb_data)
        """
        lb_data: {
            'docs_data_json': u'{"doc_ids": ["mzk.MZK01-001542205", "nlk.69370", "nkp.NKC01-000266332"], "version": 2, "data": {"mzk.MZK01-001542205": {"caslin_info": null, "doc_id": "mzk.MZK01-001542205", "unit_ids": ["BOA001.MZK01001542205.MZK50001577303000010"], "sigla": "BOA001"}, "nlk.69370": {"caslin_info": null, "doc_id": "nlk.69370", "unit_ids": ["ABA008.K101649", "ABA008.K19810604"], "sigla": "ABA008"}, "nkp.NKC01-000266332": {"caslin_info": null, "doc_id": "nkp.NKC01-000266332", "unit_ids": ["ABA001.NKC01000266332.NKC50000266332000010"], "sigla": "ABA001"}}}',
            'fee_est': {
                'count': 0,
                'avg': None,
                'avgcount': 0,
                'min': None,
                'max': None,
                'avgsum': 0
            },
            'doc_data_json': u'{"unit_ids": ["BOA001.MZK01001542205.MZK50001577303000010"], "caslin_info": null, "version": 2, "fullname": ". BCG vaccination policies. 17 stran.", "doc_id": "mzk.MZK01-001542205", "sigla": "BOA001"}',
            'fullname': u'. BCG vaccination policies. 17 stran.',
            'siglas': [u'BOA001', u'ABA008', u'ABA001'],
            'id': u'mzk.MZK01-001542205'
        }
        """

        # Nedovolit vytvořit objednávku, pokud hlavní dokument ani žádná
        # z alternativ není poskytován knihovnou aktivní v Získej
        # a s aktivní službou MVS jako DK.
        is_any_mvs_dk = False
        for sigla in lb_data.get('siglas', []):
            library = getattr(self.portal.libraries, sigla.lower(), None)
            if not library:
                continue
            if library.Is_mvs_dk():
                is_any_mvs_dk = True
                break
        if not is_any_mvs_dk:
            raise ApiException422('Invalid request: cannot create ticket for doc_id and doc_alt_ids due to no corrensponding library is providing MVS.')

        obj_dict['doc_manual'] = False

        # původní varianta
        # obj_dict['doc_id'] = lb_data['id']
        # obj_dict['doc_title'] = lb_data['title']
        # obj_dict['doc_fullname'] = lb_data['fullname']
        # obj_dict['doc_data'] = lb_data['doc_data']
        # obj_dict['docs_data'] = lb_data['docs_data']

        # nová podoba
        obj_dict['doc_id'] = lb_data['id']
        obj_dict['doc_title'] = lb_data['fullname']
        obj_dict['doc_fullname'] = lb_data['fullname']
        obj_dict['doc_data'] = lb_data['doc_data_json']
        obj_dict['docs_data'] = lb_data['docs_data_json']

        reader_note = self.text_from_request('reader_note')

        try:
            logger.info('obj_dict')
            logger.info(str(obj_dict))
        except:
            logger.info('obj_dict logger failed')

        """
        curl -d '{"eppn": "1185@mzk.cz", "ticket_type": "mvs", "doc_id": "mzk.MZK01-001579506", "doc_alt_ids": ["nkp.NKC01-002901834"], "date_requested": "2019-01-23"}' -H "Content-Type: application/json" -H "Authorization: bearer $token" https://ziskej-test.techlib.cz:9080/api/v1/tickets

        {'library_zk': u'boa001', 'lb_manual_library': u'', 'lb_state': u'', 'librarian': u'', 'doc_title': u'THALER, Richard H.; KALANDRA, Jan. Neo\u010dek\xe1van\xe9 chov\xe1n\xed. Vyd\xe1n\xed prvn\xed. 377 stran. ISBN 978-80-257-2121-6.', 'doc_manual': False, 'unread_zk': True, 'lb_manual_library_sent': u'', 'reader_date': '2019-01-23', 'library_zk_title': u'BOA001: MZK', 'reader': 'r_c85b88f32f51', 'reader_firstname': u'Jakub', 'hid_prefix': u'MVA', 'reader_lid': u'', 'reader_lastname': u'Nov\xe1k', 'doc_data': None, 'docs_data': None, 'library_zk_note': u'', 'doc_fullname': u'THALER, Richard H.; KALANDRA, Jan. Neo\u010dek\xe1van\xe9 chov\xe1n\xed. Vyd\xe1n\xed prvn\xed. 377 stran. ISBN 978-80-257-2121-6.', 'created_date': '2019-02-08', 'reader_approval': False, 'doc_id': u'mzk.MZK01-001579506'}
        """

        obj = create_ticketmvs(obj_dict)
        if not obj:
            raise ApiException500('Cannot create ticket.')
        obj_id =obj.getId()

        if reader_note:
            obj.add_message_txt(u'reader', reader_note)

        comment = u'Čtenář {user_id} vytvořil požadavek pomocí Získej API.'.format(user_id=self.user_id)
        obj.add_historylog(comment, who=['zk', 'dk'])

        # unread
        obj.unread_zk = True

        if USE_PLATEBATOR:
            # vytvořit platbu pomocí Platebátor API a nastavit vše potřebné
            # v ticketu (obj)
            error_message = platebator_create(obj, self.eppn)
            if error_message:
                error_message = error_message.encode('ascii', errors='replace')
                raise ApiException422(error_message)

        # nastaveni notifikaci viz notify()
        self.ticket = obj
        self.send_notifications('reader_creates_mvst_api')
        self.send_notifications('reader_creates_mvst_api_self')

        #item = self.get_ticket_detail(obj)

        return obj_id

    def edd_body(self, ticket_type):
        """Z dokumentace k EDD:

        EDD přes API pro CPK pro čtenáře má varianty:

            1) auto article: známe doc_id v CPK + článek
            2) auto selection: známe doc_id v CPK + výňatek
            3) manual article: neznáme doc_id v CPK + článek
            4) manual selection: neznáme doc_id v CPK + výňatek

        Přičemž přes Získej UI lze zadat (jako knihovník, tzn. asistovaná
        objednávka) varianty 3 a 4.

        Liší se od MVS, kde pro asistovaně (knihovník):

            - přes Získej UI lze zadat neznáme doc_id
            - přes CPK lze přejít do Získej UI pro známe doc_id
            a pro samoobslužně (čtenář):
            - přes CPK UI lze pomocí API pro CPK pro čtenáře pro známe doc_id
        """

        if not self.library.Is_edd_zk():
            raise ApiException422("Your library does not support EDD")

        obj_dict = dict()

        # EDD data json dict keys
        # edd: is_article, pages_from, pages_to, pages_number, pages_html,
        #      pages_str, doc_title_in, doc_quote
        #      doc_number_year, doc_number_pyear, doc_number_pnumber
        # edd selection: doc_volume
        # edd auto: doc_id_in, is_container
        # počítané: pages_number, pages_html, pages_str
        edd = dict()

        obj_dict['created_date'] = self.dt_as(DateTime(), 'iso')
        obj_dict['ticket_type'] = u'edd'  # = ticket_type
        obj_dict['created_by'] = 'cpk_reader'

        obj_dict['reader'] = self.reader.getId()
        obj_dict['reader_lid'] = u''
        obj_dict['reader_firstname'] = self.reader.first_name
        obj_dict['reader_lastname'] = self.reader.last_name
        obj_dict['reader_approval'] = u''

        obj_dict['library_zk'] = self.library_zk_sigla
        obj_dict['library_zk_title'] = self.library.Title()
        obj_dict['librarian'] = u''

        obj_dict['reader_date'] = self.date_from_request(
            'date_requested', result_type='iso')
        obj_dict['library_zk_note'] = u''
        obj_dict['unread_zk'] = True

        obj_dict['lb_state'] = u''
        obj_dict['lb_manual_library'] = u''
        obj_dict['lb_manual_library_sent'] = u''

        ticket_doc_data_source_lov = ['auto', 'manual', ]
        ticket_doc_data_source = self.text_from_request(
            'ticket_doc_data_source',
            lov=ticket_doc_data_source_lov,
            not_empty=True)
        obj_dict['doc_manual'] = bool(ticket_doc_data_source == "manual")

        edd_subtype_lov = ['article', 'selection']
        edd_subtype = self.text_from_request(
            'edd_subtype',
            lov=edd_subtype_lov,
            not_empty=True)

        obj_dict['doc_title'] = self.text_from_request(
            'doc_title', not_empty=True)
        edd['doc_title_in'] = self.text_from_request(
            'doc_title_in', not_empty=True)

        obj_dict['doc_author'] = self.text_from_request('doc_author')
        obj_dict['doc_issuer'] = self.text_from_request('doc_issuer')
        obj_dict['doc_note'] = self.text_from_request('doc_note')
        obj_dict['date_requested'] = self.date_from_request('date_requested')

        edd['doc_alt_ids'] = self.list_of_texts_from_request('doc_alt_ids')
        edd['doc_title_in'] = self.text_from_request('doc_title_in')
        edd['doc_quote'] = self.text_from_request('doc_citation')

        # Ze specifikace zadání:
        # Umožnit strany pro jakoukoli EDD objednávku, vždy volitelně
        edd['pages_from'] = self.int_from_request('pages_from')
        edd['pages_to'] = self.int_from_request('pages_to')

        edd['doc_number_year'] = self.text_from_request('doc_number_year')
        edd['doc_number_pyear'] = self.text_from_request('doc_number_pyear')
        edd['doc_number_pnumber'] = self.text_from_request('doc_number_pnumber')

        if edd_subtype == "article":
            edd['is_article'] = True
            obj_dict['doc_issn'] = self.text_from_request('doc_issn')

        elif edd_subtype == "selection":
            edd['is_article'] = False
            obj_dict['doc_isbn'] = self.text_from_request('doc_isbn')
            obj_dict['doc_issn'] = self.text_from_request('doc_issn')
            edd['doc_volume'] = self.text_from_request('doc_volume')

        # Manualní objednávka
        if obj_dict['doc_manual']:
            pass

        # Automatická objednávka.
        elif not obj_dict['doc_manual']: 
            obj_dict['doc_id'] = self.text_from_request(
                'doc_id', not_empty=True)
            # doc_id kontejneru, má smysl jen neprázdné a různé od doc_id, v tom
            # případě je doc_id přímo doc_id článku nebo kapitoly a doc_id_in je
            # doc_id časopisu nebo monografie, v opačném případě je doc_id
            # doc_id kontejneru
            doc_id_in = self.text_from_request('doc_id_in')
            edd['is_container'] = True
            if doc_id_in and doc_id_in != obj_dict['doc_id']:
                edd['doc_id_in'] = doc_id_in
                edd['is_container'] = False

            # FIXME-EDD: Načíst z CPK API potřebné informace pro doc_id,
            # doc_id_in, doc_alt_ids

        # Stejné jako v ticket crerate.
        pages_from = edd['pages_from']
        pages_to = edd['pages_to']
        if pages_from is None and pages_to is None:
            pass
        elif pages_from is None and pages_to is not None:
            raise ApiException422('Missing pages_from')
        elif pages_from is not None and pages_to is not None:
            if pages_from == pages_to:
                # Zjednodušit bez informování uživatele.
                edd['pages_to'] = None
            elif pages_from > pages_to:
                raise ApiException422('Pages_to is less than pages_from')

        # Počítané edd attr se přidají až po vytvoření objednávky

        obj_dict['edd_data'] = json.dumps(edd, ensure_ascii=False)
        obj = create_ticketmvs(obj_dict)
        if not obj:
            logger.warning('Cannot create ticket. \n obj_dict: {}'.format(obj_dict))
            raise ApiException500('Cannot create ticket.')
        obj_id =obj.getId()

        obj.calculate_and_update_edd_data()

        reader_note = self.text_from_request('reader_note')
        if reader_note:
            obj.add_message_txt(u'reader', reader_note)

        obj.unread_zk = True
        self.ticket = obj

        return obj_id


class View_messages_get(ApiCpkTicketsBaseView):
    """Get all messages between reader and ZK."""
    def call(self):
        self.reader = self.get_reader()
        self.initial_reader_setup()

        self.ticket = self.get_ticket(self.text_from_request('id'), self.reader)

        items = self.ticket.get_messages(ziskej_api=True)
        return 200, dict(items=items)


class View_messages_post(ApiCpkTicketsBaseView):
    """Create new message from reader to ZK."""
    def call(self):
        self.reader = self.get_reader()
        self.initial_reader_setup()

        self.ticket = self.get_ticket(self.text_from_request('id'), self.reader)

        text = self.text_from_request('text', not_empty=True)

        self.ticket.add_message_txt('reader', text)

        self.send_notifications('pm_r_zk_reader_api')
        #self.send_notifications('pm_r_zk_reader_api_self')

        #items = ticket.get_messages(ziskej_api=True)
        return 201, self.ticket.get_messages_counts()

class View_messages_put(ApiCpkTicketsBaseView):
    """Get all messages between reader and ZK."""
    def call(self):
        self.reader = self.get_reader()
        self.initial_reader_setup()

        self.ticket = self.get_ticket(self.text_from_request('id'), self.reader)

        self.ticket.pm_unread = ''
        self.ticket.pm_date = None
        return 200, self.ticket.get_messages_counts()

