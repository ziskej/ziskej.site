# -*- coding: utf-8 -*-
"""BrowserView: Získej API pro CPK, část Získej app, knihovny.
"""

from api_cpk_base import ApiCpkView, SERVICE_LOV


class View_get(ApiCpkView):
    """libraries GET

    Seznam sigel všech knihoven aktivních v Získej a poskytujících službu
    specifikovanu ve volitelném parametru service, default je mvszk.

    Paremetr service:
        mvszk (default) .. MVS jako ŽK
        mvsdk .. MVS jako DK
        mvs .. MVS jako ŽK nebo DK

    HTTP 200
    items .. list of
        sigla .. f.e. ABA001

    Example:
    {items: ['ABA001', 'ABA013', 'BOA001']}
    """

    def call(self):
        service = self.text_from_request('service', lov=SERVICE_LOV)
        include_deactivated = bool(
            self.request.get('include_deactivated', None))
        return 200, dict(
            items = self.get_libraries(
                service=service, include_deactivated=include_deactivated),
            )
