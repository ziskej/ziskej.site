# -*- coding: utf-8 -*-
"""BrowserView: cron jobs.
"""

from __future__ import print_function

from datetime import datetime
import json
import logging
import random

from DateTime import DateTime
from plone import api
from plone.protect.interfaces import IDisableCSRFProtection
from zope.interface import alsoProvides

from ziskej.site import BUILDOUT_TYPE
from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.library_helpers import lib_helper
from ziskej.site.library_payment_helpers import (
    platebator_check,
    ZiskejAsyncException,
    )
from ziskej.site.notifications import ZiskejNotifications, ErrorNotifications
from ziskej.site.storage import storage_set, storage_get, storage_del
from ziskej.site.ticket_helpers import (
    TICKET_PORTAL_TYPES,
    SUBTICKET_PORTAL_TYPES,
    wf_action,
    )
from ziskej.site.ticket_load_balancing import (
    load_balancing_availability_init,
    load_balancing_availability_retrieve,
    )
from ziskej.site.utils import datetime_add_wd, pprint, get_instance_type_label
from ziskej.site.ziskej_parameters import ziskej_parameter


DEBUG = False

logger = logging.getLogger("z.cron")


class CronView(ZiskejBrowserView):
    """Browser view s anonymním přístupem na adrese cron_ep_ty4wWVzYsMbMZ5g5
    zabezpečující tasky prováděné každou hodinu resp. každý den.

    Bez parametru action provede kontrolu datum a čas posledního daily i hourly
    a podle toho provede daily a/nebo hourly.  Pro action=daily kontroluje
    a případně provádí jen daily, obdobně pro hourly.

    Doporučené nastavení crontab:

        03 * * * * curl -s "http://localhost:14180/ziskej/home/cron_ep_ty4wWVzYsMbMZ5g5?action=hourly" >> /zope/ziskej/var/cron.log
        33 07 * * * curl -s "http://localhost:14180/ziskej/home/cron_ep_ty4wWVzYsMbMZ5g5?action=daily" >> /zope/ziskej/var/cron.log

    Rozestup je doporučený minimálně 20 minut, protože se provádí test na
    rozestup 0.01 dne, což je 14.4 minuty.

    Tasky každou hodinu:
    - check_platebator_check: Zkontrolovat v Platebátoru všechny dosud
                              nezaplacené objednávky, které jsou k zaplacení.
    - check_tickets_paid_uncorfimed: Zkontrolovat lhůtu pro reakci na novou
                                     zaplacenou objednávku.  Varianta hourly.
    - check_subtickets_queued: Zkontrolovat lhůtu pro reakci na přidělený
                               požadavek.  Varianta hourly.

    Tasky každý den:
    - check_tickets_paid_uncorfimed: Zkontrolovat lhůtu pro reakci na novou
                                     zaplacenou objednávku.  Varianta daily.
    - check_subtickets_queued: Zkontrolovat lhůtu pro reakci na přidělený
                               požadavek.  Varianta daily.
    """

    def check_platebator_check(self):
        """Zkontrolovat v Platebátoru všechny dosud nezaplacené objednávky,
        které jsou k zaplacení.
        """

        result = u''
        now = DateTime(self.now)

        query = dict(
            portal_type = TICKET_PORTAL_TYPES,
            review_state = 'created',
            sort_on = 'hid',
            sort_order = 'reverse',
            )
        brains = self.catalog(query)
        for brain in brains:
            obj = brain.getObject()

            if not obj.is_created_by_cpk_for_reader():
                continue
            if obj.is_paid_in_advance():
                continue

            try:
                is_updated = platebator_check(
                    obj, now, send_notifications=self.send_notifications,
                    request=self.request)
            except ZiskejAsyncException, e:
                logger.info('Ticket {} checked in Platebator with Exception: {}'.format(
                    obj.hid,
                    str(e),
                    ))
                continue

            if not is_updated:
                if DEBUG:
                    logger.info('Ticket {} checked in Platebator and still unpaid.'.format(obj.hid))
                continue

            logger.info('Ticket {} checked in Platebator and it is paid, ticket is updated.'.format(obj.hid))

            result += u'V Platebátoru zjištěno zaplacení objednávky {} čtenářem.  '.format(obj.hid)

        return result

    def check_tickets_paid_uncorfimed(self, is_hourly=False, is_daily=False):
        """Zkontrolovat lhůtu pro reakci na novou zaplacenou objednávku.
        """

        result = u''
        now = DateTime(self.now)
        now_1d = now - 1

        query = dict(
            portal_type = TICKET_PORTAL_TYPES,
            review_state = 'created',
            sort_on = 'hid',
            sort_order = 'reverse',
            )
        brains = self.catalog(query)

        for brain in brains:
            obj = brain.getObject()

            # Přeskočit:
            # - nevytvořeno pomocí CPK UI
            if not obj.is_created_by_cpk_for_reader():
                continue
            # - ověřen resp. odmítnut
            if obj.reader_approval:
                continue
            # - zatím nezaplaceno
            if not obj.is_paid_in_advance():
                continue

            # Nyní tedy existuje obj.payment_paid_date

            # Přeskočit:
            # - ještě neuběhlo 24 h od zaplacení - bez ohledu na pracovní dobu
            if obj.payment_paid_date > now_1d:
                if DEBUG:
                    logger.info('Ticket {} paid and uncorfimed, less than 24 hours.'.format(obj.hid))
                continue

            # Vždy platí: is_hourly == not is_daily, ale další kód s tím
            # nepočítá, s výjimkou nedokončené transakce po odeslání notifikace.

            send_notification = False

            # Jednou za hodinu kontrolovat jen odeslání úplně první notifikace
            # po uplynutí prvních 24 h (nezávisle na pracovní době).
            if is_hourly:
                if obj.next_action_last_notification is None:
                    send_notification = True
                    message = u'Objednávka {} zaplacena čtenářem a zatím nepotvrzena ŽK, více než 24 h od zaplacení a zatím neodeslána žádná notifikace, odeslat.  '.format(obj.hid)

            # Následně (tzn. jakmile alespoň jedna notifikace už odešla)
            # odesílat pravidelně v rámci kontroly jednou za den.  Pokud by
            # jednou za den bylo dříve než první notifikace v rámci jednou za
            # hodinu, tak odeslat rovnou tu jednou za den.  Pokud první
            # notifikace byla odeslána a uběhlo už cca 14.4 minut (0.01 dne),
            # poslat notifikaci v rámci jednou za den.
            if is_daily:
                if obj.next_action_last_notification is None:
                    send_notification = True
                    message = u'Objednávka {} zaplacena čtenářem a zatím nepotvrzena ŽK, více než 24 h od zaplacení a zatím neodeslána žádná notifikace, odeslat.  '.format(obj.hid)
                elif obj.next_action_last_notification + 0.01 <= now:
                    send_notification = True
                    message = u'Objednávka {} zaplacena čtenářem a zatím nepotvrzena ŽK, více než 24 h od zaplacení a denní notifikace zatím nebyla neodeslána, odeslat.  '.format(obj.hid)

            if not send_notification:
                continue

            # Odeslat notifikaci a zaznamenat datum a čas jejího odeslání
            self.send_notifications('t_paid_uncorfimed_cron', ticket=obj)
            obj.next_action_last_notification = now  # DateTime
            obj.reindexObject()
            logger.info('Ticket {} notification t_paid_uncorfimed_cron sent.'.format(obj.hid))

            result += message

        return result

    def check_subtickets_queued(self, is_hourly=False, is_daily=False):
        """Zkontrolovat lhůtu pro reakci na novou zaplacenou objednávku.
        """

        result = u''
        now = DateTime(self.now)
        now_1d = now - 1

        query = dict(
            portal_type = SUBTICKET_PORTAL_TYPES,
            review_state = 'queued',
            sort_on = 'hid',
            sort_order = 'reverse',
            )
        brains = self.catalog(query)

        for brain in brains:
            obj = brain.getObject()

            # obj.assigned_date je iso string obsahující jen datum bez času,
            # nehodí se pro sledování 24 h limitu, používáme místo toho
            # obj.created_dt property.
            assigned_date = obj.created_dt  # DateTime

            # Přeskočit:
            # - chybějící assigned_date, nemůže nastat
            if assigned_date is None:
                logger.warning('Subticket {} missing assigned_date.'.format(obj.hid))
                continue
            # - ještě neuběhlo 24 h od přidělení do fronty - bez ohledu na
            #   pracovní dobu
            if DEBUG or BUILDOUT_TYPE in ('test', 'development', ):
                logger.info('check_subtickets_queued {}: assigned_date {} now_1d {}'.format(
                    obj.hid,
                    assigned_date.ISO()[:19],
                    now_1d.ISO()[:19],
                    ))
            if assigned_date > now_1d:
                if DEBUG or BUILDOUT_TYPE in ('test', 'development', ):
                    logger.info('Subticket {} queued in less than 24 hours.'.format(obj.hid))
                continue

            # Vždy platí: is_hourly == not is_daily, ale další kód s tím
            # nepočítá, s výjimkou nedokončené transakce po odeslání notifikace.

            send_notification = False

            # Jednou za hodinu kontrolovat jen odeslání úplně první notifikace
            # po uplynutí prvních 24 h (nezávisle na pracovní době).
            if is_hourly:
                if obj.next_action_last_notification is None:
                    send_notification = True
                    message = u'Požadavek {} přidělen DK, více než 24 h od přidělení a zatím neodeslána žádná notifikace, odeslat.  '.format(obj.hid)

            # Následně (tzn. jakmile alespoň jedna notifikace už odešla)
            # odesílat pravidelně v rámci kontroly jednou za den.  Pokud by
            # jednou za den bylo dříve než první notifikace v rámci jednou za
            # hodinu, tak odeslat rovnou tu jednou za den.  Pokud první
            # notifikace byla odeslána a uběhlo už cca 14.4 minut (0.01 dne),
            # poslat notifikaci v rámci jednou za den.
            if is_daily:
                if obj.next_action_last_notification is None:
                    send_notification = True
                    message = u'Požadavek {} přidělen DK, více než 24 h od přidělení a zatím neodeslána žádná notifikace, odeslat.  '.format(obj.hid)
                elif obj.next_action_last_notification + 0.01 <= now:
                    send_notification = True
                    message = u'Požadavek {} přidělen DK, více než 24 h od přidělení a denní notifikace zatím nebyla notifikace, odeslat.  '.format(obj.hid)

            if not send_notification:
                continue

            # Odeslat notifikaci a zaznamenat datum a čas jejího odeslání
            self.send_notifications('wf_st_queued_cron', subticket=obj)
            obj.next_action_last_notification = now  # DateTime
            obj.reindexObject()
            logger.info('Subticket {} notification wf_st_queued_cron sent.'.format(obj.hid))

            result += message

        return result


    def check_readers_return_five_days(self, is_hourly=False, is_daily=False):
        """Zkontrolovat jestli nemá čtenář za 5 dní vrátit.
        """

        result = u''
        now = DateTime(self.now)
        now_1d = now - 1
        dt_in5_dt = DateTime(self.now) + 5
        dt_in5_iso = dt_in5_dt.ISO()[:10]

        query = dict(
            portal_type = TICKET_PORTAL_TYPES,
            review_state = 'prepared',
            sort_on = 'hid',
            sort_order = 'reverse',
            )
        brains = self.catalog(query)

        for brain in brains:
            obj = brain.getObject()

            # Přeskočit:
            # - Není ještě půjčeno čtenářem.
            if not obj.is_reader_lent:
                continue
            # - Čtenář už vrátil.
            if obj.is_reader_return:
                continue

            send_notification = False

            if is_daily:
                if obj.reader_to_date and obj.reader_to_date <= dt_in5_iso:
                    send_notification = True
                    message = u'Výpůjční doba objednávky {hid} vyprší dne {reader_to_date}.'.format(hid=obj.hid, reader_to_date=obj.reader_to_date,)

            if not send_notification:
                continue

            # Odeslat notifikaci a zaznamenat datum a čas jejího odeslání
            self.send_notifications('wf_t_return_reader', ticket=obj)
            obj.next_action_last_notification = now  # DateTime
            obj.reindexObject()
            logger.info('Ticket {} notification wf_t_return_reader sent.'.format(obj.hid))

            result += message

        return result

    def check_edd_actions(self, is_hourly=False, is_daily=False):
        """Zkontrolovat EDD automatické akce:

        Automatické uzavírání a mazání PDF souboru, když už nelze ani
        reklamovat, což nastane, když:

        - pokud nebyl soubor stažen ani jednou, tak po expiraci souboru

        - pokud byl soubor stažen, tak po uběhnutí lhůty po expiraci souboru,
          pokud platí následující podmínky:

          - neprobíhá reklamace

          - pokud byla reklamace zamítnuta, uběhla už lhůta pro podání námitky
        """

        DEBUG_EDD_ACTIONS = False

        if DEBUG_EDD_ACTIONS:
            print(" ")
            print("---------------------------------------------------------------")
            print(" ")

        result = u''
        now = DateTime(self.now)
        # now = now + 100  # DEBUG
        now_del = now - ziskej_parameter('edd_delete_days')  # 35
        # no timezone
        pdt_now = datetime.strptime(
                    now.strftime('%Y-%m-%d %-H:%M'), '%Y-%m-%d %H:%M')
        pdt_del = datetime.strptime(
                    now_del.strftime('%Y-%m-%d %-H:%M'), '%Y-%m-%d %H:%M')
        if DEBUG_EDD_ACTIONS:
            print("pdt_del", pdt_del)

        query = dict(
            portal_type = SUBTICKET_PORTAL_TYPES,
            ticket_type = u'edd',
            review_state = ('sent', 'closed', ),
            sort_on = 'hid',
            sort_order = 'reverse',
            )
        brains = self.catalog(query)

        for brain in brains:
            obj = brain.getObject()

            # žádná akce pokud není EDD, není PDF nebo není nastavena expirace
            # PDF
            if not obj.is_edd or not obj.pdf_file_id or \
                    obj.download_expiration_date is None:
                continue

            if DEBUG_EDD_ACTIONS:
                print("hid:", obj.hid)
                print("download_expiration_date:", repr(obj.download_expiration_date))
                # print(type(obj.download_expiration_date))
                # print(dir(obj.download_expiration_date))

            is_remove_pdf = False

            # automatické uzavření objednávky a požadavku pokud nebylo staženo
            # čtenářem a expiroval PDF soubor
            if not obj.downloaded_number and \
                    obj.download_expiration_date <= pdt_now:
                is_remove_pdf = True
                remove_pdf_reason = u'not-downloaded-expired'

            # automatické uzavření objednávky a požadavku pokud nebylo staženo
            # čtenářem a expiroval PDF soubor
            if obj.downloaded_number and \
                    obj.download_expiration_date <= pdt_del:
                if not obj.complaint_state:
                    is_remove_pdf = True
                    remove_pdf_reason = u'downloaded-complaint-expired'
                elif obj.complaint_state in (
                        'new', 'accepted', 'objected', 'soft', 'soft_ready',
                        'objection_accepted', ):
                    pass
                elif obj.complaint_state == 'declined':
                    # FIXME-EDD podle toho zda uběhla lhůta pro podání námitky
                    # možná by stačilo u změny stavu námitky ověřit, zda
                    # nekončí expirace + lhůta pro reklamaci před nyní + lhůta
                    # pro námitku a pokud ano, tak nastavit tak, aby nekončila
                    pass
                    # remove_pdf_reason = u'downloaded-complaint-objection-expired'
                else:
                    is_remove_pdf = True
                    remove_pdf_reason = u'downloaded-complaint-closed'

            # pokud nebude mazán PDF soubor, žádná akce, další
            if not is_remove_pdf:
                if DEBUG_EDD_ACTIONS:
                    print("  - skip")
                continue

            # smazání PDF souboru
            obj.remove_pdf_file(remove_download_expiration_date=False)
            logger.info('Subticket {} PDF file removed.'.format(obj.hid))
            if remove_pdf_reason == u'not-downloaded-expired':
                if DEBUG_EDD_ACTIONS:
                    print("  - removed expired")
                message = u'PDF soubor pro požadavek {hid} expiroval dne ' \
                    u'{download_expiration_date} a byl nyní smazán.'.format(
                        hid=obj.hid,
                        download_expiration_date=\
                            obj.download_expiration_date.strftime(
                                '%Y-%m-%d %-H:%M'),
                        )
            else:
                if DEBUG_EDD_ACTIONS:
                    print("  - removed expired and complainting expired")
                message = u'PDF soubor pro požadavek {hid} expiroval dne ' \
                    u'{download_expiration_date} a po uplynutí ochrané lhůty ' \
                    u'pro reklamace byl nyní smazán.'.format(
                        hid=obj.hid,
                        download_expiration_date=\
                            obj.download_expiration_date.strftime(
                                '%Y-%m-%d %-H:%M'),
                        )
            result += message

            # pokud bude mazán PDF soubor a zatím nebyl uzavřen požadavek,
            # uzavřít, pokud nebyla zatím uzavřena objednávka, tak uzavřít
            is_st_closing = bool(brain.review_state == 'sent')
            is_t_closing = obj.ticket.can_close

            if is_st_closing:
                # close st + t + audit log + notifikace
                wf_action(obj, 'close', u'auto close')
                logger.info('Subticket {} closed.'.format(obj.hid))

            if is_t_closing:
                wf_action(obj.ticket, 'close', u'auto close')
                logger.info('Ticket {} closed.'.format(obj.hid))

            message_tmpl = u''
            if is_st_closing and is_t_closing:
                message_tmpl = u'Požadavek (i objednávka) {hid} uzavřen'
            elif is_st_closing and not is_t_closing:
                message_tmpl = u'Požadavek {hid} uzavřen'
            elif not is_st_closing and is_t_closing:
                message_tmpl = u'Objednávka {hid} uzavřena'

            if message_tmpl:
                if remove_pdf_reason == u'not-downloaded-expired':
                    hl_message = u"Automatické uzavření po expiraci PDF souboru (nebyl nikdy stažen)"
                    message_tmpl += u', protože PDF soubor expiroval dne ' \
                        u'{download_expiration_date} a nebyl nikdy stažen'
                else:
                    hl_message = u"Automatické uzavření po expiraci PDF souboru" \
                        u"a po uplynutí ochrané lhůty pro reklamace"
                    message_tmpl += u', protože PDF soubor expiroval dne ' \
                        u'{download_expiration_date} a uběhly lhůty pro reklamace'

                message = message_tmpl.format(
                    hid=obj.hid,
                    download_expiration_date=obj.download_expiration_date.strftime(
                        '%Y-%m-%d %-H:%M'),
                    )
                result += message

                if DEBUG_EDD_ACTIONS:
                    print("  - closing with audit log:", hl_message)
                self.add_historylog(hl_message, who=['dk', 'zk', 'r'],
                    subticket=obj, is_cron=True)

                self.send_notifications('wf_t_close_edd_r', ticket=obj.ticket)
                self.send_notifications('wf_t_close_edd_zk', ticket=obj.ticket)

            obj.reindexObject()
            obj.ticket.reindexObject()

        return result

    # def check_subtickets_queued_disabled(self):
    #     """Zkontrolovat lhůtu pro reakci na přidělený požadavek.
    #     """

    #     result = u''

    #     st_queued_dk_wdays = ziskej_parameter('st_queued_dk_wdays')  # default 1
    #     st_queued_dk_notification_step_h = ziskej_parameter('st_queued_dk_notification_step_h')  # default 1

    #     query = dict(
    #         portal_type = SUBTICKET_PORTAL_TYPES,
    #         review_state = 'queued',
    #         sort_on = 'hid',
    #         sort_order = 'reverse',
    #         )
    #     brains = self.catalog(query)
    #     send_notification_debug = dict()
    #     if DEBUG:
    #         print ' '
    #         print 'for subtickets queued: debug_dict:'
    #     for brain in brains:
    #         subticket = brain.getObject()

    #         if subticket.next_action_last_notification is None:
    #             next_notification_dt = DateTime(self.now)
    #         else:
    #             next_notification_dt = subticket.next_action_last_notification + \
    #                 1.0 * st_queued_dk_notification_step_h / 24.0
    #         send_notification = bool(
    #             (subticket.next_action_date < DateTime()) and
    #             (
    #                 subticket.next_action_last_notification is None or
    #                 next_notification_dt <= DateTime(self.now)
    #             )
    #             )

    #         send_notification_debug[subticket.hid] = send_notification
    #         if DEBUG:
    #             debug_dict = dict(
    #                 hid = subticket.hid,
    #                 decide_date = subticket.decide_date,
    #                 decided_date = subticket.decided_date,
    #                 next_action_title = subticket.next_action_title,
    #                 next_action_date = subticket.next_action_date,
    #                 next_action_last_notification = subticket.next_action_last_notification,
    #                 send_notification = send_notification,
    #                 )
    #             print ' '
    #             pprint(debug_dict)

    #         if send_notification:
    #             if DEBUG:
    #                 print '--> SEND NOTIFICATION'
    #             self.send_notifications('wf_st_queued_cron', subticket=subticket)
    #             subticket.next_action_last_notification = DateTime(self.now)
    #             subticket.reindexObject()
    #             logger.info('Subticket {} notification wf_st_queued_cron sent.'.format(subticket.hid))
    #             result += u'Poslány notifikace pro požadavek {hid}, kde vypršela lhůta.  '.format(
    #                 hid = subticket.hid)

    #         # DEBUG / TESTING
    #         if self.testing_clean_stnal:
    #             print '*** CLEAN next_action_last_notification for {}'.format(subticket.hid)
    #             subticket.next_action_last_notification = None
    #             subticket.reindexObject()

    #         # subticket.next_action_date .. DateTime
    #         # subticket.decide_date .. iso date (no time)
    #         # dt = datetime_add_wd(
    #         #     st_queued_dk_wdays,
    #         #     start_date = DateTime(subticket.decide_date)  # sent_date .. text iso
    #         #     )
    #         # bool(DateTime() > dt)

    #     if DEBUG:
    #         print ' '
    #         print 'send_notification_debug:'
    #         pprint(send_notification_debug)
    #         print ' '

    #     return result

    def cron_daily(self):
        self.catalog = api.portal.get_tool(name='portal_catalog')
        result = u'Kontrola prováděná každý den.  '
        if self.t_unpaid_hours >= 24:
            result += self.check_platebator_check()
        result += self.check_tickets_paid_uncorfimed(is_daily=True)
        result += self.check_subtickets_queued(is_daily=True)
        result += self.check_readers_return_five_days(is_daily=True)
        result += self.check_edd_actions(is_daily=True)

        if DEBUG:
            print('cron_daily DONE')
        return result.strip()

    def cron_hourly(self):
        self.catalog = api.portal.get_tool(name='portal_catalog')
        result = u'Kontrola prováděná každou hodinu.  '
        if self.t_unpaid_hours < 24:
            result += self.check_platebator_check()
        result += self.check_tickets_paid_uncorfimed(is_hourly=True)
        result += self.check_subtickets_queued(is_hourly=True)
        if DEBUG:
            print('cron_hourly DONE')
        return result.strip()

    def call(self):
        frontpage = self.context
        if frontpage.portal_type != 'Frontpage':
            raise Exception('Invalid request')

        # default 24, dočasně 1
        self.t_unpaid_hours = ziskej_parameter('t_unpaid_hours')

        # action
        action = self.request.get('action', None)
        if action is None:
            is_daily = True
            is_hourly = True
        elif action == 'daily':
            is_daily = True
            is_hourly = False
        elif action == 'hourly':
            is_daily = False
            is_hourly = True
        else:
            is_daily = False
            is_hourly = False

        # daily
        cron_daily = False

        if is_daily:
            today_iso = self.now[:10]
            cron_last_day = storage_get(frontpage, 'cron_last_day')
            if cron_last_day is None:
                cron_daily = True
            else:
                if cron_last_day < today_iso:
                    cron_daily = True
            # DEBUG / TESTING
            if not cron_daily and self.testing_force_cdh:
                print('*** FORCE testing_force_cdh causes cron_daily = True')
                cron_daily = True

        if cron_daily:
            logger.info('cron-daily')
            result_daily = self.cron_daily()
            storage_set(frontpage, 'cron_last_day', today_iso)
        elif is_daily:
            result_daily = u'Kontrola prováděná každý den už byla provedena dříve.'
        else:
            result_daily = None

        # DEBUG / TESTING
        if self.testing_clean_cdh:
            print('*** CLEAN cron_last_day')
            storage_set(frontpage, 'cron_last_day', None)

        # hourly
        cron_hourly = False

        if is_hourly:
            this_hour_iso = self.now[:13]
            cron_last_hour = storage_get(frontpage, 'cron_last_hour')
            if cron_last_hour is None:
                cron_hourly = True
            else:
                if cron_last_hour < this_hour_iso:
                    cron_hourly = True
            # DEBUG / TESTING
            if not cron_hourly and self.testing_force_cdh:
                print('*** FORCE testing_force_cdh causes cron_hourly = True')
                cron_hourly = True

        if cron_hourly:
            logger.info('cron-hourly')
            result_hourly = self.cron_hourly()
            storage_set(frontpage, 'cron_last_hour', this_hour_iso)
        elif is_hourly:
            result_hourly = u'Kontrola prováděná každou hodinu už byla provedena dříve.'
        else:
            result_hourly = None

        # DEBUG / TESTING
        if self.testing_clean_cdh:
            print('*** CLEAN cron_last_hour')
            storage_set(frontpage, 'cron_last_hour', None)

        # result
        value = dict(timestamp=self.now)
        if result_daily:
            value['result_daily'] = result_daily
        if result_hourly:
            value['result_hourly'] = result_hourly
        return value

    def __call__(self):
        now = DateTime()

        self.portal = api.portal.get()

        # DEBUG / TESTING
        # ignorovat, kdy naposledy bylo spuštěno cron*, tzn. spustí se daily
        # i hourly vždy
        self.testing_force_cdh = False
        # na závěr vymaže daily i hourly
        self.testing_clean_cdh = False
        # na závěr zpracování každého subticketu v queued vymaže jeho
        # next_action_last_notification
        self.testing_clean_stnal = False

        self.instance_type, self.instance_type_label, instance_title_postfix = get_instance_type_label()
        self.is_instance_testing = bool(self.instance_type in ('development', 'dev', 'test'))

        # DEBUG / TESTING
        if self.is_instance_testing:
            self.testing_force_cdh = bool(self.request.get('force_cdh', ''))
            self.testing_clean_cdh = bool(self.request.get('clean_cdh', ''))
            self.testing_clean_stnal = bool(self.request.get('clean_stnal', ''))
            # DEBUG timeshift
            # now = now + 1.5/24.0

        self.now = now.ISO()

        alsoProvides(self.request, IDisableCSRFProtection)

        # lib_helper - inicializováno při startu bez portal, pokud ještě nikdy
        # nedostal portal, inicializuje se pomocí bind_portal, jinak levný call
        self.lib_helper = lib_helper
        self.lib_helper.bind_portal(self.portal)

        result_dict = self.call()

        # no diazo
        self.request.response.setHeader('X-Theme-Disabled', '1')

        # json content type
        self.request.response.setHeader('Content-Type', 'application/json')

        # no cache
        self.request.response.setHeader('Cache-Control', 'no-store,no-cache,must-revalidate,post-check=0,pre-check=0')
        self.request.response.setHeader('Pragma', 'no-cache')
        self.request.response.setHeader('Expires', '-1')
        self.request.response.setHeader('Last-modified', self.now)

        # jsonize
        try:
            result_json = json.dumps(result_dict, ensure_ascii=False)
        except Exception, e:
            error_dict = dict(
                status = u'Error',
                error_message = u'Interní chyba, administrátoři byli informováni.',
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        result_json += u'\n\n'
        return result_json

    def send_notifications(self, nid, ticket=None, subticket=None):
        """Wrapper pro notifikace, pošle notifikaci s id nid.  Parametry ticket
        a subticket tady nelze použít objekty nastavené z kontextu, protože
        takový kontext není a proto je nutné je tady předat jako parametr.

        Existence tohoto wrapperu je příčinou toho, že všechny wrappery
        send_notifications, musí přijímat tyto parametry a to i v případě, že je
        ignorují.
        """

        ziskej_notifications = ZiskejNotifications(ticket=ticket,
                                                   subticket=subticket)
        ziskej_notifications.send_notifications(nid)

    def error_notify(self, error_class_id, e, method_id, detail_error=None):
        user_id = 'cron'
        message = '{error_class_id} in {method_id} for user {user_id}: {e}'.format(
            error_class_id = error_class_id,
            e = str(e),
            method_id = method_id,
            user_id = user_id)
        if detail_error:
            message += ' [more info] ' + detail_error
        logger.warning('[error_notify] ' + message)

        error_notifications = ErrorNotifications()
        error_notifications.send_notifications(message)
