# -*- coding: utf-8 -*-
"""BrowserView: Reports.
"""

from __future__ import print_function

import codecs
from datetime import datetime
import json
import logging
from openpyxl import Workbook
import os
import os.path
import uuid

from DateTime import DateTime
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
#from plone.protect.utils import addTokenToUrl

from ziskej.site import ZISKEJ_VAR_DOWNLOAD, BUILDOUT_TYPE
from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.ticket_helpers import (
    TICKET_PORTAL_TYPES,
    SUBTICKET_PORTAL_TYPES,
    REPORT_ENTRY_PORTAL_TYPES,
    TICKETMVS_WF_S_LABELS,
    SUBTICKETMVS_WF_S_LABELS,
    )
from ziskej.site.utils import (
    aow_os_utils_rm,
    datetime_last_month_iso,
    pprint,
    )


logger = logging.getLogger("ziskej")

SERVICE_SHORTNAMES = dict(
    mvs=u'MVS',
    mrs=u'MRS',
    edd=u'EDD',
    )

ADD_INFO_DEBUG = False  # bool(BUILDOUT_TYPE == 'development')
ADD_PLATEBATOR_UID = False

DEBUG = False
DEBUG_DILIA = False


class ReportBaseView(ZiskejBrowserView):
    """Společný základ pro všechny reporty a statistiky.
    """

    def _call_init(self):
        super(ReportBaseView, self)._call_init()
        # Používá se jen v reportu Přehled čerpání z FK, default tedy False.
        self.skip_mvs_zk_ntk_todo_std = False
        self.only_mvs_zk_ntk_todo_std = False
        self.col_idx = None
        self.ticket_type = 'mvs'
        self.service_label = u'MVS'

    def _catalog_search(self, query, date_from=None, date_to=None):
        """Vrátit brains pro query, bypass security."""
        # https://docs.plone.org/develop/plone/searching_and_indexing/query.html#bypassing-query-security-check
        #brains = self.catalog(query)
        brains = self.catalog.unrestrictedSearchResults(query)
        logger.info('catalog ({}) for {}'.format(len(brains), query))
        return brains

    def _dates(self):
        """Filtr: Datum"""
        # uživatel / datepicker poskytuje formát, který převedeme na '2017-11-15',
        # k tomu přidáme '0:00' / '23:59', abychom pokryli celé dny
        date_from = self.date_from_request('date_from', result_type='iso')
        if date_from is None:
            date_from = self.date_from
        self.date_from = date_from

        # viz výše
        date_to = self.date_from_request('date_to', result_type='iso')
        if date_to is None:
            date_to = self.date_to
        self.date_to = date_to

        return date_from, date_to

    def _sigla(self):
        """Filtr: sigla, search_fullname_library"""
        self.sigla = None
        self.sigla_upper = u''
        self.library = None

        if self.is_librarian:
            self.library = self.ziskej_user_info['library']
            if self.library:
                sigla = self.library.sigla
            else:
                sigla = None
            if sigla:
                sigla = sigla.lower()
            return sigla

        sigla = self.text_from_request('sigla')
        if sigla:
            sigla = sigla.lower()
            self.library = self.library_by_sigla(sigla)
            if self.library:
                self.sigla = sigla
                self.sigla_upper = sigla.upper()
                return sigla
            self.add_message(
                u'Nenalezena žádná knihovna.',
                message_type='warning')
            return None

        search_fullname_library = self.text_from_request(
            'search_fullname_library')
        if not search_fullname_library:
            return None
        siglas_as_html = self._get_siglas_by_fullname(search_fullname_library, as_html=True)
        if not siglas_as_html:
            self.add_message(
                u'Žádná knihovna nenalezena, zobrazeno pro všechny knihovny.',
                message_type='warning')
            return None
        siglas = self._get_siglas_by_fullname(search_fullname_library)
        if siglas:
            siglas = [x.lower() for x in siglas]
        if DEBUG:
            print("_get_siglas_by_fullname", siglas)

        if len(siglas) > 1:
            self.add_message(
                u'Statistika je souhrnná pro všechny knihovny zobrazené v modrém oznámení, pro konkrétní knihovnu specifikujte název knihovny nebo zadejte její siglu uvedenou v modrém oznámení.',
                message_type='warning')
            message = u'Nalezeny knihovny {}.'.format(
                u', '.join([x for x in siglas_as_html]))
            if message.endswith(u'..'):
                message = message[:-1]
            self.add_message(message)
        elif len(siglas) == 1:
            message = u'Nalezena knihovna {}.'.format(
                u', '.join([x for x in siglas_as_html]))
            if message.endswith(u'..'):
                message = message[:-1]
            self.add_message(message)

        return siglas


    def call(self):
        self.items_len = 0
        self.unauthorized = True
        self.context_url_view = self.context_url
        self.reports_url = self.context_url
        self.is_ntk = False
        self.is_all = False
        if self.is_librarian and self.ziskej_user_info['library'] and \
                self.ziskej_user_info['library'].sigla == 'ABA013':
            self.is_ntk = True
        if self.is_ziskej_operator:
            sigla = self.text_from_request('sigla')
            if sigla and sigla.lower() == u'aba013':
                self.is_ntk = True
            if not sigla:
                self.is_all = True

    def ticket_get_review_state_label(self, review_state):
        """Poskytuje label pro stavy požadavků.
        """

        return TICKETMVS_WF_S_LABELS.get(review_state, review_state)

    def subticket_get_review_state_label(self, review_state):
        """Poskytuje label pro stavy podpožadavků.
        """

        return SUBTICKETMVS_WF_S_LABELS.get(review_state, review_state)


class ReportsView(ReportBaseView):
    """Seznam reportů.
    """

    template = ViewPageTemplateFile('reports.pt')

    def call(self):
        super(ReportsView, self).call()

        self.context_url_view = self.context_url

        if not (self.is_librarian or self.is_ziskej_operator):
            self.redirect(self.portal_url)
            return
        self.unauthorized = False

        base_url = self.context_url

        self.report_list_mvs = []
        self.report_list_other = []

        url = base_url + '/invoice_library?report=ziskejcredit'
        if self.is_ntk:
            label = u'Podklady pro vyúčtování NTK jako DK (NTK vůči SC ZÍSKEJ)'
            url = base_url + '/invoice_library_from'
        elif self.is_ziskej_operator:
            label = u'Podklady pro fakturaci knihovny vůči NTK, přehled pohybů na kontě v ZÍSKEJ'
        else:
            label = u'Podklady pro fakturaci knihovny vůči NTK'
        self.report_list_mvs.append(
            dict(
                url = url,
                title = label,
                )
            )

        url = base_url + '/invoice_library?report=ntkcredit'
        if self.is_ntk:
            label = u'Podklady pro vyúčtování NTK jako ŽK (SC ZÍSKEJ vůči NTK)'
            url = base_url + '/invoice_library_to'
        elif self.is_ziskej_operator:
            label = u'Podklady pro fakturaci NTK vůči knihovně, přehled čerpání z finančního konta v NTK'
        else:
            label = u'Podklady pro fakturaci NTK vůči knihovně'
        self.report_list_mvs.append(
            dict(
                url = url,
                title = label,
                )
            )

        if self.is_ziskej_operator:
            label = u'Podklady pro vyúčtování NTK jako DK (NTK vůči SC ZÍSKEJ)'
            self.report_list_mvs.append(
                dict(
                    url = base_url + '/invoice_library_from?sigla=ABA013',
                    title = label,
                    )
                )
            label = u'Podklady pro vyúčtování NTK jako ŽK (SC ZÍSKEJ vůči NTK)'
            self.report_list_mvs.append(
                dict(
                    url = base_url + '/invoice_library_to?sigla=ABA013',
                    title = label,
                    )
                )

        # NTK-197
        label = u'Platby čtenářů'
        self.report_list_mvs.append(
            dict(
                url = base_url + '/invoice_reader?service=mvs',
                title = label,
                )
            )

        # NTK-218
        label = u'Přehled objednávek souhrně (samoobslužně i asistovaně)'
        self.report_list_mvs.append(
            dict(
                url = base_url + '/invoice_library?report=zk',
                title = label,
                )
            )

        # NTK-256
        if not self.is_ntk and self.is_ziskej_operator:
            label = u'Přehled neuskutečněných čerpání z finančního konta v NTK'
            self.report_list_mvs.append(
                dict(
                    url = base_url + '/invoice_library?report=ntkcredittodo',
                    title = label,
                    )
                )

        self.report_list_edd = []

        # Přehled EDD poplatků, knihovna jako DK - pro NTK
        if self.is_ziskej_operator:
            label = u'Dília XML report'
            self.report_list_edd.append(
                dict(
                    url = base_url + '/edd_dilia_xml',
                    title = label,
                    )
                )

        # Přehled EDD poplatků, knihovna jako DK
        if self.is_ntk:
            label = u'Podklady pro vyúčtování NTK jako DK (NTK vůči SC ZÍSKEJ)'
        elif self.is_ziskej_operator:
            label = u'Podklady pro fakturaci knihovny vůči NTK, přehled pohybů na kontě v ZÍSKEJ'
        else:
            label = u'Podklady pro fakturaci knihovny vůči NTK'
        self.report_list_edd.append(
            dict(
                url = base_url + '/edd_library_from?report=edddk',
                title = label,
                )
            )

        # Přehled EDD poplatků, knihovna jako DK - pro NTK
        if self.is_ziskej_operator:
            label = u'Podklady pro vyúčtování NTK jako DK (NTK vůči SC ZÍSKEJ)'
            self.report_list_edd.append(
                dict(
                    url = base_url + '/edd_library_from?report=edddk&sigla=ABA013',
                    title = label,
                    )
                )

        # Platby čtenářů
        label = u'Platby čtenářů'
        self.report_list_edd.append(
            dict(
                url = base_url + '/invoice_reader?service=edd',
                title = label,
                )
            )

        # Přehled EDD poplatků, knihovna jako ŽK
        # Podobné jako platby čtenářů, ale rozdělené na jednotlivé části
        # (Dília, DK) a bez informací o platbě.
        label = u'Informativní přehled EDD poplatků zaplacených čtenáři ŽK'
        self.report_list_edd.append(
            dict(
                url = base_url + '/edd_library_to?report=eddzkall',
                title = label,
                )
            )

        if self.is_ziskej_operator:
            label = u'Poplatky Dílii, podklady pro report pro Dílii za celé Získej, knihovna jako ŽK (pro operátora SC)'
        else:
            label = u'Informativní přehled poplatků Dílii, kdy knihovna byla jako ŽK (neplaťte)'
        self.report_list_edd.append(
            dict(
                url = base_url + '/edd_library_to?report=eddzkdilia',
                title = label,
                )
            )

        self.stats_list = []

        self.stats_list.append(
            dict(
                url = base_url + '/library_stats?service=mvszk',
                title = u'MVS objednávky jako ŽK',
                )
            )

        self.stats_list.append(
            dict(
                url = base_url + '/library_stats?service=mvsdk',
                title = u'MVS požadavky jako DK',
                )
            )

        self.stats_list.append(
            dict(
                url = base_url + '/library_stats?service=eddzk',
                title = u'EDD objednávky jako ŽK',
                )
            )

        self.stats_list.append(
            dict(
                url = base_url + '/library_stats?service=edddk',
                title = u'EDD požadavky jako DK',
                )
            )


        if self.is_librarian:
            pass
        if self.is_ziskej_operator:
            pass


class InvoiceLibraryBaseView(ReportBaseView):
    """Společný základ a většina funkcionality pro finanční reporty.
    """

    def iterations(self):
        """Defalt seznamu iterací.  Jedna iterace je jeden dotaz do catalogu,
        pro každý může být sigla restrikcí ŽK nebo DK, viz
        get_invoice_library_type.
        """

        return [None, ]

    def get_iteration_label(self, iteration):
        """Default název iterace."""
        return None

    def get_invoice_library_type(self, iteration=None):
        """Default typu ŽK/DK, viz iterations docs."""
        return self.invoice_library_type

    def get_iteration_toggle_neg(self, iteration=None):
        """Default zda reportovat opačnou hodnotu z reportentry.cena - vesměs
        odpovídá typu ŽK ve smíšeném reportu.
        """

        return False

    def _get_add_entry_subtype_lib_rdr(self):
        return False

    def _get_add_entry_subtype_edd(self):
        return False

    def _base(self):
        """Inicializační část, zejména nezávislá na tom, zda je submit."""
        self.context_url_view = self.context_url + self.url_postfix

        if not (self.is_librarian or self.is_ziskej_operator):
            self.redirect(self.portal_url)
            return True
        self.unauthorized = False

        self.date_from = None
        self.date_to = None
        self.sigla = None
        self.sigla_upper = u''
        self.export_xlsx = False
        self.export_xlsx_url = None
        self.library = None
        self.sum_fee = 0

        self.date_from, self.date_to = datetime_last_month_iso()

        self.submit = self.request.get('submit', None)
        if not self.submit:
            if self.is_ziskej_operator:
                self.sigla = self.text_from_request('sigla')
                if self.sigla:
                    self.sigla_upper = self.sigla.upper()
            return True

        # submit base
        self.data_cols = [u'ID', u'Typ', u'ŽK', u'DK', u'Cena', u'Datum',
                          u'Poznámky']
        self._base_submit_additional()
        return False

    def _base_submit_additional(self):
        pass

    def _build_query(self, date_from, date_to, sigla, iteration=None):
        query = dict(
            portal_type = REPORT_ENTRY_PORTAL_TYPES,
            ticket_type = self.ticket_type,
            #created = dict(
            report_date = dict(
                query = (
                    DateTime(date_from + ' 0:00:00'),
                    DateTime(date_to + ' 23:59:59'),
                    ),
                range = 'min:max',
                ),
            #sort_on = 'created',
            sort_on = 'report_date',
            #sort_order = 'reverse',
            )
        entry_subtype = self.build_entry_subtype_list(iteration=iteration)
        if entry_subtype:
            query['entry_subtype'] = entry_subtype

        invoice_library_type = self.get_invoice_library_type(
            iteration=iteration)
        if invoice_library_type == 'from':
            if sigla:
                query['library_dk'] = sigla
            url_listing = u'/tickets/listing_dk'
        elif invoice_library_type == 'to':
            if sigla:
                query['library_zk'] = sigla
            url_listing = u'/tickets/listing_zk'
        else:
            logger.error('Invalid invoice_library_type: ' + str(self.invoice_library_type))
            raise Exception('Invalid invoice_library_type')
        return query, url_listing

    def _build_library_cache(self, sigla):
        """Nastavit report_entry_ids a pro credit_log_data pro siglu."""
        # Už bylo nastaveno dříve.
        if sigla in self.library_cache:
            return
        library = getattr(self.portal.libraries, sigla, None)
        if not library:
            logger.warning('Cannot find library {} in _build_library_cache'.format(sigla))
            return
        report_entry_ids = []
        credit_log_data = library.credit_log_data('mvs_zk_ntk_todo')
        if credit_log_data:
            report_entry_ids = [x['report_entry_id']
                for x in credit_log_data
                if x['item_type'] == 'mvs_zk_ntk_todo_std']
        self.library_cache[sigla] = dict(
            report_entry_ids=report_entry_ids,
            credit_log_data=credit_log_data,
            )

    def _build_data(self, brains, url_listing, toggle_neg=False, note=None):
        """Zpracovat brains a zpracované přidat do self.data"""

        if self.skip_mvs_zk_ntk_todo_std or self.only_mvs_zk_ntk_todo_std:
            if not self.library:
                # Operátor nemusí zadat siglu, nebo hledá fulltextem a může
                # najít více knihoven, takže jsou různé ŽK.
                self.library_cache = dict()
            else:
                # Operátor + sigla i ŽK mají jen jednu ŽK, takže ji lze
                # připravit hned.
                report_entry_ids = []
                credit_log_data = self.library.credit_log_data(
                    'mvs_zk_ntk_todo')
                if credit_log_data:
                    report_entry_ids = [x['report_entry_id']
                        for x in credit_log_data
                        if x['item_type'] == 'mvs_zk_ntk_todo_std']

        add_entry_subtype_lib_rdr = self._get_add_entry_subtype_lib_rdr()
        add_entry_subtype_edd = self._get_add_entry_subtype_edd()

        for brain in brains:
            row = []

            # obj is not needed now - in case we need it do not use standard
            # way brain.getObject() because it wont work, use this one
            # instead:
            # obj = brain._unrestrictedGetObject()

            if DEBUG:
                print(" ")
                # print(brain.portal_type)
                # print(brain.getPath())
                # print(brain.hid, ":", brain.cena, "with", obj.entry_type, "/", brain.entry_subtype, "(", brain.ticket_type, ")")
                print(brain.hid, ":", brain.cena, "with", brain.entry_subtype, "(", brain.ticket_type, ") by reader", '"{}"'.format(brain.reader))
                print(" ")

            value = brain.hid
            # localhost:14180/ziskej/tickets/listing_zk?search_id=100268&submit_search=1
            url = u'{}{}?search_id={}&submit_search=1'.format(
                self.portal_url, url_listing, value)
            row.append(
                dict(
                    value = value,
                    display = u'<a href="{}">{}</a>'.format(url, value),
                    )
                )

            value = self.service_label
            row.append(
                dict(
                    value = value,
                    display = value,
                    )
                )

            value = brain.library_zk
            if value:
                value = value.upper()
            row.append(
                dict(
                    value = value,
                    display = value,
                    )
                )

            value = brain.library_dk
            if value:
                value = value.upper()
            row.append(
                dict(
                    value = value,
                    display = value,
                    )
                )

            value = brain.cena
            if value is not None:
                if toggle_neg:
                    value = -value
                value_fee = value
                display = u'{} Kč'.format(value)
            else:
                display = u''
            if self.col_idx is None:
                self.col_idx = len(row)
            row.append(
                dict(
                    value = value,
                    display = display,
                    )
                )

            value = brain.report_date
            dt = DateTime(value)
            row.append(
                dict(
                    value = self.date_format(dt, is_time=False, is_html=False),
                    display = self.date_format(dt, is_time=False, is_html=True),
                    number_format = 'd.m.yyyy h:mm;@',
                    )
                )


            value = u''
            if note:
                value += note
            if add_entry_subtype_lib_rdr:
                entry_subtype = brain.entry_subtype
                if not entry_subtype:
                    value += u'?'
                elif entry_subtype[4:7] == 'lib':
                    value += u'Asistovaně'
                elif entry_subtype[4:7] == 'rdr':
                    value += u'Samoobslužně'
                else:
                    value += u'?'
            if ADD_INFO_DEBUG:
                value += u' {}'.format(brain.entry_subtype)
            if add_entry_subtype_edd:
                entry_subtype = brain.entry_subtype
                if not entry_subtype:
                    value += u'?'
                elif entry_subtype == 'edd-dilia-std':
                    value += u'Poplatek Dílii'
                elif entry_subtype == 'edd-dk-std':
                    value += u'Poplatek DK'
                else:
                    value += u'FIXME: {}'.format(str(entry_subtype))
                if brain.reader:
                    # FIXME-EDD otestovat až bude EDD implementováno v CPK
                    value += u', samoobslužně, čtenář {}'.format(brain.reader)
                else:
                    value += u', asistovaně'
            row.append(
                dict(
                    value = value,
                    display = value,
                    )
                )

            # V reportu Přehled čerpání z FK nezapočítávat, co nebylo strženo.
            if self.skip_mvs_zk_ntk_todo_std:
                # Operátor nemusí zadat siglu, nebo ji zadá fulltextem, takže
                # jsou různé ŽK.
                if not self.library:
                    self._build_library_cache(brain.library_zk)
                    cached_data = self.library_cache.get(
                        brain.library_zk, dict())
                    report_entry_ids = cached_data.get('report_entry_ids', [])
                    credit_log_data = cached_data.get('credit_log_data', [])
                if brain.getId in report_entry_ids:
                    cl_items = [x for x in credit_log_data
                        if x['report_entry_id'] == brain.getId]
                    # Existuje jen mvs_zk_ntk_todo_std pro tento report, proto
                    # tento záznam v tomto reportu neuvádět.
                    if len(cl_items) == 1:
                        logger.info('report {} for {} only mvs_zk_ntk_todo_std therefore skipped'.format(
                            brain.getId,
                            brain.hid,
                            ))
                        continue

            if self.only_mvs_zk_ntk_todo_std:
                # Operátor nemusí zadat siglu, nebo ji zadá fulltextem, takže
                # jsou různé ŽK.
                if not self.library:
                    self._build_library_cache(brain.library_zk)
                    cached_data = self.library_cache.get(
                        brain.library_zk, dict())
                    report_entry_ids = cached_data.get('report_entry_ids', [])
                    credit_log_data = cached_data.get('credit_log_data', [])
                if brain.getId in report_entry_ids:
                    cl_items = [x for x in credit_log_data
                        if x['report_entry_id'] == brain.getId]
                    # Existuje jen mvs_zk_ntk_todo_std pro tento report, proto
                    # tento záznam v tomto reportu uvádět.
                    # if len(cl_items) == 1:
                    #     pass
                    # elif len(cl_items) == 0:
                    #     continue
                    # else:
                    #     logger.info('report {} for {} multiple items therefore skipped'.format(
                    #         brain.getId,
                    #         brain.hid,
                    #         ))
                    #     continue
                    value = u'Související záznamy:'
                    display = u'Související záznamy:\n'
                    if len(cl_items):
                        display += u'<ul>\n'
                    for cl_item in cl_items:
                        value += u' {}: {} Kč ({})'.format(
                            cl_item.get('timestamp', u'?'),
                            cl_item.get('amount', u'?'),
                            cl_item.get('message', u''),
                            )
                        display += u'<li>{}: <strong>{} Kč</strong> ({})</li>\n'.format(
                            self.date_format_html(
                                cl_item.get('timestamp', None),
                                is_time=True,
                                ),
                            cl_item.get('amount', u'?'),
                            cl_item.get('message', u''),
                            )
                    if len(cl_items):
                        display += u'</ul>\n'
                    row[-1]['value'] += value
                    row[-1]['display'] = display
                else:
                    continue

            self.sum_fee += value_fee
            self.data.append(row)

    def _sort_data(self):
        """Setřídit výstup podle HID."""
        self.data = sorted(self.data, key=lambda item: item[0]['value'])

    def _export_xlsx(self):
        # prefix + date + hash - naoř. report-2017-11-15-(hash).xlsx
        date_str = DateTime().ISO()[:10]
        hash_str = str(uuid.uuid4()).replace('-', '')[:8]
        filename = 'report-{date}-{hash}.xlsx'.format(date=date_str, hash=hash_str)

        filepath = os.path.join(ZISKEJ_VAR_DOWNLOAD, filename)

        wb = Workbook()
        ws = wb.active
        ws.append(self.data_cols)
        row_nr = 1
        for row in self.data:
            row_nr += 1
            col_nr = 0
            for cell in row:
                col_nr += 1
                excel_cell = ws.cell(row=row_nr, column=col_nr, value=cell['value'])
                if 'number_format' in cell:
                    excel_cell.number_format = cell['number_format']

        # Přidat řádek se součtem, ale jen, pokud existuje nějaký řádek s daty
        if self.data:
            if self.col_idx is None:
                self.col_idx = 1
            excel_cell = ws.cell(row=row_nr+1, column=1, value="Celková cena:")
            excel_cell = ws.cell(row=row_nr+1, column=self.col_idx+1, value=self.sum_fee)

        wb.save(filepath)

        self.export_xlsx_url = self.portal_url + '/files_download/' + filename

    def call(self):
        super(InvoiceLibraryBaseView, self).call()
        if self._base():
            return
        date_from, date_to = self._dates()
        sigla = self._sigla()

        # Připravit data
        self.data = []

        # Hledat v jednotlivých iteracích
        for iteration in self.iterations():
            query, url_listing = self._build_query(
                date_from, date_to, sigla, iteration=iteration)
            brains = self._catalog_search(
                query, date_from=date_from, date_to=date_to)
            note = self.get_iteration_label(iteration)
            toggle_neg = self.get_iteration_toggle_neg(iteration)
            if note:
                note += u' '
            self._build_data(brains, url_listing, toggle_neg=toggle_neg, note=note)

        # Zpracovat připravená data
        if len(self.iterations()) > 1:
            self._sort_data()
        self.items_len = len(self.data)

        # Excel
        if self.request.get('export_xlsx', None):
            self._export_xlsx()

        # Přidat celkový součet i do zobrazované stránky
        sum_row = []
        sum_row.append(dict(
            value=u'Cena všech položek:',
            display=u'Cena všech položek:',
            ))
        if self.col_idx is None:
            self.col_idx = 1
        if self.col_idx > 1:
            for idx in range(self.col_idx - 1):
                sum_row.append(dict(
                    value=None,
                    display=u'',
                    ))
        sum_row.append(dict(
            value=self.sum_fee,
            display=u'<strong>{} Kč<strong>'.format(self.sum_fee),
            ))
        if self.data:
            while len(sum_row) < len(self.data[-1]):
                sum_row.append(dict(
                    value=None,
                    display=u'',
                    ))
        self.data.append(sum_row)


class InvoiceLibraryFromView(InvoiceLibraryBaseView):
    """Podklady pro fakturaci knihovny vůči provozovateli Získej.
    """

    template = ViewPageTemplateFile('invoice_library.pt')

    invoice_library_type = 'from'
    url_postfix = '/invoice_library_from'

    def build_entry_subtype_list(self, iteration=None):
        if not self.is_ntk:
            subtype_list = ['std-lib-szk-sdk', 'ret-lib-szk-sdk',
                            'std-lib-ntk-sdk', 'ret-lib-ntk-sdk',
                            'std-rdr-szk-sdk', 'ret-rdr-szk-sdk',
                            'std-rdr-ntk-sdk', 'ret-rdr-ntk-sdk', ]
        else:
            subtype_list = ['std-lib-szk-ntk', 'ret-lib-szk-ntk',
                            'std-rdr-szk-ntk', 'ret-rdr-szk-ntk', ]
        return subtype_list

    def call(self):
        super(InvoiceLibraryFromView, self).call()

        if self.is_ntk:
            self.title = u'Příjmy NTK jako DK (MVS)'
            self.description = u'Podklady pro vyúčtování NTK vůči SC ZÍSKEJ.'
        else:
            self.title = u'Knihovna fakturuje (MVS)'
            self.description = u'Podklady pro fakturaci knihovny vůči provozovateli ZÍSKEJ (NTK).'

        library = self.library
        self.data_desc = u''
        if library:
            if not self.is_ntk:
                self.data_desc += u'<strong>Odběratel:</strong> NTK jako provozovatel ZÍSKEJ'
                self.data_desc += u'<br>\n'
                self.data_desc += u'<strong>Dodavatel:</strong> {title} ({sigla}, IČ {ico})'.format(title=library.fullname, sigla=library.sigla, ico=library.ico)


class InvoiceLibraryToView(InvoiceLibraryBaseView):
    """Podklady pro fakturaci provozovatele Získej vůči knihovně.
    """

    template = ViewPageTemplateFile('invoice_library.pt')

    invoice_library_type = 'to'
    url_postfix = '/invoice_library_to'

    def build_entry_subtype_list(self, iteration=None):
        if not self.is_ntk:
            subtype_list = ['std-lib-szk-sdk', 'ret-lib-szk-sdk',
                            'std-lib-szk-ntk', 'ret-lib-szk-ntk', ]
        else:
            subtype_list = ['std-lib-ntk-sdk', 'ret-lib-ntk-sdk', ]
        return subtype_list

    def call(self):
        super(InvoiceLibraryToView, self).call()

        if self.is_ntk:
            self.title = u'Výdaje NTK jako ŽK (MVS)'
            self.description = u'Podklady pro vyúčtování SC ZÍSKEJ vůči NTK.'
        else:
            self.title = u'Knihovně fakturovat (MVS)'
            self.description = u'Podklady pro fakturaci provozovatele ZÍSKEJ (NTK) vůči knihovně.'

        library = self.library
        self.data_desc = u''
        if library:
            if not self.is_ntk:
                self.data_desc += u'<strong>Odběratel:</strong> {title} ({sigla}, IČ {ico})'.format(title=library.fullname, sigla=library.sigla, ico=library.ico)
                self.data_desc += u'<br>\n'
                self.data_desc += u'<strong>Dodavatel:</strong> NTK jako provozovatel ZÍSKEJ'


class EddLibraryFromView(InvoiceLibraryBaseView):
    """Podklady pro EDD pro knihovnu jako DK.
    """

    template = ViewPageTemplateFile('invoice_library.pt')

    invoice_library_type = 'from'
    url_postfix = '/edd_library_from'

    def build_entry_subtype_list(self, iteration=None):
        return self.entry_subtype

    def _get_add_entry_subtype_edd(self):
        return True
        # return bool(self.report_id in ('eddzkall', ))

    def call(self):
        self.ticket_type = 'edd'
        self.service_label = u'EDD'
        self.entry_subtype = None

        report_id = self.text_from_request('report', lov=('edddk', ))
        self.report_id = report_id

        if report_id == 'edddk':
            self.entry_subtype = 'edd-dk-std'

        super(EddLibraryFromView, self).call()

        if report_id == 'edddk':
            if self.is_ntk:
                self.title = u'Příjmy NTK jako DK (EDD)'
                self.description = u'Podklady pro vyúčtování NTK vůči SC ZÍSKEJ.'
            else:
                self.title = u'Knihovna fakturuje (EDD)'
                self.description = u'Podklady pro fakturaci knihovny vůči provozovateli ZÍSKEJ (NTK).'


        self.data_desc = u''


class EddLibraryToView(InvoiceLibraryBaseView):
    """Podklady pro EDD pro knihovnu jako ŽK.
    """

    template = ViewPageTemplateFile('invoice_library.pt')

    invoice_library_type = 'to'
    url_postfix = '/edd_library_to'

    def build_entry_subtype_list(self, iteration=None):
        return self.entry_subtype

    def _get_add_entry_subtype_edd(self):
        return True
        # return bool(self.report_id in ('eddzkall', ))

    def call(self):
        self.ticket_type = 'edd'
        self.service_label = u'EDD'
        self.entry_subtype = None

        report_id = self.text_from_request('report', lov=('eddzkdilia', 'eddzkall', ))
        self.report_id = report_id

        if report_id == 'eddzkdilia':
            self.entry_subtype = 'edd-dilia-std'
        elif report_id == 'eddzkall':
            self.entry_subtype = ['edd-dilia-std', 'edd-dk-std', ]

        super(EddLibraryToView, self).call()

        if report_id == 'eddzkdilia':
            if self.is_ziskej_operator:
                self.title = u'Poplatky Dílii (EDD)'
                self.description = u'Podklady pro report pro Dílii za celé Získej, knihovna jako ŽK (pro operátora SC).'
            else:
                self.title = u'Poplatky Dílii (EDD, jen přehled)'
                self.description = u'Informativní přehled poplatků Dílii, kdy knihovna byla jako ŽK.  Jen přehled, neplaťte!'
        elif report_id == 'eddzkall':
            self.title = u'Informativní přehled EDD poplatků zaplacených čtenáři ŽK'
            self.description = u'Přehled všech EDD poplatků, které zaplatili čtenáři knihovny v roli ŽK.  Pro každou objednávku je jak poplatek Dílii, tak poplatek DK, celkem tedy dva záznamy.  Pro přehled, kde bude jeden souhrnný záznam vč. identifikátoru platby zvolte report Platby čtenářů.'

        self.data_desc = u''


class InvoiceLibraryView(InvoiceLibraryBaseView):
    """Podklady pro fakturaci.
    """

    template = ViewPageTemplateFile('invoice_library.pt')

    url_postfix = '/invoice_library'
    report_types = ['ntkcredit', 'ziskejcredit', 'zk', 'ntkcredittodo', ]

    def _get_add_entry_subtype_lib_rdr(self):
        return bool(self.report_type == 'zk')

    def iterations(self):
        """Seznamu iterací.  Jedna iterace je jeden dotaz do catalogu, pro každý
        může být sigla restrikcí ŽK nebo DK, viz get_invoice_library_type.
        """

        if self.report_type == 'ntkcredit':
            return [None, ]
        elif self.report_type == 'ziskejcredit':
            return ['std/ret dk', 'ret zk']
        elif self.report_type == 'zk':
            return [None, ]
        elif self.report_type == 'ntkcredittodo':
            return [None, ]
        logger.warning('Invalid value in iterations')
        raise Exception('Chyba v konfiguraci')

    def get_iteration_label(self, iteration):
        if iteration is None:
            return None
        LABELS = {'std/ret dk': u'DK', 'ret zk': u'ŽK'}
        return LABELS.get(iteration, iteration)

    def get_iteration_toggle_neg(self, iteration=None):
        LABELS = {'std/ret dk': False, 'ret zk': True}
        if iteration is None or iteration not in LABELS:
            return False
        return LABELS.get(iteration)

    def get_invoice_library_type(self, iteration=None):
        """ŽK/DK pro jednotlivé iterace."""
        value = None
        if self.report_type == 'ntkcredit':
            value = 'to'
        elif self.report_type == 'ziskejcredit':
            if iteration == 'std/ret dk':
                value = 'from'
            elif iteration == 'ret zk':
                value = 'to'
        elif self.report_type == 'zk':
            value = 'to'
        elif self.report_type == 'ntkcredittodo':
            value = 'to'
        if not value:
            logger.warning('Invalid value in get_invoice_library_type')
            raise Exception('Chyba v konfiguraci')
        return value

    def build_entry_subtype_list(self, iteration=None):
        value = None
        if self.report_type == 'ntkcredit':
            if not self.is_ntk:
                value = ['std-lib-szk-sdk', 'std-lib-szk-ntk', ]
            else:  # nemělo by nastat
                value = ['std-lib-szk-sdk', ]
        elif self.report_type == 'ziskejcredit':
            if iteration == 'std/ret dk':
                if not self.is_ntk:
                    value = ['std-lib-szk-sdk', 'ret-lib-szk-sdk',
                             'std-lib-ntk-sdk', 'ret-lib-ntk-sdk',
                             'std-rdr-szk-sdk', 'ret-rdr-szk-sdk',
                             'std-rdr-ntk-sdk', 'ret-rdr-ntk-sdk', ]
                else:  # nemělo by nastat
                    value = ['std-lib-szk-ntk', 'ret-lib-szk-ntk',
                             'std-rdr-szk-ntk', 'ret-rdr-szk-ntk', ]
            elif iteration == 'ret zk':
                if not self.is_ntk:
                    value = ['ret-lib-szk-sdk', 'ret-lib-szk-ntk', ]
                else:  # nemělo by nastat
                    value = ['ret-lib-ntk-sdk', ]
        elif self.report_type == 'zk':
            if self.is_all:
                # To se efektivně rovná všem variantám, tedy jakoby tento filtr
                # nebyl vůbec.
                value = ['std-lib-szk-sdk', 'ret-lib-szk-sdk',
                         'std-lib-ntk-sdk', 'ret-lib-ntk-sdk',
                         'std-lib-szk-ntk', 'ret-lib-szk-ntk',
                         'std-rdr-szk-sdk', 'ret-rdr-szk-sdk',
                         'std-rdr-ntk-sdk', 'ret-rdr-ntk-sdk',
                         'std-rdr-szk-ntk', 'ret-rdr-szk-ntk', ]
            elif not self.is_ntk:
                value = ['std-lib-szk-sdk', 'ret-lib-szk-sdk',
                         'std-lib-szk-ntk', 'ret-lib-szk-ntk',
                         'std-rdr-szk-sdk', 'ret-rdr-szk-sdk',
                         'std-rdr-szk-ntk', 'ret-rdr-szk-ntk', ]
            else:
                value = ['std-lib-ntk-sdk', 'ret-lib-ntk-sdk',
                         'std-rdr-ntk-sdk', 'ret-rdr-ntk-sdk', ]
        elif self.report_type == 'ntkcredittodo':
            if not self.is_ntk:
                value = ['std-lib-szk-sdk', 'std-lib-szk-ntk', ]
            else:  # nemělo by nastat
                value = ['std-lib-szk-sdk', ]
        if not value:
            logger.warning('Invalid value in build_entry_subtype_list')
            raise Exception('Chyba v konfiguraci')
        return value

    def call(self):
        self.report_type = self.text_from_request('report', lov=self.report_types)
        if not self.report_type:
            self.add_message(u'Nebyl zvolen validní report.')
            self.redirect(self.context_url)
            return

        # V reportu Přehled čerpání z FK nezapočítávat, co nebylo strženo.
        self.skip_mvs_zk_ntk_todo_std = bool(self.report_type == 'ntkcredit')
        self.only_mvs_zk_ntk_todo_std = bool(
            self.report_type == 'ntkcredittodo')

        super(InvoiceLibraryView, self).call()

        if self.is_ntk and self.report_type in (
                'ntkcredit', 'ziskejcredit', 'ntkcredittodo'):
            self.add_message(u'Tento report není pro NTK (ABA013) definován.')
            self.redirect(self.reports_url)
            return

        if self.report_type == 'ntkcredit':
            # MVS 2.
            self.title = u'Knihovně fakturovat (MVS)'
            self.description = u'Podklady pro fakturaci NTK vůči knihovně, přehled čerpání z finančního konta v NTK.'
        elif self.report_type == 'ziskejcredit':
            self.title = u'Knihovna fakturuje (MVS)'
            self.description = u'Podklady pro fakturaci knihovny vůči NTK, přehled pohybů na kontě v ZÍSKEJ.'
        elif self.report_type == 'zk':
            self.title = u'Přehled MVS objednávek souhrně (samoobslužně i asistovaně)'
            self.description = u'Souhrn objednávek, nezávisle na tom, zda bylo čerpáno z finančních kont nebo zaplaceno čtenářem.'
        elif self.report_type == 'ntkcredittodo':
            self.title = u'Nestrženo z konta v NTK (MVS)'
            self.description = u'Přehled neuskutečněných čerpání z finančního konta v NTK.'

        library = self.library
        self.data_desc = u''
        if library and self.report_type in ('ntkcredit', 'ntkcredittodo', ):
            self.data_desc += u'<strong>Odběratel:</strong> {title} ({sigla}, IČ {ico})'.format(title=library.fullname, sigla=library.sigla, ico=library.ico)
            self.data_desc += u'<br>\n'
            self.data_desc += u'<strong>Dodavatel:</strong> NTK jako provozovatel ZÍSKEJ'
        elif library and self.report_type == 'ziskejcredit':
            self.data_desc += u'<strong>Odběratel:</strong> NTK jako provozovatel ZÍSKEJ'
            self.data_desc += u'<br>\n'
            self.data_desc += u'<strong>Dodavatel:</strong> {title} ({sigla}, IČ {ico})'.format(title=library.fullname, sigla=library.sigla, ico=library.ico)


class InvoiceReaderView(InvoiceLibraryBaseView):
    """Přehled plateb čtenářů.
    """

    template = ViewPageTemplateFile('invoice_library.pt')

    url_postfix = '/invoice_reader'

    def _base_submit_additional(self):
        self.data_cols = [u'ID', u'Typ', u'ŽK', u'DK', u'Cena', u'Datum',
                          u'Poznámky', ]

    def _build_query(self, date_from, date_to, sigla, iteration=None):
        ticket_type = None
        try:
            if self.query_ticket_type == 'mvs':
                ticket_type = u'mvs'
            elif self.query_ticket_type == 'edd':
                ticket_type = u'edd'
        except:
            logger.exception('Undefined self.query_ticket_type')
        query = dict(
            portal_type = TICKET_PORTAL_TYPES,
            # payment_paid_date = dict(
            #     query = (
            #         DateTime(date_from + ' 0:00:00'),
            #         DateTime(date_to + ' 23:59:59'),
            #         ),
            #     range = 'min:max',
            #     ),
            # sort_on = 'payment_paid_date',
            sort_on = 'hid',
            )
        if ticket_type:
            query['ticket_type'] = ticket_type

        if sigla:
            query['library_zk'] = sigla
        url_listing = u'/tickets/listing_zk'
        return query, url_listing

    def _catalog_search(self, query, date_from=None, date_to=None):
        """Vrátit brains pro query, bypass security."""
        # https://docs.plone.org/develop/plone/searching_and_indexing/query.html#bypassing-query-security-check
        #brains = self.catalog(query)
        brains = self.catalog.unrestrictedSearchResults(query)
        items = []
        for brain in brains:
            obj = brain._unrestrictedGetObject()
            payment_paid_date = obj.payment_paid_date
            if not payment_paid_date:
                continue
            payment_paid_date_iso_day = payment_paid_date.ISO()[:10]
            if payment_paid_date_iso_day < date_from or payment_paid_date_iso_day > date_to:
                continue
            item = dict(
                brain = brain,
                payment_paid_date_iso_day = payment_paid_date_iso_day,
                payment_amount = obj.payment_amount,
                reader_fullname = obj.reader_fullname,
                payment_id = obj.payment_id,
                payment_platebator_uuid = obj.payment_platebator_uuid,
                )
            items.append(item)
        logger.info('catalog ({}/{}) for {}'.format(len(items), len(brains), query))
        return items

    def _build_data(self, items, url_listing, toggle_neg=False, note=None):
        """Zpracovat brains a zpracované přidat do self.data"""
        for item in items:
            brain = item['brain']

            row = []

            # obj is not needed now - in case we need it do not use standard
            # way brain.getObject() because it wont work, use this one
            # instead:
            # obj = brain._unrestrictedGetObject()

            value = brain.hid
            # localhost:14180/ziskej/tickets/listing_zk?search_id=100268&submit_search=1
            url = u'{}{}?search_id={}&submit_search=1'.format(
                self.portal_url, url_listing, value)
            row.append(
                dict(
                    value = value,
                    display = u'<a href="{}">{}</a>'.format(url, value),
                    )
                )

            value = brain['ticket_type']
            if not value:
                value = u'mvs'
            value = SERVICE_SHORTNAMES.get(value, value)
            row.append(
                dict(
                    value = value,
                    display = value,
                    )
                )

            value = brain.library_zk
            if value:
                value = value.upper()
            row.append(
                dict(
                    value = value,
                    display = value,
                    )
                )

            value = brain.library_dk
            if value:
                value = value.upper()
            row.append(
                dict(
                    value = value,
                    display = value,
                    )
                )

            value = item['payment_amount']
            if value is not None:
                if toggle_neg:
                    value = -value
                value_fee = value
                display = u'{} Kč'.format(value)
            else:
                display = u''
            if self.col_idx is None:
                self.col_idx = len(row)
            row.append(
                dict(
                    value = value,
                    display = display,
                    )
                )

            value = item['payment_paid_date_iso_day']
            dt = DateTime(value)
            row.append(
                dict(
                    value = self.date_format(dt, is_time=False, is_html=False),
                    display = self.date_format(dt, is_time=False, is_html=True),
                    number_format = 'd.m.yyyy h:mm;@',
                    )
                )

            value = u''
            if note:
                value += note
            if brain.reader:
                value += u'Samoobslužně'
            else:
                value += u'Asistovaně'
            if item['reader_fullname']:
                value += u', čtenář {}'.format(item['reader_fullname'])
            value += u', id platby {}'.format(item['payment_id'])
            if ADD_PLATEBATOR_UID:
                value += u', id platby v Platebátoru {}'.format(item['payment_platebator_uuid'])

            row.append(
                dict(
                    value = value,
                    display = value,
                    )
                )

            self.sum_fee += value_fee
            self.data.append(row)

    def call(self):
        service_lov = ['', 'mvs', 'edd']
        self.query_ticket_type = self.text_from_request('service', lov=service_lov)
        print('self.query_ticket_type:', self.query_ticket_type)

        super(InvoiceReaderView, self).call()

        if self.query_ticket_type == 'mvs':
            self.title = u'Platby čtenářů (MVS)'
            self.description = u'Souhrn objednávek, které byly zaplaceny přímo čtenářem.  Pro MVS se jedná o objednávky vytvořené čtenářem (samoobslužně).'
        elif self.query_ticket_type == 'edd':
            self.title = u'Platby čtenářů (EDD)'
            self.description = u'Souhrn objednávek, které byly zaplaceny přímo čtenářem.  Pro EDD se jedná o všechny objednávky.'
        else:
            self.title = u'Platby čtenářů'
            self.description = u'Souhrn objednávek, které byly zaplaceny přímo čtenářem.  Pro MVS se jedná o objednávky vytvořené čtenářem (samoobslužně), pro EDD o všechny objednávky.'


class LibraryStatsView(ReportBaseView):
    """Statistiky knihovny.  V případě operátora volí knihovnu.
    """

    template = ViewPageTemplateFile('library_stats.pt')

    url_postfix = '/library_stats'

    def _build_query(self, query_type, sigla, date_from, date_to):
        """Pro statistiky je potřeba použít datum použít pro datum vytvoření,
        aby zahrnulo i tickety/subtickety bez report_date, protože ještě nebo
        nikdy nebyly odeslány.
        """

        if query_type.startswith('t_'):
            portal_type = TICKET_PORTAL_TYPES
        elif query_type.startswith('st_'):
            portal_type = SUBTICKET_PORTAL_TYPES
        else:
            logger.error('Unknown query_type {}'.format(query_type))
            raise Exception('Internal error')

        query = dict(
            portal_type = portal_type,
            ticket_type = self.ticket_type,
            created = dict(
                query = (
                    DateTime(date_from + ' 0:00:00'),
                    DateTime(date_to + ' 23:59:59'),
                    ),
                range = 'min:max',
                ),
            sort_on = 'created',
            #sort_order = 'reverse',
            )

        if sigla:
            if query_type.endswith('_zk'):
                query['library_zk'] = sigla
            elif query_type.endswith('_dk'):
                query['library_dk'] = sigla
            else:
                logger.error('Unknown query_type {}'.format(query_type))
                raise Exception('Internal error')

        return query

    def _count_tickets(self, sigla, date_from, date_to):
        """Objednávky

        Položky Počet MVS objednávek..., ne Počet samoobslužných MVS
        objednávek...".  Nehraje roli, jak byla objednávka vytvořena.

        Pravidla - první úravidlo ukončí a další už nenastanou, přičemž každá
        objednávka se projeví v Počet MVS objednávek (vždy jako ŽK).

        - Objednávky před schválením a vytvořením požadavku nebo zamítnutím
          nebo stornováním se nezapočítají nikde jinde.

        - Pokud je objednávka ve stavu stornována z důvodu úspěšné reklamace,
          je v reklamovaných.

        - Pokud je objednávka ve stavu stornována (tzn. z jiného důvodu), je
          ve stornovaných.

        - Pokud objednávku ŽK zamítla, je v zamítnutých.

        - Pokud je objednávka v přípravě, neprojeví se nikde jinde.

        - Pokud ŽK připravila pro čtenáře, je v připravených pro čtenáře.

        - Nevyzvednutí, nevrácení, vrácení atp. se nikde nezobrazí.

        Samoobslužné objednávky 

        Položky Počet samoobslužných MVS objednávek...".

        Pouze vytvořené čtenářem prostřednictvím CPK UI.

        Pravidla - první úravidlo ukončí a další už nenastanou, přičemž každá
        objednávka se projeví v Počet samoobslužných MVS objednávek.

        - Pokud je objednávka ve stavu stornována (z jakéhokoli důvodu), je ve
          stornovaných.

        - Pokud je objednávka ve stavu zamítnuta, je ve zamítnutých.

        - Pokud je objednávka ve stavu nová, není nikde jinde.

        - Všechny ostatní jsou v přijatých.

        Původní zadání:

        MVS ŽK:
        - odeslané
        - zamítnuté: dostupné ve fondu / chybná citace / nelze realizovat
        - stornované
        - reklamované

        Počet samoobslužných objednávek 
        - přijaté
        - zamítnuté: dostupné ve fondu / chybná citace /nelze realizovat
        - stornované
        """

        # ticket stavy
        # 'created', 'assigned', 'pending', 'prepared', 'closed', 'cancelled',
        # 'rejected'

        counter = dict(
            zk = dict(
                total = 0,
                success = 0,
                created = 0,
                assigned = 0,
                pending = 0,
                prepared = 0,  # nepoužito
                closed = 0,  # nepoužito
                rejected = 0,
                cancelled = 0,
                complained = 0,
                reject_reason = dict(),
                ),
            cpkreader = dict(
                total = 0,
                created = 0,
                accepted = 0,
                rejected = 0,
                cancelled = 0,
                ),
            )

        query = self._build_query('t_zk', sigla, date_from, date_to)
        brains = self._catalog_search(query)

        for brain in brains:
            ticket = brain.getObject()
            wf_status = ticket.wf_status
            review_state = wf_status['review_state']
            counter['zk']['total'] += 1

            if ticket.is_created_by_cpk_for_reader():
                counter['cpkreader']['total'] += 1

                if review_state == 'created':
                    counter['cpkreader']['created'] += 1
                elif review_state == 'cancelled':
                    counter['cpkreader']['cancelled'] += 1
                elif review_state == 'rejected':
                    counter['cpkreader']['rejected'] += 1
                else:
                    counter['cpkreader']['accepted'] += 1

            if review_state in ('created', 'assigned', 'pending', ):
                counter['zk'][review_state] += 1
                continue

            if review_state == 'cancelled':
                if ticket.is_complaint:
                    counter['zk']['complained'] += 1
                    continue
                else:
                    counter['zk']['cancelled'] += 1
                    continue

            if review_state == 'rejected':
                counter['zk']['rejected'] += 1
                reason = ticket.reject_reason
                if reason not in counter['zk']['reject_reason']:
                    counter['zk']['reject_reason'][reason] = 0
                counter['zk']['reject_reason'][reason] += 1
                continue

            if review_state == 'rejected':
                counter['zk']['rejected'] += 1
                continue

            # Vše ostatní je v připravených
            # 'prepared', 'closed',
            counter['zk'][review_state] += 1
            counter['zk']['success'] += 1

        if DEBUG:
            pprint(counter)
        return counter

    def _top3_dk_as_mvszk(self, sigla_zk, date_from, date_to):
        """MVS ŽK: nejčastější DK, první tři."""
        # if sigla_zk:
        #     sigla_zk = sigla_zk.lower()
        query = self._build_query('st_zk', sigla_zk, date_from, date_to)
        brains = self._catalog_search(query)
        counter_zk = len(brains)

        top_dk = dict()
        for brain in brains:
            #subticket = brain.getObject()
            #sigla = subticket.library_dk
            sigla = brain.library_dk
            if sigla not in top_dk:
                top_dk[sigla] = dict(
                    sigla = sigla,
                    counter = 0,
                    )
            top_dk[sigla]['counter'] += 1

        top3 = [0, 0, 0, ]
        min_top3 = min(top3)
        top3_sigla = [None, None, None, ]
        for sigla in top_dk:
            value = top_dk[sigla]['counter']
            if value <= min_top3:
                continue
            for idx in range(3):
                if top3[idx] > min_top3:
                    continue
                top3[idx] = value
                top3_sigla[idx] = sigla
                min_top3 = min(top3)
                break
        top3_sorted = [0, 0, 0, ]
        top3_sigla_sorted = [None, None, None, ]
        for idx in range(3):
            if top3_sigla[idx] is None:
                continue
            if top3[idx] == max(top3):
                top3_sorted[0] = top3[idx]
                top3_sigla_sorted[0] = top3_sigla[idx]
            elif top3[idx] == min(top3):
                top3_sorted[2] = top3[idx]
                top3_sigla_sorted[2] = top3_sigla[idx]
            else:
                top3_sorted[1] = top3[idx]
                top3_sigla_sorted[1] = top3_sigla[idx]
        top3_sigla_sorted = [x for x in top3_sigla_sorted if x is not None]
        top3_data = []
        for sigla in top3_sigla_sorted:
            library = getattr(self.portal.libraries, sigla, None)
            if library:
                library_title = u'{}'.format(library.fullname)
            else:
                library_title = u'{}'.format(sigla)
            top3_data.append(dict(
                sigla = sigla,
                counter = top_dk[sigla]['counter'],
                title = library_title,
                ))

        if DEBUG:
            print("top3_data dk", top3_data)
        return top3_data, counter_zk

    def _count_subtickets_dk(self, sigla_dk, date_from, date_to):
        """Požadavky jako DK

        Položky “Počet MVS požadavků jako DK”.

        Pravidla - první úravidlo ukončí a další už nenastanou, přičemž každý požadavek se projeví v “Počet MVS požadavků jako DK”.

        - Ve frontě se neprojeví nikde jinde.

        - Pokud je požadavek ve stavu stornován z důvodu úspěšné reklamace, je
          v “reklamovaných”.  A není už ani v přijatých ani nikde jinde.

        - Pokud je požadavek ve stavu stornován z důvodu odebrání SC, je
          v “odebraných SC”.

        - Pokud je požadavek ve stavu stornován (tzn. z jiného důvodu), je ve
          “stornovaných”.  A není už ani v přijatých ani nikde jinde.

        - Pokud DK podmíněně přijala, je v “podmíněně přijatých”.  A to
          i v případě, že následně jsou podmínky nepřijaty ŽK a tím je
          požadavek odmítnut nebo naopak přijaty a tím je přijmut.

        - Pokud DK jako první reakci odmítla, je v “odmítnutých”.

        - Pokud DK jako první reakci přijala, je v “přijatých”.  A to i kdyby
          následně odmítla.

        Původní zadání:

        Počet MVS jako DK
        - přijaté
        - podmíněně přijaté
        - odmítnuté: chybná citace / nelze realizovat / momentálně nedostupné
        - stornované
        - reklamované
        - nejčastější ŽK - 1.-3.
        """

        # subticket stavy:
        # 'queued', 'conditionally_accepted', 'accepted', 'sent', 'sent_back',
        # 'closed', 'cancelled', 'refused'

        counter = dict(
            dk = dict(
                total = 0,
                success_cond = 0,
                success_not_cond = 0,
                created = 0,
                accepted = 0,
                conditionally_accepted = 0,
                sent = 0,  # nepoužito
                sent_back = 0,  # nepoužito
                closed = 0,  # nepoužito
                refused = 0,
                cancelled = 0,
                complained = 0,
                cancelled_sc = 0,
                refuse_reason = dict(),
                ),
            )
        top_zk = dict()

        # if sigla_dk:
        #     sigla_dk = sigla_dk.lower()
        query = self._build_query('st_dk', sigla_dk, date_from, date_to)
        brains = self._catalog_search(query)

        for brain in brains:
            subticket = brain.getObject()
            wf_status = subticket.wf_status
            review_state = wf_status['review_state']
            counter['dk']['total'] += 1

            sigla = subticket.library_zk
            if sigla not in top_zk:
                top_zk[sigla] = dict(
                    sigla = sigla,
                    counter = 0,
                    )
            top_zk[sigla]['counter'] += 1

            if review_state == 'queued':
                counter['dk']['created'] += 1
                continue

            if review_state in ('conditionally_accepted', 'accepted', ):
                counter['dk'][review_state] += 1
                continue

            # Stav cancelled mohl nastat jedním ze tří důvodů, ty je nutné
            # odlišit.
            if review_state == 'cancelled':
                # 1) Uznaná reklamace.
                if subticket.is_complaint:
                    counter['dk']['complained'] += 1
                    continue
                # 2) Odebráno SC.
                if subticket.is_cancelled_by_sc():
                    counter['dk']['cancelled_sc'] += 1
                    continue
                # 3) Storno nebo přijatá žádost o storno.
                counter['dk']['cancelled'] += 1
                continue

            if review_state == 'refused':
                counter['dk']['refused'] += 1
                reason = subticket.refuse_reason
                if reason not in counter['dk']['refuse_reason']:
                    counter['dk']['refuse_reason'][reason] = 0
                counter['dk']['refuse_reason'][reason] += 1
                continue

            if subticket.is_conditionally_accepted_by_dk():
                counter['dk']['success_cond'] += 1
            else:
                counter['dk']['success_not_cond'] += 1
            counter['dk'][review_state] += 1

        top3 = [0, 0, 0, ]
        min_top3 = min(top3)
        top3_sigla = [None, None, None, ]
        for sigla in top_zk:
            value = top_zk[sigla]['counter']
            if value <= min_top3:
                continue
            for idx in range(3):
                if top3[idx] > min_top3:
                    continue
                top3[idx] = value
                top3_sigla[idx] = sigla
                min_top3 = min(top3)
                break
        top3_sorted = [0, 0, 0, ]
        top3_sigla_sorted = [None, None, None, ]
        for idx in range(3):
            if top3_sigla[idx] is None:
                continue
            if top3[idx] == max(top3):
                top3_sorted[0] = top3[idx]
                top3_sigla_sorted[0] = top3_sigla[idx]
            elif top3[idx] == min(top3):
                top3_sorted[2] = top3[idx]
                top3_sigla_sorted[2] = top3_sigla[idx]
            else:
                top3_sorted[1] = top3[idx]
                top3_sigla_sorted[1] = top3_sigla[idx]
        top3_sigla_sorted = [x for x in top3_sigla_sorted if x is not None]
        top3_data = []
        for sigla in top3_sigla_sorted:
            library = self.library_by_sigla(sigla)
            if library:
                library_title = u'{}'.format(library.fullname)
            else:
                library_title = u'{}'.format(sigla)
            top3_data.append(dict(
                sigla = sigla,
                counter = top_zk[sigla]['counter'],
                title = library_title,
                ))

        counter['top3'] = top3_data

        if DEBUG:
            pprint(counter)
        return counter

    def call(self):
        super(LibraryStatsView, self).call()

        self.context_url_view = self.context_url + self.url_postfix

        if not (self.is_librarian or self.is_ziskej_operator):
            self.redirect(self.portal_url)
            return
        self.unauthorized = False

        self.date_from = None
        self.date_to = None
        self.sigla = None
        self.sigla_upper = u''
        self.export_xlsx = False
        self.export_xlsx_url = None
        self.library = None

        self.date_from, self.date_to = datetime_last_month_iso()
        self.service = self.text_from_request(
            'service', lov=('mvszk', 'mvsdk', 'eddzk', 'edddk', ))

        self.submit = self.request.get('submit', None)
        if not self.submit:
            return

        date_from, date_to = self._dates()
        sigla = self._sigla()

        export_xlsx = self.request.get('export_xlsx', None)

        self.data_cols = [u' ', u'Počet']
        self.data = []

        base_query = dict(
            portal_type = SUBTICKET_PORTAL_TYPES,
            report_date = dict(
                query = (
                    DateTime(date_from + ' 0:00:00'),
                    DateTime(date_to + ' 23:59:59'),
                    ),
                range = 'min:max',
                ),
            )

        if self.service == 'mvszk':
            self.ticket_type = 'mvs'
            base_query['ticket_type'] = self.ticket_type
            stats = self._stat_zk(
                'mvs', sigla, base_query, date_from, date_to)
        elif self.service == 'mvsdk':
            self.ticket_type = 'mvs'
            base_query['ticket_type'] = self.ticket_type
            stats = self._stat_dk(
                'mvs', sigla, base_query, date_from, date_to)
        elif self.service == 'eddzk':
            self.ticket_type = 'edd'
            base_query['ticket_type'] = self.ticket_type
            stats = self._stat_zk(
                'edd', sigla, base_query, date_from, date_to)
        elif self.service == 'edddk':
            self.ticket_type = 'edd'
            base_query['ticket_type'] = self.ticket_type
            stats = self._stat_dk(
                'edd', sigla, base_query, date_from, date_to)
        else:
            self.redirect(self.context_url_view)
            return

        # data
        for stat in stats:
            row = []

            value = stat['label']
            display = stat.get('label_display', value)
            row.append(
                dict(
                    value = value,
                    display = display,
                    )
                )

            value = stat['value']
            display = stat.get('value_display', value)
            row.append(
                dict(
                    value = value,
                    display = display,
                    )
                )

            self.data.append(row)

        self.items_len = len(self.data)

        # Excel
        if export_xlsx:

            # SUGGESTION date + hash report-2017-11-15-(hash).xlsx
            date_str = DateTime().ISO()[:10]
            hash_str = str(uuid.uuid4()).replace('-', '')[:8]
            filename = 'stats-{date}-{hash}.xlsx'.format(date=date_str, hash=hash_str)

            filepath = os.path.join(ZISKEJ_VAR_DOWNLOAD, filename)

            wb = Workbook()
            ws = wb.active
            ws.append(self.data_cols)
            row_nr = 1
            for row in self.data:
                row_nr += 1
                col_nr = 0
                for cell in row:
                    col_nr += 1
                    excel_cell = ws.cell(row=row_nr, column=col_nr, value=cell['value'])
                    if 'number_format' in cell:
                        excel_cell.number_format = cell['number_format']
            wb.save(filepath)

            self.export_xlsx_url = self.portal_url + '/files_download/' + filename

    def _stat_zk(self, service, sigla, base_query, date_from, date_to):

        if service == 'mvs':
            service_label = u'MVS'
        elif service == 'edd':
            service_label = u'EDD'
        else:
            raise Exception('Internal error')

        stats = []

        self.title = u'Počet {} objednávek (vytvořené knihovníkem i čtenářem)'.format(service_label)

        # stat_zk
        query = dict(base_query)
        if sigla:
            query['library_zk'] = sigla

        # Počet MVS jako ŽK
        # - nejčastější DK - 1.-3.

        # _count_tickets použije _build_query, to použije self.ticket_type
        counter_zk = self._count_tickets(sigla, date_from, date_to)

        # MVS ŽK:
        stats.append(
            dict(
                id = 'stat_tickets_all',
                label = u'Počet {} objednávek (vždy jako ŽK)'.format(service_label),
                value = counter_zk['zk']['total'],
                )
            )

        value = counter_zk['zk']['created'] + counter_zk['zk']['assigned'] + \
            counter_zk['zk']['pending']
        stats.append(
            dict(
                id = 'stat_tickets_created',
                label = u'- nové a ve zpracování',
                value = value,
                )
            )

        # kladně vyřízené (někdy připravené pro čtenáře, nyní připravené pro
        # čtenáře, půjčené čtenáři, vrácené čtenářem, vrácené zpět DK, uzavřené
        # a reklamované) - změnit a vynechat reklamované
        stats.append(
            dict(
                id = 'stat_tickets_success',
                label = u'- kladně vyřízené (tedy připravené pro čtenáře)',
                value = counter_zk['zk']['success'],
                )
            )

        # - zamítnuté
        #   dostupné ve fondu / chybná citace / nelze realizovat
        stats.append(
            dict(
                id = 'stat_tickets_rejected',
                label = u'- zamítnuté',
                value = counter_zk['zk']['rejected'],
                )
            )

        reason_dict = {
            'reject-reason-zk-available': u'dostupné ve fondu',
            'reject-reason-zk-impossible': u'chybná citace a nepodaří se dohledat správně',
            'reject-reason-zk-unavailable': u'nelze realizovat',
            'reject-reason-r-denied': u'neověření čtenáře',
            }
        reason_list = (
            'reject-reason-zk-available',
            'reject-reason-zk-impossible',
            'reject-reason-zk-unavailable',
            'reject-reason-r-denied',
            )
        for reason in reason_list:
            stats.append(
                dict(
                    id = 'stat_tickets_rejected',
                    label = u'    z toho zamítnuté z důvodu: {}'.format(reason_dict[reason]),
                    label_display = u'&nbsp; &nbsp; &nbsp; z toho zamítnuté z důvodu: {}'.format(reason_dict[reason]),
                    value = counter_zk['zk']['reject_reason'].get(reason, 0),
                    )
                )

        # - stornované
        stats.append(
            dict(
                id = 'stat_tickets_cancelled',
                label = u'- stornované',
                value = counter_zk['zk']['cancelled'],
                )
            )

        # - reklamované
        stats.append(
            dict(
                id = 'stat_tickets_complained',
                label = u'- reklamované',
                value = counter_zk['zk']['complained'],
                )
            )

        stats.append(
            dict(
                id = 'sep',
                label = u'',
                value = None,
                )
            )

        # Počet samoobslužných objednávek
        stats.append(
            dict(
                id = 'stat_tickets_cpkreader_all',
                label = u'Počet samoobslužných {} objednávek'.format(service_label),
                value = counter_zk['cpkreader']['total'],
                )
            )

        stats.append(
            dict(
                id = 'stat_tickets_cpkreader_accepted',
                label = u'- zatím jen vytvořené',
                value = counter_zk['cpkreader']['created'],
                )
            )

        # - přijaté
        stats.append(
            dict(
                id = 'stat_tickets_cpkreader_accepted',
                label = u'- přijaté',
                value = counter_zk['cpkreader']['accepted'],
                )
            )

        # - zamítnuté
        #   dostupné ve fondu / chybná citace /nelze realizovat
        stats.append(
            dict(
                id = 'stat_tickets_cpkreader_rejected',
                label = u'- zamítnuté',
                value = counter_zk['cpkreader']['rejected'],
                )
            )

        # - stornované
        stats.append(
            dict(
                id = 'stat_tickets_cpkreader_cancelled',
                label = u'- stornované',
                value = counter_zk['cpkreader']['cancelled'],
                )
            )

        stats.append(
            dict(
                id = 'sep',
                label = u'',
                value = None,
                )
            )

        ticket_len = len(stats)

        brains = self.catalog(query)
        stat_zk = len(brains)

        top3_dk_as_mvszk, counter_zk = self._top3_dk_as_mvszk(
            sigla, date_from, date_to)

        stats.append(
            dict(
                id = 'stat_subtickets_zk',
                label = u'Počet {} požadavků zaslaných do DK'.format(service_label),
                value = counter_zk,
                )
            )

        stats.append(
            dict(
                id = 'sep',
                label = u'',
                value = None,
                )
            )

        stats.append(
            dict(
                id = 'label',
                label = u'Nejčastější dožádané knihovny:',
                value = None,
                )
            )

        idx = 0
        for top_item in top3_dk_as_mvszk:
            idx += 1
            label = u'{}. {} ({})'.format(
                idx,
                top_item['title'],
                top_item['sigla'].upper(),
                )
            stats.append(
                dict(
                    id = 'stat_subtickets_top_dk',
                    label = label,
                    value = top_item['counter'],
                    )
                )

        return stats

    def _stat_dk(self, service, sigla, base_query, date_from, date_to):

        if service == 'mvs':
            service_label = u'MVS'
        elif service == 'edd':
            service_label = u'EDD'
        else:
            raise Exception('Internal error')

        stats = []

        self.title = u'Počet {} požadavků jako DK'.format(service_label)

        # stat_dk
        query = dict(base_query)
        if sigla:
            query['library_dk'] = sigla

        brains = self.catalog(query)

        stat_dk = len(brains)

        # _count_subtickets_dk použije _build_query, to použije self.ticket_type
        counter_dk = self._count_subtickets_dk(sigla, date_from, date_to)

        # Počet MVS jako DK
        stats.append(
            dict(
                id = 'stat_subtickets_all',
                label = u'Počet {} požadavků jako DK'.format(service_label),
                value = counter_dk['dk']['total'],
                )
            )

        # 'queued', 'conditionally_accepted', 'accepted', 'sent',
        # 'sent_back', 'closed', 'cancelled', 'refused'

        value = counter_dk['dk']['created'] + counter_dk['dk']['accepted'] + \
            counter_dk['dk']['conditionally_accepted']
        stats.append(
            dict(
                id = 'stat_subtickets_created',
                label = u'- nové a ve zpracování',
                value = value,
                )
            )

        stats.append(
            dict(
                id = 'stat_subtickets_conditionally_accepted',
                label = u'- podmíněně přijaté a odeslané do ŽK',
                value = counter_dk['dk']['success_cond'],
                )
            )

        # - přijaté
        stats.append(
            dict(
                id = 'stat_subtickets_accepted',
                label = u'- přijaté a odeslané do ŽK',
                value = counter_dk['dk']['success_not_cond'],
                )
            )

        # - odmítnuté
        #   chybná citace / nelze realizovat / momentálně nedostupné
        stats.append(
            dict(
                id = 'stat_subtickets_refused',
                label = u'- odmítnuté',
                value = counter_dk['dk']['refused'],
                )
            )

        reason_dict = {
            'refuse-reason-wrong-citation': u'chybná citace a nepodaří se dohledat správně',
            'refuse-reason-dk-impossible': u'nelze realizovat',
            'refuse-reason-dk-unavailable': u'momentálně nedostupné',
            }
        reason_list = (
            'refuse-reason-wrong-citation',
            'refuse-reason-dk-impossible',
            'refuse-reason-dk-unavailable',
            )
        for reason in reason_list:
            stats.append(
                dict(
                    id = 'stat_subtickets_refused',
                    label = u'    z toho odmítnuté z důvodu: {}'.format(reason_dict[reason]),
                    label_display = u'&nbsp; &nbsp; &nbsp; z toho odmítnuté z důvodu: {}'.format(reason_dict[reason]),
                    value = counter_dk['dk']['refuse_reason'].get(reason, 0),
                    )
                )

        # odebrané SC
        stats.append(
            dict(
                id = 'stat_subtickets_cancelled_sc',
                label = u'- odebrané',
                value = counter_dk['dk']['cancelled_sc'],
                )
            )

        # - stornované
        stats.append(
            dict(
                id = 'stat_subtickets_cancelled',
                label = u'- stornované',
                value = counter_dk['dk']['cancelled'],
                )
            )

        # - reklamované
        stats.append(
            dict(
                id = 'stat_subtickets_complained',
                label = u'- reklamované',
                value = counter_dk['dk']['complained'],
                )
            )

        stats.append(
            dict(
                id = 'sep',
                label = u'',
                value = None,
                )
            )

        stats.append(
            dict(
                id = 'label',
                label = u'Nejčastější žádající knihovny:',
                value = None,
                )
            )

        # - nejčastější ŽK - 1.-3.
        idx = 0
        for top_item in counter_dk['top3']:
            idx += 1
            label = u'{}. {} ({})'.format(
                idx,
                top_item['title'],
                top_item['sigla'].upper(),
                )
            stats.append(
                dict(
                    id = 'stat_subtickets_top_zk',
                    label = label,
                    value = top_item['counter'],
                    )
                )

        return stats


class DiliaXMLView(ReportBaseView):
    """Dília XML report
    """

    template = ViewPageTemplateFile('edd_dilia_xml.pt')

    url_postfix = '/edd_dilia_xml'

    def quarters(self, start_y, start_q) :
        q_ids = []
        now = DateTime()
        now_year = now.year()
        now_month = now.month()
        now_quarter = (now_month+2)//3
        for y in range(start_y, now_year+1):
            for q in range(1, 4+1):
                if y == start_y and q < start_q:
                    continue
                if y == now_year and q > now_quarter:
                    break
                q_id = u'{}Q{}'.format(y, q)
                q_ids.append(q_id)
        return q_ids

    def call(self):
        self.ticket_type = 'edd'
        self.service_label = u'EDD'
        self.entry_subtype = None

        super(DiliaXMLView, self).call()

        if not self.is_ziskej_operator:
            self.redirect(self.portal_url)
            return
        self.unauthorized = False

        self.context_url_view = self.context_url + u'/edd_dilia_xml'

        if False:
            # self.debug_data()
            quarter = '2022Q1'
            dilia_data = self.dilia_data(quarter)
            data_xml = self.generate_xml(quarter, dilia_data)
            print(data_xml)

        # Generovat nový
        quarter = self.text_from_request('generate_xml')
        if quarter:
            self.clear_xmls(quarter)
            filename = self._get_filename_by_q(quarter)
            dilia_data = self.dilia_data(quarter)
            data_xml = self.generate_xml(quarter, dilia_data)
            if DEBUG_DILIA:
                print(data_xml)
            self.write_xml(filename, data_xml)

        # Vygenerované
        data = self.generated_xml()


        # dummy_data_1 = {
        #     '2021Q3':u'edd-dilia-2021Q3-e70e4f33.xml',
        #     '2021Q4':u'edd-dilia-2021Q4-e70e4f32.xml',
        #     '2020Q3':u'edd-dilia-2021Q3-e70e4f33.xml',
        #     '2020Q4':u'edd-dilia-2021Q4-e70e4f32.xml',
        #     }
        start_y = 2020
        start_q = 1
        base_url_download = self.portal_url + '/files_download/'
        base_url_generate = self.context_url_view + u'?generate_xml='
        # vrať kvartály od start_q do současného (včetně)
        q_ids = self.quarters(start_y, start_q,)
        self.list_for_ui = []
        for q_id in q_ids:
            item = dict(
                quarter = q_id,
                url_download = u'',
                url_generate = u'{}{}'.format(base_url_generate, q_id),
                )
            if q_id in data:
                item["url_download"] = base_url_download + data[q_id]
            self.list_for_ui.append(item)
        if self.list_for_ui:
            self.list_for_ui[-1]['is_part'] = True
        self.list_for_ui.reverse()
        """
        ToDo

        Připravit seznam pro UI, kde každá položka bude dict obsahující label pro kvartál, url pro stažení souboru, url pro vygenerování souboru.  Label bude obsahovat string " (část)", pokud daný kvartál právě probíhá.
        
        V UI zobrazit seznam kvartálů a pro každý z nich umožnit stáhnout dříve vygenerovaný soubor, pokud existuje, jinak ho vygenerovat.  Seznam třídit opačně, tedy 2022Q1 jako první.

        Později:

        Přidat funkcionalitu na vygenerování nového reportu.

        Přidat funkcionalitu na zjištění dříve vygenerovaných reportů ("data")
        """

    def debug_data(self):
        query = dict(
            portal_type=u'ReportEntry',
            ticket_type=u'edd',
            entry_subtype=u'edd-dilia-std',
            sort_on='hid',
            sort_order='reverse',
            )
        brains = self._catalog_search(query)[:3]
        idx = 0
        for brain in brains:
            re = brain._unrestrictedGetObject()
            try:
                t = getattr(self.portal.tickets, re.ticket_id, None)
            except:
                if DEBUG_DILIA:
                    print("re.ticket_id:", re.ticket_id)
                raise
            if not t or not t.is_edd:
                continue
            try:
                st = getattr(t, re.subticket_id, None)
            except:
                if DEBUG_DILIA:
                    print("re.subticket_id:", re.subticket_id)
                raise
            if not st:
                continue
            try:
                data_json = re.edd_dilia_data
            except:
                data_json = None
            if not data_json:
                continue
            if DEBUG_DILIA:
                print(
                    re.hid,
                    're',
                    re.getId() or 'X',
                    'st',
                    st and st.getId() or 'X',
                    't',
                    t and t.getId() or 'X',
                    '{}/debug'.format(re.absolute_url()),
                    )
            idx += 1
            try:
                dilia = json.loads(data_json)
            except:
                if DEBUG_DILIA:
                    print('Dilia ERROR:')
                    print(repr(data_json))
                continue
            if DEBUG_DILIA:
                print('Dilia:')
                pprint(dilia)

    def dilia_data(self, quarter):
        # Query
        year = quarter[:4]
        month = str(int(quarter[5]) * 3 - 2)
        if month == '10':
            year_end = str(int(year) + 1)
            month_end = '01'
        else:
            year_end = year
            month_end = str(int(month) + 3)
        if len(month) < 2:
            month = '0' + month
        if len(month_end) < 2:
            month_end = '0' + month_end
        date_range = dict(
            query=(
                DateTime('{}-{}-01 0:00:00'.format(year, month)),
                DateTime('{}-{}-01 0:00:00'.format(year_end, month_end)),
            ),
            range='min:max',
            )
        query = dict(
            portal_type=u'ReportEntry',
            ticket_type=u'edd',
            entry_subtype=u'edd-dilia-std',
            report_date=date_range,
            sort_on='report_date',
            )

        # Search
        brains = self._catalog_search(query)

        # Data
        data = []
        idx = 0
        for brain in brains:
            re = brain._unrestrictedGetObject()
            try:
                t = getattr(self.portal.tickets, re.ticket_id, None)
            except:
                if DEBUG_DILIA:
                    print("re.ticket_id:", re.ticket_id)
                raise
            if not t:
                logger.warning('Missing ticket for report entry {}'.format(re.getId()))
                continue
            try:
                st = getattr(t, re.subticket_id, None)
            except:
                if DEBUG_DILIA:
                    print("re.subticket_id:", re.subticket_id)
                raise
            if not st:
                logger.warning('Missing subticket for report entry {}'.format(re.getId()))
                continue
            try:
                data_json = re.edd_dilia_data
            except:
                data_json = None
            # FIXME
            if not data_json:
                continue
            idx += 1
            if DEBUG_DILIA:
                print(
                    re.hid,
                    're',
                    re.getId() or 'X',
                    'st',
                    st and st.getId() or 'X',
                    't',
                    t and t.getId() or 'X',
                    '{}/debug'.format(re.absolute_url()),
                    )
            dilia = None
            if data_json:
                try:
                    dilia = json.loads(data_json)
                except:
                    if DEBUG_DILIA:
                        print('Dilia ERROR:')
                        print(repr(data_json))
            if DEBUG_DILIA:
                print('Dilia:')
                pprint(dilia)

            reader_info = u''
            if t.reader_firstname:
                reader_info += t.reader_firstname + u' '
            if t.reader_lastname:
                reader_info += t.reader_lastname + u' '
            reader_info = reader_info.strip()
            # FIXME-EDD
            # if not reader_info:
            #     reader_info = u''
            autors = []
            kolektiv = 0
            if dilia:
                for i in range(1, 3+1):
                    if dilia.get('autor_prijmeni_{}'.format(i), None):
                        autors.append(dict(
                            jmeno=dilia.get('autor_jmeno_{}'.format(i), u''),
                            prijmeni=dilia.get('autor_prijmeni_{}'.format(i), u''),
                            ))
                if dilia.get('kolektiv', None):
                    kolektiv = 1
            te = t.Edd_data()
            item = dict(
                datum_poskytnuti=re.report_date.strftime('%d.%m.%Y'),  # "08.07.2021" – OK
                pocet_stran=st.pages_number_dk,  # "7" – OK
                oznaceni_transakce=t.getId(),  # "A344900" – tady dáme asi označení požadavku
                uzivatel=reader_info,  # "202539" – k diskuzi
                zhotovitel=st.library_dk.upper(),  # "ABA007" – tzn. sigla DK
                zastupovana_knihovna=st.library_zk.upper(),  # "106319" – tady můžeme dát siglu ŽK, že?
                podoba='ELECTRONIC',  # "ELECTRONIC" – dáme stejně
                nazev=t.doc_title,  # <NAZEV>Studies on Schiff base complexes of salicylaldehyde with sulphamethoxazole and their antimicrobial activities</NAZEV> – Název článku nebo kapitoly
                souborny_nazev=te.get('doc_title_in', u''),  #<SOUBORNY_NAZEV>Polish Journal of Chemistry</SOUBORNY_NAZEV> – Název časopisu nebo monografie
                autors=autors,
                kolektiv=kolektiv,  # <KOLEKTIV>1</KOLEKTIV> – nemáme
                vydavatel=dilia and dilia.get('vydavatel', u'') or u'',  # <VYDAVATEL>Panstwowe Wydawnictwo Naukowe</VYDAVATEL> – nemáme, ale máme rok a místo vydání
                rok_vydani=dilia and dilia.get('rok_vydani', u'') or u'',  # <ROK_VYDANI>2006</ROK_VYDANI> – nemáme, ale máme rok a místo vydání
                rocnik=te.get('doc_number_pyear', u''),  # <ROCNIK>80</ROCNIK> – ročník pro časopis někdy máme
                isbn_issn=t.doc_isbn or t.doc_issn,  # <ISBN_ISSN>0137-5083</ISBN_ISSN> – OK
                zeme_vydani=dilia and dilia.get('zeme_vydani', u'') or u'',  # <ZEME_VYDANI>PL</ZEME_VYDANI> – nemáme
                )
            if DEBUG_DILIA:
                pprint(item)
            data.append(item)
        return data

    def generate_xml(self, quarter, data):
        """Ukázka:

        <DOKUMENT datum_poskytnuti="08.07.2021" pocet_stran="7" oznaceni_transakce="A344900" uzivatel="202539" zhotovitel="ABA007" zastupovana_knihovna="106319" podoba="ELECTRONIC">
        <NAZEV>Studies on Schiff base complexes of salicylaldehyde with sulphamethoxazole and their antimicrobial activities</NAZEV>
        <SOUBORNY_NAZEV>Polish Journal of Chemistry</SOUBORNY_NAZEV>
        <AUTOR jmeno="C. D." prijmeni="Sheela" role="A" poznamka=""/>
        <AUTOR jmeno="A." prijmeni="Gomathi" role="A" poznamka=""/>
        <AUTOR jmeno="S." prijmeni="Ravichandran" role="A" poznamka=""/>
        <KOLEKTIV>1</KOLEKTIV>
        <VYDAVATEL>Panstwowe Wydawnictwo Naukowe</VYDAVATEL>
        <ROK_VYDANI>2006</ROK_VYDANI>
        <ROCNIK>80</ROCNIK>
        <ISBN_ISSN>0137-5083</ISBN_ISSN>
        <ZEME_VYDANI>PL</ZEME_VYDANI>
        </DOKUMENT>
        """
        data_xml = u''
        for item in data:
            autor_xml = u''
            for autor in item['autors']:
                autor_xml += u'<AUTOR jmeno="{}" prijmeni="{}" role="A" poznamka="" />\n'.format(autor['jmeno'],autor['prijmeni'])
            item['autor_xml'] = autor_xml
            item_xml = u'''\
<DOKUMENT datum_poskytnuti="{datum_poskytnuti}" pocet_stran="{pocet_stran}" oznaceni_transakce="{oznaceni_transakce}" uzivatel="{uzivatel}" zhotovitel="{zhotovitel}" zastupovana_knihovna="{zastupovana_knihovna}" podoba="ELECTRONIC">
<NAZEV>{nazev}</NAZEV>
<SOUBORNY_NAZEV>{souborny_nazev}</SOUBORNY_NAZEV>
{autor_xml}\
<KOLEKTIV>{kolektiv}</KOLEKTIV>
<VYDAVATEL>{vydavatel}</VYDAVATEL>
<ROK_VYDANI>{rok_vydani}</ROK_VYDANI>
<ROCNIK>{rocnik}</ROCNIK>
<ISBN_ISSN>{isbn_issn}</ISBN_ISSN>
<ZEME_VYDANI>{zeme_vydani}</ZEME_VYDANI>
</DOKUMENT>
'''.format(**item)
            data_xml += item_xml
        year = quarter[:4]
        month = str(int(quarter[5]) * 3 - 2)
        month_end = str(int(month) + 2)
        if month_end in ('3', '12', ):
            day_end = '31'
        else:
            day_end = '30'
        if len(month) < 2:
            month = '0' + month
        if len(month_end) < 2:
            month_end = '0' + month_end

        data_xml = u'''\
<?xml version="1.0" encoding="UTF-8" ?>
<EDD poskytovatel="Získej" obdobi_od="01.{}.{}" obdobi_do="{}.{}.{}">
{}\
</EDD>
'''.format(month, year, day_end, month_end, year, data_xml)
        return data_xml

    def _get_filename_by_q(self, quarter):
        hash_str = str(uuid.uuid4()).replace('-', '')[:8]
        return u'edd-dilia-{}-{}.xml'.format(quarter, hash_str)

    def generated_xml(self):
        names = os.listdir(ZISKEJ_VAR_DOWNLOAD)
        names=[x for x in names if x.startswith("edd-dilia") and x.endswith(".xml")]
        data = dict()
        for name in names:
            q = name[10:16]
            data[q] = name
        return data
        # filepath = os.path.join(ZISKEJ_VAR_DOWNLOAD, filename)

    def write_xml(self, filename, data_xml):
        filepath = os.path.join(ZISKEJ_VAR_DOWNLOAD, filename)
        with codecs.open(filepath, 'w', encoding='utf-8') as xml_file:
            xml_file.write(data_xml)

    def clear_xmls(self, quarter):
        names = os.listdir(ZISKEJ_VAR_DOWNLOAD)
        names=[x for x in names if x.startswith("edd-dilia-{}".format(quarter)) and x.endswith(".xml")]
        for name in names:
            filepath = os.path.join(ZISKEJ_VAR_DOWNLOAD, name)
            aow_os_utils_rm(filepath)
