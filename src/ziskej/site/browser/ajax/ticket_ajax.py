# -*- coding: utf-8 -*-
"""BrowserView: AJAX metody pro Ticket.
"""

import json
import random

from DateTime import DateTime
from plone import api

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.storage import storage_set, storage_get, storage_del
from ziskej.site.ticket_load_balancing import (
    load_balancing_availability_init,
    load_balancing_availability_retrieve,
    )
from ziskej.site.utils import log_exception


LOCK_AVAILABILITY_KEY = 'load_availability_lock'
DEBUG_DISABLE_LOCK = False
DEBUG_RAISE = False
DEBUG = False


class AjaxView(ZiskejBrowserView):
    """AjaxView add to it settings usefull for AJAX such as no diazo, json
    content type, no proxy or browser cache allowed.  It also catches all
    exceptions and it is transforming them into result with status Error and
    error message based on exception.  It does not provide security check while
    this should be done for each API call individually.

    Each AJAX call doing write (most of them do) has to have valid csrf token.
    """

    # def _call_init(self):
    #     super(AjaxView, self)._call_init()

    def __call__(self):
        # init
        self._call_init()

        # security
        self._check_perm()

        # catch exceptions and transform them
        try:
            result_dict = self.call()
        except Exception, e:
            result_dict = dict(
                status = u'Error',
                error_message = unicode(e),
                )

            if DEBUG_RAISE:
                raise
            log_exception(e)

        # no diazo
        self.request.response.setHeader('X-Theme-Disabled', '1')

        # json content type
        self.request.response.setHeader('Content-Type', 'application/json')

        # Angular integration
        self.request.response.setHeader('Access-Control-Allow-Origin', '*')

        # no cache
        self.request.response.setHeader('Cache-Control', 'no-store,no-cache,must-revalidate,post-check=0,pre-check=0')
        self.request.response.setHeader('Pragma', 'no-cache')
        now = DateTime().ISO()
        self.request.response.setHeader('Expires', '-1')
        self.request.response.setHeader('Last-modified', now)

        # jsonize
        try:
            result_json = json.dumps(result_dict, ensure_ascii=False)
        except Exception, e:
            error_dict = dict(
                status = u'Error',
                error_message = u'Interní chyba, administrátoři byli informováni.',
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        return result_json

    def _check_perm(self):
        """Security policy has to be defined in each subclass."""
        logger.warning('Not implemented security restriction for AJAX call')


class TicketAjaxView(AjaxView):
    """TicketAjaxView provides ticket context including ticket specific logic.
    """

    # copy & paste from ticket_core
    def setup_is_role(self):
        # toto bylo nastaveno v ZiskejBrowserView univerzálně, tzn. i bez
        # kontextu ticketu, nyní to nastavíme pro konkrétní ticket a subticket

        # reader
        if self.is_reader:
            if not self.ticket.is_reader:
                self.is_reader = False
            elif self.ticket.reader != self.user_id:
                self.is_reader = False

        # v ticket/subticket nepoužívat, ale ptát se případně explicitně na
        #     if self.is_librarian_zk or self.is_librarian_dk:
        # případně 
        #     tal:condition="python: view.is_librarian_zk or view.is_librarian_dk"
        self.is_librarian = False

        self.is_librarian_zk = bool(
            self.ziskej_user_info and
            self.ziskej_user_info['role'] == 'Librarian' and
            self.library and
            self.ticket.library_zk and
            self.library.sigla and
            self.ticket.library_zk == self.library.sigla.lower()
            )
        self.is_librarian_zk_op = self.is_librarian_zk or self.is_ziskej_operator

        self.is_librarian_dk = bool(
            self.ziskej_user_info and
            self.ziskej_user_info['role'] == 'Librarian' and
            self.library and
            self.subticket and
            self.subticket.library_dk and
            self.library.sigla and
            self.subticket.library_dk == self.library.sigla.lower()
            )
        self.is_librarian_dk_op = self.is_librarian_dk or self.is_ziskej_operator

    def _call_init(self):
        super(TicketAjaxView, self)._call_init()
        self.ticket = self.context
        self.setup_is_role()


class LoadAvailabilityInitView(TicketAjaxView):

    def _check_perm(self):
        if not self.is_librarian_zk_op:
            raise Exception('Unauthorized')

    def call(self):

        # check lock
        lock_timestamp = storage_get(self.context, LOCK_AVAILABILITY_KEY)

        # expire lock after 15min
        if lock_timestamp and DateTime(lock_timestamp)+1/24/60*15 < DateTime():
            if DEBUG:
                print 'LoadAvailabilityInitView: lock expired'
            lock_timestamp = None
            storage_del(self.context, LOCK_AVAILABILITY_KEY)

        # lock
        if lock_timestamp:
            if DEBUG:
                print 'LoadAvailabilityInitView: locked - and disabled for debug'
            if not DEBUG_DISABLE_LOCK:
                # FIXME otestovat tuto část
                return dict(
                    status = u'OK',
                    result = dict(
                        init = u'locked',
                        ),
                    )

        # create lock
        if DEBUG:
            print 'LoadAvailabilityInitView: create lock'
        lock_timestamp = DateTime().ISO()
        storage_set(self.context, LOCK_AVAILABILITY_KEY, lock_timestamp)

        # set load balancing state before calling call
        self.ticket.set_lb_state(u'load availability init')

        # call
        aft = load_balancing_availability_init(self.portal, self.ticket)

        # return info
        return dict(
            status = u'OK',
            result = dict(
                init = u'started',
                available = aft['available'],
                finished = aft['finished'],
                total = aft['total'],
                ),
            )

class LoadAvailabilityGetView(TicketAjaxView):

    def _check_perm(self):
        if not self.is_librarian_zk_op:
            raise Exception('Unauthorized')

    def call(self):
        # call
        aft = load_balancing_availability_retrieve(self.portal, self.ticket)

        if aft['finished'] >= aft['total']:
            # set load balancing state before releasing locl
            self.ticket.set_lb_state(u'load availability finished')
            # release lock when finished
            storage_del(self.context, LOCK_AVAILABILITY_KEY)

        # return info
        return dict(
            status = u'OK',
            result = dict(
                init = u'started',
                available = aft['available'],
                finished = aft['finished'],
                total = aft['total'],
                ),
            )


class LoadAvailabilityFinishView(TicketAjaxView):

    def _check_perm(self):
        if not self.is_librarian_zk_op:
            raise Exception('Unauthorized')

    def call(self):
        # set load balancing state before releasing locl
        self.ticket.set_lb_state(u'load availability finished')
        # release lock when finished
        storage_del(self.context, LOCK_AVAILABILITY_KEY)

        # return info
        return dict(
            status = u'OK',
            )


class MessagesMarkReadView(TicketAjaxView):

    def _check_perm(self):
        if not self.is_librarian_zk_op and not self.is_reader:
            raise Exception('Unauthorized')

    def call(self):
        # for ticket reader or librarian_zk could call this

        # but for now only librarian_zk is calling this
        if not self.is_librarian_zk and not self.is_reader:
            if self.is_ziskej_operator:
                return dict(
                    status = u'OK',
                    )
            raise Exception('Invalid role')

        subticket_id = self.request.get('dkpm_subticket_id', None)
        if DEBUG:
            print "MessagesMarkReadView: subticket_id: {}".format(subticket_id)

        # ticket - library_zk / reader
        if subticket_id is None:
            # reader
            if self.is_reader and self.ticket.pm_unread == 'reader':
                if DEBUG:
                    print 'MessagesMarkReadView: make ticket not unread'
                self.ticket.pm_unread = ''
                self.ticket.pm_date = None

            # librarian_zk
            if self.is_librarian_zk and self.ticket.pm_unread == 'library_zk':
                if DEBUG:
                    print 'MessagesMarkReadView: make ticket not unread'
                self.ticket.pm_unread = ''
                self.ticket.pm_date = None

        # ticket - library_zk / library_dk
        else:
            subticket = getattr(self.ticket, subticket_id, None)
            if not subticket:
                raise Exception('Invalid call')
            if self.is_librarian_zk and subticket.pm_unread == 'library_zk':
                if DEBUG:
                    print 'MessagesMarkReadView: make subticket not unread'
                subticket.pm_unread = ''
                subticket.pm_date = None

        # return info
        return dict(
            status = u'OK',
            )
