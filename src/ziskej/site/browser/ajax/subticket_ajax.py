# -*- coding: utf-8 -*-
"""BrowserView: AJAX metody pro Subticket.
"""

import json
import random

from DateTime import DateTime
from plone import api

from ticket_ajax import AjaxView


class SubticketAjaxView(AjaxView):
    """TicketAjaxView provides ticket context including ticket specific logic.
    """

    # copy & paste from ticket_core
    def setup_is_role(self):
        # toto bylo nastaveno v ZiskejBrowserView univerzálně, tzn. i bez
        # kontextu ticketu, nyní to nastavíme pro konkrétní ticket a subticket

        # reader
        if self.is_reader:
            self.is_reader = False

        # v ticket/subticket nepoužívat, ale ptát se případně explicitně na
        #     if self.is_librarian_zk or self.is_librarian_dk:
        # případně 
        #     tal:condition="python: view.is_librarian_zk or view.is_librarian_dk"
        self.is_librarian = False

        self.is_librarian_zk = False
        self.is_librarian_zk_op = self.is_librarian_zk or self.is_ziskej_operator

        self.is_librarian_dk = bool(
            self.ziskej_user_info and
            self.ziskej_user_info['role'] == 'Librarian' and
            self.library and
            self.subticket and
            self.subticket.library_dk and
            self.library.sigla and
            self.subticket.library_dk == self.library.sigla.lower()
            )
        self.is_librarian_dk_op = self.is_librarian_dk or self.is_ziskej_operator

    def _call_init(self):
        super(SubticketAjaxView, self)._call_init()
        self.subticket = self.context
        self.subticket_last = self.subticket
        self.setup_is_role()


class MessagesMarkReadView(SubticketAjaxView):

    def _check_perm(self):
        if not self.is_librarian_dk_op:
            raise Exception('Unauthorized')

    def call(self):
        if self.is_librarian_dk and self.subticket.pm_unread == 'library_dk':
            print 'make subticket not unread'
            self.subticket.pm_unread = ''
            self.subticket.pm_date = None

        # return info
        return dict(
            status = u'OK',
            )
