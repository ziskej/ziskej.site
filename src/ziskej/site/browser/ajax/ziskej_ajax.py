# -*- coding: utf-8 -*-

import json
import random

from DateTime import DateTime
from plone import api

from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.storage import storage_set, storage_get, storage_del
from ziskej.site.ticket_helpers import (
    SUBTICKET_PORTAL_TYPES,
    SUBTICKETMVS_WF_S,
    TICKET_PORTAL_TYPES,
    TICKETMVS_WF_S,
    )
from ziskej.site.ticket_load_balancing import (
    load_balancing_availability_init,
    load_balancing_availability_retrieve,
    )
from ziskej.site.utils import log_exception


DEBUG = False
DEBUG_RAISE = False


class AjaxView(ZiskejBrowserView):

    # def _call_init(self):
    #     super(AjaxView, self)._call_init()

    def __call__(self):
        # init
        self._call_init()

        # security
        self._check_perm()

        # catch exceptions and transform them
        try:
            result_dict = self.call()
        except Exception, e:
            result_dict = dict(
                status = u'Error',
                error_message = unicode(e),
                )

            if DEBUG_RAISE:
                raise
            log_exception(e)

        # no diazo
        self.request.response.setHeader('X-Theme-Disabled', '1')

        # json content type
        self.request.response.setHeader('Content-Type', 'application/json')

        # Angular integration
        self.request.response.setHeader('Access-Control-Allow-Origin', '*')

        # no cache
        self.request.response.setHeader('Cache-Control', 'no-store,no-cache,must-revalidate,post-check=0,pre-check=0')
        self.request.response.setHeader('Pragma', 'no-cache')
        now = DateTime().ISO()
        self.request.response.setHeader('Expires', '-1')
        self.request.response.setHeader('Last-modified', now)

        # jsonize
        try:
            result_json = json.dumps(result_dict, ensure_ascii=False)
        except Exception, e:
            error_dict = dict(
                status = u'Error',
                error_message = u'Interní chyba, administrátoři byli informováni.',
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        return result_json


class OverviewChartsDataView(AjaxView):

    def _check_perm(self):
        pass
        # if not self.is_librarian_zk_op and not self.is_reader:
        #     raise Exception('Unauthorized')

    def return_error(self, message):
        return dict(
            status = u'Error',
            error_message = message,
            )

    def return_ok(self, tickets_data, subtickets_data):
        return dict(
            status = u'OK',
            chartTicketsData = tickets_data,
            chartSubticketsData = subtickets_data,
            )

    def call(self):
        # for ticket reader or librarian_zk could call this

        if self.is_ziskej_operator:
            sigla = None
        elif self.is_librarian:
            sigla = self.library.sigla.lower()
        else:
            return self.return_error(u'Nějaká chybka, její vysvětlení.')

        # # Dummy data
        # tickets_data = [5, 10, 2, 10, 20, 5, 5]
        # subtickets_data = [2, 2, 2, 10, 5, 20, 5, 5]

        created_query = dict(
            query = DateTime(
                '{}-01-01T00:00:00+01:00'.format(DateTime().year())),
            range = 'min',
            )

        tickets_data = []
        for wf_state in TICKETMVS_WF_S:
            query = dict(
                portal_type = TICKET_PORTAL_TYPES,
                review_state = wf_state,
                created = created_query,
                )
            if sigla:
                query['library_zk'] = sigla
            brains = self.catalog.unrestrictedSearchResults(query)
            tickets_data.append(len(brains))
        subtickets_data = []
        for wf_state in SUBTICKETMVS_WF_S:
            query = dict(
                portal_type = SUBTICKET_PORTAL_TYPES,
                review_state = wf_state,
                created = created_query,
                )
            if sigla:
                query['library_dk'] = sigla
            brains = self.catalog.unrestrictedSearchResults(query)
            subtickets_data.append(len(brains))
        return self.return_ok(tickets_data, subtickets_data)
