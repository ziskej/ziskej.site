# -*- coding: utf-8 -*-

from datetime import datetime
import json
import jwt
import logging
import os.path

from DateTime import DateTime
from plone import api
#from plone.protect.interfaces import IDisableCSRFProtection
#from zope.interface import alsoProvides
from Products.Five import BrowserView
import transaction

from ziskej.site import (
    ZISKEJ_PYRAMID_MODE,
    ZISKEJ_API_AKS_ID,
    SHARED_SECRET_ZISKEJ_APP_PYRAMID,
    SECRETS_DIR,
    )
from ziskej.site.auth.auth_base import get_reader_by_eppn
from ziskej.site.library_helpers import lib_helper
from ziskej.site.notifications import (
    ZiskejNotifications,
    ErrorNotifications,
    )
from ziskej.site.utils import (
    format_date,
    get_instance_type_label,
    get_time0,
    get_time_ms,
    safe_unicode,
    sanitize_input_text,
    )
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger('ziskej.apiaks')
logger_time = logging.getLogger("profiling")

PROFILING_ON = True

API_EXCEPTION_MESSAGES = {}

RAISE_EXCEPTION = False


class ApiAksException(Exception):
    pass


class ApiAksException400(ApiAksException):
    pass


class ApiAksException403(ApiAksException):
    pass


class ApiAksException404(ApiAksException):
    pass


class ApiAksException500(ApiAksException):
    pass


class ApiAksBaseView(BrowserView):
    """Base view for API AKS provides common utilities and methods for any API
    AKS call including parse JWT, catch exceptions, jsonify result and set
    headers for API using json.
    """

    def __call__(self):
        """Handle BrowserView for each API call."""
        self.time0 = get_time0()

        self.portal = api.portal.get()
        self.catalog = api.portal.get_tool(name='portal_catalog')
        self.catalog_search = self.catalog.unrestrictedSearchResults

        # typ instance
        self.instance_type, self.instance_type_label, instance_title_postfix = get_instance_type_label()
        self.is_instance_prod = bool(self.instance_type == 'prod')
        self.is_instance_local = bool(self.instance_type == 'development')
        self.is_instance_testing = bool(self.instance_type in ('development', 'dev', 'test'))

        self.testing_now_dt = ziskej_parameter(
            'testing_now_str', portal=self.portal)

        # lib_helper - should be initialized during start without portal and
        # first call provides portal, therefore it is important to simulate this
        # for API AKS as first call after restart.  Cheap call for already
        # initialized cheap call
        self.lib_helper = lib_helper
        self.lib_helper.bind_portal(self.portal)

        self.reader = None
        self.user_id = u'API AKS'
        self.library = None
        self.ticket = None
        self.subticket = None

        print " "
        print "-- url:", self.request.URL
        print " "
        print "-- form:", self.request.form
        print " "

        try:
            # self.parse_jwt()
            http_code, result_data = self.call()
            result_dict = dict(
                status = u'OK',
                http_code = http_code,
                result = result_data,
                )
            result_json = json.dumps(result_dict, ensure_ascii=False)

        except ApiAksException403, e:
            if RAISE_EXCEPTION:
                raise Exception(str(e))
            transaction.abort()
            error_dict = dict(
                status = u'Error',
                http_code = 403,
                error_message = API_EXCEPTION_MESSAGES.get(str(e), unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        except ApiAksException404, e:
            if RAISE_EXCEPTION:
                raise Exception(str(e))
            transaction.abort()
            error_dict = dict(
                status = u'Error',
                http_code = 404,
                error_message = API_EXCEPTION_MESSAGES.get(str(e), unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        except ApiAksException400, e:
            if RAISE_EXCEPTION:
                raise Exception(str(e))
            transaction.abort()
            error_dict = dict(
                status = u'Error',
                http_code = 422,
                error_message = API_EXCEPTION_MESSAGES.get(str(e), unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        except ApiAksException, e:
            if RAISE_EXCEPTION:
                raise Exception(str(e))
            transaction.abort()
            logger.warning(e)
            error_dict = dict(
                status = u'Error',
                http_code = 500,
                error_message = API_EXCEPTION_MESSAGES.get(str(e), unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        except Exception, e:
            if RAISE_EXCEPTION:
                raise Exception(str(e))
            transaction.abort()
            logger.exception(e)
            error_dict = dict(
                status = u'Error',
                http_code = 500,
                error_message = u'Unexpected exception: {}'.format(unicode(e)),
                )
            result_json = json.dumps(error_dict, ensure_ascii=False)

        try:
            print "-- result_dict:", result_dict
        except:
            print "-- error_dict:", error_dict
        print " "

        # no diazo
        self.request.response.setHeader('X-Theme-Disabled', '1')

        # json content type
        self.request.response.setHeader('Content-Type', 'application/json')

        # integration
        self.request.response.setHeader('Access-Control-Allow-Origin', '*')

        # no cache
        self.request.response.setHeader('Cache-Control', 'no-store,no-cache,must-revalidate,post-check=0,pre-check=0')
        self.request.response.setHeader('Pragma', 'no-cache')
        now = DateTime().ISO()
        self.request.response.setHeader('Expires', '-1')
        self.request.response.setHeader('Last-modified', now)

        self.log_time('end of request (apiaks)')

        return result_json

    def call(self):
        self.check_jwt_apiaks()
        self.check_jwt_aks()
        action_id = self.text_from_request('action_id', None)
        if action_id is None:
            raise ApiAksException400('Missing method')
        if action_id not in self.action_ids:
            raise ApiAksException400('Invalid method')
        if action_id in self.unauthorized_action_ids:
            raise ApiAksException403('Unauthorized access')
        method = getattr(self, action_id, None)
        if method is None:
            raise ApiAksException400('Missing method')
        return method()

    def check_jwt_apiaks(self):
        token = self.text_from_request('apiaks_token')
        if not token:
            raise ApiAksException403('Missing authorization')
        try:
            token_dict = jwt.decode(
                token, SHARED_SECRET_ZISKEJ_APP_PYRAMID, algorithms=['HS256'])
        except jwt.ExpiredSignatureError, e:
            logger.warning('ExpiredSignatureError: {msg}'.format(msg=str(e)))
            raise ApiAksException403('Expired authorization token for apiaks')
        except jwt.exceptions.InvalidTokenError, e:
            logger.error('InvalidTokenError: {msg}'.format(msg=str(e)))
            raise ApiAksException403('Invalid authorization token for apiaks')
        if not token_dict.get('iat', ''):
            raise ApiAksException403('Missing iat in authorization token for apiaks')
        if not token_dict.get('exp', ''):
            raise ApiAksException403('Missing exp in authorization token for apiaks')
        if token_dict.get('iss', '') != ZISKEJ_API_AKS_ID:
            raise ApiAksException403('Invalid iss in apiaks')
        if token_dict.get('app', '') != ZISKEJ_API_AKS_ID:
            raise ApiAksException403('Invalid app in apiaks')
        return token_dict

    def check_jwt_aks(self):
        """Do the basic JWT check and parse it.  Each class could and should
        check JWT content with provided parameters.
        """

        sigla = self.text_from_request('sigla')
        if not sigla:
            raise ApiAksException400('Missing sigla')
        token = self.text_from_request('aks_token')
        if not token:
            raise ApiAksException403('Missing authorization')

        app_id = '{}'.format(sigla)
        app_id = app_id.lower()

        secret_filename = '{}-secret.txt'.format(sigla)
        filepath = os.path.join(SECRETS_DIR, secret_filename)
        if not os.path.isfile(filepath):
            if not os.path.isdir(SECRETS_DIR):
                raise ApiAksException403('Invalid configuration for secrets.')
            raise ApiAksException403('Missing secret info.')
        with open(filepath, 'rb') as f:
            secret = f.read()

        try:
            token_dict = jwt.decode(token, secret, algorithms=['HS256'])
        except jwt.ExpiredSignatureError, e:
            logger.warning('ExpiredSignatureError: {msg}'.format(msg=str(e)))
            raise ApiAksException403('Expired authorization token for aks')
        except jwt.exceptions.InvalidTokenError, e:
            logger.error('InvalidTokenError: {msg}'.format(msg=str(e)))
            raise ApiAksException403('Invalid authorization token for aks')
        if not token_dict.get('iat', ''):
            raise ApiAksException403('Missing iat in authorization token for aks')
        if not token_dict.get('exp', ''):
            raise ApiAksException403('Missing exp in authorization token for aks')
        if token_dict.get('iss', '') != app_id:
            raise ApiAksException403('Invalid iss in aks')
        if token_dict.get('app', '') != app_id:
            raise ApiAksException403('Invalid app in aks')
        return token_dict

    def log_time(self, txt, time0=None):
        if not PROFILING_ON:
            return
        if time0 is None:
            time0 = self.time0
        msg = u'{ts} {txt}'.format(ts=get_time_ms(time0), txt=txt)
        logger_time.info(msg)

    def get_DateTime_from_date_str(self, value_str):
        dt = None
        try:
            #value = datetime.strptime(value_str + ' 12:00', '%d.%m.%Y %H:%M')
            value = datetime.strptime(value_str + ' 12:00', '%Y-%m-%d %H:%M')
            value = value.strftime('%Y-%m-%d')
            dt = DateTime(value)
        except:
            logger.info('Ziskej API: Ignored invalid DateTime for value: {value_str}'.format(value_str=repr(value_str)))
            raise
        return dt

    def dt_as(self, dt, result_type='string'):
        if not dt:
            return None
        if result_type in ('string', 'datepicker', ):
            result = dt.strftime('%-d.%-m.%Y')
        elif result_type == 'iso':
            result = dt.strftime('%Y-%m-%d')
        elif result_type == 'isodt':
            result = dt.strftime('%Y-%m-%d %-H:%M:%S')
        elif result_type == 'DateTime':
            result = dt
        elif result_type == 'datetime':
            result = dt.asdatetime()
        elif result_type == 'datepicker':
            result = dt.asdatetime()
        return result

    def date_from_request(self, name, result_type='string'):
        value_str = self.request.get(name, None)
        if not value_str:
            return None
        if not isinstance(value_str, basestring):
            if isinstance(value_str, list):
                raise ApiAksException400('Expecting string but get list for {name}'.format(name=name))
            else:
                raise ApiAksException400('Expecting string but get not string for {name}'.format(name=name))
        value_str = sanitize_input_text(value_str)
        value_str = value_str.replace(' ', '')
        if not value_str:
            return None
        print " "
        print value_str
        print " "
        dt = self.get_DateTime_from_date_str(value_str)
        return self.dt_as(dt, result_type=result_type)

    def int_from_request(self, name, min=0, max=None):
        value = self.request.get(name, None)
        if value is None:
            return None
        value = value.strip()
        try:
            value = int(value)
        except ValueError:
            logger.warning(name + ' is not int: ' + str(value))
            value = None
        if min is not None and value < min:
            value = None
        if max is not None and value > max:
            value = None
        return value

    def text_from_request(self, name, lov=None):
        value = self.request.get(name, None)
        if value is None:
            return None
        if not isinstance(value, basestring):
            if isinstance(value, list):
                raise ApiAksException400('Expecting string but get list for {name}'.format(name=name))
            else:
                raise ApiAksException400('Expecting string but get not string for {name}'.format(name=name))
        #value = safe_utf8(value)
        value = safe_unicode(value)
        value = sanitize_input_text(value)  # cgi.escape
        if lov is not None and value not in lov:
            logger.info('ignored unexpected value for lov based input {name}'.format(name=name))
            value = None
        return value

    def list_of_texts_from_request(self, name, lov=None):
        values = self.request.get(name, None)
        if values is None:
            return None
        if isinstance(values, list):
            pass
        elif isinstance(values, basestring):
            # je chybou tady vrátit chybu, protože pro jednoprvkový seznam je
            # values string a převod na jednoprvkový seznam je snadný
            values = [values]
            #raise ApiAksException400('Expecting list of strings but get string for {name}'.format(name=name))
        else:
            raise ApiAksException400('Expecting list of strings for {name}'.format(name=name))

        data = []
        for value in values:
            #value = safe_utf8(value)
            value = safe_unicode(value)
            value = sanitize_input_text(value)  # cgi.escape
            if lov is not None and value not in lov:
                logger.info('ignored unexpected value for lov based input {name}'.format(name=name))
                continue
            if not value:
                continue
            if value in data:
                continue
            data.append(value)

        return data

    # def get_reader(self, eppn=None, none404=True, check_active=True,
    #                full=False):
    #     """Ověří a vrátí čtenáře na základě eppn.  Většinou ověřuje, zda je
    #     aktivní (vč. zda má GDPR souhlasy), parametr ,check_active při vytváření
    #     objednávky i zda je aktivní plně, tzn. i jeho mateřská knihovna je
    #     právě teď aktivní, parametr full.
    #     """

    #     if DEBUG_DUMMY_EPPN:
    #         self.eppn = u'ntk_ctenar'
    #         return self.portal.readers.ntk_ctenar
    #     if not eppn:
    #         eppn = self.text_from_request('eppn')
    #     if not eppn:
    #         raise ApiAksException400(u'Missing parameter eppn')
    #     self.eppn = u'{}'.format(eppn)

    #     reader = None

    #     if eppn == 'ntk_ctenar':
    #         reader = self.portal.readers.ntk_ctenar

    #     # curl -X PUT -d '{"first_name": "Test API", "last_name": "MZK Čtenář 01", "email": "auda.aow+mzk_ctenar_api01@gmail.com", "notification_enabled": false, "sigla": "BOA001", "is_gdpr_reg": true, "is_gdpr_data": true}' -H "Content-Type: application/json" -H "Authorization: bearer $token" https://ziskej-test.techlib.cz:9080/api/v1/readers/1185@mzk.cz
    #     # curl -X PUT -d '{"first_name": "Test API", "last_name": "MZK Čtenář 02", "email": "auda.aow+mzk_ctenar_api02@gmail.com", "notification_enabled": false, "sigla": "BOA001", "is_gdpr_reg": true, "is_gdpr_data": true}' -H "Content-Type: application/json" -H "Authorization: bearer $token" https://ziskej-test.techlib.cz:9080/api/v1/readers/1184@mzk.cz
    #     # curl -X PUT -d '{"first_name": "Test API", "last_name": "NTK Čtenář 01", "email": "auda.aow+ntk_ctenar_api01@gmail.com", "notification_enabled": false, "sigla": "ABA013", "is_gdpr_reg": true, "is_gdpr_data": true}' -H "Content-Type: application/json" -H "Authorization: bearer $token" https://ziskej-test.techlib.cz:9080/api/v1/readers/rasputin@techlib.cz
    #     # curl -X PUT -d '{"first_name": "Test API", "last_name": "NTK Čtenář 02", "email": "auda.aow+ntk_ctenar_api02@gmail.com", "notification_enabled": false, "sigla": "ABA013", "is_gdpr_reg": true, "is_gdpr_data": true}' -H "Content-Type: application/json" -H "Authorization: bearer $token" https://ziskej-test.techlib.cz:9080/api/v1/readers/dostojevskij@techlib.cz

    #     # curl -X PUT -d '{"first_name": "Test API", "last_name": "MZK Čtenář 02", "email": "auda.aow+mzk_ctenar_api02@gmail.com", "notification_enabled": false, "sigla": "BOA001", "is_gdpr_reg": true, "is_gdpr_data": true}' -H "Content-Type: application/json" -H "Authorization: bearer $token" http://localhost:8000/api/v1/readers/1184@mzk.cz

    #     ENABLE_TEST_USERS = False
    #     if ENABLE_TEST_USERS:
    #         TEST_USERS = {
    #             '1185@mzk.cz': '',
    #             '1184@mzk.cz': '',
    #             'rasputin@techlib.cz': '',
    #             'dostojevskij@techlib.cz': '',
    #             }
    #         if eppn in TEST_USERS:
    #             user_id = TEST_USERS[eppn]
    #             reader = getattr(self.portal.readers, user_id, None)

    #     if not reader:
    #         reader = get_reader_by_eppn(eppn)

    #     if not none404 and not reader:
    #         return None
    #     if not reader:
    #         raise ApiAksException404('Reader not found')
    #     if full and not reader.is_active_full:
    #         raise ApiAksException400('Inactive reader')
    #     elif check_active and not reader.is_active:
    #         raise ApiAksException400('Inactive reader')
    #     return reader

    # def initial_reader_setup(self, full=False):
    #     """Očekává, že je nastaven self.reader a že je aktivní"""

    #     try:
    #         if not self.reader:
    #             raise ApiAksException500('Expecting reader object')
    #     except:
    #         raise ApiAksException500('Expecting reader object')

    #     self.user_id = self.reader.getId()

    #     self.library_zk_sigla = self.reader.library
    #     if not self.library_zk_sigla:
    #         # Nemělo by nastat, protože pak není aktivní
    #         raise ApiAksException500('Reader has no library')
    #     self.library_zk_sigla = self.library_zk_sigla.lower()
    #     self.library = getattr(self.portal.libraries, self.library_zk_sigla, None)
    #     if not self.library:
    #         # Nemělo by nastat, protože pak není aktivní
    #         raise ApiAksException500('Reader has invalid library')
    #     if full and not self.library.is_active:
    #         # Nemělo by nastat, protože pak není aktivní
    #         raise ApiAksException500('Reader has no library active in Ziskej')

    # def get_ticket(self, ticket_id, reader):
    #     """Get ticket by ticket_id and check ticket reader."""
    #     if not ticket_id:
    #         raise ApiAksException400('Missing ticket_id')
    #     ticket = getattr(self.portal.tickets, ticket_id, None)
    #     if not ticket:
    #         raise ApiAksException404('Ticket not found')
    #     if not (ticket.reader and ticket.reader == reader.username):
    #         raise ApiAksException400('Invalid reader for ticket')
    #     return ticket

    # def get_libraries(self, lower_case=False, service=None,
    #                   include_deactivated=False):
    #     """Vrací seznam sigel aktivních knihoven.  Aktivní knihovnou je v tomto
    #     smyslu jakákoli aktivní knihovna, která poskytuje službu service.

    #     V případě include_deactivated True vrátí všechny knihovny, které mají
    #     v rámci specifikované služby vytvořenou objednávku pomocí Získej API pro
    #     CPK pro čtenáře.

    #     lower_case False (default) .. Vrátit seznam sigel ve tvaru ABA013,
    #                                   BOA001, v rámci volání API
    #                                   api_aks_libraries_get.

    #     lower_case True .. Vrátit seznam sigel ve tvaru aba013, boa001, v rámci
    #                        interního volání, používá se pro určení, zda má
    #                        čtenář aktivní knihovnu.
    #     """

    #     if service not in SERVICE_LOV:
    #         service = SERVICE_LOV_DEFAULT  # mvszk

    #     if include_deactivated:
    #         # cache je potřeba, použít, je-li k dispozici
    #         if self.lib_helper.is_cpkapi_including_deactivated_cache():
    #             return self.lib_helper.get_cpkapi_including_deactivated_cache()

    #     query = dict(
    #         portal_type = 'Library',
    #         sort_on = 'sigla',
    #         Was_any_zk = True,
    #         )
    #     if not include_deactivated:
    #         query['review_state'] = 'published'
    #     brains = self.catalog_search(query)
    #     items = []
    #     for brain in brains:
    #         obj = brain._unrestrictedGetObject()
    #         if not brain.sigla:
    #             continue

    #         if not include_deactivated:
    #             # Knihovna je aktivní a poksytuje MVS jako ŽK
    #             if service == 'mvszk':
    #                 # Pokud má knihovna dnes i zítra dovolenou nebo je dnes méně
    #                 # než 1 h do konce provozní doby a zítra začíná dovolená,
    #                 # tak deaktivovat pro účely seznamu aktivních knihoven,
    #                 # který CPK používá pro určení, zda lze vytvořit novou
    #                 # objednávku.

    #                 # NTK-254 Do seznamu knihoven zahrnout nezávisle na
    #                 # dovolené.
    #                 # if not obj.Is_mvs_zk(
    #                 #     ignore_vacation=False, now_dt=self.testing_now_dt):
    #                 #     continue
    #                 if not obj.Is_mvs_zk(
    #                     ignore_vacation=True, now_dt=self.testing_now_dt):
    #                     continue
    #             # Knihovna je aktivní a poksytuje MVS jako DK
    #             elif service == 'mvsdk':
    #                 if not obj.Is_mvs_dk():
    #                     continue
    #             # Knihovna je aktivní a poksytuje MVS jako ŽK nebo DK
    #             elif service == 'mvs':
    #                 # NTK-254 Do seznamu knihoven zahrnout nezávisle na
    #                 # dovolené.
    #                 # if not obj.Is_mvs_zk(ignore_vacation=False,
    #                 #                      now_dt=self.testing_now_dt) and \
    #                 #         not obj.Is_mvs_dk():
    #                 #     continue
    #                 if not obj.Is_mvs_zk(ignore_vacation=True,
    #                                      now_dt=self.testing_now_dt) and \
    #                         not obj.Is_mvs_dk():
    #                     continue
    #         else:
    #             # Knihovna je aktivní a poksytuje MVS jako ŽK
    #             if service == 'mvszk':
    #                 if not obj.Was_mvs_zk():
    #                     continue
    #             # Knihovna je aktivní a poksytuje MVS jako DK
    #             elif service == 'mvsdk':
    #                 if not obj.Was_mvs_dk():
    #                     continue
    #             # Knihovna je aktivní a poksytuje MVS jako ŽK nebo DK
    #             elif service == 'mvs':
    #                 if not obj.Was_mvs_zk() and not obj.Was_mvs_dk():
    #                     continue

    #         if not lower_case:
    #             items.append(brain.sigla.upper())
    #         else:
    #             items.append(brain.sigla.lower())

    #     if include_deactivated:
    #         # aktualziovat cache
    #         self.lib_helper.set_cpkapi_including_deactivated_cache(items)

    #     return items

    # def reader_status(self, reader):
    #     data = dict(
    #         count_tickets=0,
    #         count_tickets_open=0,
    #         count_messages=0,
    #         count_messages_unread=0,
    #         )

    #     query = dict(
    #         portal_type='TicketMVS',
    #         reader=reader.getId(),
    #         sort_on='hid',
    #         sort_order='reverse',
    #         )
    #     brains = self.catalog_search(query)

    #     for brain in brains:
    #         ticket = brain.getObject()

    #         data['count_tickets'] += 1

    #         if ticket.is_open_for_reader:
    #             data['count_tickets_open'] += 1

    #         messages_counts = ticket.get_messages_counts()
    #         data['count_messages'] += messages_counts['count_messages']
    #         data['count_messages_unread'] += messages_counts['count_messages_unread']

    #     return data

    # def date_format(self, date):
    #     """Firmat date for json response.  Nothing to do now but this is the
    #     place where it could happen if needed in future.
    #     """

    #     return date

    # def fix_reader_library(self, reader):
    #     """Try to fix broken data format when reader.library exists and is not
    #     string.
    #     """

    #     print reader.library
    #     try:
    #         sigla = reader.library.sigla.lower()
    #         reader.library = sigla
    #         reader.reindexObject()
    #     except Exception as e:
    #         logger.exception('Try to fix broken data format when reader.library exists and is not string for reader {} failed: {}.'.format(reader.getId(), str(e)))
    #         return False
    #     logger.warning('Try to fix broken data format when reader.library exists and is not string for reader {} - success.'.format(reader.getId()))
    #     return True

    def add_historylog(self, message, who=None):
        """Zapíše do audit logu ticketu message zobrazitelnou pro operátora a
        pro who (list).
        """

        if not self.ticket:
            return
        user_id = self.user_id and self.user_id or u'?'
        library = self.library and self.library.sigla or u'?'
        message = u'{} (uživatel {}, knihovna {})'.format(message, user_id, library)
        if self.subticket:
            self.subticket.add_historylog(message, who=who)
        self.ticket.add_historylog(message, who=who)

    def send_notifications(self, nid, ticket=None, subticket=None):
        """Wrapper pro notifikace, pošle notifikaci s id nid.  Parametry ticket
        a subticket jsou tady záměrně ignorovány a místo nich se použijí objekty
        nastavené z kontextu.
        """

        ziskej_notifications = ZiskejNotifications(
            ticket=self.ticket, subticket=self.subticket)
        ziskej_notifications.send_notifications(nid)

    def error_notify(self, error_class_id, e, method_id, detail_error=None):
        # FIXME send emails etc.
        #message = '{error_class_id} in {self}.{method_id}: {e}'.format(error_class_id=error_class_id, e=str(e), method_id=method_id, self=self, )
        try:
            user_id = self.user_id
        except:
            user_id = '?'
        message = '{error_class_id} in {method_id} for user {user_id}: {e}'.format(
            error_class_id = error_class_id,
            e = str(e),
            method_id = method_id,
            user_id = user_id)
        if detail_error:
            message += ' [more info] ' + detail_error
        logger.warning('[error_notify] ' + message)

        error_notifications = ErrorNotifications()
        error_notifications.send_notifications(message)

    def bool_from_request(self, name):
        """Return True or False"""
        value = self.request.get(name, None)
        if isinstance(value, basestring):
            value = bool(value not in ('false', 'False', '0', ''))
        # print "bool_from_request for", name, "is", repr(bool(value)), "original is", repr(self.request.get(name, None))
        return bool(value)

    def bool_or_None_from_request(self, name):
        """Return True, False or None"""
        value = self.request.get(name, None)
        if value is None:
            pass
        elif isinstance(value, basestring):
            if value in ('false', 'False', '0', ):
                value = False
            elif value in ('true', 'True', '1', ):
                value = True
            else:
                raise ApiAksException400('Invalid value for parameter {}.  Valid values: true, false.'.format(name))
        else:
            raise ApiAksException400('Invalid value for parameter {}.  Valid values: true, false.'.format(name))
        # print "bool_or_None_from_request for", name, "is", repr(bool(value)), "original is", repr(self.request.get(name, None))
        return value

    def date_format_html(self, date_str, is_time=False, omit_year=False):
        return self.date_format(date_str, is_time=is_time, is_html=True, omit_year=omit_year)

    def date_format(self, date_str, is_time=False, is_html=False, omit_year=False):
        """Očekává string 2017-12-10 (pro 10. prosinec 2017) a vrací správně formátované
        """

        if not date_str:
            return u''
        #return DateTime(date_str).strftime('%-d.%-m.%Y')
        return format_date(DateTime(date_str), is_time=is_time, is_html=is_html, omit_year=omit_year)
