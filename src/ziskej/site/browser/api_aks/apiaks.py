# -*- coding: utf-8 -*-

import logging

from DateTime import DateTime

from ziskej.site.browser.api_aks.apiaks_base import (
    ApiAksException400,
    ApiAksException500,
    )
from ziskej.site.browser.api_aks.apiaks_common import ApiAksCommonView
from ziskej.site.library_payment_helpers import library_payment_mvs
from ziskej.site.ticket_helpers import (
    create_reportentry,
    wf_action,
    )
from ziskej.site.utils import datetime_add_d
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger('ziskej.apiaks')


class ApiAksView(ApiAksCommonView):

    action_ids = (
        # ReadOnly
        'zk_tickets', 'zk_ticket',
        'zk_subtickets', 'zk_subticket', 'zk_subticket_messages_get',
        'dk_subtickets', 'dk_subticket', 'dk_subticket_messages_get',
        # Write
        'register_callback',
        'zk_ticket_reader_info',
        'zk_ticket_transition',
        'zk_subticket_transition',
        'zk_subticket_messages_post', 'zk_subticket_pm_unread',
        'dk_subticket_dk_info',
        'dk_subticket_transition',
        'dk_subticket_messages_post', 'dk_subticket_pm_unread',
        )

    unauthorized_action_ids = []

    def zk_tickets(self):
        library_zk = self.library_by_request_sigla()
        query = dict(
            portal_type='TicketMVS',
            # FIXME ticket_type
            library_zk=library_zk.sigla.lower(),
            sort_on='hid',
            )
        status = self.ticket_status_by_request()
        closed_in = self.open_and_closed_in_by_request()
        is_open = self.is_is_open_by_request()
        if status is not None:
            if status in ('lent', 'returned', ):
                query['review_state'] = 'prepared'
            elif status in ('accepted', 'returned', ):
                query['review_state'] = 'prepared'
            else:
                query['review_state'] = status
        elif closed_in:
            pass
        # open
        elif is_open:
            query['review_state'] = [
                'created', 'assigned', 'prepared', 'pending', ]
        # closed
        elif not is_open and is_open is not None:
            query['review_state'] = ['closed', 'rejected', 'cancelled', ]
        brains = self.catalog(query)
        # print len(brains), "for", repr(query)
        is_expand = self.is_expand_by_request()
        tickets = []
        for brain in brains:
            # Zpracovat filter, který nelze zadat v rámci query
            if status in ('prepared', 'lent', 'returned', ):
                ticket = brain.getObject()
                status_apiaks = ticket.status_apiaks
                if status_apiaks['status'] == status:
                    continue
            elif closed_in:
                ticket = brain.getObject()
                if not ticket.is_open_or_closed_in_apiaks(closed_in):
                    continue
            # Přidat expanded / id
            if is_expand:
                tickets.append(self._ticket_detail(brain))
            else:
                tickets.append(brain.getId)
        return 200, dict(tickets=tickets)

    def zk_ticket(self):
        return 200, self._ticket_detail(self._get_ticket_brain())

    def zk_subtickets(self):
        return self._zkdk_subtickets_base('zk')

    def zk_subticket(self):
        brain = self._get_subticket_brain('zk')
        return 200, self._subticket_detail(brain, 'zk')

    def zk_subticket_messages_get(self):
        brain = self._get_subticket_brain('zk')
        subticket = brain.getObject()
        messages = subticket.get_messages(
            render_html=False, is_apiaks=True, unread_by='library_zk')
        return 200, dict(items=messages)

    def dk_subtickets(self):
        return self._zkdk_subtickets_base('dk')

    def dk_subticket(self):
        brain = self._get_subticket_brain('dk')
        return 200, self._subticket_detail(brain, 'dk')

    def dk_subticket_messages_get(self):
        brain = self._get_subticket_brain('dk')
        subticket = brain.getObject()
        messages = subticket.get_messages(
            render_html=False, is_apiaks=True, unread_by='library_dk')
        return 200, dict(items=messages)

    # Write

    def zk_ticket_reader_info(self):
        brain = self._get_ticket_brain()
        ticket = brain.getObject()
        subticket, twf, stwf = self._get_sub_wf_for_ticket(ticket)

        self.ticket = ticket
        self.subticket = subticket

        # Validovat stavy, že může být nastaveno, nastavit
        obj_dict = dict()
        reader_note_as_pm = False
        reader_fee_editable = False
        if twf == 'assigned' and stwf == 'sent':
            obj_dict['reader_from_date'] = self.date_from_request(
                'reader_from_date', result_type='iso')
            obj_dict['reader_to_date'] = self.date_from_request(
                'reader_to_date', result_type='iso')
            obj_dict['reader_note'] = self.text_from_request('reader_note')
            obj_dict['reader_place'] = self.text_from_request('reader_place')

            # Jen objednávky vytvořené knihovníkem a pro ŽK, která umožňuje
            # některým čtenářům poskytnout zdarma, je možné editovat částku pro
            # čtenáře
            if (not ticket.created_by or \
                    ticket.created_by == 'librarian') \
                    and self.library.mvs_zk_cena == 0:
                reader_fee_editable = True

        elif twf == 'prepared' and not ticket.is_reader_return:
            reader_note_as_pm = True
            obj_dict['reader_to_date'] = self.date_from_request(
                'reader_to_date', result_type='iso')
            obj_dict['reader_note'] = self.text_from_request('reader_note')

        else:
            raise ApiAksException400(
                'Cannot be used for ticket in this status.')

        # Nastav cenu pro čtenáře, je-li editovatelná.
        if reader_fee_editable:
            # Parametr is_reader_free je volitelný, není-li přítomné, považuje
            # se za False
            is_reader_free = bool(self.bool_from_request('is_reader_free'))
            if is_reader_free:
                obj_dict['reader_fee'] = 0
            else:
                obj_dict['reader_fee'] = 70

        # Validovat hodnoty
        if 'reader_to_date' in obj_dict and obj_dict['reader_to_date']:
            if 'reader_from_date' in obj_dict:
                reader_from_date = obj_dict['reader_from_date']
            else:
                reader_from_date = ticket.reader_from_date
            if reader_from_date and obj_dict['reader_to_date'] <= \
                    reader_from_date:
                raise ApiAksException400(
                    'Invalid reader_from_date and reader_to_date. '\
                    'reader_from_date is greater than reader_to_date.'
                    )

        if reader_note_as_pm:
            reader_note_original = ticket.reader_note
        is_updated = ticket.update_attrs_if_changed(**obj_dict)

        if is_updated:
            if reader_note_as_pm and ticket.reader_note and \
                    ticket.is_created_by_reader and \
                    reader_note_original != ticket.reader_note:
                # Poslat automatickou zprávu čtenáři, pokud je vytvořeno
                # čtenářem.
                reader_note_as_pm_message = u'''\
Knihovník Vaší knihovny přidal novou informaci ohledně Vaší objednávky {hid} díla {doc_title} ze dne {created_date}.

Poznámka knihovníka: {reader_note}
'''.format(
    hid=ticket.hid,
    doc_title=ticket.doc_title,
    created_date=self.date_format(ticket.created_date),
    reader_note=ticket.reader_note,  # není prázdné díky podmínce výše
    )
                ticket.add_pm_auto(reader_note_as_pm_message)

                print "reader_note_as_pm_message:", repr(reader_note_as_pm_message)

            # if 'reader_to_date' in obj_dict and obj_dict['reader_to_date']:
            #     message = u'Termíny byly nastaveny.'
            #     self.add_message(message)
            # unread
            ticket.unread_r = True

            if 'reader_fee' in obj_dict:
                self.add_historylog(u'Termíny pro čtenáře byly nastaveny nebo změněny: Cena pro čtenáře {}'.format(obj_dict['reader_fee']), who=['zk'])
            else:
                self.add_historylog(u'Termíny pro čtenáře byly nastaveny nebo změněny', who=['zk'])
            ticket.reindexObject()
            # Změněno
            logger.info('zk_ticket_reader_info updated')

        else:
            # Beze změny
            logger.info('zk_ticket_reader_info no change')

        return 204, None

    def zk_ticket_transition(self):
        brain = self._get_ticket_brain()
        ticket = brain.getObject()
        subticket, twf, stwf = self._get_sub_wf_for_ticket(ticket)

        self.ticket = ticket
        self.subticket = subticket

        transition_id_lov = ('prepare', 'lent', 'return', 'close', )
        transition_id = self.text_from_request(
            'transition_id', lov=transition_id_lov)
        transition_message = self.text_from_request('transition_message')

        if transition_id == 'prepare':
            if twf != 'assigned':
                raise ApiAksException400(
                    'Invalid value of parameter transition_id for ticket '
                    'in this state.')
            if not ticket.can_prepare:
                raise ApiAksException400(
                    'Ticket is not eligible for this transition.')
            wf_action(ticket, 'prepare', transition_message)
            ticket.unread_r = True
            self.add_historylog(u'Připraveno pro čtenáře.', who=['zk', 'r'])

            # Nemá smysl posílat čtenáři zprávu, pokud není čtenář
            if ticket.is_created_by_reader:
                reader_note_str = u''
                if ticket.reader_note:
                    reader_note_str = u'\nPoznámka knihovníka: {}'.format(
                        ticket.reader_note)
                # Cena za poskytnutou službu je {reader_fee} Kč.
                message = u'''\
Vaše objednávka č. {hid} na MVS ze dne {created_date} byla vyřízena.
Požadovaný dokument je k vyzvednutí od {reader_from_date} ve Vaší knihovně.
Místo k vyzvednutí: {reader_place}.
Termín vrácení je {reader_to_date}.{reader_note}
V případě potřeby kontaktujte svou knihovnu: {library_fullname}.
Děkujeme za využití služby ZÍSKEJ.
'''.format(
    hid=ticket.hid,
    created_date=self.date_format(ticket.created_date),
    reader_from_date=self.date_format(ticket.reader_from_date),
    reader_to_date=self.date_format(ticket.reader_to_date),
    # reader_fee=self.ticket.reader_fee,
    reader_place=ticket.reader_place,
    reader_note=reader_note_str,
    library_fullname=self.library.fullname,
    )
                ticket.add_pm_auto(message)

            self.send_notifications('wf_t_prepare')

            ticket.reindexObject()

        elif transition_id == 'lent':
            if twf != 'prepared' or ticket.is_reader_lent:
                raise ApiAksException400(
                    'Invalid value of parameter transition_id for ticket '
                    'in this state.')
            ticket.reader_lent_date = self.dt_as(DateTime(), 'iso')
            self.add_historylog(u'Půjčeno čtenáři.', who=['zk', 'r'])
            ticket.reindexObject()

        elif transition_id == 'return':
            if twf != 'prepared' or not ticket.is_reader_lent or \
                    ticket.is_reader_return:
                raise ApiAksException400(
                    'Invalid value of parameter transition_id for ticket '
                    'in this state.')
            ticket.return_date = self.dt_as(DateTime(), 'iso')
            self.add_historylog(u'Vráceno čtenářem.', who=['zk', 'r'])
            ticket.reindexObject()

        elif transition_id == 'close':
            if twf != 'prepared' or ticket.is_reader_lent:
                raise ApiAksException400(
                    'Invalid value of parameter transition_id for ticket '
                    'in this state.')
            if not ticket.can_close:
                raise ApiAksException400(
                    'Ticket is not eligible for this transition.')
            wf_action(self.ticket, 'close', transition_message)
            ticket.unread_r = True
            self.add_historylog(u'<strong>Objednávka byla předčasně uzavřena, </strong> čtenář si dokument nevyzvedl.', who=['zk'])
            ticket.reindexObject()

        else:
            raise ApiAksException400(
                'Invalid value of parameter transition_id.')

        return 204, None

    def zk_subticket_transition(self):
        brain = self._get_subticket_brain('zk')
        subticket = brain.getObject()
        ticket = subticket.ticket
        twf = ticket.wf_review_state
        stwf = subticket.wf_review_state

        self.ticket = ticket
        self.subticket = subticket

        transition_id_lov = ('send_back', )
        transition_id = self.text_from_request(
            'transition_id', lov=transition_id_lov)
        transition_message = self.text_from_request('transition_message')

        if transition_id == 'send_back':
            if not subticket.can_send_back:
                raise ApiAksException400(
                    'Subticket is not eligible for this transition.')
            wf_action(subticket, 'send_back', transition_message)
            subticket.sent_back_date = self.dt_as(DateTime(), 'iso')
            subticket.set_next_action()
            subticket.unread_dk = True
            self.add_historylog(u'Dokument byl poslán zpět dožádané knihovně.', who=['zk', 'dk'])
            self.send_notifications('wf_st_send_back')
            ticket.reindexObject()
            subticket.reindexObject()

        else:
            raise ApiAksException400(
                'Invalid value of parameter transition_id.')

        return 204, None

    def zk_subticket_messages_post(self):
        brain = self._get_subticket_brain('zk')
        subticket = brain.getObject()
        ticket = subticket.ticket

        text = self.text_from_request('text')

        if not text or not text.strip():
            raise ApiAksException400('Missing text.')

        if '\r' in text:
            raise ApiAksException400(
                'Invalid end lines. Only unix end lines are allowed.')

        text = text.strip()
        subticket.add_message_txt('library_zk', text)

        ticket.reindexObject()
        subticket.reindexObject()

        return 204, None

    def zk_subticket_pm_unread(self):
        brain = self._get_subticket_brain('zk')
        subticket = brain.getObject()
        ticket = subticket.ticket

        unread = self.bool_from_request('unread')

        if unread:
            raise ApiAksException400('Invalid value for parameter unread.')

        if subticket.pm_unread != 'library_zk':
            logger.info('zk_subticket_pm_unread nothing to do')
            return 204, None

        subticket.pm_unread = ''
        subticket.pm_date = None

        ticket.reindexObject()
        subticket.reindexObject()

        logger.info('zk_subticket_pm_unread done')

        return 204, None

    def dk_subticket_dk_info(self):
        brain = self._get_subticket_brain('dk')
        subticket = brain.getObject()
        ticket = subticket.ticket
        twf = ticket.wf_review_state
        stwf = subticket.wf_review_state

        self.ticket = ticket
        self.subticket = subticket

        if stwf not in ('accepted', 'conditionally_accepted', ):
            raise ApiAksException400(
                'Subticket state does not allow this action.')

        obj_dict = dict()

        obj_dict['back_date'] = self.date_from_request(
            'back_date', result_type='iso')
        # pokud bylo podmínečně přijato se zkrácenou výpůjční
        # lhůtou, tak použij počet dnů z ní, jinak standard (28)
        st_return_min_days = ziskej_parameter('st_return_min_days')  # 28
        if subticket.cond_a_return_d:
            st_return_min_days = subticket.cond_a_return_d
        back_date_min = datetime_add_d(
            st_return_min_days
            ).strftime('%Y-%m-%d')
        if obj_dict['back_date'] < back_date_min:
            raise ApiAksException400(
                'Invalid back_date value. '\
                'Cannot be less than {} days.'.format(st_return_min_days))

        # FIXME
        # cena_mvs_default = library_dk.Mvs_dk_cena()  # 70
        cena_mvs_default = 70
        cena_mvs_min = cena_mvs_default
        cena_mvs_max = cena_mvs_default
        if subticket.cond_a_fee is not None and \
                subticket.cond_a_fee > cena_mvs_max:
            cena_mvs_max = subticket.cond_a_fee
        cena = self.int_from_request('fee')
        if cena is not None:
            if cena > cena_mvs_max:
                raise ApiAksException400(
                    'Invalid value for fee. '\
                    'Cannot be more than {}.'.format(cena_mvs_max))
            if cena < cena_mvs_min:
                raise ApiAksException400(
                    'Invalid value for fee. '\
                    'Cannot be less than {}.'.format(cena_mvs_min))
            obj_dict['cena'] = cena
        else:
            obj_dict['cena'] = cena_mvs_max

        obj_dict['doc_dk_id'] = self.text_from_request('doc_dk_id')
        obj_dict['rfid'] = self.text_from_request('rfid')
        obj_dict['barcode'] = self.text_from_request('barcode')

        is_updated = subticket.update_attrs_if_changed(**obj_dict)

        if is_updated:
            del obj_dict['back_date']
            ticket.update_attrs_if_changed(**obj_dict)

            ticket.unread_zk = True

            self.add_historylog(
                u'Informace o požadavku byly doplněny. Aktuálně platné údaje jsou: cena <strong>{}</strong> Kč, požadováno zpět <strong>{}</strong>.'.format(
                    subticket.cena,
                    self.date_format_html(subticket.back_date),
                    ),
                who=['dk'])

            ticket.reindexObject()
            subticket.reindexObject()

        if is_updated:
            logger.info('dk_subticket_dk_info done')
        else:
            logger.info('dk_subticket_dk_info nothing to do')

        return 204, None

    def dk_subticket_transition(self):
        brain = self._get_subticket_brain('dk')
        subticket = brain.getObject()
        ticket = subticket.ticket
        twf = ticket.wf_review_state
        stwf = subticket.wf_review_state

        self.ticket = ticket
        self.subticket = subticket

        transition_id_lov = ('accept', 'refuse', 'send', 'close', )
        transition_id = self.text_from_request(
            'transition_id', lov=transition_id_lov)
        transition_message = self.text_from_request('transition_message')

        if transition_id == 'accept':
            if stwf != 'queued':
                raise ApiAksException400(
                    'Invalid value of parameter transition_id for subticket '
                    'in this state.')
            if not subticket.can_accept:
                raise ApiAksException400(
                    'Subticket is not eligible for this transition.')
            wf_action(subticket, 'accept', transition_message)
            now_iso = self.dt_as(DateTime(), 'iso')
            subticket.decided_date = now_iso
            subticket.accepted_date = now_iso
            subticket.set_next_action()
            ticket.unread_zk = True
            self.add_historylog(u'Požadavek přijat.', who=['zk', 'dk'])
            self.send_notifications('wf_st_accept')
            ticket.reindexObject()
            subticket.reindexObject()

        elif transition_id == 'refuse':
            if stwf not in ('queued', 'conditionally_accepted', 'accepted', ):
                raise ApiAksException400(
                    'Invalid value of parameter transition_id for subticket '
                    'in this state.')
            if not subticket.can_refuse:
                raise ApiAksException400(
                    'Subticket is not eligible for this transition.')
            refuse_reason_ids = (
                'refuse-reason-wrong-citation', 'refuse-reason-dk-impossible',
                'refuse-reason-dk-unavailable', )
            refuse_reason = self.text_from_request(
                'refuse_reason', lov=refuse_reason_ids)
            if not refuse_reason:
                raise ApiAksException400(
                    'Invalid value of parameter refuse_reason.')

            subticket.refuse_reason = refuse_reason

            wf_action(subticket, 'refuse', u'{} {}'.format(
                refuse_reason, transition_message))

            subticket.set_next_action()
            if stwf == 'queued':
                subticket.decided_date = self.dt_as(DateTime(), 'iso')

            ticket.unread_zk = True
            if stwf in ('queued', ):
                self.send_notifications('wf_st_refuse')
                message = u'Odmítnuto knihovníkem DK (po předchozím podmínečném přijetí).'
            elif stwf in ('conditionally_accepted', ):
                self.send_notifications('wf_st_refuse')
                message = u'Odmítnuto knihovníkem DK.'
            elif stwf in ('accepted', ):
                self.send_notifications('wf_st_accepted_refuse')
                message = u'Odmítnuto knihovníkem DK (po předchozím přijetí).'
            else:
                raise ApiAksException500('Invalid state: {}'.format(wf_state))
            REFUSE_REASON = {
                u'refuse-reason-wrong-citation': u'chybná citace a nepodaří se dohledat správně',
                u'refuse-reason-dk-impossible': u'nelze realizovat',
                u'refuse-reason-dk-unavailable': u'momentálně nedostupné',
                }
            refuse_reason_label = REFUSE_REASON.get(refuse_reason, refuse_reason)
            message = u'{message}  Důvod odmítnutí: {reason}.'.format(message=message, reason=refuse_reason_label)
            self.add_historylog(message, who=['zk', 'dk'])

            subticket.reindexObject()

            # -- vytvořit nový subticket, assign_new je True, pokud se
            # to podařilo, self.subticket se v tom případě změní, jinak
            # zůstane beze změny
            old_subticket = subticket

            if refuse_reason in (u'refuse-reason-wrong-citation', ):
                assign_new = self.try_next_request('stop')
            else:
                assign_new = self.try_next_request('assign')

            # -- nový subticket, pokud se ho podařilo vytvořit
            subticket = self.subticket
            if assign_new:
                subticket.unread_dk = True
                if wf_state in ('queued', 'conditionally_accepted', ):
                    self.send_notifications('wf_st_refuse_new')
                elif wf_state in ('accepted', ):
                    self.send_notifications('wf_st_accepted_refuse_new')
                subticket.reindexObject()

            ticket.reindexObject()

        elif transition_id == 'send':
            if stwf != 'accepted':
                raise ApiAksException400(
                    'Invalid value of parameter transition_id for subticket '
                    'in this state.')
            if not subticket.can_send:
                raise ApiAksException400(
                    'Subticket is not eligible for this transition.')

            wf_action(self.subticket, 'send', transition_message)

            sent_date_dt = DateTime()
            subticket.sent_date = self.dt_as(sent_date_dt, 'iso')

            # FIXME-EDD
            # create report entry
            obj_dict = dict(
                report_date=sent_date_dt,
                cena=subticket.cena,
                user_id=self.user_id,
                entry_type=u'std',
                note=u'',
                )
            report_entry = create_reportentry(subticket, obj_dict)
            # report_date je DateTime - např.: DateTime('2017-11-15')
            subticket.report_date = sent_date_dt  # obsolete now

            # pokud bylo podmínečně přijato se zkrácenou výpůjční
            # lhůtou, tak použij počet dnů z ní, přestože by DK
            # nějaké datum vyplnila
            if subticket.cond_a_return_d:
                subticket.back_date = datetime_add_d(
                    subticket.cond_a_return_d).strftime('%Y-%m-%d')

            # jinak pokud DK nevyplní, použije se teď + 28d
            elif not subticket.back_date:
                subticket.back_date = datetime_add_d(
                    ziskej_parameter('st_return_default_days')
                    ).strftime('%Y-%m-%d')

            subticket.set_next_action(
                na_type='date_iso',
                na_value=subticket.back_date,
                na_text=u'Vrácení DK'
                )

            ticket.unread_zk = True

            if subticket.doc_sent == u'sent':
                self.send_notifications('wf_st_send')
            elif subticket.doc_sent == u'pick':
                self.send_notifications('wf_st_send_here')
            else:
                raise ApiAksException500('Missing or invalid info doc_sent.')

            self.add_historylog(
                u'DK odeslala / připravila požadavek.', who=['zk', 'dk'])

            library_payment_mvs(subticket, report_entry)

            ticket.reindexObject()
            subticket.reindexObject()

        elif transition_id == 'close':
            # FIXME ověřit, možná příliš restriktivní
            if stwf != 'sent_back':
                raise ApiAksException400(
                    'Invalid value of parameter transition_id for subticket '
                    'in this state.')
            if not subticket.can_close:
                raise ApiAksException400(
                    'Subticket is not eligible for this transition.')

            # příznak, zda se jedná o hlavní požadavek objednávky a objednávka
            # není uzavřená, protože na tuto podmínky jsou vázány další akce
            is_main_subticket = bool(
                (subticket.getId() == ticket.subticket_id) and
                ticket.can_close
                )

            wf_action(subticket, 'close', transition_message)
            now_iso = self.dt_as(DateTime(), 'iso')
            subticket.closed_date = now_iso
            # FIXME není to jen pro is_main_subticket?
            ticket.closed_date = now_iso

            # ticket close -> closed END
            if is_main_subticket:
                wf_action(ticket, 'close', u'Automaticky uzavřeno vrácením dožádané knihovně.')

            ticket.unread_zk = True
            if is_main_subticket:
                ticket.unread_r = True
            self.add_historylog(u'V pořádku vráceno zpět dožádané knihovně.', who=['zk', 'dk'])
            if is_main_subticket:
                self.add_historylog(u'<strong>Objednávka byla uzavřena</strong> uzavřením hlavního požadavku', who=['zk'])
            
            if is_main_subticket:
                self.send_notifications('wf_st_accept_back_main')
            else:
                self.send_notifications('wf_st_accept_back_other')

            ticket.reindexObject()
            subticket.reindexObject()

        else:
            raise ApiAksException400(
                'Invalid value of parameter transition_id.')

        return 204, None

    def dk_subticket_messages_post(self):
        brain = self._get_subticket_brain('dk')
        subticket = brain.getObject()
        ticket = subticket.ticket

        text = self.text_from_request('text')

        if not text or not text.strip():
            raise ApiAksException400('Missing text.')

        if '\r' in text:
            raise ApiAksException400(
                'Invalid end lines, please use unix end lines.')

        text = text.strip()
        subticket.add_message_txt('library_dk', text)

        ticket.reindexObject()
        subticket.reindexObject()

        return 204, None

    def dk_subticket_pm_unread(self):
        brain = self._get_subticket_brain('dk')
        subticket = brain.getObject()
        ticket = subticket.ticket

        unread = self.bool_from_request('unread')

        if unread:
            raise ApiAksException400('Invalid value for parameter unread.')

        if subticket.pm_unread != 'library_dk':
            logger.info('dk_subticket_pm_unread nothing to do')
            return 204, None

        subticket.pm_unread = ''
        subticket.pm_date = None

        ticket.reindexObject()
        subticket.reindexObject()

        logger.info('dk_subticket_pm_unread done')

        return 204, None

    def register_callback(self):
        library = self.library_by_request_sigla()
        callback_ids = (
            'event_reader_ticket_changed', 'event_zk_subticket_changed',
            'event_dk_subticket_changed', )
        callback_id = self.text_from_request('callback_id', lov=callback_ids)
        if not callback_id:
            raise ApiAksException400(
                'Invalid value for parameter callback_id.')
        callback_url = self.text_from_request('url')
        callback_url_placeholders = self._get_callback_placeholders(
            callback_url)
        callback_http_list = ('GET', 'POST', 'PUT', )
        callback_http = self.text_from_request('http', lov=callback_http_list)
        if not callback_http:
            raise ApiAksException400(
                'Missing or invalid value for parameter http. '\
                'Use GET, POST, or PUT.')

        if callback_id == 'event_reader_ticket_changed':
            valid_placeholders = (
                'eppn', 'reader_id', 'ticket_id', 'doc_dk_id', 'rfid',
                'barcode', )
        elif callback_id == 'event_zk_subticket_changed':
            valid_placeholders = (
                'eppn', 'reader_id', 'ticket_id', 'subticket_id', 'doc_dk_id',
                'rfid', 'barcode', )
        elif callback_id == 'event_dk_subticket_changed':
            valid_placeholders = (
                'subticket_id', 'doc_dk_id', 'rfid', 'barcode', )
        for item in callback_url_placeholders:
            if item not in valid_placeholders:
                raise ApiAksException400(
                    'Invalid placeholder in url for this callback.')

        if callback_id == 'event_reader_ticket_changed':
            library.callback_url_r_t = callback_url
            library.callback_http_r_t = callback_http
        elif callback_id == 'event_zk_subticket_changed':
            library.callback_url_zk_st = callback_url
            library.callback_http_zk_st = callback_http
        elif callback_id == 'event_dk_subticket_changed':
            library.callback_url_dk_st = callback_url
            library.callback_http_dk_st = callback_http

        return 204, None
