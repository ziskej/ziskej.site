# -*- coding: utf-8 -*-

import logging
from pprint import pprint

from DateTime import DateTime

from ziskej.site.browser.api_aks.apiaks_base import (
    ApiAksBaseView,
    ApiAksException404,
    ApiAksException400,
    ApiAksException500,
    )
from ziskej.site.ticket_helpers import wf_action
from ziskej.site.ticket_load_balancing import load_balancing_dk_list


logger = logging.getLogger('ziskej.apiaks')

DEBUG = False


class ApiAksCommonView(ApiAksBaseView):

    def library_by_request_sigla(self):
        sigla = self.text_from_request('sigla')
        if sigla is None:
            raise ApiAksException400('Missing sigla.')
        sigla = sigla.lower()
        library = getattr(self.portal.libraries, sigla, None)
        if not library:
            raise ApiAksException400('Invalid sigla.')
        # FIXME
        # if not self.library.is_apiaks:
        #     raise ApiAksException400('API AKS is not active for this library.')
        self.library = library
        return library

    def ticket_id_by_request(self):
        value = self.text_from_request('ticket_id')
        if not value:
            raise ApiAksException400('Missing ticket_id.')
        return value

    def subticket_id_by_request(self):
        value = self.text_from_request('subticket_id')
        if not value:
            raise ApiAksException400('Missing subticket_id.')
        return value

    def is_expand_by_request(self):
        value = self.text_from_request('expand', ['detail', ])
        return bool(value)

    def is_is_open_by_request(self):
        """Return True, False or None"""
        value = self.bool_or_None_from_request('is_open')
        return value

    def open_and_closed_in_by_request(self):
        return self.int_from_request('open_and_closed_in')

    def ticket_status_by_request(self):
        lov = ['created', 'assigned', 'pending', 'prepared', 'lent', 'returned',
               'closed', ]
        return self.text_from_request('status', lov)

    def subticket_status_by_request(self):
        lov = ['queued', 'conditionally_accepted', 'accepted', 'sent',
               'sent_back', 'closed', 'cancelled', 'refused', ]
        return self.text_from_request('status', lov)

    def _ticket_detail(self, brain):
        ticket = brain.getObject()
        item = dict()
        item['ticket_id'] = ticket.getId()
        item['ticket_type'] = ticket.ticket_type
        item['hid'] = ticket.hid
        item['reader_id'] = ticket.reader
        item['ticket_url'] = ticket.absolute_url()
        item['bibliographic_reference'] = ticket.doc_fullname
        item['doc_id'] = ticket.doc_id
        item['doc_dk_id'] = ticket.doc_dk_id
        item['rfid'] = ticket.rfid
        item['barcode'] = ticket.barcode
        # FIXME status_reader_cpkapi dělá korekce, které tady nechceme,
        # zejména accepted (to tady vůbec neznáme) a za accepted považuje
        # i situace, kdy je prepared, ale ještě nenastal den k vyzvednutí
        status_apiaks = ticket.status_apiaks
        item['status'] = status_apiaks['status']
        item['status_label'] = status_apiaks['status_label']
        item['is_open'] = status_apiaks['is_open']
        item['subticket_id'] = ticket.subticket_id
        item['subticket_ids'] = ticket.subticket_ids
        item['reader_from_date'] = ticket.reader_from_date
        item['reader_to_date'] = ticket.reader_to_date
        item['reader_note'] = ticket.reader_note
        item['reader_place'] = ticket.reader_place
        item['created_datetime'] = ticket.created_iso
        item['updated_datetime'] = ticket.modified_iso
        return item

    def _subticket_detail(self, brain, zkdk):
        subticket = brain.getObject()
        item = dict()
        item['ticket_id'] = subticket.ticket_id
        item['subticket_id'] = subticket.getId()
        item['subticket_type'] = subticket.ticket_type
        item['hid'] = subticket.hid
        item['sigla_dk'] = subticket.library_dk.lower()
        item['subticket_url'] = subticket.absolute_url()
        item['bibliographic_reference'] = subticket.doc_fullname
        item['doc_id'] = subticket.doc_id
        item['doc_dk_id'] = subticket.doc_dk_id
        item['rfid'] = subticket.rfid
        item['barcode'] = subticket.barcode
        status_apiaks = subticket.status_apiaks
        item['status'] = status_apiaks['status']
        item['status_label'] = status_apiaks['status_label']
        item['is_open'] = status_apiaks['is_open']
        item['is_snail_mail'] = bool(subticket.doc_sent != 'pick')
        item['accepted_date'] = subticket.accepted_date
        item['sent_date'] = subticket.sent_date
        item['back_date'] = subticket.back_date
        item['sent_back_date'] = subticket.sent_back_date
        item['closed_date'] = subticket.closed_date
        messages_counts = subticket.get_messages_counts(unread_by='library_zk')
        item['count_messages'] = messages_counts['count_messages']
        item['count_messages_unread'] = messages_counts['count_messages_unread']
        item['created_datetime'] = subticket.created_iso
        item['updated_datetime'] = subticket.modified_iso
        return item

    def _get_ticket_brain(self):
        library = self.library_by_request_sigla()
        ticket_id = self.ticket_id_by_request()
        query = dict(
            portal_type='TicketMVS',
            # FIXME ticket_type
            library_zk=library.sigla.lower(),
            getId=ticket_id,
            )
        brains = self.catalog(query)
        if len(brains) == 0:
            raise ApiAksException404('Ticket not found.')
        return brains[0]

    def _zkdk_subtickets_base(self, zkdk):
        library = self.library_by_request_sigla()
        query = dict(
            portal_type='SubticketMVS',
            # FIXME subticket_type
            sort_on='hid',
            )
        if zkdk == 'zk':
            query['library_zk'] = library.sigla.lower()
        elif zkdk == 'dk':
            query['library_dk'] = library.sigla.lower()
        status = self.subticket_status_by_request()
        closed_in = self.open_and_closed_in_by_request()
        is_open = self.is_is_open_by_request()
        if status is not None:
            query['review_state'] = status
        elif closed_in:
            pass
        # open
        elif is_open:
            query['review_state'] = [
                'queued', 'conditionally_accepted', 'accepted', 'sent',
                'sent_back', ]
        # closed
        elif not is_open and is_open is not None:
            query['review_state'] = ['closed', 'cancelled', 'refused', ]
        brains = self.catalog(query)
        # print len(brains), "for", repr(query)
        is_expand = self.is_expand_by_request()
        subtickets = []
        for brain in brains:
            # Zpracovat filter, který nelze zadat v rámci query
            if closed_in:
                subticket = brain.getObject()
                if not subticket.is_open_or_closed_in_apiaks(closed_in):
                    continue
            # Přidat expanded / id
            if is_expand:
                subtickets.append(self._subticket_detail(brain, 'zk'))
            else:
                subtickets.append(brain.getId)
        return 200, dict(subtickets=subtickets)

    def _get_subticket_brain(self, zkdk):
        library = self.library_by_request_sigla()
        subticket_id = self.subticket_id_by_request()
        query = dict(
            portal_type='SubticketMVS',
            # FIXME ticket_type
            getId=subticket_id,
            )
        if zkdk == 'zk':
            query['library_zk'] = library.sigla.lower()
        elif zkdk == 'dk':
            query['library_dk'] = library.sigla.lower()
        brains = self.catalog(query)
        if len(brains) == 0:
            raise ApiAksException404('Subticket not found.')
        return brains[0]

    def _get_sub_wf_for_ticket(self, ticket, subticket_id=None):
        if not ticket.is_mvs:
            raise ApiAksException400('Unsopported ticket type.')

        # 'created', 'assigned', 'pending', 'prepared', 'closed', 'cancelled', 'rejected'
        twf = ticket.wf_review_state

        if subticket_id is None:
            subticket_id = ticket.subticket_id

        stwf = None
        subticket = None
        if subticket_id:
            subticket = getattr(ticket, subticket_id, None)
        if subticket:
            # 'queued', 'conditionally_accepted', 'accepted', 'sent', 'sent_back', 'closed', 'cancelled', 'refused'
            stwf = subticket.wf_review_state

        return subticket, twf, stwf

    def _get_callback_placeholders(self, url):
        values = []
        idx = 0
        while url.find('{', idx) != -1:
            idx0 = url.find('{', idx)
            if idx0 == -1:
                break
            idx1 = url.find('}', idx0)
            if idx1 == -1:
                raise ApiAksException400(
                    'Invalid placeholder syntax in url. Missing }.')
            value = url[idx0+1, idx1]
            values.append(value)
            idx = idx1 + 1
        return values

    def try_next_request(self, assign_or_stop):
        """Voláno pro ukončení požadavku v různých situacích.

        Zabezpečuje posuny v rámci doc_order, ukládání do doc_order_blacklist a
        deaktivaci subticketu (tzn. že ho nadřazený ticket nepovažuje za
        aktivní).

        Pomocí parametru assign_or_stop provede příslušné kroky - vytvoří a
        přiřadí další subticket, pokud je to možné - pro 'assign'.  Nebo
        zastaví pro 'stop'. Tento parametr zohledňuje důvod odmítnutí
        a zohlednit, kdo je právě teď - pro ZK chceme, aby se zastavilo a mohl
        něco udělat, automaticky zkoušíme jen pro DK.
        """

        if assign_or_stop not in ('assign', 'stop', 'cancel', 'cancel_assign'):
            raise ApiAksException500('Internal error.')

        ticket = self.ticket
        subticket = self.subticket

        if subticket is None:
            logger.error('ticket {hid} try_next_request missing subticket'.format(hid=ticket.hid))
            raise ApiAksException500('Internal error.')

        # To by molo nastat při práci s malou reklamací po odpojení ticketu,
        # ale v těchto situacích by vůbec nemělo dojít k volání
        # try_next_request
        if self.subticket.getId() != self.ticket.subticket_id:
            raise ApiAksException500('Internal error.')

        # Manual LB - nelze auto
        if ticket.is_lb_manual:
            ticket.set_lb_state(u'manual library closed')
            ticket.deactivate_subticket()
            if assign_or_stop in ('assign', 'stop', 'cancel_assign', ):
                wf_action(ticket, 'stop', u'Je potřeba nastavit pořadí pro další požadavky.')
            ticket._p_changed = True
            return False

        # No DK - nelze auto
        if ticket.is_lb_nodk:
            if assign_or_stop in ('assign', 'stop', 'cancel_assign', ):
                wf_action(ticket, 'stop', u'Je potřeba nastavit pořadí pro další požadavky.')
            ticket._p_changed = True
            return False

        # Auto LB

        if not ticket.is_lb_auto:
            logger.error('ticket {hid} try_next_request invalid lb'.format(hid=ticket.hid))
            raise ApiAksException500('Internal error.')

        if not subticket.doc_id:
            logger.error('ticket {hid} try_next_request subticket missing doc_id'.format(hid=ticket.hid))
            raise ApiAksException500('Internal error.')

        # doc_order
        doc_order_list = []
        doc_order_sent_list = []
        if ticket.doc_order:
            doc_order_list = ticket.doc_order.split(',')
        if ticket.doc_order_sent:
            doc_order_sent_list = ticket.doc_order_sent.split(',')
        remove_first_item = True
        if subticket.doc_id not in doc_order_list:
            logger.warning('ticket {hid} try_next_request doc_id not in doc_order'.format(hid=ticket.hid))
            remove_first_item = False
            #raise ApiAksException500('Internal error.')
        if not len(doc_order_list):
            logger.warning('ticket {hid} try_next_request doc_order empty'.format(hid=ticket.hid))
            remove_first_item = False
            #raise ApiAksException500('Internal error.')
        if remove_first_item and subticket.doc_id != doc_order_list[0]:
            logger.warning('ticket {hid} try_next_request subticket is not active doc_id in doc_order'.format(hid=ticket.hid))
            remove_first_item = False
            #raise ApiAksException500('Internal error.')

        if remove_first_item:
            # doc_order odstranit první prvek - posledně použité doc_id, tím se
            # další pořadí aktualizuje dostatečně
            doc_order_list = doc_order_list[1:]
            doc_order_sent_list = doc_order_sent_list[1:]
            ticket.doc_order = ','.join(doc_order_list)
            ticket.doc_order_sent = ','.join(doc_order_sent_list)

        # aktualizace blacklistu pro Auto LB a to i po případném dalším načtení
        # dostupnosti
        doc_order_blacklist_list = []
        if ticket.doc_order_blacklist:
            doc_order_blacklist_list = ticket.doc_order_blacklist.split(',')
        if not doc_order_blacklist_list:
            doc_order_blacklist_list = []
        # FIXME-WORKFLOW tady možní blacklist pomocí sigel a ne pomocí doc_id
        if subticket.doc_id not in doc_order_blacklist_list:
            doc_order_blacklist_list.append(subticket.doc_id)
            ticket.doc_order_blacklist = ','.join(doc_order_blacklist_list)

        ticket.set_lb_state(u'auto lb closed')
        ticket.deactivate_subticket()

        if assign_or_stop == 'cancel':
            # cancel is done in complaint_cancel
            ticket._p_changed = True
            return False

        # nyní pro asign zkusíme vytvořit nový subticket, pokud nelze, tak
        # fallback na stop
        if assign_or_stop in ('assign', 'cancel_assign', ):
            if not len(doc_order_list) or not ticket.can_assign:
                assign_or_stop = 'stop'

        # assign or stop
        if assign_or_stop in ('assign', 'cancel_assign', ):
            wf_action(ticket, 'assign', u'Automatický přechod.')
            new_subticket = self.create_subticket()
            # Mohlo mezitím dojít k zneplatnění položky v dk, např.
            # k deaktivaci knihovny, i zde fallback na stop.
            if not new_subticket:
                assign_or_stop = 'stop'
        if assign_or_stop in ('assign', 'cancel_assign', ):
            pass
        elif assign_or_stop == 'stop':
            wf_action(ticket, 'stop', u'Je potřeba nastavit pořadí pro další požadavky.')
            ticket.subticket_id = u''
        else:
            raise ApiAksException500('Internal error.')

        ticket._p_changed = True
        return bool(assign_or_stop in ('assign', 'cancel_assign', ))

    def create_subticket(self):
        """Vytvoří nový subticket, vrátí jej, případně None, pokud vytvořit
        nový subticket nelze.

        Pro AutoLB se používá doc_order.  Postupně se pokusí vytvořit subticket
        na základě první položky v doc_order, pokud nelze, zneplatní ji (smaže)
        a pokračuje případně dál až do vyčerpání všech položek v doc_order,
        v takovém případě vrátí None.

        Pro ManualLB také může nastat situace, že zvolená DK už není aktivní,
        v tom případě obdobně vrátí None.
        """

        ticket = self.ticket
        if not ticket.doc_order and not ticket.lb_manual_library:
            raise ApiAksException500('Missing doc_order and lb_manual_library.')

        # Manual LB
        if ticket.is_lb_manual:
            sigla_dk = ticket.lb_manual_library
            if not sigla_dk:
                raise ApiAksException500('Internal error.')
            library_dk = getattr(self.portal.libraries, sigla_dk, None)
            if not library_dk or not library_dk.is_active:
                # Zvolená DK už není aktivní, v tom případě vrátit None.
                ticket.unread_r = True
                ticket._p_changed = True
                ticket.reindexObject()
                return None
            library_dk_title = library_dk.Title()
            dk_info = dict(
                id = u'',
                title = ticket.doc_title,
                fullname = ticket.doc_fullname,
                #doc_data = u''
                )
            doc_sent = ticket.lb_manual_library_sent
            if doc_sent not in (u'sent', u'pick', ):
                doc_sent = u'sent'
            ticket.set_lb_state(u'manual library created')

        # Auto LB based on doc_order
        else:

            # Najít první validní záznam v doc_order.
            dk_list = None
            while ticket.doc_order:
                doc_order = ticket.doc_order.split(',')
                if not doc_order:
                    break

                doc_order_sent_list = []
                if ticket.doc_order_sent:
                    doc_order_sent_list = ticket.doc_order_sent.split(',')

                # doc_order je nyní vždy aktuální a případné doc_id, které se
                # nemá použít, je z něj odstraněno při zpracování subticketu,
                # kdy je jasné, že nebude úspěšný (odmítnuto etc.)
                doc_id = doc_order[0]
                doc_sent = u'sent'
                if doc_order_sent_list:
                    doc_sent = doc_order_sent_list[0]

                # Používá Auto LB pouze pro parsování předchozích výsledků
                # uložených v ticket.  Žádné nové volání CPK.
                dk_list = load_balancing_dk_list(self.portal,
                                                 ticket,
                                                 restrict_doc_ids=[doc_id, ])

                # Nalezen validní záznam, zpracovat a případně ukončit hledání
                if dk_list:
                    dk_info = dk_list[0]

                    sigla_dk = dk_info['sigla'].lower()
                    library_dk = getattr(self.portal.libraries, sigla_dk, None)
                    # Vše OK, ukončit hledání
                    if library_dk and library_dk.is_active:
                        break
                    # Jinak hledat dál...
                    dk_list = None

                # Nenalezen validní záznam,  astane např. pokud je příslušná DK
                # mezitím deaktivována, tuto položku proto odstranit
                # z doc_order, ale nedávat ji do blacklistu.
                if len(doc_order) > 1:
                    ticket.doc_order = u','.join(doc_order[1:])
                else:
                    ticket.doc_order = u''
                if ticket.doc_order_sent:
                    if len(doc_order_sent_list) > 1:
                        ticket.doc_order_sent = u','.join(
                            doc_order_sent_list[1:])
                    else:
                        ticket.doc_order_sent = u''

            # AutoLB zastaven, vrátit None.
            if not dk_list:
                ticket.unread_r = True
                ticket._p_changed = True
                ticket.reindexObject()
                return None

            # Pokračovat ve zpracování validního záznamu
            library_dk_title = library_dk.Title()
            ticket.set_lb_state(u'auto lb created')

            if DEBUG:
                print ' '
                print 'Auto LB based on doc_order'
                print 'doc_order: ', doc_order
                print 'doc_id: ', doc_id
                print 'load_balancing_dk_list(..., restrict_doc_ids=[doc_id, ])'
                print 'dk_list'
                pprint(dk_list)
                print 'sigla_dk: ', sigla_dk
                #raise ApiAksException500('Debug')

        # pokud nejsou subtickety, tak 0, což znamená, že se ignoruje, jinak
        # 2 a víc, tzn. 1 nikdy není
        fullhid_nr = -1
        if self.subticket and self.subticket.fullhid_nr > fullhid_nr:
            fullhid_nr = self.subticket.fullhid_nr
        # for je zbytečný, protože self.subticket byl vytvořen tak že má max
        # hid, ale pro případné budoucí změny nechceme tento tichý předpoklad
        # o datech
        for x in self.subtickets:
            # x.fullhid_nr nemůže být None nebo '', vždy je číslo
            # x.fullhid_nr ale může být 0 takže by nefungovalo if x.fullhid_nr
            if x.fullhid_nr > fullhid_nr:
                fullhid_nr = x.fullhid_nr
        fullhid_nr += 1
        if fullhid_nr == 1:
            fullhid_nr = 2

        now_iso = self.dt_as(DateTime(), 'iso')

        obj_dict = dict(
            ticket_id = ticket.getId(),
            hid = ticket.hid,
            fullhid_sigla = sigla_dk,
            fullhid_nr = fullhid_nr,
            library_zk = ticket.library_zk,
            library_dk = sigla_dk,
            library_zk_title = ticket.library_zk_title,
            library_dk_title = library_dk_title,
            doc_id = dk_info['id'],
            doc_title = dk_info['title'],
            doc_fullname = dk_info['fullname'],
            doc_sent = doc_sent,
            #doc_data = dk_info['doc_data'],
            assigned_date = now_iso,
            cena = 70,
            )
        if DEBUG:
            pprint(obj_dict)
        subticket = create_subticketmvs(ticket, obj_dict, catalog=self.catalog)
        print 'new subticket'
        print subticket
        self.subtickets.insert(0, self.subticket)
        self.subticket = subticket
        self.subticket_last = self.subticket

        # v případě, že se nejedná o první subticket, je potřeba zneplatnit data
        # založena na předchozím subticketu
        ticket.closed_date = None

        # ticket - vše plynoucí z nově přidaného podpožadavku
        ticket.library_dk = sigla_dk
        if not ticket.library_dks:
            ticket.library_dks = []
        ticket.library_dks.insert(0, sigla_dk)
        ticket.library_dk_title = library_dk_title
        ticket.subticket_id = subticket.getId()
        if not ticket.subticket_ids:
            ticket.subticket_ids = []
        ticket.subticket_ids.insert(0, subticket.getId())

        subticket.set_next_action(
            na_type='1wd',
            na_text=u'Schválení nebo odmítnutí požadavku do 1 pracovního dne'
            )
        subticket.decide_date = self.dt_as(subticket.next_action_date, 'iso')

        # unread
        ticket.unread_r = True
        ticket._p_changed = True
        ticket.reindexObject()
        subticket.unread_dk = True
        subticket._p_changed = True
        subticket.reindexObject()

        return subticket
