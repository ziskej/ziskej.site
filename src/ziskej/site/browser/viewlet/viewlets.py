# -*- coding: utf-8 -*-

from DateTime import DateTime
from Acquisition import aq_base, aq_inner
from zope.interface import implementer
from zope.viewlet.interfaces import IViewlet
from zope.component import getMultiAdapter
from plone import api
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.CMFCore.utils import getToolByName

from ziskej.site.utils import get_instance_type_label
from ziskej.site.auth.auth import auth_ziskej_role
from ziskej.site.ticket_helpers import (
    TICKET_PORTAL_TYPES,
    SUBTICKET_PORTAL_TYPES,
    get_cpk_url,
    )


@implementer(IViewlet)
class ViewletBase(BrowserView):

    def __init__(self, context, request, view, manager):
        super(ViewletBase, self).__init__(context, request)
        self.context = context
        self.request = request
        self.view = view
        self.manager = manager

    def available(self):
        return True

    def update(self):
        super(ViewletBase, self).update()

        self.portal_state = getMultiAdapter((self.context, self.request),
                                            name=u'plone_portal_state')
        self.site_url = self.portal_state.portal_url()
        self.anonymous = self.portal_state.anonymous()

        self.cpk_url = get_cpk_url()

    def render(self):
        return self.template()


@implementer(IViewlet)
class HeaderViewlet(ViewletBase):

    template = ViewPageTemplateFile('viewlet_header.pt')

    def setup_reader_tabs(self):
        src_base = self.portal_url + '/++theme++ziskej-theme/img/'
        any_selected = False
        url = self.request.getURL()
        url = url.strip('/')

        item_url = self.portal_url + '/tickets'
        this_selected = item_url in url or self.context.portal_type in TICKET_PORTAL_TYPES
        any_selected = any_selected or this_selected
        self.display.append(
            dict(
                title = u'Objednávky',
                url = item_url,
                imgsrc = src_base + 'nav-pozadavky.png',
                selected = this_selected,
                )
            )

        item_url_base = self.portal_url + '/ticket_create_summary'
        item_url = item_url_base + '?action=create'
        is_selected = bool(item_url_base in url)
        any_selected = any_selected or is_selected
        self.display.append(
            dict(
                title = u'Nová objednávka',
                url = item_url,
                imgsrc = src_base + 'nav-pozadavky-vytvorit.png',
                selected = is_selected,
                )
            )

        item_url = self.ziskej_user_info['user'].absolute_url() + '/reader_update'
        self.profile_link = item_url
        any_selected = any_selected or bool(item_url in url)
        self.display.append(
            dict(
                title = u'Osobní profil',
                url = item_url,
                imgsrc = src_base + 'nav-ctenari.png',
                selected = bool(item_url in url),
                )
            )

        item_url = self.portal_url + '/?howto=r'
        is_selected = bool(self.request.get('howto', u'') == u'r')
        any_selected = any_selected or is_selected
        self.display.append(
            dict(
                title = u'Jak objednat?',
                url = item_url,
                imgsrc = src_base + 'nav-napoveda.png',
                selected = is_selected,
                )
            )

        return any_selected

    def setup_std_tabs(self):
        src_base = self.portal_url + '/++theme++ziskej-theme/img/'
        any_selected = False
        url = self.request.getURL()

        # home
        if self.is_librarian:
            self.display.append(
                dict(
                    title = u'Přehled',
                    url = self.portal_url + '/',
                    imgsrc = src_base + 'nav-ziskej.png',
                    selected = False,
                    )
                )
        elif self.is_operator:
            self.display.append(
                dict(
                    title = u'Přehled',
                    url = self.portal_url + '/',
                    imgsrc = src_base + 'nav-ziskej.png',
                    selected = False,
                    )
                )

        # elif self.is_librarian:
        #     self.display.append(
        #         dict(
        #             title = u'Čtenáři knihovny',
        #             url = self.portal_url + '/readers',
        #             imgsrc = src_base + 'nav-ctenari.png',
        #             )
        #         )
        if self.is_librarian:
            # jinak má operátor s aktivovaným sudo příliš mnoho položek
            pass
        elif self.is_operator:
            item_url = self.portal_url + '/readers'
            any_selected = any_selected or bool(item_url in url)
            self.display.append(
                dict(
                    title = u'Čtenáři',
                    url = item_url,
                    imgsrc = src_base + 'nav-ctenari.png',
                    selected = bool(item_url in url),
                    )
                )

        # libraries
        if self.is_librarian:
            item_url = self.library.absolute_url()
            any_selected = any_selected or bool(item_url in url)
            self.display.append(
                dict(
                    title = u'Profil knihovny',
                    url = item_url,
                    imgsrc = src_base + 'nav-knihovny.png',
                    selected = bool(item_url in url),
                    )
                )
        elif self.is_operator:
            item_url = self.portal_url + '/libraries'
            any_selected = any_selected or bool(item_url in url)
            self.display.append(
                dict(
                    title = u'Knihovny',
                    url = item_url,
                    imgsrc = src_base + 'nav-knihovny.png',
                    selected = bool(item_url in url),
                    )
                )

        if self.is_librarian:
            item_url_base = self.portal_url + '/ticket_create_summary'
            item_url = item_url_base + '?action=create'
            is_selected = bool(item_url_base in url)
            any_selected = any_selected or is_selected
            self.display.append(
                dict(
                    title = u'Nová objednávka',
                    #url = self.portal_url + '/tickets/create_ticket',
                    url = item_url,
                    imgsrc = src_base + 'nav-pozadavky-vytvorit.png',
                    selected = is_selected,
                    )
                )

        # tickets
        # elif self.is_operator:
        #     self.display.append(
        #         dict(
        #             title = u'Knihovny',
        #             url = self.portal_url + '/tickets',
        #             imgsrc = src_base + 'nav-pozadavky.png',
        #             )
        #         )

        # tickets ŽK
        if self.is_librarian or self.is_operator:
            item_url = self.portal_url + '/tickets/listing_zk'
            this_selected = item_url in url or self.context.portal_type in TICKET_PORTAL_TYPES
            any_selected = any_selected or this_selected
            self.display.append(
                dict(
                    title = u'Objednávky',
                    url = item_url,
                    imgsrc = src_base + 'nav-pozadavky-odchozi.png',
                    selected = this_selected,
                    )
                )

        # tickets DK
        if self.is_librarian or self.is_operator:
            item_url = self.portal_url + '/tickets/listing_dk'
            this_selected = item_url in url or self.context.portal_type in SUBTICKET_PORTAL_TYPES
            any_selected = any_selected or this_selected
            self.display.append(
                dict(
                    title = u'Požadavky',
                    url = item_url,
                    imgsrc = src_base + 'nav-pozadavky-prichozi.png',
                    selected = this_selected,
                    )
                )

        # statistiky
        if self.is_librarian or self.is_operator:
            item_url = self.portal_url + '/reports'
            this_selected = item_url in url
            any_selected = any_selected or this_selected
            self.display.append(
                dict(
                    title = u'Reporty a statistiky',
                    url = item_url,
                    imgsrc = src_base + 'nav-statistiky.png',
                    selected = this_selected,
                    )
                )

        return any_selected

    def setup_tabs(self):
        self.display = []

        if self.is_anon:
            any_selected = False
        elif self.is_reader:
            any_selected = self.setup_reader_tabs()
        else:
            any_selected = self.setup_std_tabs()

        self.display_count = len(self.display)
        if self.display_count <= 1:
            self.css_grid = u'col-md-12 col-sm-12'
        elif self.display_count <= 2:
            self.css_grid = u'col-md-6 col-sm-12'
        elif self.display_count <= 3:
            self.css_grid = u'col-md-4 col-sm-12'
        elif self.display_count <= 4:
            self.css_grid = u'col-md-3 col-sm-12'
        elif self.display_count <= 6:
            self.css_grid = u'col-md-2 col-sm-12'
            if self.display_count == 5:
                self.display.insert(
                    4,
                    dict(
                        title = u'',
                        url = '',
                        imgsrc = '',
                        )
                    )
        else:
            self.css_grid = u'col-md-2 col-sm-12'

        if not any_selected and self.display_count:
            self.display[0]['selected'] = True

    def update(self):
        super(HeaderViewlet, self).update()
        context = aq_inner(self.context)
        self.portal = api.portal.get()
        self.home = getattr(self.portal, 'home', None)
        self.portal_url = self.portal.absolute_url()
        self.context_url = self.context.absolute_url()

        instance_type, self.instance_label, instance_title_postfix = get_instance_type_label()
        self.instance_html_title = u'ZÍSKEJ {}'.format(instance_title_postfix)
        self.instance_body_css = u''
        self.instance_body_css += u'instance-{}'.format(instance_type)
        if instance_type != 'prod':
            self.instance_body_css += u' instance-not-prod'

        self.is_anon = True
        self.is_reader = None
        self.is_librarian = None
        self.is_operator = None
        self.user_name = u''
        self.library = None
        self.emergency_header = u''
        self.sudo_links = []

        if self.home is not None:
            self.emergency_header = self.home.get_parameters('emergency_header')

        self.ziskej_user_info = auth_ziskej_role(
            context, self.request, caller='viewlet')

        if self.ziskej_user_info:
            self.is_anon = self.ziskej_user_info['is_anon']
            self.is_reader = bool(self.ziskej_user_info['role'] == 'Reader')
            self.is_librarian = bool(self.ziskej_user_info['role'] == 'Librarian')
            self.is_operator = self.ziskej_user_info['is_ziskej_operator']
            if self.ziskej_user_info['user']:
                self.user_name = self.ziskej_user_info['user'].fullname
            else:
                self.user_name = self.ziskej_user_info['user_id']
            self.library = self.ziskej_user_info['library']
            self.sudo_links = self.ziskej_user_info.get('sudo_links', [])
            if self.ziskej_user_info.get('sudo_marker', False):
                self.user_name += u' / {}'.format(self.library.sigla.upper())

        self.display_user_nav_bar = not self.is_anon
        self.display_ziskej_header = False
        self.display_nav_ziskej = True
        self.display_plone_ziskej_theme = True
        #display_plone_ziskej_theme_ignore_list = ['folder_contents', 'edit', ]
        display_plone_ziskej_theme_ignore_list = []
        #display_plone_ziskej_theme_ignore_list.append('folder_contents')
        #display_plone_ziskej_theme_ignore_list.append('edit')
        #display_plone_ziskej_theme_ignore_list.append('@@overview-controlpanel')
        #display_plone_ziskej_theme_ignore_list.append('@@usergroup-userprefs')
        for url_postfix in display_plone_ziskej_theme_ignore_list:
            if not self.display_plone_ziskej_theme:
                break
            if self.request.URL.endswith(url_postfix):
                self.display_plone_ziskej_theme = False
                break
        display_plone_ziskej_theme_ignore_list_prefix = []
        display_plone_ziskej_theme_ignore_list_prefix.append(self.portal_url + '/@@')
        for url_prefix in display_plone_ziskej_theme_ignore_list_prefix:
            if not self.display_plone_ziskej_theme:
                break
            if self.request.URL.startswith(url_prefix):
                # whitelist in blacklist
                if self.request.URL.startswith(self.portal_url + '/@@confirm-action'):
                    continue
                self.display_plone_ziskej_theme = False
                break
        if self.request.get('t', None):
            self.display_plone_ziskej_theme = False
        if not self.display_plone_ziskej_theme:
            self.display_user_nav_bar = False
            self.display_ziskej_header = False
            self.display_nav_ziskej = False

        self.profile_link = None

        self.setup_tabs()


@implementer(IViewlet)
class FooterViewlet(ViewletBase):

    template = ViewPageTemplateFile('viewlet_footer.pt')

    def update(self):
        super(FooterViewlet, self).update()
        self.year = DateTime().year

        instance_type, instance_label, instance_title_postfix = get_instance_type_label()
        self.is_local = bool(instance_type == 'development')
