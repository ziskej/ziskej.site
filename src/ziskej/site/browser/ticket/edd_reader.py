# -*- coding: utf-8 -*-
"""Library default view"""

from __future__ import print_function

import logging
import uuid

from DateTime import DateTime
from plone import api
from plone.protect.interfaces import IDisableCSRFProtection
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import transaction
from zope.interface import alsoProvides

from ziskej.site import ZISKEJ_VAR_EDD, DEBUG_EDD
from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.utils import edd_get_file


logger = logging.getLogger("ziskej")

DEBUG = DEBUG_EDD


class EddReaderView(ZiskejBrowserView):

    template = ViewPageTemplateFile('edd_reader.pt')

    def __call__(self):
        if not self.context.is_edd:
            self.add_message(u'Chybný požadavek')
            self.redirect(self.portal_url)
            return self.portal_url
        download = self.text_from_request('download')
        if download:
            # ŽK ověření kvality PDF pro reklamace
            if download == 'zk':
                return self.download_pdf(is_reader=False)
            else:
                token = self.text_from_request('token')
                if token and token == self.context.reader_token:
                    return self.download_pdf()
        return super(EddReaderView, self).__call__()

    def add_message_copy(self, message, message_type='info'):
        """Voláno před super().__call__"""
        portal = api.portal.get()
        ptool = portal.plone_utils
        ptool.addPortalMessage(message, message_type)

    def redirect_copy(self, url):
        """Voláno před super().__call__"""
        logger.info('redirect: ' + url)
        self.request.RESPONSE.redirect(url)

    def download_pdf(self, is_reader=True):
        """Voláno před super().__call__"""
        self.ticket = self.context
        subticket_id = self.ticket.subticket_id
        if subticket_id:
            self.subticket = getattr(self.ticket, subticket_id, None)
        else:
            self.subticket = None
        pdf_file_id = None
        if self.subticket:
            pdf_file_id = self.subticket.pdf_file_id

        if pdf_file_id:
            # Čtenář, PDF soubor expiroval
            if is_reader and not self.subticket.is_pdf_file_unexpired():
                self.add_message_copy(
                    u'Váš požadavek není možné vyřídit, pdf soubor už '
                    u'expiroval.')
                url = u'{}/edd_download?token={}'.format(
                    self.context.absolute_url(),
                    self.text_from_request('token'),
                    )
                self.redirect_copy(url)
                return url
            # ŽK, PDF soubor neexpiroval, proběhla reklamace (úspěšně) s novým
            # dodáním
            elif not is_reader and self.subticket.is_pdf_file_unexpired() and \
                    self.subticket.complaint_state == 'soft_done':
                pass
            # ŽK, PDF soubor nelze reklamovat
            elif not is_reader and \
                    not self.subticket.is_pdf_file_complaintable():
                self.add_message_copy(
                    u'Není možné získat PDF soubor pro ověření kvality pro '
                    u'potřeby reklamace, protože už upynula stanovená lhůta od '
                    u'expirace souboru.')
                url = self.context.absolute_url()
                self.redirect_copy(url)
                return url

        if pdf_file_id:
            try:
                content = edd_get_file(ZISKEJ_VAR_EDD, pdf_file_id)
            except IOError:
                logger.exception('edd_get_file failed (IOError)')
                content = None

        if not pdf_file_id or not content:
            self.add_message_copy(
                u'Váš požadavek není možné vyřídit, soubor už neexistuje.')
            if is_reader:
                url = u'{}/edd_download?token={}'.format(
                    self.context.absolute_url(),
                    self.text_from_request('token'),
                    )
            else:
                url = self.context.absolute_url()
            self.redirect_copy(url)
            return url

        filename = self.subticket.pdf_file_filename
        now = DateTime()

        # Zaznamenat stažení pdf
        if is_reader:
            alsoProvides(self.request, IDisableCSRFProtection)
            self.subticket.downloaded_date = now
            if not self.subticket.downloaded_number:
                message = u"První stažení čtenářem"
                self.add_historylog(message, who=['dk', 'zk', 'r'],
                    subticket=self.subticket, is_edd_reader=True)
                self.subticket.downloaded_number = 1
            else:
                self.subticket.downloaded_number += 1
            self.subticket._p_changed = True
            #self.subticket.reindexObject()
            #transaction.commit()

        # no diazo
        self.request.response.setHeader('X-Theme-Disabled', '1')

        # pdf content type
        self.request.response.setHeader('Content-Type', 'application/pdf')
        self.request.response.setHeader(
            'Content-Disposition', 'attachment; filename="{}"'.format(filename))

        # no cache
        self.request.response.setHeader('Cache-Control', 'no-store,no-cache,must-revalidate,post-check=0,pre-check=0')
        self.request.response.setHeader('Pragma', 'no-cache')
        self.request.response.setHeader('Expires', '-1')
        self.request.response.setHeader('Last-modified', now.ISO())

        return content

    def call(self):
        self.is_auth_token_ok = False
        self.show_ticket_info = False
        self.show_ticket_info_for_complaint = False
        self.show_ticket_info_for_complaint_zk = False
        self.show_payment_info = False
        self.show_paid_info = False
        self.show_download_info = False
        self.show_complaint_form = False
        self.show_complaint_info = False

        self.ticket = self.context
        self.subticket = None
        self.subticket_last = None
        self.pages_number_dk = None
        self.downloaded_number = None
        self.pdf_expire_html = u''
        self.is_pdf_file_unexpired = None

        # if self.text_from_request('reset_token'):
        #     self.ticket.reader_token = u'{random_part}'.format(
        #         random_part = str(uuid.uuid4()),
        #         )
        #     print "self.ticket.reader_token", self.ticket.reader_token
        token = self.text_from_request('token')
        if not token or token != self.ticket.reader_token:
            return

        self.is_auth_token_ok = True
        self.action_url = u'{}/edd_download'.format(self.context_url)
        self.action_url_w_token = u'{}?token={}'.format(self.action_url, token)
        self.show_ticket_info = True

        if not self.ticket.payment_paid_date:
            self.show_payment_info = True
            self.platebator_url = self.ticket.platebator_url()
            if DEBUG:
                print("self.platebator_url", self.platebator_url)
        else:
            self.show_paid_info = True
            self.show_download_info = True
            self.paid_date_formatted = self.ticket.payment_paid_date.strftime(
                '%d.%m.%Y')

        subticket_id = self.ticket.subticket_id
        if subticket_id:
            self.subticket = getattr(self.ticket, subticket_id, None)
        if self.subticket:
            self.subticket_last = self.subticket
        elif self.ticket.subticket_ids:
            self.subticket_last = getattr(
                self.ticket, self.ticket.subticket_ids[-1], None)

        # edd = self.ticket.Edd_data()
        # self.doc_quote = edd.get('doc_quote', u'')

        if not self.subticket_last:
            return

        # subticket_last
        self.pages_number_dk = self.subticket_last.pages_number_dk
        self.downloaded_number = self.subticket_last.downloaded_number
        if self.subticket_last.download_expiration_date:
            date_str = self.dt_as(
                self.subticket_last.download_expiration_date, 'isodt')
            self.pdf_expire_html = self.date_format_html(
                date_str, is_time=True, omit_year=False)

        if DEBUG:
            print(" ")
            print("EDD reader complaint DEBUG:")
            print("--------------------------------")
            print("complaint_state", self.subticket_last.complaint_state)
            print("complaint_who", self.subticket_last.complaint_who)
            print("complaint_text", self.subticket_last.complaint_text)
            print("complaint_date", self.subticket_last.complaint_date)
            print("complaint_soft_date", self.subticket_last.complaint_soft_date)
            print("complaint_soft_resent_date", self.subticket_last.complaint_soft_resent_date)
            print("complaint_is_soft", self.subticket_last.complaint_is_soft)
            print("objection_text", self.subticket_last.objection_text)
            print("objection_decision_text", self.subticket_last.objection_decision_text)
            print(" ")

            # -- NEW
            # complaint_state new
            # complaint_who reader
            # complaint_text Lorem ipsum
            # complaint_date 2021-11-10 15:21:23
            # complaint_soft_date None
            # complaint_is_soft True

            # -- Accepted
            # complaint_state
            # complaint_who reader
            # complaint_text Lorem ipsum
            # complaint_date 2021-11-10 15:21:23
            # complaint_soft_date 2021-11-10
            # complaint_is_soft True

        # subticket
        if not self.subticket:
            # ŽK reklamuje a požaduje vrácení ceny, proto byl aktivní požadavek
            # ukončen, žádný jiný zatím nebyl připraven
            self.show_ticket_info = False
            self.show_ticket_info_for_complaint = True
            self.show_download_info = False
            return

        self.is_pdf_file_unexpired = self.subticket.is_pdf_file_unexpired()

        # Neumožnit zaplatit požadavek s expirovaným, ale ještě nesmazaným pdf
        if not self.is_pdf_file_unexpired:
            self.show_payment_info = False

        # Zda zobrazit informace o reklamaci, reklamuje Čtenář
        self.show_complaint_info = bool(self.subticket.complaint_text
            and self.subticket.complaint_who == u'reader')
        if DEBUG:
            print("show_complaint_info", self.show_complaint_info)
        if self.show_complaint_info:
            self.complaint_state_label = self.subticket.complaint_state_label
            if DEBUG:
                print("complaint_state_label", self.complaint_state_label)
            self.complaint_text = self.subticket.complaint_text
            self.complaint_date = None
            if self.subticket.complaint_date is not None:
                self.complaint_date = self.date_format_html(
                    self.subticket.complaint_date,
                    is_time=False, omit_year=False)
            self.complaint_soft_date = None
            if self.subticket.complaint_soft_date is not None:
                self.complaint_soft_date = self.date_format_html(
                    self.subticket.complaint_soft_date,
                    is_time=False, omit_year=False)
            self.complaint_soft_resent_date = None
            if self.subticket.complaint_soft_resent_date is not None:
                self.complaint_soft_resent_date = self.date_format_html(
                    self.subticket.complaint_soft_resent_date,
                    is_time=False, omit_year=False)
            # # Aktivní reklamace
            # if self.subticket.complaint_state not in (
            #         'declined', 'objection_declined', 'soft_done', ):
            #     self.show_ticket_info = False
            #     self.show_ticket_info_for_complaint = True
            #     self.show_download_info = False
            # # Neaktivní reklamace
            # else:
            #     self.show_ticket_info = True
            #     self.show_ticket_info_for_complaint = False

        # # Zda zobrazit informace o reklamaci, reklamuje ŽK
        # if self.subticket.complaint_text and \
        #         self.subticket.complaint_who != u'reader' and \
        #         self.subticket.complaint_state not in (
        #             'declined', 'objection_declined', 'soft_done', ):
        #     self.show_ticket_info_for_complaint_zk = True

        if self.subticket.complaint_text:
            # Aktivní reklamace
            if self.subticket.complaint_state not in (
                    'declined', 'objection_declined', 'soft_done', ):
                self.show_ticket_info = False
                self.show_ticket_info_for_complaint = True
                self.show_download_info = False
            # Neaktivní reklamace
            else:
                self.show_ticket_info = True
                self.show_ticket_info_for_complaint = False

        # Zda umožnit zadat novou reklamaci čtenářem
        self.show_complaint_form = bool(self.ticket.payment_paid_date and
            self.subticket.is_pdf_file_complaintable() and
            not self.show_complaint_info and
            not self.subticket.complaint_state)
        if DEBUG:
            print("show_complaint_form", self.show_complaint_form)
        if self.show_complaint_form and self.text_from_request('complaint'):
            do_complaint = True
            complaint_text = self.text_from_request('complaint_text')
            # Validace
            if not self.subticket.downloaded_date:
                do_complaint = False
                self.add_message(
                    u'Není možné reklamovat dříve, než jste stáhl PDF soubor.')
            if not complaint_text:
                do_complaint = False
                self.add_message(u'Chybí text reklamace.')
            if do_complaint:
                self._complaint_create(complaint_text)
                self.redirect(self.action_url_w_token)
        if DEBUG:
            print("--------------------------------")
            print(" ")

    def _complaint_create(self, text):
        """Vytvořit malou reklamaci a vše kolem standardně, jen audit log, že
        vytvořil čtenář.  Navíc poslat notifikaci i ŽK.

        Inspirováno TicketCoreCancelComplaintView._complaint_create()
        """

        self.subticket.complaint_date = self.dt_as(DateTime(), 'isodt')
        self.subticket.complaint_text = text
        self.subticket.complaint_is_soft = True
        self.subticket.complaint_state = u'new'
        self.subticket.complaint_who = u'reader'  # dříve měl jen ticket
        self.ticket.unread_zk = True
        self.subticket.unread_dk = True
        self._subticket_complaint_notify()

        self.ticket.reindexObject()
        self.subticket.reindexObject()

        self.add_message(u'Byla vytvořena reklamace požadavku.')

    def _subticket_complaint_notify(self):
        message = u'Čtenář reklamuje kvalitu EDD PDF souboru.'
        notifications = ['wfcm_small_new_dk', ]
        self.add_historylog(message, who=['zk', 'dk'], subticket=self.subticket,
                            is_edd_reader=True)

        notifications = ['wfcm_small_std_zk', 'wfcm_small_std_dk', ]
        for notification in notifications:
            self.send_notifications(notification)
