# -*- coding: utf-8 -*-
"""BrowserView: Ticket / Subticket.

Sdílený kód pro Ticket a Subticket, workflow.
"""

from __future__ import print_function

from datetime import datetime
import logging
import uuid

from DateTime import DateTime

from ziskej.site import PAYSYS_MESSAGE
from ziskej.site.browser import ZiskejBrowserView
from ziskej.site.library_payment_helpers import (
    library_payment_mvs,
    library_payment_edd_create,
    )
from ziskej.site.ticket_helpers import (
    create_reportentry,
    wf_action,
    )
from ziskej.site.utils import datetime_add_d
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger("ziskej")

DEBUG = False

BTNCSS_DEFAULT = u'btn btn-default'
BTNCSS_PRIMARY = u'btn btn-success'
# BTNCSS_DANGER =u'btn btn-danger'


class TicketCoreWorkflowView(ZiskejBrowserView):

    #def wf_ticket(self):
    #    pass

    def _wf_created_assign(self, wf_status='created'):
        self.btncss_reader_wf = BTNCSS_PRIMARY
        if self.wf_ticket_submit == 'assign':

            if self.ticket.is_mvs and PAYSYS_MESSAGE and \
                    not self.ticket.mvs_zk_enough_available():
                message = u'Není dostatek kreditu pro zaplacení MVS požadavku.'
                self.add_message(message, message_type='error')

            self.mark_librarian()
            self._complaint_big_restore()

            wf_action(self.ticket, 'assign', u'')
            if wf_status in ('created', ):
                message = u'Schváleno a přiřazena DK.'
                self.ticket.approved_date = self.dt_as(DateTime(), 'iso')
                self.ticket.unread_r = True
            else:
                message = u'Přiřazena DK.'
            self.add_message(message)

            # create and enqueue subticket
            subticket = self.create_subticket()
            self.add_historylog(u'Schváleno a přiřazena DK.', who=['zk', 'dk'])

            if wf_status in ('created', ):
                self.send_notifications('wf_t_zk_assign_dk')
            else:
                self.send_notifications('wf_t_zk_assign_dk')

            self.ticket.reindexObject()
            self.redirect(self.context_url)
            return True

        elif self.ticket.is_mvs and PAYSYS_MESSAGE and \
                not self.ticket.mvs_zk_enough_available():
            message = u'Není dostatek kreditu pro zaplacení MVS požadavku.'
            self.add_message(message, message_type='warning')

        self.ticket_transitions.append(
            dict(
                id = 'assign',
                label = u'Schválit a přiřadit DK',
                btype = u'success',
                )
            )
        return False

    def _wf_created_assign_cannot(self, wf_status='created'):
        hint = u''
        if not self.ticket.is_reader_approved:
            hint += u'Není ověřen čtenář.  '
        if not self.ticket.is_lb_ready_for_assignment:
            if self.ticket.is_lb_manual:
                if not self.ticket.lb_manual_library:
                    hint += u'Není zvolena DK.  '
            elif self.ticket.is_lb_auto:
                if self.ticket.lb_state == u'auto: load availability init':
                    hint += u'Není dokočeno načítání dostupnosti - v tomto stavu by požadavek měl být maximálně jednotky minut.  Pokud by byl déle, načtěte znovu dostupnost.  '
                elif self.ticket.lb_state == u'auto: load availability finished OK':
                    hint += u'Je načtena dostupnost, ale nebylo odsouhlaseno pořadí.  '
                elif self.ticket.lb_state == u'auto: load availability finished failed':
                    hint += u'Je načtena dostupnost, ale nepodařilo se najít žádný dostupný dokument.  Můžete zkusit načíst dostupnost později.  Jako alternativa je manuální zvolení DK.  '
                    message = u'Knihovník zvolil automatické přidělování požadavků a nejsou k dispozici žádné dokumenty k zapůjčení, které by vyhovovaly nastavení systému ZÍSKEJ'
                    self.add_message(message)
                elif self.ticket.lb_state == u'auto: load availability order accepted' and not self.ticket.doc_order:
                    # SUGGESTION prověřit, zda opravdu může nastat, že není
                    # možné zvolit?
                    hint += u'Není zvoleno žádné pořadí - pokud je možné zvolit, zvolte, jinak můžete zkusit znovu načíst dostupnost.  Jako alternativa je manuální zvolení DK.  '
                elif self.ticket.lb_state == u'auto lb closed' and not self.ticket.doc_order:
                    # SUGGESTION prověřit, zda opravdu může nastat, že není
                    # možné zvolit?
                    hint += u'Není zvoleno žádné pořadí - pokud je možné zvolit, zvolte, jinak můžete zkusit znovu načíst dostupnost.  Jako alternativa je manuální zvolení DK.  '
            elif self.ticket.is_lb_undecided:
                hint += u'Není zvolena metoda pro vytváření požadavků.  '
        if self.ticket.is_mvs:
            if self.ticket.is_created_by_cpk_for_reader():
                if not self.ticket.is_paid_in_advance():
                    hint += u'Čtenář zatím nezaplatil.  '
            else:
                if PAYSYS_MESSAGE and not self.ticket.mvs_zk_enough_available():
                    hint += u'Není dostatek kreditu pro zaplacení MVS požadavku.  '
                    message = u'Není dostatek kreditu pro zaplacení MVS požadavku.'
                    self.add_message(message, message_type='warning')
        if not hint:
            logger.warning('Cannot assign and no hint why')
        if wf_status == 'created':
            label = u'Nelze schválit a přiřadit DK: ' + hint
        else:
            label = u'Nelze přiřadit DK: ' + hint
        self.ticket_transitions.append(
            dict(
                id = '',
                label = label,
                btype = u'default',
                url = '',
                )
            )
        return False

    def _wf_created_reject(self, wf_status='created'):
        if self.wf_ticket_submit == 'reject':
            reject_reason_lov = (u'reject-reason-zk-available', u'reject-reason-zk-impossible', u'reject-reason-zk-unavailable', )
            reject_reason = self.text_from_request('reject_reason', lov=reject_reason_lov)
            if not reject_reason:
                message = u'Chybí důvod zamítnutí, který je povinný.'
                self.add_message(message)
                self.wf_ticket_submit = None
        if self.wf_ticket_submit == 'reject':
            now_iso = self.dt_as(DateTime(), 'iso')
            self.ticket.closed_date = now_iso
            self.mark_librarian()
            self.ticket.reject_reason = reject_reason
            wf_action(self.ticket, 'reject', reject_reason)
            message = u'Zamítnuto knihovníkem jako nevyříditelné.'
            self.add_message(message)
            self.ticket.unread_r = True
            self.add_historylog(message, who=['zk', 'r'])
            self.send_notifications('wf_t_reject')
            self.ticket.reindexObject()
            self.redirect(self.context_url)
            return True
        self.ticket_transitions.append(
            dict(
                id = 'reject',
                label = u'Zamítnout',
                btype = u'warning modal-wf-details',  # warning x danger
                data_toggle = 'modal',
                data_target = '#modal-wf-zk-reject',
                )
            )
        return False

    def _wf_assigned_prepare(self):
        if self.wf_ticket_submit == 'prepare':

            self.mark_librarian()

            wf_action(self.ticket, 'prepare', u'')
            message = u'Připraveno pro čtenáře.'
            self.add_message(message)

            # unread
            self.ticket.unread_r = True
            if DEBUG:
                print("unread_r")
            # unread subticketu je v create_subticket
            self.add_historylog(
                u'Knihovník žádající knihovny dokument připravil pro čtenáře a '
                u'nastavil k vyzvednutí od {}'.format(
                    self.date_format(self.ticket.reader_from_date)),
                who=['zk', 'r', 'dk'])

            # Nemá smysl posílat čtenáři zprávu, pokud není čtenář
            if self.ticket.is_created_by_reader:
                # library_zk je tady vždy i knihovnou přihlášeného uživatele,
                # proto je možné místo self.ticket.LibraryZk() použít jen
                # self.library
                reader_note_str = u''
                if self.ticket.reader_note:
                    reader_note_str = u'\nPoznámka knihovníka: {}'.format(
                        self.ticket.reader_note)
                # Cena za poskytnutou službu je {reader_fee} Kč.
                message = u'''\
Vaše objednávka č. {hid} na MVS ze dne {created_date} byla vyřízena.
Požadovaný dokument je k vyzvednutí od {reader_from_date} ve Vaší knihovně.
Místo k vyzvednutí: {reader_place}.
Termín vrácení je {reader_to_date}.{reader_note}
V případě potřeby kontaktujte svou knihovnu: {library_fullname}.
Děkujeme za využití služby ZÍSKEJ.
'''.format(
    hid=self.ticket.hid,
    created_date=self.date_format(self.ticket.created_date),
    reader_from_date=self.date_format(self.ticket.reader_from_date),
    reader_to_date=self.date_format(self.ticket.reader_to_date),
    # reader_fee=self.ticket.reader_fee,
    reader_place=self.ticket.reader_place,
    reader_note=reader_note_str,
    library_fullname=self.library.fullname,
    )
                # print(" ")
                # print(repr(message))
                # print(" ")
                # raise Exception('DEBUG')
                self.ticket.add_pm_auto(message)

                # Poslat notifikaci, jakoby knihovník napsal zprávu.
                self.send_notifications('pm_r_zk_librarian')

            self.send_notifications('wf_t_prepare')

            self.ticket.reindexObject()
            self.redirect(self.context_url)
            return True

        self.ticket_transitions.append(
            dict(
                id = 'prepare',
                label = u'Připravit pro čtenáře',
                btype = u'success',
                )
            )
        return False

    def _wf_assigned_prepare_cannot(self):
        hint = u''
        if not self.ticket.reader_from_date:
            hint += u'Není připraveno pro čtenáře od. '
        if self.ticket.is_mvs and not self.ticket.reader_to_date:
            hint += u'Není nutno vrátit do. '    
        if self.ticket.reader_fee is None or self.ticket.reader_fee == u'':
            hint += u'Není cena pro čtenáře. '
        if not self.ticket.reader_place:
            hint += u'Není místo pro čtenáře. '
                
        if not hint:
            logger.warning('Cannot assign and no hint why')
        self.ticket_transitions.append(
            dict(
                id = '',
                label = u'Nelze schválit: ' + hint, 
                btype = u'default',
                url = '',
                )
            )
        return False

    def _wf_pending_assign(self):
        return self._wf_created_assign(wf_status='pending')

    def _wf_pending_assign_cannot(self):
        return self._wf_created_assign_cannot(wf_status='pending')

    def _wf_pending_reject(self):
        return self._wf_created_reject(wf_status='pending')

    def _wf_prepared_lent_mvs(self):
        self.prepared_info = u'Připraveno pro čtenáře'
        if self.wf_ticket_submit == 'lent':
            self.mark_librarian()
            self.ticket.reader_lent_date = self.dt_as(DateTime(), 'iso')
            self.add_historylog(u'Půjčeno čtenáři.', who=['zk', 'r'])
            self.ticket.reindexObject()
            self.redirect(self.context_url)
            return True
        self.ticket_transitions.append(
            dict(
                id = 'lent',
                label = u'Půjčeno čtenáři',
                btype = u'success',
                )
            )
        return False

    def _wf_prepared_lent_mrs(self):
        self.prepared_info = u'Připraveno pro čtenáře'
        if self.wf_ticket_submit == 'lent':
            self.mark_librarian()
            self.ticket.reader_lent_date = self.dt_as(DateTime(), 'iso')
            wf_action(self.ticket, 'close', u'')
            wf_action(self.subticket, 'close', u'')
            now_iso = self.dt_as(DateTime(), 'iso')
            self.ticket.closed_date = now_iso
            self.subticket.closed_date = now_iso
            self.add_historylog(u'Půjčeno čtenáři a tím uzavřeno.', who=['zk', 'r'])
            self.ticket.reindexObject()
            self.redirect(self.context_url)
            return True
        self.ticket_transitions.append(
            dict(
                id = 'lent',
                label = u'Půjčeno čtenáři a tím uzavřeno',
                btype = u'success',
                )
            )
        return False

    # FIXME-EDD
    #def _wf_prepared_lent_edd(self):

    def _wf_prepared_return(self):
        self.prepared_info = u'Půjčeno čtenáři'
        if self.wf_ticket_submit == 'return':
            self.mark_librarian()
            self.ticket.return_date = self.dt_as(DateTime(), 'iso')
            self.add_historylog(u'Vráceno čtenářem.', who=['zk', 'r'])

            # # uzavřít ticket
            # # disabled - ZIS-144
            # wf_action(self.ticket, 'close', u'')
            # message = u'Objednávka uzavřena.'
            # self.add_message(message)
            # self.ticket.unread_r = True
            # self.add_historylog(u'<strong>Objednávka byla uzavřena</strong> vrácením čtenářem.', who=['zk', 'r'])
            # self.send_notifications('wf_t_close')

            self.ticket.reindexObject()
            self.redirect(self.context_url)
            return True
        self.ticket_transitions.append(
            dict(
                id = 'return',
                label = u'Vráceno čtenářem',
                btype = u'success',
                )
            )
        return False

    def _wf_prepared_close(self, substate):
        if self.ticket.is_edd:
            self.ticket_transitions.append(
                dict(
                    id = '',
                    label = u'Objednávka bude automaticky uzavřena následující den po zaplacení čtenářem resp. po expiraci PDF souboru.',
                    btype = u'default',
                    url = '',
                    )
                )
            return False

        if substate not in ('before_lent', 'before_return', 'after_return', ):
            raise Exception('Invalid _wf_prepared_close substate')

        if self.wf_ticket_submit == 'close':
            self.mark_librarian()

            wf_action(self.ticket, 'close', u'')
            if substate == 'before_lent':
                message = u'Pokud si čtenář knihu nevyzvedl, zašlete ji zpět dožádané knihovně.'
            elif substate == 'before_return':
                message = u'Objednávka uzavřena předčasně, protože čtenář nevrátil a už asi nevrátí.'
            elif substate == 'after_return':
                message = u'Objednávka uzavřena manuálně.'
            self.add_message(message)

            # unread
            self.ticket.unread_r = True
            if DEBUG:
                print("unread_r")
            if substate == 'before_lent':
                self.add_historylog(u'<strong>Objednávka byla předčasně uzavřena, </strong> čtenář si dokument nevyzvedl.', who=['zk'])
            elif substate == 'before_return':
                self.add_historylog(u'<strong>Objednávka byla uzavřena</strong> předčasně, protože čtenář nevrátil a už asi nevrátí', who=['zk'])
            elif substate == 'after_return':
                self.add_historylog(u'<strong>Objednávka byla uzavřena</strong> manuálně', who=['zk'])

            # ve skutečnosti nepotřebujeme žádnou notifikaci, není komu ji
            # posílat, toto zajímá jen ŽK, který ale akci provádí
            # if substate == 'before_lent':
            #     self.send_notifications('wf_t_close_before_lent')
            # elif substate == 'before_return':
            #     self.send_notifications('wf_t_close_before_return')

            self.ticket.reindexObject()
            self.redirect(self.context_url)
            return True

        if substate == 'before_lent':
            desc = u'Objednávku uzavřete v případě, že čtenář nepřišel, nebo dal vědět, že nepřijde, případně nemá o objednávku zájem.'
        elif substate == 'before_return':
            desc = u'Uzavřít předčasně, protože čtenář nevrátil a už asi nevrátí.'
        elif substate == 'after_return':
            desc = u'Uzavřít manuálně'

        self.ticket_transitions.append(
            dict(
                id = 'close',
                label = u'Uzavřít',
                desc = desc,
                btype = u'danger',
                )
            )
        return False

    def setup_wf(self):
        # ticket
        t_wf_status = self.ticket.wf_status
        self.ticket_review_state = t_wf_status['review_state']
        self.ticket_review_state_label = t_wf_status['review_state_label']

        # subticket
        if self.subticket:
            s_wf_status = self.subticket.wf_status
            self.subticket_review_state = s_wf_status['review_state']
            self.subticket_review_state_label = s_wf_status['review_state_label']
        else:
            self.subticket_review_state = ''
            self.subticket_review_state_label = u''

        # reader wf label
        if self.is_reader:
            self.ticket_review_state_label_reader = self.ticket.review_state_reader_label
        else:
            self.ticket_review_state_label_reader = u''

        # wf panel location - jen pro ticket view
        self.display_wf_r = False
        self.display_wf_dk = False
        self.display_wf_dk_subticket = False

        # wf alert UI
        # SUGGESTION nastavit podle toho, jaký je stav, kdy je nejbližší akce
        # atd.
        self.review_state_css = u'alert alert-success'  # nebo warning danger

    def wf_subticket_history(self, subticket):
        """This part differs from current wf (wf_subticket) because only few
        states are valid for historic subticket (see also wf_subticket docs
        string):

        [state: transitions]

        returned: send_back, lose
        sent_back: report_error_sent_back, accept_back, lose
        error_sent_back: accept_back, lose
        (for final states see wf_subticket docs string)

        Only Library ZK part of it is handled here.  Library DK is still part of
        wf_subticket.
        """

        if DEBUG:
            print('wf_subticket_history subticket: {}'.format(subticket.library_dk))
        if not subticket:
            return False

        # for library dk part standard wf_subticket is called
        if not self.is_librarian_zk:
            if DEBUG:
                print('- not is_librarian_zk')
            return False

        # setup subticket and its wf
        subticket_id = subticket.getId()
        s_wf_status = subticket.wf_status
        subticket_review_state = s_wf_status['review_state']
        subticket_review_state_label = s_wf_status['review_state_label']

        # transitions
        subticket_transitions = []

        # požadavek na přechod
        dkhwf_subticket_id = self.request.get('dkhwf_subticket_id', None)
        if dkhwf_subticket_id == subticket_id:
            wf_subticket_submit = self.request.get('wf_subticket_history_submit', None)
            comment = self.text_from_request('comment_history')
            if comment:
                comment = comment.strip()
            else:
                comment = u''
        else:
            wf_subticket_submit = None
            comment = u''

        # požadavek na změnu stavu + comment = přidej i zprávu druhé knihovně,
        # na rozdíl od zprávy ale tady žádná status message a žádné přesměrování
        if wf_subticket_submit and comment:
            self.send_message_zk_dk(subticket_id, comment, info_message=False)

        # SUGGESTION don't use self

        if DEBUG:
            print('subticket_review_state: {}'.format(subticket_review_state))

        if subticket_review_state == 'sent':

            # history sent send_back
            if subticket.can_send_back:
                if wf_subticket_submit == 'send_back':

                    self.mark_librarian()

                    # send_back -> sent_back
                    wf_action(subticket, 'send_back', u'')
                    subticket.sent_back_date = self.dt_as(DateTime(), 'iso')
                    subticket.set_next_action()

                    message = u'Posláno zpět dožádané knihovně.'
                    self.add_message(message)

                    # unread
                    subticket.unread_dk = True
                    self.add_historylog(u'Dokument byl poslán zpět dožádané knihovně.', who=['zk', 'dk'])

                    self.send_notifications('wf_st_send_back')

                    self.ticket.reindexObject()
                    subticket.reindexObject()
                    self.redirect(self.context_url)
                    return True

                subticket_transitions.append(
                    dict(
                        id = 'send_back',
                        label = u'Posláno zpět DK',
                        btype = u'success',
                        )
                    )

            # FIXME-EDD cancel complaint
            print("-- EDD FIXME 006")

            # # history sent close by zk - only for MRS
            # if subticket.can_close:
            #     if wf_subticket_submit == 'close':

            #         self.mark_librarian()

            #         wf_action(subticket, 'close', u'')
            #         now_iso = self.dt_as(DateTime(), 'iso')
            #         subticket.closed_date = now_iso

            #         message = u'Uzavřeno'
            #         self.add_message(message)

            #         message = u'Požadavek úspěšně uzavřen.'
            #         self.add_message(message)

            #         # unread
            #         subticket.unread_dk = True

            #         self.ticket.reindexObject()
            #         self.subticket.reindexObject()

            #         self.send_notifications('wf_st_accept_back')

            #         #self.redirect(self.context_url)
            #         self.redirect(self.ticket.absolute_url())
            #         return True

            #     subticket_transitions.append(
            #         dict(
            #             id = 'close',
            #             label = u'Uzavřít',
            #             btype = u'danger',
            #             )
            #         )

        # url se použije jen v tal:condition, ale musí existovat, protože výše
        # url nastavujeme jen na '', tak když neexistuje, musíme přidělit
        # jakýkoli True, třeba id
        for transition in subticket_transitions:
            if 'url' not in transition:
                transition['url'] = transition['id']

        # we already have structure where we need to add this so find it and
        # modify it
        tab_st_history = None
        for item in self.tabs_st_history:
            if item['subticket_id'] == subticket_id:
                tab_st_history = item
                break
        if tab_st_history is None:
            raise Exception('Invalid internal state')
        tab_st_history['transitions'] = subticket_transitions

    #def wf_subticket(self):
    #    pass

    def _wf_queued_dk_accept(self):
        if self.wf_subticket_submit == 'accept':

            self.mark_librarian()

            wf_action(self.subticket, 'accept', u'')
            now_iso = self.dt_as(DateTime(), 'iso')
            self.subticket.decided_date = now_iso
            self.subticket.accepted_date = now_iso
            self.subticket.set_next_action()
            #self.subticket.decide_date = None

            message = u'Přijato knihovníkem.'
            self.add_message(message)

            # unread
            self.ticket.unread_zk = True
            self.add_historylog(u'Požadavek přijat.', who=['zk', 'dk'])

            self.send_notifications('wf_st_accept')

            self.ticket.reindexObject()
            self.subticket.reindexObject()
            self.redirect(self.context_url)
            return True

        self.subticket_transitions.append(
            dict(
                id = 'accept',
                label = u'Přijmout',
                btype = u'success',
                )
            )
        return False

    def _wf_queued_dk_conditionally_accept(self):
        cond_a_reason_comment = u''

        # načíst a zkontrolovat důvody podmíněného přijetí - jsou
        # mandatory pro odmítnutí požadavku
        if self.wf_subticket_submit == 'conditionally_accept':
            if self.ticket.is_mvs:
                # FIXME-MRS
                COND_A_REASON_LABEL = {
                    u'cond-a-fee': u'vyšší cena (Kč)',
                    u'cond-a-edition': u'jiné vydání',
                    u'cond-a-send_date': u'delší lhůta vyřízení (dny)',
                    u'cond-a-only-here': u'pouze k prezenčnímu studiu',
                    u'cond-a-return_d': u'zkrácená výpůjční lhůta (dny)',
                    }
                cond_a_reason_lov = (
                    u'cond-a-fee',
                    u'cond-a-edition',
                    u'cond-a-send_date',
                    u'cond-a-only-here',
                    u'cond-a-return_d',
                    )
            elif self.ticket.is_edd:
                COND_A_REASON_LABEL = {
                    #u'cond-a-fee': u'vyšší cena (Kč)',
                    u'cond-a-edition': u'jiné vydání',
                    u'cond-a-send_date': u'delší lhůta vyřízení (dny)',
                    #u'cond-a-only-here': u'pouze k prezenčnímu studiu',
                    #u'cond-a-return_d': u'zkrácená výpůjční lhůta (dny)',
                    }
                cond_a_reason_lov = (
                    #u'cond-a-fee',
                    u'cond-a-edition',
                    u'cond-a-send_date',
                    #u'cond-a-only-here',
                    #u'cond-a-return_d',
                    )
            cond_a_reason = self.text_from_request('cond_a_reason', lov=cond_a_reason_lov)
            if DEBUG:
                print("cond_a_reason: {}".format(cond_a_reason))

            # chybí (nebo nebyl validní) povinný důvod odmítnutí,
            #zastavit provádění odmítnutí
            if not cond_a_reason:
                message = u'Chybí důvod podmínečného přijetí, který je povinný.'
                self.add_message(message)
                self.wf_subticket_submit = None

            # dodatečné parametry zvoleného důvodu k odmítnutí,
            # pokud jsou k němu nějaké
            elif cond_a_reason == u'cond-a-fee':
                # Implicitní validace v int_from_request je nezáporné celé
                # číslo, jinak vrátí None, min None znamená, že range validace
                # proběhne až zde, mimo int_from_request
                value = self.int_from_request('cond_a_fee_value', min=None)
                if not value:
                    message = u'Chybí odhadovaná cena.'
                    self.add_message(message)
                    self.wf_subticket_submit = None
                elif value < 10:
                    message = u'Příliš nízká nebo záporná odhadovaná cena.'
                    self.add_message(message)
                    self.wf_subticket_submit = None
                elif value > 5000:
                    message = u'Příliš vysoká odhadovaná cena.'
                    self.add_message(message)
                    self.wf_subticket_submit = None
                else:
                    # valid
                    self.subticket.cond_a_reason = cond_a_reason
                    self.subticket.cond_a_fee = value
                    self.subticket.cena = value
                    cond_a_reason_comment = u'{}: {}'.format(
                        COND_A_REASON_LABEL.get(cond_a_reason, cond_a_reason),
                        value,
                        )

            elif cond_a_reason == u'cond-a-edition':
                value = self.text_from_request('cond_a_edition_value')
                if value:
                    self.subticket.cond_a_reason = cond_a_reason
                    self.subticket.cond_a_edition = value
                    cond_a_reason_comment = u'{}: {}'.format(
                        COND_A_REASON_LABEL.get(cond_a_reason, cond_a_reason),
                        value,
                        )
                else:
                    message = u'Chybí upřesnění vydání.'
                    self.add_message(message)
                    self.wf_subticket_submit = None

            elif cond_a_reason == u'cond-a-send_date':
                value = self.date_from_request('cond_a_send_date_value', result_type='iso')
                # some validation?
                if value:
                    self.subticket.cond_a_reason = cond_a_reason
                    self.subticket.cond_a_send_date = value
                    cond_a_reason_comment = u'{}: {}'.format(
                        COND_A_REASON_LABEL.get(cond_a_reason, cond_a_reason),
                        value,
                        )
                else:
                    message = u'Chybí upřesnění předpokládaného data odeslání.'
                    self.add_message(message)
                    self.wf_subticket_submit = None

            elif cond_a_reason == u'cond-a-only-here':
                self.subticket.cond_a_reason = cond_a_reason
                cond_a_reason_comment = u'{}'.format(
                    COND_A_REASON_LABEL.get(cond_a_reason, cond_a_reason),
                    )

            elif cond_a_reason == u'cond-a-return_d':
                # Implicitní validace v int_from_request je nezáporné celé
                # číslo, jinak vrátí None, min None znamená, že range validace
                # proběhne až zde, mimo int_from_request
                value = self.int_from_request('cond_a_return_d_value', min=None)
                if not value:
                    message = u'Chybí počet dnů od odeslání k vrácení do DK.'
                    self.add_message(message)
                    self.wf_subticket_submit = None
                # cond_a_return_d_value_min .. 5
                elif value < ziskej_parameter('cond_a_return_d_value_min'):
                    message = u'Příliš nízký nebo záporný počet dnů od odeslání k vrácení do DK.'
                    self.add_message(message)
                    self.wf_subticket_submit = None
                # st_return_min_days .. 28
                elif value >= ziskej_parameter('st_return_min_days'):
                    message = u'Příliš vysoký počet dnů od odeslání k vrácení do DK.'
                    self.add_message(message)
                    self.wf_subticket_submit = None
                else:
                    # valid
                    self.subticket.cond_a_reason = cond_a_reason
                    self.subticket.cond_a_return_d = value
                    cond_a_reason_comment = u'{}: {}'.format(
                        COND_A_REASON_LABEL.get(cond_a_reason, cond_a_reason),
                        value,
                        )

            else:
                raise Exception('Invalid input')

        # podmínečně přijmout požadavek, validace už proběhla
        if self.wf_subticket_submit == 'conditionally_accept':

            self.mark_librarian()

            wf_action(self.subticket, 'conditionally_accept', '')

            now_iso = self.dt_as(DateTime(), 'iso')
            self.subticket.set_next_action()
            self.subticket.decide_date = None
            self.subticket.cond_accepted_date = now_iso

            message = u'Podmíněně přijato knihovníkem DK, {}.'.format(cond_a_reason_comment)
            self.add_message(message)
            self.add_historylog(message, who=['zk', 'dk'])
            self.send_notifications('wf_st_conditionally_accept')

            self.ticket.unread_zk = True

            self.ticket.reindexObject()
            self.subticket.reindexObject()
            self.redirect(self.context_url)
            return True

        self.subticket_transitions.append(
            dict(
                id = 'conditionally_accept',
                label = u'Podmíněně přijmout',
                btype = u'warning modal-wf-details',  # warning x danger
                data_toggle = 'modal',
                data_target = '#modal-wf-dk-cond-a',
                )
            )
        return False

    def _wf_queued_dk_refuse(self, wf_state='queued'):
        # načíst a zkontrolovat důvody odmítnutí - jsou mandatory
        # pro odmítnutí požadavku
        if self.wf_subticket_submit == 'refuse':
            refuse_reason_lov = (
                u'refuse-reason-wrong-citation',
                u'refuse-reason-dk-impossible',
                u'refuse-reason-dk-unavailable',
                )
            refuse_reason = self.text_from_request('refuse_reason', lov=refuse_reason_lov)
            # chybí (nebo nebyl validní) povinný důvod odmítnutí,
            #zastavit provádění odmítnutí
            if not refuse_reason:
                message = u'Chybí důvod zamítnutí, který je povinný.'
                self.add_message(message)
                self.wf_subticket_submit = None
            else:
                self.subticket.refuse_reason = refuse_reason

        # odmítnutí požadavku, pokud je vše správně nastaveno, což
        # už bylo zkontrolováno
        if self.wf_subticket_submit == 'refuse':

            # -- starý subticket

            self.mark_librarian()

            wf_action(self.subticket, 'refuse', refuse_reason)

            self.subticket.set_next_action()

            now_iso = self.dt_as(DateTime(), 'iso')
            if not self.subticket.decided_date:
                self.subticket.decided_date = now_iso
            if not self.subticket.refused_date:
                self.subticket.refused_date = now_iso

            # unread
            self.ticket.unread_zk = True
            if wf_state in ('queued', ):
                self.send_notifications('wf_st_refuse')
                message = u'Odmítnuto knihovníkem DK (po předchozím podmínečném přijetí).'
            elif wf_state in ('conditionally_accepted', ):
                self.send_notifications('wf_st_refuse')
                message = u'Odmítnuto knihovníkem DK.'
            elif wf_state in ('accepted', ):
                self.send_notifications('wf_st_accepted_refuse')
                message = u'Odmítnuto knihovníkem DK (po předchozím přijetí).'
            else:
                raise Exception('Invalid state: {}'.format(wf_state))
            REFUSE_REASON = {
                u'refuse-reason-wrong-citation': u'chybná citace a nepodaří se dohledat správně',
                u'refuse-reason-dk-impossible': u'nelze realizovat',
                u'refuse-reason-dk-unavailable': u'momentálně nedostupné',
                }
            refuse_reason_label = REFUSE_REASON.get(refuse_reason, refuse_reason)
            message = u'{message}  Důvod odmítnutí: {reason}.'.format(message=message, reason=refuse_reason_label)
            self.add_message(message)
            self.add_historylog(message, who=['zk', 'dk'])
            self.subticket.reindexObject()

            # -- vytvořit nový subticket, assign_new je True, pokud se
            # to podařilo, self.subticket se v tom případě změní, jinak
            # zůstane beze změny
            old_subticket = self.subticket

            if refuse_reason in (u'refuse-reason-wrong-citation', ):
                assign_new = self.try_next_request('stop')
            else:
                assign_new = self.try_next_request('assign')

            # -- nový subticket, pokud se ho podařilo vytvořit

            if assign_new:
                self.subticket.unread_dk = True
                if wf_state in ('queued', 'conditionally_accepted', ):
                    self.send_notifications('wf_st_refuse_new')
                elif wf_state in ('accepted', ):
                    self.send_notifications('wf_st_accepted_refuse_new')
                self.subticket.reindexObject()

            self.ticket.reindexObject()
            self.redirect(old_subticket.absolute_url())
            return True

        self.subticket_transitions.append(
            dict(
                id = 'refuse',
                label = u'Odmítnout',
                btype = u'warning modal-wf-details',  # warning x danger
                data_toggle = 'modal',
                data_target = '#modal-wf-dk-refuse',
                )
            )
        return False

    def _wf_conditionally_accepted_zk_accept(self):
        self.display_wf_dk_subticket = True

        if self.wf_subticket_submit == 'accept':

            self.mark_librarian()

            wf_action(self.subticket, 'accept', u'')
            now_iso = self.dt_as(DateTime(), 'iso')
            self.subticket.decided_date = now_iso
            self.subticket.accepted_date = now_iso

            self.subticket.unread_dk = True

            message = u'ŽK přijala podmínky podmínečného přijetí DK, díky tomu je požadavek přijat.'
            self.add_message(message)
            self.add_historylog(message, who=['zk', 'dk'])
            self.send_notifications('wf_st_ca_accept')

            self.ticket.reindexObject()
            self.subticket.reindexObject()
            self.redirect(self.context_url)
            return True

        self.subticket_transitions.append(
            dict(
                id = 'accept',
                label = u'Přijmout',
                btype = u'success',
                )
            )
        return False

    def _wf_conditionally_accepted_zk_refuse(self):
        self.display_wf_dk_subticket = True

        if self.wf_subticket_submit == 'refuse':

            # -- starý subticket

            if DEBUG:
                print('self.subticket.library_dk OLD')
                print(self.subticket.library_dk)

            self.mark_librarian()

            wf_action(self.subticket, 'refuse', u'')
            now_iso = self.dt_as(DateTime(), 'iso')
            self.subticket.decided_date = now_iso
            self.subticket.refused_date = now_iso

            self.subticket.unread_dk = True
            self.send_notifications('wf_st_ca_retract')
            self.subticket.reindexObject()

            message = u'Podmínky DK nepřijaty, díky tomu je požadavek odmítnut.'
            self.add_message(message)

            # -- vytvořit nový subticket
            assign_new = self.try_next_request('stop')

            # -- nový subticket, pokud se ho podařilo vytvořit

            if DEBUG:
                print('self.subticket.library_dk NEW')
                print(self.subticket.library_dk)

            if assign_new:
                self.subticket.unread_dk = True
                self.send_notifications('wf_st_ca_retract_new')
                self.subticket.reindexObject()

            self.add_historylog(u'ŽK nepřijala podmínky podmínečného přijetí.', who=['zk', 'dk'])
            self.ticket.reindexObject()
            self.redirect(self.ticket.absolute_url())
            return True

        self.subticket_transitions.append(
            dict(
                id = 'refuse',
                label = u'Odmítnout',
                btype = u'danger',
                )
            )
        return False

    def _wf_conditionally_accepted_dk_refuse(self):
        return self._wf_queued_dk_refuse(wf_state='conditionally_accepted')

    def _wf_accepted_dk_send(self):
        self.st_lib_edit_btn = BTNCSS_DEFAULT
        if self.wf_subticket_submit in ('send', 'send_here', ):

            self.mark_librarian()

            wf_action(self.subticket, 'send', u'')

            # dates
            sent_date_dt = DateTime()
            # sent_date je text - např.: '2017-11-15'
            self.subticket.sent_date = self.dt_as(sent_date_dt, 'iso')

            if self.ticket.is_mvs:

                # create report entry
                obj_dict = dict(
                    report_date = sent_date_dt,
                    cena = self.subticket.cena,
                    user_id = self.user_id,
                    entry_type = u'std',
                    note = u'',
                    )
                report_entry = create_reportentry(self.subticket, obj_dict)  # is_std = True

                # report_date je DateTime - např.: DateTime('2017-11-15')
                self.subticket.report_date = sent_date_dt  # obsolete now

                # pokud bylo podmínečně přijato se zkrácenou výpůjční
                # lhůtou, tak použij počet dnů z ní, přestože by DK
                # nějaké datum vyplnila
                if self.subticket.cond_a_return_d:
                    self.subticket.back_date = datetime_add_d(self.subticket.cond_a_return_d).strftime('%Y-%m-%d')

                # jinak pokud DK nevyplní, použije se teď + 28d
                elif not self.subticket.back_date:
                    self.subticket.back_date = datetime_add_d(
                        ziskej_parameter('st_return_default_days')
                        ).strftime('%Y-%m-%d')

                self.subticket.set_next_action(
                    na_type='date_iso',
                    na_value=self.subticket.back_date,
                    na_text=u'Vrácení DK'
                    )

            if self.ticket.is_edd:
                self.subticket.is_sent_here = False
                message = u'Odesláno čtenáři.'
            elif self.wf_subticket_submit == 'send':
                self.subticket.is_sent_here = False
                message = u'Odesláno do ŽK.'
            else:
                self.subticket.is_sent_here = True
                message = u'Připraveno k vyzvednutí.'
            self.add_message(message)

            self.ticket.unread_zk = True

            if self.ticket.is_edd:
                self.add_historylog(
                    u'DK odeslala požadavek čtenáři.', who=['zk', 'dk'])
                self.send_notifications('wf_st_send_edd_zk')  # ŽK
                # Notifikaci čtenáři zatím neposílat, protože ještě není
                # vytvořená platba v platebátoru.
            elif self.wf_subticket_submit == 'send':
                self.add_historylog(
                    u'DK odeslala požadavek.', who=['zk', 'dk'])
                self.send_notifications('wf_st_send')
            else:
                self.add_historylog(
                    u'DK připravila požadavek.', who=['zk', 'dk'])
                self.send_notifications('wf_st_send_here')

            # Platby
            if self.ticket.is_mvs:
                #self.add_historylog(u'Požadavek byl zaplacen s využitím kreditu v NTK.', who=['zk', ])
                #self.add_historylog(u'Byly připsán poplatek za požadavek.', who=['dk', ])
                library_payment_mvs(self.subticket, report_entry)
            elif self.ticket.is_mrs:
                # FIXME-MRS
                pass
            elif self.ticket.is_edd:

                # Připravit ticket pro pčtenáře
                wf_action(self.ticket, 'prepare', u'Automaticky připraveno pro čtenáře odesláním PDF čtenáři v rámci požadavku.')

                # Vytvořit platbu v platebátoru
                library_payment_edd_create(self.subticket)

                # Nastavit reader token
                self.ticket.reader_token = u'{random_part}'.format(
                    random_part = str(uuid.uuid4()),
                    )

                # Poslat odkaz na stažení souboru, který až do zaplacení bude
                # informovat plus odkazovat na platbu v platebátoru
                self.send_notifications('wf_st_send_edd_r')

                # Nastavit expiraci PDF souboru
                dt_exp = DateTime() + ziskej_parameter('edd_expire_days')  # 21
                # no timezone
                pdt_exp = datetime.strptime(
                    dt_exp.strftime('%Y-%m-%d %-H:%M'), '%Y-%m-%d %H:%M')
                print("-- EDD DEBUG: set download_expiration_date to", pdt_exp)
                self.subticket.download_expiration_date = pdt_exp

                # Přidat záznam do audit logu, jen pro SC.
                self.add_historylog(u'Vytvořen předpis pro platbu čtenáře, odeslána notifikace.', who=[])

            self.ticket.reindexObject()
            self.subticket.reindexObject()
            self.redirect(self.context_url)
            return True

        if self.ticket.is_edd:
            self.subticket_transitions.append(
                dict(
                    id = 'send',
                    label = u'Vyřízeno, odeslat PDF čtenáři',
                    btype = u'success',
                    )
                )
        elif self.subticket.doc_sent == u'sent':
            self.subticket_transitions.append(
                dict(
                    id = 'send',
                    label = u'Vyřízeno, odesláno do žádající knihovny',
                    btype = u'success',
                    )
                )
        elif self.subticket.doc_sent == u'pick':
            self.subticket_transitions.append(
                dict(
                    id = 'send_here',
                    label = u'Vyřízeno, připraveno k vyzvednutí',
                    btype = u'success',
                    )
                )
        else:
            raise Exception(u'Není stanoveno, zda poslat poštou nebo k vyzvednutí.')
        return False

    def _wf_accepted_dk_send_cannot(self):
        self.st_lib_edit_btn = BTNCSS_PRIMARY
        hint = u''
        if self.ticket.is_mvs:
            if self.subticket.cena is None or self.subticket.cena == u'':
                hint += u'Není vyplněná cena.  '
            #elif self.subticket.cena > FIXME_max:
            #    hint += u'Je vyplněná cena, která je vyšší, než maximální cena vaší knihovny.  '
            #if self.subticket.back_date is None or self.subticket.back_date == u'':
            #    hint += u'Není vyplněný termín vrácení do DK.  '

        elif self.ticket.is_mrs:
            # FIXME-MRS
            pass

        elif self.ticket.is_edd:
            # Hodnota 0 nemůže nastat a pokud by nastala, má stejný význam jako
            # None
            if not self.subticket.pages_number_dk:
                hint += u'Není vyplněný skutečný počet stran.  '
            if not self.subticket.Is_pdf_uploaded():
                hint += u'Není vložený PDF soubor.  '

        self.subticket_transitions.append(
            dict(
                id = '',
                label = u'Nelze poslat.  ' + hint,
                btype = u'default',
                url = '',
                )
            )
        return False

    def _wf_accepted_dk_refuse(self):
        return self._wf_queued_dk_refuse(wf_state='accepted')

    def _wf_sent_zk_send_back(self):
        self.display_wf_dk_subticket = True

        if self.wf_subticket_submit == 'send_back':


            self.mark_librarian()

            # send_back -> sent_back
            wf_action(self.subticket, 'send_back', u'')
            self.subticket.sent_back_date = self.dt_as(DateTime(), 'iso')
            self.subticket.set_next_action()

            message = u'Posláno zpět dožádané knihovně.'
            self.add_message(message)

            # unread
            self.subticket.unread_dk = True
            if self.is_ziskej_operator:  # FIXME-sudo
                self.ticket.unread_zk = True
                self.add_historylog(u'Posláno zpět dožádané knihovně. (Operátor)',who=[])
                # self.send_notifications('wf_st_send_back')  # FIXME-sudo
            else:
                self.add_historylog(u'Posláno zpět dožádané knihovně.', who=['zk', 'dk'])
                self.send_notifications('wf_st_send_back')



            self.ticket.reindexObject()
            self.subticket.reindexObject()
            self.redirect(self.context_url)
            return True

        self.subticket_transitions.append(
            dict(
                id = 'send_back',
                label = u'Posláno zpět DK',
                btype = u'success',
                )
            )
        return False

    def _wf_sent_back_dk_close(self, is_operator=False):
        if self.wf_subticket_submit == 'close':

            # příznak, zda se jedná o hlavní požadavek objednávky a objednávka
            # není uzavřená, protože na tuto podmínky jsou vázány další akce
            is_main_subticket = bool(
                (self.subticket.getId() == self.ticket.subticket_id) and
                self.ticket.can_close
                )


            self.mark_librarian()

            wf_action(self.subticket, 'close', u'')
            now_iso = self.dt_as(DateTime(), 'iso')
            self.subticket.closed_date = now_iso
            # FIXME není to jen pro is_main_subticket?
            self.ticket.closed_date = now_iso

            message = u'V pořádku vráceno zpět dožádané knihovně.'
            self.add_message(message)

            # ticket close -> closed END
            if is_main_subticket:
                wf_action(self.ticket, 'close', u'Automaticky uzavřeno vrácením dožádané knihovně.')

            message = u'Požadavek úspěšně uzavřen.'
            self.add_message(message)

            # unread
            self.ticket.unread_zk = True
            if is_main_subticket:
                self.ticket.unread_r = True
            self.add_historylog(u'V pořádku vráceno zpět dožádané knihovně.', who=['zk', 'dk'])
            if is_main_subticket:
                self.add_historylog(u'<strong>Objednávka byla uzavřena</strong> uzavřením hlavního požadavku', who=['zk'])
            
            if is_main_subticket:
                self.send_notifications('wf_st_accept_back_main')
            else:
                self.send_notifications('wf_st_accept_back_other')

            self.ticket.reindexObject()
            self.subticket.reindexObject()
            #self.redirect(self.context_url)
            self.redirect(self.ticket.absolute_url())
            return True

        self.subticket_transitions.append(
            dict(
                id = 'close',
                label = u'Uzavřít',
                btype = u'success',
                )
            )
        return False


# FIXME-EDD

# # sent close MRS
# if self.subticket.can_close:
#     if self.wf_subticket_submit == 'close':

#         self.mark_librarian()

#         wf_action(self.subticket, 'close', u'')
#         now_iso = self.dt_as(DateTime(), 'iso')
#         self.subticket.closed_date = now_iso
#         self.ticket.closed_date = now_iso

#         message = u'Uzavřeno vyřízením.'
#         self.add_message(message)

#         # ticket close -> closed END
#         # FIXME-WORKFLOW
#         wf_action(self.ticket, 'close', u'Automaticky uzavřeno vrácením dožádané knihovně.')

#         message = u'Požadavek úspěšně uzavřen.'
#         self.add_message(message)

#         # unread
#         self.subticket.unread_dk = True

#         self.ticket.reindexObject()
#         self.subticket.reindexObject()

#         self.send_notifications('wf_st_accept_back')

#         #self.redirect(self.context_url)
#         self.redirect(self.ticket.absolute_url())
#         return True

#     self.subticket_transitions.append(
#         dict(
#             id = 'close',
#             label = u'Uzavřít',
#             btype = u'default',
#             )
#         )

# sent cancel aka Malé storno
# not included in standard workflow

# elif self.is_librarian_dk:

#     if self.subticket.can_refuse:
#         if self.wf_subticket_submit == 'refuse':

#             # -- starý subticket

#             if DEBUG:
#                 print('self.subticket.library_dk OLD')
#                 print(self.subticket.library_dk)

#             self.mark_librarian()

#             # refuse -> refused
#             wf_action(self.subticket, self.wf_subticket_submit, u'')
#             self.subticket.set_next_action()

#             # unread
#             self.ticket.unread_zk = True
#             self.subticket.reindexObject()
#             self.send_notifications('wf_st_sent_refuse')

#             message = u'K odeslání nedošlo a nemůže dojít, odmítnuto knihovníkem DK (po předchozím odeslání).'
#             self.add_message(message)

#             # -- vytvořit nový subticket, assign_new je True, pokud se
#             # to podařilo, self.subticket se v tom případě změní, jinak
#             # zůstane beze změny
#             assign_new = self.try_next_request()

#             # -- nový subticket, pokud se ho podařilo vytvořit

#             if DEBUG:
#                 print('self.subticket.library_dk NEW')
#                 print(self.subticket.library_dk)

#             if assign_new:
#                 # unread
#                 self.subticket.unread_dk = True
#                 self.subticket.reindexObject()
#                 self.send_notifications('wf_st_sent_refuse_new')

#             self.ticket.reindexObject()

#             self.redirect(self.ticket.absolute_url())
#             return True

#         self.subticket_transitions.append(
#             dict(
#                 id = 'refuse',
#                 label = u'K odeslání nedošlo a nemůže dojít, odmítnout',
#                 btype = u'danger',
#                 )
#             )
