# -*- coding: utf-8 -*-
"""BrowserView: Ticket / Subticket.

Sdílený kód pro Ticket a Subticket.
"""

from __future__ import print_function

import logging

from DateTime import DateTime

from ziskej.site.browser.ticket.ticket_core_cancel_complaint import (
    TicketCoreCancelComplaintView,
    BTNCSS_DEFAULT,
    BTNCSS_PRIMARY,
    )
from ziskej.site.utils_lib import pprint
from ziskej.site.ticket_helpers import (
    create_subticketmvs,
    wf_action,
    )
from ziskej.site.ticket_load_balancing import load_balancing_dk_list


logger = logging.getLogger("ziskej")

DEBUG = False


class TicketCoreView(TicketCoreCancelComplaintView):

    def setup_primary_btn(self):
        # js way
        # self.js_vars += u'''$('button[data-target="#modal-reader-update"]').removeClass('btn-default').addClass('btn-success');\n'''

        # ticket and subticket workflow state

        ticket = self.ticket
        subticket = self.subticket

        self.datapicker_reader_max = self.today_add_d_datapicker(90)

        # 'created', 'assigned', 'pending', 'prepared', 'closed', 'cancelled', 'rejected'
        twf = ticket.wf_review_state

        if subticket:
            # 'queued', 'conditionally_accepted', 'accepted', 'sent', 'sent_back', 'closed', 'cancelled', 'refused'
            stwf = subticket.wf_review_state
        else:
            stwf = None

        # Reader
        if self.is_reader:
            if self.ticket.is_big_cancel_asked:
                self.help_html = u'<p>Byla podána žádost o storno objednávky a čeká se na odpověď.</p>'

            elif twf in ('created', ):
                self.help_html = u'<p>Objednávka byla vytvořena, nyní se čeká na její zpracování.  V první řadě projde schválením a nastavením ve vaší mateřské knihovně.  Váš knihovník vás bude informovat o termínu připravení k vyzvednutí prostřednictvím e-mailové notifikace ze systému ZÍSKEJ.  Informace budou zobrazeny i na této adrese.</p>'

            elif twf in ('assigned', 'pending', ):
                self.help_html = u'<p>Objednávka byla vytvořena, nyní se čeká na její zpracování.  Prošla schválením a nastavením ve vaší mateřské knihovně.  Váš knihovník vás bude informovat o termínu připravení k vyzvednutí prostřednictvím e-mailové notifikace ze systému ZÍSKEJ.  Informace budou zobrazeny i na této adrese.</p>'

            elif twf in ('prepared', ) and not ticket.is_reader_lent:

                reader_from_date = self.ticket.reader_from_date
                if reader_from_date:
                    reader_from_date = DateTime(reader_from_date).strftime('%-d.%-m.%Y')
                else:
                    reader_from_date = u''

                reader_to_date = self.ticket.reader_to_date
                if reader_to_date:
                    reader_to_date = DateTime(reader_to_date).strftime('%-d.%-m.%Y')
                else:
                    reader_to_date = u''

                reader_fee = self.ticket.reader_fee
                if not reader_fee:
                    reader_fee = u''

                reader_place = self.ticket.reader_place
                if not reader_place:
                    reader_place = u''

                reader_note = self.ticket.reader_note
                if  not reader_note:
                    reader_note = u''

                self.help_html = u'''
                <p>Objednávka je pro vás připravena od {}.</p>
                <p>Vrátit ji je potřeba do {}.</p>
                
                '''.format(reader_from_date, reader_to_date,)

            elif twf in ('prepared', ) and ticket.is_reader_lent and not ticket.is_reader_return:

                # FIXME-MRS
                # FIXME-EDD prepared
                print("-- EDD FIXME 003")

                reader_to_date = self.ticket.reader_to_date
                if reader_to_date:
                    reader_to_date = DateTime(reader_to_date).strftime('%-d.%-m.%Y')
                else:
                    reader_to_date = u''

                self.help_html = u'''
                <p>Objedávku je potřeba vrátit do {}.</p>
                <p>Cena</p>
                '''.format(reader_to_date, )

            elif twf in ('prepared', ) and ticket.is_reader_lent and ticket.is_reader_return:
                self.help_html = u'''
                <p>Objedávka je uzavřena.</p>
                '''

        # ZK/op
        elif self.is_librarian_zk:

            if self.ticket.is_big_cancel_asked:
                self.btncss_reader_ticket = BTNCSS_DEFAULT
                self.help_html = u'''
                <p>Byla podána žádost o storno objednávky. Dk se nyní může rozhodnout jestli storno přijme, nebo ne.
                Pokud ho přijme, tak se celá objednávka stornuje. Pokuho ho odmítne, tak objednávka bude pokračovat normálně dál.</p>
                '''

            elif twf in ('created', ):
                self.reader_approval_disabled = False
                self.show_load_availability = True

                if self.ticket.ticket_type in ('mrs', 'edd', ):
                    self.show_manual_edit = True
                    self.show_manual_edd_full_edit = self.ticket.doc_manual
                else:
                    self.show_manual_edit = self.ticket.doc_manual

                # if DEBUG and self.show_manual_edit:
                #     print('obj')
                #     for k in ('doc_title', 'doc_number', 'doc_author', 'doc_issuer', 'doc_isbn',
                #             'doc_issn', 'doc_signature'):
                #         print(k, getattr(self.ticket, k, None))
                #     print(' ')

                self.show_manual_lb = True
                self.show_reader_approval = True

                self._check_show_load_availability()

                slots_html = dict()
                if self.show_load_availability:
                    slots_html['auto_manual_dk'] = u'''<li><strong>Zvolit</strong> automatické nebo manuální přidělení DK, viz níže</li>'''
                    slots_html['auto_dk'] = u'''
                    <p>Automatické přidělení DK</p>
                    <ol class="list-group">
                        <li><strong>Načíst dostupnost</strong>: zjistí informace z CPK o dostupnosti v jednotlivých knihovnách a navrhne pořadí oslovování dožádaných knihoven</li>
                        <li><strong>Upravit</strong> pořadí DK</li>
                    </ol>
                    '''
                    slots_html['manual_dk'] = u'''
                    <p>Manuální přidělení DK</p>
                    <ol class="list-group">
                        <li><strong>Manuálně zvolit DK</strong></li>
                    </ol>
                    '''
                else:
                    slots_html['auto_manual_dk'] = u'''<li><strong>Manuálně zvolit DK</strong></li>'''
                    slots_html['auto_dk'] = u''
                    slots_html['manual_dk'] = u''
                self.help_html = u'''
                <p>Objednávka byla vytvořena a nyní je na vás ji připravit a schválit nebo odmítnout.</p>
                <p>Příprava pro schválení a přiřazení dožádané knihovně (DK):</p>
                <ol class="list-group">
                    <li><strong>Upravit</strong> informace o čtenáři (volitelné): ID čtenáře, poznámka knihovníka</li>
                    {auto_manual_dk}
                    <li><strong>Změnit stav</strong>: Schválit</li>
                </ol>
                {auto_dk}
                {manual_dk}
                <p>Po schválení dojde k vytvoření požadavku a přiřazení dožádané knihovně (DK), která jej může přijmout nebo zamítnout.</p>
                '''.format(**slots_html)
    
                if not self.ticket.is_reader_approved:
                    self.btncss_reader_update = BTNCSS_PRIMARY
                elif self.btncss_reader_wf != BTNCSS_DEFAULT:
                    pass
                elif self.ticket.ticket_doc_data_source() == 'manual' and not self.subticket and not self.subtickets and not self.ticket.lb_manual_library:
                    self.btncss_manual_lb = BTNCSS_PRIMARY
                elif not self.subticket and not self.subtickets and not self.ticket.doc_order and not self.ticket.doc_order_suggested:
                    if self.ticket.lb_state == u'auto: load availability finished failed':
                        self.btncss_load_availability = BTNCSS_DEFAULT
                        self.btncss_manual_lb = BTNCSS_PRIMARY
                    else:
                        self.btncss_load_availability = BTNCSS_PRIMARY
                elif not self.subticket and not self.subtickets and not self.ticket.doc_order:
                    self.js_vars += u'''$('button[name="submit_dk"]').removeClass('btn-default').addClass('btn-success');\n'''
                elif self.subticket and self.display_wf_dk_subticket:
                    self.btncss_dkcurrent_wf = BTNCSS_PRIMARY
                elif not self.ticket.reader_lid:
                    self.btncss_reader_update = BTNCSS_PRIMARY

            elif twf in ('assigned', ):
                self.btncss_reader_ticket = BTNCSS_DEFAULT
                self.btncss_reader_update = BTNCSS_DEFAULT
                #self.show_load_availability = True
                #self.show_manual_lb =True
                
                if not self.subticket:
                    # should be already logged
                    logger.warning('setup_primary_btn: wf state assigned and no subticket for ticket {hid} at {url}'.format(
                        hid=self.ticket.hid,
                        url=self.ticket.absolute_url(),
                        ))
                    return

                if self.subticket.is_small_cancel_asked:
                    self.btncss_reader_ticket = BTNCSS_DEFAULT
                    self.help_html = u'''
                    <p>Byla podána žádost o storno požadavku. Dk se nyní může rozhodnout jestli storno přijme, nebo ne.
                    Pokud ho přijme, tak se požadavek stornuje. Pokuho ho odmítne, tak požadavek bude pokračovat normálně dál</p>
                    '''
                elif stwf in ('queued', ):
                    #self.show_load_availability = True

                    self.help_html = u'''
                    <p>Objednávka byla vytvořena a schválena, byl vytvořen požadavek a přiřazen dožádané knihovně (DK), která jej může přijmout nebo zamítnout.  Termín pro toto rozhodnutí je 1 pracovní den.</p>
                    '''

                elif stwf in ('conditionally_accepted', ):
                    #self.show_load_availability = True

                    self.help_html = u'''
                    <p>Dožádané knihovna (DK) požadavek ani nepřijala ani neodmítla, ale podmínečně přijala za dodatečných podmínek.  Pokud tyto podmínky přijmete, je požadavek automaticky přijat.  Pokud je odmítnete, je automaticky zamítnut.  Můžete také na podmínky jen odpovědět a rozhodnutí nechat na DK.</p>
                    '''

                elif stwf in ('accepted', ):
                    if self.ticket.is_mvs:
                        self.help_html = u'''
                        <p>Dožádaná knihovna (DK) požadavek přijala.  Nyní jej připravuje k odeslání.  Poté, co určí jeho cenu, požadovaný termín vrácení a změní stav (Odeslat), uvidíte více informací a budete moci s požadavkem dál pracovat.</p>
                        '''
                    elif self.ticket.is_edd:
                        # self.help_html = u'''
                        # <p>Dožádaná knihovna (DK) požadavek přijala.  Nyní jej připravuje k odeslání.  Poté, co připraví PDF soubor a změní stav (Odeslat), uvidíte více informací a budete moci s požadavkem dál pracovat.</p>
                        # '''
                        self.help_html = u'''
                        <p>Dožádaná knihovna požadavek přijala. Nyní jej připravuje k odeslání čtenáři.</p>
                        '''
                elif stwf in ('sent', ):
                    self.btncss_reader_update = BTNCSS_DEFAULT
                    if self.ticket.is_mvs:
                        self.show_reader_date_update = True
                        self.show_reader_pickup_info = True

                        if not self.ticket.reader_to_date or \
                                not self.ticket.reader_from_date or \
                                not self.ticket.reader_fee or \
                                not self.ticket.reader_place:
                            self.btncss_reader_ticket = BTNCSS_PRIMARY
                        if self.ticket.can_prepare:
                            self.btncss_reader_ticket = BTNCSS_DEFAULT
                        self.help_html = u'''
                        <p>Požadavek byl odeslán z Dožádané knihovny (DK).</p>
                        <p>Další kroky:</p>
                        <ol class="list-group">
                            <li><strong>Upravit detail objednávky</strong> pro čtenáře: kdy bude připraveno k vyzvednutí, kdy je potřeba vrátit zpět, cena a místo k vyzvednutí</li>
                            <li><strong>Změnit stav</strong>: Připraveno pro čtenáře</li>
                        </ol>
                        <p>K odeslání e-mailové notifikace čtenáři dojde po změně stavu <em>Připraveno pro čtenáře</em>.</p>
                        '''
                        # self.help_html = u'''
                        #      <p>Požadavek byl odeslán z Dožádané knihovny (DK).</p>
                        #      <p>Další kroky:</p>
                        #      <ol class="list-group">
                        #          <li><strong>Upravit detail objednávky</strong> pro čtenáře: kdy bude připraveno k vyzvednutí, kdy je potřeba vrátit zpět, cena a místo k vyzvednutí</li>
                        #          <li><strong>Změnit stav</strong>: Připraveno pro čtenáře</li>
                        #      </ol>
                        #      <p>K odeslání e-mailové notifikace čtenáři dojde po změně stavu <em>Připraveno pro čtenáře</em>.</p>
                        #      '''
                    elif self.ticket.is_mrs:
                        pass  # FIXME-MRS
                    elif self.ticket.is_edd:
                        pass  # FIXME-EDD, pravděpodobně tady nic
                        # print("-- EDD FIXME 017a")

            elif twf == 'pending':
                self.show_load_availability = True
                self.show_manual_edit = bool(
                    self.ticket.doc_manual or
                    self.ticket.ticket_type in ('mrs', 'edd', ))
                self.show_manual_lb = True

                self._check_show_load_availability()

                self.help_html = u''  # FIXME

            elif twf == 'prepared':
                if self.ticket.is_mvs:
                    self.show_reader_pickup_info = True

                    if not ticket.is_reader_lent:
                        self.show_reader_date_update = True
                        self.reader_note_editable = True
                        self.reader_to_date_editable = True
                        self.help_html = u'''
                        <p>Objednávka byla nebo je připravena pro čtenáře (viz časová osa).  Čtenáři byla odeslána e-mailová notifikace s touto informací.</p>
                        '''
                    elif not ticket.is_reader_return:  # FIXME-MRS
                        self.show_reader_date_update = True
                        self.reader_to_date_editable = True
                        self.help_html = u'''
                        <p>Objednávka byla půjčena čtenáři.</p>
                        '''

                    else:

                        self.help_html = u'''
                        <p>Čtenář vrátil nebo nevyzvedl.  Je možné odeslat zpět dožádané knhovně (DK).  Následně změňte stav <em>Posláno zpět dožádané knihovně</em>.</p>
                        '''

                elif self.ticket.is_mrs:
                    pass  # FIXME-MRS

                elif self.ticket.is_edd:
                    self.help_html = u'''
                    <p>Dožádaná knihovna připravila PDF soubor pro čtenáře. Čtenář obdržel e-mail s informacemi o platbě a s odkazem ke stažení.</p>
                    '''

            elif twf == 'closed' and stwf not in ('closed', ):
                self.show_reader_pickup_info = True
                if self.ticket.is_mvs:
                    self.help_html = u'''
                    <p>Odesláno zpět dožádané knhovně (DK).</p>
                    '''

            elif twf == 'closed':
                self.show_reader_pickup_info = True
                # self.help_html = u'''
                # <p>Požadavek byl vrácen a tím je úspěšně uzavřen.</p>
                # '''

            elif twf == 'cancelled':
                pass
                # self.help_html = u'''
                # <p>Požadavek byl stornován.</p>
                # '''

            elif twf == 'rejected':
                self.help_html = u'''
                <p>Požadavek byl zamítnut.</p>
                '''

            else:
                raise Exception('Invalid internal state')

        # ZK/op
        elif self.is_librarian_dk:

            if stwf is None:
                pass

            elif self.subticket.is_small_cancel_asked:
                self.btncss_reader_ticket = BTNCSS_DEFAULT
                self.help_html = u'''
                <p>Byla podána žádost o storno požadavku. Nyní se může rozhodnout jestli storno přijmete, nebo ne.
                Pokud ho přijmete, tak se požadavek stornuje a nebude účtován. Pokuho ho odmítne, tak požadavek bude pokračovat standardně dál. </p>
            '''
                
            elif stwf in ('queued', ):
                self.help_html = u''

                #u'''
                #<p>Máte nový požadavek ve frontě.  Je potřeba jej přijmout nebo odmítnout v určené lhůtě.  Je také #možné jej přijmout podmínečně.</p>
                #'''

            elif stwf in ('conditionally_accepted', ):
                self.help_html = u'''
                <p>Požadavek jste přijali s podmínkou.  Nyní se čeká na žádající knihovnu (ŽK), které podmínky buď přijme a v tom případě je požadavek přijat, nebo je odmítne a v tom případě je požadavek odmítnut.  Stále jej můžete odmítnout přímo.</p>
                '''

            elif stwf in ('accepted', ):
                if self.subticket.is_mvs:
                    if self.subticket.doc_sent == 'sent':
                        self.help_html = u'''
                        <p>Požadavek byl přijat. Nyní jej připravte, doplňte požadovaný termín vrácení a odešlete. Tyto informace ŽK uvidí hned, ale stále je můžete měnit, dokud požadavek neodešlete.</p>
                        '''
                    elif self.subticket.doc_sent == 'pick':
                        self.help_html = u'''
                        <p> Požadavek byl přijat. Nyní jej připravte, doplňte požadovaný termín vrácení a připravte k vyzvednutí. Tyto informace ŽK uvidí hned, ale stále je můžete měnit, dokud požadavek nepřipravíte k vyzvednutí. </p>
                        '''
                elif self.subticket.is_mrs:
                    pass  # FIXME-MRS
                elif self.subticket.is_edd:
                    self.help_html = u'''
                        <p>Požadavek byl přijat.  Nyní jej připravte, vložte PDF a upřesněte skutečný počet stran.  Skutečný počet stran uvidí ŽK hned, ale stále jej můžete měnit, dokud neodešlete PDF čtenáři.  PDF soubor není nikomu viditelný, dokud neodešlete PDF čtenáři a dokud čtenář nezaplatí příslušný poplatek, pak je po stanovenou dobu přístupný čtenáři ke stažení.</p>
                    '''

            elif stwf in ('sent', ):
                self.help_html = u'''
                <p>Požadavek byl odeslán, tím je na vaší straně vyřízen.</p>
                '''
                if self.subticket.complaint_state == 'soft':
                    self.st_lib_edit_btn = BTNCSS_PRIMARY
                    self.help_html = u''
                elif self.subticket.complaint_state == 'soft_ready':
                    self.help_html = u''
                else:
                    pass

            elif stwf in ('sent_back', ):
                self.help_html = u'''
                <p>ŽK odeslala dokument zpět.</p>
                '''

            elif stwf in ('closed', ):
                self.help_html = u'''
                <p>Požadavek byl uzavřen.</p>
                '''

            elif stwf in ('cancelled', ):
                self.help_html = u'''
                <p>Požadavek byl stornován a nebude započítán v reportech.</p>
                '''

            elif stwf in ('refused', ):
                self.help_html = u'''
                <p>Požadavek byl odmítnut.</p>
                '''

        elif self.is_ziskej_operator:
            self.help_html = u''

        if self.is_ticket and self.subticket and self.subticket.back_date:
            self.datapicker_reader_max = self.date_datapicker(self.subticket.back_date)
    
    def _check_show_load_availability(self):
        if self.is_ticket and self.show_load_availability:
            if self.ticket.is_mvs or self.ticket.is_mrs:
                self.show_load_availability = self.show_load_availability and not self.ticket.doc_manual
            elif self.ticket.is_edd:
                # FIXME-EDD prověřit, zda už je finální zadání
                # Implementace novějšího zadání
                if len(self.ticket.get_unit_ids()):
                    pass
                else:
                    self.show_load_availability = False
                # Implemetace dřívějšího zadání
                # # Pro EDD neumožnit načítání dostupnosti (a tím AutoLB)
                # # s výjimkou EDD auto výňatek + má ISBN
                # if self.ticket.doc_manual:
                #     self.show_load_availability = False
                # elif self.ticket.is_edd_article:
                #     self.show_load_availability = False
                # elif self.ticket.doc_isbn:
                #     pass
                # else:
                #     self.show_load_availability = False

    def format_timeline_date(self, obj, datename, label, titype='past', default=None):
        if obj is None:
            return u''
        value = getattr(obj, datename, None)
        if value:
            value = self.date_format(value, omit_year=True)
        else:
            if default is None:
                return u''
            value = default

        if titype == 'past':
            tiicon = u'fa-calendar-check-o'
        elif titype == 'future':
            tiicon = u'fa-calendar-o'
        else:
            tiicon = u'fa-calendar-o'

        result = u'''
            <p>
              <i class="fa {tiicon}" aria-hidden="true"></i>
              <span title="{label}">{value}</span> ({label}) <br>
            </p>
            '''.format(label=label, value=value, tiicon=tiicon)
        return result

    def setup_timeline(self):
        ticket = self.ticket
        result = u''
        result_future = u''

        wf_status = ticket.wf_status

        # workflow state of ticket
        ticket_review_state = wf_status['review_state']

        # workflow state of active subticket of this ticket
        # only for reader and ZK (or Operator in ZK view)
        subticket_review_state = wf_status['subticket_review_state']

        # Reader timeline
        if self.is_reader:
            result += self.format_timeline_date(ticket, 'created_date', u'Vytvořena')

            result += self.format_timeline_date(ticket, 'payment_paid_date', u'Zaplacena')

            result += self.format_timeline_date(ticket, 'accepted_date', u'Přijata')

            # Čtenář: Storno
            result += self.format_timeline_date(ticket, 'cancellation_big_ask_date', u'Požádáno o storno')

            result += self.format_timeline_date(ticket, 'reader_mail_date', u'Mail čtenáři odeslán')

            if ticket_review_state in ('prepared', 'closed', ):
                value_html = self.format_timeline_date(ticket, 'reader_from_date', u'Připraveno od', titype='future')
                if not ticket.is_reader_lent:
                    result_future += value_html
                result += value_html

            value_html = self.format_timeline_date(ticket, 'reader_date', u'Požadováno čtenářem', titype='future')
            if not ticket.is_reader_lent:
                result_future += value_html
            result += value_html

            result += self.format_timeline_date(ticket, 'reader_lent_date', u'Půjčeno čtenáři')

            # Čtenář: Reklamace B) Došlo něco jiného
            result += self.format_timeline_date(ticket, 'complaint_date', u'Reklamace')

            if ticket_review_state in ('prepared', 'closed', ):
                value_html = self.format_timeline_date(ticket, 'reader_to_date', u'Vrátit do', titype='future')
                if not ticket.is_reader_return:
                    result_future += value_html
                result += value_html
        
            result += self.format_timeline_date(ticket, 'return_date', u'Vráceno')

        # ticket ZK/op timeline
        elif self.is_ticket and self.is_librarian_zk_op:
            result += u'<h4><small>Čtenář</small></h4>'
            result_future += u'<h4><small>Čtenář</small></h4>'

            result += self.format_timeline_date(ticket, 'created_date', u'Vytvořen')

            result += self.format_timeline_date(ticket, 'payment_paid_date', u'Zaplacen čtenářem')

            result += self.format_timeline_date(ticket, 'approved_date', u'Schválen')

            # ŽK: Velké storno
            result += self.format_timeline_date(ticket, 'cancellation_big_ask_date', u'Požádáno o storno')

            result += self.format_timeline_date(ticket, 'reader_mail_date', u'Mail čtenáři odeslán')

            value_html = self.format_timeline_date(ticket, 'reader_from_date', u'Připraveno od', titype='future')
            if not ticket.is_reader_lent:
                result_future += value_html
            result += value_html

            value_html = self.format_timeline_date(ticket, 'reader_date', u'Požadováno čtenářem', titype='future')
            if not ticket.is_reader_lent:
                result_future += value_html
            result += value_html

            result += self.format_timeline_date(ticket, 'reader_lent_date', u'Půjčeno čtenáři')

            # Čtenář: Reklamace
            result += self.format_timeline_date(ticket, 'complaint_date', u'Reklamace')

            value_html = self.format_timeline_date(ticket, 'reader_to_date', u'Vrátit do', titype='future')
            if not ticket.is_reader_return:
                result_future += value_html
            result += value_html

            result += self.format_timeline_date(ticket, 'return_date', u'Vráceno')

            # Previous subtickets
            subticket_nr = 0
            for subticket in self.subtickets:

                # toto by nemělo nastat, ale nastalo by po vytvoření prvního
                # subticketu při redirectu, když by z důvodu nějaké chyby
                # nedošlo k ukončení před setup_timeline
                if not subticket:
                    logger.warning('self.subtickets contains empty item')
                    #print(self.subtickets)
                    continue

                if self.subticket and (subticket.getId() == self.subticket.getId()):
                    continue
                subticket_nr += 1
                result_subticket = u''
                result_subticket += self.format_timeline_date(subticket, 'assigned_date', u'Přidělen')
                result_subticket += self.format_timeline_date(subticket, 'decide_date', u'Přijmout do', titype='future')
                result_subticket = self.format_timeline_date(subticket, 'cond_accepted_date', u'Podmínečně přijat')
                result_subticket_accepted = self.format_timeline_date(subticket, 'accepted_date', u'Přijat')
                if result_subticket_accepted:
                    result_subticket += result_subticket_accepted
                result_subticket += self.format_timeline_date(subticket, 'refused_date', u'Odmítnut')
                # ŽK: Velké storno
                result_subticket += self.format_timeline_date(subticket, 'cancellation_small_ask_date', u'Požádáno o storno')
                result_subticket += self.format_timeline_date(subticket, 'sent_date', u'Odeslán')
                # ŽK: Reklamace
                result_subticket += self.format_timeline_date(subticket, 'complaint_date', u'Reklamace')
                if self.ticket.is_mvs:
                    result_subticket += self.format_timeline_date(subticket, 'back_date', u'Požadováno zpět', titype='future')
                    result_subticket += self.format_timeline_date(subticket, 'sent_back_date', u'Odeslán zpět')
                result_subticket += self.format_timeline_date(subticket, 'closed_date', u'Uzavřeno')
                if result_subticket:
                    subticket_sigla = u''
                    if subticket.library_dk:
                        subticket_sigla = subticket.library_dk.upper()
                    result += u'<h4><small>{nr}. {sigla}</small></h4>'.format(nr=subticket_nr, sigla=subticket_sigla)
                    result += result_subticket

            # Active subticket (None if not exist)
            subticket = self.subticket
            if subticket:
                subticket_nr += 1
                subticket_sigla = u''
                if subticket.library_dk:
                    subticket_sigla = subticket.library_dk.upper()
                result += u'<h4><small>{nr}. {sigla}</small></h4>'.format(nr=subticket_nr, sigla=subticket_sigla)
                result_future += u'<h4><small>{nr}. {sigla}</small></h4>'.format(nr=subticket_nr, sigla=subticket_sigla)

            result += self.format_timeline_date(subticket, 'assigned_date', u'Přidělen')

            value_html = self.format_timeline_date(subticket, 'decide_date', u'Přijmout do', titype='future')
            if subticket_review_state in ('queued', ):
                result_future += value_html
            result += value_html

            result += self.format_timeline_date(subticket, 'cond_accepted_date', u'Podmínečně přijat')
            result_accepted = self.format_timeline_date(subticket, 'accepted_date', u'Přijat')
            if result_accepted:
                result += result_accepted
            result += self.format_timeline_date(subticket, 'refused_date', u'Odmítnut')

            # ŽK: Malé storno
            result += self.format_timeline_date(subticket, 'cancellation_small_ask_date', u'Požádáno o storno')

            result += self.format_timeline_date(subticket, 'sent_date', u'Odeslán')

            # ŽK: Reklamace
            result += self.format_timeline_date(subticket, 'complaint_date', u'Reklamace')

            if self.ticket.is_mvs:

                value_html = self.format_timeline_date(subticket, 'back_date', u'Požadováno zpět', titype='future')
                if subticket_review_state not in ('closed', ):
                    result_future += value_html
                result += value_html

                result += self.format_timeline_date(subticket, 'sent_back_date', u'Odeslán zpět')

            #result += self.format_timeline_date(subticket, 'closed_date', u'Uzavřeno')
            result += self.format_timeline_date(ticket, 'closed_date', u'Uzavřeno')

        # subticket DK/op timeline
        elif self.is_subticket and self.is_librarian_dk_op:

            # DK (or Operator in DK view) cares only about this subticket what
            # could differ from active subticket of this ticket
            subticket = self.subticket
            ticket = subticket.ticket
            wf_review_state = subticket.wf_review_state

            result += self.format_timeline_date(subticket, 'assigned_date', u'Přidělen')

            value_html = self.format_timeline_date(subticket, 'decide_date', u'Přijmout do', titype='future')
            if wf_review_state in ('queued', ):
                result_future += value_html
            result += value_html

            result += self.format_timeline_date(subticket, 'cond_accepted_date', u'Podmínečně přijat')
            result_accepted = self.format_timeline_date(subticket, 'accepted_date', u'Přijat')
            if result_accepted:
                result += result_accepted
            result += self.format_timeline_date(subticket, 'refused_date', u'Odmítnut')

            # DK: Malé storno
            result += self.format_timeline_date(subticket, 'cancellation_small_ask_date', u'Požádáno o storno')

            result += self.format_timeline_date(subticket, 'sent_date', u'Vyřízeno')

            result += self.format_timeline_date(ticket, 'reader_from_date', u'Připraveno pro čtenáře')

            # DK: Reklamace
            result += self.format_timeline_date(subticket, 'complaint_date', u'Reklamace')

            if self.ticket.is_mvs:

                value_html = self.format_timeline_date(subticket, 'back_date', u'Požadováno zpět', titype='future')
                if wf_review_state not in ('closed', ):
                    result_future += value_html
                result += value_html

                result += self.format_timeline_date(subticket, 'sent_back_date', u'Odeslán zpět')

            result += self.format_timeline_date(subticket, 'closed_date', u'Uzavřeno')

        self.timeline_html = result
        self.timeline_future_html = result_future

    def mark_librarian(self):
        """Označ ticket / subticket aktuálním knihovníkem.  Voláno ze všech míst
        měnících ticket / subticket.  Má smysl jen, pokud je přihlášen
        knihovník.  Označení se následně dá použít pro filtrování ve frontě
        požadavků.  Vrací True / False zda došlo ke změně.
        """

        if DEBUG:
            print("mark_librarian")

            print("self.user_id: " + str(self.user_id))
            print("self.ticket.librarian: " + str(self.ticket.librarian))

        if self.is_librarian_zk:
            if self.user_id and self.user_id != self.ticket.librarian:
                self.ticket.librarian = self.user_id
                if DEBUG:
                    print("mark_librarian ticket marked " + str(self.ticket.librarian))
                return True

        if self.is_librarian_dk:
            if self.user_id and self.user_id != self.subticket.librarian:
                self.subticket.librarian = self.user_id
                return True

        return False

    def setup_library_zk_dk(self):
        self.ticket_library_zk = None
        library_zk_sigla = self.ticket.library_zk
        if library_zk_sigla:
            library_zk_sigla = library_zk_sigla.lower()
            self.ticket_library_zk = getattr(self.portal.libraries, library_zk_sigla, None)

        self.librarian_zk_html = None
        if self.ticket.librarian:
            self.librarian_zk_html = u'<a href="{library_url}/{librarian}" target="_blank">{librarian}</a>'.format(
                library_url=self.ticket_library_zk.absolute_url(),
                librarian=self.ticket.librarian,
                )

        self.subticket_library_dk = None
        if self.subticket:
            library_dk_sigla = self.subticket.library_dk
            if library_dk_sigla:
                library_dk_sigla = library_dk_sigla.lower()
                self.subticket_library_dk = getattr(self.portal.libraries, library_dk_sigla, None)

    def setup_is_role(self):
        # toto bylo nastaveno v ZiskejBrowserView univerzálně, tzn. i bez
        # kontextu ticketu, nyní to nastavíme pro konkrétní ticket a subticket

        # reader
        if self.is_reader:
            if not self.ticket.is_reader:
                self.is_reader = False
            if self.ticket.reader != self.user_id:
                self.is_reader = False

        # v ticket/subticket nepoužívat, ale ptát se případně explicitně na
        #     if self.is_librarian_zk or self.is_librarian_dk:
        # případně 
        #     tal:condition="python: view.is_librarian_zk or view.is_librarian_dk"
        self.is_librarian = False

        self.is_librarian_zk = bool(
            self.ziskej_user_info and
            self.ziskej_user_info['role'] == 'Librarian' and
            self.library and
            self.ticket.library_zk and
            self.library.sigla and
            self.ticket.library_zk == self.library.sigla.lower()
            )
        self.is_librarian_zk_op = self.is_librarian_zk or self.is_ziskej_operator

        self.is_librarian_dk = bool(
            self.ziskej_user_info and
            self.ziskej_user_info['role'] == 'Librarian' and
            self.library and
            self.subticket and
            self.subticket.library_dk and
            self.library.sigla and
            self.subticket.library_dk == self.library.sigla.lower()
            )
        self.is_librarian_dk_op = self.is_librarian_dk or self.is_ziskej_operator

    def setup_pms(self):
        self.get_messages_r_zk = []
        self.get_messages_zk_dk = []

    def pm_zk_dk(self):
        # form PM zk/dk

        if not self.subticket:
            self.get_messages_zk_dk = []
            return False

        # většina má smysl jen pro ŽK / DK (ani pro operátora ne)
        if not self.is_librarian_zk and not self.is_librarian_dk:
            if self.is_ziskej_operator:
                # minimum pro operátora
                self.get_messages_zk_dk = self.subticket.get_messages()
            return False

        # form - submit
        message_text = None
        if self.request.get('submit_pm_zk_dk', None):
            message_text = self.text_from_request('add_message_zk_dk')
            if message_text:
                message_text = message_text.replace('\r', '')
                message_text = message_text.replace('\n', ' ')
        if message_text:
            dkpm_subticket_id = self.request.get('dkpm_subticket_id', None)
            if self.send_message_zk_dk(dkpm_subticket_id, message_text, info_message=True):
                # unred viz send_message_zk_dk
                if self.is_librarian_zk:
                    self.send_notifications('pm_zk_dk_librarian_zk')
                    #self.add_historylog(u'Byla poslána zpráva žádající knihovně: {}'.format(message_text))
                elif self.is_librarian_dk:
                    self.send_notifications('pm_zk_dk_librarian_dk')
                    #self.add_historylog(u'Byla poslána zpráva dožádané knihovně: {}'.format(message_text))

                self.redirect(self.context_url)
                return True

        # form - příprava
        self.get_messages_zk_dk = self.subticket.get_messages()
        return False

    def send_message_zk_dk(self, subticket_id, text, info_message=True):
        subticket = self.subticket
        if self.is_librarian_zk:
            message_sender = u'library_zk'
            if subticket_id and self.subticket.getId() != subticket_id:
                subticket = getattr(self.ticket, subticket_id, None)
                if not subticket:
                    raise Exception('Invalid subticket (message)')
        elif self.is_librarian_dk:
            message_sender = u'library_dk'
        is_updated = subticket.add_message_txt(message_sender, text.strip())
        if is_updated:
            if info_message:
                message = u'Zpráva byla odeslána.'
                self.add_message(message)
            # případné přesměrování je o úroveň výš (ne vždy dochází k přesměrování)

            # unread
            if self.is_librarian_zk:
                subticket.unread_dk = True
            elif self.is_librarian_dk:
                self.ticket.unread_zk = True

            self.mark_librarian()

            self.ticket.reindexObject()
            subticket.reindexObject()

            return True

        else:
            if info_message:
                message = u'Nebyla zadána žádná zpráva.'
                self.add_message(message)
        return False

    def wf_subticket(self):
        """Main restrictions are handled by CMF Workflow setup.  More detailed
        ones are in this method.
        """

        if not self.subticket:
            return False

        if not self.is_librarian_zk_op and not self.is_librarian_dk:
            return False

        # storno zastavuje běžné workflow - ano, vždy
        if self.subticket.is_small_cancel_asked:
            return False

        # nerozhodnutá reklamace zastavuje běžné workflow - ano, vždy
        if self.subticket.is_small_complaint_undecided:
            return False

        # storno objednávky zastavuje běžné workflow požadavku - asi ne
        #if self.ticket.is_big_cancel_asked:
        #    return False

        # nerozhodnutá reklamace objednávky zastavuje běžné workflow požadavku - opravdu?
        if self.ticket.is_big_complaint_undecided:
            return False

        # požadavek na přechod
        self.wf_subticket_submit = self.request.get('wf_subticket_submit', None)
        comment = self.text_from_request('comment')
        if comment:
            comment = comment.strip()
        else:
            comment = u''

        # požadavek na změnu stavu + comment = přidej i zprávu druhé knihovně,
        # na rozdíl od zprávy ale tady žádná status message a žádné přesměrování
        if self.wf_subticket_submit and comment:
            self.send_message_zk_dk(self.subticket.getId(), comment, info_message=False)

        # uvnitř jednotlivých _wf_(state)_(role)_(transition) vracíme True,
        # pokud došlo k akci vyžadující znovunačtení (redirect)
        wfret = False

        # Byl vytvořen požadavek, nyní DK odmítne nebo vyplní cenu a přijme
        if self.subticket_review_state == 'queued':
            if self.is_librarian_dk:
                # queued accept
                if self.subticket.can_accept:
                    wfret = wfret or self._wf_queued_dk_accept()
                # queued conditionally_accept
                if self.subticket.can_conditionally_accept:
                    wfret = wfret or self._wf_queued_dk_conditionally_accept()  # ca reasons
                # queued refuse
                if self.subticket.can_refuse:
                    wfret = wfret or self._wf_queued_dk_refuse()  # r reasons

        # Byl podmíněně přijat požadavek, nyní ŽK akceptuje podmínky DK a tím
        # je tedy požadavek přijatý nebo je odmítne a tím je odmítnutý
        elif self.subticket_review_state == 'conditionally_accepted':
            if self.is_librarian_zk:
                # conditionally_accepted accept
                if self.subticket.can_accept:
                    wfret = wfret or self._wf_conditionally_accepted_zk_accept()
                # conditionally_accepted refuse by ZK
                if self.subticket.can_refuse:
                    wfret = wfret or self._wf_conditionally_accepted_zk_refuse()
            if self.is_librarian_dk:
                # conditionally_accepted refuse by DK
                if self.subticket.can_refuse:
                    wfret = wfret or self._wf_conditionally_accepted_dk_refuse()  # r reasons, similar

        # Byl přijat požadavek, nyní DK odešle, ve výjimečných případech
        # odmítne (myši sežraly)
        elif self.subticket_review_state == 'accepted':
            if self.is_librarian_dk:
                # accepted send
                if self.subticket.can_send:
                    wfret = wfret or self._wf_accepted_dk_send()
                else:
                    wfret = wfret or self._wf_accepted_dk_send_cannot()
                # accepted refuse
                if self.subticket.can_refuse:
                    wfret = wfret or self._wf_accepted_dk_refuse()  # r reasons, similar

        elif self.subticket_review_state == 'sent':
            if self.is_librarian_zk_op:
                # sent send_back
                if self.subticket.can_send_back:
                    wfret = wfret or self._wf_sent_zk_send_back()

        elif self.subticket_review_state == 'sent_back':
            if self.is_librarian_dk:
                # sent_back close
                if self.subticket.can_close:
                    wfret = wfret or self._wf_sent_back_dk_close()
            elif self.is_ziskej_operator:
                if self.subticket.can_close:
                    wfret = wfret or self._wf_sent_back_dk_close(is_operator=True)

        if wfret:
            return True

        # url se použije jen v tal:condition, ale musí existovat, protože výše
        # url nastavujeme jen na '', tak když neexistuje, musíme přidělit
        # jakýkoli True, třeba id
        for transition in self.subticket_transitions:
            if 'url' not in transition:
                transition['url'] = transition['id']

        if DEBUG:
            print(self.subticket_transitions)

    def call_finalize(self):
        if not DEBUG:
            return

        print(' ')
        print( '----------------------------------------------------------------')
        print(' ')
        if self.ticket:
            print('ticket {hid}'.format(hid=self.ticket.hid))
            #ticket_fields = ('reader', 'library_zk', 'library_dk', 'library_dks', 'library_zk_title', 'library_dk_title', 'subticket_id', 'subticket_ids', 'doc_id', 'doc_title', 'doc_fullname', 'doc_order', 'doc_order_sent', 'doc_order_suggested', 'doc_order_blacklist', 'librarian', )
            ticket_fields = ('library_dk', 'library_dks', 'subticket_id', 'subticket_ids', 'doc_id', 'doc_order', 'doc_order_sent', 'doc_order_suggested', 'doc_order_blacklist', )
            for k in ticket_fields:
                print('-- ' + k)
                value = getattr(self.ticket, k, None)
                pprint(value)
            print(' ')

        if self.subticket:
            print('subticket {hid}'.format(hid=self.subticket.hid))
            #subticket_fields = ('ticket_id', 'fullhid_sigla', 'fullhid_nr', 'library_zk', 'library_dk', 'library_zk_title', 'library_dk_title', 'doc_id', 'doc_title', 'doc_fullname', 'librarian', )
            subticket_fields = ('ticket_id', 'fullhid_sigla', 'fullhid_nr', 'library_dk', 'doc_id', )
            for k in subticket_fields:
                print('-- ' + k)
                value = getattr(self.subticket, k, None)
                pprint(value)
            print(' ')

        print('----------------------------------------------------------------')
        print(' ')

    def try_next_request(self, assign_or_stop):
        """Voláno pro ukončení požadavku v různých situacích.

        Zabezpečuje posuny v rámci doc_order, ukládání do doc_order_blacklist a
        deaktivaci subticketu (tzn. že ho nadřazený ticket nepovažuje za
        aktivní).

        Pomocí parametru assign_or_stop provede příslušné kroky - vytvoří a
        přiřadí další subticket, pokud je to možné - pro 'assign'.  Nebo
        zastaví pro 'stop'. Tento parametr zohledňuje důvod odmítnutí
        a zohlednit, kdo je právě teď - pro ZK chceme, aby se zastavilo a mohl
        něco udělat, automaticky zkoušíme jen pro DK.
        """

        if assign_or_stop not in ('assign', 'stop', 'cancel', 'cancel_assign'):
            raise Exception('Internal error')

        ticket = self.ticket
        subticket = self.subticket

        if subticket is None:
            logger.error('ticket {hid} try_next_request missing subticket'.format(hid=ticket.hid))
            raise Exception('Internal error')

        # To by molo nastat při práci s malou reklamací po odpojení ticketu,
        # ale v těchto situacích by vůbec nemělo dojít k volání
        # try_next_request
        if self.subticket.getId() != self.ticket.subticket_id:
            raise Exception('Internal error')

        # Manual LB - nelze auto
        if ticket.is_lb_manual:
            ticket.set_lb_state(u'manual library closed')
            ticket.deactivate_subticket()
            if assign_or_stop in ('assign', 'stop', 'cancel_assign', ):
                wf_action(ticket, 'stop', u'Je potřeba nastavit pořadí pro další požadavky.')
            ticket._p_changed = True
            return False

        # No DK - nelze auto
        if ticket.is_lb_nodk:
            if assign_or_stop in ('assign', 'stop', 'cancel_assign', ):
                wf_action(ticket, 'stop', u'Je potřeba nastavit pořadí pro další požadavky.')
            ticket._p_changed = True
            return False

        # Auto LB

        if not ticket.is_lb_auto:
            logger.error('ticket {hid} try_next_request invalid lb'.format(hid=ticket.hid))
            raise Exception('Internal error')

        if not subticket.doc_id:
            logger.error('ticket {hid} try_next_request subticket missing doc_id'.format(hid=ticket.hid))
            raise Exception('Internal error')

        # doc_order
        doc_order_list = []
        doc_order_sent_list = []
        if ticket.doc_order:
            doc_order_list = ticket.doc_order.split(',')
        if ticket.doc_order_sent:
            doc_order_sent_list = ticket.doc_order_sent.split(',')
        remove_first_item = True
        if subticket.doc_id not in doc_order_list:
            logger.warning('ticket {hid} try_next_request doc_id not in doc_order'.format(hid=ticket.hid))
            remove_first_item = False
            #raise Exception('Internal error')
        if not len(doc_order_list):
            logger.warning('ticket {hid} try_next_request doc_order empty'.format(hid=ticket.hid))
            remove_first_item = False
            #raise Exception('Internal error')
        if remove_first_item and subticket.doc_id != doc_order_list[0]:
            logger.warning('ticket {hid} try_next_request subticket is not active doc_id in doc_order'.format(hid=ticket.hid))
            remove_first_item = False
            #raise Exception('Internal error')

        if remove_first_item:
            # doc_order odstranit první prvek - posledně použité doc_id, tím se
            # další pořadí aktualizuje dostatečně
            doc_order_list = doc_order_list[1:]
            doc_order_sent_list = doc_order_sent_list[1:]
            ticket.doc_order = ','.join(doc_order_list)
            ticket.doc_order_sent = ','.join(doc_order_sent_list)

        # aktualizace blacklistu pro Auto LB a to i po případném dalším načtení
        # dostupnosti
        doc_order_blacklist_list = []
        if ticket.doc_order_blacklist:
            doc_order_blacklist_list = ticket.doc_order_blacklist.split(',')
        if not doc_order_blacklist_list:
            doc_order_blacklist_list = []
        if subticket.doc_id not in doc_order_blacklist_list:
            doc_order_blacklist_list.append(subticket.doc_id)
            ticket.doc_order_blacklist = ','.join(doc_order_blacklist_list)

        ticket.set_lb_state(u'auto lb closed')
        ticket.deactivate_subticket()

        if assign_or_stop == 'cancel':
            # cancel is done in complaint_cancel
            ticket._p_changed = True
            return False

        # nyní pro asign zkusíme vytvořit nový subticket, pokud nelze, tak
        # fallback na stop
        if assign_or_stop in ('assign', 'cancel_assign', ):
            if not len(doc_order_list) or not ticket.can_assign:
                assign_or_stop = 'stop'

        # assign or stop
        if assign_or_stop in ('assign', 'cancel_assign', ):
            wf_action(ticket, 'assign', u'Automatický přechod.')
            new_subticket = self.create_subticket()
            # Mohlo mezitím dojít k zneplatnění položky v dk, např.
            # k deaktivaci knihovny, i zde fallback na stop.
            if not new_subticket:
                assign_or_stop = 'stop'
        if assign_or_stop in ('assign', 'cancel_assign', ):
            pass
        elif assign_or_stop == 'stop':
            wf_action(ticket, 'stop', u'Je potřeba nastavit pořadí pro další požadavky.')
            ticket.subticket_id = u''
        else:
            raise Exception('Internal error')

        ticket._p_changed = True
        return bool(assign_or_stop in ('assign', 'cancel_assign', ))

    def create_subticket(self):
        """Vytvoří nový subticket, vrátí jej, případně None, pokud vytvořit
        nový subticket nelze.

        Pro AutoLB se používá doc_order.  Postupně se pokusí vytvořit subticket
        na základě první položky v doc_order, pokud nelze, zneplatní ji (smaže)
        a pokračuje případně dál až do vyčerpání všech položek v doc_order,
        v takovém případě vrátí None.

        Pro ManualLB také může nastat situace, že zvolená DK už není aktivní,
        v tom případě obdobně vrátí None.
        """

        ticket = self.ticket
        if not ticket.doc_order and not ticket.lb_manual_library:
            raise Exception('Missing doc_order and lb_manual_library.')

        # Manual LB
        if ticket.is_lb_manual:
            sigla_dk = ticket.lb_manual_library
            if not sigla_dk:
                raise Exception('Internal error')
            library_dk = getattr(self.portal.libraries, sigla_dk, None)
            if not library_dk or not library_dk.is_active:
                # Zvolená DK už není aktivní, v tom případě vrátit None.
                ticket.unread_r = True
                ticket._p_changed = True
                ticket.reindexObject()
                return None
            library_dk_title = library_dk.Title()
            dk_info = dict(
                id = u'',
                title = ticket.doc_title,
                fullname = ticket.doc_fullname,
                #doc_data = u''
                )
            doc_sent = ticket.lb_manual_library_sent
            if doc_sent not in (u'sent', u'pick', ):
                doc_sent = u'sent'
            ticket.set_lb_state(u'manual library created')

        # Auto LB based on doc_order
        else:

            # Najít první validní záznam v doc_order.
            dk_list = None
            while ticket.doc_order:
                doc_order = ticket.doc_order.split(',')
                if not doc_order:
                    break

                doc_order_sent_list = []
                if ticket.doc_order_sent:
                    doc_order_sent_list = ticket.doc_order_sent.split(',')

                # doc_order je nyní vždy aktuální a případné doc_id, které se
                # nemá použít, je z něj odstraněno při zpracování subticketu,
                # kdy je jasné, že nebude úspěšný (odmítnuto etc.)
                doc_id = doc_order[0]
                doc_sent = u'sent'
                if doc_order_sent_list:
                    doc_sent = doc_order_sent_list[0]

                # Používá Auto LB pouze pro parsování předchozích výsledků
                # uložených v ticket.  Žádné nové volání CPK.
                dk_list = load_balancing_dk_list(self.portal,
                                                 ticket,
                                                 restrict_doc_ids=[doc_id, ])

                # Nalezen validní záznam, zpracovat a případně ukončit hledání
                if dk_list:
                    dk_info = dk_list[0]

                    sigla_dk = dk_info['sigla'].lower()
                    library_dk = getattr(self.portal.libraries, sigla_dk, None)
                    # Vše OK, ukončit hledání
                    if library_dk and library_dk.is_active:
                        break
                    # Jinak hledat dál...
                    dk_list = None

                # Nenalezen validní záznam,  astane např. pokud je příslušná DK
                # mezitím deaktivována, tuto položku proto odstranit
                # z doc_order, ale nedávat ji do blacklistu.
                if len(doc_order) > 1:
                    ticket.doc_order = u','.join(doc_order[1:])
                else:
                    ticket.doc_order = u''
                if ticket.doc_order_sent:
                    if len(doc_order_sent_list) > 1:
                        ticket.doc_order_sent = u','.join(
                            doc_order_sent_list[1:])
                    else:
                        ticket.doc_order_sent = u''

            # AutoLB zastaven, vrátit None.
            if not dk_list:
                ticket.unread_r = True
                ticket._p_changed = True
                ticket.reindexObject()
                return None

            # Pokračovat ve zpracování validního záznamu
            library_dk_title = library_dk.Title()
            ticket.set_lb_state(u'auto lb created')

            if DEBUG:
                print(' ')
                print('Auto LB based on doc_order')
                print('doc_order: ', doc_order)
                print('doc_id: ', doc_id)
                print('load_balancing_dk_list(..., restrict_doc_ids=[doc_id, ])')
                print('dk_list')
                pprint(dk_list)
                print('sigla_dk: ', sigla_dk)
                #raise Exception('Debug')

        # pokud nejsou subtickety, tak 0, což znamená, že se ignoruje, jinak
        # 2 a víc, tzn. 1 nikdy není
        fullhid_nr = -1
        if self.subticket and self.subticket.fullhid_nr > fullhid_nr:
            fullhid_nr = self.subticket.fullhid_nr
        # for je zbytečný, protože self.subticket byl vytvořen tak že má max
        # hid, ale pro případné budoucí změny nechceme tento tichý předpoklad
        # o datech
        for x in self.subtickets:
            # x.fullhid_nr nemůže být None nebo '', vždy je číslo
            # x.fullhid_nr ale může být 0 takže by nefungovalo if x.fullhid_nr
            if x.fullhid_nr > fullhid_nr:
                fullhid_nr = x.fullhid_nr
        fullhid_nr += 1
        if fullhid_nr == 1:
            fullhid_nr = 2

        now_iso = self.dt_as(DateTime(), 'iso')

        # FIXME-EDD zatím je to čistě MVS
        obj_dict = dict(
            ticket_id = ticket.getId(),
            hid = ticket.hid,
            fullhid_sigla = sigla_dk,
            fullhid_nr = fullhid_nr,
            library_zk = ticket.library_zk,
            library_dk = sigla_dk,
            library_zk_title = ticket.library_zk_title,
            library_dk_title = library_dk_title,
            doc_id = dk_info['id'],
            doc_title = dk_info['title'],
            doc_fullname = dk_info['fullname'],
            doc_sent = doc_sent,
            #doc_data = dk_info['doc_data'],
            assigned_date = now_iso,
            cena = 70,
            )

        if ticket.is_edd:
            edd_data = ticket.Edd_data()
            if edd_data['pages_number']:
                estimate_fee = edd_data['pages_number']
            else:
                estimate_fee = None
            obj_dict['cena'] = estimate_fee
            # Pro EDD nepoužívat informace z dotazované jednotky (dk_info), ale
            # z nastavení objednávky
            obj_dict['doc_title'] = ticket.doc_title
            obj_dict['doc_fullname'] = ticket.doc_fullname

        if DEBUG:
            pprint(obj_dict)
            print("-- create subticket")
        subticket = create_subticketmvs(ticket, obj_dict, catalog=self.catalog)
        if DEBUG:
            print('new subticket')
            print(subticket)
        self.subtickets.insert(0, self.subticket)
        self.subticket = subticket
        self.subticket_last = self.subticket

        # v případě, že se nejedná o první subticket, je potřeba zneplatnit data
        # založena na předchozím subticketu
        ticket.closed_date = None

        # ticket - vše plynoucí z nově přidaného podpožadavku
        ticket.library_dk = sigla_dk
        if not ticket.library_dks:
            ticket.library_dks = []
        ticket.library_dks.insert(0, sigla_dk)
        ticket.library_dk_title = library_dk_title
        ticket.subticket_id = subticket.getId()
        if not ticket.subticket_ids:
            ticket.subticket_ids = []
        ticket.subticket_ids.insert(0, subticket.getId())

        subticket.set_next_action(
            na_type='1wd',
            na_text=u'Schválení nebo odmítnutí požadavku do 1 pracovního dne'
            )
        subticket.decide_date = self.dt_as(subticket.next_action_date, 'iso')

        # unread
        ticket.unread_r = True
        ticket._p_changed = True
        ticket.reindexObject()
        subticket.unread_dk = True
        subticket._p_changed = True
        subticket.reindexObject()

        return subticket
