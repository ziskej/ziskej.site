# -*- coding: utf-8 -*-
"""BrowserView: Ticket / Subticket.

Sdílený kód pro Ticket a Subticket, storno a reklamace.
"""

from __future__ import print_function

import logging

from DateTime import DateTime

from ziskej.site.browser.ticket.ticket_core_workflow import (
    TicketCoreWorkflowView,
    BTNCSS_DEFAULT,
    BTNCSS_PRIMARY,
    )
from ziskej.site.library_payment_helpers import (
    library_payment_mvs_complaint,
    library_payment_mvs_cancel,
    )
from ziskej.site.ticket_helpers import (
    create_reportentry,
    wf_action,
    )
from ziskej.site.utils import (get_instance_type_label, datetime_add_d, )
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger("ziskej")

# Reklamace v. 2, jen malá, jen ŽK, jen obecná
COMPLAINT_2 = True

DEBUG_complaint = False
DEBUG_cancel = False


class TicketCoreCancelComplaintView(TicketCoreWorkflowView):
    """Sdílený kód pro Ticket a Subticket, storno a reklamace.

    big_cancel / velké storno je storno ticketu (objednávky)
    small_cancel / malé storno je storno subticketu (požadavku)
    """

    def complaint_cancel(self):
        if self._complaint_cancel_cancel():
            return True
        if self._complaint_cancel_complaint():
            return True
        return False

    def _complaint_cancel_complaint(self):
        # defaults
        self.show_small_complaint_general = False
        self.show_small_complaint_late = False
        self.show_small_complaint_wrong = False
        self.show_small_complaint_decide = False
        self.show_small_complaint_objection = False
        self.show_small_complaint_objection_decide = False
        self.small_complaint_soft = False
        self.small_complaint_info = u''
        self.show_small_complaint_zk = False

        self.show_big_complaint_wrong = False
        self.show_big_complaint_decide = False
        self.show_big_complaint_objection = False
        self.show_big_complaint_objection_decide = False
        self.big_complaint_info = u''

        if self.is_reader:
            who = u'reader'
        elif self.is_librarian_zk:
            who = u'zk'
        elif self.is_librarian_dk:
            who = u'dk'
        elif self.is_ziskej_operator:
            who = u'operator'
        else:
            return False

        ticket = self.ticket
        # Jsou situace, kdy ticket už nemá aktivní subticket, přesto je potřeba
        # řešit poslední subticket, např. podat námitku, proto
        # self.subticket_last nejen self.subticket, protože, pokud
        # self.subticket existuje, je to totéž, ale v případech, kdy už 
        # neexistuje, např. protože nastala velká reklamace (ta odpojí
        # subticket), ale příslušná malá reklamace ještě nedoběhla.
        subticket = self.subticket_last
        twf = ticket.wf_review_state
        if subticket:
            stwf = subticket.wf_review_state
        else:
            stwf = None

        if DEBUG_complaint:
            print(' ')
            print('_complaint_cancel_complaint')
            print('who: {}'.format(who))
            print('ticket: {}'.format(ticket))
            print('subticket: {}'.format(subticket))
            print('twf / stwf: {} / {}'.format(twf, stwf))
            print('ticket.complaint_state: {}'.format(ticket.complaint_state))
            print('ticket.complaint_date: {}'.format(ticket.complaint_date))
            if subticket:
                print('subticket.complaint_state: {}'.format(subticket.complaint_state))
                print('subticket.complaint_date: {}'.format(subticket.complaint_date))
            print(' ')


        if not COMPLAINT_2:

            # nastavit na co má uživatel v tomto kontextu právo
            if self.is_ticket and self.is_reader:
                # big
                if ticket.is_complaint and \
                        ticket.complaint_state == u'declined':
                    self.show_big_complaint_objection = True
                # new big by reader
                # FIXME zvážit přidat limit jak dlouho od zapůjčení je možné
                # reklamovat
                if twf == 'prepared' and self.ticket.is_reader_lent and \
                        not self.ticket.is_reader_return:
                    if not ticket.is_complaint:
                        self.show_big_complaint_wrong = True
            elif self.is_ticket and self.is_librarian_zk:
                # small by ŽK
                if subticket and subticket.is_complaint and \
                        subticket.complaint_state == u'declined':
                    self.show_small_complaint_objection = True
                # new small by ŽK
                if twf == 'assigned' and stwf == 'sent':
                    if subticket and not subticket.is_complaint and \
                            not subticket.is_small_cancel_asked:
                        if self._cc_cond_late(subticket):
                            self.show_small_complaint_late = True
                        self.show_small_complaint_wrong = True
                # big
                if ticket.is_complaint and \
                        ticket.complaint_state == u'new':
                    self.show_big_complaint_decide = True
                # new big by ŽK
                if twf == 'prepared' and stwf == 'sent':
                    if not ticket.is_complaint:
                        self.show_big_complaint_wrong = True
            elif self.is_subticket and self.is_librarian_dk:
                # small by ŽK
                if subticket.is_complaint and \
                        subticket.complaint_state == u'new':
                    self.show_small_complaint_decide = True
            elif self.is_ziskej_operator:
                if subticket and subticket.is_complaint and \
                        subticket.complaint_state == u'objected':
                    self.show_small_complaint_objection_decide = True
                if ticket.is_complaint and \
                        ticket.complaint_state == u'objected':
                    self.show_big_complaint_objection_decide = True

        # COMPLAINT_2
        else:
            if self.is_ticket and self.is_librarian_zk:
                # small by ŽK
                if subticket and subticket.is_complaint and \
                        subticket.complaint_state == u'declined':
                    self.show_small_complaint_objection = True
                # new small by ŽK
                if twf in ('assigned', 'prepared', ) and stwf == 'sent':
                    if subticket and not subticket.is_complaint and \
                            not subticket.is_small_cancel_asked:
                        # if self._cc_cond_late(subticket):
                        #     # self.show_small_complaint_late = True
                        #     self.show_small_complaint_general = True

                        # self.show_small_complaint_wrong = True
                        if subticket.is_mvs:
                            self.show_small_complaint_general = True
                        elif subticket.is_edd:
                            self.show_small_complaint_general = \
                                subticket.is_pdf_file_complaintable()
                # # big
                # if ticket.is_complaint and \
                #         ticket.complaint_state == u'new':
                #     self.show_big_complaint_decide = True
                # # new big by ŽK
                # if twf == 'prepared' and stwf == 'sent':
                #     if not ticket.is_complaint:
                #         self.show_big_complaint_wrong = True
            elif self.is_subticket and self.is_librarian_dk:
                # small by ŽK
                if subticket.is_complaint and \
                        subticket.complaint_state == u'new':
                    self.show_small_complaint_decide = True
                    self.small_complaint_soft = subticket.complaint_is_soft
            elif self.is_ziskej_operator:
                if subticket and subticket.is_complaint and \
                        subticket.complaint_state == u'objected':
                    self.show_small_complaint_objection_decide = True
                # if ticket.is_complaint and \
                #         ticket.complaint_state == u'objected':
                #     self.show_big_complaint_objection_decide = True

        # texts and redo
        if subticket:
            self.small_complaint_info = subticket.complaint_label
            self.show_small_complaint_zk = True
        self.big_complaint_info = ticket.complaint_label

        if DEBUG_complaint:
            for k in ('show_small_complaint_general',
                      'show_small_complaint_late',
                      'show_small_complaint_wrong',
                      'show_small_complaint_decide',
                      'show_small_complaint_objection',
                      'show_small_complaint_objection_decide',
                      'show_big_complaint_wrong',
                      'show_big_complaint_decide',
                      'show_big_complaint_objection',
                      'show_big_complaint_objection_decide',
                      ):
                value = getattr(self, k, None)
                print('{}: {}'.format(k, value))
            print(' ')

        # není povolen žádný submit, předčasný konec metody
        if not self.show_small_complaint_general and \
                not self.show_small_complaint_late and \
                not self.show_small_complaint_wrong and \
                not self.show_small_complaint_decide and \
                not self.show_small_complaint_objection and \
                not self.show_small_complaint_objection_decide and \
                not self.show_big_complaint_wrong and \
                not self.show_big_complaint_decide and \
                not self.show_big_complaint_objection and \
                not self.show_big_complaint_objection_decide:
            if DEBUG_complaint:
                print('NO SUBMIT ALLOWED in complaint')
                print(' ')
            return False

        # submit values

        objection_text = u''
        objection_decision_text = u''
        complaint_is_soft = False

        if self.show_small_complaint_general or \
                self.show_small_complaint_late or \
                self.show_small_complaint_wrong:
            small_complaint_lov = []
            if self.show_small_complaint_general:
                small_complaint_lov.append(u'complaint_general')
            if self.show_small_complaint_late:
                small_complaint_lov.append(u'complaint_late')
            if self.show_small_complaint_wrong:
                small_complaint_lov.append(u'complaint_wrong')
            small_complaint = self.text_from_request(
                'small_complaint',
                lov=small_complaint_lov)
            if small_complaint == 'complaint_general':
                small_complaint_text = self.text_from_request('small_complaint_text')
                complaint_is_soft = bool(
                    self.text_from_request('is_soft') == 'complaint-is-soft')
                if DEBUG_complaint:
                    print("small_complaint_text, complaint_is_soft", small_complaint_text, complaint_is_soft)

        else:
            small_complaint = u''

        if self.show_small_complaint_decide:
            small_complaint_decide_lov = (u'accept', u'decline', )
            small_complaint_decide = self.text_from_request(
                'small_complaint_decide',
                lov=small_complaint_decide_lov)
        else:
            small_complaint_decide = u''

        if self.show_small_complaint_objection:
            small_complaint_objection_lov = (u'complaint_objection', )
            small_complaint_objection = self.text_from_request(
                'small_complaint_objection',
                lov=small_complaint_objection_lov)
            objection_text = self.text_from_request('objection_text')
            if not objection_text and small_complaint_objection:
                small_complaint_objection = u''
                message = u'Je nutné zadat odůvodnění námitky.'
                self.add_message(message)
        else:
            small_complaint_objection = u''

        if self.show_small_complaint_objection_decide:
            small_complaint_objection_decide_lov = (u'accept', u'decline', )
            small_complaint_objection_decide = self.text_from_request(
                'small_complaint_objection_decide',
                lov=small_complaint_objection_decide_lov)
            objection_decision_text = self.text_from_request('objection_decision_text')
            if not objection_decision_text and small_complaint_objection_decide:
                small_complaint_objection_decide = u''
                message = u'Je nutné zadat odůvodnění rozhodnutí o námitce.'
                self.add_message(message)
        else:
            small_complaint_objection_decide = u''

        if self.show_big_complaint_wrong:
            big_complaint_lov = (u'complaint_wrong', )
            big_complaint = self.text_from_request(
                'big_complaint',
                lov=big_complaint_lov)
        else:
            big_complaint = u''

        if self.show_big_complaint_decide:
            big_complaint_decide_lov = (u'accept', u'decline', )
            big_complaint_decide = self.text_from_request(
                'big_complaint_decide',
                lov=big_complaint_decide_lov)
        else:
            big_complaint_decide = u''

        if self.show_big_complaint_objection:
            big_complaint_objection_lov = (u'complaint_objection', )
            big_complaint_objection = self.text_from_request(
                'big_complaint_objection',
                lov=big_complaint_objection_lov)
            objection_text = self.text_from_request('objection_text')
            if not objection_text and big_complaint_objection:
                big_complaint_objection = u''
                message = u'Je nutné zadat odůvodnění námitky.'
                self.add_message(message)
        else:
            big_complaint_objection = u''

        if self.show_big_complaint_objection_decide:
            big_complaint_objection_decide_lov = (u'accept', u'decline', )
            big_complaint_objection_decide = self.text_from_request(
                'big_complaint_objection_decide',
                lov=big_complaint_objection_decide_lov)
            objection_decision_text = self.text_from_request('objection_decision_text')
            if not objection_decision_text and big_complaint_objection_decide:
                big_complaint_objection_decide = u''
                message = u'Je nutné zadat odůvodnění rozhodnutí o námitce.'
                self.add_message(message)
        else:
            big_complaint_objection_decide = u''

        # do action
        if self.show_small_complaint_general and small_complaint == u'complaint_general':
            if DEBUG_complaint:
                print('DO: _complaint_create complaint_general')
                print(' ')
            self._complaint_create(
                who, small_complaint_text, is_soft=complaint_is_soft)
            self.redirect()
            return True

        if self.show_small_complaint_late and small_complaint == u'complaint_late':
            if DEBUG_complaint:
                print('DO: _complaint_create complaint_late')
                print(' ')
            self._complaint_create(who, small_complaint)
            self.redirect()
            return True

        if self.show_small_complaint_wrong and small_complaint == u'complaint_wrong':
            if DEBUG_complaint:
                print('DO: _complaint_create complaint_wrong')
                print(' ')
            self._complaint_create(who, small_complaint)
            self.redirect()
            return True

        if self.show_small_complaint_decide and small_complaint_decide:
            if DEBUG_complaint:
                print('DO: _complaint_decide')
                print(' ')
            if self._complaint_decide(who, small_complaint_decide):
                self.redirect()
                return True

        if self.show_small_complaint_objection and small_complaint_objection:
            if DEBUG_complaint:
                print('DO: _complaint_objection')
                print(' ')
            self._complaint_objection(who, objection_text)
            self.redirect()
            return True

        if self.show_small_complaint_objection_decide and small_complaint_objection_decide:
            if DEBUG_complaint:
                print('DO: _complaint_objection_decide')
                print(' ')
            if self._complaint_objection_decide(who, 
                    small_complaint_objection_decide, objection_decision_text):
                self.redirect()
                return True

        if self.show_big_complaint_wrong and big_complaint == u'complaint_wrong':
            if DEBUG_complaint:
                print('DO: _complaint_big_create')
                print(' ')
            self._complaint_big_create(who, big_complaint)
            self.redirect()
            return True

        if self.show_big_complaint_decide and big_complaint_decide:
            if DEBUG_complaint:
                print('DO: _complaint_big_decide')
                print(' ')
            if self._complaint_big_decide(who, big_complaint_decide):
                self.redirect()
                return True

        if self.show_big_complaint_objection and big_complaint_objection:
            if DEBUG_complaint:
                print('DO: _complaint_big_objection')
                print(' ')
            self._complaint_big_objection(who, objection_text)
            self.redirect()
            return True

        if self.show_big_complaint_objection_decide and big_complaint_objection_decide:
            if DEBUG_complaint:
                print('DO: _complaint_big_objection_decide')
                print(' ')
            if self._complaint_big_objection_decide(who, 
                    big_complaint_objection_decide, objection_decision_text):
                self.redirect()
                return True

        return False

    def _cc_cond_late(self, subticket):
        complaint_st_late_days = ziskej_parameter('complaint_st_late_days')  # default 6
        if self.is_instance_testing:
            complaint_st_late_days = 1
        dt = datetime_add_d(
            complaint_st_late_days,
            start_date = DateTime(subticket.sent_date)  # sent_date .. text iso
            )
        return bool(DateTime() > dt)

    def _complaint_big_create(self, who, text):
        self.ticket.complaint_date = self.dt_as(DateTime(), 'isodt')
        self.ticket.complaint_text = text
        self.ticket.complaint_who = who
        self.ticket.complaint_state = u'new'
        self.ticket.reindexObject()
        self._ticket_complaint_notify(who)

    def _complaint_big_restore(self):
        self.ticket.complaint_date = None
        self.ticket.complaint_text = u''
        self.ticket.complaint_who = u''
        self.ticket.complaint_state = u''
        self.ticket.reindexObject()

    def _complaint_big_objection(self, who, text):
        self.ticket.complaint_state = u'objected'
        self.ticket.objection_text = text
        self.ticket.reindexObject()
        self._ticket_complaint_notify(who)

    def _complaint_big_decide(self, who, decision):
        if decision == u'accept':
            self.ticket.complaint_state = u'accepted'
            self._ticket_complaint_accepted(who)
            self._ticket_complaint_notify(who)
            return True
        elif decision == u'decline':
            self.ticket.complaint_state = u'declined'
            self.ticket.reindexObject()
            self._ticket_complaint_notify(who)
            return True
        return False

    def _complaint_big_objection_decide(self, who, decision, text):
        if decision == u'accept':
            self.ticket.complaint_state = u'objection_accepted'
            self.ticket.objection_decision_text = text
            # who je operator, celá malá reklamace ale vyžaduje, aby její who bylo 'zk'
            self._ticket_complaint_accepted(u'zk', after_objection=True)
            self._ticket_complaint_notify(who)
            return True
        elif decision == u'decline':
            self.ticket.complaint_state = u'objection_declined'
            self.ticket.objection_decision_text = text
            self.ticket.reindexObject()
            self._ticket_complaint_notify(who)
            return True
        return False

    def _ticket_complaint_accepted(self, who, after_objection=False):
        # nechceme storno požadavku, jen zahájit reklamaci - to je vlastně jen
        # zkratka, aby nemusel klikat dvakrát, ale nemusíme ani řešit kdy
        # dovolit a tak (v prepared totiž už nedovolujeme malou reklamaci)
        self._complaint_create(who, u'complaint_wrong')

        # nechceme aby požadavek byl dál aktivní, ale to by mělo být už v rámci
        # _complaint_create
        # if self.subticket and (self.subticket.getId() == self.ticket.subticket_id):
        #     self.try_next_request('cancel')

        # a je na čase vrátit objednávku do pending, ale i to by mělo být
        # v rámci _complaint_create
        # if not after_objection:
        #     comment = 'big coplaint was accepted'
        # else:
        #     comment = 'big coplaint was accepted - after objection and by operator'
        # wf_action(self.ticket, 'stop', comment)

        # subticket i ticket reindex jsou v _complaint_create
        # self.ticket.reindexObject()

    def _ticket_complaint_notify(self, who):
        # setup: notification + mark + unread + message
        message = u'Stav reklamace objednávky byl změněn.'
        if who == 'zk':
            self.ticket.unread_r = True
            self.mark_librarian()
            if self.ticket.complaint_state == 'new':
                message = u'Byla vytvořena reklamace objednávky.'
                #notifications = ['wfcm_big_new_r', ]
                notifications = []
                self.add_historylog(
                    u'Byla vytvořena reklamace objednávky.',
                    who=['zk', 'r'],
                    )
            elif self.ticket.complaint_state == 'objected':
                self.ticket.unread_op = True
                self.ticket.reindexObject()
                #notifications = ['wfcm_big_std_r', 'wfcm_big_std_op', ]
                notifications = ['wfcm_big_std_op', ]
                self.add_historylog(
                    u'Byl změněn stav reklamace objednávky.  Momentální stav: {}.'.format(
                        self.ticket.complaint_label,
                        ),
                    who=['zk', 'r'],
                    )
            else:
                #notifications = ['wfcm_big_std_r', ]
                notifications = []
                self.add_historylog(
                    u'Byl změněn stav reklamace objednávky. Momentální stav: {}.'.format(
                        self.ticket.complaint_label,
                        ),
                    who=['zk', 'r'],
                    )
        elif who == 'reader':
            self.ticket.unread_zk = True
            if self.subticket_last.complaint_state == 'new':
                message = u'Byla vytvořena reklamace objednávky.'
                notifications = ['wfcm_big_new_zk', ]
                self.add_historylog(
                    u'Byla vytvořena reklamace objednávky.',
                    who=['zk', 'r'],
                    )
            elif self.ticket.complaint_state == 'objected':
                self.ticket.unread_op = True
                self.ticket.reindexObject()
                notifications = ['wfcm_big_std_zk', 'wfcm_big_std_op', ]
                self.add_historylog(
                    u'Byl změněn stav reklamace objednávky. Momentální stav: {}.'.format(
                        self.ticket.complaint_label,
                        ),
                    who=['zk', 'r'],
                    )
            else:
                notifications = ['wfcm_big_std_zk', ]
                self.add_historylog(
                    u'Byl změněn stav reklamace objednávky. Momentální stav: {}.'.format(
                        self.ticket.complaint_label,
                        ),
                    who=['zk', 'r'],
                    )
        elif who == 'operator':
            self.ticket.unread_r = True
            self.ticket.unread_zk = True
            self.ticket.unread_op = False
            self.ticket.reindexObject()
            #notifications = ['wfcm_big_std_zk', 'wfcm_big_std_r', ]
            notifications = ['wfcm_big_std_zk', ]
            self.add_historylog(
                u'Byl změněn stav reklamace objednávky. Momentální stav: {}.'.format(
                    self.ticket.complaint_label,
                    ),
                who=['zk', 'r'],
                )

        for notification in notifications:
            self.send_notifications(notification)

        self.add_message(message)

    def _complaint_create(self, who, text, is_soft=False):
        if DEBUG_complaint:
            print("_complaint_create", who, text, is_soft)
        # Vytvořit reklamaci aktuálního / posledního subticketu
        self.subticket_last.complaint_date = self.dt_as(DateTime(), 'isodt')
        if COMPLAINT_2:
            # Volný text reklamace
            self.subticket_last.complaint_text = text
            # Zda je požadováno nové dodání (jinak zastavení požadavku
            # a vrácení peněz)
            self.subticket_last.complaint_is_soft = is_soft
            # Kdo vytvořil reklamaci
            self.subticket_last.complaint_who = u'zk'
        else:
            self.subticket_last.complaint_text = text
            self.subticket_last.complaint_is_soft = False
        self.subticket_last.complaint_state = u'new'
        self.subticket_last.reindexObject()

        self._subticket_complaint_notify(who)

        # Pro nové dodání je to vše
        # FIXME-COMPLAINT Pro MVS nové dodání resetovat nastavení spojené
        # s půjčením čtenáři
        if COMPLAINT_2 and self.subticket_last.complaint_is_soft:
            return

        # zastavit ticket, není-li zastaven (viz NTK-38)
        if self.subticket_last.getId() != self.ticket.subticket_id:
            return
        self.try_next_request('cancel')
        comment = 'ticket was stopped due to created complaint of main subticket'
        wf_action(self.ticket, 'stop', comment)
        
        # Ujistit se, že je resetováno všechno nastavení spojené např.
        # s půjčením čtenáři.  Hraje roli při úspěšné reklamaci, že došlo něco
        # jiného, která byla zadána až po půjčení čtenáři a vrácení čtenářem.
        # Vedlejší efekt je, že toto půjčení čtenáři a vrácení čtenářem zmizí
        # z časové osy.
        is_resetted = False
        if self.ticket.reader_lent_date:
            self.ticket.reader_lent_date = None
            is_resetted = True
        if self.ticket.return_date:
            self.ticket.return_date = None
            is_resetted = True
        # SUGGESTION zvážit, zda neresetovat i samotnou reklamaci
        if is_resetted:
            self.add_historylog(
                u'Půjčení čtenáři a případně i vrácení čtenářem bylo smazáno z důvodu zastavení požadavku např. díky uznané reklamaci.',
                who=['zk', ],
                )

        self.ticket.reindexObject()

    def _complaint_objection(self, who, text):
        self.subticket_last.complaint_state = u'objected'
        self.subticket_last.objection_text = text
        self.subticket_last.reindexObject()
        self._subticket_complaint_notify(who)

    def _complaint_decide(self, who, decision):
        if decision == u'accept':
            self.subticket_last.complaint_state = u'accepted'
            self._subticket_complaint_accepted()
            self._subticket_complaint_notify(who)
            return True
        elif decision == u'decline':
            self.subticket_last.complaint_state = u'declined'
            self.subticket_last.reindexObject()
            self._subticket_complaint_notify(who)
            return True
        return False

    def _complaint_objection_decide(self, who, decision, text):
        if decision == u'accept':
            self.subticket_last.complaint_state = u'objection_accepted'
            self.subticket_last.objection_decision_text = text
            self._subticket_complaint_accepted(after_objection=True)
            self._subticket_complaint_notify(who)
            return True
        elif decision == u'decline':
            self.subticket_last.complaint_state = u'objection_declined'
            self.subticket_last.objection_decision_text = text
            self.subticket_last.reindexObject()
            self._subticket_complaint_notify(who)
            return True
        return False

    def _subticket_complaint_accepted(self, skip_stop=False, after_objection=False):
        """Provést samotnou reklamaci."""

        # FIXME-COMPLAINT pro měkkou reklamaci zcela jinak:
        # Neukončovat subticket, nevracet peníze, ale jen označit, že má
        # subticket měkkou reklamaci přijatou dnes a vrátit do běžného procesu
        # workflow, tzn. smazat complaint_state.
        # Každopádně neblokovat novou reklamaci a neblokovat další přirozený
        # běh.
        # Je otázka, co s termínem vrácení, zda ho neposunout, ale zatím nevíme
        # o kolik.  TODO
        # Seznam objednávek / požadavků lze filtrovat i ve výsledcích zobrazovat
        # na základě existence complaint_soft_date.  TODO
        # Deployment počítá s tím, že nebude žádná rozpracovaná reklamace.  TODO

        # vrátit peníze
        if not self.subticket_last.complaint_is_soft:
            # pokud už bylo zaneseno do reportu, tak vytvoř nový report, kde odečti stejnou částku
            if self.subticket_last.wf_review_state in ('sent', ):
                # create report entry
                obj_dict = dict(
                    report_date = DateTime(),
                    cena = -self.subticket_last.cena,
                    user_id = self.user_id,
                    entry_type = u'auto: complaint',
                    note = u'',
                    )
                report_entry = create_reportentry(self.subticket_last, obj_dict)  # is_std = False
                library_payment_mvs_complaint(self.subticket_last, report_entry, after_objection)

            wf_action(self.subticket_last, 'cancel', 'complaint accepted')
            self.subticket_last.reindexObject()

            # Operátor vyřizuje námitku pro subticket_last ve chvíli, kdy ticket
            # nemá aktivní subticket
            if self.subticket_last.getId() != self.ticket.subticket_id:
                return

            self.try_next_request('cancel')
            if not skip_stop:
                if not after_objection:
                    comment = 'subticket was cancelled due to accepted complaint'
                else:
                    comment = 'subticket was cancelled due to accepted complaint - after objection by operator'
                wf_action(self.ticket, 'stop', comment)
                self.ticket.reindexObject()

        # nové dodání (nevrací se peníze)
        else:
            now_iso = self.dt_as(DateTime(), 'iso')

            # označit, že má ticket měkkou reklamaci přijatou dnes
            self.subticket_last.complaint_soft_date = now_iso

            # stav soft znamená, že byla přijata malá měkká reklamace a je
            # v procesu zpracování, tzn. nového dodání, DK ukončí tento proces
            # novým dodáním PDF souboru – zatím jen EDD umožňuje malou měkkou
            # reklamaci
            self.subticket_last.complaint_state = u'soft'

            # následně změnit kontroly pro tento případ, změna termínů
            # pro vrácení atp., ale ne teď a ne na tomto místě

            self.subticket_last.reindexObject()
            self.ticket.reindexObject()

    def _subticket_complaint_notify(self, who):
        # setup: notification + mark + unread + message
        message = u'Stav reklamace požadavku byl změněn.'
        if who == 'zk':
            self.subticket_last.unread_dk = True
            self.mark_librarian()
            if self.subticket_last.complaint_state == 'new':
                message = u'Byla vytvořena reklamace požadavku.'
                if self.subticket_last.complaint_is_soft:
                    message += u'  Je požadováno nové dodání.'
                else:
                    message += u'  Je požadováno ukončení požadavku a vrácení ceny.'
                notifications = ['wfcm_small_new_dk', ]
                self.add_historylog(
                    message,
                    who=['zk', 'dk'],
                    subticket=self.subticket_last,
                    )
            elif self.subticket_last.complaint_state == 'objected':
                self.subticket_last.unread_op = True
                notifications = ['wfcm_small_std_dk', 'wfcm_small_std_op', ]
                self.add_historylog(
                    u'Byl změněn stav reklamace požadavku. Momentální stav: {}.'.format(
                        self.subticket_last.complaint_label,
                        ),
                    who=['zk', 'dk'],
                    subticket=self.subticket_last,
                    )
                self.subticket_last.reindexObject()
            else:
                notifications = ['wfcm_small_std_dk', ]
                self.add_historylog(
                    u'Byl změněn stav reklamace požadavku. Momentální stav: {}.'.format(
                        self.subticket_last.complaint_label,
                        ),
                    who=['zk', 'dk'],
                    subticket=self.subticket_last,
                    )
        elif who == 'dk':
            self.ticket.unread_zk = True
            self.mark_librarian()
            if self.subticket_last.complaint_state == 'new':
                message = u'Byla vytvořena reklamace požadavku.'
                notifications = ['wfcm_small_new_zk', ]
                self.add_historylog(
                    u'Byla vytvořena reklamace požadavku.',
                    who=['zk', 'dk'],
                    subticket=self.subticket_last,
                    )
            elif self.subticket_last.complaint_state == 'objected':
                self.subticket_last.unread_op = True
                notifications = ['wfcm_small_std_zk', 'wfcm_small_std_op', ]
                self.add_historylog(
                    u'Byl změněn stav reklamace požadavku. Momentální stav: {}.'.format(
                        self.subticket_last.complaint_label,
                        ),
                    who=['zk', 'dk'],
                    subticket=self.subticket_last,
                    )
                self.subticket_last.reindexObject()
            else:
                notifications = ['wfcm_small_std_zk', ]
                self.add_historylog(
                    u'Byl změněn stav reklamace požadavku. Momentální stav: {}.'.format(
                        self.subticket_last.complaint_label,
                        ),
                    who=['zk', 'dk'],
                    subticket=self.subticket_last,
                    )

        elif who == 'operator':
            self.subticket_last.unread_dk = True
            self.ticket.unread_zk = True
            self.subticket_last.unread_op = False
            self.subticket_last.reindexObject()
            notifications = ['wfcm_small_std_zk', 'wfcm_small_std_dk', ]
            self.add_historylog(
                u'Byl změněn stav reklamace požadavku. Momentální stav: {}.'.format(
                    self.subticket_last.complaint_label,
                    ),
                who=['zk', 'dk'],
                subticket=self.subticket_last,
                )
        for notification in notifications:
            self.send_notifications(notification)

        self.add_message(message)

    def _complaint_cancel_cancel(self):
        if DEBUG_cancel:
            print(' ')
            print('_complaint_cancel_cancel')
            print(' ')

        big_cancel_submit = False
        small_cancel_submit = False
        cancel_directly = False
        small_cancel_declined = False
        self.is_complaint_restriction = False

        # Někdy je nutné pracovat s aktivním požadavkem (self.subticket), ne
        # s posledním aktivním nezávisle na tom, jestli je stále ještě aktivní
        # (self.subticket_last).  Obecně pro blokování workflow a nové storno
        # je potřeba aktivní, pro info poslední aktivní.

        self.small_cancel_decline_info = u''
        if self.subticket_last and self.subticket_last.is_small_cancel_declined:
            if DEBUG_cancel:
                print('small cancel *declined')
            small_cancel_declined = True
            self.small_cancel_decline_info = u'Žádost o storno byla dožádanou knihovnou zamítnuta.'

        # Jsme na objednávce, jen čtenář a ŽK může dělat úpravy. operátor ne
        # a nikdo jiný ani nevidí.
        # Změna: NTK-135 Přidat možnost SC odebírat požadavky: Umožnit SC
        # stornovat požadavek, který zatím nebyl ani přijat ani podmínečně
        # přijat.
        if self.is_ticket:
            if not self.is_reader and not self.is_librarian_zk_op:
                return False

            # Reklamace a storno se navzájem významně ovlivňují (vylučují),
            # např. když je podána reklamace, nelze storno plnohodnotně, ale ani
            # nelze zcela vypnout, protože čtenář může podat žádost o storno,
            # resp. knihovník ŽK tuto žádost může podat za něj, dále pak
            # knihovník ŽK může na storno reagovat (zamítnout) atp.
            if self.is_librarian_zk and self.subticket and \
                    self.subticket.is_complaint:
                if self.subticket.complaint_state in ('new', 'objected', ):
                    #logger.info('is_complaint_restriction YES: subticket complaint open therefore ticket cancel disabled')
                    self.is_complaint_restriction = True
                elif self.subticket.complaint_state in ('accepted', 'objection_accepted', ):
                    #logger.info('is_complaint_restriction YES: subticket complaint accepted therefore ticket cancel disabled')
                    self.is_complaint_restriction = True
                    if DEBUG_cancel:
                        print('END complaint_state *accepted')
                    return False
                elif self.subticket.complaint_state in ('declined', 'objection_declined', ):
                    #logger.info('is_complaint_restriction YES: subticket complaint declined')
                    self.is_complaint_restriction = True
                    #return False

            # Umožnit SC stornovat požadavek, který zatím nebyl ani přijat ani
            # podmínečně přijat.
            if self.is_ziskej_operator and not self.is_sudo and \
                    not self.ticket.is_big_cancel_asked and \
                    self.subticket and \
                    not self.is_complaint_restriction and \
                    not small_cancel_declined and \
                    self.subticket.wf_review_state == 'queued':
                cancel_directly = True
                self.show_small_cancel = True
                self.small_cancel_desc = u''

            # další část neplatí pro operátora, proto přeskočit
            if self.is_ziskej_operator and not self.is_sudo:
                pass

            # zjistíme, zda aktuální uživatel může přímo udělat velké resp. malé
            # storno a vše podle toho nastavíme
            elif not self.ticket.is_big_cancel_asked and (
                    (self.is_reader and self.ticket.can_cancel_directly_reader) or \
                    (self.is_librarian_zk and self.ticket.can_cancel_directly)
                    ):
                self.show_big_cancel = True
                cancel_directly = not self.is_complaint_restriction
                self.big_cancel_desc = u'Storno objednávky'
                # jen ŽK může malé storno, ale jen pokud existuje aktuální subticket
                if self.is_librarian_zk and self.subticket and \
                        not self.is_complaint_restriction and \
                        not small_cancel_declined:
                    self.show_small_cancel = True
                    self.small_cancel_desc = u'Storno požadavku'

            # pokud ne, zjistíme, zda může alespoň zažádat o velké resp. malé storno a vše podle toho nastavíme
            elif self.ticket.can_cancel_ask_or_directly:
                if not self.ticket.is_big_cancel_asked:
                    self.show_big_cancel = True
                    self.big_cancel_desc = u'Žádost o storno objednávky'
                elif self.is_librarian_zk:
                    if not self.ticket.cancellation_big_ask_authorized:
                        # odmítnout žádost čtenáře o velké storno nebo přijmout
                        # a v tom případě rovnou udělat pokud můžu, jinak
                        # autorizovat a automaticky vytvořit žádost o malé storno
                        self.show_big_cancel_answer = True
                        # jen zamítnout, pokud ŽK už reklamuje požadavek
                        if self.is_complaint_restriction:
                            self.show_big_cancel_answer_only_decline = True
                    elif self.subticket and not self.subticket.is_small_cancel_asked:
                        # bylo zažádáno o velké storno a toto bylo autorizováno
                        # a je aktivní subticket, který nemá žádnou žádost
                        # o malé storno, tzn. ta automatická z autorizace
                        # (vytvoření) velkého storna byla zamítnuta
                        self.show_big_cancel_answer = True
                        self.show_big_cancel_answer_only_decline = True
                    elif not self.subticket:
                        # bylo zažádáno o velké storno a toto bylo autorizováno
                        # a není aktivní subticket tzn. ŽK může rozhodnout
                        # o velkém stornu
                        self.show_big_cancel_answer = True
                    else:
                        logger.info('')
                        self.big_cancel_info = u'Žádost o storno objednávky byla podána a čeká se na odpověď.'
                else:
                    self.big_cancel_info = u'Žádost o storno objednávky byla podána a čeká se na odpověď.'

                # Jen ŽK může malé storno, ale jen pokud existuje aktuální
                # subticket.  V tom případě platí
                # self.subticket == self.subticket_last
                if self.is_librarian_zk and self.subticket and \
                        not self.is_complaint_restriction:
                    # existuje žádost o velké storno?
                    if self.ticket.is_big_cancel_asked:
                        # existuje aktuální žádost o malé storno?
                        if self.subticket.is_small_cancel_asked:
                            self.small_cancel_info = u'Žádost o storno požadavku byla podána v rámci storna objednávky (nebo před ním) a čeká se na odpověď.'
                        elif not small_cancel_declined:
                            self.show_small_cancel = True
                            self.small_cancel_desc = u'Žádost o storno požadavku'
                    else:
                        if self.subticket.is_small_cancel_asked:
                            self.small_cancel_info = u'Žádost o storno požadavku byla podána a čeká se na odpověď.'
                        elif not small_cancel_declined:
                            self.show_small_cancel = True
                            self.small_cancel_desc = u'Žádost o storno požadavku'

            # Nelze aktivně zažádat o velké storno, ale je možné, že už dříve
            # bylo zažádáno a alespoň je potřeba zobrazit informace z dřívějšího
            # pokusu
            else:
                if self.ticket.is_big_cancel_asked:
                    self.big_cancel_info = u'Žádost o storno objednávky byla podána a čeká se na odpověď.'
                elif self.ticket.is_big_cancel_declined:
                    if self.is_librarian_zk and self.ticket.wf_review_state in ('pending', ):
                        # Tato situace v současném workflow nenastává
                        self.big_cancel_info = u'Žádost o storno objednávky byla zamítnuta.  Ale nyní není žádný aktivní požadavek, zvažte zda nepřehodnotit zamítnutí.'
                    else:
                        self.big_cancel_info = u'Žádost o storno objednávky byla zamítnuta.'

            if DEBUG_cancel:
                for k in (
                        'is_complaint_restriction',
                        'show_big_cancel',
                        'show_big_cancel_answer',
                        'show_big_cancel_answer_only_decline',
                        'show_small_cancel',
                        ):
                    value = getattr(self, k, None)
                    print('{}: {}'.format(k, value))
                print(' ')

            # pokud nezobrazujeme formulář (pozor info zobrazit můžeme), tak se
            # nemá smysl ptát, zda už nebyl použit, takže končíme
            if not self.show_big_cancel and not self.show_small_cancel and not self.show_big_cancel_answer:
                return False

            # nyní už víme, co aktuální uživatel může, tak jdeme zjistit, jestli
            # to už neudělal, tím je provedena autorizace

            # uživatel může udělat nebo zažádat o velké storno
            if self.show_big_cancel:
                big_cancel_submit = self.request.get('big_cancel', None)
                # a zároveň může rovnou udělat, tak udělat
                if big_cancel_submit and cancel_directly:
                    self._ticket_cancel()
                    self.redirect()
                    return True
                # jinak jen zažádat
                elif big_cancel_submit:
                    # pro ŽK obsahuje rovnou i žádost o malé storno, pro čtenáře
                    # ne, musí ŽK prve autorizovat (nebo rovnou udělat, pokud ŽK
                    # na rozdíl od čtenáře může rovnou udělat)
                    self._ticket_cancel_ask()
                    self.redirect()
                    return True

            # ŽK může reagovat na čtenářovu žádost o velké storno
            if self.show_big_cancel_answer:
                big_cancel_accept_submit = self.request.get('big_cancel_accept', None)
                big_cancel_decline_submit = self.request.get('big_cancel_decline', None)

                # žádost přijata
                if big_cancel_accept_submit:
                    if self.ticket.can_cancel_directly:
                        self._ticket_cancel()
                    else:
                        self._ticket_cancel_accept()
                    self.redirect()
                    return True
                # žádost odmítnuta
                elif big_cancel_decline_submit:
                    self._ticket_cancel_decline()
                    self.redirect()
                    return True

            # uživatel může udělat nebo zažádat o malé storno
            if self.show_small_cancel:
                small_cancel_submit = self.request.get('small_cancel', None)
                # a zároveň může rovnou udělat, tak udělat
                if small_cancel_submit and cancel_directly:
                    self._subticket_cancel()
                    self.redirect()
                    return True
                # jinak jen zažádat
                elif small_cancel_submit:
                    self._subticket_cancel_ask()
                    self.redirect()
                    return True

        # subticket a DK - reakce na žádost o malé storno
        elif self.is_subticket and self.is_ziskej_operator:
            # Umožnit SC stornovat požadavek, který zatím nebyl ani přijat ani
            # podmínečně přijat.
            if self.subticket.wf_review_state == 'queued':
                self.show_small_cancel_op = True
            else:
                return False

            small_cancel_op_submit = self.request.get('small_cancel_op', None)
            if small_cancel_op_submit:
                self._subticket_cancel()
                self.redirect()
                return True

        # subticket a DK - reakce na žádost o malé storno
        elif self.is_subticket:
            if not self.is_librarian_dk:
                return False

            if not self.subticket_last.is_small_cancel_asked:
                return False

            small_cancel_accept_submit = self.request.get('small_cancel_accept', None)
            small_cancel_decline_submit = self.request.get('small_cancel_decline', None)

            # žádost přijata
            if small_cancel_accept_submit:
                self._subticket_cancel_accept()
                self.redirect()
                return True
            # žádost odmítnuta
            elif small_cancel_decline_submit:
                self._subticket_cancel_decline()
                self.redirect()
                return True
            # jinak jen zobraz formulář
            else:
                self.show_small_cancel_answer = True

        return False

    def _ticket_cancel_ask(self):
        date_str = self.dt_as(DateTime(), 'isodt')
        self.ticket.cancellation_big_ask_date = date_str
        message = u'Bylo požádáno o storno objednávky (tzv. velké storno).'
        self.add_message(message)
        self.add_historylog(
            u'Bylo požádáno o storno objednávky.',
            who=['zk', 'r'],
            )
        if self.is_reader:
            self.ticket.cancellation_big_ask_authorized = False
            self.send_notifications('wfcc_big_ask_zk')
        elif self.is_librarian_zk:
            self.ticket.cancellation_big_ask_authorized = True
            #self.send_notifications('wfcc_big_ask_r')
            # Pokud ex. hlavní požadavek a ŽK může zažádat o jeho malé storno,
            # udělat to hned
            message = u''
            if not self.subticket:
                pass
            elif self.show_small_cancel:
                # Stejné datum žádosti o malé storno v rámci žádosti o velké
                # storno
                self._subticket_cancel_ask(date_str=date_str)
            elif self.subticket.is_small_cancel_declined:
                message = u'Nemohla být vytvořena žádost o storno požadavku, protože byla už dříve odmítnuta.'
            elif self.subticket.is_small_cancel_asked:
                message = u'Nemohla být vytvořena žádost o storno požadavku, protože už existuje.'
            if message:
                self.add_message(message)
                self.add_historylog(
                    message,
                    who=['zk', ],
                    )

    def _ticket_cancel_accept(self):
        # toto dělá ŽK a nemůže direct
        if not self.is_librarian_zk:
            raise Exception('Internal error')
        self.ticket.cancellation_big_ask_authorized = True
        self.add_historylog(
            u'Byla přijata žádost o storno objednávky.',
            who=['zk', 'r'],
            )
        # Pokud ex. hlavní požadavek a ŽK může zažádat o jeho malé storno,
        # udělat to
        if self.subticket and self.show_small_cancel:
            self._subticket_cancel_ask()
        else:
            message = u'Nemohla být vytvořena žádost o storno požadavku, protože už dříve byla odmítnuta.'
            self.add_message(message)
            self.add_historylog(
                message,
                who=['zk', ],
                )

    def _ticket_cancel_decline(self, date_str=None, is_message=True):
        if date_str is None:
            date_str = self.dt_as(DateTime(), 'isodt')
        # MVS
        # toto dělá ŽK
        # případ, kdy DK nepřijme neviolá automaticky tento kód, ale nechává na
        # ŽK co dál
        #self.send_notifications('wfcc_big_decline_r')
        # EDD
        # toto je automatický důsledek odmítnutého malého storna
        self.ticket.cancellation_big_answer_date = date_str
        if is_message:
            message = u'Byla zamítnuta žádost o storno objednávky.'
            self.add_message(message)
        self.add_historylog(
            u'Byla zamítnuta žádost o storno objednávky.',
            who=['zk', 'r'],
            )

    def _subticket_cancel_ask(self, date_str=None):
        if date_str is None:
            date_str = self.dt_as(DateTime(), 'isodt')
        self.subticket.cancellation_small_ask_date = date_str
        message = u'Bylo požádáno o storno požadavku (tzv. malé storno).'
        self.add_message(message)
        self.send_notifications('wfcc_small_ask_dk')
        self.add_historylog(
            u'Bylo požádáno o storno požadavku.',
            who=['zk', 'dk'],
            subticket=self.subticket,
            )

    def _subticket_cancel_accept(self):
        self.add_historylog(
            u'Byla přijata žádost o storno požadavku.',
            who=['zk', 'dk'],
            subticket=self.subticket_last,
            )
        if self.ticket.is_big_cancel_asked:
            self._ticket_cancel()
            self.send_notifications('wfcc_small_big_accept_zk')
            #self.send_notifications('wfcc_small_big_accept_r')
        else:
            self._subticket_cancel()
            self.send_notifications('wfcc_small_accept_zk')

    def _subticket_cancel_decline(self):
        self.send_notifications('wfcc_small_decline_zk')
        date_str = self.dt_as(DateTime(), 'isodt')
        self.subticket.cancellation_small_answer_date = date_str
        message = u'Byla zamítnuta žádost o storno požadavku (tzv. malé storno).'
        self.add_message(message)
        self.add_historylog(
            u'Byla zamítnuta žádost o storno požadavku.',
            who=['zk', 'dk'],
            subticket=self.subticket,
            )
        # EDD automaticky odmítne i žádost o storno objednávky, jinak by nebylo
        # možné odeslat z DK dřív, než ŽK nějak zareaguje.
        if self.ticket.is_edd and self.ticket.is_big_cancel_asked:
            self._ticket_cancel_decline(date_str=date_str, is_message=False)

    def _ticket_cancel(self):
        if not self.ticket.can_cancel:
            raise Exception('Invalid internal state')

        if self.subticket:
            self._subticket_cancel(skip_stop=True)

        wf_action(self.ticket, 'cancel', '')
        message = u'Objednávka byla stornována.'
        self.add_message(message)
        now_iso = self.dt_as(DateTime(), 'iso')
        self.ticket.closed_date = now_iso

        if self.is_reader:
            self.ticket.unread_zk = True
            self.send_notifications('wf_t_cancel_by_reader_zk')
        elif self.is_librarian_zk:
            self.mark_librarian()
            self.ticket.unread_r = True
            self.send_notifications('wf_t_cancel_by_zk_reader')
        else:
            self.ticket.unread_zk = True
            self.ticket.unread_r = True
            self.send_notifications('wf_t_cancel_r_zk')
        self.add_historylog(
            u'Objednávka byla stornována.',
            who=['zk', 'r'],
            )
        self.ticket.reindexObject()

    def _subticket_cancel(self, skip_stop=False):
        if not self.subticket.can_cancel:
            raise Exception('Invalid internal state')

        # pokud už bylo zaneseno do reportu, tak vytvoř nový report, kde odečti stejnou částku
        if self.subticket.wf_review_state in ('sent', ):
            # create report entry
            obj_dict = dict(
                report_date = DateTime(),
                cena = -self.subticket.cena,
                user_id = self.user_id,
                entry_type = u'auto: cancel',
                note = u'',
                )
            report_entry = create_reportentry(self.subticket, obj_dict)  # is_std = False
            library_payment_mvs_cancel(self.subticket_last, report_entry)

        if self.is_ziskej_operator:
            skip_stop = True
            wf_comment = 'small cancellation by operator'
            message = u'Požadavek byl odebrán Servisním centrem ZÍSKEJ a tedy stornován.'
        else:
            wf_comment = 'small or big cancellation'
            message = u'Požadavek byl stornován.'

        wf_action(self.subticket, 'cancel', wf_comment)

        self.add_message(message)

        if self.is_ziskej_operator:
            self.subticket.unread_dk = True
            self.subticket.unread_zk = True
            self.send_notifications('wf_st_cancel_op_zk')
            self.send_notifications('wf_st_cancel_op_dk')
        elif self.is_librarian_zk:
            self.mark_librarian()
            self.subticket.unread_dk = True
            self.send_notifications('wf_st_cancel_dk')
        elif self.is_librarian_dk:
            self.mark_librarian()
            self.subticket.unread_zk = True
            self.send_notifications('wf_st_cancel_zk')

        self.add_historylog(
            message,
            who=['zk', 'dk'],
            subticket=self.subticket,
            )
        self.subticket.reindexObject()

        if self.is_ziskej_operator:
            is_created = self.try_next_request('cancel_assign')
            if is_created:
                message = u'Byl úspěšně vytvořen nový požadavek.'
                self.add_message(message)
            else:
                message = u'Nemohl být vytvořen nový požadavek, objednávka je zastavena a čeká se na další rozhodnutí ŽK.'
                self.add_message(message)
                self.send_notifications('wf_t_stop_zk')
        else:
            self.try_next_request('cancel')

        if not skip_stop:
            wf_action(self.ticket, 'stop', 'subticket was cancelled')
            if self.is_librarian_zk:
                message = u'Objednávka byla pozastavena, čeká se na další rozhodnutí ŽK.'
                self.add_message(message)
            elif self.is_librarian_dk:
                message = u'Požadavek byl úspěšně stornován.'
                self.add_message(message)
                self.send_notifications('wf_t_stop_zk')

        self.ticket.reindexObject()
