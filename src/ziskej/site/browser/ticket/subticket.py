# -*- coding: utf-8 -*-
"""BrowserView: Subticket.
"""

from __future__ import print_function

from datetime import datetime
import logging

from DateTime import DateTime
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.protect.utils import addTokenToUrl
from plone.protect.auto import safeWrite
from plone.protect.interfaces import IDisableCSRFProtection
from zope.interface import alsoProvides

from ziskej.site import VISIT_RESETS_UNREAD_OP
from ziskej.site.browser.ticket.ticket_core import (
    TicketCoreView,
    BTNCSS_DEFAULT,
    BTNCSS_PRIMARY,
    )
from ziskej.site.utils import datetime_add_d
from ziskej.site.ticket_helpers import get_cpk_url
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger("ziskej")

DEBUG = False


class SubticketView(TicketCoreView):

    template = ViewPageTemplateFile('subticket.pt')

    def setup_ticket_subticket(self):
        self.is_ticket = False
        self.is_subticket = True
        self.subticket = self.context
        self.subticket_last = self.subticket
        self.ticket = self.context.__parent__
        if not self.ticket:
            # nenastane a pokud, tak je to chyba, která si vyžaduje pozornost
            logger.error('subticket browser view: no parent for ' + str(self.context))
            self.redirect(self.portal_url)
            return True

        subticket_ids = self.ticket.subticket_ids
        if not subticket_ids:
            subticket_ids = []
        self.subtickets = []
        for item_id in subticket_ids:
            item = getattr(self.ticket, item_id, None)
            if not item:
                continue
            # LIFO, později vytvořené jsou aktuálnější
            self.subtickets.insert(0, item)

        if self.ticket.doc_id:
            self.doc_url = get_cpk_url(self.ticket.doc_id)
        else:
            self.doc_url = u''

        # bez parametrů v requestu měníme objekt bezpečně - jen přečteno atp.
        if len(self.request.form.keys()) == 0:
            if DEBUG:
                print('alsoProvides(self.request, IDisableCSRFProtection)')
            alsoProvides(self.request, IDisableCSRFProtection)

        # initial values
        self.js_vars = u''
        self.show_lib_edit = False

        # možné přechody
        self.subticket_transitions = []
        self.st_lib_edit_btn = BTNCSS_DEFAULT
        self.show_small_cancel_answer = False

        # url_ajax_messages_mark_read
        self.url_ajax_messages_mark_read = self.context_url + '/ajax_messages_mark_read'
        self.url_ajax_messages_mark_read = addTokenToUrl(self.url_ajax_messages_mark_read)
        self.js_vars += u"var url_ajax_messages_mark_read='{}';\n".format(self.url_ajax_messages_mark_read)

        self.library_zk = getattr(self.portal.libraries, self.subticket.library_zk, None)
        self.library_dk = getattr(self.portal.libraries, self.subticket.library_dk, None)
        if self.library_zk is None or self.library_dk is None:
            raise Exception('Internal error')

        return False

    def setup_subticket_data(self):
        # id, library
        subticket = self.subticket
        subticket_id = subticket.getId()
        ticket = subticket.ticket

        # librarian
        librarian_zk_html = None
        librarian_dk_html = None
        if ticket.librarian:
            librarian_zk_html = u'<a href="{library_url}/{librarian}" target="_blank">{librarian}</a>'.format(
                library_url=self.library_zk.absolute_url(),
                librarian=ticket.librarian,
                )
        if subticket.librarian:
            librarian_dk_html = u'<a href="{library_url}/{librarian}" target="_blank">{librarian}</a>'.format(
                library_url=self.library_dk.absolute_url(),
                librarian=subticket.librarian,
                )

        # messages
        messages = subticket.get_messages()
        tab_messages_total = len(messages)
        if subticket.pm_unread == 'library_dk':
            tab_messages_unread = 1
        else:
            tab_messages_unread = 0
        envelope_css = u'fa fa-envelope-open'
        if tab_messages_unread:
            envelope_css = u'fa fa-envelope ziskej-orange-std'
        elif not tab_messages_total:
            envelope_css = u'fa fa-envelope-open-o'

        # to mohlo napáchat škody v datech, opravit je za běhu
        #self.subticket.cena = u'{}'.format(self.subticket.cena)
        if self.subticket.cena is not None and \
                isinstance(self.subticket.cena, basestring):
            self.subticket.cena = int(self.subticket.cena)

        # tab data
        tab = dict(
            subticket_id = subticket_id,
            library_sigla = subticket.library_zk.upper(),
            library_fullname = self.library_zk.fullname,
            library_href = self.library_zk.absolute_url(),
            library_address = self.library_zk.Address(),
            librarian_zk_html = librarian_zk_html,
            librarian_dk_html = librarian_dk_html,
            doc_sent = subticket.doc_sent,
            pm_unread = tab_messages_unread,
            pm_total = tab_messages_total,
            display_messages = bool(tab_messages_total),
            envelope_css = envelope_css,
            fee = subticket.cena,
            pages_number_dk = subticket.pages_number_dk,
            is_pdf_uploaded = subticket.Is_pdf_uploaded(),
            wf_review_state = subticket.wf_review_state,
            wf_review_state_label = subticket.wf_review_state_label,
            )
        self.subticket_data = tab

    def redirect_when_needed(self):
        # ZK
        if not self.is_librarian_dk and self.is_librarian_zk:
            self.redirect(self.ticket.absolute_url())
            return True

        # not (DK, OP)
        if not self.is_librarian_dk_op:
            self.redirect(self.portal_url)
            return True

        return False

    def setup_texts(self):
        if self.is_librarian_dk_op:
            self.title = self.subticket.fullname

    def add_info(self):
        self.cena_editable = False
        self.pages_number_dk_editable = False
        self.pdf_file_editable = False
        self.back_date_editable = False
        self.back_date_editable_full = False
        self.prolong = False
        self.show_small_complaint_soft_finished = False
        self.show_small_complaint_soft_unfinished = False

        # jen DK (ani operátor ne) a jen v určitých stavech smí alespoň něco
        # editovat
        if (not self.is_librarian_dk or
                self.subticket.wf_review_state not in (
                    'accepted', 'conditionally_accepted', 'sent', 
                    )):
            return False
        if self.ticket.is_edd:
            if self.subticket.complaint_state in ('soft', 'soft_ready', ):
                pass  # Požadováno nové dodání, tedy výjimka
            elif self.subticket.wf_review_state != 'accepted':
                return False
        self.show_lib_edit = True

        if (self.ticket.is_mvs or self.ticket.is_mrs) and \
                self.subticket.wf_review_state in (
                'queued', 'accepted', 'conditionally_accepted', ):
            self.cena_editable = True
            self.back_date_editable = True
            self.back_date_editable_full = True

            # pokud bylo podmínečně přijato se zkrácenou výpůjční
            # lhůtou, tak použij počet dnů z ní, jinak standard (28)
            if self.subticket.cond_a_return_d:
                back_date_min = datetime_add_d(self.subticket.cond_a_return_d)
            else:
                back_date_min = datetime_add_d(
                    ziskej_parameter('st_return_min_days')  # 28
                    )
            self.back_date_editable_min = self.dt_as(
                back_date_min,
                result_type='datepicker',
                )

        # Už odesláno, tak DK může jen zlepšit - prodloužit
        elif (self.ticket.is_mvs or self.ticket.is_mrs) and \
                self.subticket.wf_review_state in ('sent', ):
            self.show_lib_edit = True
            self.back_date_editable = True
            self.prolong = True
            self.back_date_editable_min = self.dt_as(
                DateTime(self.subticket.back_date),
                result_type='datepicker',
                )

        elif self.ticket.is_edd and \
                self.subticket.wf_review_state == 'accepted':
            self.pages_number_dk_editable = True
            self.pdf_file_editable = True
        elif self.ticket.is_edd and \
                self.subticket.complaint_state in ('soft', 'soft_ready', ):
            self.pdf_file_editable = True
            if self.subticket.complaint_state == 'soft_ready':
                self.show_small_complaint_soft_finished = True
            else:
                self.show_small_complaint_soft_unfinished = True

        self.cena_mvs_default = self.library_dk.Mvs_dk_cena()  # 70
        self.cena_mvs_min = self.cena_mvs_default
        self.cena_mvs_max = self.cena_mvs_default
        if self.subticket.cond_a_fee is not None and \
                self.subticket.cond_a_fee > self.cena_mvs_max:
            self.cena_mvs_max = self.subticket.cond_a_fee
        self.pages_number_dk_min = 1
        if self.ticket.is_edd_article:
            self.pages_number_dk_max = 1000
        else:
            self.pages_number_dk_max = 20

        # form Doplnit informace
        # form - submit
        if self.request.get('submit_add_info', None):

            # validation
            is_updated = False
            obj_dict = dict()
            if self.cena_editable:
                # Implicitní validace v int_from_request je nezáporné celé
                # číslo, jinak vrátí None
                obj_dict['cena'] = self.int_from_request('cena')
                if obj_dict['cena'] > self.cena_mvs_max:
                    obj_dict['cena'] = self.cena_mvs_max
                    self.add_message(u'Maximálně cena za MVS je {} Kč.  Pokud by měla být cena vyšší, je potřeba přijmout podmínečně s důvodem vyšší cena.'.format(self.cena_mvs_max))
                elif obj_dict['cena'] < self.cena_mvs_min:
                    obj_dict['cena'] = self.cena_mvs_default
                    self.add_message(u'Minimální cena za MVS je {} Kč.'.format(self.cena_mvs_default))

            if self.pages_number_dk_editable:
                obj_dict['pages_number_dk'] = self.int_from_request(
                    'pages_number_dk')
                if obj_dict['pages_number_dk'] > self.pages_number_dk_max:
                    obj_dict['pages_number_dk'] = None
                    self.add_message(u'Maximální počet stran je {}.'.format(self.pages_number_dk_max))
                elif obj_dict['pages_number_dk'] < self.pages_number_dk_min:
                    obj_dict['pages_number_dk'] = None
                    self.add_message(u'Minimální počet stran je {}.'.format(self.pages_number_dk_min))
                if obj_dict['pages_number_dk'] is not None:
                    obj_dict['cena'] = self.subticket.cena_from_pages_number(
                        obj_dict['pages_number_dk'])
                else:
                    edd_data = self.ticket.Edd_data()
                    obj_dict['cena'] = edd_data['pages_number']
                # FIXME-EDD-DILIA
                print(self.request.form)
                dilia_dict = dict()
                dilia_dict['rok_vydani'] = self.int_from_request('dilia_rok_vydani')
                dilia_dict['zeme_vydani'] = self.text_from_request('dilia_zeme_vydani')
                dilia_dict['vydavatel'] = self.text_from_request('dilia_vydavatel')
                dilia_dict['is_kolektiv'] = self.bool_from_request('dilia_is_kolektiv')
                dilia_dict['autor_jmeno_1'] = self.text_from_request('dilia_autor_jmeno_1')
                dilia_dict['autor_prijmeni_1'] = self.text_from_request('dilia_autor_prijmeni_1')
                dilia_dict['autor_jmeno_2'] = self.text_from_request('dilia_autor_jmeno_2')
                dilia_dict['autor_prijmeni_2'] = self.text_from_request('dilia_autor_prijmeni_2')
                dilia_dict['autor_jmeno_3'] = self.text_from_request('dilia_autor_jmeno_3')
                dilia_dict['autor_prijmeni_3'] = self.text_from_request('dilia_autor_prijmeni_3')

                dilia_updated = self.ticket.update_edd_dilia_data_if_changed(
                    **dilia_dict)
                if dilia_updated:
                    is_updated = True

            if self.back_date_editable:
                obj_dict['back_date'] = self.date_from_request('back_date', result_type='iso')

                # pokud bylo podmínečně přijato se zkrácenou výpůjční
                # lhůtou, tak použij počet dnů z ní, jinak standard (28)
                if self.subticket.cond_a_return_d:
                    back_date_min = datetime_add_d(
                        self.subticket.cond_a_return_d
                        ).strftime('%Y-%m-%d')
                else:
                    back_date_min = datetime_add_d(
                        ziskej_parameter('st_return_min_days')  # 28
                        ).strftime('%Y-%m-%d')

                if not self.back_date_editable_full and \
                        obj_dict['back_date'] < self.subticket.back_date:
                    del obj_dict['back_date']
                    self.add_message(u'Po odeslání nemůžete datum požadovaného vrácení posunout na dřívější termín.')
                elif self.back_date_editable_full and obj_dict['back_date'] and \
                        obj_dict['back_date'] < back_date_min:
                    del obj_dict['back_date']
                    self.add_message(u'Datum požadovaného vrácení je dříve, než uplyne minimální doba, zůstává nezměněno.')

            # obj_dict může být i prázdný, pak je result určitě False
            if self.subticket.update_attrs_if_changed(**obj_dict):
                is_updated = True

            if self.pdf_file_editable:
                pdf_file_remove = self.bool_from_request('pdf_file_remove')
                pdf_file = self.request.form.get('pdf_file', None)
                pdf_file_filename = None
                pdf_file_ctype = None
                pdf_file_content = None
                pdf_file_csize = None
                if pdf_file:
                    pdf_file_filename = pdf_file.filename
                if pdf_file_filename:
                    pdf_file_ctype = pdf_file.headers.get('content-type', None)
                if pdf_file_ctype == 'application/pdf':
                    pdf_file_content = pdf_file.read()
                    pdf_file_csize = len(pdf_file_content)
                if pdf_file_csize and DEBUG:
                    print(" ")
                    print("form")
                    print(self.request.form.keys())
                    print("pdf_file", repr(pdf_file))
                    print("pdf_file.filename", repr(pdf_file_filename))
                    print("pdf_file_ctype", pdf_file_ctype)
                    print("pdf_file_csize", pdf_file_csize)
                    #print("pdf_file dir", dir(pdf_file))
                    #print(" ")
                    #print("pdf_file.headers", repr(pdf_file.headers))
                    #print("pdf_file.headers dir", dir(pdf_file.headers))
                    #print("pdf_file.headers.items", repr(pdf_file.headers.items()))
                    #print("pdf_file.headers.get content-type", repr(pdf_file.headers.get('content-type', None)))
                    #print("pdf_file.name", pdf_file.name)
                    print(" ")
                if self.subticket.Is_pdf_uploaded() and (pdf_file_remove or
                        pdf_file_csize):
                    self.subticket.remove_pdf_file()
                    is_updated = True
                if pdf_file_csize:
                    self.subticket.add_pdf_file(
                        pdf_file_content, pdf_file_filename)
                    if DEBUG:
                        print("pdf_file_id", self.subticket.pdf_file_id)
                    is_updated = True
                    # EDD reklamace, nové dodání
                    if self.subticket.complaint_state == 'soft':
                        self.subticket.complaint_state = 'soft_ready'

            if is_updated:
                # Některé atributy se z aktivního subticketu přenášejí do
                # příslušného ticketu, např. cena pro MVS.
                if self.ticket.is_mvs:
                    if 'cena' in obj_dict:
                        self.ticket.update_attrs_if_changed(cena=obj_dict['cena'])
                elif self.ticket.is_mrs:
                    pass  # FIXME-MRS
                elif self.ticket.is_edd:
                    # Pro EDD nepřenášet skutečný počet stran a PDF ID, ale brát
                    # vždy z aktivního subticketu.
                    pass
                    # Přepočítat cenu pro čtenáře (je odlišná od ceny pro DK)
                    if 'cena' in obj_dict:
                        self.ticket.update_edd_cena()

                message = u'Informace byly doplněny.'
                self.add_message(message)

                # unread
                self.ticket.unread_zk = True
                if DEBUG:
                    print("unread_zk")

                self.mark_librarian()

                if self.ticket.is_mvs:
                    self.add_historylog(
                        u'Informace o požadavku byly doplněny. Aktuálně platné údaje jsou: cena <strong>{}</strong> Kč, požadováno zpět <strong>{}</strong>.'.format(
                            self.subticket.cena,
                            self.date_format_html(self.subticket.back_date),
                            ),
                        who=['dk'])
                elif self.ticket.is_mrs:
                    pass  # FIXME-MRS
                elif self.ticket.is_edd:
                    self.add_historylog(
                        u'Informace o požadavku byly doplněny. Aktuálně platné údaje jsou: skutečný počet stran <strong>{}</strong>, PDF soubor <strong>{}</strong>, cena DK <strong>{}</strong> Kč.'.format(
                            self.subticket.pages_number_dk,
                            self.subticket.Is_pdf_uploaded() and u'ANO' or u'NE',
                            self.subticket.cena,
                            ),
                        who=['dk'])

                self.ticket.reindexObject()
                self.subticket.reindexObject()

                self.redirect(self.context_url)
                return True
            else:
                message = u'Nebyla zadána žádná změna.'
                self.add_message(message)

        if self.show_small_complaint_soft_finished and \
                self.request.get('small_complaint_soft_finished', None):

            # Stav soft znamená, že byla přijata malá měkká reklamace a je
            # v procesu zpracování, tzn. nového dodání, DK ukončí tento proces
            # novým dodáním PDF souboru – zatím jen EDD umožňuje malou měkkou
            # reklamaci.
            # Stav soft_ready je stav soft poté co byl změněn soubor.
            # Stav soft_done je stav soft po novém dodání.
            self.subticket.complaint_state = 'soft_done'

            now_iso = self.dt_as(DateTime(), 'iso')
            self.subticket.complaint_soft_resent_date = now_iso

            # Resetovat nastavení
            self.subticket.downloaded_number = 0
            self.subticket.downloaded_date = None

            # Nastavit expiraci PDF souboru
            dt_exp = DateTime() + ziskej_parameter('edd_expire_days')  # 21
            # no timezone
            pdt_exp = datetime.strptime(
                dt_exp.strftime('%Y-%m-%d %-H:%M'), '%Y-%m-%d %H:%M')
            if DEBUG:
                print("-- EDD DEBUG: set download_expiration_date to", pdt_exp)
            self.subticket.download_expiration_date = pdt_exp

            # Přidat záznam do audit logu
            self.add_historylog(
                u'Byla odeslána opravená verze PDF souboru, nastavena její '
                u'nová expirace.',
                who=['zk', 'dk'])

            self.send_notifications('wf_st_send_edd_again_r')
            self.send_notifications('wf_st_send_edd_again_zk')

            self.subticket_last.reindexObject()
            self.ticket.reindexObject()

            self.redirect(self.context_url)
            return True

        return False

    def call(self):
        self.unauthorized = True
        if self.is_anon:
            return

        if self.setup_ticket_subticket():
            return
        self.setup_library_zk_dk()

        self.setup_is_role()
        if self.redirect_when_needed():
            return
        self.unauthorized = False

        self.setup_texts()
        self.setup_wf()
        self.setup_pms()

        if self.complaint_cancel():
            return

        if self.is_librarian_dk_op:
            if self.pm_zk_dk():
                return

            # edit forms
            if self.add_info():
                return

            # workflow
            if self.wf_subticket():
                return

            self.setup_subticket_data()
            self.setup_primary_btn()
            self.setup_timeline()

        # unread
        if self.is_librarian_dk and self.subticket.unread_dk:
            safeWrite(self.subticket, self.request)
            self.subticket.unread_dk = False
            self.subticket.reindexObject()

        if VISIT_RESETS_UNREAD_OP and self.is_ziskej_operator and self.subticket.unread_op:
            safeWrite(self.subticket, self.request)
            self.subticket.unread_op = False
            self.subticket.reindexObject()

        return
