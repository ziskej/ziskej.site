# -*- coding: utf-8 -*-
"""BrowserView: Ticket.
"""

from __future__ import print_function

import logging
import json

from DateTime import DateTime
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.protect.utils import addTokenToUrl
from plone.protect.auto import safeWrite
from plone.protect.interfaces import IDisableCSRFProtection
from zope.interface import alsoProvides, Invalid

from ziskej.site.validators import validate_email

from ziskej.site import (
    BUILDOUT_TYPE,
    SHOW_OP_DK_ORDER_OVERVIEW_DETAILS,
    VISIT_RESETS_UNREAD_OP,
    )
from ziskej.site.browser.ticket.ticket_core import (
    BTNCSS_DEFAULT,
    BTNCSS_PRIMARY,
    TicketCoreView,
    )
from ziskej.site.library_payment_helpers import (
    platebator_check,
    library_payment_edd_check,
    )
from ziskej.site.ticket_helpers import (
    create_reportentry,
    wf_action,
    get_cpk_url,
    generate_fullname_manual,
    generate_quote_edda,
    generate_quote_eddb,
    )
from ziskej.site.ticket_load_balancing import load_balancing_dk_list
from ziskej.site.utils import (
    datetime_normalize,
    get_time_ms,
    pprint,
    )


logger = logging.getLogger("ziskej")

DEBUG = False


class TicketView(TicketCoreView):

    template = ViewPageTemplateFile('ticket.pt')
    template_reader = ViewPageTemplateFile('ticket_reader.pt')
    template_availability_ajax = ViewPageTemplateFile('ticket_availability_ajax.pt')
    template_availability_ng = ViewPageTemplateFile('ticket_availability_ng.pt')
    template_ticket_dkorder = ViewPageTemplateFile('ticket_dkorder.pt')

    def setup_init(self):
        # initial values
        self.js_vars = u''
        self.availability_ajax_html = u''
        self.dkorder_html = u''
        self.debug = u''
        self.reader_approval_disabled = True
        self.reader_approval = None
        self.reader_fee_disabled = False
        #editace informací čtenáři
        self.reader_place_editable = False
        self.reader_fee_editable = False
        self.reader_from_date_editable = False
        self.reader_to_date_editable = False
        self.reader_note_editable = False
        # možné přechody
        self.ticket_transitions = []
        self.subticket_transitions = []

        # DK order
        self.show_dk_order_overview = False
        self.show_dk_order_overview_details = False
        self.show_dk_order_form = False
        self.show_lbdebug = False

        # url_ajax_messages_mark_read
        self.url_ajax_messages_mark_read = self.context_url + '/ajax_messages_mark_read'
        self.url_ajax_messages_mark_read = addTokenToUrl(self.url_ajax_messages_mark_read)
        self.js_vars += u"var url_ajax_messages_mark_read='{}';\n".format(self.url_ajax_messages_mark_read)

        # bez parametrů v requestu měníme objekt bezpečně - jen přečteno atp.
        if len(self.request.form.keys()) == 0:
            if DEBUG:
                print('alsoProvides(self.request, IDisableCSRFProtection)')
            alsoProvides(self.request, IDisableCSRFProtection)

        # setup_primary_btn default
        self.btncss_reader_update = BTNCSS_DEFAULT
        self.btncss_load_availability = BTNCSS_DEFAULT
        self.btncss_reader_wf = BTNCSS_DEFAULT
        self.btncss_dkcurrent_wf = BTNCSS_DEFAULT

        self.show_big_cancel = False
        self.show_small_cancel = False
        self.show_big_cancel_answer = False
        self.show_big_cancel_answer_only_decline = False
        self.big_cancel_desc = u''
        self.small_cancel_desc = u''
        self.big_cancel_info = u''
        self.small_cancel_info = u''

        self.show_reader_approval = False
        self.show_reader_date_update = False
        self.show_manual_edit = False
        self.show_load_availability = False
        self.show_manual_lb = False
        self.prepared_info = u''
        self.show_reader_pickup_info = False

    def setup_ticket_subticket_subtickets(self):
        self.is_ticket = True
        self.is_subticket = False
        self.ticket = self.context
        self.subticket = None
        self.subticket_last = None

        subticket_id = self.ticket.subticket_id
        subticket_ids = self.ticket.subticket_ids
        if not subticket_ids:
            subticket_ids = []
        self.subtickets = []
        for item_id in subticket_ids:
            item = getattr(self.ticket, item_id, None)
            if not item:
                logger.warning('Cannot find subticket with id {} for {}'.format(item_id, self.ticket.getId()))
                continue
            if item_id == subticket_id:
                self.subticket = item
                continue
            # ticket.subticket_ids .. LIFO
            # self.subtickets .. FIFO
            # musíme obrátit pořadí, později vytvořené jsou na začátku
            # ticket.subticket_ids, ale self.subtickets je potřeba od starších
            # k novějším
            self.subtickets.insert(0, item)
        # Jsou situace, kdy ticket už nemá aktivní subticket, přesto je potřeba
        # řešit reklamaci posledního subticketu, např. podat námitku.
        if self.subticket:
            self.subticket_last = self.subticket
        elif self.subtickets:
            self.subticket_last = self.subtickets[-1]

        self.doc_url = None
        if self.ticket.doc_id:
            self.doc_url = get_cpk_url(self.ticket.doc_id)

    def manual_caslin_siglas(self):
        siglas = []
        docs_data_json = self.ticket.docs_data
        if not docs_data_json:
            return siglas
        docs_data = json.loads(docs_data_json)
        doc_ids = [x for x in docs_data['doc_ids'] if x.startswith('caslin.')]
        if not len(doc_ids):
            return siglas
        for doc_id in doc_ids:
            item = docs_data['data'].get(doc_id, None)
            if not item:
                continue
            caslin_info = item.get('caslin_info', None)
            if DEBUG:
                print("caslin_info:", repr(caslin_info))
            if not caslin_info:
                continue
            sigla_ids = caslin_info.get('sigla_ids', None)
            if not sigla_ids or not len(sigla_ids):
                continue
            for sigla in sigla_ids:
                if sigla not in siglas:
                    siglas.append(sigla)

        if DEBUG:
            #print(" ")
            #print("--------------------------------")
            print("manual_caslin_siglas")
            print("docs_data:", docs_data)
            print("siglas:", siglas)
            #print("--------------------------------")
            #print(" ")

        return siglas

    def manual_lb(self):
        self.lb_manual_caslin_siglas = self.manual_caslin_siglas()
        lb_manual_caslin_siglas_in_ziskej = []
        caslin_lb_manual_library_lov = []
        self.lb_manual_library_lov = []

        query = dict(
            portal_type = 'Library',
            review_state = 'published',
            sort_on = 'sigla',
            )
        brains = self.catalog(query)
        for brain in brains:
            is_in_caslin_siglas = bool(brain.sigla in self.lb_manual_caslin_siglas)
            if brain.sigla == self.library.sigla:
                continue
            library = brain.getObject()
            if self.ticket.is_mvs and not library.Is_mvs_dk():
                continue
            if self.ticket.is_edd and not library.Is_edd_dk():
                continue
            label = brain.fullname
            if is_in_caslin_siglas:
                label += u' (SK ČR)'
            item = dict(
                sigla = brain.sigla,
                shortname = brain.shortname,
                fullname = label,
                is_in_caslin_siglas = is_in_caslin_siglas,
                disabled = False,
                )
            if is_in_caslin_siglas:
                caslin_lb_manual_library_lov.insert(0, item)  # reverse
                lb_manual_caslin_siglas_in_ziskej.append(brain.sigla)
            else:
                self.lb_manual_library_lov.append(item)

        if len(self.lb_manual_caslin_siglas):
        #if len(caslin_lb_manual_library_lov):
            label = u'Ostatní knihovny aktivní v ZÍSKEJ nevlastnící dle SK ČR požadovaný dokument.'
            item = dict(
                sigla = u'',
                shortname = u'',
                fullname = label,
                is_in_caslin_siglas = False,
                disabled = True,
                )
            self.lb_manual_library_lov.insert(0, item)

        if len(self.lb_manual_caslin_siglas) > len(lb_manual_caslin_siglas_in_ziskej):
            label = u'Sigly knihoven vlastnících požadovaný dokument dle SK ČR, neaktivních v ZÍSKEJ jako dožádaná knihovna'
            item = dict(
                sigla = u'',
                shortname = u'',
                fullname = label,
                is_in_caslin_siglas = False,
                disabled = True,
                )
            self.lb_manual_library_lov.insert(0, item)

            lb_manual_caslin_siglas_not_in_ziskej = [
                x for x in self.lb_manual_caslin_siglas
                if x not in lb_manual_caslin_siglas_in_ziskej]
            BATCH_SIZE = 9
            counter_limit = (len(lb_manual_caslin_siglas_not_in_ziskej) + BATCH_SIZE - 1) / BATCH_SIZE
            counter = 0
            while counter <= counter_limit:
                idx_0 = counter * BATCH_SIZE
                idx_1 = min(idx_0 + BATCH_SIZE, len(lb_manual_caslin_siglas_not_in_ziskej))
                batch = lb_manual_caslin_siglas_not_in_ziskej[idx_0:idx_1]
                label = u', '.join(batch)
                item = dict(
                    sigla = u'',
                    shortname = u'',
                    fullname = label,
                    is_in_caslin_siglas = False,
                    disabled = True,
                    )
                self.lb_manual_library_lov.insert(1 + counter, item)
                counter += 1

        if len(caslin_lb_manual_library_lov):
            item = dict(
                sigla = u'',
                shortname = u'',
                fullname = u'',
                is_in_caslin_siglas = False,
                disabled = True,
                )
            self.lb_manual_library_lov.insert(0, item)

        for item in caslin_lb_manual_library_lov:
            self.lb_manual_library_lov.insert(0, item)  # reverse 2.

        if len(caslin_lb_manual_library_lov):
            label = u'Knihovny aktivní v ZÍSKEJ vlastnící požadovaný dokument dle SK ČR'
            item = dict(
                sigla = u'',
                shortname = u'',
                fullname = label,
                is_in_caslin_siglas = False,
                disabled = True,
                )
            self.lb_manual_library_lov.insert(0, item)

        if self.ticket.is_mvs:
            self.lb_manual_library_sent_lov = [
                dict(id='sent', label=u'Poslat poštou'),
                dict(id='pick', label=u'Vyzvednout osobně'),
                ]
        elif self.ticket.is_edd:
            self.lb_manual_library_sent_lov = [
                dict(id='sent', label=u'Poslat'),
                ]

        if not self.request.get('submit_manual_lb', None):
            return False
        lb_manual_library = self.text_from_request('lb_manual_library')
        if not lb_manual_library and not self.ticket.lb_manual_library:
            self.add_message(u'Není určena žádná manuálně zvolená dožádaná knihovna.')
            return False
        if not lb_manual_library:
            self.ticket.set_lb_state(u'manual library removed')
            self.ticket.lb_manual_library = u''
            self.ticket.lb_manual_library_sent = u''
            self.add_message(u'Byla smazána manuálně zvolená dožádaná knihovna.')
            self.add_historylog(u'Byla smazána manuálně zvolená dožádaná knihovna.', who=['zk'])
            self.redirect()
            return True
        lb_manual_library = lb_manual_library.lower()
        library = getattr(self.portal.libraries, lb_manual_library, None)
        if not library:
            self.add_message(u'Chybně zadaná sigla manuálně zvolené dožádané knihovny.')
            return False
        if not library.is_active:
            self.add_message(u'Manuálně zvolená dožádaná knihovna není v ZÍSKEJ aktivována.')
            return False
        if self.ticket.is_mvs and not library.Is_mvs_dk():
            self.add_message(u'Manuálně zvolená dožádaná knihovna nemá aktivovánu službu MVS jako DK.')
            return False
        if self.ticket.is_edd and not library.Is_edd_dk():
            self.add_message(u'Manuálně zvolená dožádaná knihovna nemá aktivovánu službu EDD jako DK.')
            return False
        if library == self.library:
            self.add_message(u'Nelze zvolit vlastní knihovnu.')
            return False

        lb_manual_library_sent = self.text_from_request('lb_manual_library_sent',
            lov=[x['id'] for x in self.lb_manual_library_sent_lov])
        if not lb_manual_library_sent:
            lb_manual_library_sent = u'sent'

        if self.ticket.lb_manual_library == lb_manual_library:
            if self.ticket.lb_manual_library_sent == lb_manual_library_sent:
                self.add_message(u'Nebyla zadána žádná změna manuálně zvolené dožádané knihovny.')
                return False
            self.ticket.lb_manual_library_sent = lb_manual_library_sent
            self.add_message(u'Byl zvolen způsob doručení pro manuální volbu dožádané knihovny.')
            self.add_historylog(u'Byl zvolen způsob doručení pro manuální volbu dožádané knihovny: {}.'.format(lb_manual_library_sent), who=['zk'])
        else:
            self.ticket.lb_manual_library = lb_manual_library
            self.ticket.lb_manual_library_sent = lb_manual_library_sent
            self.ticket.set_lb_state(u'manual library chosen')

            self.add_message(u'Byla manuálně zvolená dožádaná knihovna.')
            if self.ticket.is_mvs:  # FIXME-MRS
                self.add_historylog(u'Byla manuálně zvolená dožádaná knihovna {}, doručení: {}.'.format(lb_manual_library, lb_manual_library_sent), who=['zk'])
            elif self.ticket.is_edd:
                self.add_historylog(u'Byla manuálně zvolená dožádaná knihovna {}.'.format(lb_manual_library), who=['zk'])

        self.redirect()
        return True

    def redirect_when_needed(self):
        
        if self.is_librarian_zk_op or self.is_reader:
            # ŽK, Operátor a Čtenář mají přímý pohled na Ticket
            pass

        else:
            # ostatní nemají přímý pohled na Ticket, ale mohou mít na některý
            # z jeho Subtickets, takže je zkusme na některý přesměrovat

            # přístup na některý Subticket může mít jen knihovník (operátor má 
            # přístup i na Ticket)

            # je jakýkoli knihovník?
            is_librarian = bool(
                self.ziskej_user_info and
                self.ziskej_user_info['role'] == 'Librarian' and
                self.library and
                self.library.sigla
                )

            if is_librarian:
                # pokud je DK pro jakýkoliv subticket, nejen ten aktivní, tak
                # přesměrovat na poslední takový - může jich být více, ale ty
                # nechť jsou přistupovány přímo, toto je jen mírné zlepšení,
                # ne hlavní cesta

                # může být, že aktivní není žádný (je zneplatněn a zatím nebyl
                # určen nový), ale požadavek má subtickety

                # defaultní subticket
                if self.subticket and (self.subticket.library_dk == self.library.sigla.lower()):
                    self.redirect(self.subticket.absolute_url())
                    return True

                # všechny ostatní subtickety - jsou už seřazeny od posledních
                # k prvním
                for item in self.subtickets:
                    if item.library_dk == self.library.sigla.lower():
                        self.redirect(item.absolute_url())
                        return True

            # ve všech ostatních případech na dashboard, protože neautorizovaný
            # přístup + příslušné info
            if DEBUG:
                print('self.ticket.library_zk')
                print(self.ticket.library_zk)
                print('self.ticket.library_dk')
                print(self.ticket.library_dk)
            self.add_message(u'Neautorizovaný přístup')
            self.redirect(self.portal_url)
            return True

        return False

    def setup_texts(self):
        if self.is_reader:
            self.use_template_reader = True
            self.title = u'{doc_title}, {created_date}'.format(
                doc_title=self.ticket.doc_title, 
                created_date=self.date_format(self.ticket.created_date),
                )
        elif self.is_librarian_zk_op:
            self.title = self.ticket.fullname
        self.reader_lid_label = u'Čtenář ID'
        self.dk_order_info = u''

    def set_platebator_paid(self):
        """Simulovat zaplacení, rollback této simulace, nástroje SC pro MVS."""

        # Připravit default
        self.set_platebator_paid_test_url = None
        self.set_platebator_paid_sc_url = None

        if not self.is_ziskej_operator:
            return False

        # Připravit jsou-li zobrazeny akce
        display_test = False
        is_user_admin = self.is_manager and self.user_id == 'admin'
        # if is_user_admin:
        #     # Neomezovat uživatele admin pro jakoukoli instanci
        #     display_test = True
        # elif BUILDOUT_TYPE in ('development', 'test', ):
        #     # Ostatním umožnit jen podle typu instance
        #     display_test = True

        url = u'{}?set_platebator_paid_sc=sc'.format(self.context_url)
        self.set_platebator_paid_sc_url = addTokenToUrl(url)
        if display_test:
            url = u'{}?set_platebator_paid_test=test'.format(self.context_url)
            self.set_platebator_paid_test_url = addTokenToUrl(url)

        # Zpracovat submit, je-li nějaký
        submit_undo = bool(
            self.request.get('set_platebator_paid_undo', None) == 'undo')
        submit_sc = bool(
            self.request.get('set_platebator_paid_sc', None) == 'sc')
        # submit_test = bool(
        #     self.request.get('set_platebator_paid_test', None) == 'test')

        # Zneplatnit simulované zaplacení, nemá UI, manuálně, jen admin
        if is_user_admin and submit_undo:
            self.ticket.payment_paid_date = None
            self.ticket.add_historylog(u'Bylo zneplatněno předchozí zaplacení.')

        # Simulovat zaplacení, nástroj SC
        elif submit_sc:
            platebator_check(
                self.ticket,
                DateTime(),
                force_paid=True,
                send_notifications=self.send_notifications)
            self.ticket.add_historylog(
                u'Zaplacení objednávky bylo nastaveno Servisním centrem '\
                u'ZÍSKEJ (uživatel {}).'.format(self.user_id))

        # Simulovat zaplacení, testování (odlišný audit log), jen TEST
        # elif display_test and submit_test:
        #     platebator_check(
        #         self.ticket,
        #         DateTime(),
        #         force_paid=True,
        #         send_notifications=self.send_notifications)
        #     self.ticket.add_historylog(
        #         u'Toto zaplacení bylo nastaveno pomocí nástroje pro operátora '\
        #         u'pro testování, nikoli na základě informace z Platebátoru.')

        else:
            # Žádný submit, přesněji žádný validní submit
            return False

        # Standardní redirect po změně
        self.redirect()
        return True

    def set_platebator_paid_edd(self):
        """Kontrola zaplacení EDD čtenářem, simulace zaplacení pro testovací
        účely.
        """

        # Má smysl jen pro EDD
        if not self.ticket.is_edd:
            return False

        # Nemá smysl, pokud není vytvořená platba nebo už bylo zaplaceno
        if not self.ticket.payment_platebator_uuid \
                or self.ticket.payment_paid_date:
            return False

        # Zkontrolovat zaplacení v Platebátoru, pokud dojde k zaplacení,
        # provede se vše potřebné vč. notifikací
        is_changed = library_payment_edd_check(
            self.ticket,
            send_notifications=self.send_notifications,
            request=self.request)
        if is_changed:
            return True

        # Simulace zaplacení - jen na TESTu (a LOCAL)
        if BUILDOUT_TYPE not in ('development', 'test', ):
            return False

        # Simulace zaplacení - nastavit pro UI
        url = u'{}?set_platebator_paid_edd=test'.format(self.context_url)
        self.set_platebator_paid_edd_url = addTokenToUrl(url)
        if self.request.get('set_platebator_paid_edd', None) != 'test':
            return False

        # Simulace zaplacení - vyžádáno
        alsoProvides(self.request, IDisableCSRFProtection)
        # platebator_check(self.ticket, DateTime(), force_paid=True, send_notifications=self.send_notifications)
        date_dt = DateTime()
        self.ticket.reader_pay_by_platebator_edd_dummy(date_dt)
        self.ticket.add_historylog(u'Toto zaplacení bylo nastaveno pomocí nástroje pro testování, nikoli na základě informace z Platebátoru.')

        # create report entries
        report_date_dt = DateTime()
        is_paid_in_advance = self.subticket.ticket.is_paid_in_advance()
        obj_dict = dict(
            report_date = report_date_dt,
            cena = self.subticket.cena,
            user_id = self.user_id,
            entry_type = u'std',
            entry_subtype = u'edd-dk-std',
            is_paid_in_advance = is_paid_in_advance,
            note = u'',
            )
        report_entry = create_reportentry(
            self.subticket, obj_dict)  # is_std = True
        obj_dict = dict(
            report_date = report_date_dt,
            cena = self.ticket.cena - self.subticket.cena,  # Dília poplatek
            user_id = self.user_id,
            entry_type = u'std',
            entry_subtype = u'edd-dilia-std',
            is_paid_in_advance = is_paid_in_advance,
            note = u'',
            )
        report_entry = create_reportentry(
            self.subticket, obj_dict)  # is_std = True

        self.redirect()
        return True

    def pm_r_zk(self):
        # used only for is_librarian_zk
        self.pm_r_zk_unread = 0
        self.pm_r_zk_total = 0
        self.pm_r_zk_envelope_css = u''

        # form PM r/zk

        # pokud ticket nemá čtenáře, nemají zprávy čtenáři nebo od něj smysl
        if not self.ticket.is_reader:
            self.is_pm_r_zk = False
            return False
        self.is_pm_r_zk = True

        # většina má smysl jen pro R / ŽK (ani pro operátora ne)
        if not self.is_reader and not self.is_librarian_zk:
            if self.is_ziskej_operator:
                # minimum pro operátora
                self.get_messages_r_zk = self.ticket.get_messages()
                self.pm_r_zk_total = len(self.get_messages_r_zk)
                if self.ticket.pm_unread == 'library_zk':
                    self.pm_r_zk_unread = 1
                    self.pm_r_zk_envelope_css = u'fa fa-envelope ziskej-orange-std'
                else:
                    self.pm_r_zk_unread = 0
                    if self.pm_r_zk_total:
                        self.pm_r_zk_envelope_css = u'fa fa-envelope-open'
                    else:
                        self.pm_r_zk_envelope_css = u'fa fa-envelope-open-o'
            return False

        # form - submit
        add_message = None
        if self.request.get('submit_pm_r_zk', None):
            add_message = self.text_from_request('add_message_r_zk')
            if add_message:
                add_message = add_message.strip()
        if add_message:
            if self.is_librarian_zk:
                #message_sender = u'{title}'.format(title=ticket.library_zk_title)
                message_sender = u'library_zk'
            elif self.is_reader:
                #message_sender = u'Čtenář'
                message_sender = u'reader'
            is_updated = self.ticket.add_message_txt(message_sender, add_message.strip())
            if is_updated:
                if self.is_librarian_zk:
                    self.send_notifications('pm_r_zk_librarian')
                    # diskutovat
                    #self.add_historylog(u'Byla poslána zpráva knihovně: {}.'.format(add_message))
                elif self.is_reader:
                    self.send_notifications('pm_r_zk_reader')
                    # diskutovat
                    #self.add_historylog(u'Byla poslána zpráva čtenáři: {}.'.format(add_message))
                message = u'Zpráva byla odeslána.'
                self.add_message(message)
                # unread
                if self.is_reader:
                    self.ticket.unread_zk = True
                    self.ticket.reindexObject()
                    if DEBUG:
                        print("unread_zk")
                elif self.is_librarian_zk:
                    self.mark_librarian()
                    self.ticket.unread_r = True
                    self.ticket.reindexObject()
                    if DEBUG:
                        print("unread_r")
                self.ticket.reindexObject()
                self.redirect(self.context_url)
                return True
            else:
                message = u'Nebyla zadána žádná zpráva.'
                self.add_message(message)

        # new Ticket UI for librarian ZK
        if self.is_librarian_zk:
            self.get_messages_r_zk = self.ticket.get_messages()
            self.pm_r_zk_total = len(self.get_messages_r_zk)
            if self.ticket.pm_unread == 'library_zk':
                self.pm_r_zk_unread = 1
                self.pm_r_zk_envelope_css = u'fa fa-envelope ziskej-orange-std'
            else:
                self.pm_r_zk_unread = 0
                if self.pm_r_zk_total:
                    self.pm_r_zk_envelope_css = u'fa fa-envelope-open'
                else:
                    self.pm_r_zk_envelope_css = u'fa fa-envelope-open-o'
            if DEBUG:
                print('self.pm_r_zk_unread: {}'.format(self.pm_r_zk_unread))
                print('self.pm_r_zk_total: {}'.format(self.pm_r_zk_total))

        # new Ticket UI for for reader
        elif self.is_reader:
            self.get_messages_r_zk = self.ticket.get_messages()
            self.pm_r_zk_total = len(self.get_messages_r_zk)
            if self.ticket.pm_unread == 'reader':
                self.pm_r_zk_unread = 1
                self.pm_r_zk_envelope_css = u'fa fa-envelope ziskej-orange-std'
            else:
                self.pm_r_zk_unread = 0
                if self.pm_r_zk_total:
                    self.pm_r_zk_envelope_css = u'fa fa-envelope-open'
                else:
                    self.pm_r_zk_envelope_css = u'fa fa-envelope-open-o'
            if DEBUG:
                print('self.pm_r_zk_unread: {}'.format(self.pm_r_zk_unread))
                print('self.pm_r_zk_total: {}'.format(self.pm_r_zk_total))

        return False

    def add_info(self):
        # form Doplnit informace
        # form - submit
        is_edd = self.ticket.is_edd
        if self.request.get('submit_add_info', None):
            reader_lid = self.text_from_request('reader_lid')
            library_zk_note = self.text_from_request('library_zk_note')
            reader_firstname = self.text_from_request('reader_firstname')
            reader_lastname = self.text_from_request('reader_lastname')

            is_error = False
            if is_edd: 
                reader_email = self.text_from_request('reader_email')
                if reader_email is not None and reader_email.strip() != u'':
                    try:
                        validate_email(reader_email)
                    except Invalid, e:
                        is_error = True
                        self.add_message(u"Neplatný formát e-mailu, e-mail nebyl změněný.",
                                         message_type='error')
                        reader_email = self.ticket.reader_email

            if self.ticket.wf_review_state == "created":
                reader_approval_lov = (u'', u'ok', u'stop', u'reject', )
                reader_approval = self.text_from_request('reader_approval', lov=reader_approval_lov)
                if is_edd: 
                    if reader_approval == u'ok' and reader_email == u'':
                        is_error = True
                        self.add_message(u"Nelze ověřit čtenáře bez e-mailu!",
                                         message_type='error')
                        reader_approval = self.ticket.reader_approval

                is_updated = False
                if self.ticket.is_created_by_cpk_for_reader():
                        if self.ticket.is_paid_in_advance() and \
                                not self.ticket.accepted_date:
                            self.ticket.accepted_date = self.dt_as(
                                DateTime(), result_type='iso')
                            self.ticket.add_historylog(u'Stav pro čtenáře byl změněn na accepted.')
                            self.send_notifications('wf_t_zk_accepted_reader')
                            is_updated = True
                        else:
                            if self.ticket.accepted_date:
                                self.ticket.accepted_date = None
                                self.ticket.add_historylog(u'Stav pro čtenáře byl změněn na created / paid z accepted.')
                                is_updated = True
                if is_edd:
                    is_updated = self.ticket.update_attrs_if_changed(
                        reader_lid = reader_lid,
                        library_zk_note = library_zk_note,
                        reader_email = reader_email,
                        reader_approval = reader_approval,
                        reader_firstname = reader_firstname,
                        reader_lastname = reader_lastname,
                        ) or is_updated
                else:
                    is_updated = self.ticket.update_attrs_if_changed(
                        reader_lid = reader_lid,
                        library_zk_note = library_zk_note,
                        reader_approval = reader_approval,
                        reader_firstname = reader_firstname,
                        reader_lastname = reader_lastname,
                        ) or is_updated
            else:
                is_updated = self.ticket.update_attrs_if_changed(
                    reader_lid = reader_lid,
                    library_zk_note = library_zk_note,
                    reader_firstname = reader_firstname,
                    reader_lastname = reader_lastname,)
            if is_updated:
                message = u'Informace o čtenáři byly aktualizovány.'
                self.add_message(message)
                self.mark_librarian()
                if self.ticket.reader_approval == u'ok' and self.ticket.reader:
                    self.library.set_reader_lid(self.ticket.reader, reader_lid)

                # Refuse ticket if reader is rejected
                if self.ticket.reader_approval == u'reject':
                    self.ticket.reject_reason = u'reject-reason-r-denied'
                    wf_action(self.ticket, 'reject', u'')
                    message = u'Uzavřeno knihovníkem jako nevyříditelné z důvodu neověření čtenáře.'
                    self.add_message(message)
                    # unread
                    self.ticket.unread_r = True
                    if DEBUG:
                        print("unread_r")
                    self.send_notifications('wf_t_reject_reader')
                else:
                    # unread beze změny
                    if DEBUG:
                        print("unread_x")
                self.add_historylog(u'Byl upraven čtenář, stav ověření: <strong>{}</strong> '.format(self.ticket.reader_approval_label), who=['zk'])
                self.ticket.reindexObject()
                self.redirect(self.context_url)
                return True
            else:
                if not is_error:
                    message = u'Nebyla zadána žádná změna.'
                    self.add_message(message, message_type='error')

        # form - příprava
        reader_lid_label = self.library.reader_lid_label
        if reader_lid_label:
            self.reader_lid_label = reader_lid_label
        self.reader_approval = self.ticket.reader_approval
        
        reader_lid_help = self.library.reader_lid_help
        if reader_lid_help:
            self.reader_lid_help = reader_lid_help
        return False

    def manual_edit(self):
        if self.request.get('submit_manual_edit', None):
            if self.ticket.ticket_type == 'mvs':
                return self.manual_edit_mvs()
            elif self.ticket.ticket_type == 'edd':
                return self.manual_edit_edd()
            else:
                logger.info('Inavlid ticket_type')
                return False


    def manual_edit_edd(self):
        obj_dict = dict()
        edd_data = dict()

        is_manual = self.ticket.doc_manual
        is_article = self.ticket.is_edd_article

        for k in ('doc_author', 'doc_title', ):
            obj_dict[k] = self.text_from_request(k)
        for k in ('pages_from', 'pages_to', 'doc_number_year',
                  'doc_number_pyear', 'doc_number_pnumber', ):
            edd_data[k] = self.text_from_request(k)
        if not is_article:
            edd_data['doc_volume'] = self.text_from_request('doc_volume')
        if is_manual:
            obj_dict['doc_issuer'] = self.text_from_request('doc_issuer')
            edd_data['doc_title_in'] = self.text_from_request('doc_title_in')
            if is_article:
                obj_dict['doc_issn'] = self.text_from_request('doc_issn')
            else:
                obj_dict['doc_isbn'] = self.text_from_request('doc_isbn')

        # Validace stránek.
        pages_from = None
        pages_to = None
        if edd_data['pages_from']:
            try:
                pages_from = int(edd_data['pages_from'])
                pages_to = int(edd_data['pages_to'])
            except:
                message = u'Rozsah stran musí být čísla.'
                self.add_message(message, message_type='error')
                return False

        if pages_from is None and pages_to is None:
            pass
        elif pages_from is None and pages_to is not None:
            message = u'Nelze zadat jen poslední stránku.'
            self.add_message(message, message_type='error')
            return False
        elif pages_from < 1:
            message = u'Nelze zadat první stranu menší jak 1'
            self.add_message(message, message_type='error')
            return False
        elif pages_from is not None and pages_to is not None:
            if pages_from == pages_to:
                # Zjednodušit bez informování uživatele.
                pages_to = None
            elif pages_from > pages_to:
                message = u'První stránka nemůže následovat za poslední.'
                self.add_message(message, message_type='error')
                return False
        # edd = self.ticket.Edd_data()
        edd_data['pages_from'] = pages_from
        edd_data['pages_to'] = pages_to

        is_updated_obj_dict = self.ticket.update_attrs_if_changed(**obj_dict)
        is_updated_edd_data = self.ticket.update_edd_data_if_changed(**edd_data)

        if is_updated_obj_dict or is_updated_edd_data:
            # přegenerovat citaci
            fullname_old = self.ticket.doc_fullname
            obj_dict_full = dict(obj_dict)
            if not is_manual:
                obj_dict_full['doc_issuer'] = self.ticket.doc_issuer
                obj_dict_full['doc_isbn'] = self.ticket.doc_isbn
                obj_dict_full['doc_issn'] = self.ticket.doc_issn
            edd_data_full = self.ticket.Edd_data()
            if is_article:
                fullname = generate_quote_edda(obj_dict_full, edd_data_full)
            else:
                fullname = generate_quote_eddb(obj_dict_full, edd_data_full)
            self.ticket.doc_fullname = fullname
            # FIXME-EDD prověřit

            message = u'Citace a její metadata byla aktualizována.'
            self.add_message(message)
            self.mark_librarian()

            self.add_historylog(
                u'Byla upravena citace, stará hodnota {fullname_old}, nová hodnota: {fullname}'.format(
                    fullname = fullname,
                    fullname_old = fullname_old,
                    ),
                who=['zk'])

            self.ticket.reindexObject()
            self.redirect(self.context_url)
            return True
        else:
            message = u'Nebyla zadána žádná změna.'
            self.add_message(message, message_type='error')
        return False

    def manual_edit_mvs(self):
        obj_dict = dict()
        for k in ('doc_title', 'doc_number', 'doc_author', 'doc_issuer',
                'doc_isbn', 'doc_issn', 'doc_signature', ):
            obj_dict[k] = self.text_from_request(k)
        is_updated = self.ticket.update_attrs_if_changed(**obj_dict)
        if is_updated:
            # přegenerovat citaci
            fullname_old = self.ticket.doc_fullname
            fullname = generate_fullname_manual(obj_dict)
            self.ticket.doc_fullname = fullname

            message = u'Citace a její metadata byla aktualizována.'
            self.add_message(message)
            self.mark_librarian()

            self.add_historylog(
                u'Byla upravena citace, stará hodnota {fullname_old}, nová hodnota: {fullname}'.format(
                    fullname = fullname,
                    fullname_old = fullname_old,
                    ),
                who=['zk'])

            self.ticket.reindexObject()
            self.redirect(self.context_url)
            return True
        else:
            message = u'Nebyla zadána žádná změna.'
            self.add_message(message)

        # form - příprava
        # není potřeba

        return False

    def setup_tab_st(self, subticket, is_current=None):
        # id, library
        subticket_id = subticket.getId()
        library_dk_sigla = subticket.library_dk
        library_dk = getattr(self.portal.libraries, library_dk_sigla, None)
        if library_dk is None:
            raise Exception('Internal error')

        # librarian
        librarian_dk_html = None
        if subticket.librarian:
            librarian_dk_html = u'<a href="{library_url}/{librarian}" target="_blank" title="Za dožádanou knihovnu naposledy změnil knihovník, zobrazit profil knihovníka v novém okně.">{librarian}</a>'.format(
                library_url=library_dk.absolute_url(),
                librarian=subticket.librarian,
                )

        # messages
        messages = subticket.get_messages()
        tab_messages_total = len(messages)
        if subticket.pm_unread == 'library_zk':
            tab_messages_unread = 1
        else:
            tab_messages_unread = 0
        envelope_css = u'fa fa-envelope-open'
        if tab_messages_unread:
            envelope_css = u'fa fa-envelope ziskej-orange-std'
        elif not tab_messages_total:
            envelope_css = u'fa fa-envelope-open-o'
        modal_id = u'modal-dk-pm-{}'.format(subticket_id)

        # attrs
        # wf_modal_href
        if is_current:
            display_wf_dk_subticket = True
            wf_modal_href = None,
        else:
            display_wf_dk_subticket = bool(
                subticket.wf_review_state == 'sent' and subticket.can_send_back
                )
            wf_modal_href = u'#modal-dkhistory-wf-{}'.format(subticket_id)

        # modal dk pm data
        dkpm = dict(
            subticket_id = subticket_id,
            modal_id = modal_id,
            library_sigla = library_dk_sigla.upper(),
            messages = messages,
            )
        self.dkpm_list.append(dkpm)

        # tab data
        tab = dict(
            subticket_id = subticket_id,
            tab_id = u'tablib-{}'.format(subticket_id),
            href = u'#tablib-{}'.format(subticket_id),
            library_sigla = library_dk_sigla.upper(),
            library_fullname = library_dk.fullname,
            library_href = library_dk.absolute_url(),
            library_address = library_dk.Address(),
            librarian_html = librarian_dk_html,
            doc_sent = subticket.doc_sent,
            pm_unread = tab_messages_unread,
            pm_total = tab_messages_total,
            display_messages = bool(tab_messages_total),
            envelope_css = envelope_css,
            dkpm_modal_id = u'#{}'.format(modal_id),
            fee = subticket.cena,
            display_wf_dk_subticket = display_wf_dk_subticket,
            wf_modal_href = wf_modal_href,
            wf_review_state = subticket.wf_review_state,
            wf_review_state_label = subticket.wf_review_state_label,
            refuse_reason = subticket.refuse_reason,
            complaint_label = subticket.complaint_label,
            objection_text = subticket.objection_text,
            objection_decision_text = subticket.objection_decision_text,
            transitions = [],
            reports_html = subticket.reports_html(),
            is_last_active = False,
            )
        return tab

    def setup_tabs_st(self):
        self.show_tabs_st = False
        self.tabs_st_history = []
        self.tabs_st_current = None
        self.dk_future_siglas = []  # see end of dk_order
        self.show_tabs_st_tab_edit = False
        self.dkpm_list = []

        if len(self.subtickets):
            self.show_tabs_st = True

            # self.subtickets obsahuje už přímo historické subtickety - je to
            # založeno na ticket.subticket_ids s vynecháním toho aktuálního a
            # dotažení celého objektu an základě subtickt id
            for subticket in self.subtickets:
                tab = self.setup_tab_st(subticket, is_current = False)
                self.tabs_st_history.append(tab)
            # pokud ticket nemá žádný aktuální (main) subticket, označme
            # poslední takový mezi historickými - je potřeba pro podání námitky
            # k odmítnutí malé reklamace v rámci velké reklamace
            if not self.subticket:
                self.tabs_st_history[-1]['is_last_active'] = True

        if self.subticket:
            self.show_tabs_st = True
            self.tabs_st_current = self.setup_tab_st(self.subticket, is_current = True)

        if self.ticket.lb_manual_library:
            if self.ticket.is_mvs:
                LOV_LABELS = dict(
                    sent = u'poslat poštou',
                    pick = u'vyzvednout osobně',
                    )
            elif self.ticket.is_edd:
                LOV_LABELS = dict(
                    sent = u'',
                    )
            dk_sent = self.ticket.lb_manual_library_sent
            if dk_sent:
                dk_sent = LOV_LABELS.get(dk_sent, dk_sent)
            if dk_sent:
                dk_sent = u' ({})'.format(dk_sent)
            dk_future_siglas_item = u'{}{}'.format(
                self.ticket.lb_manual_library.upper(),
                dk_sent)
            self.dk_future_siglas.append(dk_future_siglas_item)

        if DEBUG:
            print('setup_tabs_st')
            pprint(self.tabs_st_history)
            pprint(self.tabs_st_current)
            pprint(self.dk_future_siglas)
            print(' ')

    # def debug_add(self, obj, key, value=None):
    #     DEBUG_LOCAL = False or bool(self.request.get('debug', None))
    #     if not DEBUG_LOCAL:
    #         return
    #     if value is None:
    #         if obj == 'self':
    #             value = getattr(self, key, None)
    #         elif obj == 'ticket':
    #             value = getattr(self.ticket, key, None)
    #         elif obj == 'subticket':
    #             value = getattr(self.subticket, key, None)
    #         elif obj == 'subtickets':
    #             value = []
    #             for subticket in self.subtickets:
    #                 v = getattr(subticket, key, None)
    #                 try:
    #                     v = v()
    #                 except TypeError:
    #                     pass
    #                 if v is None:
    #                     v = u'(None)'
    #                 value.append(v)
    #             value = u', '.join(value)
    #     try:
    #         value = value()
    #     except TypeError:
    #         pass
    #     self.debug += u'<li><strong>{obj}.{k}:</strong> {v}</li>\n'.format(obj=obj, k=key, v=value)

    def dk_order(self):
        """Příprava a zpracování formuláře pro pořadí DK.

        ticket.doc_order .. schválené pořadí DK
        ticket.doc_order_suggested .. navržené pořadí DK
        self.dk_list .. seznam všech možných DK, ze kterých může knihovník 
                        vybírat
        """

        if self.ticket.wf_review_state not in ('created', 'pending', ):
            return False

        if self.ticket.doc_manual:
            return False

        if self.is_ziskej_operator and not self.is_sudo:
            if not SHOW_OP_DK_ORDER_OVERVIEW_DETAILS:
                return False
            if not self.ticket.doc_order_suggested_data:
                print("missing doc_order_suggested_data")
                return False
            try:
                self.dk_list_full = json.loads(self.ticket.doc_order_suggested_data)
                self.show_dk_order_overview_details = bool(self.dk_list_full)
            except:
                logger.info('OPERATOR Invalid doc_order_suggested_data for ticket {}'.format(self.ticket.hid))
            return False

        self.show_dk_order_overview = self.ticket.is_lb_auto_and_can_order
        self.show_dk_order_overview_details = bool(
            self.show_dk_order_overview or
            self.ticket.lb_state == u'auto: load availability finished failed')
        self.show_dk_order_load_availability = self.show_dk_order_overview

        # self.debug_add('self', 'show_dk_order_overview')
        # self.debug_add('self', 'ticket_review_state')
        # self.debug_add('ticket', 'doc_order')
        # self.debug_add('ticket', 'doc_order_sent')
        # self.debug_add('ticket', 'doc_order_suggested')
        # self.debug_add('subticket', 'subticket', '')
        # self.debug_add('subticket', 'library_dk')
        # self.debug_add('subticket', 'wf_review_state')
        # self.debug_add('subtickets', 'subtickets', '(history)')
        # self.debug_add('subtickets', 'library_dk')
        # self.debug_add('subtickets', 'wf_review_state')

        # Ticket AJAX settings
        self.ajax_load_availability_vars = u''
        ajax_load_availability_vars = dict(
            url_ticket = self.context_url,
            )
        for k in ajax_load_availability_vars:
            # string variables
            self.ajax_load_availability_vars += u'var {k} = "{v}";\n'.format(k=k, v=ajax_load_availability_vars[k])

        # Angular availability integration
        urlAsyncInit = self.context_url + '/ajax_load_availability_async_init'
        urlAsyncGet = self.context_url + '/ajax_load_availability_async_get'
        urlAsyncFinish = self.context_url + '/ajax_load_availability_async_finish'
        urlAsyncInit = addTokenToUrl(urlAsyncInit)
        urlAsyncGet = addTokenToUrl(urlAsyncGet)
        urlAsyncFinish = addTokenToUrl(urlAsyncFinish)
        timeout_default = 5000
        self.ng_init = u'''
            var urlAsyncInit = '{urlAsyncInit}';
            var urlAsyncGet = '{urlAsyncGet}';
            var urlAsyncFinish = '{urlAsyncFinish}';
            var timeout_default = {timeout_default};
            '''.format(
                urlAsyncInit = urlAsyncInit,
                urlAsyncGet = urlAsyncGet,
                urlAsyncFinish = urlAsyncFinish,
                timeout_default = timeout_default,
                )
        if DEBUG:
            print("self.ng_init")
            print(self.ng_init)
            print(" ")
        #self.availability_ajax_html = self.template_availability_ajax()
        self.availability_ajax_html = self.template_availability_ng()

        # form Potvrdit pořadí
        # form - příprava 1/2

        # Podmínka, zda zobrazit pořadí DK: 1) stav 2) už bylo schváleno pořadí
        # nebo alespoň load balancing alg už navrhl
        self.show_dk_order_form = self.show_dk_order_overview
        if not self.show_dk_order_form and not self.ticket.doc_order:
            self.dk_list_full = []
            if self.ticket.doc_order_suggested_data:
                try:
                    self.dk_list_full = json.loads(self.ticket.doc_order_suggested_data)
                except:
                    logger.warning('Invalid doc_order_suggested_data for ticket {}'.format(self.ticket.hid))
            return False

        self.dk1 = None
        self.dk2 = None
        self.dk3 = None

        if self.ticket.doc_order_suggested:
            restrict_doc_ids = self.ticket.doc_order_suggested.split(',')
        elif self.ticket.doc_order:
            restrict_doc_ids = self.ticket.doc_order.split(',')
        else:
            restrict_doc_ids = None
        if DEBUG:
            print('restrict_doc_ids: {}'.format(restrict_doc_ids))
        if restrict_doc_ids:
            # Používá Auto LB pouze pro parsování předchozích výsledků uložených
            # v ticket.  Žádné nové volání CPK.
            self.dk_list = load_balancing_dk_list(self.portal, self.ticket, restrict_doc_ids=restrict_doc_ids)
        else:
            self.dk_list = []
        self.dk_list_plus = [x for x in self.dk_list]
        empty_option = dict(
            id = u'',
            label = u'Nezvoleno',
            )
        self.dk_list_plus.insert(0, empty_option)
        dk_list_ids = [x['id'] for x in self.dk_list]
        try:
            self.dk_list_full = json.loads(self.ticket.doc_order_suggested_data)
        except:
            logger.warning('Invalid doc_order_suggested_data for ticket {}'.format(self.ticket.hid))
            self.dk_list_full = []
        if DEBUG:
            print('self.ticket.doc_order_suggested_data')
            print(self.ticket.doc_order_suggested_data)
            print(' ')
            print('self.dk_list_full')
            print(self.dk_list_full)
            print(' ')

        if self.ticket.is_mvs:
            self.dk1_sent_lov = [
                dict(id='sent', label=u'Poslat poštou'),
                dict(id='pick', label=u'Vyzvednout osobně'),
                ]
            self.dk2_sent_lov = [
                dict(id='sent', label=u'Poslat poštou'),
                dict(id='pick', label=u'Vyzvednout osobně'),
                ]
            self.dk3_sent_lov = [
                dict(id='sent', label=u'Poslat poštou'),
                dict(id='pick', label=u'Vyzvednout osobně'),
                ]
        elif self.ticket.is_edd:
            self.dk1_sent_lov = [
                dict(id='sent', label=u'Poslat'),
                ]
            self.dk2_sent_lov = [
                dict(id='sent', label=u'Poslat'),
                ]
            self.dk3_sent_lov = [
                dict(id='sent', label=u'Poslat'),
                ]

        if DEBUG:
            print('dk_list_ids: {}'.format(dk_list_ids))
            print(u'self.ticket.doc_order: {}'.format(self.ticket.doc_order))
            print(u'self.ticket.doc_order_suggested: {}'.format(self.ticket.doc_order_suggested))

        # form - submit
        if self.request.get('submit_dk', None):
            dk1 = None
            dk2 = None
            dk3 = None

            blacklist = self.ticket.doc_order_blacklist
            if not blacklist:
                blacklist = []
            blacklist = [x for x in blacklist]

            dk_str_list = []
            dk_sent_str_list = []

            dk1 = self.text_from_request('dk1')
            if not dk1:
                dk1 = None
            elif dk1 not in dk_list_ids or dk1 in blacklist:
                dk1 = None
            if dk1:
                dk_str_list.append(dk1)
                blacklist.append(dk1)

                dk1_sent = self.text_from_request('dk1_sent',
                    lov=[x['id'] for x in self.dk1_sent_lov])
                if not dk1_sent:
                    dk1_sent = u'sent'
                dk_sent_str_list.append(dk1_sent)

            dk2 = self.text_from_request('dk2')
            if not dk2:
                dk2 = None
            elif dk2 not in dk_list_ids or dk2 in blacklist:
                dk2 = None
            if dk2:
                dk_str_list.append(dk2)
                blacklist.append(dk2)

                dk2_sent = self.text_from_request('dk2_sent',
                    lov=[x['id'] for x in self.dk2_sent_lov])
                if not dk2_sent:
                    dk2_sent = u'sent'
                dk_sent_str_list.append(dk2_sent)

            dk3 = self.text_from_request('dk3')
            if not dk3:
                dk3 = None
            elif dk3 not in dk_list_ids or dk3 in blacklist:
                dk3 = None
            if dk3:
                dk_str_list.append(dk3)
                blacklist.append(dk3)

                dk3_sent = self.text_from_request('dk3_sent',
                    lov=[x['id'] for x in self.dk3_sent_lov])
                if not dk3_sent:
                    dk3_sent = u'sent'
                dk_sent_str_list.append(dk3_sent)

            dk_str = u','.join(dk_str_list)
            dk_sent_str = u','.join(dk_sent_str_list)

            # Pokud je nějaké pořadí schválené a změnilo se pořadí, způsob
            # doručení nebo Auto LB stav nebyl schválené pořadí
            if dk_str and (dk_str != self.ticket.doc_order or \
                    dk_sent_str != self.ticket.doc_order_sent or \
                    self.ticket.lb_state != u'auto: load availability order accepted'):

                # Nastavit stav Auto LB
                self.ticket.set_lb_state(u'load availability order accepted')
                # Uložit pořadí
                self.ticket.doc_order = dk_str
                self.ticket.doc_order_suggested = u''
                # Uložit způsob doručení
                if dk_sent_str != self.ticket.doc_order_sent:
                    self.ticket.doc_order_sent = dk_sent_str
                # unread beze změny
                if DEBUG:
                    print("unread_x")
                self.mark_librarian()
                self.add_historylog(
                    u'Automaticky zvolené knihovny byly uloženy v pořadí: {}, s upřesněním způsobu dodání: {}'.format(
                        u', '.join(dk_str_list),
                        u', '.join(dk_sent_str_list)),
                    who=['zk'])
                self.ticket.reindexObject()
                self.redirect(self.context_url)
                return True

        # form - příprava 2/2
        if self.ticket.doc_order:
            already_assigned = True
            doc_order = self.ticket.doc_order.split(',')
            doc_order_sent = self.ticket.doc_order_sent.split(',')
        elif self.ticket.doc_order_suggested:
            already_assigned = False
            doc_order = self.ticket.doc_order_suggested.split(',')
            doc_order_sent = []
        elif self.ticket.lb_state == u'auto: load availability finished failed':
            doc_order = []
            doc_order_sent = []
        else:
            raise Exception('Internal error')

        for doc_id in doc_order:
            if doc_id not in dk_list_ids:
                logger.warning('DK order is not valid anymore, trying suggest new one.')
                logger.info('doc_id {doc_id} not in dk_list_ids {dk_list_ids}'.format(doc_id=doc_id, dk_list_ids=dk_list_ids))
                if already_assigned:
                    message = u'Schválené pořadí dožádaných knihoven přestalo být validní, bylo navrženo nové pořadí.'
                else:
                    message = u'Schválené pořadí dožádaných knihoven přestalo být validní, bylo navrženo nové pořadí.'
                self.add_message(message)
                
                # buď jde o suggested a v tom případě je ticket.doc_order
                # prázdný, nebo ho potřebujeme udělat prázdným, protože přestal
                # být validní.
                self.ticket.doc_order = u''

                # nový návrh pořadí
                self.ticket.doc_order_suggested = u''
                #self.call_load_balancing()  # OBSOLETE

                self.redirect(self.context_url)
                return True

        self.dk1_sent = u''
        if len(doc_order) > 0:
            self.dk1 = doc_order[0]
            if len(doc_order_sent) >= 1:
                self.dk1_sent = doc_order_sent[0]
        else:
            self.dk1 = u''

        self.dk2_sent = u''
        if len(doc_order) > 1:
            self.dk2 = doc_order[1]
            if len(doc_order_sent) >= 2:
                self.dk2_sent = doc_order_sent[1]
        else:
            self.dk2 = u''

        self.dk3_sent = u''
        if len(doc_order) > 2:
            self.dk3 = doc_order[2]
            if len(doc_order_sent) >= 3:
                self.dk3_sent = doc_order_sent[2]
        else:
            self.dk3 = u''

        # only when already accepted by ZK
        is_add_to_dk_future_siglas = bool(self.ticket.lb_state in (
            u'auto: load availability order accepted',
            u'auto: current',
            u'auto: last closed'
            ))

        if is_add_to_dk_future_siglas:
            if self.ticket.is_mvs:
                LOV_LABELS = dict(
                    sent = u'poslat poštou',
                    pick = u'vyzvednout osobně',
                    )
            if self.ticket.is_edd:
                LOV_LABELS = dict(
                    sent = u'',
                    )

        counter = -1
        for dk in doc_order:
            counter += 1
            if self.subticket and self.subticket.doc_id == dk and self.ticket.wf_review_state != 'pending':
                continue
            if DEBUG:
                print('dk: ')
                print(dk)
            sigla = dk[:6]
            for dk_item in self.dk_list:
                if dk_item['id'] == dk:
                    sigla = dk_item['sigla']
                    break
            if is_add_to_dk_future_siglas:
                dk_sent = u''
                if counter < len(doc_order_sent):
                    dk_sent = doc_order_sent[counter]
                if dk_sent:
                    dk_sent = LOV_LABELS.get(dk_sent, dk_sent)
                    if dk_sent:
                        dk_sent = u' ({})'.format(dk_sent)
                dk_future_siglas_item = u'{}{}'.format(sigla.upper(),
                    dk_sent)
                self.dk_future_siglas.append(dk_future_siglas_item)

        self.dkorder_html = self.template_ticket_dkorder()

        return False

    def reader_dates(self):
        """form Nastavit termíny v časové ose pro čtenáře

        show_reader_date_update je nastaveno až později a to takto:

            - assigned / sent .. True
            - prepared + not lent .. True
            - prepared + not return .. True
            - else .. False

        poznámka: not lent ==> not return, proto stačí test na return

        Jiná otázka je, co vše se v tomto modálním okně dá editovat a které
        ikonky editace jsou u daných hodnot zobrazeny, tam se jednotlivé
        varianty liší, přestože všechny mají show_reader_date_update True.

        SUGGESTION Zvážit volat setup_primary_btn dříve, pak by nebylo nutné
        tady nastavovat a dalo by se použít show_reader_date_update.
        """

        # 'created', 'assigned', 'pending', 'prepared', 'closed', 'cancelled', 'rejected'
        twf = self.ticket.wf_review_state

        if self.subticket:
            # 'queued', 'conditionally_accepted', 'accepted', 'sent', 'sent_back', 'closed', 'cancelled', 'refused'
            stwf = self.subticket.wf_review_state
        else:
            stwf = None

        if twf == 'assigned' and stwf == 'sent':
            self.reader_from_date_editable = True
            self.reader_to_date_editable = True
            self.reader_place_editable = True
            self.reader_note_editable = True
            self.reader_back_date_editable_min = self.today_datapicker()

            # Jen objednávky vytvořené knihovníkem a pro ŽK, která umožňuje
            # některým čtenářům poskytnout zdarma, je možné editovat částku pro
            # čtenáře
            if (not self.ticket.created_by or \
                    self.ticket.created_by == 'librarian') \
                    and self.library.mvs_zk_cena == 0:
                self.reader_fee_editable = True

        elif twf == 'prepared' and not self.ticket.is_reader_return:
            self.reader_to_date_editable = True
            self.reader_note_editable = True

            self.reader_back_date_editable_min = self.dt_as(
                DateTime(self.ticket.reader_to_date),
                result_type='datepicker')

        # Pokud už bylo dříve vyplněno a správně tak není nutné změnit, jinak
        # ale najít správný default a ten uložit
        if self.ticket.reader_fee not in (u'0', u'70', ):

            # Pokud je vytvořeno čtenářem pomocí CPK API a pokud už bylo
            # zaplaceno, pak není co platit
            if self.ticket.created_by == "cpk_reader" and \
                    self.ticket.is_paid_in_advance:
                self.ticket.reader_fee = u'0'

            # Jinak standardně 70 Kč, protože i kdyby ŽK měla nastaveno fixní
            # cena 0 Kč, znamená to, že může (nemusí) poskytnout zdarma, ale
            # i tak předvyplnit 70 Kč
            else:
                self.ticket.reader_fee = u'70'

            self.ticket.reindexObject()

        # Nyní už vždy má ticket nastavenou cenu a proto můžeme použít jako
        # default pro případný formulář, je-li editovatelné
        self.reader_fee = self.ticket.reader_fee

        if DEBUG:
            print("self.ticket.reader_fee:", repr(self.ticket.reader_fee))
            print("self.reader_fee:", repr(self.reader_fee))

        if self.ticket.reader_place:
            self.reader_place = self.ticket.reader_place
        else:
            self.reader_place = self.library.address

        if self.request.get('submit_reader_dates', None):
            obj_dict = dict()
            reader_note_as_pm = False
            if twf == 'assigned' and stwf == 'sent':
                obj_dict['reader_from_date'] = self.date_from_request(
                    'reader_from_date', result_type='iso')
                obj_dict['reader_to_date'] = self.date_from_request(
                    'reader_to_date', result_type='iso')
                obj_dict['reader_note'] = self.text_from_request('reader_note')
                obj_dict['reader_place'] = self.text_from_request('reader_place')
            elif twf == 'prepared' and not self.ticket.is_reader_return:
                reader_note_as_pm = True
                obj_dict['reader_to_date'] = self.date_from_request(
                    'reader_to_date', result_type='iso')
                obj_dict['reader_note'] = self.text_from_request('reader_note')

            if self.reader_fee_disabled:
                if self.reader_fee is not None:
                    obj_dict['reader_fee'] = int(self.reader_fee)
                else:
                    obj_dict['reader_fee'] = None
            elif self.reader_fee_editable:
                reader_fee_value = self.int_from_request("reader_fee")
                if reader_fee_value is not None and \
                        reader_fee_value not in (0, 70, ):
                    if self.ticket.reader_fee is None or \
                            self.ticket.reader_fee in ('0', '70', ):
                        reader_fee_value = self.ticket.reader_fee
                    else:
                        reader_fee_value = None
                    message = u"Cena pro čtenáře musí být 70 Kč nebo 0 Kč."
                    self.add_message(message)
                if reader_fee_value is not None:
                    # if reader_fee_value > self.subticket.cena:
                    #     reader_fee_value = self.subticket.cena
                    #     message = u"Cena pro čtenáře nesmí být vyšší jak celková cena požadavku!"
                    #     self.add_message(message)
                    self.reader_fee = reader_fee_value
                    obj_dict['reader_fee'] = reader_fee_value
                else:
                    obj_dict['reader_fee'] = None
            if obj_dict.get('reader_fee', None) is not None:
                obj_dict['reader_fee'] = u'{}'.format(obj_dict['reader_fee'])

            # validace
            if 'reader_to_date' in obj_dict and obj_dict['reader_to_date']:
                if 'reader_from_date' in obj_dict:
                    reader_from_date = obj_dict['reader_from_date']
                else:
                    reader_from_date = self.ticket.reader_from_date
                if reader_from_date and obj_dict['reader_to_date'] <= reader_from_date:
                    message = u'Termín, do kdy musí čtenář vrátit dokument své knihovně musí být později, než datum, od kdy je dokument k dispozici pro čtenáře v jeho knihovně.'
                    self.add_message(message)
                    del obj_dict['reader_to_date']

            is_updated = False
            if obj_dict:
                if reader_note_as_pm:
                    reader_note_original = self.ticket.reader_note
                is_updated = self.ticket.update_attrs_if_changed(**obj_dict)

            if is_updated:
                if reader_note_as_pm and self.ticket.reader_note and \
                        self.ticket.is_created_by_reader and \
                        reader_note_original != self.ticket.reader_note:
                    # Poslat automatickou zprávu čtenáři, pokud je vytvořeno
                    # čtenářem.
                    # library_zk je tady vždy i knihovnou přihlášeného
                    # uživatele, proto je možné místo self.ticket.LibraryZk()
                    # použít jen self.library
                    reader_note_as_pm_message = u'''\
Knihovník Vaší knihovny přidal novou informaci ohledně Vaší objednávky {hid} díla {doc_title} ze dne {created_date}.

Poznámka knihovníka: {reader_note}
'''.format(
    hid=self.ticket.hid,
    doc_title=self.ticket.doc_title,
    created_date=self.date_format(self.ticket.created_date),
    reader_note=self.ticket.reader_note,  # není prázdné díky podmínce výše
    #library_fullname=self.library.fullname,
    )
                    # print(" ")
                    # print(repr(reader_note_as_pm_message))
                    # print(" ")
                    # raise Exception('DEBUG')
                    self.ticket.add_pm_auto(reader_note_as_pm_message)

                    # Poslat notifikaci, jakoby knihovník napsal zprávu.
                    self.send_notifications('pm_r_zk_librarian')

                if 'reader_to_date' in obj_dict and obj_dict['reader_to_date']:
                    message = u'Termíny byly nastaveny.'
                    self.add_message(message)
                # unread
                self.ticket.unread_r = True
                if DEBUG:
                    print("unread_r")
                self.mark_librarian()

                self.add_historylog(u'Termíny pro čtenáře byly nastaveny nebo změněny: Cena pro čtenáře {}'.format(self.reader_fee), who=['zk'])
                self.ticket.reindexObject()
                self.redirect(self.context_url)
                return True

            message = u'Nebyla zadána žádná změna.'
            self.add_message(message)

        return False

    def wf_ticket(self):
        if self.ticket.wf_review_state == 'assigned' and not self.subticket:
            logger.error('wf_ticket: wf state assigned and no subticket for ticket {hid} at {url}'.format(
                hid=self.ticket.hid,
                url=self.ticket.absolute_url(),
                ))

        # storno zastavuje běžné workflow
        if self.ticket.is_big_cancel_asked:
            return False

        # nerozhodnutá reklamace zastavuje běžné workflow
        if self.ticket.is_big_complaint_undecided:
            return False

        # storno hlavního požadavku zastavuje běžné workflow objednávky - asi ano
        if self.subticket and self.subticket.is_small_cancel_asked:
            return False

        # nerozhodnutá reklamace hlavního požadavku zastavuje běžné workflow objednávky - asi ano
        if self.subticket and self.subticket.is_small_complaint_undecided:
            return False

        # požadavek na přechod
        self.wf_ticket_submit = self.request.get('wf_ticket_submit', None)
        comment = self.text_from_request('comment')
        
        if not comment:
            comment = u''

        # uvnitř jednotlivých _wf_(state)_(transition) vracíme True,  pokud
        # došlo k akci vyžadující znovunačtení (redirect)
        wfret = False

        # čtenář nebo knihovník vytvořil, nyní čeká na schválení
        if self.ticket_review_state == 'created':
            if self.is_librarian_zk:
                self.display_wf_r = True
                # created assign
                if self.ticket.can_assign:
                    wfret = wfret or self._wf_created_assign()
                else:
                    wfret = wfret or self._wf_created_assign_cannot()
                # created reject
                if self.ticket.can_reject:
                    wfret = wfret or self._wf_created_reject()

        # má aktivní subticket
        elif self.ticket_review_state == 'assigned':
            if self.is_librarian_zk:
                self.display_wf_r = True
                # assigned prepare
                if self.ticket.can_prepare:
                    wfret = wfret or self._wf_assigned_prepare()
                elif self.subticket and self.subticket.wf_review_state == "sent":
                    wfret = wfret or self._wf_assigned_prepare_cannot()
        # subticket skončil a čeká se na rozhodnutí - od created se liší jen
        # tím, že už dříve bylo schváleno, jinak vše víceméně stejně
        elif self.ticket_review_state == 'pending':
            if self.is_librarian_zk:
                self.display_wf_r = True
                # pending assign
                if self.ticket.can_assign:
                    wfret = wfret or self._wf_pending_assign()
                else:
                    wfret = wfret or self._wf_pending_assign_cannot()
                # pending reject
                if self.ticket.can_reject:
                    wfret = wfret or self._wf_pending_reject()
        # připraveno pro čtenáře (vše nastaveno a odeslán mail s informacemi)
        elif self.ticket_review_state == 'prepared':  # FIXME-sudo
            if self.is_librarian_zk_op:

                self.display_wf_r = True
                # prepared reader lent
                # is_reader_lent reader_lent_date
                if not self.ticket.is_reader_lent:
                    if self.ticket.is_mvs:
                        wfret = wfret or self._wf_prepared_lent_mvs()
                    elif self.ticket.is_mrs:
                        wfret = wfret or self._wf_prepared_lent_mrs()
                    elif self.ticket.is_edd:
                        pass
                    # čtenář vůbec nepřišel nebo dal vědět, že nechce příliš
                    # pozdě a potřebujeme celé workflow standardně dokončit
                    # prepared close
                    if self.ticket.can_close:
                        wfret = wfret or self._wf_prepared_close('before_lent')
                # prepared reader return
                # is_reader_return return_date
                elif not self.ticket.is_reader_return:
                    wfret = wfret or self._wf_prepared_return()
                    # čtenář nevrátil a už asi nevrátí
                    # prepared close
                    if self.ticket.can_close:
                        wfret = wfret or self._wf_prepared_close('before_return')
                # prepared reader return
                # is_reader_return return_date
                else:
                    # neumožnit uzavřít ticket po vrácení čtenářem, bude
                    # automaticky uzavřen po uzavření hlavního subticketu,
                    # výjimkou jsou speciální situace, kdy nastavit na True
                    enable_prepared_close = False
                    if not self.subticket:
                        # k tomuto by asi nemělo dojít, nicméně východisko je
                        # uzavřít objednávku, proto umožnit
                        enable_prepared_close = True
                    elif self.subticket_review_state == 'sent':
                        self.prepared_info = u'Vráceno čtenářem, k uzavření dojde uzavřením hlavního požadavku.  Nyní pošlete zpět dožádané knihovně.'
                    elif self.subticket_review_state == 'sent_back':
                        self.prepared_info = u'Vráceno čtenářem, k uzavření dojde uzavřením hlavního požadavku.  Bylo posláno zpět dožádané knihovně, čeká se na přijetí zpět a uzavření požadavku - tím bude automaticky uzavřena i objednávka.'
                    elif self.subticket_review_state == 'closed':
                        # k tomuto by nemělo dojít, nicméně východisko je
                        # uzavřít objednávku, proto umožnit
                        enable_prepared_close = True
                    # uzavření objednávky je umožněno, zobrazit ho nebo
                    # zpracovat
                    if enable_prepared_close and self.ticket.can_close:
                        wfret = wfret or self._wf_prepared_close('after_return')

        # koncové stavy
        elif self.ticket_review_state in ('cancelled', 'rejected', ):
            pass

        elif self.ticket_review_state in ('closed', ):
            if not self.subticket:
                pass
            elif self.subticket_review_state == 'sent':
                self.prepared_info = u'Pokud si čtenář knihu nevyzvedl, zašlete ji zpět dožádané knihovně. Pokud čtenář knihu ztratil nebo se lze domnívat, že ji nevrátí, zašlete ji zpět fiktivně a náhradu škody vyřešte mimo ZÍSKEJ.'
                # self.prepared_info = u'Čtenář nevyzvedl - nyní pošlete zpět dožádané knihovně.  Nebo čtenář ztratil a asi už nevrátí, jakoby pošlete zpět.  Náhradu dohodněte s dožádanou knihovnou mimo systém ZÍSKEJ.'
            elif self.subticket_review_state == 'sent_back':
                self.prepared_info = u'Pokud si čtenář knihu nevyzvedl, zašlete ji zpět dožádané knihovně. Pokud ji ztratil nebo se lze domnívat, že ji nevrátí, zašlete ji dožádané knihovně fiktivě a náhradu dohodněte mimo ZÍSKEJ'
                # self.prepared_info = u'Čtenář nevyzvedl a posláno zpět dožádané knihovně.  Nebo čtenář ztratil a asi už nevrátí, proto jakoby posláno zpět a čeká se na uzavření dožádanou knihovnou.  Náhradu dohodněte s dožádanou knihovnou mimo systém ZÍSKEJ.'

        # cokoli jinak je chyba kódu nebo dat a chceme o tom vědět
        else:
            logger.warning('Invalid ticket state {}'.format(self.ticket_review_state))

        # znovunačtení po akci
        if wfret:
            return True

        # url se použije jen v tal:condition, ale musí existovat, protože výše
        # url nastavujeme jen na '', tak když neexistuje, musíme přidělit
        # jakýkoli True, třeba id
        for transition in self.ticket_transitions:
            if 'url' not in transition:
                transition['url'] = transition['id']

        if DEBUG:
            print('self.ticket_transitions')
            pprint(self.ticket_transitions)

        return False

    # def _parse_lbdebug(self):
    #     self.lbdebug_html = u''
    #     self.lbdebug_html += u'<ul>\n'

    #     for doc_id in self.lbdebug['ids']:
    #         doc_data = self.lbdebug['data'][doc_id]

    #         self.lbdebug_html += u'<li>\n'

    #         self.lbdebug_html += u'Dokument <a href="{doc_url}" target="_blank">{doc_id}</a><br>\n'.format(doc_id=doc_id, doc_url=get_cpk_url(doc_id))

    #         self.lbdebug_html += u'Penalizace <span class="small">{rating}</span><br>\n'.format(rating=doc_data['rating'])
    #         self.lbdebug_html += u'Label <span class="small">{label}</span><br>\n'.format(label=doc_data['label'])
    #         if doc_data['skipped']:
    #             self.lbdebug_html += u'Nepoužito <span class="small">{skip_reason}</span><br>\n'.format(skip_reason=doc_data['skip_reason'])

    #         # Jednotky
    #         unit_ids = doc_data['ids']

    #         self.lbdebug_html += u'Jednotky ({value}):\n'.format(value=len(unit_ids))
    #         self.lbdebug_html += u'<ul>\n'

    #         for unit_id in unit_ids:
    #             self.lbdebug_html += u'<li>\n'

    #             try:
    #                 unit_data = doc_data['data'][unit_id]
    #             except KeyError:
    #                 unit_data = None

    #             self.lbdebug_html += u'Jednotka <span class="small">{unit_id}</span><br>\n'.format(unit_id=unit_id)
    #             if doc_data['best'] and doc_data['best'] == unit_id:
    #                 self.lbdebug_html += u'Nejlepší jednotka<br>\n'

    #             if unit_data:
    #                 value = unit_data.get('rating', u'')
    #                 self.lbdebug_html += u'Penalizace <span class="small">{value}</span><br>\n'.format(value=value)

    #                 value = unit_data.get('log', u'')
    #                 value = value.strip(u' \n')
    #                 value = value.replace(u'rating', u'penalizace')
    #                 value = value.replace(u'nastaven ', u'nastavena ')
    #                 value = value.replace(u'\n', u' &ndash; ')
    #                 self.lbdebug_html += u'Detaily: <span class="small">{value}</span><br>\n'.format(value=value)

    #             else:
    #                 self.lbdebug_html += u'Detaily o jednotce nejsou k dispozici<br>\n'

    #             self.lbdebug_html += u'</li>\n'

    #         self.lbdebug_html += u'</ul>\n'

    #         self.lbdebug_html += u'</li>\n'

    #     self.lbdebug_html += u'</ul>\n'

    # def setup_lbdebug(self):
    #     try:
    #         doc_order_suggested_data = self.ticket.doc_order_suggested_data
    #     except AttributeError:
    #         doc_order_suggested_data = None
    #     if doc_order_suggested_data:
    #         try:
    #             self.lbdebug = json.loads(doc_order_suggested_data)
    #         except Exception, e:
    #             logger.warning('setup_lbdebug failed: ' + str(e))
    #             return
    #         self.show_lbdebug = True
    #         self._parse_lbdebug()

    def call(self):
        self.unauthorized = True
        if self.is_anon:
            return

        self.setup_init()
        self.setup_ticket_subticket_subtickets()
        self.setup_library_zk_dk()

        self.setup_is_role()
        if self.redirect_when_needed():
            return
        self.unauthorized = False

        self.setup_texts()
        self.setup_wf()
        self.setup_pms()
        #self.setup_lbdebug()

        if self.complaint_cancel():
            return

        if self.is_reader:
            if self.pm_r_zk():
                return
            self.setup_primary_btn()
            self.setup_timeline()

        elif self.is_librarian_zk_op:
            if self.pm_r_zk():
                return
            if self.setup_tabs_st():
                return
            if self.set_platebator_paid():
                return
            if self.set_platebator_paid_edd():
                return

            # Workflow
            if self.wf_ticket():
                return

            # nejen ŽK, ale i operátor, protože by měl vidět detaily
            # z posledního AutoLB - rozlišení je uvnitř dk_order()
            if self.dk_order():
                return

            if self.subticket:
                if self.wf_subticket():
                    return

            if self.is_librarian_zk:

                # edit forms
                if self.add_info():
                    return
                if self.reader_dates():
                    return
                if self.manual_lb():
                    return
                if self.manual_edit():
                    return

                if self.subticket:
                    if self.pm_zk_dk():
                        return

                # subtickets
                for subticket in self.subtickets:
                    if self.wf_subticket_history(subticket):
                        return

            self.setup_primary_btn()
            self.setup_timeline()

        #self.log_time('lorem')

        # unread
        if self.is_reader and self.ticket.unread_r:
            safeWrite(self.ticket, self.request)
            self.ticket.unread_r = False
            self.ticket.reindexObject()

        if self.is_librarian_zk and self.ticket.unread_zk:
            safeWrite(self.ticket, self.request)
            self.ticket.unread_zk = False
            self.ticket.reindexObject()

        if VISIT_RESETS_UNREAD_OP and self.is_ziskej_operator and self.ticket.unread_op:
            safeWrite(self.ticket, self.request)
            self.ticket.unread_op = False
            self.ticket.reindexObject()

        return
