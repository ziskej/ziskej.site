# -*- coding: utf-8 -*-
"""Společná BrowserView třída ZiskejBrowserView pro celé Získej UI, nezahrnuje
API a AJAX.

Obsahuje funkcionalitu dostupnou v jakékoli instanci třídy BrowserView v Získej
UI, např. text_from_request.
"""

from __future__ import print_function

import logging
import urllib
from datetime import datetime

from DateTime import DateTime
from plone import api
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.protect.utils import addTokenToUrl
import transaction

from ziskej.site import IS_MRS, IS_EDD, IS_EDD_AT_TEST
from ziskej.site.auth.auth import auth_ziskej_role
from ziskej.site.library_helpers import lib_helper
from ziskej.site.notifications import (
    ZiskejNotifications,
    ErrorNotifications,
    )
from ziskej.site.ticket_helpers import get_cpk_url
from ziskej.site.utils import (
    datetime_add_d,
    datetime_add_wd,
    format_date,
    get_instance_type_label,
    get_time0,
    get_time_ms,
    pprint,
    safe_unicode,
    safe_utf8,
    sanitize_input_text,
    )
from ziskej.site.ziskej_parameters import ziskej_parameter


logger = logging.getLogger("ziskej.browser.init")
logger_time = logging.getLogger("profiling")

PROFILING_ON = True


class ZiskejBrowserView(BrowserView):
    """Base BrowserView pro projekt Získej - použita jen tam, kde má význam, pro
    triviální BrowserView v rámci projektu není potřeba.
    """

    template = ViewPageTemplateFile('dummy_view.pt')

    IS_MRS = IS_MRS
    IS_EDD = IS_EDD
    IS_MULTISERVICE = bool(IS_MRS or IS_EDD)
    IS_EDD_AT_TEST = IS_EDD_AT_TEST

    def valid_sigla(self, sigla, is_message=True):
        if not sigla:
            return None
        sigla = sigla.lower()
        if len(sigla) != 6:
            if is_message:
                self.add_message(
                    u'Zadaná sigla není validní.',
                    message_type='error')
            return None
        try:
            sigla_str = str(sigla)
        except UnicodeEncodeError:
            if is_message:
                self.add_message(
                    u'Zadaná sigla není validní, obsahuje zakázané znaky.',
                    message_type='error')
            return None
        return sigla_str

    def library_by_sigla(self, sigla, is_message=True):
        if not sigla:
            return None
        sigla = sigla.lower()
        if len(sigla) != 6:
            if is_message:
                self.add_message(
                    u'Zadaná sigla není validní.',
                    message_type='error')
            return None
        try:
            sigla_str = str(sigla)
            result_library = getattr(self.portal.libraries, sigla_str, None)
        except UnicodeEncodeError:
            if is_message:
                self.add_message(
                    u'Zadaná sigla není validní, obsahuje zakázané znaky.',
                    message_type='error')
            return None
        return result_library

    def _get_siglas_by_fullname(self, fullname, as_html=False, as_obj=False):
        default = None
        if not fullname:
            return default
        # Pro Olomouc najít i Olomouci.
        if not fullname.endswith(u'*'):
            fullname = u'{}*'.format(fullname)
        query = dict(
            portal_type='Library',
            SearchableText=fullname,
            )
        # Potřebujeme i neaktivní knihovny, protože mohou mít nějaké objednávky
        # či požadavky a až poté být deaktivovány.
        brains = self.catalog.unrestrictedSearchResults(query)
        len_brains = len(brains)
        print(len_brains, 'for query', query)
        if not len_brains:
            return default
        result = []
        if as_html:
            for brain in brains:
                value = brain.Title
                if not value:
                    value = brain._unrestrictedGetObject().Title()
                    logger.warning('Recataloging is needed, _get_siglas_by_fullname.')
                result.append(value)
        elif as_obj:
            for brain in brains:
                result.append(brain._unrestrictedGetObject())
        else:
            for brain in brains:
                value = brain.sigla
                if not value:
                    value = brain._unrestrictedGetObject().sigla
                    logger.warning('Recataloging is needed, _get_siglas_by_fullname.')
                result.append(value)

        return result

    def redirect(self, url=None, message=None):
        if url is None:
            url = self.context_url
        # if message is not None:
        #     if '?' in url:
        #         url = url + '&'
        #     else:
        #         url = url + '?'
        #         message = safe_utf8(message)
        #         message_encoded = urllib.urlencode(dict(message=message))
        #         url = url + message_encoded
        logger.info('redirect: ' + url)
        self.is_redirect = True
        self.request.RESPONSE.redirect(url)

    def add_message(self, message, message_type='info'):
        ptool = self.portal.plone_utils
        ptool.addPortalMessage(message, message_type)

    def add_historylog(self, message, who=None, subticket=None,
                       is_edd_reader=False, is_cron=False):
        """Zapíše do audit logu ticketu a případně i subticketu message
        zobrazitelnou pro operátora a pro who (list).  Do subticketu se zapisují
        změny provedené DK (is_subticket True) a vše, kde dk je v seznamu who.

        Volitelný argument subticket jednak vynutí zápis do subticketu, ale
        zejména zvolí subticket, který tak může být i jiný, než aktuální.
        """

        # cron task
        if is_cron:
            pass

        # EDD reader
        elif is_edd_reader:
            # Toto nastane jen pro edd čtenáře který není přihlášený.
            # V této situaci dohadneme library z ticketu. 
            library_str = u'knihovna {}'.format(self.ticket.library_zk)
            user_id = u"Čtenář"

        # standardní přístup
        else:
            user_id = self.user_id or u'?'
            if self.is_ziskej_operator:
                library_str = u'SC ZÍSKEJ'
            elif self.library and self.library.sigla:
                library_str = u'knihovna {}'.format(self.library.sigla)
            else:
                library_str = u'?'

        if is_cron:
            message = u'{} (automaticky)'.format(message)
        else:
            message = u'{} (uživatel {}, {})'.format(message, user_id, library_str)
            
        if subticket is not None:
            subticket.add_historylog(message, who=who)
        elif self.is_subticket or 'dk' in who:
            if self.subticket:
                self.subticket.add_historylog(message, who=who)
            else:
                try:
                    logger.warning(
                        'add_historylog: Missing (bv).subticket but dk in who, hid {}, message: {}'.format(
                            self.ticket.hid,
                            message.encode('ascii', errors='replace'),
                            ))
                except:
                    logger.warning('add_historylog: Missing (bv).subticket but dk in who')

        if is_cron:
            subticket.ticket.add_historylog(message, who=who)
        else:
            self.ticket.add_historylog(message, who=who)

    def get_historylog(self):
        if self.is_ziskej_operator:
            who = 'operator'
        elif self.is_librarian_zk:
            who = 'library_zk'
        elif self.is_librarian_dk:
            who = 'library_dk'
        # reader
        else:
            who = ''
            logger.warning('history log: who is unknown')
        if not self.is_subticket:
            return self.ticket.get_historylog(who=who)
        else:
            return self.subticket.get_historylog(who=who)

    def _transaction(self):
        # Commit transaction
        transaction.commit()
        # Perform ZEO client synchronization (if running in clustered mode)
        self.portal._p_jar.sync()  # untested

    def build_url(self, url_base, url_dict):
        if url_dict is None:
            return url_base
        url_query = urllib.urlencode(url_dict)
        url = '{base}?{query}'.format(base=url_base, query=url_query)
        return url

    def date_format_html(self, date_str, is_time=False, omit_year=False):
        return self.date_format(date_str, is_time=is_time, is_html=True, omit_year=omit_year)

    def date_format(self, date_str, is_time=False, is_html=False, omit_year=False):
        """Očekává string 2017-12-10 (pro 10. prosinec 2017) a vrací správně formátované
        """

        if not date_str:
            return u''
        #return DateTime(date_str).strftime('%-d.%-m.%Y')
        return format_date(DateTime(date_str), is_time=is_time, is_html=is_html, omit_year=omit_year)

    def get_DateTime_from_date_str(self, value_str):
        dt = None
        try:
            value = datetime.strptime(value_str + ' 12:00', '%d.%m.%Y %H:%M')
            value = value.strftime('%Y-%m-%d')
            dt = DateTime(value)
        except:
            logger.info('Ignored invalid DateTime for value: {value_str}'.format(value_str=repr(value_str)))
            raise
        return dt

    def dt_as(self, dt, result_type='string'):
        if not dt:
            return None
        if result_type in ('string', 'datepicker', ):
            result = dt.strftime('%-d.%-m.%Y')
        elif result_type == 'iso':
            result = dt.strftime('%Y-%m-%d')
        elif result_type == 'isodt':
            result = dt.strftime('%Y-%m-%d %-H:%M:%S')
        elif result_type == 'DateTime':
            result = dt
        elif result_type == 'datetime':
            result = dt.asdatetime()
        elif result_type == 'datepicker':
            result = dt.asdatetime()
        return result

    def date_datapicker(self, date_str):
        return self.dt_as(DateTime(date_str), result_type='datepicker')

    def today_datapicker(self):
        return self.dt_as(DateTime(), result_type='datepicker')

    def today_add_d_datapicker(self, d):
        return self.dt_as(datetime_add_d(d), result_type='datepicker')

    def today_add_wd_datapicker(self, wd):
        return self.dt_as(datetime_add_wd(wd), result_type='datepicker')

    def date_from_request(self, name, result_type='string', message_invalid=None):
        """Převede datum z parametru s názvem name a standardním datapicker
        formátu (dd.mm.yyyy) na DateTime a následně na result_type.

        Pokud dojde k výjimce, například kvůli neplatnému formátu, tak
        - pokud je message_invalid None, dojde k propagaci výjimky
        - pokud je message_invalid True (neprázdný string), vrátí se None
          a nastaví se hláška systému na message_invalid
        - pokud je message_invalid False (prázdný string), vrátí se None
        """

        try:
            value_str = self.request.get(name, None)
            if not value_str:
                return None
            if not isinstance(value_str, basestring):
                if isinstance(value_str, list):
                    raise Exception('Expecting string but get list for {name}'.format(name=name))
                else:
                    raise Exception('Expecting string but get not string for {name}'.format(name=name))
            value_str = sanitize_input_text(value_str)
            value_str = value_str.replace(' ', '')
            if not value_str:
                return None
            dt = self.get_DateTime_from_date_str(value_str)
            if not dt:
                return None
            return self.dt_as(dt, result_type=result_type)
        except:
            if message_invalid is None:
                raise
            if message_invalid:
                self.add_message(message_invalid)
            return None

    def bool_from_request(self, name):
        value = self.request.get(name, None)
        if value is None:
            value = False
        elif isinstance(value, basestring):
            value = bool(value == 'on')
        elif not isinstance(value, bool):
            logger.warning(name + ' is not string (on) or bool: ' + repr(value))
            value = False
        return value

    def int_from_request(self, name, min=0, max=None):
        value = self.request.get(name, None)
        if value is None:
            return None
        value = value.strip()
        try:
            value = int(value)
        except ValueError:
            logger.warning(name + ' is not int: ' + str(value))
            value = None
        if min is not None and value < min:
            value = None
        if max is not None and value > max:
            value = None
        return value

    def text_from_request(self, name, lov=None, default=None, sanitize=True):
        value = self.request.get(name, None)
        if value is None:
            return default
        if not isinstance(value, basestring):
            if isinstance(value, list):
                raise Exception('Expecting string but get list for {name}'.format(name=name))
            else:
                raise Exception('Expecting string but get not string for {name}'.format(name=name))
        #value = safe_utf8(value)
        value = safe_unicode(value)
        if sanitize:
            value = sanitize_input_text(value)  # cgi.escape
        if lov is not None and value not in lov:
            logger.info('ignored unexpected value for lov based input {name}'.format(name=name))
            value = default
        return value

    def select_from_request(self, name, alllist):
        result = self.multiselect_from_request(name, alllist)
        if len(result) == 0:
            return None
        if len(result) == 1:
            return result[0]
        raise Exception('Invalid option')

    def multiselect_from_request(self, name, alllist):
        value = self.request.get(name, None)
        if value is None:
            value = []
        elif isinstance(value, basestring):
            value = [value]
        if not isinstance(value, list):
            raise Exception('Invalid options.')
        result = []
        for item in value:
            if not item:
                continue
            if not isinstance(item, basestring):
                raise Exception('Invalid option.')
            item = safe_unicode(item)
            if item not in alllist:
                raise Exception('Invalid option.')
            result.append(item)
        return result

    def js_multiselect_from_request(self, name, alllist):
        value = self.request.get(name, None)
        if value is None:
            value = u''
        elif isinstance(value, basestring):
            value = safe_unicode(value)
        else:
            raise Exception('Invalid options.')
        result = []
        for item in value.split(','):
            if not item.strip():
                continue
            if item not in alllist:
                raise Exception('Invalid option.')
            result.append(item)
        return result

    def send_notifications(self, nid, ticket=None, subticket=None):
        """Wrapper pro notifikace, pošle notifikaci s id nid.  Parametry ticket
        a subticket jsou tady záměrně ignorovány a místo nich se použijí objekty
        nastavené z kontextu.
        """

        # ŽK už notifikaci dostala - posílá se notifikace do DK, protože správce
        # jedná za DK
        AS_DK_NIDS = (
            "wf_t_stop_zk",
            "wf_st_accept", "wf_st_conditionally_accept",
            "wf_st_refuse", "wf_st_accepted_refuse",
            "wf_st_send", "wf_st_send_here", "wf_st_send_edd_zk",
            "wf_st_accept_back_main", "wf_st_accept_back_other",
            "wfcc_small_accept_zk", "wfcc_small_big_accept_zk",
            "wfcc_small_decline_zk", "wfcm_small_new_zk", "wfcm_small_std_zk",
            "pm_zk_dk_librarian_dk",
            )
        # DK už notifikaci dostala - posílá se notifikace do ŽK, protože správce
        # jedná za ŽK
        AS_ZK_NIDS = (
            "wf_t_zk_assign_dk", "wf_t_reject", "wf_t_prepare",
            "wf_st_ca_accept", "wf_st_ca_retract",
            "wf_st_send_back", "wfcc_small_ask_dk", "wfcm_small_new_dk",
            "wfcm_small_std_dk", "wf_t_cancel_by_zk_reader",
            "pm_zk_dk_librarian_zk", "pm_r_zk_librarian",
            "wf_t_zk_accepted_reader", "wf_t_reject_reader",
            )

        ziskej_notifications = ZiskejNotifications(
            ticket=self.ticket,
            subticket=self.subticket_last)

        if self.is_sudo:
            # ŽK už notifikaci dostala - posílá se notifikace do DK
            if nid in AS_DK_NIDS:
                ziskej_notifications.send_notifications('op_as_dk')
            # DK už notifikaci dostala - posílá se notifikace do ŽK
            elif nid in AS_ZK_NIDS:
                ziskej_notifications.send_notifications('op_as_zk')
            else:
                logger.warn('May be missing notification during sudo for nid {}'.format(nid))

        ziskej_notifications.send_notifications(nid)

    def error_notify(self, error_class_id, e, method_id, detail_error=None):
        try:
            user_id = self.user_id
        except:
            user_id = '?'
        message = '{error_class_id} in {method_id} for user {user_id}: {e}'.format(
            error_class_id = error_class_id,
            e = str(e),
            method_id = method_id,
            user_id = user_id)
        if detail_error:
            message += ' [more info] ' + detail_error
        logger.warning('[error_notify] ' + message)

        error_notifications = ErrorNotifications()
        error_notifications.send_notifications(message)

    def reader_gdpr_check(self):
        if not self.is_reader:
            return True
        reader = self.ziskej_user_info['user']
        if reader.gdpr_registration and reader.gdpr_data:
            return True
        if self.context in (reader, self.portal):
            return True
        self.add_message(u'Systém ZÍSKEJ není možné používat bez souhlasu s registrací a s uchováváním dat.')
        #url = self.portal_url + '/logout'
        url = reader.absolute_url() + '/reader_update'
        self.redirect(url)
        return False

    def get_reader_by_id(self, reader_id, default=None):
        reader = getattr(self.portal.readers, reader_id, default)
        return reader

    def call(self):
        """Každý BrowserView definuje vlastní call"""
        pass

    def call_finalize(self):
        """Každý BrowserView může definovat akce po skončení vlastního call"""
        pass

    def log_time(self, txt, time0=None):
        if not PROFILING_ON:
            return
        if time0 is None:
            time0 = self.time0
        msg = u'{ts} {txt}'.format(ts=get_time_ms(time0), txt=txt)
        logger_time.info(msg)

    def _call_init(self):
        self.time0 = get_time0()
        self.use_template_reader = False

        # catalog, portal etc.
        self.catalog = api.portal.get_tool(name='portal_catalog')
        self.portal = api.portal.get()
        self.home = None
        try:
            self.home = self.portal.home
        except:
            query = dict(
                portal_type = 'Frontpage',
                sort_on = 'modified',
                )
            brains = self.catalog(query)
            if brains:
                self.home = brains[0].getObject()
        if not self.home:
            logger.error('Missing home with parameters')
            self.add_message(u'Chybí titulní stránka.')
        if self.home:
            self.get_parameters = self.home.get_parameters
        self.portal_url = self.portal.absolute_url()
        self.context_url = self.context.absolute_url()
        self.context_url_protected = addTokenToUrl(self.context_url)

        # typ instance
        self.instance_type, self.instance_type_label, instance_title_postfix = get_instance_type_label()
        self.is_instance_prod = bool(self.instance_type == 'prod')
        self.is_instance_local = bool(self.instance_type == 'development')
        self.is_instance_testing = bool(self.instance_type in ('development', 'dev', 'test'))

        # lib_helper - inicializováno při startu bez portal, pokud ještě nikdy
        # nedostal portal, inicializuje se pomocí bind_portal, jinak levný call
        self.lib_helper = lib_helper
        self.lib_helper.bind_portal(self.portal)
        # toto je potřeba kvůli aktuálním lib_ids a lib_ids_active, je ale
        # zbytečné volat pro každý request
        #self.lib_helper.update_lid_ids()

        # Umožnit testovat provozní dobu a dovolenou pro vytváření objednávek
        # a pro AutoLB.
        self.testing_now_dt = ziskej_parameter(
            'testing_now_str', portal=self.portal)

        self.ticket = None
        self.subticket = None
        self.subticket_last = None

        self.cpk_url = get_cpk_url()

        self.is_redirect = False
        self.ticket_safe = False
        self.subticket_safe = False

        # auth ziskej user info
        # dict(user_id = None, user = None, role = None, library = None, )
        # role .. None, Reader, Librarian
        self.ziskej_user_info = auth_ziskej_role(
            self.context, self.request, caller='bv')
        self.groups = self.ziskej_user_info.get('groups', [])

        # sudo marker
        self.is_sudo = self.ziskej_user_info.get('sudo_marker', False)

        self.is_anon = self.ziskej_user_info['is_anon']
        # low level přístup
        self.is_manager = self.ziskej_user_info['is_manager']
        self.is_ziskej_admin = self.ziskej_user_info['is_ziskej_admin']
        self.is_ziskej_operator = self.ziskej_user_info['is_ziskej_operator']
        self.is_library_admin = self.ziskej_user_info['is_library_admin']
        self.is_library_admin_or_operator = self.is_ziskej_operator or self.is_library_admin
        # admin v daném kontextu (např. správce knihovny v kontextu jeho knihovny)
        self.is_admin = self.ziskej_user_info['is_admin']
        # může v daném kontextu alespoň něco editovat (např. knihovník v kontextu jeho knihovny)
        self.is_edit = self.ziskej_user_info['is_edit']
        # see https://docs.plone.org/develop/plone/security/permission_lists.html
        # nedokončená konfigurace oprávnění
        self.is_manager_fixme = self.is_admin

        # reader, librarian settings
        self.is_reader = False
        self.is_librarian = False
        self.library = None
        self.user_id = None
        if self.ziskej_user_info:
            self.is_reader = bool(self.ziskej_user_info['role'] == 'Reader')
            # pozor is_librarian je v ticket_core nastaven vždy na False a místo
            # toho je nastaveno is_librarian_zk resp. is_librarian_dk
            self.is_librarian = bool(self.ziskej_user_info['role'] == 'Librarian')
            self.library = self.ziskej_user_info['library']
            self.user_id = self.ziskej_user_info['user_id']
        self.is_reader_or_librarian = self.is_reader or self.is_librarian

        # only admin
        self.is_root = bool(self.user_id == 'admin')

        # theme
        self.ziskej_theme_enabled = not bool(self.request.get('t',  None))

        # Debug warning
        if self.text_from_request('test_portal_message'):
            self.add_message(u'Testovací portal message', message_type='warning')

    def __call__(self):
        exception_redirect = False

        try:
            self._call_init()

            # Reader GDPR check log out if reader does not have gdpr rekated
            # agreement
            if not self.reader_gdpr_check():
                return u''

            exception_redirect = True

            # Test exception catcher
            TEST_EXCEPTION_CATCHER = False
            if TEST_EXCEPTION_CATCHER:
                a = dict()
                if self.context.portal_type != 'Frontpage':
                    a['a']
                else:
                    a['a']
                    #pass

            # každý BrowserView definuje vlastní call, kde je logika specifická
            # pro daný pohled
            self.call()

            # někdy call má spoustu konců a na závěr chceme udělat nějakou akci
            # vždy, k tomu slouží
            self.call_finalize()

            self.log_time('before template')

            # některé BrowserView mají více šablon, typicky jinou šablonu pro
            # čtenáře
            if self.use_template_reader:
                result_template = self.template_reader()

            # většina má ale jen jednu šablonu, použijme ji
            else:
                result_template = self.template()

            self.log_time('after template = end of request')
            return result_template

        # Exception catcher
        except Exception, e:
            # try:
            #     message = u'Uncatched exception for {url}: {e}'.format(url=self.request.URL, e=e, )
            #     logger.exception(message)
            # except Exception, e:
            #     message = u'Uncatched exception for {url}'.format(url=self.request.URL)
            #     logger.exception(message)
            message = u'Uncatched exception for {url}'.format(url=self.request.URL)
            logger.exception(message)
            #logger.warning('[error_notify] ' + message)

            # production has to be False, True is only for debugging
            DEBUG_EXCEPTIONS = bool(self.instance_type not in ('prod', 'demo', ))
            if DEBUG_EXCEPTIONS:
                raise

            error_notifications = ErrorNotifications()
            error_notifications.send_notifications(message)

            portal = api.portal.get()
            ptool = portal.plone_utils
            ptool.addPortalMessage(u'Při zpracování vašeho požadavku se vyskytla chyba.  Provozovatel byl informován.', 'error')

            if self.context.portal_type == 'Frontpage':
                return 'Internal error'
            if not exception_redirect:
                return 'Internal error'
            url = portal.absolute_url()
            self.request.RESPONSE.redirect(url)
            return url
