# -*- coding: utf-8 -*-
"""Pomocné nástroje pro práci s knihovnami a jejich import z CPK API.

Pro každý worker definuje zejména lib_helper používaný pro rychlou práci
s knihovnami.
"""

import json
import logging
import codecs
import os.path
import random

import transaction
from plone import api
from plone.dexterity.utils import createContent

from ziskej.site import ZISKEJ_DATA_DIR, ZISKEJ_ASYNC_BASE_URL
from ziskej.site import cpk_api
from ziskej.site.marc21 import ZiskejMarc
from ziskej.site.library_cpk_ids import cpk_all_libraries
from ziskej.site.utils_lib import pprint, safe_utf8
#from ziskej.site.library_helpers_storage import ?


logger = logging.getLogger("ziskej.site.library_helpers")

DEBUG = False

LIBRARY_DATA_CPK_FILENAME_SAVE = os.path.join(ZISKEJ_DATA_DIR, 'library-data-cpk.json')
LIBRARY_DATA_CPK_FILENAME_LOAD = os.path.join(ZISKEJ_DATA_DIR, 'library-data-cpk-2018-03-31.json')


class LibHelperBase(ZiskejMarc):
    """

    Profil knihovny a napojení na CPK import

    full ADR
    --------

    Sigla
    - CT: sigla
    - CPK: SGL

    Název
    - CT: fullname
    - CPK: NAZ nebo NAZ a

    Adresa
    - CT: address
    - CPK: ADR u m c nebo ADR[0] u m c (to je ale dost blbá heuristika, prostě vzít první)

    URL
    - CT: url
    - CPK: URL complex

    kde editovat v ADR
    - CPK: DRL

    kdy naposledy modifikováno v ADR
    - CPK: AKT

    first ADR later Získej
    ----------------------

    (fakturační údaje)

    Fakturace - název
    - CT: invoice_name
    - CPK: NAZ - už bylo

    Fakturace - adresa
    - CT: invoice_address
    - CPK: ADR u m c ? - už bylo

    IČ
    - CT: ico
    - CPK: ICO a

    only Získej
    -----------

    Zkratka knihovny
    - CT: shortname

    Je v CPK?
    - CT: is_cpk

    IdP EntityID
    - CT: idp_entityid

    deeplink_base_url
    - CT: deeplink_base_url
    - CPK: asi není

    Název pro Čtenář ID
    - CT: reader_lid_label

    Pravidelná otvírací doba
    - CT: opening_hours, json
    - CPK: OTD complex

    Dovolená
    - CT: vacation, json

    Whitelist
    - CT: whitelist

    Blacklist
    - CT: blacklist

    fees
    - CT: ['mvs_zk_cena', 'mvs_zk_dk_cena', 'mvs_dk_cena', 'mvs_dk_cena_min', 'mvs_dk_cena_max', ]

    Jaké služby (pohled operátor, pohled správce knihovny)

    """

    def __init__(self):
        super(LibHelperBase, self).__init__()
        self._data = []
        self._data_cpk_idx = dict()
        self._data_sigla_idx = dict()

    def sigla_by_cpk(self, library_id):
        if library_id not in self._data_cpk_idx:
            return None
        return self._data[self._data_cpk_idx[library_id]]['id']  # sigla aba001

    def cpk_by_sigla(self, sigla):
        if not sigla:
            return None
        sigla = sigla.lower()
        if sigla not in self._data_sigla_idx:
            return None
        return self._data[self._data_sigla_idx[sigla]]

    def save_data(self, filename=None):
        if filename is None:
            filename = LIBRARY_DATA_CPK_FILENAME_SAVE
        library_json = json.dumps(self._data, ensure_ascii=False)
        with codecs.open(filename, 'w', encoding='utf-8') as json_file:
            json_file.write(library_json)

    def load_data(self, filename=None):
        if filename is None:
            filename = LIBRARY_DATA_CPK_FILENAME_LOAD
        with codecs.open(filename, 'r', encoding='utf-8') as json_file:
            library_json = json_file.read()

        self._data = json.loads(library_json)  # []
        self._data_cpk_idx = dict()
        self._data_sigla_idx = dict()

        _data_idx = 0
        for obj_dict in self._data:
            self._data_cpk_idx[obj_dict['library_id']] = _data_idx
            self._data_sigla_idx[obj_dict['id']] = _data_idx
            _data_idx += 1

    def dig_more_from_cpk(self, obj_dict, cpk_data):
        if cpk_data is None:
            return obj_dict

        # phones
        cpk_key = u'TEL'
        cpk_value = cpk_data.get(cpk_key, None)
        if not isinstance(cpk_value, list):
            cpk_value = [cpk_value]
        phone = None
        if cpk_value:
            phone = cpk_value[0]
        phone_mvs = None
        phone_mvs_list = [x for x in cpk_value if 'mvs' in x.lower()]
        if phone_mvs_list:
            phone_mvs = phone_mvs_list[0]

        # mvs email
        cpk_key = u'EML'
        cpk_value = cpk_data.get(cpk_key, None)
        if not isinstance(cpk_value, list):
            cpk_value = [cpk_value]
        email = None
        if cpk_value:
            email = cpk_value[0].get('u', u'')
        email_mvs = None
        email_mvs_list = [x.get('u', u'') for x in cpk_value if 'mvs' in x.get('z', u'').lower()]
        if email_mvs_list:
            email_mvs = email_mvs_list[0]

        obj_dict['phone'] = phone
        obj_dict['phone_mvs'] = phone_mvs
        obj_dict['email'] = email
        obj_dict['email_mvs'] = email_mvs
        return obj_dict

    def _parse_cpkdata(self, obj_dict, data):
        MAPPING_PORTAL_CPK = dict(
            sigla = 'SGL',
            fullname = 'NAZ',
            shortname = 'ZKR',
            ico = 'ICO',
            )
        # standardní stringy
        for k in MAPPING_PORTAL_CPK.keys():
            obj_dict[k] = self.parse_library_std(data.get(MAPPING_PORTAL_CPK[k], None))
        # speciální struktury
        obj_dict[u'url'] = self.parse_library_url(data.get('URL', None))
        obj_dict[u'address'] = self.parse_library_address(data.get('ADR', None))
        # kopie už existujících
        obj_dict[u'invoice_name'] = obj_dict['fullname']
        obj_dict[u'invoice_address'] = obj_dict['address']

        try:
            self.dig_more_from_cpk(obj_dict, data)
        except Exception, e:
            logger.info(e)
            logger.exception('Silently skipping exception in dig_more_from_cpk.')

        return obj_dict

    def reparse_cpkdata(self, obj_dict):
        #pprint(obj_dict['cpkdata'])
        obj_dict = self._parse_cpkdata(obj_dict, obj_dict['cpkdata'])
        return obj_dict

    def reparse(self):
        # idx: 1311 .. ADR má cm nemá u, URL string
        #for counter in range(10):
        idx_limit = len(self._data)
        for idx in range(idx_limit):
        #for idx in range(6594, 6596+1):
        #for idx in (6582, ):
            #idx = random.randrange(len(self._data))
            #idx = 1311
            
            #print '-- idx: {} / {}'.format(idx, idx_limit)
            obj_dict = self._data[idx]
            obj_dict = self.reparse_cpkdata(obj_dict)

            #obj_dict_copy = dict(obj_dict)
            #del obj_dict_copy['cpkdata']
            #pprint(obj_dict_copy)
            #print ' '

            self._data[idx] = obj_dict

        self.save_data()


class LibHelperCpk(LibHelperBase):
    """
    """

    def __init__(self, cpk_api_settings):
        super(LibHelperCpk, self).__init__()
        self.cpk_api_settings = cpk_api_settings

    def cpk_library_ids(self):
        # all library ids
        ALL_LIBRARY_IDS = True
        if ALL_LIBRARY_IDS:
            # Plone app neumí (timeout) získat všechny library ids z CPK, toto
            # je načteno z dat získaných přímo z ziskej.cpk_api
            library_ids = sorted(cpk_all_libraries)

        # failed library ids - už je opraveno, takže už nemá smysl, ponecháno
        # jako inspirace pro případné rozšiřování
        # příklady chyb před opravou
        # failed for library.000003036 with: 0
        # failed for library.000003378 with: 'list' object has no attribute 'strip'
        # FAILED_LIBRARY_IDS = False
        # if FAILED_LIBRARY_IDS:
        #     library_ids = sorted(failed_library_ids)
        
        # testovací sada library ids
        library_ids = ['library.000008094', 'library.000003036', 'library.000003378', ]

        if DEBUG:
            print 'len(library_ids): ' + str(len(library_ids))
        return library_ids

    def fetch_cpk_libraries(self):
        self._data = []
        self._data_cpk_idx = dict()
        self._data_sigla_idx = dict()
        if DEBUG:
            print ' '
            print 'fetch_cpk_libraries'
            print ' '
        library_ids = self.cpk_library_ids()
        checked_ok = []
        checked_failed = []
        for idx, library_id in enumerate(library_ids):
            try:
                obj_dict = self.parse_cpk_library(library_id)
            except Exception, e:
                try:
                    print 'failed for ' + library_id + ' with: ' + str(e)
                except:
                    print 'failed for '
                    print library_id
                checked_failed.append(library_id)
                raise
                #continue
            checked_ok.append(library_id)
            _data_idx = len(self._data)
            self._data.append(obj_dict)
            self._data_cpk_idx[library_id] = _data_idx
            self._data_sigla_idx[obj_dict['id']] = _data_idx

        if DEBUG:
            print ' '
            print 'ok'
            print checked_ok
            print ' '
            print 'failed'
            print checked_failed
            print ' '

    def fetch_cpk_library(self, library_id):
        cpklibrary = cpk_api.CpkLibrary(self.cpk_api_settings, library_id=library_id)
        cpklibrary.call()
        if DEBUG:
            print 'data for ' + str(library_id)
            pprint(cpklibrary.data)
            print " "
        return cpklibrary.data

    def parse_cpk_library(self, library_id):
        data = self.fetch_cpk_library(library_id)

        # sigla
        obj_id = data.get('SGL').lower()

        # obj_dict keys: ['library_id', 'id', 'sigla', 'fullname', 'shortname',
        #                 'ico', 'url', 'address', 'cpkdata', ]
        obj_dict = dict(
            id = obj_id,  # sigla lower: aba001
            sigla = obj_id.upper(),  # sigla upper: ABA001
            library_id = library_id,  # cpk library id
            cpkdata = data,  # original cpk data
            )

        try:
            obj_dict = self._parse_cpkdata(obj_dict, data)
        except cpk_api.CpkApiException, e:
            print 'library_id: ' + library_id + ' ERROR: ' + str(e)
            logger.warning('Unknown data: ' + str(e))
            raise

        return obj_dict


class LibHelperPortalSync(LibHelperCpk):
    """
    """

    #obj_attr_names = ['library_id', 'id', 'sigla', 'fullname', 'shortname',
    #                  'ico', 'url', 'address', 'cpkdata', ]
    create_keys = ['sigla', 'fullname', 'shortname', 'ico', 'url', 'address',
                   'invoice_name', 'invoice_address', 'phone', 'phone_mvs',
                   'email', 'email_mvs', ]
    update_keys = ['fullname', 'url', 'address', ]
    full_update_keys = ['fullname', 'shortname', 'ico', 'url', 'address',
                        'invoice_name', 'invoice_address', 'phone', 'phone_mvs',
                        'email', 'email_mvs', ]

    def __init__(self, portal):
        # settings None .. use default fropm ziskej.site.cpk_api
        super(LibHelperPortalSync, self).__init__(None)
        self.portal = portal
        if self.portal:
            self.libs_obj = self.portal.libraries
            self.lib_ids = self.libs_obj.objectIds()
            self.load_data()
        else:
            self.libs_obj = None
            self.lib_ids = []

    def _transaction(self, dry=False):
        if dry:
            return
        # Commit transaction
        transaction.commit()
        # Perform ZEO client synchronization (if running in clustered mode)
        app._p_jar.sync()  # used only within "instance run" where app is defined

    def portal_update(self, is_create=True, is_update=True, is_full_update=False, dry=False):
        VERBOSE = True

        # check for multiple records
        touched_data_idx = []
        touched_lib_ids = []

        # update
        updated_count = 0
        unchanged_count = 0
        if VERBOSE:
            updated_lib_ids = []
            unchanged_lib_ids = []
        if is_update:
            for lib_id in self.lib_ids:

                # existing library not on cpk data
                if lib_id not in self._data_sigla_idx:
                    logger.warning('Library {} not available in CPK data.'.format(lib_id))
                    continue
                _data_idx = self._data_sigla_idx[lib_id]

                # check for multiple records
                if _data_idx in touched_data_idx or lib_id in touched_lib_ids:
                    raise Exception('Multiple use of {} / {} in portal_update update.'.format(_data_idx, lib_id))
                touched_data_idx.append(_data_idx)
                touched_lib_ids.append(lib_id)

                # update
                obj_dict = self._data[_data_idx]
                obj = self.libs_obj[lib_id]
                is_updated = self._update_ct(obj, obj_dict, dry=dry, is_full_update=is_full_update)
                self._transaction(dry=dry)

                # counters
                if is_updated:
                    if VERBOSE:
                        updated_lib_ids.append(lib_id)
                    updated_count += 1
                else:
                    if VERBOSE:
                        unchanged_lib_ids.append(lib_id)
                    unchanged_count += 1

        # create
        if VERBOSE:
            created_lib_ids = []
        created_count = 0
        if is_create:
            _data_idx = 0
            for obj_dict in self._data:

                # skip updated
                if obj_dict['id'] in self.lib_ids:
                    _data_idx += 1
                    continue

                # check for multiple records
                lib_id = str(obj_dict['id'])
                if _data_idx in touched_data_idx or lib_id in touched_lib_ids:
                    raise Exception('Multiple use of {} / {} in portal_update create.'.format(_data_idx, lib_id))
                # touched_data_idx tady při postupném procházení nepotřebuejeme
                # měnit, jde jen o to, co už bylo z update
                #touched_data_idx.append(_data_idx)  
                touched_lib_ids.append(lib_id)

                # create
                blacklist_lib_ids = []
                #blacklist_lib_ids = ['aba000', 'aba003', 'aba004', 'aba005', ]
                if lib_id not in blacklist_lib_ids:
                    if DEBUG:
                        print 'create library {} for {}: {}'.format(obj_dict['id'], obj_dict['library_id'], safe_utf8(obj_dict['fullname']), )
                    if not dry:
                        obj = self._create_ct(obj_dict)
                        self._transaction(dry=dry)
                else:
                    if DEBUG:
                        print 'blacklist: do not create library {} for {}: {}'.format(obj_dict['id'], obj_dict['library_id'], safe_utf8(obj_dict['fullname']), )

                # counters
                if VERBOSE:
                    created_lib_ids.append(lib_id)
                created_count += 1
                _data_idx += 1

        # debug
        if VERBOSE:
            print ' '
            print 'updated: '
            print updated_lib_ids
            print ' '
            print 'unchanged_count: '
            print unchanged_lib_ids
            print ' '
            print 'created: '
            print created_lib_ids
            print ' '
        if DEBUG:
            print 'updated: ' + str(updated_count)
            print 'unchanged_count: ' + str(unchanged_count)
            print 'created: ' + str(created_count)

        return (updated_count, unchanged_count, created_count, )

    def _create_ct(self, obj_dict):
        # see https://docs.plone.org/external/plone.app.dexterity/docs/reference/manipulating-content-objects.html
        # see alse https://docs.plone.org/develop/plone/content/creating.html
        obj_id = str(obj_dict['id'])
        obj_dict_ = dict(id=obj_id)
        # obj_dict keys: ['library_id', 'id', 'sigla', 'fullname', 'shortname',
        #                 'ico', 'url', 'address', ]
        for k in self.create_keys:
            obj_dict_[k] = obj_dict[k]
        obj = createContent('Library', **obj_dict_)
        self.libs_obj[obj_id] = obj
        obj = self.libs_obj[obj_id]
        obj.cpkdata = json.dumps(obj_dict['cpkdata'], ensure_ascii=False)
        obj.add_historylog(value=u'Created from CPK API source')
        obj.reindexObject()
        return obj

    def _update_ct(self, obj, obj_dict, dry=False, is_full_update=False):
        if DEBUG:
            logger.info('update library {}'.format(obj_dict['id']))
        is_updated = False
        if is_full_update:
            update_keys = self.full_update_keys
        else:
            update_keys = self.update_keys
        for k in update_keys:
            v_orig = getattr(obj, k, None)
            v = obj_dict.get(k, None)
            if v_orig != v:
                if not dry:
                    setattr(obj, k, v)
                if DEBUG:
                    logger.info(u'doit for {k}: {v_orig} to {v}'.format(k=k, v_orig=v_orig, v=v))
                is_updated = True
            elif DEBUG:
                logger.info(u'nothing to do for {k}: {v_orig} to {v}'.format(k=k, v_orig=v_orig, v=v))

        if is_updated and not dry:
            obj.cpkdata = json.dumps(obj_dict['cpkdata'], ensure_ascii=False)
            obj.add_historylog(value=u'Updated from CPK API source')
            obj.reindexObject()
        return is_updated


class LibHelper(LibHelperPortalSync):
    """
    """

    def __init__(self, portal):
        # settings None .. use default fropm ziskej.site.cpk_api
        super(LibHelper, self).__init__(portal)
        self.portal = portal
        if self.portal:
            self.libs_obj = self.portal.libraries
            self.lib_ids = self.libs_obj.objectIds()
            self.catalog = api.portal.get_tool(name='portal_catalog')
        else:
            self.libs_obj = None
            self.lib_ids = []
        self.lib_ids_active = []
        self.cpkapi_including_deactivated_cache = dict()
        self.load_data()

    def bind_portal(self, portal):
        if not portal or self.portal is not None:
            return
        logger.info('LibHelper.bind_portal inner')
        self.portal = portal
        self.libs_obj = self.portal.libraries
        self.catalog = api.portal.get_tool(name='portal_catalog')

    def update_lid_ids(self):
        # bez tohoto volání v rámci requestu neposkytne aktuální data
        self.lib_ids = self.libs_obj.objectIds()
        query = dict(
            path = '/'.join(self.libs_obj.getPhysicalPath()),
            portal_type = 'Library',
            review_state = 'published',
            sort_on = 'sigla',
            )
        brains = self.catalog(query)
        self.lib_ids_active = [x['getId'] for x in brains]
        logger.info('LibHelper.refresh {}/{}'.format(len(self.lib_ids_active), len(self.lib_ids)))
        if len(self.lib_ids_active) == 0:
            logger.warning('Invalid LibHelper.refresh')

    def get_title(self, sigla):
        obj_dict = self.obj_dict_by_sigla(sigla)
        if not obj_dict:
            return u''
        #pprint(obj_dict)
        return obj_dict.get('fullname', sigla)

    def get_title_with_sigla(self, sigla):
        obj_dict = self.obj_dict_by_sigla(sigla)
        if not obj_dict:
            return u''
        #pprint(obj_dict)
        return u'{fullname} ({sigla})'.format(
            fullname=obj_dict.get('fullname', sigla),
            sigla=sigla.upper())

    def obj_dict_by_sigla(self, sigla):
        if not sigla:
            return None
        sigla = sigla.lower().strip()
        idx = self._data_sigla_idx.get(sigla, None)
        if not idx:
            return None
        return self._data[idx]

    def library_by_cpk(self, library_id):
        if library_id not in self._data_cpk_idx:
            return None
        lib_id = self._data[self._data_cpk_idx[library_id]]['id']  # sigla aba001
        if not hasattr(self.libs_obj, lib_id):
            return None
        return getattr(self.libs_obj, lib_id)

    def library_by_sigla(self, sigla):
        if not sigla:
            return None
        lib_id = sigla.lower()
        if not hasattr(self.libs_obj, lib_id):
            return None
        return getattr(self.libs_obj, lib_id)

    def library_sync(self, obj, is_full_update=False):
        # self, obj, obj_dict, dry=False, is_full_update=False
        cpk_dict = self.cpk_by_sigla(obj.sigla)
        if not cpk_dict:
            raise Exception('Cannot find library_cpkid for this sigla')
        library_cpkid = cpk_dict.get('library_id', None)
        if not library_cpkid:
            raise Exception('Invalid library_cpkid for this sigla')
        obj_dict = self.parse_cpk_library(library_cpkid)
        is_updated = self._update_ct(obj, obj_dict, is_full_update=is_full_update)
        return is_updated

    def invalidate_cpkapi_including_deactivated_cache(self):
        self.cpkapi_including_deactivated_cache = dict()

    def set_cpkapi_including_deactivated_cache(self, service, value):
        self.cpkapi_including_deactivated_cache[service] = list(value)

    def get_cpkapi_including_deactivated_cache(self, service):
        if self.cpkapi_including_deactivated_cache.get(service, None) is None:
            return None
        return list(self.cpkapi_including_deactivated_cache[service])

    def is_cpkapi_including_deactivated_cache(self, service):
        return bool(self.cpkapi_including_deactivated_cache.get(service, None)
                    is not None)


lib_helper = LibHelper(None)
# při inicializaci není portal k dispozici, workaround je pomocí kódu
# v ziskej.site.browser ZiskejBrowserView._call_init()
#portal = api.portal.get()
#portal = app.ziskej
#lib_helper.bind_portal(portal)
