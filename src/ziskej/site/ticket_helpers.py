# -*- coding: utf-8 -*-
"""Pomocné nástroje týkající se ticketu (objednávky) a workflow:

- obsahuje vše potřebné na vytvoření ticketu a důležité operace s ním
- obsahuje utility pro notifikace
- obsahuje utility pro seznamy požadavků podle různých kritérií
- atd.
"""

import json
import logging
import urllib
import uuid
from datetime import datetime

import transaction
from AccessControl.unauthorized import Unauthorized
from Acquisition import aq_inner, aq_parent
from plone import api
from plone.dexterity.utils import createContent
from plone.protect.utils import addTokenToUrl

from ziskej.site import CPK_DEV_BASE_URL, CPK_DEV_RECORDE_URL
from ziskej.site.utils import safe_utf8, pprint


logger = logging.getLogger('ziskej.ticket_helpers')

DEBUG = False


# used portal_types
TICKET_PORTAL_TYPES = ('TicketMVS', )
SUBTICKET_PORTAL_TYPES = ('SubticketMVS', )
REPORT_ENTRY_PORTAL_TYPES = ('ReportEntry', )
TICKET_SUBTICKET_PORTAL_TYPES = []
TICKET_SUBTICKET_PORTAL_TYPES.extend(TICKET_PORTAL_TYPES)
TICKET_SUBTICKET_PORTAL_TYPES.extend(SUBTICKET_PORTAL_TYPES)
# Human ID - minimum
TICKET_HID_MIN=100001  # hodí se pro int/str převody, exporty a tisky
SUBTICKET_HID_MIN=1  # tady asi nepotřebujeme začínat na 101

# TicketMVS stavy a koncové stavy
TICKETMVS_WF_S = ['created', 'assigned', 'pending', 'prepared', 'closed', 
        'cancelled', 'rejected', ]
TICKETMVS_WF_S_OPEN = ['created', 'assigned', 'pending', 'prepared', ]
TICKETMVS_WF_S_LABELS = dict(
    created = u'Nová',
    assigned = u'Ve zpracování',
    pending = u'Čeká na rozhodnutí',
    prepared = u'Vyřízena',
    closed = u'Uzavřena',
    cancelled = u'Stornována',
    rejected = u'Zamítnuta',
    )
TICKETMVS_WF_S_END = ['closed', 'cancelled', 'rejected', ]

# SubticketMVS stavy a koncové stavy
SUBTICKETMVS_WF_S = ['queued', 'conditionally_accepted', 'accepted', 'sent', 'sent_back', 'closed', 'cancelled', 'refused', ]
SUBTICKETMVS_WF_S_OPEN = ['queued', 'conditionally_accepted', 'accepted', 'sent', 'sent_back', ]
SUBTICKETMVS_WF_S_LABELS = dict(
    queued = u'Nový',
    conditionally_accepted = u'Přijato s podmínkou',
    accepted = u'Ve zpracování',
    sent = u'Vyřízeno - odesláno',
    sent_back = u'Vyřízeno - Odesláno zpět knihovně',
    closed = u'Uzavřeno',
    cancelled = u'Stornována',
    refused = u'Odmítnuto',
    )
SUBTICKETMVS_WF_S_END = ['accepted_back', 'refused', 'changed', 'lost', ]


def get_ticket_wf_label(review_state, lower=False, short=False):
    if short:
        label = TICKETMVS_WF_S_LABELS.get(review_state, review_state)
    else:
        label = TICKETMVS_WF_S_LABELS.get(review_state, review_state)
    if lower:
        label = label.lower()
    return label

def get_subticket_wf_label(review_state, lower=False, short=False):
    label = SUBTICKETMVS_WF_S_LABELS.get(review_state, review_state)
    if lower:
        label = label.lower()
    return label

def wf_action(obj, transition_id, comment):
    api.content.transition(obj=obj, transition=transition_id, comment=comment)

def get_wf_url(context_url, transition_id, comment, add_token=True):
    logger.info('deprecated: get_wf_url')
    params = dict(
        workflow_action = transition_id,
        comment = safe_utf8(comment),
        )
    redirect_url = context_url + '/content_status_modify?'
    redirect_url += urllib.urlencode(params)
    if add_token:
        redirect_url = addTokenToUrl(redirect_url)
    return redirect_url

def get_next_ticket_hid(catalog=None):
    """Vrátí další hid pro ticket, neřeší transakce.
    """

    if catalog is None:
        catalog = api.portal.get_tool(name='portal_catalog')

    query = dict(
        portal_type='TicketMVS',
        sort_on='hid',
        sort_order='reverse',
        sort_limit=1,
        )
    brains = catalog(query)[:1]
    if not len(brains):
        return TICKET_HID_MIN
    return brains[0].hid + 1

def get_next_subticket_hid(ticket=None, catalog=None):
    """Vrátí další hid pro subticket, neřeší transakce.
    """

    if catalog is None:
        catalog = api.portal.get_tool(name='portal_catalog')

    query = dict(
        path='/'.join(ticket.getPhysicalPath()),
        portal_type='SubticketMVS',
        sort_on='hid',
        sort_order='reverse',
        sort_limit=1,
        )
    brains = catalog(query)[:1]
    if not len(brains):
        return SUBTICKET_HID_MIN
    return brains[0].hid + 1

def create_ticketmvs(obj_dict, portal=None, catalog=None):
    """Vytvoří TicketMVS objekt v portal.tickets s obsahem obj_dict a s náhodným
    16 znakovým id.
    """

    if portal is None:
        portal = api.portal.get()
    parent = portal.tickets
    hid = get_next_ticket_hid(catalog=catalog)
    obj_dict['hid'] = hid
    obj = _create_obj(parent=parent, portal_type='TicketMVS', obj_dict=obj_dict)
    return obj

def create_subticketmvs(ticket, obj_dict, catalog=None):
    """Vytvoří SubticketMVS objekt v ticket s obsahem obj_dict a s náhodným
    16 znakovým id.
    """

    # změna, nyní se používá ticket.hid (2017-10)
    #hid = get_next_subticket_hid(ticket=ticket, catalog=catalog)
    #obj_dict['hid'] = hid
    obj = _create_obj(parent=ticket, portal_type='SubticketMVS', obj_dict=obj_dict)
    return obj

def create_reportentry(subticket, obj_dict):
    """Vytvoří Report objekt v subticket s obsahem obj_dict a s náhodným
    16 znakovým id.

    Očekává report_date, cena, user_id, entry_type a volitelně i note v obj_dict.

    obj_dict = dict(
        report_date = sent_date_dt,
        cena = self.subticket.cena,
        user_id = self.user_id,
        entry_type = u'std',
        note = u'',
        )

    """

    parent = subticket
    ticket = subticket.ticket

    # expected report_date*, cena*, entry_type*, note in obj_dict
    obj_dict['ticket_type'] = ticket.ticket_type
    obj_dict['ticket_id'] = ticket.getId()
    obj_dict['subticket_id'] = subticket.getId()
    obj_dict['hid'] = subticket.hid
    obj_dict['hid_prefix'] = subticket.hid_prefix
    obj_dict['library_zk'] = subticket.library_zk
    obj_dict['library_dk'] = subticket.library_dk
    obj_dict['library_zk_title'] = subticket.library_zk_title
    obj_dict['library_dk_title'] = subticket.library_dk_title

    obj = _create_obj(parent=parent, portal_type='ReportEntry', obj_dict=obj_dict)
    if ticket.ticket_type == 'mvs':
        obj.setup(is_zk_std=bool(subticket.library_zk != 'aba013'),
                  is_dk_std=bool(subticket.library_dk != 'aba013'),
                  is_paid_in_advance=ticket.is_paid_in_advance())
    elif ticket.ticket_type == 'edd':
        # FIXME-EDD Platby
        logger.info('FIXME-EDD Platby')
    return obj

def _create_obj(parent=None, obj_id=None, portal_type=None, obj_dict=dict()):
    """Vytvoří objekt typu portal_type (povinné) v rámci parent (None .. 
    portal.tickets) s obj_id nebo náhodným 16 místným id unikátním v rámci
    parent.  V případě, že obj_id je zadáno a existuje v rámci parent, je místo
    něj zvoleno náhodné jiné.  Počteční data objektu jsou v obj_dict.
    """

    # print " "
    # print "obj_dict: "
    # pprint(obj_dict)
    # print " "

    # web by reader
    """
obj_dict:
{'created_date': '2019-08-13',
 'doc_data': {"fullname": ". BCG vaccination policies. 17 stran.", "version": 2, "doc_id": "mzk.MZK01-001542205", "unit_ids": ["BOA001.MZK01001542205.MZK50001577303000010"], "sigla": "BOA001"},
 'doc_fullname': . BCG vaccination policies. 17 stran.,
 'doc_id': mzk.MZK01-001542205,
 'doc_manual': False,
 'doc_title': . BCG vaccination policies. 17 stran.,
 'docs_data': {"doc_ids": ["mzk.MZK01-001542205", "nlk.69370", "nkp.NKC01-000266332"], "version": 2, "data": {"mzk.MZK01-001542205": {"doc_id": "mzk.MZK01-001542205", "unit_ids": ["BOA001.MZK01001542205.MZK50001577303000010"], "sigla": "BOA001"}, "nlk.69370": {"doc_id": "nlk.69370", "unit_ids": ["ABA008.K101649", "ABA008.K19810604"], "sigla": ""}, "nkp.NKC01-000266332": {"doc_id": "nkp.NKC01-000266332", "unit_ids": ["ABA001.NKC01000266332.NKC50000266332000010"], "sigla": "ABA001"}}},
 'hid': 100022,
 'lb_manual_library': ,
 'lb_manual_library_sent': ,
 'lb_state': ,
 'librarian': ,
 'library_zk': aba013,
 'library_zk_note': ,
 'library_zk_title': ABA013: NTK,
 'reader': 'ntk_ctenar',
 'reader_approval': ,
 'reader_date': '2019-08-21',
 'reader_firstname': ,
 'reader_lastname': ,
 'reader_lid': 123456,
 'ticket_type': mvs,
 'unread_zk': True}
    """

# api
    """
obj_dict:
{'created_date': '2019-08-13',
 'doc_data': {"fullname": ". BCG vaccination policies. 17 stran.", "version": 2, "doc_id": "mzk.MZK01-001542205", "unit_ids": ["BOA001.MZK01001542205.MZK50001577303000010"], "sigla": "BOA001"},
 'doc_fullname': . BCG vaccination policies. 17 stran.,
 'doc_id': mzk.MZK01-001542205,
 'doc_manual': False,
 'doc_title': . BCG vaccination policies. 17 stran.,
 'docs_data': {"doc_ids": ["mzk.MZK01-001542205", "nlk.69370", "nkp.NKC01-000266332"], "version": 2, "data": {"mzk.MZK01-001542205": {"doc_id": "mzk.MZK01-001542205", "unit_ids": ["BOA001.MZK01001542205.MZK50001577303000010"], "sigla": "BOA001"}, "nlk.69370": {"doc_id": "nlk.69370", "unit_ids": ["ABA008.K101649", "ABA008.K19810604"], "sigla": ""}, "nkp.NKC01-000266332": {"doc_id": "nkp.NKC01-000266332", "unit_ids": ["ABA001.NKC01000266332.NKC50000266332000010"], "sigla": "ABA001"}}},
 'hid': 100024,
 'lb_manual_library': ,
 'lb_manual_library_sent': ,
 'lb_state': ,
 'librarian': ,
 'library_zk': 'aba013',
 'library_zk_note': ,
 'library_zk_title': ABA013: NTK,
 'reader': 'ntk_ctenar',
 'reader_approval': ,
 'reader_date': '2019-09-01',
 'reader_firstname': Čtenář,
 'reader_lastname': NTK,
 'reader_lid': ,
 'ticket_type': mvs,
 'unread_zk': True}
    """

    if portal_type is None:
        raise Exception('Missing parameter')

    while obj_id is None or hasattr(parent, obj_id):
        obj_id = str(uuid.uuid4()).replace('-', '')[:16]
    obj_dict['id'] = obj_id

    if DEBUG:
        print ' '
        print 'createContent'
        pprint(obj_dict)
        print ' '

    obj = createContent(portal_type, **obj_dict)
    parent[obj_id] = obj
    obj = parent[obj_id]

    if DEBUG:
        print 'obj'
        for k in ('doc_title', 'doc_number', 'doc_author', 'doc_issuer', 'doc_isbn',
                'doc_issn', 'doc_signature'):
            print k, getattr(obj, k, None)
        print ' '

    if portal_type in ('TicketMVS', ):
        obj.add_historylog(
            u'<strong>Objednávka vytvořena</strong>',
            who=['zk', 'r'],
            )
    elif portal_type in ('SubticketMVS', ):
        obj.add_historylog(
            u'<strong>Požadavek vytvořen</strong> a přiřazen knihovně.',
            who=['dk'],
            )
        parent.add_historylog(
            u'<strong>Požadavek vytvořen</strong> a přiřazen knihovně.',
            who=['zk'],
            )

    logger.info('{portal_type} {obj_id} has been created'.format(portal_type=portal_type, obj_id=obj_id))

    return obj

def get_cpk_url(doc_id=None):
    url = CPK_DEV_BASE_URL
    if doc_id:
        url += CPK_DEV_RECORDE_URL
        url += str(doc_id)
    return url

def generate_fullname_manual(obj_dict):
    # https://www.knihovny.cz/Record/mzk.MZK01-001318874
    # FIELDING, Joy. Potok stínů. Vyd. 1. Praha: Ikar, 2013. 302 s. ISBN 978-80-249-2210-2.
    fullname = u''

    if obj_dict['doc_author']:
        fullname += u'{doc_author}. '.format(**obj_dict)
    if obj_dict['doc_title']:
        fullname += u'{doc_title}. '.format(**obj_dict)
    if obj_dict['doc_number']:
        fullname += u'{doc_number}. '.format(**obj_dict)
    if obj_dict['doc_issuer']:
        fullname += u'{doc_issuer}. '.format(**obj_dict)
    if obj_dict['doc_isbn']:
        fullname += u'ISBN {doc_isbn}. '.format(**obj_dict)
    if obj_dict['doc_issn']:
        fullname += u'ISSN {doc_issn}. '.format(**obj_dict)
    if obj_dict['doc_signature']:
        fullname += u'{doc_signature}. '.format(**obj_dict)
    #print 'fullname'
    #print fullname
    return fullname


def generate_quote_edda(obj_dict, edd_data):
    """VOPĚNKA, Petr. Neexistence množiny všech přirozených čísel. Vesmír. Praha, 2015, 94(6), 12-24. ISSN 0042-4544.
    """

    fullname = u''
    if obj_dict['doc_author']:
        fullname += u'{doc_author}. '.format(**obj_dict)
    if obj_dict['doc_title']:
        fullname += u'{doc_title}. '.format(**obj_dict)
    if fullname:
        # předpokládá fullname s '. ' na konci, což je zajištěno neprázdností
        fullname = fullname[:-2] + u' in '
    if edd_data['doc_title_in']:
        fullname += u'{doc_title_in}. '.format(**edd_data)
    if obj_dict['doc_issuer']:
        fullname += u'{doc_issuer}, '.format(**obj_dict)
    if edd_data['doc_number_pyear']:
        fullname += u'{doc_number_pyear}'.format(**edd_data)
    if edd_data['doc_number_pnumber']:
        fullname += u'({doc_number_pnumber})'.format(**edd_data)
    if edd_data['doc_number_pyear'] or edd_data['doc_number_pnumber']:
        fullname += u', '
    if edd_data['pages_from']:
        fullname += u'{pages_from}'.format(**edd_data)
        if edd_data['pages_to'] and \
                edd_data['pages_from'] != edd_data['pages_to']:
            fullname += u'-{pages_to}'.format(**edd_data)
        fullname += u'. '
    if obj_dict['doc_issn']:
        fullname += u'ISSN {doc_issn}. '.format(**obj_dict)
    fullname = fullname.strip()
    return fullname

def generate_quote_eddb(obj_dict, edd_data):
    """VOPĚNKA, Petr. Neexistence množiny všech přirozených čísel. Vesmír. Praha, 2015, 12-24. ISBN 0042-4544.
    """

    fullname = u''
    if obj_dict['doc_author']:
        fullname += u'{doc_author}. '.format(**obj_dict)
    if obj_dict['doc_title']:
        fullname += u'{doc_title}. '.format(**obj_dict)
    if fullname:
        # předpokládá fullname s '. ' na konci, což je zajištěno neprázdností
        fullname = fullname[:-2] + u' in '
    if edd_data['doc_title_in']:
        fullname += u'{doc_title_in}. '.format(**edd_data)
    if obj_dict['doc_issuer']:
        fullname += u'{doc_issuer}, '.format(**obj_dict)
    if edd_data['doc_number_pyear']:
        fullname += u'{doc_number_pyear}'.format(**edd_data)
    if edd_data['doc_number_pnumber']:
        fullname += u'({doc_number_pnumber})'.format(**edd_data)
    if edd_data['doc_number_pyear'] or edd_data['doc_number_pnumber']:
        fullname += u', '
    if edd_data['pages_from']:
        fullname += u'{pages_from}'.format(**edd_data)
        if edd_data['pages_to'] and \
                edd_data['pages_from'] != edd_data['pages_to']:
            fullname += u'-{pages_to}'.format(**edd_data)
        fullname += u'. '
    if obj_dict['doc_isbn']:
        fullname += u'ISBN {doc_isbn}. '.format(**obj_dict)
    fullname = fullname.strip()
    return fullname

def get_ticket_by_id(ticket_id, catalog=None):
    if catalog is None:
        catalog = api.portal.get_tool(name='portal_catalog')
    query = dict(
        portal_type = TICKET_PORTAL_TYPES,
        getId = ticket_id,
        )
    brains = catalog.unrestrictedSearchResults(query)
    if not len(brains):
        return None
    return brains[0]._unrestrictedGetObject()

def get_ticket_by_hid(hid, catalog=None):
    if catalog is None:
        catalog = api.portal.get_tool(name='portal_catalog')
    query = dict(
        portal_type = TICKET_PORTAL_TYPES,
        hid = hid,
        )
    brains = catalog.unrestrictedSearchResults(query)
    if not len(brains):
        return None
    return brains[0]._unrestrictedGetObject()

def get_subticket_by_id(subticket_id, catalog=None):
    if catalog is None:
        catalog = api.portal.get_tool(name='portal_catalog')
    query = dict(
        portal_type = SUBTICKET_PORTAL_TYPES,
        getId = subticket_id,
        )
    brains = catalog.unrestrictedSearchResults(query)
    if not len(brains):
        return None
    return brains[0]._unrestrictedGetObject()

def get_report_entry_by_id(report_entry_id, catalog=None):
    if catalog is None:
        catalog = api.portal.get_tool(name='portal_catalog')
    query = dict(
        portal_type = REPORT_ENTRY_PORTAL_TYPES,
        getId = report_entry_id,
        )
    brains = catalog.unrestrictedSearchResults(query)
    if not len(brains):
        return None
    return brains[0]._unrestrictedGetObject()

def get_subticket_by_report_entry(report_entry=None, report_entry_id=None, catalog=None):
    if catalog is None:
        catalog = api.portal.get_tool(name='portal_catalog')
    if report_entry is None:
        report_entry = get_report_entry_by_id(report_entry_id, catalog=catalog)
    if report_entry is None:
        return None
    return get_subticket_by_id(report_entry.subticket_id, catalog=catalog)

def list_tickets_by_sigla(sigla, service, catalog=None):
    if catalog is None:
        catalog = api.portal.get_tool(name='portal_catalog')
    if not sigla:
        return []
    sigla = sigla.lower()
    query = dict(
        portal_type = TICKET_PORTAL_TYPES,
        library_zk = sigla,
        )
    if service:
        query['ticket_type'] = service
    brains = catalog.unrestrictedSearchResults(query)
    return brains

def list_subtickets_by_sigla(sigla, service, catalog=None):
    if catalog is None:
        catalog = api.portal.get_tool(name='portal_catalog')
    if not sigla:
        return []
    sigla = sigla.lower()
    query = dict(
        portal_type = TICKET_PORTAL_TYPES,
        library_dk = sigla,
        )
    if service:
        query['ticket_type'] = service
    brains = catalog.unrestrictedSearchResults(query)
    return brains
