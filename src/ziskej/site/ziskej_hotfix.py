# -*- coding: utf-8 -*-
from __future__ import print_function

from datetime import datetime
import logging
import uuid

from plone import api

from ziskej.site.utils_lib import pprint


logger = logging.getLogger("ziskej_hotfix")


def hotfix_payment_log_kvg501_101482():
    """Hotfix opravující chybějící zápis v logu plateb pro připsání platby
    za požadavek hid 101482 dožádané knihovně KVG501.
    """

    portal = api.portal.get()

    try:
        subticket = portal.tickets['423dbd3efcff47b3']['ea91b63875c645ca']
        report_entry = subticket['c0c13ab42ceb47c8']
    except:
        return u'Skipped not applicable hotfix_payment_log_kvg501_101482'

    # query = dict(
    #     portal_type='ReportEntry',
    #     path='/'.join(subticket.getPhysicalPath()),
    # )
    # catalog = api.portal.get_tool(name='portal_catalog')
    # brains = catalog(query)
    # for brain in brains:
    #     obj = brain.getObject()
    # report_entry_dict = report_entry.export_as_dict(use_repr=True)

    library = portal.libraries['kvg501']
    data = library.credit_log_data(None)
    data.reverse()
    hotfix_item = None
    for item in data:
        if item.get('hid', None) == 101482:
            hotfix_item = item
            break

    if hotfix_item is not None:
        return u'Skipped hotfix_payment_log_kvg501_101482'

    # Připravit informace
    action_type = u'std'
    payment_id = u'{random_part}-{hid}'.format(
        random_part=str(uuid.uuid4()).replace('-', '')[-12:],
        hid=subticket.hid,
        )
    timestamp = datetime.now().isoformat()[:19].replace('T', ' ')
    payment_timestamp = '2021-03-18 08:26:00'
    amount = abs(report_entry.cena)

    # Připsat poplatek na credit_ziskej DK
    library.mvs_payment_credit_ziskej(
        subticket,
        report_entry,
        payment_id,
        timestamp,
        action_type=action_type,
        payment_timestamp=payment_timestamp,
        )

    # Audit log
    # MVS DK std
    subticket.add_historylog(
        u'Do kreditu v Získej byl připsán MVS poplatek {} Kč.  Poplatek byl administrátorem připsán dodatečně.'.format(
            amount),
        who = ['dk', ])

    logger.warn('Applied hotfix_payment_log_kvg501_101482.')
    return u'Applied hotfix_payment_log_kvg501_101482'

def hotfix_all():
    """Aplikovat všechny aplikovatelné hotfixy.

    from ziskej.site.ziskej_hotfix import hotfix_all
    hotfix_all()
    """

    logger.info('Applying available hotfixes.')
    result = []
    result.append(hotfix_payment_log_kvg501_101482())
    logger.info('Applying available hotfixes - done.')
    return result
